UPGRADE FROM 1.16 to 1.17
=========================
If you want to update from a lower version than 1.16 to 1.17, please read previous UPGRADE notes.

PHP 7.3 support
---------------
Configuratorware 1.17 is tested with PHP 7.3.

Symfony 4 update
----------------
Configuratorware 1.17 requires an update from Symfony 3.4 to 4.4. To prepare for the update, follow these steps:

- fix deprecations in your custom code, read https://symfony.com/doc/4.4/setup/upgrade_major.html
- if you can't find `symfony/flex` in composer.json, move to symfony flex: https://symfony.com/doc/current/setup/flex.html#upgrade-to-flex
- Update your composer.json (especially dev requirements) to be ready for Symfony 4 and DoctrineBundle 2. A composer.json can look like this:

```
{
    "type": "project",
    "license": "proprietary",
    "require": {
        "symfony/flex": "^1.0",
        "configuratorware/configurator-api": "^1.15",
        "symfony/dotenv": ">=3.4"
    },
    "require-dev": {
        "doctrine/doctrine-fixtures-bundle": "^3.0",
        "symfony/browser-kit": "^4.4",
        "symfony/debug": "^4.4",
        "symfony/debug-bundle": "^4.4",
        "symfony/maker-bundle": "^1.21",
        "symfony/test-pack": "^1.0",
        "symfony/web-profiler-bundle": "^4.4"
    },
    ...
    "conflict": {
        "symfony/symfony": "*"
    },
    "extra": {
        "symfony": {
            "id": "",
            "allow-contrib": false
        }
    }
}

```

- remove the following registered Bundles from `config/bundles.php`:
    - Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle::class => ['all' => true],
    - Sensio\Bundle\DistributionBundle\SensioDistributionBundle::class => ['all' => true],
    - Doctrine\Bundle\DoctrineCacheBundle\DoctrineCacheBundle::class => ['all' => true],
- run `composer update`

Script Handler
--------------
In previous versions it was possible to use Symfony's deprecated Script Handler to run some of configuratorware's commands. This has been removed,
execute the commands natively.

Fixtures
--------
If you are using the Fixtures from configuratorware (which is not recommended) and load them by path you need to adjust your setup, as this
functionality was replaced by the `group feature` in DoctrineFixturesBundle 3 (compatibale with DoctrineBundle 2).

Database Schema
---------------
Previous versions contained a bug in the Importer that led to duplicate entries in the table optionclassification. This was fixed in this version. If
there already exist duplicate entries in your database, you cannot update your database. In this case mirgrate your data before update. To check if
this is a problem in your project, run the following statement on a copy of your production database:
`SELECT count(*), id FROM optionclassification GROUP BY identifier HAVING count(*) > 1` If the result of this statement is empty, you can safely
update without any further change.

Deprecations
------------

- The object GETParameterCollection was deprecated and replaced by ControlParameters
- The Command redhotmagmacore:updateschema has been removed, use native doctrine command `doctrine:schema:update` instead
- Global Constant C_ADMIN_MODE use ControlParameters->isAdminMode() instead
- Redhotmagma\ConfiguratorApiBundle\Repository\ColorRepository::findOneByColorPaletteAndColorIdentifier
- Redhotmagma\ConfiguratorApiBundle\Service\AdminMode\AdminModeHelper