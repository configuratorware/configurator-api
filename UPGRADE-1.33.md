# UPGRADE FROM 1.32 to 1.33
If you want to update from a lower version than 1.32 to 1.33, please read previous UPGRADE notes.

## Update to PHP 8.1 
From this version on Configuratorware will support the following PHP versions:
* 7.4
* 8.0
* 8.1

It is recommended to directly update to PHP 8.1 once the update to 1.33 is done, because the Support of 7.4 and 8.0 will be dropped soon.