# configuratorware api #

api and core logic for **configuratorware** configurator software.

## Development

1. create `.env.local`
2. use `.env.local` to override settings that you need different on local
3. `cp docker/.env.dist docker/.env` for adjustable environment variable (also app .env/.env.test/.env.local vars can be overwritten)
4. `docker-compose down -v && docker-compose  up -d`
5. `docker exec -u 1000 -it configuratorapi_app bash`
6. `phing setup:dev`
7. `phing test`

Notice: you need to be able to pull from our Docker registry server to build the container. Add *172.16.1.40 dev.docker-registry.local* to your hosts file. See [this article](https://docs.google.com/document/d/1JnKEaTS0uXys1yFZLllak7Ibp1F3cmniqzdk_r3472k) on how to add the certificate.

## Debugging
* To disable debugging when running tests, use `phing test` or `phing phpunit`
* For step debugging with tests use: `phing phpunit-debug`

## Setup debug in phpstorm
1. Check:
   * `$XDEBUG_REMOTE_HOST=` is set to `host.docker.internal` or an IP that docker can reach your local with
   * `$XDEBUG_START_WITH_REQUEST` is set to `"yes"` or `"trigger"`
1. go to Languages & Frameworks > PHP > `Servers`
1. add a new one with the following settings
   * Name: configuratorapi (see $PHP_IDE_CONFIG)
   * Host: localhost
   * Port: 10030 (= $HOST_HTTP_PORT)
   * Debugger: Xdebug
   * Add `/app` to path mapping
1. click on `Start listening for PHP Debug Connections`

# testing # 

## Guidelines
* test the happy path with an api test
* test each code branch with a unit test

### api tests
- use ApiTestCase
- fixture data is stored as sql file within a sub folder in the _data folder
- use redhotmagma/symfony-test-utils to store and load the response data:
    - Response-fixtures, a combination of meta information and the json-response, can be stored with ResponseFixtureTrait->storeResponseFixture()
    - Response-fixtures can be loaded with ResponseFixtureTrait->loadResponseFixture()

### unit tests
- use PHPUnit\TestCase
- use vfsStream to store files only in memory
- use Phake to generate Mock objects
- do not use IO!

# php cs fixer #

The project ships with a php cs fixer ruleset, which is checked on CI. You can run it by:
`phing php-cs-fixer:dev` from container, but its recommended to setup your IDE:

## PHPStorm Inspection:
https://www.jetbrains.com/help/phpstorm/using-php-cs-fixer.html

## PHPStorm file watcher:
* Open Preferences > Tools > File Watchers
* Create a custom one
* Settings:
    * File type: PHP
    * Scope: Project Files
    * Program: path/to/local/.composer/vendor/bin/php-cs-fixer
    * Arguments: fix $FileDir$/$FileName$
    * uncheck everything else

# Deprecations #

Set `SYMFONY_DEPRECATIONS_HELPER=` if you want to output the deprecations during test run.

# blackfire #

Change $PHP_DEBUGGER in your local `docker/.env`

# docs #

the documentation is done in restructured text format. after the docs have been changed they have to be rebuild. Go to the docs folder and execute:

``make clean && make html``

**TROUBLESHOOTING:**

* if you cannot access to [local documentation](http://localhost/docs/api/rest.html): Verify that a public `/app/_public/docs` folder exists, and if not, create it with the command `phing link:docs`
