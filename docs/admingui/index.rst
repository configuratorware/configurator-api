.. _admin_gui:
.. role:: bash(code)
   :language: bash
.. role:: javascript(code)
   :language: javascript

=========
Admin GUI
=========

*************************
Configuration Admin Panel
*************************

The administration gui is a JavaScript Application inside :bash:`${webroot}/admin` and can be configured in the containing index.html:

.. code-block:: html
    :linenos:

    <!doctype html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="shortcut icon" href="favicon.ico">
            <base href="/admin/">
            <link href="bundle.css" rel="stylesheet">
            <title>Admin Panel</title>
        </head>
        <body>
            <div id="root"></div>

            <script>
                var config = // configuration;
            </script>

            <script src="fabric.min.js"></script>
            <script src="bundle.js"></script>
        </body>
    </html>


The :javascript:`var config` contains a object Literal with the following main keys:

.. code-block:: javascript
    :linenos:

    var config = {
        features: [{ //... }]
        basePath: "/"
        baseURL: "api/"
    }

features
########
Defines the menu items of the admin area.

.. note::
    Admin area sections are only displayed if the user has the given rights. So it is possible, that config.features is configured properly but a menu item still does not show up, so test with full permissions granted.

"features" is an array and can contain multiple objects to define the specific section of the admin area by implementing a tree structue:

.. code-block:: javascript
    :linenos:

    {
        name: "items",
        items: [
            {
                name: "items",
                label: "productdata"
            },
            {
                name: "coredata",
                items: [
                    {
                        name: "attributes"
                    },
                    {
                        name: "attribute_values"
                        },
                        {
                            name: 'groups'
                        },
                        {
                            name: 'group_entries'
                        }
                ]
             },
             {
                name: "configuration_variants"
             },
        ]
    }

The available names referencing the sections in code, can be found in :bash:`src/Screens/index.js`.

basePath
########
Defines where the JavaScript and CSS files for the admin app are located.

baseURL
#######
Defines the base api endpoint for http admin area api calls.

********
Settings
********
There are several Settings that can be set in the admin area to customize configuratorware.

Basic Settings
##############

Clients
-------
A client defines how the configurator is branded for a customer. So there can be configured:

* Email
* Users
* Theme
* Contact

.. image:: assets/basic_settings_clients.png
    :align: center

Email
^^^^^
Defines the Owner/Administrator of the Client. The contact email can be specified separately (see below).
Multiple Clients can have the same Email address specified here.

Users
^^^^^
Create Users who can only access this client and e.g. change the theme data (if they are authorized for this section).

.. _admin_gui_theme:

Theme
^^^^^
The Theme consists out of 3 main settings:

* Theme Highlight Color
* Theme Font
* Client Logo

Contact
^^^^^^^
Contact data stored here is used where the configurator outputs contact data like in the configuration pdf.

Channels
--------
A channel manages the currency handling inside the configurator's prices and calculation.
Select a currency or choose to not display prices.


Designer Settings
#################

Core data / Production methods
------------------------------

Minimum Font Size
^^^^^^^^^^^^^^^^^
Defines the minimum font size for the design production method, measured in pixels (px).
Important: the metric size (eg. in millimeters) always depends on the "dot per inch" (DPI) setting of the designer,
which is 300 by default.

**Example #1:** I am an administrator, and I want to define the minimum font size for a design production method.

* I know it in millimeters, so I need to convert it to pixels.

*Concrete example:* we want to get the pixel size of a 2 mm font size, assuming the default DPI is used (300)

::

  "font size (px)" = "font size (mm)" * "DPI value" / 25.4
                   =         2        *     300     / 25.4
                   = ~24 (px)

* I know it in points (pt), so I need to convert it to pixels.

*Concrete example:* we want to get the pixel size of a 12pt font size, assuming the default DPI is used (300)

::

  "font size (px)" = "font size (pt)" * "DPI value" / 72
                   =        12        *     300     / 72
                   = 50 (px)

**Example #2:** I am an administrator, and I open the design production method settings to check/change the minimum
font size.

* I can see it in pixels, but I need it in millimeters.

*Concrete example:* we want to get the millimeter size of a 24 px font size, assuming the default DPI is used (300)

::

  "font size (mm)" = "font size (px)" / "DPI value" * 25.4
                   =        24        /     300     * 25.4
                   = ~2 (mm)

* I can see it in pixels, but I need it in points (pt).

*Concrete example:* we want to get the pt size of a 100 px font size, assuming the default DPI is used (300)

::

  "font size (pt)" = "font size (px)" / "DPI value" * 72
                   =        50        /     300     * 72
                   = 12 (pt)

Note: fonts are created by typeface designers (artists), so their actually measurable size almost always differs from
the selected font size, also from the size of other fonts.
In other words, a 2 mm (24 px by 300 dpi) font size can be measured in a range like from 1.8 mm to 2.5 mm,
depending on the selected font.
