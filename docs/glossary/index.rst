.. _glossary:

########
Glossary
########

.. glossary::
    adminarea
        Separated part of the application via a login to view and maintain the configurator data through an interface.

    administrator
        Identifies the super user of the adminarea, who has full access. There is also the role admin, which grants all access.

    basic costs
        As soon as a design area is provided with a text or an image, basic costs are charged for this printing area.
        The amount of the costs depends on the chosen production method. Depending on the process, basic costs are calculated depending on the number of colours, i.e. basic costs are calculated for each colour selected.

    base shape
        In order to have a 3D effect when using 2D photos, configuratorware provides base shapes. A base shape is an invisible geometric shape on which a design area is positioned. In the default case, this is a plane. To display e.g. a text “wrapping around” a ballpen or cup a cylinder can be used.

    component
        For the individualization of the item there is a set of components in the creator, a component is a part of the item e.g. a hood of a hoodie.

    configuration
       A configuration contains all information about the user's customized variant of an item. The user can either change options in the creator, change the design in the designer, or do both to create a new configuration in configuratorware.

    configuration variant
       A possibility to group parent items together.

    creator
        One of two types of configuratorware in which users can create their customized item by selecting options. The other type is *designer*.

    design area
        Describes the area onto which the user places his design.

    designer
        One of two types of configuratorware in which users can design their item using a built in graphics editor. Typically their design is printed on the item. The other type is *creator*.

    design production method
        Describes the method of application, i.e. different printing methods, engraving, embroidery, etc. with additional information such as minimum font size, permitted color palette(s) or data for calculating costs.

    design view
        Describes the view of an item in configuratorware, i.e. front side or back side.

    editor
        An editor is an adminarea user, who can edit the data in the adminarea.

    image gallery
        Gallery of SVG images, which are maintained by the administrator and can be placed on a design area by the user in the designer.

    image resolution
        The size of a raster image in Pixels (width/height).

    item
        The customizable base product or service.

    itemIdentifier
        The itemIdentifier is a core concept of configuratorware: it is a unique string that is used to open the configurator. Use only lowercase letters, numbers and no special characters except "-", "_" and "." to make sure that it can be used in an URL.

    layerImage
        Image to visualize each option of an item in the creator. configuratorware will create an item view by creating a stack of these images on top of each other.

    options
        Each component can have multiple options for a component, e.g. different colors.

    production information
        Includes all production-relevant data like used design production methods, texts, images, positions ond/or used options. It can be accessed within the *adminarea*.

    refinement
        With the refinement it is possible to add a visualization effect to the item, e.g. embroidery, embossing, etc.

    user
        The user of the application (creator / designer) who wants to use it to individualize his item.
