.. configuratorware documentation master file, created by
   sphinx-quickstart on Wed Mar  4 07:14:23 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Overview
========

In the :ref:`integration` section you will find information on how to integrate the configurator into your system.
How to import data, open the configurator and get information about configurations.

The :ref:`frontend_gui` section describes how to customize the look of your configurator frontend and how to add custom functionality.

The :ref:`admin_gui` section describes how to do basic settings in the admin area and how to add custom functionality.

The :ref:`administration` section describes how to administer configuratorware.

The :ref:`api` section describes the available api calls and how to add custom functionality.

The :ref:`media` section describes how to store images and 3D data.

The :ref:`glossary` section defines terms for configuratorware.


Index
=====

.. toctree::
   :maxdepth: 2

   integration/index
   frontendgui/index
   admingui/index
   administration/index
   api/index
   media/index
   setup/index
   glossary/index







