########
Tracking
########

******************
Google Tag Manager
******************

Configuratorware has a Google Tag Manager Custom Events Implementation to allow a flexible configuration of any supported web tracking.
After the javaScript snippet from a container has been added to the Code Snippets Section in the adminarea the Configurator will fire Events and push variables to the dataLayer array of GTM.

.. note::
    It is recommended to use the Google Tag Manager tool set like the preview to see how triggers get fired and variables get populated. For more info see: `<https://support.google.com/tagmanager/answer/6107056>`_

Adding Code snippet
===================
The code snippet from a GTM Container can be added in the Admin Area:

.. image:: assets/tracking_tracking_codes.png
    :align: center

Variables and Triggers
======================
The following Event-Triggers and DataLayer-Variables can be used.

.. note::
    For a smooth integration in existing containers, an example export can be used to import this in a GTM container. :download:`download import file <assets/GTM-MWDJXXD_v10.json>`


Variables
---------
The configurator has predefined variables, that get populated by several triggers. Those triggers can be found below including examples of the objects that get pushed to the dataLayer.

==================================================  ============
Name                                                Type
==================================================  ============
file_type                                           string
configuration_code                                  string
image_edit_operation                                string
item_identifier                                     string
removed_design_area_identifier                      string
selected_amounts                                    json
selected_color                                      string
selected_color_amount                               number
selected_component                                  string
selected_design_area_identifier                     string
selected_design_areas                               array
selected_design_production_method_identifier        string
selected_design_production_methods                  array
selected_font_name                                  string
selected_image                                      json
selected_option                                     string
selected_options                                    json
selected_variant_identifier                         string
switch_from                                         string
switch_to                                           string
==================================================  ============


Triggers
--------

accept_design_approve (deprecated)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'accept_design_approve' 
        item_identifier: '343-20303',
        selected_amounts: {343-20303-1: 100},
        selected_design_areas: ['front_center', 'left_center'],
        selected_design_production_methods: ['digital_printing', 'screen_printing'],
        selected_options: [{"application":[{"identifier":"zipper","amount":1}]}]
    }

accept_design_open_dialog (deprecated)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'accept_design_open_dialog' 
    }

accept_configuration_approve
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'accept_configuration_approve' 
        item_identifier: '343-20303',
        selected_amounts: {343-20303-1: 100},
        selected_design_areas: ['front_center', 'left_center'],
        selected_design_production_methods: ['digital_printing', 'screen_printing'],
        selected_options: [{"application":[{"identifier":"zipper","amount":1}]}]
    }

accept_configuration_open_dialog
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'accept_configuration_open_dialog' 
    }

calculation_widget_add_design_area
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'calculation_widget_add_design_area' 
        selected_design_area_identifier: 'front'
    }

calculation_widget_change_amount
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'calculation_widget_change_amount' 
        selected_amounts: {343-20303-1: 100}
    }

calculation_widget_change_design_production_method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'calculation_widget_change_design_production_method' 
        selected_design_production_method_identifier: 'print'
    }


calculation_widget_open_configurator
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. code-block:: javascript

    {
        event: 'calculation_widget_open_configurator' 
        item_identifier: '343-20303',
        selected_amounts: {343-20303-1: 100},
        selected_design_areas: ['front_center', 'left_center'],
        selected_design_production_methods: ['digital_printing', 'screen_printing'],
        selected_options: [{"application":[{"identifier":"zipper","amount":1}]}]
    }

calculation_widget_pdf
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'calculation_widget_pdf' 
    }

calculation_widget_remove_design_area
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_change_variant' 
        removed_design_area_identifier: 'front'
    }

components_select_component
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
         event:"components_select_component",
         selected_component: "application"
    }

components_select_option
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
         event:"components_select_option",
         selected_component: "application",
         selected_option: "zipper"
    }

configuration_mode_switch
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
         event:"configuration_mode_switch",
         switch_from: "creator",
         switch_to: "designer"
    }

edit_design_change_design_area
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_change_variant' 
        selected_design_area_identifier: 'front'
    }

edit_design_change_design_production_method
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_change_variant' 
        selected_design_production_method_identifier: 'screen_printing'
    }

header_change_variant
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_change_variant' 
        selected_variant: '343-20303-1'
    }

header_email
^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_email' 
    }


header_load_configuration_open_dialog
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_load_configuration_open_dialog' 
    }

header_load_configuration_request
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_load_configuration_request' 
        configuration_code: 'XSFFAWWS'
    }

header_pdf
^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_pdf' 
    }

header_save_configuration
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'header_save_configuration' 
    }

image_add
^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_add' 
    }

image_edit
^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_edit' 
    }

image_edit_colorize
^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_edit_colorize' 
    }

image_edit_colorize_select_color_amount
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_edit_colorize_select_color_amount' 
        color_amount: 5
    }

image_edit_colorize_select_color
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_edit_colorize_select_color' 
        selected_color: '85c342'
    }


image_edit_option_usage
^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_edit_option_usage' 
    }

image_edit_remove_background
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_edit_background' 
        image_edit_operation: 'remove background from border'
    }

image_edit_upload_file
^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_edit_upload_file' 
        file_type: 'image/svg+xml'
    }

image_gallery_select_gallery_image
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_gallery_select_gallery_image' 
        selected_image: {
            fileName: 'e566ad769c2c6e94c749074accb7b27a',
            originalFileName: 'company_logo.svg',
            original: {
                url: null,
                format: 'SVG',
                mimeType: 'image/svg+xml',
                width: 116,
                height: 32
            },
            preview: { // see original}
            thumbnail: { // see original}
        }
    }

image_gallery_upload_new
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'image_gallery_upload_new' 
    }

object_controls_align_center_horizontal
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'object_controls_align_center_horizontal' 
    }

object_controls_align_center_vertical
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'object_controls_align_center_vertical' 
    }

object_controls_level_down
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'object_controls_level_down' 
    }

object_controls_level_up
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'object_controls_level_up' 
    }

preview_close
^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'preview_close' 
    }

preview_open
^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'preview_open' 
    }

text_controls_add
^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_add' 
    }

text_controls_align_center
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_align_center' 
    }

text_controls_align_justify
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_align_justify' 
    }

text_controls_align_left
^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_align_left' 
    }

text_controls_align_right
^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_align_right' 
    }

text_controls_bold
^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_bold' 
    }

text_controls_italic
^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_italic' 
    }

text_controls_select_color
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_select_color' 
        selected_color: '85c342'
    }

text_controls_select_font
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: javascript

    {
        event: 'text_controls_select_font' 
        selected_font_name: 'Arial'
    }

