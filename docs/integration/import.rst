######
Import
######

This documents describes the format of the import data and the values that can be configured to setup how the importer handles this data.

*********
Execution
*********

The importer is implemented as a command line application of ``bin/console``. To run it use:

``path/to/project/bin/console configuratorware:import [parameters]``

For a description of parameters see: :ref:`integration_import_parameters`

The importer can also be called with a POST request via the Connector API, see :ref:`api_rest`

********************
Important properties
********************

The following properties control access and visibility of items. Find a complete explanation for all properties: :ref:`integration_import_nodes`

================================  ===============================================
Property                          Possible values
================================  ===============================================
itemIdentifier                    string to identify and open a product: "https://[configuratorURL]/identifier:{itemIdentifier}"
itemStatusIdentifier              "available", "not_available"
visualizationModeIdentifier       "2DLayer", "2dVariant", "3dScene, "3dVariant"
configurationMode                 "creator", "designer", "creator+designer"
================================  ===============================================

********
Examples
********

JSON: Creator
--------------

.. literalinclude:: assets/import_creator.json
    :language: json

JSON: Designer
--------------

.. literalinclude:: assets/import_designer.json
    :language: json

XML: Creator + Designer
-----------------------

.. literalinclude:: assets/import_creator+designer.xml
    :language: xml

***********
Data format
***********

The importer works with json data. It also supports XML. If XML data is provided it is internally converted to json before importing.

.. _integration_import_nodes:

*****
Nodes
*****

Item
----

An item describes an article(sku) or a group of articles.

Item properties
^^^^^^^^^^^^^^^

- itemIdentifier: Unique identifier for the item. As for all identifier fields: Use only lowercase letters, numbers and no special characters except "-", "_" and "." to make sure that it can be used in an URL.
- texts: Titles and descriptions for different languages.
- images: An item can have two different images: thumbnail and detailImage. Images are copied by the import and saved in the configurators directory structure.
- minimumOrderAmount: The minimum order amount for an item. Optional, defaults to
- visualizationModeIdentifier: The visualizationMode for the item. Required to have a visualization.
- configurationMode: In case the license covers only "creator" or "designer", this value needs to be set for each item the same. For licenses for creator+designer this can be used to specify the usage of the item. Possible values are "creator", "designer" or "creator+designer".
- itemStatusIdentifier: Activate the product or disable.
- overwriteComponentOrder: Use item's component order instead of global.
- prices: Bulk prices for different channels, for details see below.
- attributes: A list of attributes for the item, for details see below.
- children: optional. If the children are set the item is treated as an article group, for details see below.
- designAreas: A list of design areas on the item, for details see below.
- components: A list of components on the item, for details see below.
- itemClassifications: Can be used to group items into categories.

Child item properties
^^^^^^^^^^^^^^^^^^^^^

A child item differs in two ways from a root item:
- It cannot have further children (children).
- It needs information about how to group within the root item (childGroups). It contains a list of child groups, see: [child group properties](#child-group-properties).

Child group properties
^^^^^^^^^^^^^^^^^^^^^^

An item can be put in multiple groups within a root item. It gets a value for each group it is put in. For example an item can have a group "color" with the value "red" and the group "size" with the value "s".
From having groups and values for each group an item is in the configurator can build up a hierarchy within the root item.

- group: a group has an identifier and has text entries to have translated titles for different languages
- groupValue: a group value has an identifier and has text entries to have translated titles for different languages

Item prices
^^^^^^^^^^^

Item prices contain a list of bulk prices per channel.

- channelIdentifier: The identifier of the channel the prices should be applied to.
- bulkPrices: A list of prices. The bulks are defined by the itemAmountFrom property. The configurator does no vat calculation net prices can be applied as separate fields.

Item attributes
^^^^^^^^^^^^^^^

An item can have multiple attributes and multiple attribute values per attribute.

- attributeIdentifier: Unique identifier for the attribute.
- type: Data type of the attribute, allowed values are "string", "number" and "boolean".
- texts: Translations for the attribute title
- attributeValues: Attribute values have a "value" and an (optional) list of translations for the value.

Item statuses
^^^^^^^^^^^^^

- configurationModeIdentifier: Configuration mode for that the item should be allowed to be configured. Allowed values are: "creator" and "designer"
- available: Boolean.

Design areas
^^^^^^^^^^^^

Design areas describe areas on an item that can be designed in the configurator. They also contain information of how to visualize them, how they are produced and whats the price for designing them.

- designAreaIdentifier: Unique (within the current item) identifier of the design area.
- texts: Translations for the title of the design area.
- widthMillimeter: The width of the design area in millimeters.
- heightMillimeter: The height of the design area in millimeters.
- visualData: A list of views in which the design area is visible and the positioning within that view, see: [design views](#design-views).
- designProductionMethods: A list of design production methods and the prices for, see: [design production methods](#design-production-methods).
- customData: The custom data object is directly saved serialized in the database.
- visualizationBackgroundColors: A color per itemIdentifier that is used for the visualization of some desing production methods (engraving), see: [visualization background colors](#visualization-background-colors).

Design views
^^^^^^^^^^^^

Design views describe different views on the item. It can be only one view for 3d, or views from different sides, e.g. "front", "back".
Positioning and size in pixels will be translated to relative positions by the importer to be able to scale correctly if displayed in the frontend.

- designViewIdentifier: unique (within the current item) identifier of the design view.
- texts: Translations for the title of the design view.
- mediaURLs: A list of URLs for the visualization of the design view, see: [design view media URLs](#design-view-media-urls).
- position: x and y values in Pixels to Position the design area on the background image (texture of the 3d object if using 3d data).
- widthPixels: If a position is set, the width in pixels is used to display the design area in the correct size on the background image (texture if using 3d data).
- heightPixels: If a position is set, the height in pixels is used to display the design area in the correct size on the background image (texture if using 3d data).
- customData: The custom data object is directly saved serialized in the database.

Visualization background colors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

wil be saved in the additional_data field

- itemIdentifier: Identifier of the item, e.g. different background color per item (red, blue)
- color: hex color of the color to be shown for visualizing some design production methods (engraving)

Design view media URLs
^^^^^^^^^^^^^^^^^^^^^^

For single items (no grouping) there is only one entry, but when in a grouped item it contains an entry for each item within the group.
Images are copied by the import and saved in the configurators directory structure.

- itemIdentifier: Identifier of the item for which the media is applied, e.g. different background image per item (red, blue)
- mediaURL: URL to get the media from, it can be an image file or a zip (e.g. for 3d models)

Design view images
^^^^^^^^^^^^^^^^^^

A .jpg or .png image url depicting each view of the item. Separate image for parent and each child item. Image copied to the directory structure, named by \[parent_item_identifier\]/item_identifier/view_identifier.\[jpg|png\]

Design production methods
^^^^^^^^^^^^^^^^^^^^^^^^^

A list of design production methods that are allowed for the current design area, e.g. offset print, engraving.
Design production methods are not created by the importer the importer just adds the relations to existing design production methods.

- designProductionMethodIdentifier: Identifier of an existing design production method.
- prices: The prices per channel when using this design production method on the current design area.
- widthMillimeter: The size for the design production method on the current design area can be overwritten
- heightMillimeter: The size for the design production method on the current design area can be overwritten
- defaultColors: A design production method may have a defaultColors field, containing the item identifier + color / hex value and color palette identifier. If no matching data found, then this info is skipped.
- default (bool): A default production method and design area can be set per item - if the import contains multiple default values, then the last one will be stared, resetting all other defaults (already stored or in current import)
- allowBulkNames (bool): If bulk names are allowed here

Components
^^^^^^^^^^
- componentIdentifier: Identifier for a new or existing component
- isMultiselect: is multiple option selectable
- sequencenumber: the default priority order of the components
- hiddenInFrontend: defines if the component is hidden in the frontend, defaults to false
- images: thumbnail and detail images to import for a component
- texts: Titles and descriptions for different languages.
- attributes: A list of attributes for the component, see: [item attributes](#item-attributes).
- options: an array of available options for this component
- optionsOrder: an array of available options identifiers for this component that will override the global default priority order of the options

Options
^^^^^^^
- optionIdentifier: Identifier for a new or existing option
- amountIsSelectable: defaults to 1 piece or the amount can be set manually
- sequencenumber: the default priority order of the components
- images: thumbnail and detail images to import for an option
- texts: Titles and descriptions for different languages.
- attributes: A list of attributes for the component, see: [item attributes](#item-attributes).
- prices: The prices for different channels, see: [item prices](#item-prices).
- deltaPrices: Price difference per channels

.. _integration_import_parameters:

**********
Parameters
**********

The behaviour of the import can be configured by setting parameters when calling the command.

.. note::
    By running ``path/to/project/bin/console configuratorware:import -h`` all parameters and default values will be listed.


Source
------

The source of the import can either be set to a directory (read XML or json files) or to a URL that supplies the data.

==============================================================  =======
Parameter                                                       Type
==============================================================  =======
source                                                          string
==============================================================  =======

Delete
------

Different nodes in the data can be defined to be deleted if they do not exist in the current import data set.
If one of these nodes is deleted all data below is also deleted from the database.
The behavior defaults to these settings if they are not set in the commands parameters:

==============================================================  =======
Parameter                                                       Type
==============================================================  =======
deleteNonExistingItems                                          boolean
deleteNonExistingAttributeRelations                             boolean
deleteNonExistingItemPrices                                     boolean
deleteNonExistingDesignAreas                                    boolean
deleteNonExistingDesignAreaVisualData                           boolean
deleteNonExistingDesignAreaDesignProductionMethodPrices         boolean
deleteNonExistingDesignViewThumbnails                           boolean
deleteNonExistingComponentRelations                             boolean
==============================================================  =======

Item Status
-----------

There is a parameter that controls if item statuses are updated from the import data.

==============================================================  =======
Parameter                                                       Type
==============================================================  =======
updateItemStatuses                                              boolean
==============================================================  =======

Price Channel
-------------

The "priceChannel" parameter defines if the import should manipulate prices only in this specified channel, e.g. "_default":

==============================================================  =======
Parameter                                                       Type
==============================================================  =======
priceChannel                                                    string
==============================================================  =======

Dry Run
-------
The import can be called as dry-run

==============================================================  =======
Parameter                                                       Type
==============================================================  =======
dryRun                                                          boolean
==============================================================  =======
