###################
System Requirements
###################

*********************
Hardware Requirements
*********************

==========  ======================================================================================
OS          Debian 12 (bookworm) or other linux distribution that covers the required packages, Debian or Ubuntu are recommended.
RAM         >= 8GB
Disk Space  > 50 GB

            (depends mostly on saved resources like uploaded images and the screenshots that

            are stored for saved configurations. The number of configurations to be

            expected * the image size of the screenshots for a configuration provide a good

            guide value. It can be useful to archive or delete old screenshots from time to time.)
==========  ======================================================================================

**********************
Webserver and Packages
**********************

Users & Rights
==============

* php & php cli needs to write/read to systems tmp folder

Folders & Files
===============

* all files need to be readable by the php web executing user (usually www-data)
* the following folders (+ subfolders) need to be writable by the php web executing user:

    * app/public/client
    * app/public/designer
    * app/public/images
    * app/public/img
    * app/public/visualization_component
    * app/var

Tmp folder
==========
Configuratorware makes use of saving files to /tmp, so consider to setup you server that it deletes them automatically after certain amount of time to save
Disc Space according to your needs.

Symfony Cache
=============
Configuratorware relies on the symfony cache. To set up a server to work properly with it, check the `Symfony Documentation`_.

.. _Symfony Documentation: https://symfony.com/doc/current/setup/file_permissions.html

Web Document Root
=================
The web document root needs to point to `app/public`

Apache
======

* mod_rewrite
* mod_headers
* .htaccess lookup needs to be enabled for everything under `app/public`

Mailing
=======

For sending offer request mails, configuratorware uses the default the `Symfony mailing stack`_.
If you're using the offer request feature, smtp mailing needs to be available on the server.

.. _Symfony mailing stack: https://symfony.com/doc/current/mailer.html

Libraries
=========

* ImageMagick (dependency of php-imagick, ^v6.9)


PHP
===

* Version 8.3

Although configuratorware still supports older PHP versions, it is recommended to directly use version 8.3.

PHP ini settings
----------------

==================  =======
max_execution_time  300
memory_limit        128M
==================  =======

PHP Modules
-----------

* dom
* exif
* gd
* intl
* imagick
* json
* libxml
* mbstring
* pdo, pdo_mysql
* simpleXML
* zip


PHP Opcache
===========

Opcache enabled. Recommended settings:

* opcache.validate_timestamps=0
* opcache.memory_consumption=256
* opcache.max_accelerated_files=20000

Database
========

You can use MariaDB or MySQL

========    ============
MariaDB     10.2 - 10.7
MySQL       5.7 or 8.0
========    ============

By default configuratorware is configured for MySQL 5.7. For different platforms specify the version ``?serverVersion=`` in the DATABASE_URL environment variable according to `Symfony Database Configuration`_.

.. _Symfony Database Configuration: https://symfony.com/doc/current/doctrine.html#configuring-the-database




************************************
Additional Requirements for designer
************************************

Libraries
=========

The following libraries are necessary, if you have a designer or creator+designer license:

* inkscape (>= v.0.92.4)
* ghostscript (v9.27)
* potrace (v1.15)

Support of vectorization and convert PDFs
=====================================
To support PDF conversion, the default ImageMagick policies need to be changed. Do this at your own risk!

An option is to run the following command on the system, that will change the policies in /etc/ImageMagick-6/policy.xml:
`sed -i '/disable ghostscript format types/,+6d' /etc/ImageMagick-6/policy.xml`

To support uploaded user images in vector formats, you need to add the following entries manually to the `/etc/ImageMagick-6/policy.xml` file:

`<policy domain="coder" rights="read|write" pattern="PDF" />`

`<policy domain="coder" rights="read|write" pattern="XPS" />`

Workaround for inkscape in version 1.2.2
========================================
Due to the issue described in https://gitlab.com/inkscape/inkscape/-/issues/3524 a workaround is necessary for servers running the designer with inkscape version 1.2.2.
The content of `/usr/share/inkscape/extensions/eps_input.inx` has to be updated. replace

.. code-block:: xml

        <param name="autorotate" type="optiongroup" appearance="combo" gui-text="Determine page orientation from text direction"
        gui-description="The PS/EPS importer can try to determine the page orientation such that the majority of the text runs left-to-right.">
            <option value="None">Disabled</option>
            <option value="PageByPage">Page by page</option>
            <option value="All">All</option>
        </param>

with

.. code-block:: xml

    <param name="autorotate" type="string" gui-hidden="true">None</param>


**********************************
Additional Requirements for finder
**********************************

To prevent / fix errors when uploading finder .json files in the adminarea, you need to increase the max allowed package size in your database settings.
For mysql, add this to your `my.ini` or `~/.my.cnf` in the [mysqld] or [client] section and restart your mysql server:

`max_allowed_packet=256M`