####################
Server Configuration
####################

This part of the documentation contains tips of examples when preparing infrastructure according to the configuratorware system requirements.
It addresses System Administrators that know their infrastructure well and can decide whether to apply the given tips on their specific systems and
how to modify the given examples properly.

*********
Webserver
*********

Apache
======
Configuratorware ships with an example ``.htaccess`` that can be adjusted and used for routing the resources.
If you don't want to use .htaccess, configure apache accordingly.

Nginx
=====
It is possible to use nginx to serve configuratorware.
It will require to manage the routing for the app completely independent from the application as nginx doesn't have an equivilant to ``.htaccess``
Find below an example configuration for nginx:

.. code-block::

    server {
        listen 8080;
        root /app/public;
        index index.php index.htm index.html;
        client_max_body_size 20m;

        location ~ ^/(dynamic_file|media|frontendapi|api)(/|$) {
            try_files $uri /index.php$is_args$args;
        }

        location ~ ^/index\.php(/|$) {
            fastcgi_pass php;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            internal;
        }

        location ~ \.php$ {
            return 404;
        }

        location ~ admin((\/$)|$) {
            try_files $uri /admin/index.html$is_args$args;
        }

        location / {
            try_files $uri /index.html$is_args$args;
        }
    }

Htaccess
========

Browser Caching
---------------

To disallow browser caching for certain files, you can adjust and add the following to ``.htaccess``:

.. code-block::

    <If "%{REQUEST_URI} =~ m#^/translations_cached/.*\.json#">
      FileETag None
      <ifModule mod_headers.c>
         Header unset ETag
         Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
         Header set Pragma "no-cache"
         Header set Expires "Thu, 1 Jan 1970 00:00:00 GMT"
      </ifModule>
    </If>

********
Packages
********

ImageMagick
===========
ImageMagick is a common library that has a php extension.
A lot of hosting providers have the possiblity to enable the php-ext as well as the CLI tool via their administration-interfaces.


Inkscape
========
Inkscape is usually not installable via an administration-interface or CLI. Ask the hosting provider support, if they can install it in the required version.


Ghostscript
===========
Ghostscript is a required dependency for Inkscape. So usually no separate install is required.


Potrace
=======
Potrace might be installed as an dependency with inkscape. If it is not installed after inkscape was added, check if it can be installed via CLI.
For example on Hetzner V-Server it is possible to install via:  ``software install potrace``


Weasyprint
==========
Weasyprint was replace with PHP library DomPDF in configuratorware version 1.25 and is included automatically via composer.
If you still need to use Weasyprint, you can install and use it manually (see upgrade notes for `Version 1.25 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.25.md>`_ and `Version 1.35 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.35.md>`_):

Weasyprint needs pip (a common python package manager) for installation. Pip is available through venv in many infrastructures (e.g. Hetzner V-Server):

.. code-block:: sh

    python3 -m venv ./venv
    . ./venv/bin/activate

Alternatively Pip can be installed manually:

.. code-block:: sh

    curl -O https://bootstrap.pypa.io/get-pip.py
    python3 get-pip.py --user

If pip or pip3 is available, weasyprint can be installed:

.. code-block:: sh

    pip install weasyprint  #or pip3 install weasyprint


PATH environment variable
=========================
After installation of packages it is necessary that PHP knows about all folders that contain executables. Therefore configuratorware relies on the `PATH`
environment variable. In managed hosting scenarios there are several ways to do that:


Prepend script with putenv()
----------------------------
Set a prepend script via ``php.ini``: ``auto_prepend_file = /home/your-user/bin/prepend.php``

.. code-block:: php

    <?php

    putenv("PATH=/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin");

Be aware that if PHP is installed via SAPI, environment variables set by ``putenv()`` are not available to the local process.


.htaccess
---------
Set the environment variables via .htaccess, this can be done like that:

.. code-block:: sh

    SetEnv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/home/your-user/.linuxbrew/bin:/usr/home/your-user/venv/bin

Be aware that if PHP is installed via SAPI, a ``REMOTE_`` is added to the environment variable set by .htaccess.


Prepend script modifies global constants
----------------------------------------
If there is no different possibility to set the environment variable, there is a suboptimal solution to modify php's ``$_SERVER`` and ``$_ENV`` global:

.. code-block:: php

    <?php

    $_SERVER['PATH'] = '/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin';

    $_ENV['PATH'] = '/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin';
