############################################
Communication with the embedded configurator
############################################

The embedded configurator uses the `postMessage` api to notify the parent window about various events,
like "add to cart" or "configuration saved".

##################
Receiving Messages
##################

The container page can listen to these events, so it can react to them. This can be done in the following way:

.. code-block:: html

    <!-- listening to message events -->
    <script>
            window.addEventListener('message', (event) => {
            const data = event.data;
            console.log('message posted from the embedded page', data);

            const method = data.method;
            const code = data.code;
            const save_id = data.save_id;

            switch (method) {
                case 'configurationsaved': {
                    console.log('the user clicked the save button in the top right corner, and received the following code: ' + code);
                    // do other actions here
                    break;
                }
                case 'addtocart': {
                    console.log('the user clicked the "add to cart" button in the bottom right corner, and received the following code: ' + code);
                    // do other actions here
                    break;
                }
                case 'configurationsaved_remotely': {
                    console.log('the configuration was saved remotely and received the following code: ' + code + ', the save_id was ' + save_id);
                    // do other actions here
                    break;
                }
                case 'requestoffer_form_sent': {
                    console.log('the request offer form was sent for the configuration code: ' + code);
                    // do other actions here
                    break;
                }
                default: {
                    console.log('unhandled or unknown method: ' + method);
                }
            }
        });
    </script>

    <!-- embedding the configurator -->
    <iframe src="/identifier:softshell_creator_2d" id="configurator_page" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>

With the configuration code from the message event, the surrounding page can retrieve the detail information for the saved configuration and the calculation.

.. code-block:: html

    https://[configuratorURL]/frontendapi/configuration/loadconfigurationinfo/{code}

The call returns a json object with all relevant information to the configuration, e.g. selected options, selected designareas, and the calculated price. The price is calculated as soon as you load this call, so you can update the price e.g. from your cart. JSON Example:

.. code-block:: json

    {
      "code": "XYZ123",
      "item": "Item Name",
      "itemIdentifier": "123456",
      "calculation": {
        "total": 519,
        "totalFormatted": "519.00 €",
        "netTotal": 436.13,
        "netTotalFormatted": "436.13 €",
      },
      "optionclassifications": [
        {
          "identifier": "component_identifier",
          "title": "Component Title",
          "selectedoptions": [
            {
              "identifier": "option_identifier",
              "title": "Option Title",
              "amount": 1
            }
          ]
        },
      ],
      "images": [
        "//urltoimage1"
      ],
      "customdata": {},
      "date_created": {
        "date": "2037-07-13 17:32:41.000000",
        "timezone_type": 3,
        "timezone": "Europe/Paris"
      }
    }

.. note::

    More information on the `postMessage` api: https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage

***********
Add to cart
***********

If the user selects to add a configuration to the cart, in the configurator a message is sent to the parent window.

.. code-block:: javascript

    window.parent.postMessage({ method: 'addtocart', code: 'XYZ123' }, targetOrigin);


******************
Save configuration
******************

After the user saves a configuration, the following message is sent to the parent window:

.. code-block:: javascript

    window.parent.postMessage({ method: 'configurationsaved_remotely', code: 'XYZ123' }, targetOrigin);

.. _saved_configuration:

****************************
Configuration saved remotely
****************************

After a configuration was saved remotely, the following message is sent to the parent window:

.. code-block:: javascript

    window.parent.postMessage({ method: 'configurationsaved', code: 'XYZ123', save_id: 'a_random_id_123' }, targetOrigin);

The save_id equals the value that you passed in - see :ref:`saving_configuration`.

***********************
Request offer form sent
***********************

After the user sends the request offer form, the following message is sent to the parent window:

.. code-block:: javascript

    window.parent.postMessage({ method: 'requestoffer_form_sent', code: 'XYZ123' }, targetOrigin);

################
Sending Messages
################

**************
Switch options
**************

To switch options for a loaded creator item, you can *send* a postMessage with the components / options to select as JSON payload to the embedded configurator:

.. code-block:: javascript

    document.getElementById('configurator_page').contentWindow.postMessage({type: 'switch_options', options: '[{"identifier":"component_identifier1","selectedoptions":[{"identifier":"option_identifier_1","amount":2},{"identifier":"option_identifier_2"}]},{"identifier":"component_identifier2","selectedoptions":[{"identifier":"option_identifier_3"}]}]'});

If you don't provide an amount for an option, it defaults to 1.

.. _saving_configuration:

**********************
Saving a configuration
**********************

To save the current configuration, you can *send* a postMessage with an optional save_id to identify the async response (see :ref:`saved_configuration`) as payload to the embedded configurator:

.. code-block:: javascript

    document.getElementById('configurator_page').contentWindow.postMessage({type: 'switch_options', save_id: 'a_random_id_123', save_type: 'cart'});

The save_type parameter can be set to either 'cart' or 'user' and defaults to 'user' if it's not provided.

####################################
Editing configurations from the cart
####################################

If you want to let the users edit their configurations that are already added to the cart you can open the saved configuration with the parameter ``_frombasket``.

.. code-block:: html

    <iframe src="https://[configuratorURL]/code:{code}?_frombasket=1" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>

If the user then chooses to add the configuration to the cart again it will be overwritten. The Post Message then contains the same code for the configuration as before.
By that code you can then update your position in the cart.
