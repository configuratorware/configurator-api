########################
Opening the Configurator
########################

The configurator usually gets integrated as an iframe in the corresponding shop/homepage.
The height and width of the iframe can be set to fit the surrounding page. The configurator will behave responsively.

***************
Opening an Item
***************

An item can be opened by its identifier.

.. code-block:: html

    <iframe src="https://[configuratorURL]/identifier:{identifier}" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>

***********************
Loading a Configuration
***********************

A configuration can be opened by its code.

.. code-block:: html

    <iframe src="https://[configuratorURL]/code:{code}" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>

******************************
Opening the Calculation Widget
******************************

The widget can be opened for an item by its identifier

.. code-block:: html

    <iframe src="https://[configuratorURL]/widget/identifier:{identifier}" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>


.. _integration_opening_paramteres:

**********
Parameters
**********

+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| Name                  | Value                                                                     | Description                                |
+=======================+===========================================================================+============================================+
| _channel              | distribution channel, e.g. de_eur                                         | sets the pool of prices to be used         |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _cta_element          | addToCart, requestOffer, none                                             | defines the behavior of the call to action |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _client               | client, e.g. myshop1, myshop2                                             | sets the client colors, contact data, etc  |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _designerPreSelection | json object, see below                                                    | pre selections for the designer            |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _display_net_prices   | 1                                                                         | all prices are output in their net version |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _frombasket           | 1                                                                         | if this parameter is set the configurator  |
|                       |                                                                           |                                            |
|                       |                                                                           | overwrites the configuration when adding it|
|                       |                                                                           |                                            |
|                       |                                                                           | to the basket to enable editing of         |
|                       |                                                                           |                                            |
|                       |                                                                           | configurations that already where put into |
|                       |                                                                           |                                            |
|                       |                                                                           | the basket                                 |
|                       |                                                                           |                                            |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _language             | language an area code ISO 3166, e.g. en_GB, de_DE                         | sets the language of the configurator      |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _hide_prices          | 1                                                                         | overrides the channel settings to show or  |
|                       |                                                                           |                                            |
|                       |                                                                           | hide prices                                |
|                       |                                                                           |                                            |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _share_url            | url encoded string containing a "{code}" placeholder,                     | overrides the share url that is used when  |
|                       |                                                                           |                                            |
|                       | e.g. http%3A%2F%2Fwww.myshop.de%2F%3Fmy_code                              | sharing a configuration link as well as on |
|                       |                                                                           |                                            |
|                       | %3D%7Bcode%7D%26my_param%3D2                                              | the cover of the configuration pdf         |
|                       |                                                                           |                                            |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+
| _switch_options       | [{"identifier":"component_identifier1","selectedoptions":                 | preselects the provided options for        |
|                       |                                                                           |                                            |
|                       | [{"identifier":"option_identifier_1","amount":2,                          | components                                 |
|                       | "inputText":"input text value"}]}]                                        |                                            |
+-----------------------+---------------------------------------------------------------------------+--------------------------------------------+

For boolean values always use 0 and 1, don't use true/false strings.

Designer Pre Selections
=======================

Design area selections and selected amounts can be preselected when opening the configurator.

.. code-block:: json

    {
      "designAreas": [
        {
          "identifier": "front_left",
          "productionMethod": "digital_print",
          "colorAmount": 5
        },
        {
          "identifier": "back_top",
          "productionMethod": "embroidery",
          "colorAmount": 2
        }
      ],
      "itemAmounts": {
        "hoodie_red_s": 10,
        "hoodie_blue_m": 50
      }
    }
