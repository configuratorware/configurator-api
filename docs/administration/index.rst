.. _administration:

##############
Administration
##############


************
Translations
************
The core project of the frontend gui provides a base set of translations (de and en). During installing the package, those translations are copied to the public folder of the frontendgui part of the client project.

In the frontendgui project you can override and add new translations by adding new json files to a directory translations_custom:

.. code-block::

    index.html
    bundle.js
    ...
    +--- translations
    |    +--- de_DE.json
    |    \--- en_GB.json
    \---translations_custom
         +--- de_DE.json
         \--- en_GB.json

In development mode, the fromtendgui will load both files and merge them.

When builing the frontendgui for deployment, both translation folders will be copied to the public folder of the api part of the project. The symfony command `./bin/console configuratorware:create-cached-translation` will merge them and create cached translations out of them. In production mode, the configurator only loads the cached translations, to not do additional calls and merging in runtime.

The translation files can be updated in the admin area. The command that builds the cached translations keeps the changes made over the admin area. So if new translations are deployed, the changes made in the admin area are not lost.

*******
Clients
*******
Clients are used to be able to load the configurator with different CI settings (highlight color, font).
They also can have differing contact data used for generating PDFs and retrieving offers.

One client can have multiple channels.
Usually, clients are used to embed the configurator in different subshop systems.

Note that the availability of this feature depends on your license (see `pricing`_).

.. _pricing: https://configuratorware.com/pricing

********
Channels
********
A channel represents a pool of prices, e.g. de_eur, de_at, ...

Both channel and client can be selected by parameters when opening the configurator, see: :ref:`integration_opening_paramteres`.


*************************
Users, Roles, Credentials
*************************
Users and roles can be created in the admin area to give admin area users access to the different parts of the administration.
The credentials are delivered with the configuratorware api bundle. Additional credentials for new areas created in client projects can be added to the table credential.

When a user logs in to the admin area, the menu entries are filtered by the users credentials.

If you want to add new protected routes for the admin area you can add the needed credential in the route definition:
Example:

.. code-block:: yaml

  my_new_entity_list:
      path:     /api/my_entity{trailingSlash}
      defaults: { _controller: AppBundle\MyEntityController:list, trailingSlash: "/", __api_credential: 'myentity' }
      methods:  [GET]
      requirements: { trailingSlash: "[/]{0,1}"

********
API Keys
********
API keys are used to connect to API calls in the connector space (see `link`_). The API key can be created and revoked in the admin area.

The connector is used for shop systems to import their products to the confiugrator and to get information about the configurability of items in the configurator.

.. _link: /docs/_static/api/index.html

*******
Setting
*******
The setting table is used to hold basic settings for the configurator. The installer will ask for them during setup.

If additional settings are needed in a client project, they can be added in the customsettings field.
