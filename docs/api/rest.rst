.. _api_rest:

#########
Rest APIs
#########

To open the Rest API docs in a new tab you can use this `link`_.

.. _link: /docs/_static/api/index.html

.. raw:: html

    <iframe src="/docs/_static/api/index.html" height="800px" width="100%" frameborder="0"></iframe>
