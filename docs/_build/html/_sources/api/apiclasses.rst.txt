
###########
API classes
###########


**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ApiVersion\\ApiVersionApi
**********************************************************************

getVersionInfo
**************


Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ApiVersion



****************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\AssemblyPoint\\AssemblyPointApi
****************************************************************************

getByOption
***********

Parameters
==========

* mixed : $optionId

getOptionsList
**************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Option : $option

getSampleOptions
****************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SampleOptionListRequestArguments : $arguments



********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Attribute\\AttributeApi
********************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Attribute

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Attribute : $attribute

delete
******

Parameters
==========

* mixed : $id



******************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\AttributeValue\\AttributeValueApi
******************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Attributevalue

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Attributevalue : $attributeValue

delete
******

Parameters
==========

* mixed : $id



************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Calculation\\CalculationApi
************************************************************************

calculate
*********

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\Configuration : $configuration
* bool : $calculateBulkSavings



****************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Channel\\ChannelApi
****************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Channel

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Channel : $channel

delete
******

Parameters
==========

* mixed : $id



**************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Client\\ClientApi
**************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Client

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Client : $client

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Client

delete
******

Parameters
==========

* mixed : $id

frontendClient
**************

Parameters
==========

* string : $clientIdentifier
* string : $channelIdentifier

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\Client

uploadFont
**********

Parameters
==========

* int : $id
* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\FontUploadArguments : $fontUpload

Return Value
============
bool

uploadLogo
**********

Parameters
==========

* int : $id
* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ClientLogoUploadArguments : $fontUpload

Return Value
============
void



************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\CodeSnippet\\CodeSnippetApi
************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\CodeSnippet

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\CodeSnippet : $codeSnippet

delete
******

Parameters
==========

* mixed : $id

findCurrent
***********




**************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ColorPalette\\ColorPaletteApi
**************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ColorPalette

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\ColorPalette : $colorPalette

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ColorPalette

delete
******

Parameters
==========

* mixed : $id

Return Value
============
void



****************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Configuration\\ConfigurationApi
****************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Configuration

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Configuration : $configuration

delete
******

Parameters
==========

* mixed : $id

frontendSave
************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\ConfigurationSaveData : $configurationSaveData

getPrintFileListResults
***********************

Parameters
==========

* string : $configurationCode

Return Value
============
array

uploadPrintData
***************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\ConfigurationPrintFileSaveData : $configurationPrintFileSaveData

loadByItemIdentifier
********************

Parameters
==========

* string : $itemIdentifier

loadByConfigurationCode
***********************

Parameters
==========

* string : $configurationCode

switchOption
************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\ConfigurationSwitchOptionData : $configurationSwitchOptionData

validateConfiguration
*********************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\Configuration : $configuration

generatePdf
***********

Parameters
==========

* string : $documentType
* string : $configurationCode

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\FileResult

generateHtml
************

Parameters
==========

* string : $documentType
* string : $configurationCode

Return Value
============
string

generateZip
***********

Parameters
==========

* string : $configurationCode

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\FileResult



********************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ConfigurationDocument\\ConfigurationDocumentApi
********************************************************************************************

generatePdf
***********

Parameters
==========

* string : $documentType
* string : $configurationCode

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\FileResult

generateHtml
************

Parameters
==========

* string : $documentType
* string : $configurationCode

Return Value
============
string



************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ConfigurationInfo\\ConfigurationInfoApi
************************************************************************************

getConfigurationInfo
********************

Parameters
==========

* string : $configurationCode

getProductionInfo
*****************

Parameters
==========

* string : $configurationCode



******************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ConfigurationVariant\\ConfigurationVariantApi
******************************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ConfigurationVariant

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\ConfigurationVariant : $configurationVariant

delete
******

Parameters
==========

* mixed : $id

findItems
*********

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getFrontendConfigurationVariants
********************************

Parameters
==========

* mixed : $itemIdentifier



****************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ConstructionPattern\\ConstructionPatternApi
****************************************************************************************

getOne
******

Parameters
==========

* int : $itemId

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ConstructionPattern

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\ConstructionPattern : $constructionPattern



************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\CreatorItem\\CreatorItemApi
************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult



**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Credential\\CredentialApi
**********************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult



******************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Currency\\CurrencyApi
******************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Currency

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Currency : $currency

delete
******

Parameters
==========

* mixed : $id



********************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DataTransfer\\DesignDataTransferApi
********************************************************************************

transfer
********

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignDataTransfer : $designDataTransferStructure



**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DesignArea\\DesignAreaApi
**********************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignArea

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignArea : $structure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignArea

delete
******

Parameters
==========

* string : $id

Return Value
============
void

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments

Return Value
============
void

uploadMask
**********

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\DesignAreaMaskArguments : $arguments

Return Value
============
bool

uploadDesignProductionMethodMask
********************************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\DesignAreaMaskArguments : $arguments

Return Value
============
bool



**********************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DesignProductionMethod\\DesignProductionMethodApi
**********************************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignProductionMethod

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignProductionMethod : $structure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignProductionMethod

delete
******

Parameters
==========

* mixed : $id

Return Value
============
void



******************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DesignTemplate\\DesignTemplateApi
******************************************************************************

getFrontendList
***************

Parameters
==========

* string : $itemIdentifier



**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DesignView\\DesignViewApi
**********************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignView

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignView : $structure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignView

delete
******

Parameters
==========

* mixed : $id

Return Value
============
void

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments

Return Value
============
void



**********************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DesignerFallback\\DesignerFallbackApi
**********************************************************************************

getCheckedItemSetting
*********************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\Item : $item

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ItemSettingRelation



************************************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DesignerGlobalCalculationType\\DesignerGlobalCalculationTypeApi
************************************************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignerGlobalCalculationType

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignerGlobalCalculationType : $structure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignerGlobalCalculationType

delete
******

Parameters
==========

* string : $ids

Return Value
============
void



************************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\DesignerGlobalItemPrice\\DesignerGlobalItemPriceApi
************************************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignerGlobalItemPriceItem

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignerGlobalItemPriceItem : $structure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\DesignerGlobalItemPriceItem



**********************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Font\\FontApi
**********************************************************

getFrontendDesignerList
***********************


Return Value
============
array

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Font

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Font : $structure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Font

upload
******

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\FontUploadArguments : $fontUpload

Return Value
============
bool

delete
******

Parameters
==========

* mixed : $id

Return Value
============
void



************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Group\\GroupApi
************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Group

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Group : $group

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Group

delete
******

Parameters
==========

* mixed : $id

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments



**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\GroupEntry\\GroupEntryApi
**********************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\GroupEntry

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\GroupEntry : $groupEntry

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\GroupEntry

delete
******

Parameters
==========

* mixed : $id

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments



************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ImageGalleryImage\\ImageGalleryImageApi
************************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ImageGalleryImage

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\ImageGalleryImage : $imageGalleryImage

delete
******

Parameters
==========

* mixed : $id

upload
******

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ImageGalleryImageUploadData : $imageGalleryImageUploadData

frontendList
************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ImageGalleryImageFrontendList : $imageGalleryImageFrontendList

frontendListTags
****************


saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments

Return Value
============
void



**************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Import\\ImportApi
**************************************************************

readFromSource
**************

Parameters
==========

* string : $source
* Redhotmagma\\ConfiguratorApiBundle\\Models\\ImportOptions : $importOptions

prepareImportArray
******************

Parameters
==========

* string : $raw

Return Value
============
array

executeImport
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Models\\ImportOptions : $options
* array : $importArray

Return Value
============
bool

removeOldImportFiles
********************




************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Inspiration\\InspirationApi
************************************************************************

getFrontendList
***************

Parameters
==========

* string : $itemIdentifier



**********************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Item\\ItemApi
**********************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Item

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Item : $item

delete
******

Parameters
==========

* mixed : $id

getConfigurableItemsByParent
****************************

Parameters
==========

* mixed : $parentIdentifier



**************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ItemClassification\\ItemClassificationApi
**************************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ItemClassification

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\ItemClassification : $itemClassification

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments

delete
******

Parameters
==========

* mixed : $id



****************************************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ItemConfigurationModeItemStatus\\ItemConfigurationModeItemStatusApi
****************************************************************************************************************

getOne
******

Parameters
==========

* int : $itemId
* string : $configurationModeIdentifier

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ItemConfigurationModeItemStatus

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\ItemConfigurationModeItemStatus : $structure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\ItemConfigurationModeItemStatus



******************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ItemItemStatus\\ItemItemStatusApi
******************************************************************************

getConnectorListResult
**********************

Parameters
==========

* string : $itemIdentifiers

Return Value
============
array



************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ItemSetting\\ItemSettingApi
************************************************************************

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\ItemSetting : $structure

Return Value
============
bool



**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ItemStatus\\ItemStatusApi
**********************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult



******************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Language\\LanguageApi
******************************************************************

getFrontendList
***************


Return Value
============
array



************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Media\\MediaApi
************************************************************

getMedia
********

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\MediaArguments : $mediaArguments



**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Option\\FrontendOptionApi
**********************************************************************

getList
*******

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\Configuration : $configuration
* mixed : $itemIdentifier
* mixed : $optionClassificationIdentifier
* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\OptionFilterArguments : $optionFilters

getDetail
*********

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\Configuration : $configuration
* mixed : $optionIdentifier

getMatchingOptions
******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\Configuration : $configuration
* mixed : $optionClassificationIdentifier



**************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Option\\OptionApi
**************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Option

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Option : $option

delete
******

Parameters
==========

* mixed : $id

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments



******************************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\OptionClassification\\OptionClassificationApi
******************************************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\OptionClassification

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\OptionClassification : $optionClassification

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments

delete
******

Parameters
==========

* mixed : $id



**********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\OptionPool\\OptionPoolApi
**********************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\OptionPool

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\OptionPool : $optionPool

delete
******

Parameters
==========

* mixed : $id



**************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\ReceiveOffer\\ReceiveOfferApi
**************************************************************************

request
*******

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\ReceiveOfferRequest : $structure

sendRequestMail
***************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\ReceiveOfferRequest : $structure

Return Value
============
bool



**********************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Role\\RoleApi
**********************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Role

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Role : $role

delete
******

Parameters
==========

* mixed : $id



**********************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Rule\\RuleApi
**********************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Rule

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Rule : $rule

delete
******

Parameters
==========

* mixed : $id



******************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\RuleType\\RuleTypeApi
******************************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult



****************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Setting\\SettingApi
****************************************************************

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Setting

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Setting : $structure

Return Value
============
bool

frontendGetMaxZoom2d
********************


Return Value
============
float



********************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\Tag\\TagApi
********************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Tag

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Tag : $tag

delete
******

Parameters
==========

* mixed : $id

saveSequenceNumbers
*******************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\SequenceNumberArguments : $sequenceNumberArguments

Return Value
============
void



**********************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\User\\UserApi
**********************************************************

getListResult
*************

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\RequestArguments\\ListRequestArguments : $arguments

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Internal\\PaginationResult

getOne
******

Parameters
==========

* int : $id

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\User

save
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\User : $user

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\User

saveApiuser
***********

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\User : $user

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\User

delete
******

Parameters
==========

* mixed : $id

Return Value
============
void

refreshKey
**********

Parameters
==========

* int : $id



****************************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\UserImage\\DesignerUserImageApi
****************************************************************************

edit
****

Parameters
==========

* Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\DesignerUserImageRequest : $designerUserImageRequestStructure

Return Value
============
Redhotmagma\\ConfiguratorApiBundle\\Structure\\Frontend\\DesignerUserImageResponse



********************************************************************
Redhotmagma\\ConfiguratorApiBundle\\Service\\UserImage\\UserImageApi
********************************************************************

upload
******

Parameters
==========

* Symfony\\Component\\HttpFoundation\\File\\UploadedFile : $imageFormData

