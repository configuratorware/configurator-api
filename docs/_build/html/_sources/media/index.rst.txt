.. _media:

Media
=====

====
Item
====
The item image & thumbnail are used for different purposes like generating the PDF or in the add-to-cart or receive-offer-request dialog. They need to be stored no matter if 3D or 2D is used. If groups are in use, the itemIdentifier is the identifier of the groupItem.

Item-Image
##########

The files need to be named after the following structure:
``/images/item/image/[itemIdentifier].jpg``


Item-Thumbnail
##############

The files need to be named after the following structure:
``/images/item/thumb/[itemIdentifier].jpg``


=======
2D & 3D
=======

.. toctree::
   :maxdepth: 2

   2D
   3D
