.. _3D:

==
3D
==

3D Data is stored in the public exposed directory (/public or /web) in directory ``/visualization_component``.
Depending on the configuration mode (creator or designer) you are using the data is put in the corresponding directory.
Within that each item has a directory with its identifier (the identifier of the parent item).

The 3d data consists of three types of files.

- models: the gltf format is used for model files.
  Optionally, draco encoded gltf files are supported. Compressing gltf files with draco encoder drastically reduces the files size. Encode your files when exporting them from e.g. Blender or use the `glTF Pipeline <https://github.com/CesiumGS/gltf-pipeline>`_ tool , if your 3D Modelling Software does not support draco encoding.

- textures: textures can be png or jpg

- params: json files with settings for the model they are applied to. the following settings are supported:


.. code-block:: php

    - texture maps (value should be an image src)
    -- alphaMap
    -- aoMap
    -- emissiveMap
    -- lightMap
    -- map (the main texture)
    -- metalnessMap
    -- normalMap
    -- roughnessMap
    - numeric params (value should be a float number between 0 ... 1)
    -- aoMapIntensity
    -- emissiveIntensity
    -- envMapIntensity
    -- lightMapIntensity
    -- metalness
    -- opacity
    -- refractionRatio
    -- roughness
    - color params
    -- color (custom color)
    -- emissive

For more information on the settings see: https://threejs.org/docs/api/en/materials/MeshBasicMaterial.html

Files deeper in the tree override the corresponding files on a higher level. Params are merged together.

*******
Creator
*******

General models that are independent from the selected options are put into the directory ``basemodel``.
Textures based on selected options (named by the option identifier) are put into ``optiontextures``. Textures can be overridden per component / option.

In the following example a base model that contains the parts that are not changed during configuration is found in the ``basemodel`` directory.
For both components (``body`` and ``sleeve``) a separate model is found.
Options textures are stored globaly in ``optiontextures``, but if green is selected for the body color the texture is overridden and an alpha map is applied.

.. code-block:: php

    \--- visualization_component
        \--- creator
            \--- shirt_creator_3d
                +--- basemodel
                |    +--- base.gltf
                |    +--- base.json
                |    +--- scenedata.gltf
                |    +--- scenedata.json
                |    \--- base.png
                \--- components
                    \--- body
                        +--- body.gltf
                        |    \--- options
                        |        \--- green
                        |            +--- optiontextures
                        |            |    +--- alpha.png
                        |            |    \--- green.png
                        |            \--- params.json => {"alphaMap":"optiontextures/alpha.png"}
                        \--- sleeve
                            \--- sleeve.gltf
                                \--- optiontextures
                                    +--- green.png
                                    |--- red.png
                                    \--- blue.png

********
Designer
********

Models, textures and settings are grouped together by their basename.
More specific files in deeper directories override the corresponding files of a higher level.
The overriding is based on the item groups of the item (in a lot of cases this will be the color).

In the following example the pen consists of a shaft and a clip.
Based on the variant a different texture is set for the shaft.
For the black variant a different texture is loaded for the clip and a metalness map is added.

.. code-block:: php

    \--- visualization_component
        \--- designer
            \--- pen_designer_3d
                \--- color
                    +--- red
                    |   \--- shaft.png
                    |--- black
                    |   +--- shaft.png
                    |   |--- clip.json => {"metalnessMap":"metalness.png"}
                    |   \--- metalness.png
                    |--- shaft.gltf
                    |---- clip.gltf
                    \---- clip.png
