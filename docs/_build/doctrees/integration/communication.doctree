���U      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]�(�docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�,Communication with the embedded configurator�h]�h	�Text����,Communication with the embedded configurator�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�m/builds/redhotmagma/configuratorware/configuratorware-dev/configurator-api/docs/integration/communication.rst�hKubh	�	paragraph���)��}�(h��The embedded configurator uses the `postMessage` api to notify the parent window about various events,
like "add to cart" or "configuration saved".�h]�(h�#The embedded configurator uses the �����}�(hh/hhhNhNubh	�title_reference���)��}�(h�`postMessage`�h]�h�postMessage�����}�(hh9hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh/ubh�k api to notify the parent window about various events,
like “add to cart” or “configuration saved”.�����}�(hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubeh}�(h!]��,communication-with-the-embedded-configurator�ah#]�h%]��,communication with the embedded configurator�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Receiving Messages�h]�h�Receiving Messages�����}�(hh\hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhYhhhh,hK
ubh.)��}�(h�nThe container page can listen to these events, so it can react to them. This can be done in the following way:�h]�h�nThe container page can listen to these events, so it can react to them. This can be done in the following way:�����}�(hhjhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhYhhubh	�literal_block���)��}�(hX}  <!-- listening to message events -->
<script>
        window.addEventListener('message', (event) => {
        const data = event.data;
        console.log('message posted from the embedded page', data);

        const method = data.method;
        const code = data.code;
        const save_id = data.save_id;

        switch (method) {
            case 'configurationsaved': {
                console.log('the user clicked the save button in the top right corner, and received the following code: ' + code);
                // do other actions here
                break;
            }
            case 'addtocart': {
                console.log('the user clicked the "add to cart" button in the bottom right corner, and received the following code: ' + code);
                // do other actions here
                break;
            }
            case 'configurationsaved_remotely': {
                console.log('the configuration was saved remotely and received the following code: ' + code + ', the save_id was ' + save_id);
                // do other actions here
                break;
            }
            case 'requestoffer_form_sent': {
                console.log('the request offer form was sent for the configuration code: ' + code);
                // do other actions here
                break;
            }
            default: {
                console.log('unhandled or unknown method: ' + method);
            }
        }
    });
</script>

<!-- embedding the configurator -->
<iframe src="/identifier:softshell_creator_2d" id="configurator_page" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>�h]�hX}  <!-- listening to message events -->
<script>
        window.addEventListener('message', (event) => {
        const data = event.data;
        console.log('message posted from the embedded page', data);

        const method = data.method;
        const code = data.code;
        const save_id = data.save_id;

        switch (method) {
            case 'configurationsaved': {
                console.log('the user clicked the save button in the top right corner, and received the following code: ' + code);
                // do other actions here
                break;
            }
            case 'addtocart': {
                console.log('the user clicked the "add to cart" button in the bottom right corner, and received the following code: ' + code);
                // do other actions here
                break;
            }
            case 'configurationsaved_remotely': {
                console.log('the configuration was saved remotely and received the following code: ' + code + ', the save_id was ' + save_id);
                // do other actions here
                break;
            }
            case 'requestoffer_form_sent': {
                console.log('the request offer form was sent for the configuration code: ' + code);
                // do other actions here
                break;
            }
            default: {
                console.log('unhandled or unknown method: ' + method);
            }
        }
    });
</script>

<!-- embedding the configurator -->
<iframe src="/identifier:softshell_creator_2d" id="configurator_page" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>�����}�hhzsbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��force���language��html��highlight_args�}�uh+hxhh,hKhhYhhubh.)��}�(h��With the configuration code from the message event, the surrounding page can retrieve the detail information for the saved configuration and the calculation.�h]�h��With the configuration code from the message event, the surrounding page can retrieve the detail information for the saved configuration and the calculation.�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK9hhYhhubhy)��}�(h�Phttps://[configuratorURL]/frontendapi/configuration/loadconfigurationinfo/{code}�h]�h�Phttps://[configuratorURL]/frontendapi/configuration/loadconfigurationinfo/{code}�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��html�h�}�uh+hxhh,hK;hhYhhubh.)��}�(hX  The call returns a json object with all relevant information to the configuration, e.g. selected options, selected designareas, and the calculated price. The price is calculated as soon as you load this call, so you can update the price e.g. from your cart. JSON Example:�h]�hX  The call returns a json object with all relevant information to the configuration, e.g. selected options, selected designareas, and the calculated price. The price is calculated as soon as you load this call, so you can update the price e.g. from your cart. JSON Example:�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK?hhYhhubhy)��}�(hX�  {
  "code": "XYZ123",
  "item": "Item Name",
  "itemIdentifier": "123456",
  "calculation": {
    "total": 519,
    "totalFormatted": "519.00 €",
    "netTotal": 436.13,
    "netTotalFormatted": "436.13 €",
  },
  "optionclassifications": [
    {
      "identifier": "component_identifier",
      "title": "Component Title",
      "selectedoptions": [
        {
          "identifier": "option_identifier",
          "title": "Option Title",
          "amount": 1
        }
      ]
    },
  ],
  "images": [
    "//urltoimage1"
  ],
  "customdata": {},
  "date_created": {
    "date": "2037-07-13 17:32:41.000000",
    "timezone_type": 3,
    "timezone": "Europe/Paris"
  }
}�h]�hX�  {
  "code": "XYZ123",
  "item": "Item Name",
  "itemIdentifier": "123456",
  "calculation": {
    "total": 519,
    "totalFormatted": "519.00 €",
    "netTotal": 436.13,
    "netTotalFormatted": "436.13 €",
  },
  "optionclassifications": [
    {
      "identifier": "component_identifier",
      "title": "Component Title",
      "selectedoptions": [
        {
          "identifier": "option_identifier",
          "title": "Option Title",
          "amount": 1
        }
      ]
    },
  ],
  "images": [
    "//urltoimage1"
  ],
  "customdata": {},
  "date_created": {
    "date": "2037-07-13 17:32:41.000000",
    "timezone_type": 3,
    "timezone": "Europe/Paris"
  }
}�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��json�h�}�uh+hxhh,hKAhhYhhubh	�note���)��}�(h�nMore information on the `postMessage` api: https://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage�h]�h.)��}�(hh�h]�(h�More information on the �����}�(hh�hhhNhNubh8)��}�(h�`postMessage`�h]�h�postMessage�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h7hh�ubh� api: �����}�(hh�hhhNhNubh	�	reference���)��}�(h�Chttps://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage�h]�h�Chttps://developer.mozilla.org/en-US/docs/Web/API/Window/postMessage�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��refuri�h�uh+h�hh�ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKghh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hhYhhhh,hNubh)��}�(hhh]�(h)��}�(h�Add to cart�h]�h�Add to cart�����}�(hj
  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj  hhhh,hKkubh.)��}�(h�sIf the user selects to add a configuration to the cart, in the configurator a message is sent to the parent window.�h]�h�sIf the user selects to add a configuration to the cart, in the configurator a message is sent to the parent window.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKmhj  hhubhy)��}�(h�Qwindow.parent.postMessage({ method: 'addtocart', code: 'XYZ123' }, targetOrigin);�h]�h�Qwindow.parent.postMessage({ method: 'addtocart', code: 'XYZ123' }, targetOrigin);�����}�hj&  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��
javascript�h�}�uh+hxhh,hKohj  hhubeh}�(h!]��add-to-cart�ah#]�h%]��add to cart�ah']�h)]�uh+h
hhYhhhh,hKkubh)��}�(hhh]�(h)��}�(h�Save configuration�h]�h�Save configuration�����}�(hjA  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj>  hhhh,hKvubh.)��}�(h�YAfter the user saves a configuration, the following message is sent to the parent window:�h]�h�YAfter the user saves a configuration, the following message is sent to the parent window:�����}�(hjO  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKxhj>  hhubhy)��}�(h�cwindow.parent.postMessage({ method: 'configurationsaved_remotely', code: 'XYZ123' }, targetOrigin);�h]�h�cwindow.parent.postMessage({ method: 'configurationsaved_remotely', code: 'XYZ123' }, targetOrigin);�����}�hj]  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��
javascript�h�}�uh+hxhh,hKzhj>  hhubh	�target���)��}�(h�.. _saved_configuration:�h]�h}�(h!]�h#]�h%]�h']�h)]��refid��saved-configuration�uh+jm  hK~hj>  hhhh,ubeh}�(h!]��save-configuration�ah#]�h%]��save configuration�ah']�h)]�uh+h
hhYhhhh,hKvubh)��}�(hhh]�(h)��}�(h�Configuration saved remotely�h]�h�Configuration saved remotely�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(h�]After a configuration was saved remotely, the following message is sent to the parent window:�h]�h�]After a configuration was saved remotely, the following message is sent to the parent window:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubhy)��}�(h�vwindow.parent.postMessage({ method: 'configurationsaved', code: 'XYZ123', save_id: 'a_random_id_123' }, targetOrigin);�h]�h�vwindow.parent.postMessage({ method: 'configurationsaved', code: 'XYZ123', save_id: 'a_random_id_123' }, targetOrigin);�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��
javascript�h�}�uh+hxhh,hK�hj�  hhubh.)��}�(h�RThe save_id equals the value that you passed in - see :ref:`saving_configuration`.�h]�(h�6The save_id equals the value that you passed in - see �����}�(hj�  hhhNhNubh �pending_xref���)��}�(h�:ref:`saving_configuration`�h]�h	�inline���)��}�(hj�  h]�h�saving_configuration�����}�(hj�  hhhNhNubah}�(h!]�h#]�(�xref��std��std-ref�eh%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��integration/communication��	refdomain�j�  �reftype��ref��refexplicit���refwarn���	reftarget��saving_configuration�uh+j�  hh,hK�hj�  ubh�.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubeh}�(h!]�(�configuration-saved-remotely�jz  eh#]�h%]�(�configuration saved remotely��saved_configuration�eh']�h)]�uh+h
hhYhhhh,hK��expect_referenced_by_name�}�j�  jo  s�expect_referenced_by_id�}�jz  jo  subh)��}�(hhh]�(h)��}�(h�Request offer form sent�h]�h�Request offer form sent�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(h�`After the user sends the request offer form, the following message is sent to the parent window:�h]�h�`After the user sends the request offer form, the following message is sent to the parent window:�����}�(hj	  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubhy)��}�(h�^window.parent.postMessage({ method: 'requestoffer_form_sent', code: 'XYZ123' }, targetOrigin);�h]�h�^window.parent.postMessage({ method: 'requestoffer_form_sent', code: 'XYZ123' }, targetOrigin);�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��
javascript�h�}�uh+hxhh,hK�hj�  hhubeh}�(h!]��request-offer-form-sent�ah#]�h%]��request offer form sent�ah']�h)]�uh+h
hhYhhhh,hK�ubeh}�(h!]��receiving-messages�ah#]�h%]��receiving messages�ah']�h)]�uh+h
hhhhhh,hK
ubh)��}�(hhh]�(h)��}�(h�Sending Messages�h]�h�Sending Messages�����}�(hj:  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj7  hhhh,hK�ubh)��}�(hhh]�(h)��}�(h�Switch options�h]�h�Switch options�����}�(hjK  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjH  hhhh,hK�ubh.)��}�(h��To switch options for a loaded creator item, you can *send* a postMessage with the components / options to select as JSON payload to the embedded configurator:�h]�(h�5To switch options for a loaded creator item, you can �����}�(hjY  hhhNhNubh	�emphasis���)��}�(h�*send*�h]�h�send�����}�(hjc  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+ja  hjY  ubh�d a postMessage with the components / options to select as JSON payload to the embedded configurator:�����}�(hjY  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjH  hhubhy)��}�(hX_  document.getElementById('configurator_page').contentWindow.postMessage({type: 'switch_options', options: '[{"identifier":"component_identifier1","selectedoptions":[{"identifier":"option_identifier_1","amount":2},{"identifier":"option_identifier_2"}]},{"identifier":"component_identifier2","selectedoptions":[{"identifier":"option_identifier_3"}]}]'});�h]�hX_  document.getElementById('configurator_page').contentWindow.postMessage({type: 'switch_options', options: '[{"identifier":"component_identifier1","selectedoptions":[{"identifier":"option_identifier_1","amount":2},{"identifier":"option_identifier_2"}]},{"identifier":"component_identifier2","selectedoptions":[{"identifier":"option_identifier_3"}]}]'});�����}�hj{  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��
javascript�h�}�uh+hxhh,hK�hjH  hhubh.)��}�(h�?If you don't provide an amount for an option, it defaults to 1.�h]�h�AIf you don’t provide an amount for an option, it defaults to 1.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjH  hhubjn  )��}�(h�.. _saving_configuration:�h]�h}�(h!]�h#]�h%]�h']�h)]�jy  �saving-configuration�uh+jm  hK�hjH  hhhh,ubeh}�(h!]��switch-options�ah#]�h%]��switch options�ah']�h)]�uh+h
hj7  hhhh,hK�ubh)��}�(hhh]�(h)��}�(h�Saving a configuration�h]�h�Saving a configuration�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(h��To save the current configuration, you can *send* a postMessage with an optional save_id to identify the async response (see :ref:`saved_configuration`) as payload to the embedded configurator:�h]�(h�+To save the current configuration, you can �����}�(hj�  hhhNhNubjb  )��}�(h�*send*�h]�h�send�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+ja  hj�  ubh�L a postMessage with an optional save_id to identify the async response (see �����}�(hj�  hhhNhNubj�  )��}�(h�:ref:`saved_configuration`�h]�j�  )��}�(hj�  h]�h�saved_configuration�����}�(hj�  hhhNhNubah}�(h!]�h#]�(j�  �std��std-ref�eh%]�h']�h)]�uh+j�  hj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�j�  �	refdomain�j�  �reftype��ref��refexplicit���refwarn��j�  �saved_configuration�uh+j�  hh,hK�hj�  ubh�*) as payload to the embedded configurator:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubhy)��}�(h��document.getElementById('configurator_page').contentWindow.postMessage({type: 'switch_options', save_id: 'a_random_id_123', save_type: 'cart'});�h]�h��document.getElementById('configurator_page').contentWindow.postMessage({type: 'switch_options', save_id: 'a_random_id_123', save_type: 'cart'});�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��
javascript�h�}�uh+hxhh,hK�hj�  hhubh.)��}�(h�jThe save_type parameter can be set to either 'cart' or 'user' and defaults to 'user' if it's not provided.�h]�h�xThe save_type parameter can be set to either ‘cart’ or ‘user’ and defaults to ‘user’ if it’s not provided.�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubeh}�(h!]�(�saving-a-configuration�j�  eh#]�h%]�(�saving a configuration��saving_configuration�eh']�h)]�uh+h
hj7  hhhh,hK�j�  }�j%  j�  sj�  }�j�  j�  subeh}�(h!]��sending-messages�ah#]�h%]��sending messages�ah']�h)]�uh+h
hhhhhh,hK�ubh)��}�(hhh]�(h)��}�(h�$Editing configurations from the cart�h]�h�$Editing configurations from the cart�����}�(hj5  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj2  hhhh,hK�ubh.)��}�(h��If you want to let the users edit their configurations that are already added to the cart you can open the saved configuration with the parameter ``_frombasket``.�h]�(h��If you want to let the users edit their configurations that are already added to the cart you can open the saved configuration with the parameter �����}�(hjC  hhhNhNubh	�literal���)��}�(h�``_frombasket``�h]�h�_frombasket�����}�(hjM  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+jK  hjC  ubh�.�����}�(hjC  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj2  hhubhy)��}�(h��<iframe src="https://[configuratorURL]/code:{code}?_frombasket=1" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>�h]�h��<iframe src="https://[configuratorURL]/code:{code}?_frombasket=1" width="100%" height="100%" allow="clipboard-write self *;" frameborder="0"></iframe>�����}�hje  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h��h��html�h�}�uh+hxhh,hK�hj2  hhubh.)��}�(h��If the user then chooses to add the configuration to the cart again it will be overwritten. The Post Message then contains the same code for the configuration as before.
By that code you can then update your position in the cart.�h]�h��If the user then chooses to add the configuration to the cart again it will be overwritten. The Post Message then contains the same code for the configuration as before.
By that code you can then update your position in the cart.�����}�(hju  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj2  hhubeh}�(h!]��$editing-configurations-from-the-cart�ah#]�h%]��$editing configurations from the cart�ah']�h)]�uh+h
hhhhhh,hK�ubeh}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�(jz  ]�jo  aj�  ]�j�  au�nameids�}�(hVhSj4  j1  j;  j8  j�  j}  j�  jz  j�  j�  j,  j)  j/  j,  j�  j�  j%  j�  j$  j!  j�  j�  u�	nametypes�}�(hV�j4  �j;  �j�  �j�  �j�  �j,  �j/  �j�  �j%  �j$  �j�  �uh!}�(hShj1  hYj8  j  j}  j>  jz  j�  j�  j�  j)  j�  j,  j7  j�  jH  j�  j�  j!  j�  j�  j2  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]�(h	�system_message���)��}�(hhh]�h.)��}�(hhh]�h�9Hyperlink target "saved-configuration" is not referenced.�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type��INFO��source�h,�line�K~uh+j  ubj  )��}�(hhh]�h.)��}�(hhh]�h�:Hyperlink target "saving-configuration" is not referenced.�����}�hj8  sbah}�(h!]�h#]�h%]�h']�h)]�uh+h-hj5  ubah}�(h!]�h#]�h%]�h']�h)]��level�K�type�j2  �source�h,�line�K�uh+j  ube�transformer�N�include_log�]��
decoration�Nhhub.