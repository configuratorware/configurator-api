��
Q      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Server Configuration�h]�h	�Text����Server Configuration�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�t/builds/redhotmagma/configuratorware/configuratorware-dev/configurator-api/docs/integration/server-configuration.rst�hKubh	�	paragraph���)��}�(hXL  This part of the documentation contains tips of examples when preparing infrastructure according to the configuratorware system requirements.
It addresses System Administrators that know their infrastructure well and can decide whether to apply the given tips on their specific systems and
how to modify the given examples properly.�h]�hXL  This part of the documentation contains tips of examples when preparing infrastructure according to the configuratorware system requirements.
It addresses System Administrators that know their infrastructure well and can decide whether to apply the given tips on their specific systems and
how to modify the given examples properly.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�	Webserver�h]�h�	Webserver�����}�(hh@hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh=hhhh,hKubh)��}�(hhh]�(h)��}�(h�Apache�h]�h�Apache�����}�(hhQhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhhNhhhh,hKubh.)��}�(h��Configuratorware ships with an example ``.htaccess`` that can be adjusted and used for routing the resources.
If you don't want to use .htaccess, configure apache accordingly.�h]�(h�'Configuratorware ships with an example �����}�(hh_hhhNhNubh	�literal���)��}�(h�``.htaccess``�h]�h�	.htaccess�����}�(hhihhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghh_ubh�} that can be adjusted and used for routing the resources.
If you don’t want to use .htaccess, configure apache accordingly.�����}�(hh_hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhNhhubeh}�(h!]��apache�ah#]�h%]��apache�ah']�h)]�uh+h
hh=hhhh,hKubh)��}�(hhh]�(h)��}�(h�Nginx�h]�h�Nginx�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hKubh.)��}�(h��It is possible to use nginx to serve configuratorware.
It will require to manage the routing for the app completely independent from the application as nginx doesn't have an equivilant to ``.htaccess``
Find below an example configuration for nginx:�h]�(h��It is possible to use nginx to serve configuratorware.
It will require to manage the routing for the app completely independent from the application as nginx doesn’t have an equivilant to �����}�(hh�hhhNhNubhh)��}�(h�``.htaccess``�h]�h�	.htaccess�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghh�ubh�/
Find below an example configuration for nginx:�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�hhubh	�literal_block���)��}�(hX�  server {
    listen 8080;
    root /app/public;
    index index.php index.htm index.html;
    client_max_body_size 20m;

    location ~ ^/(dynamic_file|media|frontendapi|api)(/|$) {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass php;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    location ~ admin((\/$)|$) {
        try_files $uri /admin/index.html$is_args$args;
    }

    location / {
        try_files $uri /index.html$is_args$args;
    }
}�h]�hX�  server {
    listen 8080;
    root /app/public;
    index index.php index.htm index.html;
    client_max_body_size 20m;

    location ~ ^/(dynamic_file|media|frontendapi|api)(/|$) {
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass php;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    location ~ admin((\/$)|$) {
        try_files $uri /admin/index.html$is_args$args;
    }

    location / {
        try_files $uri /index.html$is_args$args;
    }
}�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve��force���language��default��highlight_args�}�uh+h�hh,hKhh�hhubeh}�(h!]��nginx�ah#]�h%]��nginx�ah']�h)]�uh+h
hh=hhhh,hKubh)��}�(hhh]�(h)��}�(h�Htaccess�h]�h�Htaccess�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hK;ubh)��}�(hhh]�(h)��}�(h�Browser Caching�h]�h�Browser Caching�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhh�hhhh,hK>ubh.)��}�(h�eTo disallow browser caching for certain files, you can adjust and add the following to ``.htaccess``:�h]�(h�WTo disallow browser caching for certain files, you can adjust and add the following to �����}�(hh�hhhNhNubhh)��}�(h�``.htaccess``�h]�h�	.htaccess�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghh�ubh�:�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK@hh�hhubh�)��}�(hX8  <If "%{REQUEST_URI} =~ m#^/translations_cached/.*\.json#">
  FileETag None
  <ifModule mod_headers.c>
     Header unset ETag
     Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
     Header set Pragma "no-cache"
     Header set Expires "Thu, 1 Jan 1970 00:00:00 GMT"
  </ifModule>
</If>�h]�hX8  <If "%{REQUEST_URI} =~ m#^/translations_cached/.*\.json#">
  FileETag None
  <ifModule mod_headers.c>
     Header unset ETag
     Header set Cache-Control "max-age=0, no-cache, no-store, must-revalidate"
     Header set Pragma "no-cache"
     Header set Expires "Thu, 1 Jan 1970 00:00:00 GMT"
  </ifModule>
</If>�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h̉h�h�h�}�uh+h�hh,hKBhh�hhubeh}�(h!]��browser-caching�ah#]�h%]��browser caching�ah']�h)]�uh+h
hh�hhhh,hK>ubeh}�(h!]��htaccess�ah#]�h%]��htaccess�ah']�h)]�uh+h
hh=hhhh,hK;ubeh}�(h!]��	webserver�ah#]�h%]��	webserver�ah']�h)]�uh+h
hhhhhh,hKubh)��}�(hhh]�(h)��}�(h�Packages�h]�h�Packages�����}�(hjE  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjB  hhhh,hKPubh)��}�(hhh]�(h)��}�(h�ImageMagick�h]�h�ImageMagick�����}�(hjV  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjS  hhhh,hKSubh.)��}�(h��ImageMagick is a common library that has a php extension.
A lot of hosting providers have the possiblity to enable the php-ext as well as the CLI tool via their administration-interfaces.�h]�h��ImageMagick is a common library that has a php extension.
A lot of hosting providers have the possiblity to enable the php-ext as well as the CLI tool via their administration-interfaces.�����}�(hjd  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKThjS  hhubeh}�(h!]��imagemagick�ah#]�h%]��imagemagick�ah']�h)]�uh+h
hjB  hhhh,hKSubh)��}�(hhh]�(h)��}�(h�Inkscape�h]�h�Inkscape�����}�(hj}  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjz  hhhh,hKYubh.)��}�(h��Inkscape is usually not installable via an administration-interface or CLI. Ask the hosting provider support, if they can install it in the required version.�h]�h��Inkscape is usually not installable via an administration-interface or CLI. Ask the hosting provider support, if they can install it in the required version.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKZhjz  hhubeh}�(h!]��inkscape�ah#]�h%]��inkscape�ah']�h)]�uh+h
hjB  hhhh,hKYubh)��}�(hhh]�(h)��}�(h�Ghostscript�h]�h�Ghostscript�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK^ubh.)��}�(h�^Ghostscript is a required dependency for Inkscape. So usually no separate install is required.�h]�h�^Ghostscript is a required dependency for Inkscape. So usually no separate install is required.�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK_hj�  hhubeh}�(h!]��ghostscript�ah#]�h%]��ghostscript�ah']�h)]�uh+h
hjB  hhhh,hK^ubh)��}�(hhh]�(h)��}�(h�Potrace�h]�h�Potrace�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKcubh.)��}�(h��Potrace might be installed as an dependency with inkscape. If it is not installed after inkscape was added, check if it can be installed via CLI.
For example on Hetzner V-Server it is possible to install via:  ``software install potrace``�h]�(h��Potrace might be installed as an dependency with inkscape. If it is not installed after inkscape was added, check if it can be installed via CLI.
For example on Hetzner V-Server it is possible to install via:  �����}�(hj�  hhhNhNubhh)��}�(h�``software install potrace``�h]�h�software install potrace�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghj�  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKdhj�  hhubeh}�(h!]��potrace�ah#]�h%]��potrace�ah']�h)]�uh+h
hjB  hhhh,hKcubh)��}�(hhh]�(h)��}�(h�
Weasyprint�h]�h�
Weasyprint�����}�(hj   hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hKiubh.)��}�(hX�  Weasyprint was replace with PHP library DomPDF in configuratorware version 1.25 and is included automatically via composer.
If you still need to use Weasyprint, you can install and use it manually (see upgrade notes for `Version 1.25 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.25.md>`_ and `Version 1.35 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.35.md>`_):�h]�(h��Weasyprint was replace with PHP library DomPDF in configuratorware version 1.25 and is included automatically via composer.
If you still need to use Weasyprint, you can install and use it manually (see upgrade notes for �����}�(hj  hhhNhNubh	�	reference���)��}�(h�d`Version 1.25 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.25.md>`_�h]�h�Version 1.25�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name��Version 1.25��refuri��Rhttps://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.25.md�uh+j  hj  ubh	�target���)��}�(h�U <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.25.md>�h]�h}�(h!]��version-1-25�ah#]�h%]��version 1.25�ah']�h)]��refuri�j)  uh+j*  �
referenced�Khj  ubh� and �����}�(hj  hhhNhNubj  )��}�(h�d`Version 1.35 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.35.md>`_�h]�h�Version 1.35�����}�(hj>  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name��Version 1.35�j(  �Rhttps://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.35.md�uh+j  hj  ubj+  )��}�(h�U <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.35.md>�h]�h}�(h!]��version-1-35�ah#]�h%]��version 1.35�ah']�h)]��refuri�jN  uh+j*  j9  Khj  ubh�):�����}�(hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKjhj�  hhubh.)��}�(h��Weasyprint needs pip (a common python package manager) for installation. Pip is available through venv in many infrastructures (e.g. Hetzner V-Server):�h]�h��Weasyprint needs pip (a common python package manager) for installation. Pip is available through venv in many infrastructures (e.g. Hetzner V-Server):�����}�(hjf  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKmhj�  hhubh�)��}�(h�,python3 -m venv ./venv
. ./venv/bin/activate�h]�h�,python3 -m venv ./venv
. ./venv/bin/activate�����}�hjt  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h̉h͌sh�h�}�uh+h�hh,hKohj�  hhubh.)��}�(h�,Alternatively Pip can be installed manually:�h]�h�,Alternatively Pip can be installed manually:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKthj�  hhubh�)��}�(h�Fcurl -O https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py --user�h]�h�Fcurl -O https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py --user�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h̉h͌sh�h�}�uh+h�hh,hKvhj�  hhubh.)��}�(h�9If pip or pip3 is available, weasyprint can be installed:�h]�h�9If pip or pip3 is available, weasyprint can be installed:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK{hj�  hhubh�)��}�(h�3pip install weasyprint  #or pip3 install weasyprint�h]�h�3pip install weasyprint  #or pip3 install weasyprint�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h̉h͌sh�h�}�uh+h�hh,hK}hj�  hhubeh}�(h!]��
weasyprint�ah#]�h%]��
weasyprint�ah']�h)]�uh+h
hjB  hhhh,hKiubh)��}�(hhh]�(h)��}�(h�PATH environment variable�h]�h�PATH environment variable�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(h��After installation of packages it is necessary that PHP knows about all folders that contain executables. Therefore configuratorware relies on the `PATH`
environment variable. In managed hosting scenarios there are several ways to do that:�h]�(h��After installation of packages it is necessary that PHP knows about all folders that contain executables. Therefore configuratorware relies on the �����}�(hj�  hhhNhNubh	�title_reference���)��}�(h�`PATH`�h]�h�PATH�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+j�  hj�  ubh�V
environment variable. In managed hosting scenarios there are several ways to do that:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh)��}�(hhh]�(h)��}�(h�Prepend script with putenv()�h]�h�Prepend script with putenv()�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(h�]Set a prepend script via ``php.ini``: ``auto_prepend_file = /home/your-user/bin/prepend.php``�h]�(h�Set a prepend script via �����}�(hj  hhhNhNubhh)��}�(h�``php.ini``�h]�h�php.ini�����}�(hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghj  ubh�: �����}�(hj  hhhNhNubhh)��}�(h�7``auto_prepend_file = /home/your-user/bin/prepend.php``�h]�h�3auto_prepend_file = /home/your-user/bin/prepend.php�����}�(hj&  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghj  ubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh�)��}�(h�b<?php

putenv("PATH=/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin");�h]�h�b<?php

putenv("PATH=/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin");�����}�hj:  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h̉h͌php�h�}�uh+h�hh,hK�hj�  hhubh.)��}�(h�}Be aware that if PHP is installed via SAPI, environment variables set by ``putenv()`` are not available to the local process.�h]�(h�IBe aware that if PHP is installed via SAPI, environment variables set by �����}�(hjJ  hhhNhNubhh)��}�(h�``putenv()``�h]�h�putenv()�����}�(hjR  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghjJ  ubh�( are not available to the local process.�����}�(hjJ  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubeh}�(h!]��prepend-script-with-putenv�ah#]�h%]��prepend script with putenv()�ah']�h)]�uh+h
hj�  hhhh,hK�ubh)��}�(hhh]�(h)��}�(h�	.htaccess�h]�h�	.htaccess�����}�(hju  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhjr  hhhh,hK�ubh.)��}�(h�HSet the environment variables via .htaccess, this can be done like that:�h]�h�HSet the environment variables via .htaccess, this can be done like that:�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjr  hhubh�)��}�(h��SetEnv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/home/your-user/.linuxbrew/bin:/usr/home/your-user/venv/bin�h]�h��SetEnv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/home/your-user/.linuxbrew/bin:/usr/home/your-user/venv/bin�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h̉h͌sh�h�}�uh+h�hh,hK�hjr  hhubh.)��}�(h�pBe aware that if PHP is installed via SAPI, a ``REMOTE_`` is added to the environment variable set by .htaccess.�h]�(h�.Be aware that if PHP is installed via SAPI, a �����}�(hj�  hhhNhNubhh)��}�(h�``REMOTE_``�h]�h�REMOTE_�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghj�  ubh�7 is added to the environment variable set by .htaccess.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hjr  hhubeh}�(h!]��id1�ah#]�h%]��	.htaccess�ah']�h)]�uh+h
hj�  hhhh,hK�ubh)��}�(hhh]�(h)��}�(h�(Prepend script modifies global constants�h]�h�(Prepend script modifies global constants�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK�ubh.)��}�(h��If there is no different possibility to set the environment variable, there is a suboptimal solution to modify php's ``$_SERVER`` and ``$_ENV`` global:�h]�(h�wIf there is no different possibility to set the environment variable, there is a suboptimal solution to modify php’s �����}�(hj�  hhhNhNubhh)��}�(h�``$_SERVER``�h]�h�$_SERVER�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghj�  ubh� and �����}�(hj�  hhhNhNubhh)��}�(h�	``$_ENV``�h]�h�$_ENV�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hghj�  ubh� global:�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK�hj�  hhubh�)��}�(h��<?php

$_SERVER['PATH'] = '/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin';

$_ENV['PATH'] = '/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin';�h]�h��<?php

$_SERVER['PATH'] = '/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin';

$_ENV['PATH'] = '/usr/local/bin:/usr/bin:/bin:/home/your-user/bin:/home/your-user/.local/bin';�����}�hj  sbah}�(h!]�h#]�h%]�h']�h)]�h�h�h̉h͌php�h�}�uh+h�hh,hK�hj�  hhubeh}�(h!]��(prepend-script-modifies-global-constants�ah#]�h%]��(prepend script modifies global constants�ah']�h)]�uh+h
hj�  hhhh,hK�ubeh}�(h!]��path-environment-variable�ah#]�h%]��path environment variable�ah']�h)]�uh+h
hjB  hhhh,hK�ubeh}�(h!]��packages�ah#]�h%]��packages�ah']�h)]�uh+h
hhhhhh,hKPubeh}�(h!]��server-configuration�ah#]�h%]��server configuration�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,�translation_progress�}�(�total�K �
translated�K uuh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(�output�NhN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jd  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j9  j6  j?  j<  h�h�h�h�j7  j4  j/  j,  j1  j.  jw  jt  j�  j�  j�  j�  j�  j�  j�  j�  j5  j2  jX  jU  j)  j&  jo  jl  j�  j�  j!  j  u�	nametypes�}�(j9  �j?  �h��h։j7  �j/  �j1  �jw  �j�  �j�  �j�  �j�  �j5  �jX  �j)  �jo  �j�  �j!  �uh!}�(j6  hj<  h=h�hNh�h�j4  h�j,  h�j.  jB  jt  jS  j�  jz  j�  j�  j�  j�  j�  j�  j2  j,  jU  jO  j&  j�  jl  j�  j�  jr  j  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}�jr  Ks��R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.