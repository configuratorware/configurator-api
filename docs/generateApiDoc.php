<?php declare(strict_types=1);

// static docs
$apiClassDoc = "
###########
API classes
###########

";

$servicePath = __DIR__ . '/../src/Service';
$apiClasses = glob($servicePath . '/*/*Api.php');

// collect all public methods from api classes
$apiMethods = [];
foreach ($apiClasses as $apiClass) {

    $classPath = realpath($apiClass);
    include "$classPath";

    $className = str_replace(
        [$servicePath, '/', '.php'], ['Redhotmagma\ConfiguratorApiBundle\Service', '\\', ''], $apiClass
    );
    try {
        $sourceClass = new ReflectionClass($className);
        $methods = $sourceClass->getMethods(ReflectionMethod::IS_PUBLIC);
    }
    catch (ReflectionException $e) {
        $methods = [];
    }

    foreach ($methods as $method) {
        if ($method->getName() !== '__construct') {

            $classTitle = str_replace('\\', '\\\\', $sourceClass->getName());

            if(!isset($apiMethods[$sourceClass->getName()])) {

                $apiClassDoc .= "\n\n" .
                str_repeat('*', strlen($classTitle)) . "\n" .
                $classTitle . "\n" .
                str_repeat('*', strlen($classTitle)) . "\n" .
                "\n";
                $apiMethods[$sourceClass->getName()] = [];
            }


            $apiClassDoc .= $method->getName() . "\n";
            $apiClassDoc .= str_repeat('*', strlen($method->getName())) . "\n\n";

            if($method->getParameters()) {

                $apiClassDoc .= "Parameters\n";
                $apiClassDoc .= "==========\n\n";

                foreach($method->getParameters() as $parameter) {
                    $apiClassDoc .= '* ';
                    if($parameter->hasType()) {
                        $apiClassDoc .= str_replace('\\', '\\\\', $parameter->getType()->getName());
                    }
                    else {
                        $apiClassDoc .= 'mixed';
                    }
                    $apiClassDoc .= ' : $' . $parameter->getName() . "\n";
                }
            }
            $apiClassDoc .= "\n";
            if($method->hasReturnType()) {

                $apiClassDoc .= "Return Value\n";
                $apiClassDoc .= "============\n";
                $apiClassDoc .= str_replace('\\', '\\\\', $method->getReturnType()->getName()) . "\n\n";
            }
        }
    }
}

file_put_contents(__DIR__ . '/../docs/api/apiclasses.rst', $apiClassDoc);

