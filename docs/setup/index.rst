.. _setup:

Setup
=====

======
Update
======
To update configuratorware it is necessary to have a valid licence installed.

The update is a 2-step process:

* Update the api and database
* Update or rebuild the frontendgui and admingui

Api and Database
################
For each update, it is recommended to first backup the database before upgrading.
To update the api and the database run via shell from the project directory:
``composer update-configuratorware``

Make sure to read the upgrade notes for the version you're about to install. Deprecations, breaking changes, updated dependencies and other version specific changes are documented there:

* `Version 1.15 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.15.md>`_
* `Version 1.16 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.16.md>`_
* `Version 1.17 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.17.md>`_
* `Version 1.18 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.18.md>`_
* `Version 1.19 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.19.md>`_
* `Version 1.20 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.20.md>`_
* `Version 1.21 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.21.md>`_
* `Version 1.22 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.22.md>`_
* `Version 1.24 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.24.md>`_
* `Version 1.25 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.25.md>`_
* `Version 1.27 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.27.md>`_
* `Version 1.28 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.28.md>`_
* `Version 1.29 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.29.md>`_
* `Version 1.31 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.31.md>`_
* `Version 1.32 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.32.md>`_
* `Version 1.33 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.33.md>`_
* `Version 1.35 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.35.md>`_
* `Version 1.38 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.38.md>`_
* `Version 1.42 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.42.md>`_
* `Version 1.45 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.45.md>`_
* `Version 1.49 <https://gitlab.com/configuratorware/configurator-api/-/blob/master/UPGRADE-1.49.md>`_

The latest upgrade notes can also be found in the vendor folder in your configuratorware based project: ``vendor/configuratorware/configurator-api/UPGRADE-<VersionNumber>.md`` 

frontendgui and admingui
########################
If there is no customizing in frontend projects it's possible to use prebundled frontends by running:
``bin/console configuratorware:download-frontend-apps [argument]``

This command downloads a ready to use version of the configuratorware frontendgui and admingui and can be run with the
following different arguments:

* ``frontendgui`` to update only the frontendgui
* ``admingui`` to update only the admingui
* ``frontendgui+admingui`` to update both

For customized frontends it is necessary to setup a build process to create bundled versions.
