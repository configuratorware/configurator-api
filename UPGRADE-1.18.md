UPGRADE FROM 1.17 to 1.18
=========================
If you want to update from a lower version than 1.17 to 1.18, please read previous UPGRADE notes.

Deprecations
------------

- Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository::getByItemId