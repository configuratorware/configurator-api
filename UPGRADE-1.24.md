# UPGRADE FROM 1.22/1.23 to 1.24
If you want to update from a lower version than 1.22 to 1.24, please read previous UPGRADE notes.

## Deprecations
* Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\Designer\FrontendDesignerApi

## Removed API
* the POST request `api/configurations` has been removed. As a replacement there has been already a save call `frontendapi/configurations/frontendsave` that saves a 
  configuration like the frontend in admin mode which can be used to save a configuration from admin.

## Changes
* Properties, Constructors and Accessors of Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView

## Removed Interfaces/Classes
* Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewRepositoryInterface
* Redhotmagma\ConfiguratorApiBundle\Interfaces\DataMapperInterface
* Redhotmagma\ConfiguratorApiBundle\Interfaces\StructureFromEntityRelationConverterInterface
* Redhotmagma\ConfiguratorApiBundle\Interfaces\StructureFromEntityRelationConverterInterface

## Removed ApiClass Function
* The function Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationApi::save has been removed, use ConfigurationApi::frontendSave instead.

## Removed internal Classes
* Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ConfigurationResolver
* Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewRepository
* Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientService
* Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSave
* Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationEntityFromStructureConverter
* Redhotmagma\ConfiguratorApiBundle\Service\Configuration\PartsListHash
* Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\Designer\FrontendDesignerDataMapper
* Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette\StructureProvider\ColorPalettesMappedByDesignProductionMethod
* Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewFactory 
* Redhotmagma\ConfiguratorApiBundle\Service\DesignView\LayerDesignViewGenerator
* Redhotmagma\ConfiguratorApiBundle\Validator\ConfigurationValidator
