# UPGRADE FROM 1.31 to 1.32
If you want to update from a lower version than 1.31 to 1.32, please read previous UPGRADE notes.

## MySQL and MariaDB
From this version on the supported MySQL and MariaDB Versions will change. The support of MySQL 5.6 got dropped. 
It is EOL, please update soon. The currently supported versions are:
* MariaDB: 10.2 - 10.7
* MySQL: 5.7 or 8.0

## Foreign Key constraints
This version introduces Foreign Key Constraints to the database. Any update should be tested with a live database dump 
to check if any data cleanup is necessary for inconsistent references that might break the Schema Update.

If for any reason you want to keep your database without foreign key constraints, this can be done with the following implications:
* disable Schema Update Core Migrations and manage the Schema on your own
* create a class `CustomMySqlPlatform.php`
* configure `CustomMySqlPlatform`  as `platform_service` for doctrine

### Disable Schema Update
Point the `Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate` namespace to an empty directory. If you have already configured `migrations_path`,
enhance your configuration (do not remove configured paths).

```yaml
doctrine_migrations:
    migrations_paths:
        'Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate': /any/existing/empty/path
```

### Create a class CustomMySqlPlatform
Create a Class `CustomMySqlPlatform.php` with the following contents:

```php
namespace App\Doctrine;

use Doctrine\DBAL\Platforms\MySqlPlatform;

class CustomMySqlPlatform extends MySqlPlatform
{
    public function supportsForeignKeyConstraints(): bool
    {
        return false;
    }

    public function supportsForeignKeyOnUpdate(): bool
    {
        return false;
    }
}

```

### Configure doctrine to use your CustomMySqlPlatform
In `config/packages/doctrine.yaml` add `platform_service` to `doctrine.dbal`.

```yaml
doctrine:
    dbal:
        platform_service: 'App\Doctrine\CustomMySqlPlatform'
```