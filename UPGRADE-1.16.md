UPGRADE FROM 1.15 to 1.16
=========================
If you want to update from a lower version than 1.15 to 1.16, please read previous UPGRADE notes.

Visualization Data
------------------
The determination of which visualization data the frontend uses has been changed. Therefore a <1.16 frontend will produce errors when used together with a >=1.16 api. While there is no guarantee, that different versions are compatible, please check in this particular case definitely if you have upgraded all parts of the app if encounter any errors.

Item Status
-----------
The item status has been moved to the item. Configuratorware ships with a migration to migrate your existing data to the item. To make a smooth migration possible, it is recommended for each update to backup the database first before upgrading.

Deprecations
------------
The following entities have been deprecated:
- ConfigurationMode
- ItemConfigurationModeItemStatus
- Itemsetting

Import
------
The Import format for importing the item status has changed. Please check the documentation for the new format. Configuratorware ships with a transformer to correct your data if you use the old format, but this will be deprecated soon.

Interface
---------
The interface for Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check::execute has changed.
