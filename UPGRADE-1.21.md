# UPGRADE FROM 1.20 to 1.21
If you want to update from a lower version than 1.20 to 1.21, please read previous UPGRADE notes.

## Internal classes
The configuratorware backend has a public API, usually having the name "...Api" included.
These classes are not marked as @internal and are therefore part of our backward compatibility promise.
All classes that are marked as internal might change at any time with a new release without deprecations.
