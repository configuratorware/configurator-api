# UPGRADE FROM 1.42 to 1.44
If you want to update from a lower version than 1.42 to 1.44, please read previous UPGRADE notes.

## Three js update
The visualization component was updated to use the latest version of three js.
If you're using a custom visualization component, you'll have to adjust it to be compatible with the update:
https://github.com/mrdoob/three.js/releases/tag/r164