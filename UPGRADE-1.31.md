# UPGRADE FROM 1.29 to 1.31
If you want to update from a lower version than 1.29 to 1.31, please read previous UPGRADE notes.

## Deprecations

## Symfony 5.4 and PHP 7.4 required
From this version on Symfony 5.4 and PHP 7.4 are now a requirement enforced by composer.

## Changes
* route api/login_check requires now a json in the request body instead of multi-part form data
* Redhotmagma\ConfiguratorApiBundle\Entity\User::setPassword() now requires Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface as second argument