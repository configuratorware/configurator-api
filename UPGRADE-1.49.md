# UPGRADE FROM 1.48 to 1.49
If you want to update from a lower version than 1.48 to 1.49, please read previous UPGRADE notes.

## Update to Symfony 6
This version updates the framework to symfony 6.4. There are changes necessary to meet the new requirements.
* if you are using any dependencies that conflict with symfony 6 update them
* change project files (see below)

### Update composer.json
Your composer.json should look similar to that:
```json 
{
  "type": "project",
  "license": "proprietary",
  "require": {
    "php": "^8.1||^8.2||^8.3",
    "configuratorware/configurator-api": "^1.49",
    "symfony/console": "6.*",
    "symfony/dotenv": "6.*",
    "symfony/flex": "^1.17|^2",
    "symfony/framework-bundle": "6.*",
    "symfony/runtime": "6.*",
    "symfony/yaml": "6.*"
  },
  "config": {
    "optimize-autoloader": true,
    "preferred-install": {
      "*": "dist"
    },
    "sort-packages": true,
    "allow-plugins": {
      "composer/package-versions-deprecated": true,
      "endroid/installer": true,
      "symfony/flex": true,
      "symfony/runtime": true
    }
  },
  "autoload": {
    "psr-4": {
      "App\\": "src/"
    }
  },
  "autoload-dev": {
    "psr-4": {
      "Tests\\": "tests/"
    }
  },
  "replace": {
    "symfony/polyfill-ctype": "*",
    "symfony/polyfill-iconv": "*",
    "symfony/polyfill-php72": "*"
  },
  "scripts": {
    "auto-scripts": {
      "cache:clear": "symfony-cmd",
      "assets:install %PUBLIC_DIR%": "symfony-cmd"
    },
    "post-install-cmd": [
      "@auto-scripts"
    ],
    "post-update-cmd": [
      "@auto-scripts"
    ],
    "update-configuratorware": [
      "@composer update",
      "bin/console doctrine:migrations:migrate --no-interaction --quiet",
      "bin/console configuratorware:add-credentials",
      "bin/console configuratorware:create-cached-translation",
      "bin/console configuratorware:refresh-browsercache-hash"
    ]
  },
  "conflict": {
    "symfony/symfony": "*"
  },
  "extra": {
    "symfony": {
      "allow-contrib": false,
      "require": "6.*"
    }
  }
}
```

This is an example list of dev-dependencies, that are compatible with this update:
```php 
"require-dev": {
    "dama/doctrine-test-bundle": "^8.0",
    "friendsofphp/php-cs-fixer": "^3.8",
    "phake/phake": "4.2",
    "phpstan/phpstan": "^1.5",
    "phpunit/phpunit": "^8.5",
    "redhotmagma/symfony-test-utils": "^0.3.4",
    "symfony/browser-kit": "^6.2",
    "symfony/css-selector": "^6.2",
    "symfony/phpunit-bridge": "^6.2",
    "vimeo/psalm": "^4.22"
},
```

### Change Files
The following files need to be changed:
- app/config/packages/endroid_qr_code.yaml
- app/config/packages/doctrine.yaml
- app/templates/bundles/RedhotmagmaConfiguratorApiBundle/configuration/main.html.twig

If you do not have any custom changes, you can just copy the following files (or adapt to your needs):

### app/config/packages/endroid_qr_code.yaml
```php 
endroid_qr_code:
    default:
        writer: Endroid\QrCode\Writer\PngWriter
        size: 300
        margin: 10
        error_correction_level: 'low'
        validate_result: false
```

### app/config/packages/doctrine.yaml
```php 
doctrine:
    dbal:
        connections:
            default:
                url: '%env(resolve:DATABASE_URL)%'
                use_savepoints: true
    orm:
        auto_generate_proxy_classes: true
        auto_mapping: true
        mappings:
            App:
                is_bundle: false
                type: annotation
                dir: '%kernel.project_dir%/src/Entity'
                prefix: 'App\Entity'
                alias: App
```

If you have a customized pdf template, that uses a qr code, the `{ writer: 'png' }` argument has to be removed from the `qr_code_data_uri` call like this: 

### app/templates/bundles/RedhotmagmaConfiguratorApiBundle/configuration/main.html.twig
```php 
<td id="qr-code">
    {% if documentInfo.documentType != 'widget' and cover.configurationUrl != '' %}
        <img src="{{ base64_encode(qr_code_data_uri(cover.configurationUrl)) }}"/>
    {% endif %}
</td>
```

Deprecations
------------

- Support for PHP 7.4 and PHP 8.0 is deprecated from now on. Please update to PHP 8.1 - 8.3.