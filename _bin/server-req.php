<?php

const CW_PHP_VERSION = '7.2.0';
const CW_REQUIRED_EXTENSIONS = [
    'dom',
    'exif',
    'gd',
    'intl',
    'imagick',
    'json',
    'libxml',
    'mbstring',
    'pdo',
    'pdo_mysql',
    'simpleXML',
    'zip',
];
const CW_MAX_EXECUTION_TIME = 300;
const CW_CLI_TOOLS = [
    'mysql --version',
    'potrace --version',
    'inkscape --version',
    'convert --version',
    'weasyprint --version',
];

$missingReqs = '';

// check correct php version
if (version_compare(PHP_VERSION, CW_PHP_VERSION, '<=')) {
    $missingReqs .= sprintf('Wrong PHP Version, expected min: %s', CW_PHP_VERSION) . PHP_EOL;
}

// check installed extensions
foreach (CW_REQUIRED_EXTENSIONS as $extension) {
    if (!extension_loaded($extension)) {
        $missingReqs .= sprintf('Extension missing, expected: %s', $extension) . PHP_EOL;
    }
}

// check max_execution_time
if (CW_MAX_EXECUTION_TIME > ini_get('max_execution_time') && 0 < ini_get('max_execution_time')) {
    $missingReqs .= sprintf('Max Execution Time is too low, expected: %s', CW_MAX_EXECUTION_TIME) . PHP_EOL;
}

// check memory limit
if (-1 == ini_get('memory_limit')) {
    $missingReqs .= sprintf('Memory Limit needs to be set, recommended: 128M') . PHP_EOL;
}

// check cli tools
foreach (CW_CLI_TOOLS as $exec) {
    exec($exec . ' 2>&1', $output, $return);
    if ($return !== 0) {
        $missingReqs .= sprintf('CLI tool is not installed, checked %s', $exec) . PHP_EOL;
    }
}

if (touch('permissions.txt')) {
    unlink('permissions.txt');
} else {
    $missingReqs .= sprintf('Was not able to create file. Please check permissions.') . PHP_EOL;
}

echo PHP_EOL . <<<'EOT'
                  __ _                       _                                                                          _               _    
                 / _(_)                     | |                                                                        | |             | |   
  ___ ___  _ __ | |_ _  __ _ _   _ _ __ __ _| |_ ___  _ ____      ____ _ _ __ ___   ___  ___ _ ____   _____ _ __    ___| |__   ___  ___| | __
 / __/ _ \| '_ \|  _| |/ _` | | | | '__/ _` | __/ _ \| '__\ \ /\ / / _` | '__/ _ \ / __|/ _ \ '__\ \ / / _ \ '__|  / __| '_ \ / _ \/ __| |/ /
| (_| (_) | | | | | | | (_| | |_| | | | (_| | || (_) | |   \ V  V / (_| | | |  __/ \__ \  __/ |   \ V /  __/ |    | (__| | | |  __/ (__|   < 
 \___\___/|_| |_|_| |_|\__, |\__,_|_|  \__,_|\__\___/|_|    \_/\_/ \__,_|_|  \___| |___/\___|_|    \_/ \___|_|     \___|_| |_|\___|\___|_|\_\
                        __/ |                                                                                                                
                       |___/                                                                                                                 

EOT;

if ([] !== $missingReqs) {
    echo $missingReqs;
}

// output some info
echo 'Memory Limit: ' . ini_get('memory_limit') . PHP_EOL;
echo 'Installed Apache Modules: ' . shell_exec('apache2ctl -M') . PHP_EOL;
