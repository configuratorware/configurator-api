#!/bin/bash
if [ ${NODE_VERSION} = 14 ]
then
  rm -f /usr/bin/node
  rm -f /usr/bin/npm
  rm -f /usr/bin/npx
  ln -s /home/application/.nvm/versions/node/v14.16.0/bin/node /usr/bin/node
  ln -s /home/application/.nvm/versions/node/v14.16.0/bin/npm /usr/bin/npm
  ln -s /home/application/.nvm/versions/node/v14.16.0/bin/npx /usr/bin/npx
  echo "nvm use 14" >> /home/application/.bashrc
fi
if [ ${NODE_VERSION} = 16 ]
then
  rm -f /usr/bin/node
  rm -f /usr/bin/npm
  rm -f /usr/bin/npx
  ln -s /home/application/.nvm/versions/node/v16.20.2/bin/node /usr/bin/node
  ln -s /home/application/.nvm/versions/node/v16.20.2/bin/npm /usr/bin/npm
  ln -s /home/application/.nvm/versions/node/v16.20.2/bin/npx /usr/bin/npx
  echo "nvm use 16" >> /home/application/.bashrc
fi
if [ ${NODE_VERSION} = 18 ]
then
  rm -f /usr/bin/node
  rm -f /usr/bin/npm
  rm -f /usr/bin/npx
  ln -s /home/application/.nvm/versions/node/v18.18.0/bin/node /usr/bin/node
  ln -s /home/application/.nvm/versions/node/v18.18.0/bin/npm /usr/bin/npm
  ln -s /home/application/.nvm/versions/node/v18.18.0/bin/npx /usr/bin/npx
  echo "nvm use 18" >> /home/application/.bashrc
fi
if [ ${NODE_VERSION} = 20 ]
then
  rm -f /usr/bin/node
  rm -f /usr/bin/npm
  rm -f /usr/bin/npx
  ln -s /home/application/.nvm/versions/node/v20.17.0/bin/node /usr/bin/node
  ln -s /home/application/.nvm/versions/node/v20.17.0/bin/npm /usr/bin/npm
  ln -s /home/application/.nvm/versions/node/v20.17.0/bin/npx /usr/bin/npx
  echo "nvm use 20" >> /home/application/.bashrc
fi