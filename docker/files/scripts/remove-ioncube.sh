#!/bin/sh
ini_dir_cli=$(php -i | grep "Scan this dir for additional .ini files" | cut -c44-)
ini_dir_fpm=$(php-fpm -i | grep "Scan this dir for additional .ini files" | cut -c44-)

rm -f ${ini_dir_cli}/*ioncube*
rm -f ${ini_dir_fpm}/*ioncube*