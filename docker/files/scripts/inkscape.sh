#!/bin/bash
`inkscape --version > /tmp/version.txt`
VERSION=`cat /tmp/version.txt`
if [[ $VERSION =~ .*1\.2.* ]]; then
  cp /opt/docker/provision/eps_input.inx /usr/share/inkscape/extensions/eps_input.inx
fi