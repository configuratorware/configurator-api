#!/bin/bash
# Pull webdevops images
docker pull webdevops/php-apache-dev:7.4
docker pull webdevops/php-apache-dev:8.0
docker pull webdevops/php-apache-dev:8.1
docker pull webdevops/php-apache-dev:8.2
docker pull webdevops/php-apache-dev:8.3

# Build Docker images
docker build --build-arg="PHP_VERSION=8.0" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.0 . --no-cache
docker build --build-arg="PHP_VERSION=8.1" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.1 . --no-cache
docker build --build-arg="PHP_VERSION=8.2" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.2 . --no-cache
docker build --build-arg="PHP_VERSION=8.3" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.3 . --no-cache
docker build --build-arg="PHP_VERSION=7.4" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-7.4 . --no-cache
docker build -f Dockerfile-weasyprint --build-arg="PHP_VERSION=7.4" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-7.4-weasy . --no-cache
docker build -f Dockerfile-weasyprint --build-arg="PHP_VERSION=8.0" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.0-weasy . --no-cache
docker build -f Dockerfile-weasyprint --build-arg="PHP_VERSION=8.1" -t registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.1-weasy . --no-cache
docker tag registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.3 registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:latest

# Push Docker images
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.0
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.1
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.2
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.3
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-7.4
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-7.4-weasy
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.0-weasy
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:php-8.1-weasy
docker push registry.gitlab.com/configuratorware/configurator-api/configuratorware-php-apache-dev:latest