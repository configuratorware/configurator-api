@echo off
for /f %%i in ('docker ps --filter "ancestor=configuratorapi" --format "{{.ID}}"') do set CONTAINER_ID=%%i
echo opening bash for container: %CONTAINER_ID%
docker exec -i -t %CONTAINER_ID% /bin/bash
