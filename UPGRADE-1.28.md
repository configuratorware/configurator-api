# UPGRADE FROM 1.27 to 1.28
If you want to update from a lower version than 1.27 to 1.28, please read previous UPGRADE notes.

## Replacement of symfony/swiftmailer-bundle by symfony/mailer
This version replaces the symfony/swiftmailer-bundle with the symfony/mailer. If you are using the receiveOffer feature, remove your swiftmailer config and add the config for symfony mailer.

1) remove the following entry from `./config/bundles.php`

```php
Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle::class => ['all' => true],
```

2) remove swiftmailer.yaml files under `./config/packages/{dev|prod|test}`

3) add new file `./config/packages/mailer.yaml` with the following contents:

```yaml
framework:
    mailer:
        dsn: '%env(MAILER_DSN)%' 
```

4) add to `./.env` or  `./.env.local`:

```sh
MAILER_DSN=smtp://user:pass@smtp.example.com:port
```

For further information check: https://symfony.com/doc/current/mailer.html