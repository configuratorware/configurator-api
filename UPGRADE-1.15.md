UPGRADE FROM 1.x to 1.15
========================

Doctrine Migrations
-------------------

In configuratorware 1.15 the setup for doctrine migrations changes in a way that is incompatible with projects that have databases 
created with a version lower than 1.15. So it is necessary to add the following setting for the key `doctrine_migrations` 
in your configuration, before you update to 1.15:

```yaml
doctrine_migrations:
    migrations_paths:
        'Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate': /any/existing/empty/path
        'Redhotmagma\ConfiguratorApiBundle\Migrations\BaseData': /ny/existing/empty/path
        'Application\Migrations': '%redhotmagma_configurator_api.project_dir%/src/DoctrineMigrations'
```

Configuration
-------------
In configuratorware 1.15 we changed the default configuration for some parameters. Please check if they are necessary to overwrite in your project configuration.

```yaml
parameters:
    client.logo.base_path: '%kernel.project_dir%/var/logo/' # if you want to keep the pdf logo in this location
    component_thumbnail_path: 'images/optionclassifications' # if you want to keep component images in this location
    image_gallery: # if your webroot is /web (old symfony project structure)
        filesystem_path: '%kernel.project_dir%/web/images/imagegallery' 
        url_path: "/images/imagegallery" 
        thumb_size:
            width: 150
            height: 120
    mediaservice.base_path: '%kernel.project_dir%/web' # if your webroot is /web (old symfony project structure)
    configuration.code.length:
        user: 8
        designtemplate: 7
        partslist: 6
        defaultoptions: 9
        inspiration: 10
        cart: 11
        mail: 11
        share: 11
        languageswitch: 11
        channelswitch: 11
        print: 11
```

Security
--------
In configuratorware 1.15 we moved the security config into the bundle. Symfony does not allow to have multiple definitions for `security.firewalls`
and `security.access_control`. If you haven't added custom `providers` or changed the `encoders` you can replace the security config with:

```yaml
security: ~
```