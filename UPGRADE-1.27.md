# UPGRADE FROM 1.26 to 1.27
If you want to update from a lower version than 1.26 to 1.27, please read previous UPGRADE notes.

## INCOMPATIBILITY RULES
If one of your items use the ruletype "direct incompatibilty" be aware of the following:
Due to a bug in the data structures for the rule "direct incompatibility" in prior versions, it is necessary to delete all entries in the table `rule` of this ruletype. This will happen via a migration automatically and you will have to re-add those rules in the adminarea after the update again. To check what was deleted, use the following sql statement:  ``SELECT * FROM `rule` WHERE `rule`.`ruletype_id` = (SELECT `id` from `ruletype` WHERE `identifier` = 'optionexclusion') ORDER BY `rule`.`date_deleted` DESC` ``.
