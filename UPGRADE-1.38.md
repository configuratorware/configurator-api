# UPGRADE FROM 1.35 to 1.38
If you want to update from a lower version than 1.35 to 1.38, please read previous UPGRADE notes.

## Frontend: Creator Layout Updated

The layout of the creator screen was updated, to make more use of the available screen space for the visualization and component list.
If you're using a custom `CreatorAlignTarget`, you should test manually, if your layout is affected / needs to be adjusted.
