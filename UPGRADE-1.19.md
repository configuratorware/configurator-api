UPGRADE FROM 1.18 to 1.19
=========================
If you want to update from a lower version than 1.18 to 1.19, please read previous UPGRADE notes.

Remove Sensiolabs Security Checker from composer.json
-----------------------------------------------------
This service shuts down, please remove it.
Please replace the following section in your composer.json:

```json
    "scripts": {
        "auto-scripts": {
            "cache:clear": "symfony-cmd",
            "assets:install %PUBLIC_DIR%": "symfony-cmd",
            "security-checker security:check": "script"
        },
```

with:
```json
    "scripts": {
        "auto-scripts": {
            "cache:clear": "symfony-cmd",
            "assets:install %PUBLIC_DIR%": "symfony-cmd"
        },
```

PHP 7.4 support
---------------
Configuratorware 1.19 is tested with PHP 7.4.

New Commands:
-------------
- a new Command `configuratorware:refresh-browsercache-hash` was introduced to conveniently update the `?version=abcd` in index.html files

Deprecations
------------

- Support for PHP 7.2 and PHP 7.3 is deprecated from now on. Update to PHP 7.4.