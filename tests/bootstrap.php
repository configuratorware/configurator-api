<?php

use Symfony\Component\Dotenv\Dotenv;

require dirname(__DIR__) . '/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__) . '/.env');

Phake\Annotation\MockInitializer::setDefaultReader(new Phake\Annotation\LegacyReader());
