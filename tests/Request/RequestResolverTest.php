<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Request;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Exception\ClassDoesNotExist;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidRequest;
use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

final class RequestResolverTest extends TestCase
{
    /**
     * @var RequestResolver
     */
    private $requestResolver;

    protected function setUp(): void
    {
        vfsStream::setup();

        $this->requestResolver = new RequestResolver(new StructureFromDataConverter());
    }

    /**
     * @param object $expectedStructure
     * @param string $givenRequestContent
     * @param string $givenStructureClassName
     *
     * @dataProvider providerShouldResolveToStructure
     */
    public function testShouldResolveToStructure(object $expectedStructure, string $givenRequestContent, string $givenStructureClassName): void
    {
        $request = new Request([], [], [], [], [], [], $givenRequestContent);

        $structure = $this->requestResolver->resolveToStructure($request, $givenStructureClassName);

        self::assertEquals($expectedStructure->id, $structure->id);
        self::assertEquals($expectedStructure->identifier, $structure->identifier);
    }

    public function providerShouldResolveToStructure(): \Generator
    {
        yield [new StructureWithPublicProperties(1, 'sku'), '{"id":1, "identifier": "sku"}', StructureWithPublicProperties::class];
    }

    public function testCannotResolveToStructureShouldThrowClassDoesNotExist(): void
    {
        $this->expectException(ClassDoesNotExist::class);

        $request = new Request([], [], [], [], [], [], '{"id":1, "identifier": "sku"}');

        $this->requestResolver->resolveToStructure($request, 'App\IDoNotExist');
    }

    public function testCannotResolveToStructureShouldThrowInvalidRequest(): void
    {
        $this->expectException(InvalidRequest::class);

        $this->requestResolver->resolveToStructure(new Request(), StructureWithPublicProperties::class);
    }

    public function testShouldResolveAttributeValue(): void
    {
        $key = 'attr_key';
        $value = 'attr_value';

        $request = new Request([], [], [$key => $value]);

        self::assertEquals($value, $this->requestResolver->resolveAttributeValue($request, $key));
    }

    public function testCannotResolveAttributeValue(): void
    {
        $this->expectException(InvalidRequest::class);

        $this->requestResolver->resolveAttributeValue(new Request(), 'any_key');
    }

    public function testShouldResolveUploadedFile(): void
    {
        $root = vfsStream::create(['/tmp' => ['PWQJWQ' => 'I am a test file']]);

        $key = 'file';
        $uploadedFile = new UploadedFile($root->url() . '/tmp/PWQJWQ', 'filename.png', null, null, true);

        $request = new Request([], [], [], [], [$key => $uploadedFile]);

        self::assertEquals($uploadedFile, $this->requestResolver->resolveUploadedFile($request, $key));
    }

    public function testCannotResolveUploadedFile(): void
    {
        $this->expectException(InvalidRequest::class);

        $this->requestResolver->resolveUploadedFile(new Request(), 'any_key');
    }
}

final class StructureWithPublicProperties
{
    public $id;

    public $identifier;

    public function __construct($id = null, $identifier = null)
    {
        $this->id = $id;
        $this->identifier = $identifier;
    }
}
