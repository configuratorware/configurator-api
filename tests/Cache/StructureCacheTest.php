<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Cache;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Cache\StructureCache;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

class StructureCacheTest extends TestCase
{
    use EntityTestTrait;

    /**
     * @var StructureCache
     */
    private $cache;

    public function setUp(): void
    {
        $this->cache = new StructureCache();
        parent::setUp();
    }

    public function testShouldCacheItems(): void
    {
        $entityId = 123;
        $entityToCache = new Attribute();
        $this->setPrivateId($entityToCache, $entityId);

        $toStore = ['some' => 'things', 'to' => 'store'];

        $this->cache->store($entityToCache, $toStore);

        $cached = $this->cache->findStructureForEntity($entityToCache);

        self::assertSame($toStore, $cached);
    }

    public function testShouldFindNUllWhenItemIsNotCached(): void
    {
        $entityId = 123;
        $entityToCache = new Attribute();
        $this->setPrivateId($entityToCache, $entityId);
        $actual = $this->cache->findStructureForEntity($entityToCache);

        self::assertNull($actual);
    }

    public function testShouldNotUseStructureCacheIfDisabled(): void
    {
        $this->cache = new StructureCache(false);

        $entityId = 123;
        $entityToCache = new Attribute();
        $this->setPrivateId($entityToCache, $entityId);
        $toStore = ['some' => 'things', 'to' => 'store'];

        $this->cache->store($entityToCache, $toStore);
        $actual = $this->cache->findStructureForEntity($entityToCache);

        self::assertNull($actual);
    }
}
