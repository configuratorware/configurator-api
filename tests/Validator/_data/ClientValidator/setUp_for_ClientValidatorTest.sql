INSERT INTO client
    (id, identifier, email, theme,  date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, custom_css)
VALUES
    (101, 'configuratorware1', 'client1@configuratorware.de', '{"highlightColor":"ff0000"}', NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, ''),
    (102, 'configuratorware2', 'client2@configuratorware.de', '{"highlightColor":"ff0000"}', NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, '');

INSERT INTO user
    (id, externalid, username, firstname, lastname, email, is_active, api_key, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, password)
VALUES
    (101, NULL, NULL, NULL, NULL, 'client1@configuratorware.de', 1, NULL, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 'somepassword'),
    (102, NULL, NULL, NULL, NULL, 'client2@configuratorware.de', 1, NULL, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 'somepassword');

INSERT INTO client_user
    (id, client_id, user_id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES
    (101, 101, 101, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL),
    (102, 102, 102, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL);
