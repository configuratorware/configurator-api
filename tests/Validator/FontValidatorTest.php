<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Validator;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\FontValidator;

class FontValidatorTest extends TestCase
{
    /**
     * @var FontRepository|\Phake_IMock
     * @Mock FontRepository
     */
    private $fontRepository;

    /**
     * @var FontStructure|\Phake_IMock
     * @Mock FontStructure
     */
    private $fontStructure;

    /**
     * @var FontValidator
     */
    private $fontValidator;

    /**
     * @var StructureValidator|\Phake_IMock
     * @Mock StructureValidator
     */
    private $structureValidator;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        \Phake::when($this->structureValidator)->validate()->thenReturn([]);

        $this->fontValidator = new FontValidator($this->fontRepository, $this->structureValidator);
    }

    /**
     * @param int $id
     * @param string $name
     * @param bool $active
     * @param string $fileName
     *
     * @throws \Exception
     *
     * @dataProvider providerShouldPassValidation
     */
    public function testShouldPassValidation(
        int $id,
        string $name,
        bool $active,
        string $fileName
    ) {
        $structure = $this->populateStructure($this->fontStructure, $id, $name, $active, $fileName);

        \Phake::when($this->fontRepository)->findOneBy(['name' => $structure->name])->thenReturn($this->getRepository($structure->name));

        $this->fontValidator->validate($structure);
        $this->addToAssertionCount(1);
    }

    /**
     * @param int $id
     * @param string $name
     * @param bool $active
     * @param string $fileName
     *
     * @throws \Exception
     *
     * @dataProvider providerShouldThrowValidationException
     */
    public function testShouldThrowValidationException(
        int $id,
        string $name,
        bool $active,
        string $fileName
    ) {
        $this->expectException(ValidationException::class);

        $structure = $this->populateStructure($this->fontStructure, $id, $name, $active, $fileName);

        \Phake::when($this->fontRepository)->findOneBy(['name' => $structure->name])->thenReturn($this->getRepository($structure->name));

        $this->fontValidator->validate($structure);
    }

    private function getRepository($name)
    {
        if ('OpenSans' === $name) {
            $font = \Phake::mock(Font::class);
            \Phake::when($font)->getId()->thenReturn(1);

            return $font;
        }
    }

    public function providerShouldPassValidation()
    {
        return [
            [0, 'FontName', true, 'FontName_Regular.ttf'],
            [1, 'OpenSans', true, 'OpenSans.ttf'],
        ];
    }

    public function providerShouldThrowValidationException()
    {
        return [
            [0, 'Arial', true, 'Missing_Font_Name.ttf'],
            [0, 'OpenSans', true, 'Existing_Font.ttf'],
            [2, 'OpenSans', true, 'OpenSans.ttf'],
        ];
    }

    private function populateStructure($structure, $id, $name, $active, $fileName)
    {
        $structure->id = $id;
        $structure->name = $name;
        $structure->active = $active;
        $structure->fileNameRegular = $fileName;
        $structure->fileNameBold = $fileName;
        $structure->fileNameItalic = $fileName;
        $structure->fileNameBoldItalic = $fileName;

        return $structure;
    }
}
