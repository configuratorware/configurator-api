<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Validator;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ApiUser;
use Redhotmagma\ConfiguratorApiBundle\Validator\ApiUserValidator;

class ApiUserValidatorTest extends TestCase
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ApiUserValidator
     */
    private $apiUserValidator;

    public function setUp(): void
    {
        $this->structureValidator = $this->getMockBuilder(StructureValidator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->userRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->apiUserValidator = new ApiUserValidator($this->userRepository);
    }

    public function testUserIsValid(): void
    {
        $this->structureValidator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $user = new ApiUser();
        $user->email = 'test@redhotmagma.de';
        $user->username = 'test';

        $this->apiUserValidator->validate($user);

        // the validation returns nothing, so there is no error when there is nothing returned
        self::assertNull(null);
    }

    public function testUserIsDuplicate(): void
    {
        $userEntity = new \Redhotmagma\ConfiguratorApiBundle\Entity\User();

        $this->structureValidator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userEntity);

        $user = new ApiUser();
        $user->email = 'admin@redhotmagma.de';
        $user->username = 'admin';

        $this->expectException(ValidationException::class);

        $this->apiUserValidator->validate($user);
    }

    public function testApiUserShouldHaveUsername(): void
    {
        $userEntity = new \Redhotmagma\ConfiguratorApiBundle\Entity\User();

        $this->structureValidator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userEntity);

        $user = new ApiUser();
        $user->api_key = 'test_key';

        $this->expectException(ValidationException::class);

        $this->apiUserValidator->validate($user);
    }

    public function testApiUserOnlyUsernameAndKey(): void
    {
        $this->structureValidator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $user = new ApiUser();
        $user->api_key = 'test_key';
        $user->username = 'test';

        $this->apiUserValidator->validate($user);

        // the validation returns nothing, so there is no error when there is nothing returned
        self::assertNull(null);
    }

    public function testApiUserDuplicateKey(): void
    {
        $this->structureValidator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturnCallback(
                static function ($param) {
                    return $param === ['api_key' => 'test_key'] ?
                        new \Redhotmagma\ConfiguratorApiBundle\Entity\User() : null;
                }
            );

        $user = new ApiUser();
        $user->api_key = 'test_key';
        $user->username = 'test';

        $this->expectException(ValidationException::class);

        $this->apiUserValidator->validate($user);
    }
}
