<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Validator;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileValidatorTest extends TestCase
{
    /**
     * @var UploadedFile|\Phake_IMock
     * @Mock UploadedFile
     */
    private $uploadedFile;

    /**
     * @var FileValidator
     */
    private $fileValidator;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        \Phake::when($this->uploadedFile)->getClientOriginalName()->thenReturn('randomstring');

        $this->fileValidator = new FileValidator();
    }

    /**
     * @param string $mimeType
     * @param int $size
     * @param array $allowedMimeTypes
     * @param string $regExStringForCharacterCheckOnFileName
     * @dataProvider providerShouldPassValidation
     */
    public function testShouldPassValidation(
        string $mimeType,
        int $size,
        array $allowedMimeTypes,
        string $regExStringForCharacterCheckOnFileName
    ) {
        \Phake::when($this->uploadedFile)->getMimeType()->thenReturn($mimeType);
        \Phake::when($this->uploadedFile)->getSize()->thenReturn($size);

        $this->fileValidator->validate(
            $this->uploadedFile,
            $allowedMimeTypes,
            $regExStringForCharacterCheckOnFileName
        );
        $this->addToAssertionCount(1);
    }

    /**
     * @param string $mimeType
     * @param int $size
     * @param array $allowedMimeTypes
     * @param string $regExStringForCharacterCheckOnFileName
     * @dataProvider providerShouldThrowValidationException
     */
    public function testShouldThrowValidationException(
        string $mimeType,
        int $size,
        array $allowedMimeTypes,
        string $regExStringForCharacterCheckOnFileName
    ) {
        $this->expectException(ValidationException::class);

        \Phake::when($this->uploadedFile)->getMimeType()->thenReturn($mimeType);
        \Phake::when($this->uploadedFile)->getSize()->thenReturn($size);

        $this->fileValidator->validate(
            $this->uploadedFile,
            $allowedMimeTypes,
            $regExStringForCharacterCheckOnFileName
        );
    }

    public function providerShouldPassValidation()
    {
        return [
            ['image/png', 1, ['image/svg+xml', 'image/png'], ''],
        ];
    }

    public function providerShouldThrowValidationException()
    {
        return [
            // disallowed mimeType
            ['application/octet-stream', 1, ['image/svg+xml', 'image/png'], ''],
            // empty file
            ['image/png', 0, ['image/svg+xml', 'image/png'], ''],
            // no mimeTypes allowed
            ['image/png', 1, [], ''],
            // /randomstring/ is not allowed as filename
            ['image/png', 1, ['image/svg+xml', 'image/png'], '/randomstring/'],
        ];
    }
}
