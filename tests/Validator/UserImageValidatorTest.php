<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Validator;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\ImageConversionException;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageDataProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImagickFactory;
use Redhotmagma\ConfiguratorApiBundle\Validator\UserImageValidator;
use Redhotmagma\ConfiguratorApiBundle\Vector\LegacyInkscapeConverter;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserImageValidatorTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var UserImageValidator
     */
    private $userImageValidator;

    protected function setUp(): void
    {
        parent::setUp();

        $this->root = vfsStream::setup();
        $this->fileSystem = new Filesystem();
        $this->userImageValidator = new UserImageValidator(new ImageDataProvider(new ImagickFactory($this->fileSystem, new LegacyInkscapeConverter($this->fileSystem))));
    }

    /**
     * @param string $fileName
     * @param string $mimeType
     * @param int $clientSize
     *
     * @throws \Exception
     * @dataProvider providerShouldPassValidation
     */
    public function testShouldPassValidation(string $fileName, string $mimeType): void
    {
        $filePath = $this->root->url() . '/' . $fileName;
        $this->fileSystem->dumpFile($filePath, file_get_contents(__DIR__ . '/_data/UserImageValidator/test.png'));

        $uploadedFile = new UploadedFile($filePath, $fileName, $mimeType);

        $this->userImageValidator->validate($uploadedFile);
        $this->addToAssertionCount(1);
    }

    public function providerShouldPassValidation(): array
    {
        return [
            ['test.png', 'image/png'],
            ['test.eps', 'application/octet-stream'],
        ];
    }

    /**
     * @param string $fileName
     * @param string $mimeType
     * @param int $clientSize
     *
     * @throws \Exception
     * @dataProvider providerShouldThrowValidationException
     */
    public function testShouldThrowValidationException(string $fileName, string $mimeType): void
    {
        $this->expectException(ValidationException::class);

        $filePath = $this->root->url() . '/' . $fileName;
        $this->fileSystem->dumpFile($filePath, file_get_contents(__DIR__ . '/_data/UserImageValidator/' . $fileName));

        $uploadedFile = new UploadedFile($filePath, $fileName, $mimeType);

        $this->userImageValidator->validate($uploadedFile);
    }

    public function providerShouldThrowValidationException(): array
    {
        return [
            ['empty.png', 'image/png'],
            ['test.txt', 'application/txt'],
        ];
    }

    /**
     * @param string $fileName
     * @param string $mimeType
     * @param int $clientSize
     *
     * @throws \Exception
     * @dataProvider providerShouldThrowImageConversionException
     */
    public function testShouldThrowImageConversionException(string $fileName, string $mimeType): void
    {
        $this->expectException(ImageConversionException::class);

        $filePath = $this->root->url() . '/' . $fileName;
        $this->fileSystem->dumpFile($filePath, '!<arch>debian-binary15495377370');

        $uploadedFile = new UploadedFile($filePath, $fileName, $mimeType);

        $this->userImageValidator->validate($uploadedFile);
    }

    public function providerShouldThrowImageConversionException(): array
    {
        return [
            ['test.eps', 'application/octet-stream'],
        ];
    }
}
