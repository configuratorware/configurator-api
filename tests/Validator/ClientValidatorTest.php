<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientUserCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;
use Redhotmagma\ConfiguratorApiBundle\Validator\ClientValidator;
use Symfony\Bundle\SecurityBundle\Security;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @BackupDatabase
 */
class ClientValidatorTest extends ApiTestCase
{
    /**
     * @var ClientValidator
     */
    private $clientValidator;

    /**
     * @var StructureValidator|\Phake_IMock
     * @Mock StructureValidator
     */
    private $structureValidator;

    /**
     * @var Security|\Phake_IMock
     * @Mock Security
     */
    private $security;

    protected function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        \Phake::when($this->structureValidator)->validate()->thenReturn([]);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ClientValidator/setUp_for_ClientValidatorTest.sql'));

        $clientRepository = $this->em->getRepository(Client::class);
        $userRepository = $this->em->getRepository(\Redhotmagma\ConfiguratorApiBundle\Entity\User::class);

        $clientUserCheck = new ClientUserCheck($this->security, $clientRepository, $userRepository);

        $this->clientValidator = new ClientValidator(
            $this->structureValidator,
            $clientRepository,
            $clientUserCheck
        );
    }

    /**
     * @throws \Exception
     */
    public function testShouldPassValidation(): void
    {
        $user = new User();
        $user->email = 'client1@configuratorware.de';

        $client = new \Redhotmagma\ConfiguratorApiBundle\Structure\Client();
        $client->id = 101;
        $client->users = [$user];

        $this->clientValidator->validate($client);
        $this->addToAssertionCount(1);
    }

    /**
     * @throws \Exception
     */
    public function testShouldThrowValidationException(): void
    {
        $this->expectException(ValidationException::class);

        $user = new User();
        $user->email = 'client1@configuratorware.de';

        $client = new \Redhotmagma\ConfiguratorApiBundle\Structure\Client();
        $client->id = 102;
        $client->users = [$user];

        $this->clientValidator->validate($client);
    }
}
