<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer;

use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\StoppableEventInterface;
use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\OfferRequestCustomerEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\ReceiveOfferRequestEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client as ClientDTO;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ZipUrlResolver;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\ReceiveOfferApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\ShareUrlResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ReceiveOfferApiTest extends TestCase
{
    private ReceiveOfferApi $receiveOfferApi;

    /**
     * @var Client|\Phake_IMock
     * @Mock Client
     */
    private Client $client;

    /**
     * @var ConfigurationRepository|\Phake_IMock
     * @Mock ConfigurationRepository
     */
    private ConfigurationRepository $configurationRepository;

    /**
     * @var EventDispatcherInterface|\Phake_IMock
     * @Mock EventDispatcherInterface
     */
    private EventDispatcherInterface $eventDispatcher;

    /**
     * @var ShareUrlResolver|\Phake_IMock
     * @Mock ShareUrlResolver
     */
    private ShareUrlResolver $shareUrl;

    /**
     * @var ZipUrlResolver|\Phake_IMock
     * @Mock ZipUrlResolver
     */
    private ZipUrlResolver $productionZip;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        \Phake::when($this->configurationRepository)->findOneByCode('CODE')->thenReturn($this->createConfiguration());
        \Phake::when($this->shareUrl)->getShareUrl('CODE', \Phake::ignoreRemaining())->thenReturn('SHARE_URL');
        \Phake::when($this->eventDispatcher)->dispatch->thenReturn(\Phake::mock(StoppableEventInterface::class));

        $this->receiveOfferApi = new ReceiveOfferApi(
            $this->client,
            $this->configurationRepository,
            $this->eventDispatcher,
            $this->shareUrl,
            new NullLogger(),
            $this->productionZip
        );
    }

    public function testShouldProcessOfferRequest(): void
    {
        \Phake::when($this->client)->getClient()->thenReturn(ClientDTO::fromEntity($this->createClient()));

        $this->receiveOfferApi->request($this->createReceiveOfferRequest(), new ControlParameters());

        \Phake::verify($this->eventDispatcher)->dispatch(
            \Phake::capture($event)->when($this->isInstanceOf(ReceiveOfferRequestEvent::class)),
            'configuratorware.receive_offer.request'
        );
        $this->assertInstanceOf(ReceiveOfferRequestEvent::class, $event);
        \Phake::verify($this->eventDispatcher)->dispatch(\Phake::capture($eventCustomer), 'configuratorware.receive_offer.customer_event');
        $this->assertInstanceOf(OfferRequestCustomerEvent::class, $eventCustomer);
    }

    private function createClient($sendToCustomer = true): ClientEntity
    {
        $client = new ClientEntity();
        $client->setFromEmailAddress('from@test.local');
        $client->setToEmailAddresses('to@test.local');
        $client->setSendOfferRequestMailToCustomer($sendToCustomer);

        return $client;
    }

    private function createConfiguration(): Configuration
    {
        $configuration = new Configuration();
        $configuration->setCode('CODE');
        $item = new Item();
        $item->setConfigurationMode('creator');
        $configuration->setItem($item);

        return $configuration;
    }

    private function createReceiveOfferRequest(): ReceiveOfferRequest
    {
        $structure = new ReceiveOfferRequest();
        $structure->configurationCode = 'CODE';
        $structure->emailAddress = 'customer@local.test';
        $structure->name = 'John Customer';

        return $structure;
    }
}
