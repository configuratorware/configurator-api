<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\EmailNotSendException;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\CustomerOfferEmail;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\ReceiveOfferEmail;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\ReceiveOfferSender;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ReceiveOfferSenderTest extends TestCase
{
    /**
     * @var MailerInterface|\Phake_IMock
     * @Mock MailerInterface
     */
    public MailerInterface $mailer;

    /**
     * @var TranslatorInterface|\Phake_IMock
     * @Mock TranslatorInterface
     */
    public TranslatorInterface $translator;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        \Phake::when($this->translator)->trans('This is a subject', ['{{code}}' => 'XYZ'], '')->thenReturn('This is a subject');
        \Phake::when($this->translator)->trans('This is a subject', ['%{code}' => 'CODE'], '')->thenReturn('This is a subject');
    }

    public function testShouldProvideEmailFromConfig(): void
    {
        $givenDto = self::generateDto('This is a subject', '', ['service@configuratorware.de'], [], []);
        $sender = new ReceiveOfferSender('info@configuratorware.de', $this->mailer, $this->translator);

        $expectedEmail = (new TemplatedEmail())
            ->from('info@configuratorware.de')
            ->to('info@configuratorware.de')
            ->cc()
            ->bcc()
            ->subject('This is a subject')
            ->textTemplate('@RedhotmagmaConfiguratorApi/receive-offer/client.txt.twig')
            ->htmlTemplate('@RedhotmagmaConfiguratorApi/receive-offer/client.html.twig')
            ->context(['dto' => $givenDto]);

        $sender->send($givenDto);
        \Phake::verify($this->mailer)->send($expectedEmail);
    }

    public function testShouldProvideEmailFromConfigForCustomer(): void
    {
        $givenDto = self::generateCustomerDto('This is a subject', '', 'customer@configuratorware.de');
        $sender = new ReceiveOfferSender('info@configuratorware.de', $this->mailer, $this->translator);

        $expectedEmail = (new TemplatedEmail())
            ->from('info@configuratorware.de')
            ->to('customer@configuratorware.de')
            ->subject('This is a subject')
            ->textTemplate('@RedhotmagmaConfiguratorApi/receive-offer/customer.txt.twig')
            ->htmlTemplate('@RedhotmagmaConfiguratorApi/receive-offer/customer.html.twig')
            ->context(['dto' => $givenDto]);

        $sender->sendCustomerEmail($givenDto);
        \Phake::verify($this->mailer)->send($expectedEmail);
    }

    public function testDoesNotHaveValidFields(): void
    {
        $givenDto = self::generateDto('This is a subject', 'service@configuratorware.de', [], [], []);
        $sender = new ReceiveOfferSender('', $this->mailer, $this->translator);

        $this->expectException(EmailNotSendException::class);
        $sender->send($givenDto);
    }

    public function testShouldNotFailIfCcAndBccEmpty(): void
    {
        $this->expectNotToPerformAssertions();

        $givenDto = self::generateDto('This is a subject', 'service@configuratorware.de', ['info@configuratorware.de'], [null], ['']);
        $sender = new ReceiveOfferSender('', $this->mailer, $this->translator);

        $sender->send($givenDto);
    }

    public function testShouldTakeToEmailIfFromIsNotGiven(): void
    {
        $givenDto = self::generateDto('This is a subject', null, ['info@configuratorware.de'], [], []);

        $sender = new ReceiveOfferSender('', $this->mailer, $this->translator);

        $expectedEmail = (new TemplatedEmail())
            ->from('info@configuratorware.de')
            ->to('info@configuratorware.de')
            ->cc()
            ->bcc()
            ->subject('This is a subject')
            ->textTemplate('@RedhotmagmaConfiguratorApi/receive-offer/client.txt.twig')
            ->htmlTemplate('@RedhotmagmaConfiguratorApi/receive-offer/client.html.twig')
            ->context(['dto' => $givenDto]);

        $sender->send($givenDto);
        \Phake::verify($this->mailer)->send($expectedEmail);
    }

    public function testHasFullDataFromClient(): void
    {
        $givenDto = self::generateDto('This is a subject', 'info@configuratorware.de', ['service@configuratorware.de', 'info@configuratorware.de'], ['service@configuratorware.de', 'info@configuratorware.de'], ['service@configuratorware.de', 'info@configuratorware.de']);
        $sender = new ReceiveOfferSender('', $this->mailer, $this->translator);

        $expectedEmail = (new TemplatedEmail())
            ->from('info@configuratorware.de')
            ->to('service@configuratorware.de', 'info@configuratorware.de')
            ->cc('service@configuratorware.de', 'info@configuratorware.de')
            ->bcc('service@configuratorware.de', 'info@configuratorware.de')
            ->subject('This is a subject')
            ->textTemplate('@RedhotmagmaConfiguratorApi/receive-offer/client.txt.twig')
            ->htmlTemplate('@RedhotmagmaConfiguratorApi/receive-offer/client.html.twig')
            ->context(['dto' => $givenDto]);

        $sender->send($givenDto);
        \Phake::verify($this->mailer)->send($expectedEmail);
    }

    private static function generateDto(string $subject, ?string $from, array $to, array $cc, array $bcc): ReceiveOfferEmail
    {
        return new ReceiveOfferEmail($subject, $from, $to, $cc, $bcc, 'XYZ', 'customer@example.com', '', '', '', '', '', '', '', '', '', 'www.configuratorware.com/productionzip');
    }

    private static function generateCustomerDto(string $subject, ?string $from, string $to): CustomerOfferEmail
    {
        return new CustomerOfferEmail($subject, $from, 'CODE', $to, 'name', '');
    }
}
