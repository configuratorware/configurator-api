<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation;

use Danmichaelo\Coma\ColorDistance;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;
use Symfony\Component\Filesystem\Filesystem;

class VectorizeUnitTest extends TestCase
{
    /**
     * @var Filesystem|\Phake_IMock
     * @Mock Filesystem
     */
    protected $fileSystem;

    /**
     * @var ColorDistance|\Phake_IMock
     * @Mock ColorDistance
     */
    protected $colorDistance;

    /**
     * @var ProcessFactory|\Phake_IMock
     * @Mock ProcessFactory
     */
    protected $processFactory;

    /**
     * @var Vectorize
     */
    protected $vectorizeOperation;

    protected static $threshold = 4;

    protected function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->colorDistance = \Phake::mock(ColorDistance::class);
        \Phake::when($this->colorDistance)->cie76->thenReturn(2);

        $this->vectorizeOperation = new Vectorize(
            $this->fileSystem,
            $this->processFactory,
            $this->colorDistance
        );
    }

    /**
     * Test color reduction.
     *
     * @throws \ReflectionException
     */
    public function testColorThreshold()
    {
        $class = new \ReflectionClass($this->vectorizeOperation);
        $method = $class->getMethod('getTargetColors');
        $method->setAccessible(true);
        $colorResult = $method->invokeArgs($this->vectorizeOperation, [$this->getTestColors(), self::$threshold]);
        self::assertCount(self::$threshold, $colorResult);
    }

    /**
     * Test if paths are filteres.
     *
     * @throws \ReflectionException
     */
    public function removeEmptyNodes()
    {
        $class = new \ReflectionClass($this->vectorizeOperation);
        $method = $class->getMethod('extractPathFromFile');
        $method->setAccessible(true);

        // sztart node count
        $dom = new \DOMDocument();
        $dom->loadXML($this->getTestSvgString());
        self::assertCount(6, $dom->childNodes->count());

        $filteredSvg = $method->invokeArgs($this->vectorizeOperation, [$this->getTestSvgString()]);

        // filteres node count
        $dom = new \DOMDocument();
        $dom->loadXML($filteredSvg);
        self::assertCount(3, $dom->childNodes->count());
    }

    /**
     * @return array Test colors mock
     */
    protected function getTestColors(): array
    {
        $colors = [];
        for ($i = 0; $i < 10; ++$i) {
            $colors[] = [
                'color' => ['r' => $i * 10, 'g' => $i * 10, 'b' => $i * 10, 'a' => 1],
            ];
        }

        return $colors;
    }

    /**
     * @return string
     */
    protected function getTestSvgString(): string
    {
        return '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <g transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#ffffff" stroke="none">
                <path d="M0 6000 l0 -6000 6000 0 6000 0 0 6000 0 6000 -6000 0 -6000 0 0 -6000z m6192 4977 c-2 -9 -38 -75 -81 -145 -458 -748 -722 -1464 -782 -2121 -16 -184 -7 -536 19 -706 79 -510 262 -967 608 -1517 148 -236 525 -768 544 -768 26 0 352 115 465 165 486 211 868 519 1187 956 274 376 521 914 677 1479 18 63 39 139 47 168 39 140 87 -16 113 -368 60 -806 -31 -2015 -220 -2929 -17 -79 -29 -145 -27 -146 5 -5 201 128 293 198 281 213 557 488 825 822 100 126 118 145 131 145 48 1 -41 -314 -196 -700 -116 -287 -205 -481 -371 -810 -840 -1666 -1940 -2902 -3023 -3396 -331 -150 -609 -227 -1016 -281 -194 -25 -648 -25 -815 0 -748 114 -1378 490 -1833 1094 -358 474 -612 1119 -707 1793 -149 1048 182 2271 966 3577 588 979 1450 2036 2324 2848 354 329 702 610 812 655 35 14 64 8 60 -13z"/>
            </g>
            <g transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#e3051c" stroke="none">
                <path d=""/>
            </g>
            <g transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#ed5c6b" stroke="none">
                <path d=""/>
            </g>
            <g transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#f07681" stroke="none">
                <path d=""/>
            </g>
            <g transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#e93649" stroke="none">
                <path d="M6465 5710 c-35 -13 -341 -80 -505 -110 -85 -16 -324 -54 -530 -85 -560 -84 -789 -129 -1025 -200 -351 -105 -627 -252 -842 -447 -374 -340 -671 -778 -818 -1208 -230 -673 -107 -1401 316 -1879 274 -309 727 -567 1214 -691 478 -122 966 -110 1476 36 430 122 915 372 1269 652 138 109 430 406 554 562 513 647 912 1571 1151 2666 4 18 -22 8 -168 -66 -206 -103 -379 -169 -571 -215 -214 -52 -328 -66 -573 -72 l-222 -6 -16 29 c-164 299 -297 507 -530 829 -75 105 -142 195 -148 202 -7 6 -20 8 -32 3z"/>
            </g>
            <g transform="translate(0.000000,1200.000000) scale(0.100000,-0.100000)" fill="#f28e98" stroke="none">
                <path d="M6132 10990 c-69 -29 -300 -202 -511 -383 -645 -556 -1397 -1373 -1961 -2132 -1255 -1686 -1816 -3255 -1630 -4565 65 -461 218 -948 419 -1335 171 -330 394 -625 643 -852 86 -78 208 -178 208 -170 0 2 -37 35 -81 73 -543 461 -733 1276 -474 2034 147 430 444 868 818 1208 215 195 491 342 842 447 236 71 465 116 1025 200 206 31 445 69 530 85 166 30 470 97 507 111 21 8 34 -8 155 -174 241 -331 385 -555 553 -861 l16 -29 222 6 c245 6 359 20 573 72 192 46 365 112 571 215 146 74 172 84 168 66 -239 -1095 -638 -2019 -1151 -2666 -124 -156 -416 -453 -554 -562 -204 -161 -430 -301 -701 -432 -417 -202 -813 -308 -1269 -340 -30 -2 3 -2 74 1 235 7 528 50 754 109 1159 302 2311 1386 3283 3089 127 223 401 764 499 985 198 447 340 855 340 976 0 69 -9 63 -114 -69 -324 -410 -665 -736 -1018 -974 -67 -45 -124 -81 -126 -79 -2 2 8 57 22 122 191 905 285 2143 225 2954 -11 153 -33 313 -51 372 -11 36 -31 58 -44 46 -2 -3 -27 -86 -55 -184 -362 -1289 -954 -2069 -1874 -2469 -113 -50 -439 -165 -465 -165 -19 0 -396 532 -544 768 -380 604 -570 1112 -626 1671 -13 134 -13 425 1 571 59 639 328 1364 780 2102 43 70 79 136 81 145 4 21 -25 27 -60 13z M3310 1546 c0 -2 8 -10 18 -17 15 -13 16 -12 3 4 -13 16 -21 21 -21 13z M3350 1516 c0 -2 8 -10 18 -17 15 -13 16 -12 3 4 -13 16 -21 21 -21 13z M3580 1366 c0 -3 9 -10 20 -16 11 -6 20 -8 20 -6 0 3 -9 10 -20 16 -11 6 -20 8 -20 6z M3704 1296 c11 -9 24 -16 30 -16 12 0 7 5 -24 19 -24 11 -24 11 -6 -3z M4398 1053 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z M4448 1043 c7 -3 16 -2 19 1 4 3 -2 6 -13 5 -11 0 -14 -3 -6 -6z M4508 1033 c6 -2 18 -2 25 0 6 3 1 5 -13 5 -14 0 -19 -2 -12 -5z M4573 1023 c9 -2 23 -2 30 0 6 3 -1 5 -18 5 -16 0 -22 -2 -12 -5z M4658 1013 c12 -2 32 -2 45 0 12 2 2 4 -23 4 -25 0 -35 -2 -22 -4z M4803 1003 c15 -2 39 -2 55 0 15 2 2 4 -28 4 -30 0 -43 -2 -27 -4z"/>
            </g>
            </svg>';
    }
}
