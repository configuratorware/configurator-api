<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation;

use Danmichaelo\Coma\ColorDistance;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageConverterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImagickFactory;
use Redhotmagma\ConfiguratorApiBundle\Vector\LegacyInkscapeConverter;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class VectorizeFileColorTest extends ApiTestCase
{
    /**
     * @var Vectorize
     */
    protected $vectorizeOperation;

    /**
     * @var ImageModel|\Phake_IMock
     * @Mock ImageModel
     */
    protected $imageModelGoodQualitySample;

    /**
     * @var ImageModel|\Phake_IMock
     * @Mock ImageModel
     */
    protected $imageModelArtifactSampleJpeg;

    /**
     * @var ImageModel|\Phake_IMock
     * @Mock ImageModel
     */
    protected $imageModelArtifactSamplePng;

    /**
     * @var ColorDistance|\Phake_IMock
     * @Mock ColorDistance
     */
    protected $colorDistance;

    /**
     * @var ImageConverterService
     */
    protected $imageConverterService;

    /**
     * @var ImagickFactory
     */
    protected $imagickFactory;

    protected function setUp(): void
    {
        parent::setUp();
        static::bootKernel();

        \Phake::initAnnotations($this);
        \Phake::when($this->colorDistance)->cie76->thenReturn(0);
        \Phake::when($this->imageModelGoodQualitySample)->getPath->thenReturn(static::SAMPLE_PNG_PATH);
        \Phake::when($this->imageModelArtifactSampleJpeg)->getPath->thenReturn(static::ARTIFACT_JPEG_PATH);
        \Phake::when($this->imageModelArtifactSamplePng)->getPath->thenReturn(static::ARTIFACT_PNG_PATH);

        $filesystem = new Filesystem();

        $this->imagickFactory = new ImagickFactory($filesystem, new LegacyInkscapeConverter($filesystem));
        $this->imageConverterService = new ImageConverterService($this->imagickFactory, $filesystem);

        $this->vectorizeOperation = new Vectorize(
            $filesystem,
            new ProcessFactory(),
            static::$kernel->getContainer()->get('Danmichaelo\Coma\ColorDistance')
        );
    }

    public static function tearDownAfterClass(): void
    {
        $generatedFilePaths = [
            static::SAMPLE_RESULT_SVG_PATH,
            static::ARTIFACT_PNG_PATH,
            static::ARTIFACT_RESULT_PATH,
        ];
        foreach ($generatedFilePaths as $path) {
            if (file_exists($path)) {
                unlink($path);
            }
        }
    }

    /**
     * Mock config parameter.
     *
     * @return \StdClass
     */
    protected function getOperationConfig(): \stdClass
    {
        $config = new \stdClass();
        $config->threshold = static::COLOR_THRESHOLD;

        return $config;
    }

    /**
     * Test creation of vector file.
     */
    public function testVectorFileCreation()
    {
        $result = $this->vectorizeOperation->process($this->getOperationConfig(), $this->getGoodQualitySampleSource());
        self::assertEquals(static::SAMPLE_RESULT_SVG_PATH, $result['preview']->getPath());
        self::assertFileExists(static::SAMPLE_RESULT_SVG_PATH);
    }

    /**
     * @depends testVectorFileCreation
     */
    public function testSvgFileContent()
    {
        $svgContent = file_get_contents(static::SAMPLE_RESULT_SVG_PATH);
        self::assertNotEmpty($svgContent);
        $xml = simplexml_load_string($svgContent); /* @var $xml \SimpleXMLElement */
        self::assertEquals(10, $xml->count());
    }

    /**
     * Test if a JPEG file with conversion artifacts gets converted correctly.
     */
    public function testCompressedJpegConversion()
    {
        $artifactSource = $this->getArtifactSampleSource();

        // convert JPG to PNG to simulate upload
        $this->imageConverterService->scaleDown(
            $artifactSource['original']->getPath(),
            $artifactSource['preview']->getPath(),
            480,
            320,
            'png'
        );
        self::assertFileExists(static::ARTIFACT_PNG_PATH);

        $config = $this->getOperationConfig();
        $config->threshold = null;

        // vectorize the PNG
        $this->vectorizeOperation->process(
            $config,
            $this->getArtifactSampleSource()
        );
        self::assertFileExists(static::ARTIFACT_RESULT_PATH);

        // check for expected path number after artifact reduction
        $svgContent = file_get_contents(static::ARTIFACT_RESULT_PATH);
        self::assertNotEmpty($svgContent);
        $xml = simplexml_load_string($svgContent); /* @var $xml \SimpleXMLElement */
        self::assertEquals(6, $xml->count());
    }

    /**
     * Mock source.
     *
     * @return ImageModel[]
     */
    protected function getGoodQualitySampleSource()
    {
        return [
            'original' => $this->imageModelGoodQualitySample,
            'preview' => $this->imageModelGoodQualitySample,
        ];
    }

    /**
     * Mock source.
     *
     * @return ImageModel[]
     */
    protected function getArtifactSampleSource()
    {
        return [
            'original' => $this->imageModelArtifactSampleJpeg,
            'preview' => $this->imageModelArtifactSamplePng,
        ];
    }

    /**
     * @var string
     */
    protected const SAMPLE_RESULT_SVG_PATH = __DIR__ . '/_data/sample.svg';

    /**
     * @var string
     */
    protected const SAMPLE_PNG_PATH = __DIR__ . '/_data/sample.png';

    /**
     * @var string
     */
    protected const ARTIFACT_JPEG_PATH = __DIR__ . '/_data/aruba_flag_with_compression_artifacts.jpg';

    /**
     * @var string
     */
    protected const ARTIFACT_PNG_PATH = __DIR__ . '/_data/aruba_flag_with_compression_artifacts.png';

    /**
     * @var string
     */
    protected const ARTIFACT_RESULT_PATH = __DIR__ . '/_data/aruba_flag_with_compression_artifacts.svg';

    /**
     * @var int Color threshold for vectorizing
     */
    protected const COLOR_THRESHOLD = 10;
}
