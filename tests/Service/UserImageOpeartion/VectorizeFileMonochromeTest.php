<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation;

use Danmichaelo\Coma\ColorDistance;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageConverterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImagickFactory;
use Redhotmagma\ConfiguratorApiBundle\Vector\LegacyInkscapeConverter;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class VectorizeFileMonochromeTest extends ApiTestCase
{
    /**
     * @var Vectorize
     */
    protected $vectorizeOperation;

    /**
     * @var ImageModel|\Phake_IMock
     * @Mock ImageModel
     */
    protected $monochromeImageModel;

    /**
     * @var ImageModel|\Phake_IMock
     * @Mock ImageModel
     */
    protected $imageModelGoodQualitySample;

    /**
     * @var ColorDistance|\Phake_IMock
     * @Mock ColorDistance
     */
    protected $colorDistance;

    /**
     * @var ImageConverterService
     */
    protected $imageConverterService;

    /**
     * @var ImagickFactory
     */
    protected $imagickFactory;

    public static function tearDownAfterClass(): void
    {
        parent::tearDownAfterClass();
        if (file_exists(self::MONOCHROME_RESULT_SVG_PATH)) {
            unlink(self::MONOCHROME_RESULT_SVG_PATH);
        }
    }

    /**
     * Test creation of vector file.
     */
    public function testProblematicMonochromeFileFileCreation()
    {
        $result = $this->vectorizeOperation->process(
            $this->getOperationConfig(),
            $this->getProblematicMonochromeFileSources()
        );
        self::assertEquals(static::MONOCHROME_RESULT_SVG_PATH, $result['preview']->getPath());
        self::assertFileExists(static::MONOCHROME_RESULT_SVG_PATH);
    }

    /**
     * @depends testProblematicMonochromeFileFileCreation
     */
    public function testProblematicMonochromeSvgFileContent()
    {
        $svgContent = file_get_contents(static::MONOCHROME_RESULT_SVG_PATH);
        self::assertNotEmpty($svgContent);
        $xml = simplexml_load_string($svgContent);
        /* @var $xml \SimpleXMLElement */
        self::assertEquals(1, $xml->count());
    }

    /**
     * Test creation of vector file.
     */
    public function testVectorFileCreation()
    {
        $result = $this->vectorizeOperation->process($this->getOperationConfig(), $this->getGoodQualitySampleSource());
        self::assertEquals(static::SAMPLE_RESULT_SVG_PATH, $result['preview']->getPath());
        self::assertFileExists(static::SAMPLE_RESULT_SVG_PATH);
    }

    /**
     * @depends testVectorFileCreation
     */
    public function testSvgFileContent()
    {
        $svgContent = file_get_contents(static::SAMPLE_RESULT_SVG_PATH);
        self::assertNotEmpty($svgContent);
        $xml = simplexml_load_string($svgContent);
        /* @var $xml \SimpleXMLElement */
        self::assertEquals(1, $xml->count());
    }

    protected function setUp(): void
    {
        parent::setUp();
        static::bootKernel();

        \Phake::initAnnotations($this);
        \Phake::when($this->colorDistance)->cie76->thenReturn(0);
        \Phake::when($this->imageModelGoodQualitySample)->getPath->thenReturn(static::SAMPLE_PNG_PATH);
        \Phake::when($this->monochromeImageModel)->getPath->thenReturn(static::MONOCHROME_SAMPLE_PNG_PATH);

        $filesystem = new Filesystem();

        $this->imagickFactory = new ImagickFactory($filesystem, new LegacyInkscapeConverter($filesystem));
        $this->imageConverterService = new ImageConverterService($this->imagickFactory, $filesystem);

        $this->vectorizeOperation = new Vectorize(
            $filesystem,
            new ProcessFactory(),
            static::$kernel->getContainer()->get('Danmichaelo\Coma\ColorDistance')
        );
    }

    /**
     * Mock config parameter.
     *
     * @return \StdClass
     */
    protected function getOperationConfig(): \stdClass
    {
        $config = new \stdClass();
        $config->threshold = static::COLOR_THRESHOLD;
        $config->monochrome = true;

        return $config;
    }

    /**
     * Mock source.
     *
     * @return ImageModel[]
     */
    protected function getGoodQualitySampleSource()
    {
        return [
            'original' => $this->imageModelGoodQualitySample,
            'preview' => $this->imageModelGoodQualitySample,
        ];
    }

    /**
     * @return array
     */
    private function getProblematicMonochromeFileSources()
    {
        return [
            'original' => $this->monochromeImageModel,
            'preview' => $this->monochromeImageModel,
        ];
    }

    /**
     * @var string
     */
    protected const SAMPLE_RESULT_SVG_PATH = __DIR__ . '/_data/sample.svg';

    /**
     * @var string
     */
    protected const SAMPLE_PNG_PATH = __DIR__ . '/_data/sample.png';

    /**
     * @var string
     */
    protected const MONOCHROME_RESULT_SVG_PATH = __DIR__ . '/_data/monochrome_sample.svg';

    /**
     * @var string
     */
    protected const MONOCHROME_SAMPLE_PNG_PATH = __DIR__ . '/_data/monochrome_sample.png';

    /**
     * @var int
     */
    public const COLOR_THRESHOLD = 1;
}
