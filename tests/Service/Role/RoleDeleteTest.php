<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Role;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @BackupDatabase
 */
class RoleDeleteTest extends ApiTestCase
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var Security|\Phake_IMock
     * @Mock Security
     */
    private $security;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleDelete
     */
    private $roleDelete;

    protected function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->roleRepository = $this->em->getRepository(Role::class);
        $this->userRepository = $this->em->getRepository(User::class);
        $this->roleDelete = new RoleDelete($this->roleRepository, $this->security);
    }

    public function testShouldThrowValidationExceptionWhenDeletingAdminRole(): void
    {
        $user = $this->userRepository->findOneBy(['username' => 'admin']);
        \Phake::when($this->security)->getUser()->thenReturn($user);

        $this->expectException(ValidationException::class);

        $this->roleDelete->delete(1);
    }

    public function testShouldThrowValidationExceptionWhenDeletingLastRoleOfUser(): void
    {
        $this->expectException(ValidationException::class);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/testShouldThrowValidationExceptionWhenDeletingLastRoleOfUser_setup.sql'));

        $user = $this->userRepository->findOneBy(['username' => 'test_user']);
        \Phake::when($this->security)->getUser()->thenReturn($user);

        $roleToDelete = $this->roleRepository->findOneBy(['name' => 'test_role']);
        $this->roleDelete->delete($roleToDelete->getId());
    }
}
