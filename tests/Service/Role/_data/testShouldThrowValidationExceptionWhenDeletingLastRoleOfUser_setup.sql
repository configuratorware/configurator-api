INSERT INTO `user` (username, is_active, date_created, date_updated, date_deleted, password)
VALUES ('test_user', 1, NOW(), NULL, '0001-01-01 00:00:00', '');

INSERT INTO `role` (name, role, date_created, date_updated, date_deleted)
VALUES ('test_role', '', NOW(), NULL, '0001-01-01 00:00:00');

INSERT INTO `user_role` (date_created, date_updated, date_deleted, user_id, role_id)
VALUES (NOW(), NULL, '0001-01-01 00:00:00', (SELECT id FROM `user` WHERE `username` = 'test_user'),
        (SELECT id FROM `role` WHERE `name` = 'test_role'));

INSERT INTO `role_credential` (date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                               user_deleted_id, role_id, credential_id)
VALUES (NOW(), NULL, '0001-01-01 00:00:00', 0, 0, 0, (SELECT id FROM `role` WHERE `name` = 'test_role'), 1);

