<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Inspiration;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Entity\InspirationText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationImages;
use Redhotmagma\ConfiguratorApiBundle\Service\Inspiration\FrontendInspirationStructureFromEntity;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

class FrontendInspirationStructureFromEntityTest extends TestCase
{
    use EntityTestTrait;

    /**
     * @var ConfigurationImages|\Phake_IMock
     * @Mock ConfigurationImages
     */
    private $configurationImages;

    /**
     * @var FrontendInspirationStructureFromEntity
     */
    private $structureFromEntity;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->structureFromEntity = new FrontendInspirationStructureFromEntity($this->configurationImages);
    }

    /**
     * @param $state
     * @param string $languageIso
     * @dataProvider providerShouldCreateFrontendInspirationStructureWithEmptyImages
     */
    public function testShouldCreateFrontendInspirationStructureWithEmptyImages($state, string $languageIso): void
    {
        \Phake::when($this->configurationImages)->getPreviewImage->thenReturn(null);

        $givenEntity = $this->generateEntity($state);

        $resultStructure = $this->structureFromEntity->fromEntity($givenEntity, $languageIso);

        self::assertEquals($state['configuration_code'], $resultStructure->configurationCode);
        self::assertNull($resultStructure->description);
        self::assertNull($resultStructure->title);
        self::assertNull($resultStructure->thumbnail);
        self::assertNull($resultStructure->previewImage);
    }

    public function providerShouldCreateFrontendInspirationStructureWithEmptyImages(): \Generator
    {
        yield [['configuration_code' => null, 'thumbnail' => null, 'texts' => []], 'de_DE'];
        yield [['configuration_code' => 'CODE', 'thumbnail' => null, 'texts' => []], 'de_DE'];
    }

    /**
     * @param $state
     * @param string $languageIso
     * @dataProvider providerShouldCreateFrontendInspirationStructureWithImages
     */
    public function testShouldCreateFrontendInspirationStructureWithImages($state, string $languageIso): void
    {
        \Phake::when($this->configurationImages)->getPreviewImage('CODE')->thenReturn('/images/CODE.png');

        $givenEntity = $this->generateEntity($state);

        $resultStructure = $this->structureFromEntity->fromEntity($givenEntity, $languageIso);

        self::assertEquals($state['configuration_code'], $resultStructure->configurationCode);
        self::assertEquals($state['texts']['de_DE']['title'], $resultStructure->title);
        self::assertEquals($state['texts']['de_DE']['description'], $resultStructure->description);
        self::assertEquals($state['thumbnail'], $resultStructure->thumbnail);
        self::assertEquals($state['previewImage'], $resultStructure->previewImage);
    }

    public function providerShouldCreateFrontendInspirationStructureWithImages(): \Generator
    {
        yield [
            [
                'configuration_code' => 'CODE',
                'thumbnail' => '/images/thumbnail.png',
                'previewImage' => '/images/CODE.png',
                'texts' => [
                    'de_DE' => [
                        'title' => 'title_de',
                        'description' => 'description_de', ],
                ],
            ],
           'de_DE',
        ];
    }

    private function generateEntity($state): Inspiration
    {
        $item = new Item();
        $this->setPrivateId($item, 1);

        $inspiration = Inspiration::forItem($item);

        foreach ($state['texts'] as $iso => $text) {
            $language = new Language();
            $language->setIso($iso);

            $inspirationText = InspirationText::forInspiration($inspiration, $language);
            $inspirationText->setTitle($text['title'])->setDescription($text['description']);

            $inspiration->addInspirationText($inspirationText);
        }

        $inspiration->setConfigurationCode($state['configuration_code']);
        $inspiration->setThumbnail($state['thumbnail']);

        return $inspiration;
    }
}
