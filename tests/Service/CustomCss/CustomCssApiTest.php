<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\CustomCss;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\CustomCss\CustomCssApi;

class CustomCssApiTest extends TestCase
{
    /**
     * @var CustomCssApi
     */
    private $customCssApi;

    /**
     * @var ClientRepository|\Phake_IMock
     * @Mock ClientRepository
     */
    private $clientRepository;

    /**
     * @var SettingRepository|\Phake_IMock
     * @Mock SettingRepository
     */
    private $settingRepository;

    public function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->customCssApi = new CustomCssApi($this->clientRepository, $this->settingRepository);
    }

    /**
     * @param string|null $clientIdentfier
     * @param string $expected
     * @dataProvider providerShouldLoadByClient
     */
    public function testShouldLoadByClient(?string $clientIdentfier, string $expected): void
    {
        \Phake::when($this->settingRepository)->findAll()->thenReturn($this->getSettings());

        \Phake::when($this->clientRepository)->findOneBy(['identifier' => '_default'])->thenReturn($this->getClient());

        self::assertEquals($expected, $this->customCssApi->loadByClient($clientIdentfier));
    }

    public function providerShouldLoadByClient()
    {
        return [
            [
                '_default',
                'setting_css
client_css',
            ],
            [
                null,
                'setting_css
client_css',
            ],
            [
                'non_existent_client',
                'setting_css',
            ],
        ];
    }

    private function getSettings(): array
    {
        $setting = new Setting();

        $setting->setCustomCss('setting_css');

        return [$setting];
    }

    private function getClient(): Client
    {
        $client = new Client();

        $client->setCustomCss('client_css');

        return $client;
    }
}
