<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Option;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSwitchOption;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionList;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\RulesRetriever;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

class FrontendOptionListTest extends TestCase
{
    /**
     * @var ConfigurationSwitchOption|\Phake_IMock
     * @Mock ConfigurationSwitchOption
     */
    private $configurationSwitchOption;

    /**
     * @var RulesRetriever|\Phake_IMock
     * @Mock RulesRetriever
     */
    private $rulesRetriever;

    private FrontendOptionList $frontendOptionList;

    protected function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->frontendOptionList = new FrontendOptionList(
            $this->configurationSwitchOption,
            $this->rulesRetriever
        );
    }

    public function testShouldSkipRulesCheckIfNoRules(): void
    {
        $configuration = new Configuration();
        $configuration->item = new Item();
        $configuration->item->identifier = 'test';
        $option1 = new Option();
        $option1->identifier = 'option1';
        $option2 = new Option();
        $option2->identifier = 'option2';

        \Phake::when($this->rulesRetriever)->hasRulesFor($configuration)->thenReturn(false);

        $this->frontendOptionList->runChecksForOptionsList([$option1, $option2], $configuration, 'component');

        \Phake::verify($this->configurationSwitchOption, \Phake::never())->switchOption();
    }

    public function testShouldCheckRules(): void
    {
        $configuration = new Configuration();
        $configuration->item = new Item();
        $configuration->item->identifier = 'test';
        $option1 = new Option();
        $option1->identifier = 'option1';
        $option2 = new Option();
        $option2->identifier = 'option2';

        \Phake::when($this->rulesRetriever)->hasRulesFor($configuration)->thenReturn(true);

        $switchOption1 = new \stdClass();
        $switchOption1->identifier = 'option1';
        $switchOption1->amount = 1;
        $switchOptions1 = ['component' => $switchOption1];
        $switchOption2 = new \stdClass();
        $switchOption2->identifier = 'option2';
        $switchOption2->amount = 1;
        $switchOptions2 = ['component' => $switchOption2];

        $this->frontendOptionList->runChecksForOptionsList([$option1, $option2], $configuration, 'component');

        \Phake::verify($this->configurationSwitchOption)->switchOption($configuration, $switchOptions1, false);
        \Phake::verify($this->configurationSwitchOption)->switchOption($configuration, $switchOptions2, false);
    }
}
