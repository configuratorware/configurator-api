<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Option;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputNotEmptyValidator;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

class OptionInputNotEmptyValidatorTest extends TestCase
{
    public const VALIDATION_MESSAGE = 'Invalid value. Value must not be empty.';

    /**
     * @var  OptionInputNotEmptyValidator
     */
    private $validator;

    protected function setUp(): void
    {
        $this->validator = new OptionInputNotEmptyValidator();
    }

    /**
     * @dataProvider provideValidationValues
     */
    public function testShouldValidate(?string $value, ?string $expected): void
    {
        $option = new Option();
        $option->inputText = $value;
        $actual = $this->validator->validate($option, new Configuration());

        self::assertEquals($expected, $actual);
    }

    public function provideValidationValues()
    {
        return [
                [null, self::VALIDATION_MESSAGE],
                ['', self::VALIDATION_MESSAGE],
                ['0', null],
                ['test', null],
            ];
    }
}
