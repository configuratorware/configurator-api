<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Option;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputNumberValidator;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

class OptionInputNumberValidatorTest extends TestCase
{
    public const VALIDATION_MESSAGE = 'Invalid value: %s is not a number';

    /**
     * @var  OptionInputNumberValidator
     */
    private $validator;

    protected function setUp(): void
    {
        $this->validator = new OptionInputNumberValidator();
    }

    /**
     * @dataProvider provideValidationValues
     */
    public function testShouldValidate(?string $value, ?string $expected): void
    {
        $option = new Option();
        $option->inputText = $value;
        $actual = $this->validator->validate($option, new Configuration());

        self::assertEquals($expected, $actual);
    }

    public function provideValidationValues()
    {
        return [
                [null, sprintf(self::VALIDATION_MESSAGE, 'null')],
                ['hallo', sprintf(self::VALIDATION_MESSAGE, 'hallo')],
                ['0', null],
                ['1', null],
                ['1.5', null],
            ];
    }
}
