<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Option;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputNotEmptyValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputNumberValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputValidation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;

class OptionInputValidationTest extends TestCase
{
    /**
     * @var OptionInputValidation
     */
    private $optionInputValidation;

    /**
     * @var  OptionRepository|\Phake_IMock
     * @Mock OptionRepository
     */
    private $optionRepository;

    /**
     * @var  OptionInputNumberValidator|\Phake_IMock
     * @Mock OptionInputNumberValidator
     */
    private $numberValidator;

    /**
     * @var  OptionInputNotEmptyValidator|\Phake_IMock
     * @Mock OptionInputNotEmptyValidator
     */
    private $notEmptyValidator;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        \Phake::whenStatic($this->numberValidator)->getType()->thenReturn(OptionInputNumberValidator::getType());
        \Phake::whenStatic($this->notEmptyValidator)->getType()->thenReturn(OptionInputNotEmptyValidator::getType());
        $this->optionInputValidation = new OptionInputValidation($this->optionRepository);
        $this->optionInputValidation->addValidator($this->numberValidator);
        $this->optionInputValidation->addValidator($this->notEmptyValidator);
    }

    public function testShouldListValidationTypes(): void
    {
        $expected = [OptionInputNumberValidator::getType(), OptionInputNotEmptyValidator::getType()];
        $actual = $this->optionInputValidation->getValidationTypes();

        self::assertEquals($expected, $actual);
    }

    public function testShouldPassOnNonExistingType(): void
    {
        $options = [['identifier' => 'optionIdentifier', 'inputValidationType' => 'non_existing_type']];
        \Phake::when($this->optionRepository)->findTextInputOptionsByIdentifiers()->thenReturn($options);
        $configuration = $this->buildConfigurationWithComponent($this->buildComponentWithSelectedOptions($options));

        $actual = $this->optionInputValidation->validateConfiguration($configuration);
        self::assertNull($actual);
    }

    public function testShouldValidate(): void
    {
        $optionText = 'not_a_number';
        $optionIdentifier = 'optionIdentifier';
        $options = [['identifier' => $optionIdentifier, 'inputValidationType' => OptionInputNumberValidator::getType()]];
        \Phake::when($this->optionRepository)->findTextInputOptionsByIdentifiers([$optionIdentifier])->thenReturn($options);
        $configuration = $this->buildConfigurationWithComponent($this->buildComponentWithSelectedOptions($options, [$optionText]));
        \Phake::when($this->numberValidator)->validate($configuration->optionclassifications[0]->selectedoptions[0], $configuration)->thenReturn('validation error');
        $actual = $this->optionInputValidation->validateConfiguration($configuration);
        self::assertNotEmpty($actual);
        \Phake::verify($this->numberValidator)->validate($configuration->optionclassifications[0]->selectedoptions[0], $configuration);
    }

    protected function buildComponentWithSelectedOptions(array $options, array $inputTexts = null): OptionClassification
    {
        $component = new OptionClassification();
        $component->selectedoptions = [];
        foreach ($options as $i => $option) {
            $selectedOption = new Option();
            $selectedOption->identifier = $option['identifier'];
            if ($inputTexts) {
                $selectedOption->inputText = $inputTexts[$i];
            }
            $component->selectedoptions[] = $selectedOption;
        }

        return $component;
    }

    protected function buildConfigurationWithComponent(OptionClassification $component): Configuration
    {
        $configuration = new Configuration();
        $configuration->optionclassifications = [$component];

        return $configuration;
    }
}
