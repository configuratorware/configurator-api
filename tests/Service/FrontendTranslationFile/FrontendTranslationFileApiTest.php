<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\FrontendTranslationFile;

use org\bovigo\vfs\vfsStream;
use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\CacheClear\CacheClearCommandRunner;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\TranslationsPaths;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FrontendTranslationFileUploadArguments;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class FrontendTranslationFileApiTest extends ApiTestCase
{
    private FrontendTranslationFileApi $translationApi;

    private CachedFrontendTranslationFileCreator $cachedTranslationCreator;

    /**
     * @var LanguageRepository|\Phake_IMock
     * @Mock LanguageRepository
     */
    private $languageRepository;

    /**
     * @var Language|\Phake_IMock
     * @Mock Language
     */
    private $mockLangGB;

    /**
     * @var Language|\Phake_IMock
     * @Mock Language
     */
    private $mockLangDE;

    /**
     * @var CacheClearCommandRunner|\Phake_IMock
     * @Mock CacheClearCommandRunner
     */
    private CacheClearCommandRunner $cacheClearCommandRunner;

    /**
     * @var SettingRepository|\Phake_IMock
     * @Mock SettingRepository
     */
    private SettingRepository $settingsRepository;

    private string $root;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        \Phake::when($this->languageRepository)->findAll->thenReturn([
            $this->mockLangGB,
            $this->mockLangDE,
        ]);
        \Phake::when($this->mockLangDE)->getIso->thenReturn('de_DE');
        \Phake::when($this->mockLangGB)->getIso->thenReturn('en_GB');

        $this->root = vfsStream::setup(
            'root',
            0777,
            [
                'upload' => [
                    // uploaded file with BOM
                    'en_GB.json' => '{"uploaded_key":"testvalue_uploaded_EN", "cached_key":"testvalue_override_cached_EN"}' . chr(239) . chr(187) . chr(191),
                ],
                '/translations/' => [
                    'en_GB.json' => '{"base_key":"testvalue_base_EN"}',
                ],
                '/translations_user/' => [
                    'en_GB.json' => '{"user_key":"testvalue_user_generated_EN", "custom_key":"testvalue_override_custom_EN"}',
                ],
                '/translations_cached/' => [
                    'messages.en_GB.json' => '{"cached_key":"testvalue_cached_EN"}',
                    'messages.de_DE.json' => '{"cached_key":"testvalue_cached_DE"}',
                ],
                '/translations_custom/' => [
                    'en_GB.json' => '{"custom_key":"testvalue_custom_EN"}',
                ],
            ]
        )->url();

        $translationsPath = new TranslationsPaths(
            '',
            $this->root . '/translations',
            $this->root . '/translations_cached',
            $this->root . '/translations_custom',
            $this->root . '/translations_user',
        );

        $this->cachedTranslationCreator = new CachedFrontendTranslationFileCreator(
            $translationsPath,
            $this->languageRepository,
            new NullLogger()
        );

        $this->translationApi = new FrontendTranslationFileApi($translationsPath, $this->cachedTranslationCreator, $this->settingsRepository, $this->cacheClearCommandRunner);
    }

    public function testApiShouldProvideDownloadabelFile()
    {
        $fileResult = $this->translationApi->download('de_DE');
        self::assertInstanceOf(FileResult::class, $fileResult);
    }

    public function testApiShouldThrowErrorForMissingFile()
    {
        $this->expectException(FileNotFoundException::class);
        $this->translationApi->download('hu_HU');
    }

    public function testApiShouldSaveFile()
    {
        $uploadArg = new FrontendTranslationFileUploadArguments();
        $uploadArg->uploadedFile = new UploadedFile($this->root . '/upload/en_GB.json', 'en_GB.json');
        $this->translationApi->save('en_GB', $uploadArg);

        self::assertFileExists($this->root . '/translations_cached/messages.en_GB.json');
        self::assertSame(
            '{"base_key":"testvalue_base_EN","custom_key":"testvalue_override_custom_EN","user_key":"testvalue_user_generated_EN","uploaded_key":"testvalue_uploaded_EN","cached_key":"testvalue_override_cached_EN"}',
            file_get_contents($this->root . '/translations_cached/messages.en_GB.json')
        );
    }

    public function testApiShouldClearCache()
    {
        $setting = new Setting();
        $setting->setClearCacheOnTranslationsUpload(true);
        \Phake::when($this->settingsRepository)->getSetting()->thenReturn($setting);
        $uploadArg = new FrontendTranslationFileUploadArguments();
        $uploadArg->uploadedFile = new UploadedFile($this->root . '/upload/en_GB.json', 'en_GB.json');
        $this->translationApi->save('en_GB', $uploadArg);

        \Phake::verify($this->cacheClearCommandRunner)->clearCache();
    }

    public function testApiShouldNotClearCache()
    {
        $setting = new Setting();
        $setting->setClearCacheOnTranslationsUpload(false);
        \Phake::when($this->settingsRepository)->getSetting()->thenReturn($setting);
        $uploadArg = new FrontendTranslationFileUploadArguments();
        $uploadArg->uploadedFile = new UploadedFile($this->root . '/upload/en_GB.json', 'en_GB.json');
        $this->translationApi->save('en_GB', $uploadArg);

        \Phake::verifyNoInteraction($this->cacheClearCommandRunner);
    }
}
