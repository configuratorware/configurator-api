<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\FrontendTranslationFile;

use org\bovigo\vfs\vfsStream;
use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\TranslationsPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CachedFrontendTranslationFileCreatorTest extends ApiTestCase
{
    private CachedFrontendTranslationFileCreator $cachedTranslationCreator;

    private string $root;

    /**
     * @var LanguageRepository|\Phake_IMock
     * @Mock LanguageRepository
     */
    private $languageRepository;

    /**
     * @var Language|\Phake_IMock
     * @Mock Language
     */
    private $mockLangGB;

    /**
     * @var Language|\Phake_IMock
     * @Mock Language
     */
    private $mockLangDE;

    /**
     * @var array
     */
    private array $translationFiles;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->root = vfsStream::setup()->url();

        $this->cachedTranslationCreator = new CachedFrontendTranslationFileCreator(
            new TranslationsPaths(
                '',
                $this->root . '/translations',
                $this->root . '/translations_cached',
                $this->root . '/translations_custom',
                $this->root . '/translations_user',
            ),
            $this->languageRepository,
            new NullLogger()
        );

        \Phake::when($this->languageRepository)->findAll->thenReturn([
            $this->mockLangGB,
            $this->mockLangDE,
        ]);
        \Phake::when($this->mockLangDE)->getIso->thenReturn('de_DE');
        \Phake::when($this->mockLangGB)->getIso->thenReturn('en_GB');

        $this->translationFiles = [
            '/translations/' => [
                'en_GB.json' => '{"incompatibility":{"dialogTitle":"dialogTitletext","rules":{"optionexclusion":{"text":"optionexclusiontext"},"optiondependency":{"text": "optiondependencytext"}}},"rotationInstructions":"rotationInstructionstext"}',
                'de_DE.json' => '{"existing_key":"testvalue_base_DE"}',
            ],
            '/translations_user/' => [
                'en_GB.json' => '{"rotationInstructions":"rotation_user_generated_EN"}',
                'de_DE.json' => '{"existing_key":"testvalue_user_generated_DE"}',
            ],
            '/translations_cached/' => [
                'messages.de_DE.json' => 'dummy DE',
            ],
            '/translations_custom/' => [
                'en_GB.json' => '{"existing_key":{"existing_subkey":"testvalue_custom_EN","new_custom_subkey":"custom subvalue EN"},"new_custom_key":"custom value EN"}',
            ],
        ];
    }

    public function testCachedFileAndDirCreation()
    {
        $files = $this->getTranslationFiles([
            '/translations/',
            '/translations_custom/',
        ]);
        vfsStream::create($files);
        $this->cachedTranslationCreator->createCachedTranslationFiles();
        self::assertDirectoryExists($this->root . '/translations_cached');
        self::assertDirectoryExists($this->root . '/translations_user');
        self::assertFileExists($this->root . '/translations_cached/messages.en_GB.json');
    }

    public function testCachedFileContent()
    {
        $files = $this->getTranslationFiles([
            '/translations/',
            '/translations_cached/',
            '/translations_custom/',
        ]);
        vfsStream::create($files);
        $this->cachedTranslationCreator->createCachedTranslationFiles();

        // test base content
        self::assertFileExists($this->root . '/translations_cached/messages.de_DE.json');
        self::assertSame(
            '{"existing_key":"testvalue_base_DE"}',
            file_get_contents($this->root . '/translations_cached/messages.de_DE.json')
        );

        // test custom content with new key
        self::assertFileExists($this->root . '/translations_cached/messages.en_GB.json');
        self::assertSame(
            '{"incompatibility":{"dialogTitle":"dialogTitletext","rules":{"optionexclusion":{"text":"optionexclusiontext"},"optiondependency":{"text":"optiondependencytext"}}},"rotationInstructions":"rotationInstructionstext","existing_key":{"existing_subkey":"testvalue_custom_EN","new_custom_subkey":"custom subvalue EN"},"new_custom_key":"custom value EN"}',
            file_get_contents($this->root . '/translations_cached/messages.en_GB.json')
        );
    }

    public function testUserFileContent()
    {
        $files = $this->getTranslationFiles([
            '/translations/',
            '/translations_cached/',
            '/translations_custom/',
            '/translations_user/',
        ]);
        vfsStream::create($files);
        $this->cachedTranslationCreator->createCachedTranslationFiles();

        self::assertFileExists($this->root . '/translations_cached/messages.en_GB.json');
        self::assertSame(
            '{"incompatibility":{"dialogTitle":"dialogTitletext","rules":{"optionexclusion":{"text":"optionexclusiontext"},"optiondependency":{"text":"optiondependencytext"}}},"rotationInstructions":"rotation_user_generated_EN","existing_key":{"existing_subkey":"testvalue_custom_EN","new_custom_subkey":"custom subvalue EN"},"new_custom_key":"custom value EN"}',
            file_get_contents($this->root . '/translations_cached/messages.en_GB.json')
        );
        self::assertFileExists($this->root . '/translations_cached/messages.de_DE.json');
        self::assertSame(
            '{"existing_key":"testvalue_user_generated_DE"}',
            file_get_contents($this->root . '/translations_cached/messages.de_DE.json')
        );
    }

    public function testUserFileDiff()
    {
        $files = $this->getTranslationFiles([
            '/translations/',
            '/translations_cached/',
            '/translations_custom/',
            '/translations_user/',
        ]);
        vfsStream::create($files);
        $this->cachedTranslationCreator->createCachedTranslationFiles();

        $iso = 'en_GB';
        $uploadedTranslations = '{"incompatibility":{"dialogTitle": "dialogTitletext","rules":{"optionexclusion":{"text": "optionexclusiontextUpdated"},"optiondependency":{"text":"optiondependencytext","newtext":"optiondependencytextnew"}}},"newrotationInstructions":"rotationInstructionstextnew"}';
        $this->cachedTranslationCreator->writeUserTranslationDiff($uploadedTranslations, $iso);
        self::assertSame(
            '{"rotationInstructions":"rotation_user_generated_EN","incompatibility":{"rules":{"optionexclusion":{"text":"optionexclusiontextUpdated"},"optiondependency":{"newtext":"optiondependencytextnew"}}},"newrotationInstructions":"rotationInstructionstextnew"}',
            file_get_contents($this->root . '/translations_user/en_GB.json')
        );
    }

    private function getTranslationFiles(array $directories): array
    {
        $files = [];

        foreach ($directories as $dir) {
            $files[$dir] = $this->translationFiles[$dir];
        }

        return $files;
    }
}
