<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewFileRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\DTO\CreatorView as CreatorViewDTO;
use Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo\DefaultExpectedMediaPath;
use Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo\ViewMediaInfo;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\CreatorViewThumbnailRepositoryMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

class ViewMediaInfoTest extends TestCase
{
    use EntityTestTrait;

    private const VIEWS = [
         1 => [
            'directory' => 'item_1/1_mock_view_1',
            'view' => 'mock_view_1',
            'item' => 'item_1',
            'sequence' => '1',
        ],
        2 => [
            'directory' => 'item_1/2_mock_view_2',
            'view' => 'mock_view_2',
            'item' => 'item_1',
            'sequence' => '2',
        ],
        3 => [
            'directory' => 'item_1/3_mock_view_3',
            'view' => 'mock_view_3',
            'item' => 'item_1',
            'sequence' => '3',
        ],
    ];

    /**
     * @var CreatorViewRepository|\Phake_IMock
     * @Mock CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var CreatorViewFileRepository|\Phake_IMock
     * @Mock CreatorViewFileRepository
     */
    private $creatorViewFileRepository;

    /**
     * @var CreatorViewDelete|\Phake_IMock
     * @Mock CreatorViewDelete
     */
    private $creatorViewDelete;

    /**
     * @var ViewMediaInfo
     */
    private $viewMediaInfo;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $defaultExpectedMediaPath = new DefaultExpectedMediaPath(
            '/view_thumb_base_path/',
            'component_thumbnail_base_path',
            'option_image_base_path',
            'layer_image_base_path'
        );
        $this->viewMediaInfo = new ViewMediaInfo($defaultExpectedMediaPath, $this->creatorViewRepository, new CreatorViewThumbnailRepositoryMock(), $this->creatorViewFileRepository, $this->creatorViewDelete);
    }

    public function testViewMediaInfoRetrieval(): void
    {
        $item = $this->generateItem();

        \Phake::when($this->creatorViewRepository)->findByItemId($item->getId())->thenReturn($this->generateCreatorViews($item));

        $result = $this->viewMediaInfo->getViewMediaInfo($item);

        self::assertSame(
            json_encode([
                [
                    'id' => 1,
                    'identifier' => 'mock_view_1',
                    'sequenceNumber' => '1',
                    'thumbnail' => [
                        'fileName' => 'mock_view_1.png',
                        'src' => '/view_thumb_base_path/mock_view_1.png',
                        'expectedSrc' => null,
                    ],
                ],
                [
                    'id' => 2,
                    'identifier' => 'mock_view_2',
                    'sequenceNumber' => '2',
                    'thumbnail' => [
                        'fileName' => 'mock_view_2.jpg',
                        'src' => '/view_thumb_base_path/mock_view_2.jpg',
                        'expectedSrc' => null,
                    ],
                ],
                [
                    'id' => 3,
                    'identifier' => 'mock_view_3',
                    'sequenceNumber' => '3',
                    'thumbnail' => [
                        'fileName' => null,
                        'src' => null,
                        'expectedSrc' => '/view_thumb_base_path/mock_view_3.(jpeg|jpg|png)',
                    ],
                ],
            ]),
            json_encode($result)
        );
    }

    public function testShouldDeleteCreatorViews(): void
    {
        $item = $this->generateItem();
        $generatedViews = $this->generateCreatorViews($item);
        $deletedViewIndex = 0;
        $generatedViewDTOs = [];

        foreach ($generatedViews as $index => $generatedView) {
            if ($index !== $deletedViewIndex) {
                $generatedViewDTOs[] = $this->creatorViewToDTO($generatedView);
            }
        }

        \Phake::when($this->creatorViewRepository)->findByItemId($item->getId())->thenReturn($generatedViews);
        \Phake::when($this->creatorViewFileRepository)->findCreatorViews($item)->thenReturn($generatedViewDTOs);

        $this->viewMediaInfo->getViewMediaInfo($item);

        \Phake::verify($this->creatorViewDelete)->deleteEntity($generatedViews[$deletedViewIndex]);
    }

    public function testShouldCreateCreatorView(): void
    {
        $item = $this->generateItem();
        $generatedViews = $this->generateCreatorViews($item);
        $generatedViewDTOs = [];

        foreach ($generatedViews as $generatedView) {
            $generatedViewDTOs[] = $this->creatorViewToDTO($generatedView);
        }
        $addedView = CreatorViewDTO::from('added_view_identifier', '999', $item->getIdentifier());
        $generatedViewDTOs[] = $addedView;

        \Phake::when($this->creatorViewRepository)->findByItemId($item->getId())->thenReturn($generatedViews);
        \Phake::when($this->creatorViewFileRepository)->findCreatorViews($item)->thenReturn($generatedViewDTOs);

        $this->viewMediaInfo->getViewMediaInfo($item);

        \Phake::verify($this->creatorViewRepository)->save(CreatorView::from(null, $addedView->getIdentifier(), $item, $addedView->getSequenceNumber()));
    }

    public function generateItem(): Item
    {
        $item = new Item();
        $this->setPrivateId($item, 1);
        $item->setIdentifier('item_1');

        return $item;
    }

    public function generateCreatorViews(Item $item): array
    {
        $views = [];

        foreach (self::VIEWS as $id => $view) {
            $creatorView = CreatorView::from($view['directory'], $view['view'], $item, $view['sequence']);
            $this->setPrivateId($creatorView, $id);
            $views[] = $creatorView;
        }

        return $views;
    }

    public function creatorViewToDTO(CreatorView $view): CreatorViewDTO
    {
        return CreatorViewDTO::from($view->getIdentifier(), $view->getSequenceNumber(), $view->getItem()->getIdentifier());
    }
}
