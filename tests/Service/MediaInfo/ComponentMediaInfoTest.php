<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo\ComponentMediaInfo;
use Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo\DefaultExpectedMediaPath;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\ComponentImageRepositoryMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\LayerImageRepositoryMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\OptionImageRepositoryMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

class ComponentMediaInfoTest extends TestCase
{
    use EntityTestTrait;

    private const VIEWS = [
        1 => [
            'directory' => 'item_1/01_mock_view_1',
            'view' => 'mock_view_1',
            'item' => 'item_1',
            'sequence' => '01',
        ],
        2 => [
            'directory' => 'item_1/02_mock_view_2',
            'view' => 'mock_view_2',
            'item' => 'item_1',
            'sequence' => '02',
        ],
        3 => [
            'directory' => 'item_1/03_mock_view_3',
            'view' => 'mock_view_3',
            'item' => 'item_1',
            'sequence' => '03',
        ],
    ];

    /**
     * @var CreatorViewRepository|\Phake_IMock
     * @Mock CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var ComponentMediaInfo
     */
    private $componentMediaInfo;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $optionImageRepositoryMock = new OptionImageRepositoryMock();
        $this->componentMediaInfo = new ComponentMediaInfo(
            new ComponentImageRepositoryMock(),
            new DefaultExpectedMediaPath('view', 'component', 'option', 'layer'),
            new LayerImageRepositoryMock(),
            $optionImageRepositoryMock,
            $optionImageRepositoryMock,
            $this->creatorViewRepository
        );
    }

    public function testComponentMediaInfoRetrieval(): void
    {
        $item = $this->createItem();
        \Phake::when($this->creatorViewRepository)->findByItemId($item->getId())->thenReturn($this->generateCreatorViews($item));

        $result = $this->componentMediaInfo->getComponentMediaInfo($item);

        self::assertSame($this->getExpectedResult(), json_decode(json_encode($result), true));
    }

    /**
     * @return array[]
     */
    private function getExpectedResult(): array
    {
        return [
            [
                'id' => 1,
                'identifier' => 'mock_component_1',
                'translated_title' => '{mock_component_1}',
                'images' => [
                    'componentThumbnail' => [
                        'fileName' => 'mock_component_1.png',
                        'src' => '/components/mock_component_1.png',
                        'expectedSrc' => null,
                    ],
                ],
                'options' => [
                    [
                        'id' => 1,
                        'identifier' => 'mock_option_1',
                        'translated_title' => '{mock_option_1}',
                        'images' => [
                            'optionThumbnail' => [
                                'fileName' => 'mock_option_1.jpg',
                                'src' => '/option/mock_option_1.jpg',
                                'expectedSrc' => null,
                            ],
                            'optionDetailImage' => [
                                'fileName' => 'mock_option_1_fullsize.jpg',
                                'src' => '/option/mock_option_1_fullsize.jpg',
                                'expectedSrc' => null,
                            ],
                            'visualizationImages' => [
                                'mock_view_1' => [
                                    'fileName' => 'mock_option_1.jpg',
                                    'src' => '/item_1/mock_view_1/mock_component_1/mock_option_1.jpg',
                                    'expectedSrc' => null,
                                ],
                                'mock_view_2' => [
                                    'fileName' => 'mock_option_1.jpg',
                                    'src' => '/item_1/mock_view_2/mock_component_1/mock_option_1.jpg',
                                    'expectedSrc' => null,
                                ],
                                'mock_view_3' => [
                                    'fileName' => null,
                                    'src' => null,
                                    'expectedSrc' => '/layer/item_1/01_mock_view_3/01_mock_component_1/mock_option_1.(jpeg|jpg|png)',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return Item
     */
    private function createItem(): Item
    {
        $option = (new Option())->setIdentifier('mock_option_1');
        $this->setPrivateId($option, 1);

        $optionClassification = (new Optionclassification())->setIdentifier('mock_component_1');
        $this->setPrivateId($optionClassification, 1);

        $itemOptionClassification = new ItemOptionclassification();
        $itemOptionClassification->setOptionclassification($optionClassification);
        $itemOptionClassification->addItemOptionclassificationOption((new ItemOptionclassificationOption())->setOption($option));

        $item = new Item();
        $this->setPrivateId($item, 1);
        $item->setIdentifier('item_1');
        $itemOptionClassification->setItem($item);
        $item->addItemOptionclassification($itemOptionClassification);

        return $item;
    }

    /**
     * {@inheritDoc}
     */
    public function generateCreatorViews(Item $item): array
    {
        $views = [];

        foreach (self::VIEWS as $id => $view) {
            $creatorView = CreatorView::from($view['directory'], $view['view'], $item, $view['sequence']);
            $this->setPrivateId($creatorView, $id);
            $views[] = $creatorView;
        }

        return $views;
    }
}
