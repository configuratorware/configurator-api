<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo\PossibleNamingIssues;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\LayerImageRepositoryMock;

final class PossibleNamingIssuesTest extends TestCase
{
    /**
     * @var PossibleNamingIssues
     */
    private $possibleNamingIssues;

    public function setUp(): void
    {
        $this->possibleNamingIssues = new PossibleNamingIssues(new LayerImageRepositoryMock());
    }

    /**
     * @dataProvider providerShouldFindPossibleNamingIssues
     *
     * @param array $expectedIssues
     */
    public function testShouldFindPossibleNamingIssues(array $expectedIssues): void
    {
        $actualIssues = $this->possibleNamingIssues->getPossibleNamingIssues($this->generateItem());

        self::assertEquals($expectedIssues, $actualIssues);
    }

    public function providerShouldFindPossibleNamingIssues(): \Generator
    {
        yield [['some/possible/naming/issue']];
    }

    /**
     * @return Item
     */
    private function generateItem(): Item
    {
        $itemOptionClassification = new ItemOptionclassification();
        $itemOptionClassification->setOptionclassification((new Optionclassification())->setIdentifier('mock_component_1'));

        $option = (new Option())->setIdentifier('mock_option_1');
        $itemOptionClassification->addItemOptionclassificationOption((new ItemOptionclassificationOption())->setOption($option));

        $item = new Item();
        $item->setIdentifier('item_1');
        $itemOptionClassification->setItem($item);
        $item->addItemOptionclassification($itemOptionClassification);

        return $item;
    }
}
