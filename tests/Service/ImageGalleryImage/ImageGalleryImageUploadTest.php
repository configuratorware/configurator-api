<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageConverterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImagickFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage\FrontendImageGalleryImageStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage\ImageGalleryImageUpload;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageUploadData;
use Redhotmagma\ConfiguratorApiBundle\Vector\LegacyInkscapeConverter;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageGalleryImageUploadTest extends TestCase
{
    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var ImageGalleryImageUpload
     */
    private $imageGalleryImageUpload;

    /**
     * @var ImageGalleryImageRepository|\Phake_IMock
     * @Mock ImageGalleryImageRepository
     */
    private $imageGalleryImageRepository;

    /**
     * @var FrontendImageGalleryImageStructureFromEntityConverter|\Phake_IMock
     * @Mock FrontendImageGalleryImageStructureFromEntityConverter
     */
    private $frontendStructureFromEntityConverter;

    public function setUp(): void
    {
        if (!defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', 'en_GB');
        }

        \Phake::initAnnotations($this);

        $config = [
            'filesystem_path' => __DIR__ . '/_data/imagegallery',
            'url_path' => '/imagegallery',
            'thumb_size' => [
                    'width' => 150,
                    'height' => 120,
                ],
        ];
        $this->fileSystem = new Filesystem();
        $imagickFactory = new ImagickFactory($this->fileSystem, new LegacyInkscapeConverter($this->fileSystem));
        $imageConverterService = new ImageConverterService($imagickFactory, $this->fileSystem);

        $this->imageGalleryImageUpload = new ImageGalleryImageUpload(
            $this->frontendStructureFromEntityConverter,
            $this->imageGalleryImageRepository,
            $imageConverterService,
            $config
        );
    }

    /**
     * @dataProvider providerShouldUpload
     *
     * @param int $id
     * @param string $fileExtension
     * @param array $expectedFiles
     */
    public function testShouldUpload(int $id, string $fileExtension, array $expectedFiles): void
    {
        $imageGalleryImageUploadData = new ImageGalleryImageUploadData();
        $imageGalleryImageUploadData->galleryImageFormData = $this->getTestImage($fileExtension);
        $imageGalleryImageUploadData->printImageFormData = $this->getTestImage($fileExtension);

        \Phake::when($this->imageGalleryImageRepository)->findOneBy->thenReturn($this->getEntity($id));

        $this->imageGalleryImageUpload->upload($imageGalleryImageUploadData);

        foreach ($expectedFiles as $file) {
            $filePath = __DIR__ . '/_data/imagegallery/' . $file;
            self::assertFileExists($filePath);
            $this->fileSystem->remove($filePath);
        }
    }

    /**
     * @return array
     */
    public function providerShouldUpload(): array
    {
        return [
            [
                1,
                '.svg',
                [
                    'c4ca4238a0b923820dcc509a6f75849b.svg',
                    'c4ca4238a0b923820dcc509a6f75849b_p.svg',
                    'c4ca4238a0b923820dcc509a6f75849b_t.png',
                ],
            ],
            [
                2,
                '',
                [
                    'c81e728d9d4c2f636f067f89cc14862c.svg',
                    'c81e728d9d4c2f636f067f89cc14862c_p.svg',
                    'c81e728d9d4c2f636f067f89cc14862c_t.png',
                ],
            ],
        ];
    }

    private function getEntity(int $id)
    {
        $entity = new ImageGalleryImage();
        $this->setPrivateProperty($entity, 'id', $id);

        return $entity;
    }

    private function getTestImage(string $fileExtension): UploadedFile
    {
        $fileName = random_int(1, 100000) . $fileExtension;
        $filePath = __DIR__ . '/_data/' . $fileName;

        $this->fileSystem->dumpFile($filePath, $this->svg);

        return new UploadedFile($filePath, $fileName, null, null, true);
    }

    private function setPrivateProperty($object, string $property, $value): void
    {
        $reflect = new \ReflectionClass($object);
        $prop = $reflect->getProperty($property);
        $prop->setAccessible(true);
        $prop->setValue($object, $value);
        $prop->setAccessible(false);
    }

    private $svg = <<<EOT
<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 23.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Ebene_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 123.6 33.8" style="enable-background:new 0 0 123.6 33.8;" xml:space="preserve">
<style type="text/css">
	.st0{fill:#E3121E;}
	.st1{fill:#FDD11B;}
	.st2{fill:#048952;}
</style>
<g>
	<g>
		<path class="st0" d="M20.2,19.7c-0.5-1-1.2-2-1.9-3c-2.6-3.5-5.5-7.4-0.9-14.6c0.1-0.1,0.1-0.2,0-0.2C16.6,1.9,4.1,13,5.9,22.3 c0.6,3.3,2.1,5.2,3.8,6.4c1,0.7,2.1,1.1,3.1,1.2c0.2,0,0.3,0,0.5,0.1c0.2,0,0.5,0,0.7,0c0.1,0,0.1,0,0.2,0c0.1,0,0.1,0,0.2,0 c2.4-0.2,6.5-1.8,6.7-7.3C21,21.6,20.7,20.6,20.2,19.7z"/>
	</g>
</g>
<g>
	<path class="st1" d="M25.1,8.8c0,0-0.1,0-0.1,0.1c-1.1,4.4-3.2,6.4-5.4,7.3c0.7,0.9,1.3,1.8,1.8,2.9c0.6,1.2,0.9,2.4,0.8,3.6 c0,1.1-0.2,2.1-0.5,3c1.4-2,2.3-4.5,2.9-7C25.7,13.6,25.4,8.8,25.1,8.8z"/>
</g>
<g>
<path class="st2" d="M26.1,17.5c-0.1,0.5-0.2,0.9-0.3,1.4c-0.3,1.3-0.7,2.6-1.1,3.7c2.4-3.6,3.6-7.3,3.3-7.3c0,0-0.1,0-0.1,0.1 C27.4,16.3,26.8,16.9,26.1,17.5z"/>
<path class="st2" d="M14.1,29.9c-0.1,0-0.2,0-0.2,0c-0.1,0-0.1,0-0.2,0C13.8,30,13.9,30,14.1,29.9C14.1,30,14.1,30,14.1,29.9 L14.1,29.9z"/>
</g>
<path class="st0" d="M32.2,20.2c0.3-0.7,0.7-1.5,2.4-2.3v1.3c-0.9,0.2-1.6,0.5-2.3,2.3v8h-0.9V18.2h0.9V20.2z"/>
<path class="st2" d="M36.1,20.9c0-1.7,0.9-2.7,2.5-2.7c2.6,0,2.6,2.2,2.6,3.6V24h-4.2v2.8c0,1.5,0.7,1.9,1.7,1.9
	c0.2,0,1.1,0,1.5-0.9c0.2-0.4,0.2-0.5,0.2-2.1h0.9v1.3c0,1.7-0.9,2.7-2.6,2.7c-0.5,0-1.2-0.1-1.8-0.6c-0.6-0.5-0.8-1.2-0.8-2.1V20.9
	z M40.4,21.1c0-0.4,0-2.1-1.6-2.1c-1.6,0-1.6,1.6-1.6,2.2v2h3.3V21.1z"/>
<path class="st1" d="M49,13.6v15.9h-0.9v-1.2c-0.2,0.5-0.8,1.3-2.1,1.3c-2.3,0-2.3-2.3-2.3-2.9v-5.7c0-2.6,1.6-2.9,2.3-2.9
	c1.2,0,1.7,0.6,2,1.1v-5.7H49z M48,21.1c0-0.3-0.3-2-1.8-2c-0.3,0-1.6,0.1-1.6,2v5.5c0,0.5,0,2.2,1.6,2.2c1.3,0,1.8-1.4,1.8-1.6
	V21.1z"/>
<path class="st0" d="M52.8,13.6v5.5c0.3-0.4,0.9-1,2-1c0.5,0,1.1,0.2,1.4,0.5c0.8,0.6,0.8,1.5,0.8,2v9h-1v-8.4c0-0.7,0-2.1-1.5-2.1
	c-0.5,0-0.9,0.2-1.3,0.5c-0.3,0.3-0.4,0.7-0.5,1v9h-1V13.6H52.8z"/>
<path class="st0" d="M59.7,20.9c0-2.3,1.7-2.7,2.7-2.7c2.6,0,2.6,2.1,2.6,2.8v5.4c0,0.9-0.1,1.6-0.5,2.2c-0.7,1.1-1.9,1.1-2.2,1.1
	c-0.2,0-0.9,0-1.5-0.4c-1-0.5-1.1-1.4-1.1-2.4V20.9z M64,21c0-0.4,0-2-1.6-2c-1.8,0-1.8,1.5-1.8,2.2v5.5c0,0.4,0,1.9,1.6,1.9
	c0.9,0,1.7-0.5,1.7-2V21z"/>
<path class="st0" d="M67.7,15.3h1v2.9h1.7v0.9h-1.7v8.5c0,0.6,0,1.1,0.8,1.1c0.2,0,0.6,0,0.9,0v0.9c-0.3,0-0.7,0.1-1,0.1
	c-1.3,0-1.7-0.5-1.7-1.3v-9.1h-1.2v-0.9h1.2V15.3z"/>
<path class="st0" d="M73.1,19.1c0.2-0.3,0.9-1.1,2.1-1.1c1.3,0,1.7,0.9,1.9,1.4c0.3-0.5,0.9-1.4,2.3-1.4c0.9,0,1.8,0.5,2,1.5
	c0.1,0.4,0.1,0.9,0.1,1.9v8h-1v-8.1c0-0.9,0-1.2-0.1-1.6c-0.2-0.8-0.9-0.9-1.3-0.9c-1.4,0-1.9,1.4-1.9,1.4v9.2h-1v-8.9
	c0-0.2,0-0.7-0.2-1c-0.2-0.4-0.7-0.7-1.2-0.7c-1.3,0-1.8,1.2-1.8,1.2v9.3h-1V18.2h1V19.1z"/>
<path class="st0" d="M84.2,21.7c0-1.7,0-3.6,2.6-3.6c2.3,0,2.3,1.8,2.3,2.5v7.6c0,0.4,0.3,0.4,0.7,0.4v0.9c-1,0-1.4-0.1-1.6-0.9
	c-0.5,0.4-1.1,0.9-2.2,0.9c-1.6,0-2.2-1.3-2.2-2.7c0-2.2,1.3-3,2.5-3.6c1.1-0.6,1.3-0.7,1.9-1.1v-1.3c0-1-0.1-1.8-1.4-1.8
	c-1.5,0-1.5,1.1-1.5,2.7H84.2z M88.2,23.2c-0.4,0.2-0.5,0.3-1.4,0.8c-1.1,0.7-2.1,1.2-2.1,2.9c0,0.9,0.4,1.7,1.5,1.7
	c1,0,1.5-0.6,2-1.1V23.2z"/>
<path class="st0" d="M91.8,20.7c0-1.2,0.4-2.6,2.3-2.6c1.1,0,1.6,0.7,1.9,1v-1h1v11.9c0,0.7,0,1.6-0.9,2.4c-0.5,0.5-1.2,0.7-1.9,0.7
	c-1,0-1.5-0.4-1.7-0.5c-0.7-0.5-0.8-1.2-0.8-1.9h0.9c0,0.4,0,1.5,1.6,1.5c0.2,0,1.8,0,1.8-1.8v-1.9c-0.4,0.5-0.9,1-2,1
	c-2.2,0-2.2-2.1-2.2-3.1V20.7z M96,20.5c0-0.3-0.5-1.6-1.7-1.6c-1.6,0-1.6,1.5-1.6,2.2v5.2c0,0.7,0,2.2,1.5,2.2
	c1.3,0,1.8-1.4,1.8-1.7V20.5z"/>
<path class="st0" d="M100.8,19.1c0.2-0.3,0.9-1.1,2.1-1.1c1.3,0,1.7,0.9,1.9,1.4c0.3-0.5,0.9-1.4,2.3-1.4c0.9,0,1.8,0.5,2,1.5
	c0.1,0.4,0.1,0.9,0.1,1.9v8h-1v-8.1c0-0.9,0-1.2-0.1-1.6c-0.2-0.8-0.9-0.9-1.3-0.9c-1.4,0-1.9,1.4-1.9,1.4v9.2h-1v-8.9
	c0-0.2,0-0.7-0.2-1c-0.2-0.4-0.7-0.7-1.2-0.7c-1.3,0-1.8,1.2-1.8,1.2v9.3h-1V18.2h1V19.1z"/>
<path class="st0" d="M111.9,21.7c0-1.7,0-3.6,2.6-3.6c2.3,0,2.3,1.8,2.3,2.5v7.6c0,0.4,0.3,0.4,0.7,0.4v0.9c-1,0-1.4-0.1-1.6-0.9
	c-0.5,0.4-1.1,0.9-2.2,0.9c-1.6,0-2.2-1.3-2.2-2.7c0-2.2,1.3-3,2.5-3.6c1.1-0.6,1.3-0.7,1.9-1.1v-1.3c0-1-0.1-1.8-1.4-1.8
	c-1.5,0-1.5,1.1-1.5,2.7H111.9z M115.8,23.2c-0.4,0.2-0.5,0.3-1.4,0.8c-1.1,0.7-2.1,1.2-2.1,2.9c0,0.9,0.4,1.7,1.5,1.7
	c1,0,1.5-0.6,2-1.1V23.2z"/>
</svg>
EOT;
}
