INSERT INTO
    item(id, identifier, externalid, configurable, deactivated, minimum_order_amount, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id, accumulate_amounts, configuration_mode, visualization_mode_id, item_status_id)
VALUES
    (701, 'pen_designer_3d', NULL, NULL, 0, 50, NOW(), NULL, '0001-01-01 00:00:00', 1, 1, NULL, NULL, 1, 'designer', 4, 2),
    (702, 'pen_designer_3d_red', NULL, NULL, 0, 0, NOW(), NULL, '0001-01-01 00:00:00', 1, 1, NULL, 701, 1, NULL, NULL, NULL),
    (703, 'pen_designer_3d_black', NULL, NULL, 0, 0, NOW(), NULL, '0001-01-01 00:00:00', 1, 1, NULL, 701, 1, NULL, NULL, NULL);

INSERT INTO itemgroupentry(id, identifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES
    (1, 'color_red', 1, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
    (2, 'color_black', 2, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO
    item_itemgroupentry(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, itemgroupentry_id, itemgroup_id)
VALUES
    (1, '2020-07-23 10:41:04', '2020-09-07 08:49:21', '0001-01-01 00:00:00', 1, 1, NULL, 702, 1, 1),
    (2, '2020-07-23 10:41:04', '2020-09-07 08:49:21', '0001-01-01 00:00:00', 1, 1, NULL, 703, 2, 1);
