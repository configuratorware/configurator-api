<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\Configuration as ConfigurationDTO;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationData3dScene;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationObjectFinder;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\VisualizationData3dScene as VisualizationData3dSceneStructure;

class VisualizationData3dSceneTest extends TestCase
{
    /**
     * @var VisualizationData3dScene
     */
    private $visualizationData3dScene;

    /**
     * @var  VisualizationObjectFinder|\Phake_IMock
     * @Mock VisualizationObjectFinder
     */
    private $visualizationDataFinder;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->visualizationData3dScene = new VisualizationData3dScene($this->visualizationDataFinder);
    }

    public function testShouldReturnVisualizationData()
    {
        $configuration = new Configuration();
        $configuration->item = (object)['identifier' => '123'];
        $configurationDTO = ConfigurationDTO::from($configuration);
        $baseModelVisualizationData = [1];
        $visualizationdata = [[1], [2], [3], [4]];
        $expected = new VisualizationData3dSceneStructure();
        $expected->sceneData = array_merge(...[$baseModelVisualizationData, $visualizationdata]);

        \Phake::when($this->visualizationDataFinder)->findBaseModelVisualizationObjectFor($configurationDTO)->thenReturn($baseModelVisualizationData);
        \Phake::when($this->visualizationDataFinder)->findVisualizationObjectsFor($configurationDTO)->thenReturn($visualizationdata);

        $actual = $this->visualizationData3dScene->getData($configuration);

        self::assertEquals($expected, $actual);
    }
}
