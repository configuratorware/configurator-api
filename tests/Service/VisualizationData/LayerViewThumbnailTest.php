<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\LayerViewThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Symfony\Component\Filesystem\Filesystem;

class LayerViewThumbnailTest extends TestCase
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var LayerViewThumbnail
     */
    private $layerViewThumbnail;

    /**
     * @var Item
     */
    private $item;

    /**
     * @var Configuration
     */
    private $configuration;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->root = vfsStream::setup('root');
        $this->initMocks();
        $this->layerViewThumbnail = new LayerViewThumbnail(
            $this->filesystem,
            'view_thumb',
            $this->root->url()
        );
    }

    /**
     * Create mock of entities for tests.
     */
    private function initMocks()
    {
        $this->filesystem = new Filesystem();

        $this->item = new Item();
        $this->configuration = new Configuration();
        $this->configuration->item = $this->item;
        $this->configuration->visualizationData = new \stdClass();
    }

    public function testShouldReturnEmptyViewThumbnails(): void
    {
        $this->item->identifier = 'sofa_three_seater';
        $thumbnails = $this->layerViewThumbnail->getThumbnails($this->configuration->item->identifier);

        self::assertSame([], $thumbnails);
    }

    public function testShouldReturnValidViewThumbnails(): void
    {
        $this->createTestFiles();

        $this->item->identifier = 'sofa_three_seater';
        $thumbnails = $this->layerViewThumbnail->getThumbnails($this->configuration->item->identifier);

        self::assertSame(
            [
                'front' => '/view_thumb/sofa_three_seater/sofa_three_seater/front.jpg',
                'rear' => '/view_thumb/sofa_three_seater/sofa_three_seater/rear.png',
            ],
            $thumbnails
        );
    }

    /**
     * Create virtual file structure based on item mock's values.
     */
    private function createTestFiles()
    {
        $viewThumbsDir = $this->root->url() . '/view_thumb/sofa_three_seater/sofa_three_seater/';

        $this->filesystem->dumpFile($viewThumbsDir . 'front.jpg', 'This is a mock file');
        $this->filesystem->dumpFile($viewThumbsDir . 'rear.png', 'This is a mock file');
        $this->filesystem->dumpFile($viewThumbsDir . 'fake.bmp', 'This is a mock file');
        $this->filesystem->dumpFile($viewThumbsDir . 'somethingelse', 'This is a mock file');
    }
}
