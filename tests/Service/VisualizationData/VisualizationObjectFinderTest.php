<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Phake;
use Phake_IMock;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\ComponentCombination;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\OptionCombination;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationObjectFinder;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationResourcesRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\VisualizationObject;

class VisualizationObjectFinderTest extends TestCase
{
    /**
     * @var VisualizationObjectFinder
     */
    private $finder;

    /**
     * @var  VisualizationResourcesRepositoryInterface|Phake_IMock
     * @Mock VisualizationResourcesRepositoryInterface
     */
    private $visualizationDataRepository;

    private $webRoot = 'vfs://root';

    /**
     * @var vfsStreamDirectory
     */
    private $baseDir;

    public function setUp(): void
    {
        $this->baseDir = vfsStream::setup();
        vfsStream::create(['base.gltf' => 'test123', 'base.png' => 'test123', 'base.json' => '{"a1": "b2"}'], $this->baseDir);
        Phake::initAnnotations($this);
        $this->finder = new VisualizationObjectFinder($this->visualizationDataRepository, $this->webRoot);
    }

    public function testShouldFindBaseModelVisualizationObjectForConfiguration()
    {
        $configuration = new Configuration();
        $configuration->itemIdentifier = 'some-identifier';
        $baseDirPath = $this->baseDir->url();
        $resources = [new \SplFileInfo($baseDirPath . '/base.gltf')];

        $expected = [new VisualizationObject([], 'base.gltf', null)];

        \Phake::when($this->visualizationDataRepository)->findBaseModelFor($configuration->itemIdentifier)->thenReturn($resources);

        $actual = $this->finder->findBaseModelVisualizationObjectFor($configuration);

        self::assertEquals($expected, $actual);
    }

    public function testShouldFindMultipleBaseModelFilesForConfiguration()
    {
        vfsStream::create([
            'base.gltf' => 'test123',
            'another-base.gltf' => 'test123',
            'another-another-base.gltf' => 'test123',
            'base.png' => 'test123',
            'some.png' => 'test123',
            'base.json' => '{"a1": "b2"}',
            'another-base.json' => '{"a3": "b4"}',
        ], $this->baseDir);

        $configuration = new Configuration();
        $configuration->itemIdentifier = 'some-identifier';
        $baseDirPath = $this->baseDir->url();
        $resources = [
            new \SplFileInfo($baseDirPath . '/base.gltf'),
            new \SplFileInfo($baseDirPath . '/another-base.gltf'),
            new \SplFileInfo($baseDirPath . '/another-another-base.gltf'),
            new \SplFileInfo($baseDirPath . '/base.json'),
            new \SplFileInfo($baseDirPath . '/another-base.json'),
            new \SplFileInfo($baseDirPath . '/base.png'),
            new \SplFileInfo($baseDirPath . '/some.png'),
        ];

        $expected = [
            new VisualizationObject(['base.png' => 'base.png', 'some.png' => 'some.png'], 'base.gltf', json_decode('{"a1": "b2"}')),
            new VisualizationObject(['base.png' => 'base.png', 'some.png' => 'some.png'], 'another-base.gltf', json_decode('{"a3": "b4"}')),
            new VisualizationObject(['base.png' => 'base.png', 'some.png' => 'some.png'], 'another-another-base.gltf', null),
        ];

        \Phake::when($this->visualizationDataRepository)->findBaseModelFor($configuration->itemIdentifier)->thenReturn($resources);

        $actual = $this->finder->findBaseModelVisualizationObjectFor($configuration);

        self::assertEquals($expected, $actual);
    }

    public function testShouldFindMultipleModelFilesForConfiguration()
    {
        vfsStream::create([
            'base.gltf' => 'test123',
            'another-base.gltf' => 'test123',
            'another-another-base.gltf' => 'test123',
            'base.png' => 'test123',
            'some.png' => 'test123',
            'base.json' => '{"a1": "b2"}',
            'another-base.json' => '{"a3": "b4"}',
        ], $this->baseDir);
        $configuration = new Configuration();
        $configuration->itemIdentifier = 'some-identifier';
        $baseDirPath = $this->baseDir->url();

        $resources = [
            new \SplFileInfo($baseDirPath . '/base.gltf'),
            new \SplFileInfo($baseDirPath . '/another-base.gltf'),
            new \SplFileInfo($baseDirPath . '/another-another-base.gltf'),
            new \SplFileInfo($baseDirPath . '/base.json'),
            new \SplFileInfo($baseDirPath . '/another-base.json'),
            new \SplFileInfo($baseDirPath . '/base.png'),
            new \SplFileInfo($baseDirPath . '/some.png'),
        ];

        $expected = [
            new VisualizationObject(['base.png' => 'base.png', 'some.png' => 'some.png'], 'base.gltf', json_decode('{"a1": "b2"}')),
            new VisualizationObject(['base.png' => 'base.png', 'some.png' => 'some.png'], 'another-base.gltf', json_decode('{"a3": "b4"}')),
            new VisualizationObject(['base.png' => 'base.png', 'some.png' => 'some.png'], 'another-another-base.gltf', null),
        ];

        $componentCombination = \Phake::mock(ComponentCombination::class);
        $optionWithModel = \Phake::mock(OptionCombination::class);

        \Phake::when($this->visualizationDataRepository)->findComponentCombinationsFor($configuration->itemIdentifier)->thenReturn([$componentCombination]);
        \Phake::when($this->visualizationDataRepository)->findOptionsFor($componentCombination, $configuration)->thenReturn([$optionWithModel]);
        \Phake::when($this->visualizationDataRepository)->findVisualizationFilesForOption($optionWithModel)->thenReturn($resources);

        $actual = $this->finder->findVisualizationObjectsFor($configuration);

        self::assertEquals($expected, $actual);
    }

    public function testShouldFindVisualizationObjectsForConfigurationByOptions()
    {
        $configuration = new Configuration();
        $configuration->itemIdentifier = 'some-identifier';
        $baseDirPath = $this->baseDir->url();
        $resources = [new \SplFileInfo($baseDirPath . '/base.gltf')];

        $expected = [new VisualizationObject([], 'base.gltf', null)];

        $componentCombination = \Phake::mock(ComponentCombination::class);
        $optionWithModel = \Phake::mock(OptionCombination::class);

        \Phake::when($this->visualizationDataRepository)->findComponentCombinationsFor($configuration->itemIdentifier)->thenReturn([$componentCombination]);
        \Phake::when($this->visualizationDataRepository)->findOptionsFor($componentCombination, $configuration)->thenReturn([$optionWithModel]);
        \Phake::when($this->visualizationDataRepository)->findVisualizationFilesForOption($optionWithModel)->thenReturn($resources);

        $actual = $this->finder->findVisualizationObjectsFor($configuration);

        self::assertEquals($expected, $actual);
    }

    public function testShouldFindVisualizationObjectsForConfigurationByComponents()
    {
        $configuration = new Configuration();
        $configuration->itemIdentifier = 'some-identifier';
        $baseDirPath = $this->baseDir->url();
        $optionResources = [new \SplFileInfo($baseDirPath . '/base.json')];
        $resources = [new \SplFileInfo($baseDirPath . '/base.gltf')];

        $expected = [
            new VisualizationObject([], null, json_decode('{"a1": "b2"}')),
            new VisualizationObject([], 'base.gltf', null),
        ];

        $componentCombination = \Phake::mock(ComponentCombination::class);
        $optionWithOutModel = \Phake::mock(OptionCombination::class);

        \Phake::when($this->visualizationDataRepository)->findComponentCombinationsFor($configuration->itemIdentifier)->thenReturn([$componentCombination]);
        \Phake::when($this->visualizationDataRepository)->findOptionsFor($componentCombination, $configuration)->thenReturn([$optionWithOutModel]);
        \Phake::when($this->visualizationDataRepository)->findVisualizationFilesForOption($optionWithOutModel)->thenReturn($optionResources);
        \Phake::when($this->visualizationDataRepository)->findVisualizationFilesForComponent($componentCombination)->thenReturn($resources);

        $actual = $this->finder->findVisualizationObjectsFor($configuration);

        self::assertEquals($expected, $actual);
    }
}
