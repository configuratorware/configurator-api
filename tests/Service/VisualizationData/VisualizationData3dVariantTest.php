<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * Class VisualizationData3dVariantTest.
 *
 * @BackupDatabase
 */
class VisualizationData3dVariantTest extends ApiTestCase
{
    /**
     * @var VisualizationData3dVariant
     */
    private $visualizationData3dVariant;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/setUp_for_VisualizationData3dVariantTest.sql'));

        $dirStructure = [
            'public' => [
                'visualization_component' => [
                    'designer' => [
                        'pen_designer_3d' => [
                            'color' => [
                                'color_red' => [
                                    'shaft.png' => 'mock file',
                                    'tip.png' => 'mock file',
                                ],
                                'color_black' => [
                                    'shaft.png' => 'mock file',
                                    'tip.json' => '{"metalnessMap":"metalness.png"}',
                                    'metalness.png' => 'mock file',
                                ],
                            ],
                            'shaft.gltf' => 'mock file',
                            'tip.gltf' => 'mock file',
                            'tip.png' => 'mock file',
                        ],
                    ],
                ],
            ],
        ];
        $this->root = vfsStream::setup('root', null, $dirStructure);

        $visualizationData3d = new VisualizationData3d(
            $this->root->url() . '/public',
            $this->root->url() . '/public/visualization_component/{configurationMode}/{itemIdentifier}'
        );
        $itemRepository = $this->em->getRepository(\Redhotmagma\ConfiguratorApiBundle\Entity\Item::class);
        $itemGroupRepository = $this->em->getRepository(Itemgroup::class);

        $this->visualizationData3dVariant = new VisualizationData3dVariant($visualizationData3d, $itemRepository, $itemGroupRepository);
    }

    /**
     * @requires PHP >= 8.0
     * sorting is different on 7.4
     */
    public function testShouldReturnVisualizationData()
    {
        $item = new Item();
        $item->identifier = 'pen_designer_3d';

        $configuration = new Configuration();
        $configuration->item = $item;

        $visualizationData = $this->visualizationData3dVariant->getData($configuration);

        self::assertEquals(
            json_decode(
                file_get_contents(__DIR__ . '/_data/visualizationData3dVariantTestData_expected.json')
            ),
            json_decode(json_encode($visualizationData->designViews))
        );
    }
}
