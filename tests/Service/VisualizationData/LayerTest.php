<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LayerImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class LayerTest extends ApiTestCase
{
    use ArraySubsetAsserts;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var Layer
     */
    private $layer;

    /**
     * @var LayerImagePathsInterface|\Phake_IMock
     * @Mock LayerImagePathsInterface
     */
    private $layerImagePaths;

    /**
     * @var Item
     */
    private $item;

    /**
     * @var Configuration
     */
    private $configuration;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->layer = new Layer($this->layerImagePaths);
        $this->mockObjects();
    }

    /**
     * Create mock of entities for tests.
     */
    private function mockObjects()
    {
        $this->filesystem = new Filesystem();

        $this->item = new Item();
        $this->configuration = new Configuration();
        $this->configuration->item = $this->item;
        $this->configuration->visualizationData = new \stdClass();
        $this->configuration->optionclassifications = json_decode(
            file_get_contents(__DIR__ . '/_data/mock_optionClassification.json'),
            false
        );
    }

    public function testShouldReturnViewImageData(): void
    {
        $this->item->identifier = 'sofa_three_seater';
        $this->item->parentItemIdentifier = 'sofa_three_seater';
        $this->createFileStructure();

        $this->layer->addViewImages($this->configuration);
        $visualizationData = $this->configuration->visualizationData;

        self::assertArraySubset(
            json_decode(
                file_get_contents(__DIR__ . '/_data/layerVisualizationData_viewImages.json'),
                true
            ),
            json_decode(json_encode($visualizationData), true) // conversion to match structure
        );
    }

    /**
     * Create virtual file structure based on item mock's values.
     */
    private function createFileStructure()
    {
        $dirStructure = [
            'images' => [
                'configuratorimages' => [
                    'sofa_three_seater' => [
                        '01_front' => [
                            '01_base' => [],
                            '02_seat' => [],
                            '03_back' => [],
                            '04_pillow' => [],
                        ],
                        '02_frontleft' => [
                            '01_base' => [],
                            '02_seat' => [],
                            '03_back' => [],
                            '04_pillow' => [],
                        ],
                        '03_left' => [
                            '01_base' => [],
                            '02_seat' => [],
                            '03_back' => [],
                            '04_pillow' => [],
                        ],
                    ],
                ],
            ],
        ];

        $this->root = vfsStream::setup('root', null, $dirStructure);

        \Phake::when($this->layerImagePaths)->getLayerImagePath()->thenReturn($this->root->url() . '/images/configuratorimages/');
        \Phake::when($this->layerImagePaths)->getLayerImagePathRelative()->thenReturn('images/configuratorimages');

        // add view files
        foreach ($dirStructure['images']['configuratorimages']['sofa_three_seater'] as $view => $viewDirectories) {
            foreach ($viewDirectories as $optionClassification => $images) {
                $this->filesystem->dumpFile(
                    $this->root->url() . '/images/configuratorimages/sofa_three_seater/' . $view . '/' .
                    $optionClassification . '/010010018001.png',
                    'This is a mock file'
                );
            }
        }
    }
}
