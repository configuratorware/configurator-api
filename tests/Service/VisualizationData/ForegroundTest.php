<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemGroupingWalker;
use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ForegroundTest extends ApiTestCase
{
    use ArraySubsetAsserts;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var Foreground
     */
    private $foreground;

    /**
     * @var MediaHelper|\Phake_IMock
     * @Mock MediaHelper
     */
    private $mediaHelper;

    /**
     * @var Item
     */
    private $item;

    /**
     * @var Configuration
     */
    private $configuration;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->foreground = new Foreground(
            $this->mediaHelper,
            new FrontendItemGroupingWalker(),
            new Filesystem()
        );
        $this->mockObjects();
    }

    /**
     * Create mock of entities for tests.
     */
    private function mockObjects()
    {
        $this->filesystem = new Filesystem();

        $this->item = new Item();
        $this->item->itemGroup = [];
        $this->configuration = new Configuration();
        $this->configuration->item = $this->item;
        $this->configuration->visualizationData = new \stdClass();
    }

    public function testShouldReturnForegroundDataForItemWithChildren(): void
    {
        $this->item->identifier = 'sofa_three_seater';
        $this->item->parentItemIdentifier = 'sofa_three_seater_parent';

        for ($i = 1; $i < 4; ++$i) {
            $childItem = new Item();
            $childItem->identifier = 'sofa_three_seater_child_' . $i;
            $this->item->itemGroup[] = $childItem;
        }

        $this->createFileStructure();

        $this->foreground->addForegroundImages($this->configuration);
        $visualizationData = $this->configuration->visualizationData;

        self::assertArraySubset(
            json_decode(
                file_get_contents(__DIR__ . '/_data/layerVisualizationData_foreground_item_with_children.json'),
                true
            ),
            json_decode(json_encode($visualizationData), true) // conversion to match structure
        );
    }

    public function testShouldReturnForegroundDataForItemWithoutChildren(): void
    {
        $this->item->identifier = 'sofa_three_seater';
        $this->item->parentItemIdentifier = 'sofa_three_seater';
        $this->createFileStructure();

        $this->foreground->addForegroundImages($this->configuration);
        $visualizationData = $this->configuration->visualizationData;

        self::assertArraySubset(
            json_decode(
                file_get_contents(__DIR__ . '/_data/layerVisualizationData_foreground_item_without_children.json'),
                true
            ),
            json_decode(json_encode($visualizationData), true) // conversion to match structure
        );
    }

    /**
     * Create virtual file structure based on item mock's values.
     */
    private function createFileStructure()
    {
        $dirStructure = [
            'images' => [
                'item' => [
                    'foreground' => [
                        $this->item->parentItemIdentifier => [
                            '01_front' => [],
                            '02_frontleft' => [],
                            '03_left' => [],
                        ],
                    ],
                ],
            ],
        ];

        $this->root = vfsStream::setup('root', null, $dirStructure);

        \Phake::when($this->mediaHelper)->getImageBasePath()->thenReturn($this->root->url() . '/images/');
        \Phake::when($this->mediaHelper)->getBasePath()->thenReturn($this->root->url());

        // add foreground files
        $viewsNamesArray = array_keys($dirStructure['images']['item']['foreground'][$this->item->parentItemIdentifier]);
        foreach ($viewsNamesArray as $viewIdentifier) {
            $mockFileDir = $this->root->url() . '/images/item/foreground/' . $this->item->parentItemIdentifier . '/' .
                $viewIdentifier;

            if ($this->item->identifier !== $this->item->parentItemIdentifier) {
                $this->filesystem->dumpFile(
                    $mockFileDir . '/sofa_three_seater_parent.png',
                    'This is a mock file'
                );
                $this->filesystem->dumpFile(
                    $mockFileDir . '/sofa_three_seater_child_2.png',
                    'This is a mock file'
                );
                $this->filesystem->dumpFile(
                    $mockFileDir . '/sofa_three_seater_child_3.png',
                    'This is a mock file'
                );
            } else {
                $this->filesystem->dumpFile(
                    $mockFileDir . '/sofa_three_seater.png',
                    'This is a mock file'
                );
            }
        }
    }
}
