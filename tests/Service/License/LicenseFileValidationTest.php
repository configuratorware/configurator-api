<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\License;

use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LicensePathInterface;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class LicenseFileValidationTest extends ApiTestCase
{
    /**
     * @param string      $fileName
     * @param array       $expectedFeatures
     * @param string|null $expectedProduct
     *
     * @dataProvider licenseFileDataProvider
     */
    public function testLicenseFile(string $fileName, array $expectedFeatures, ?string $expectedProduct): void
    {
        $validator = new LicenseFileValidator(new LicenseFilePathMock($fileName), new LicenseChecker(), new Filesystem());

        self::assertSame($expectedFeatures, $validator->getFeatures());
        self::assertSame($expectedProduct, $validator->getValidProduct());
    }

    /**
     * @return array[]
     */
    public function licenseFileDataProvider(): array
    {
        return [
            ['license_creator', [], 'creator'],
            ['license_designer', [], 'designer'],
            ['license_creator_designer', [], 'creator+designer'],
            ['license_invalid_hash', [], null],
            ['license_invalid_product', [], null],
            ['license_empty', [], null],
            ['license_missing', [], null],
            ['license_creator_designer_feature_clients', ['clients'], 'creator+designer'],
            ['license_creator_feature_clients_unknown_features', ['clients'], 'creator'],
        ];
    }
}

final class LicenseFilePathMock implements LicensePathInterface
{
    /**
     * @var string
     */
    private $fileName;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    public function getLicenseFilePath(): string
    {
        return __DIR__ . '/_data/' . $this->fileName . '.txt';
    }
}
