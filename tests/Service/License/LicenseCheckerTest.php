<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\License;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseChecker;

final class LicenseCheckerTest extends TestCase
{
    private $licenseChecker;

    public function setUp(): void
    {
        $this->licenseChecker = new LicenseChecker();
    }

    /**
     * @param string $licenseFileContents
     *
     * @dataProvider providerShouldCheckOnValidLicence
     */
    public function testShouldCheckOnValidLicence(string $licenseFileContents): void
    {
        self::assertNotNull($this->licenseChecker->getLicensedProduct($licenseFileContents));
    }

    public function providerShouldCheckOnValidLicence(): \Generator
    {
        yield ['designer###meinshop;mein-shop;redhotmagma ' . chr(32) . PHP_EOL . '056b92a53dc30ce5e4a98d4be65a90c154bf90980d2f195c29029c51e4905448'];
    }

    /**
     * @dataProvider productCombinationProvider
     *
     * @param bool   $expectedResult
     * @param string $productCombination
     */
    public function testShouldValidateProductCombination(string $productCombination, bool $expectedResult): void
    {
        self::assertEquals($expectedResult, $this->licenseChecker->isValidProductCombination($productCombination));
    }

    public function productCombinationProvider(): array
    {
        return [
            ['creator', true],
            ['designer', true],
            ['finder', true],
            ['creator+designer', true],
            ['creator+finder', true],
            ['designer+finder', true],
            ['creator+designer+finder', true],
            ['creator+designer+finder+other', false],
            ['other', false],
            ['', false],
        ];
    }
}
