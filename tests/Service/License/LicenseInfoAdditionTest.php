<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\License;

use org\bovigo\vfs\vfsStream;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\PathsMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * @MockPaths
 */
class LicenseInfoAdditionTest extends ApiTestCase
{
    use VfsTestTrait;

    /**
     * @var \org\bovigo\vfs\vfsStreamDirectory
     */
    private $root;

    public function setUp(): void
    {
        parent::setUp();
        $this->root = vfsStream::setup('root', 0777, ['license' => [], 'media' => ['images' => []]]);
    }

    public function testLicenseInfoPresenceOnLoadByConfigurationCodeCall(): void
    {
        $this->addLicenseFileWithoutFeatures();

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/frontendapi/configuration/loadbyconfigurationcode/defaultoptions_1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        $result = json_decode($response->getContent());
        self::assertSame('creator+designer', $result->license);
    }

    public function testLicenseInfoPresenceOnLoadByItemIdentifierCall(): void
    {
        $this->addLicenseFileWithoutFeatures();

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/frontendapi/configuration/loadbyitemidentifier/sheep');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        $result = json_decode($response->getContent());
        self::assertSame('creator+designer', $result->license);
    }

    public function testLicenseInfoPresenceOnAdminAuthentication(): void
    {
        $this->addLicenseFileWithoutFeatures();

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/login_check', [], [], ['CONTENT_TYPE' => 'application/json'], '{"username": "admin", "password": "admin"}');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        $result = json_decode($response->getContent());
        self::assertSame('creator+designer', $result->license);
        self::assertNotContains('clients', $result->credentials);
    }

    public function testCredentialsPresenceOnAdminAuthentication(): void
    {
        $this->addLicenseFileWithFeatures();

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/login_check', [], [], ['CONTENT_TYPE' => 'application/json'], '{"username": "admin", "password": "admin"}');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        $result = json_decode($response->getContent());
        self::assertContains('clients', $result->credentials);
    }

    private function addLicenseFileWithoutFeatures(): void
    {
        // reset test license file contents
        $licenseFileContent = <<<LICENSE
creator+designer###only-one-domain
63972c643c2a9abac40da2fd9f4a39d8e145a2667512e5a128b1f7d5332b9847
LICENSE;

        file_put_contents(PathsMock::MOCKED_LICENSE_PATH, $licenseFileContent);
    }

    private function addLicenseFileWithFeatures(): void
    {
        // reset test license file contents
        $licenseFileContent = <<<LICENSE
creator###meinshop;mein-shop;redhotmagma###1feature;2feature;clients;geilfeature;megafeature
5c95827a162b89ea78baf87c3a0a0addd289e4eaac7d62024f6212ceef461f93
LICENSE;

        file_put_contents(PathsMock::MOCKED_LICENSE_PATH, $licenseFileContent);
    }
}
