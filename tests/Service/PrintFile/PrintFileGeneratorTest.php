<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\PrintFile;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class PrintFileGeneratorTest extends TestCase
{
    /**
     * @var ConfigurationLoad|\Phake_IMock
     * @Mock ConfigurationLoad
     */
    private $configurationLoad;

    /**
     * @var ConfigurationRepository|\Phake_IMock
     * @Mock ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var Filesystem|\Phake_IMock
     * @Mock Filesystem
     */
    private $fileSystem;

    /**
     * @var PrintFileGenerator
     */
    private $printFileGenerator;

    /**
     * @var ProcessFactory|\Phake_IMock
     * @Mock ProcessFactory
     */
    private $processFactory;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->printFileGenerator = new PrintFileGenerator(
            $this->configurationLoad,
            $this->configurationRepository,
            $this->fileSystem,
            '',
            __DIR__ . '/_data',
            $this->processFactory
        );
    }

    /**
     * @param string $configurationCode
     * @param string $updatedDate
     * @param array $designAreas
     *
     * @dataProvider providerShouldReturnImages
     */
    public function testShouldReturnImages(
        string $configurationCode,
        array $designAreas,
        string $updatedDate
    ) {
        \Phake::when($this->configurationLoad)->loadByCode($configurationCode,
            \Phake::ignoreRemaining())->thenReturn($this->getConfiguration($designAreas));
        \Phake::when($this->configurationRepository)->findOneBy(['code' => $configurationCode])->thenReturn($this->getRepository($updatedDate));
        \Phake::whenStatic($this->processFactory)->createWithDefaults->thenReturn($this->getProcess());

        $response = $this->printFileGenerator->generate($configurationCode);

        self::assertSame($this->getExpectedResult(), $response, 'Response Array is not valid.');
    }

    /**
     * @param string $configurationCode
     * @param string $updatedDate
     * @param array $designAreas
     *
     * @dataProvider providerShouldThrowException
     */
    public function testShouldThrowException(
        string $configurationCode,
        array $designAreas,
        string $updatedDate
    ) {
        $this->expectException(Exception::class);

        \Phake::when($this->configurationLoad)->loadByCode($configurationCode,
            \Phake::ignoreRemaining())->thenReturn($this->getConfiguration($designAreas));
        \Phake::when($this->configurationRepository)->findOneBy(['code' => $configurationCode])->thenReturn($this->getRepository($updatedDate));
        \Phake::whenStatic($this->processFactory)->createWithDefaults->thenReturn($this->getProcess());

        $this->printFileGenerator->generate($configurationCode);
    }

    private function getConfiguration(array $designAreas)
    {
        $frontendConfiguration = new FrontendConfigurationStructure();
        $frontendConfiguration->designdata = new \stdClass();

        foreach ($designAreas as $designArea) {
            $frontendConfiguration->designdata->$designArea = '';
        }

        return $frontendConfiguration;
    }

    private function getRepository(string $updatedDate)
    {
        $updatedDate = new \DateTime($updatedDate);

        $configuration = \Phake::mock(Configuration::class);
        \Phake::when($configuration)->getDateUpdated->thenReturn($updatedDate);

        return $configuration;
    }

    private function getProcess()
    {
        $process = \Phake::mock(Process::class);
        \Phake::when($process)->run()->thenReturn(1);
        \Phake::when($process)->isSuccessful()->thenReturn(true);

        return $process;
    }

    public function providerShouldReturnImages()
    {
        return [
            ['default_hoodie', ['back', 'front'], '2019-01-01 00:00:00'],
        ];
    }

    public function providerShouldThrowException()
    {
        return [
            ['default_hoodie', ['missing1', 'missing2'], '2019-01-01 00:00:00'],
            ['default_hoodie', ['back', 'front'], '3020-01-01 00:00:00'],
        ];
    }

    private function getExpectedResult()
    {
        return [
            'back' => __DIR__ . '/_data/print_file_default_hoodie/back.png',
            'front' => __DIR__ . '/_data/print_file_default_hoodie/front.png',
        ];
    }
}
