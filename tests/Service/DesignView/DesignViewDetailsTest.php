<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewDetail;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemRelation;

class DesignViewDetailsTest extends TestCase
{
    /**
     * @var DesignViewImage|\Phake_IMock
     * @Mock DesignViewImage
     */
    private $designViewImage;

    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var Configuration|\Phake_IMock
     * @Mock Configuration
     */
    private $defaultConfiguration;

    /**
     * @var DesignView|\Phake_IMock
     * @Mock DesignView
     */
    private $structure;

    /**
     * @var ItemRelation|\Phake_IMock
     * @Mock ItemRelation
     */
    private $item;

    /**
     * @var DesignViewDetail
     */
    private $designViewDetail;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->designViewDetail = new DesignViewDetail(
            $this->designViewImage,
            $this->itemRepository
        );

        $this->structure->item = $this->item;
        $this->structure->identifier = 'mock_identifier';
        $this->structure->item->identifier = 'mock_identifier';

        \Phake::when($this->itemRepository)->getItemsByParent
            ->thenReturn([]);

        \Phake::when($this->defaultConfiguration)->getCode
            ->thenReturn('mock_code');
    }

    public function testViewImagesDataShouldBeFilled()
    {
        // mock view images
        \Phake::when($this->designViewImage)->getImages->thenReturn($this->getViewImagesMockData());

        $this->designViewDetail->addImageData($this->structure);
        self::assertEquals($this->getViewImagesMockData(), $this->structure->images);
    }

    private function getViewImagesMockData(): array
    {
        return [
            'demo_hoodie_blue_s' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_blue_s.jpg',
            'demo_hoodie_blue_m' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_blue_m.jpg',
            'demo_hoodie_blue_l' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_blue_l.jpg',
            'demo_hoodie_blue_xl' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_blue_xl.jpg',
            'demo_hoodie_green_s' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_green_s.jpg',
            'demo_hoodie_green_m' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_green_m.jpg',
            'demo_hoodie_green_l' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_green_l.jpg',
            'demo_hoodie_green_xl' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_green_xl.jpg',
            'demo_hoodie_red_s' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_red_s.jpg',
            'demo_hoodie_red_m' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_red_m.jpg',
            'demo_hoodie_red_l' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_red_l.jpg',
            'demo_hoodie_red_xl' => '/images/item/view/demo_hoodie/front_view/demo_hoodie_red_xl.jpg',
        ];
    }

    private function getLayerImagesMockDataJson(): string
    {
        return '{
            "viewImages": {
                "03_left": [
                    "/images/configuratorimages/sofa_three_seater/03_left/01_base/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/03_left/02_seat/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/03_left/03_back/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/03_left/04_pillow/010010018001.png"
                ],
                "02_frontleft": [
                    "/images/configuratorimages/sofa_three_seater/02_frontleft/01_base/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/02_frontleft/02_seat/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/02_frontleft/03_back/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/02_frontleft/04_pillow/010010018001.png"
                ],
                "01_front": [
                    "/images/configuratorimages/sofa_three_seater/01_front/01_base/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/01_front/02_seat/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/01_front/03_back/010010018001.png",
                    "/images/configuratorimages/sofa_three_seater/01_front/04_pillow/010010018001.png"
                ]
            }
        }';
    }
}
