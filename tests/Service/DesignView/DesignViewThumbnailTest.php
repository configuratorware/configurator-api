<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewThumbnail;
use Symfony\Component\Filesystem\Filesystem;

class DesignViewThumbnailTest extends TestCase
{
    /**
     * @var DesignViewThumbnail
     */
    private $designViewThumbnail;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    protected function setUp(): void
    {
        $this->root = vfsStream::setup();
        $this->fileSystem = new Filesystem();
        $this->designViewThumbnail = new DesignViewThumbnail(new DesignViewPathsMock());
    }

    /**
     * @param bool $createParent
     * @param array $createFilesForItemIdentifiers
     * @param array $itemIdentifiers
     * @param string $fileExtension
     * @param array|null $expected
     *
     * @dataProvider shouldReturnImagesDataProvider
     */
    public function testShouldReturnImages(
        bool $createParent,
        array $createFilesForItemIdentifiers,
        array $itemIdentifiers,
        string $fileExtension,
        ?array $expected
    ) {
        $parent = 'parent';
        $view = 'view';

        if ($createParent) {
            $this->fileSystem->dumpFile(
                $this->root->url() . '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/' . $parent . '/' . $view . '.' . $fileExtension,
                'This is a mock file:' . $parent
            );
        }

        foreach ($createFilesForItemIdentifiers as $childIdentifier) {
            $this->fileSystem->dumpFile(
                $this->root->url() . '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/' . $parent . '/' . $childIdentifier . '/' . $view . '.' . $fileExtension,
                'This is a mock file:' . $childIdentifier
            );
        }

        $images = $this->designViewThumbnail->getImages($view, $parent, $itemIdentifiers);

        self::assertEquals($expected, $images);
    }

    /**
     * @return array
     */
    public function shouldReturnImagesDataProvider()
    {
        return [
            [
                true,
                ['child_1', 'child_2', 'child_3', 'child_4', 'child_5'],
                ['child_1', 'child_2', 'child_3', 'child_4', 'child_5'],
                'png',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_1/view.png',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_2/view.png',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_3/view.png',
                    'child_4' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_4/view.png',
                    'child_5' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_5/view.png',
                ],
            ],
            [
                true,
                ['child_1', 'child_2', 'child_3'],
                ['child_1', 'child_2', 'child_3'],
                'jpg',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_1/view.jpg',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_2/view.jpg',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_3/view.jpg',
                ],
            ],
            [
                true,
                ['child_1', 'child_2', 'child_3', 'child_4', 'child_5'],
                ['child_1', 'child_2', 'child_3'],
                'png',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_1/view.png',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_2/view.png',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_3/view.png',
                ],
            ],
            [
                true,
                [],
                ['child_1', 'child_2', 'child_3'],
                'jpg',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/view.jpg',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/view.jpg',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/view.jpg',
                ],
            ],
            [
                false,
                [],
                ['child_1', 'child_2', 'child_3'],
                'jpg',
                [],
            ],
            [
                false,
                ['child_1'],
                ['child_1', 'child_2', 'child_3'],
                'jpg',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_1/view.jpg',
                ],
            ],
            [
                true,
                ['child_1'],
                ['child_1', 'child_2', 'child_3'],
                'jpg',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/child_1/view.jpg',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/view.jpg',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_THUMBNAIL_PATH . '/parent/view.jpg',
                ],
            ],
        ];
    }
}
