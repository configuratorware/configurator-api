<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\DesignViewDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\DesignViewDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewImage;
use Symfony\Component\Filesystem\Filesystem;

class DesignViewImageTest extends TestCase
{
    /**
     * @var DesignViewDetailImage
     */
    private $designViewImage;

    /**
     * @var DesignViewDetailImageRepositoryInterface|\Phake_IMock
     * @Mock DesignViewDetailImageRepositoryInterface
     */
    private $designViewDetailImageRepository;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->root = vfsStream::setup();
        $this->fileSystem = new Filesystem();
        $this->designViewImage = new DesignViewImage(new DesignViewPathsMock(), $this->designViewDetailImageRepository);
    }

    public function testShouldSaveImageForParent(): void
    {
        $expected = DesignViewDetailImage::from(null, 'view', 'parent', 'parent', 'realPath', 'filename');

        $this->designViewImage->uploadDetailImage($this->generateParentItem('parent'), $this->generateDesignView('view'), 'realPath', 'filename');

        \Phake::verify($this->designViewDetailImageRepository)->saveDetailImage(\Phake::equalTo($expected));
    }

    public function testShouldSaveImageForChild(): void
    {
        $expected = DesignViewDetailImage::from(null, 'view', 'parent', 'child', 'realPath', 'filename');

        $this->designViewImage->uploadDetailImage($this->generateChildItem('parent', 'child'), $this->generateDesignView('view'), 'realPath', 'filename');

        \Phake::verify($this->designViewDetailImageRepository)->saveDetailImage(\Phake::equalTo($expected));
    }

    public function testShouldDeleteDetailImage(): void
    {
        $expected = DesignViewDetailImage::from('test/url.png', 'view', 'parent', 'child', 'realPath', 'filename.png');

        $item = $this->generateChildItem('parent', 'child');
        $designView = $this->generateDesignView('view');
        \Phake::when($this->designViewDetailImageRepository)->findDetailImage($item, $designView)->thenReturn($expected);

        $this->designViewImage->deleteDetailImage($item, $designView, 'realPath', 'filename');

        \Phake::verify($this->designViewDetailImageRepository)->deleteDetailImage(\Phake::equalTo($expected));
    }

    /**
     * @param array $createFilesForItemIdentifiers
     * @param array $itemIdentifiers
     * @param string $fileExtension
     * @param array|null $expected
     * @dataProvider dataProviderShouldReturnChildImages
     */
    public function testShouldReturnChildImages(
        array $createFilesForItemIdentifiers,
        array $itemIdentifiers,
        string $fileExtension,
        array $expected
    ): void {
        $parent = 'parent';
        $view = 'view';

        foreach ($createFilesForItemIdentifiers as $childIdentifier) {
            $this->fileSystem->dumpFile(
                $this->root->url() . '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/' . $parent . '/' . $view . '/' . $childIdentifier . '.' . $fileExtension,
                'This is a mock file:' . $childIdentifier
            );
        }

        $images = $this->designViewImage->getImages($view, $parent, $itemIdentifiers);

        self::assertEquals($expected, $images);
    }

    /**
     * @return array
     */
    public function dataProviderShouldReturnChildImages(): array
    {
        return [
            [
                ['child_1', 'child_2', 'child_3', 'child_4', 'child_5'],
                ['child_1', 'child_2', 'child_3', 'child_4', 'child_5'],
                'png',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_1.png',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_2.png',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_3.png',
                    'child_4' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_4.png',
                    'child_5' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_5.png',
                ],
            ],
            [
                ['child_1', 'child_2', 'child_3'],
                ['child_1', 'child_2', 'child_3'],
                'jpg',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_1.jpg',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_2.jpg',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_3.jpg',
                ],
            ],

            [
                ['child_1', 'child_2', 'child_3'],
                ['child_1', 'child_2', 'child_3', 'child_4', 'child_5'],
                'png',
                [
                    'child_1' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_1.png',
                    'child_2' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_2.png',
                    'child_3' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/child_3.png',
                ],
            ],
        ];
    }

    /**
     * @param string $fileExtension
     * @param array $expected
     * @dataProvider dataProviderShouldReturnParentImage
     */
    public function testShouldReturnParentImage(string $fileExtension, array $expected): void
    {
        $parent = 'parent';
        $view = 'view';

        $this->fileSystem->dumpFile(
            $this->root->url() . '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/' . $parent . '/' . $view . '/' . $parent . '.' . $fileExtension,
            'This is a mock file:' . $parent
        );

        $images = $this->designViewImage->getImages($view, $parent, []);

        self::assertEquals($expected, $images);
    }

    /**
     * @return array
     */
    public function dataProviderShouldReturnParentImage(): array
    {
        return [
            [
                'png',
                [
                    'parent' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/parent.png',
                ],
            ],
            [
                'jpg',
                [
                    'parent' => '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/parent/view/parent.jpg',
                ],
            ],
        ];
    }

    /**
     * @param array $createFilesForItemIdentifiers
     * @param array $itemIdentifiers
     * @param string $fileExtension
     * @dataProvider dataProviderShouldReturnEmpty
     */
    public function testShouldReturnEmpty(
        array $createFilesForItemIdentifiers,
        array $itemIdentifiers,
        string $fileExtension
    ): void {
        $parent = 'parent';
        $view = 'view';

        foreach ($createFilesForItemIdentifiers as $childIdentifier) {
            $this->fileSystem->dumpFile(
                $this->root->url() . '/' . DesignViewPathsMock::DESIGN_VIEW_IMAGE_PATH . '/' . $parent . '/' . $view . '/' . $childIdentifier . '.' . $fileExtension,
                'This is a mock file:' . $childIdentifier
            );
        }

        $images = $this->designViewImage->getImages($view, $parent, $itemIdentifiers);

        self::assertEmpty($images);
    }

    /**
     * @return array
     */
    public function dataProviderShouldReturnEmpty(): array
    {
        return [
            [
                [],
                [],
                'jpg',
            ],
        ];
    }

    private function generateChildItem(string $parentIdentifier, string $childIdentifier): Item
    {
        $parent = (new Item())->setIdentifier($parentIdentifier);
        $child = (new Item())->setIdentifier($childIdentifier);

        $child->setParent($parent);

        return $child;
    }

    private function generateParentItem(string $identifier): Item
    {
        return (new Item())->setIdentifier($identifier);
    }

    private function generateDesignView(string $identifier): DesignView
    {
        return new DesignView($identifier);
    }
}
