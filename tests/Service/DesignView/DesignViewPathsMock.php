<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignViewImagePathsInterface;

final class DesignViewPathsMock implements DesignViewImagePathsInterface
{
    public const DESIGN_VIEW_IMAGE_PATH = 'images/item/view';
    public const DESIGN_VIEW_THUMBNAIL_PATH = 'images/item/view_thumb';

    public function getDesignViewImagePath(): string
    {
        return 'vfs://root' . '/' . $this->getDesignViewImagePathRelative();
    }

    public function getDesignViewImagePathRelative(): string
    {
        return self::DESIGN_VIEW_IMAGE_PATH;
    }

    public function getDesignViewThumbnailPath(): string
    {
        return 'vfs://root' . '/' . $this->getDesignViewThumbnailPathRelative();
    }

    public function getDesignViewThumbnailPathRelative(): string
    {
        return self::DESIGN_VIEW_THUMBNAIL_PATH;
    }
}
