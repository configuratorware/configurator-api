<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\FrontendTranslationFile;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Validator\FrontendTranslationFileValidator;
use Symfony\Component\Filesystem\Filesystem;

class FrontendTranslationFileValidatorTest extends TestCase
{
    /**
     * @var FrontendTranslationFileValidator
     */
    private $translationValidator;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $root;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->root = vfsStream::setup(
            'root',
            null,
            [
                'translations',
                'translations_custom',
                'translations_cached',
            ]
        )->url();

        $this->filesystem = new Filesystem();
        $this->filesystem->dumpFile($this->root . '/translations/de_DE.json', '{"existing_key":"testvalue_base"}');
        $this->filesystem->dumpFile($this->root . '/translations_custom/de_DE.json', '{"existing_key":"testvalue_custom"}');
        $this->filesystem->dumpFile($this->root . '/translations_cached/messages.de_DE.json', '{"existing_key":"testvalue_cached"}');

        $this->translationValidator = new FrontendTranslationFileValidator($this->root);
    }

    public function testInvalidLangCodeError()
    {
        try {
            $this->translationValidator->validate('de_DE', 'this is not a json');
        } catch (ValidationException $exception) {
            self::assertSame(['invalid_json_file'], $exception->getViolations());

            return;
        }

        $this->fail('Expected exception not thrown');
    }

    public function testInvalidJsonError()
    {
        try {
            $this->translationValidator->validate('en_GB', '{}');
        } catch (ValidationException $exception) {
            self::assertSame(['empty_translation_file'], $exception->getViolations());

            return;
        }

        $this->fail('Expected exception not thrown');
    }

    public function testSuccessfulValidation()
    {
        $this->translationValidator->validate('de_DE', '{"existing_key":"valid value"}');
        $this->addToAssertionCount(1);
    }

    public function testSuccessfulBOMValidation()
    {
        $bomContent = file_get_contents(__DIR__ . '/_data/bom_translation.json');
        $this->translationValidator->validate('de_DE', $bomContent);
        $this->addToAssertionCount(1);
    }
}
