<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChildItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;

class ItemValidatorTest extends TestCase
{
    /**
     * @var StructureValidator|\Phake_IMock
     * @Mock StructureValidator
     */
    private $structureValidator;

    /**
     * @var ItemValidator
     */
    private $itemValidator;

    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemgroupRepository|\Phake_IMock
     * @Mock ItemgroupRepository
     */
    private $groupRepository;

    /**
     * @var ItemgroupentryRepository|\Phake_IMock
     * @Mock ItemgroupentryRepository
     */
    private $groupEntryRepository;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        \Phake::when($this->structureValidator)->validate->thenReturn([]);

        $this->itemValidator = new ItemValidator($this->structureValidator, $this->itemRepository, $this->groupRepository, $this->groupEntryRepository);
    }

    public function testItemGroupExceptions(): void
    {
        $item = new Item();
        $childItem = new ChildItem();
        $item->children = [$childItem];
        $itemGroup = new GroupRelation();
        $childItem->groups = [$itemGroup];
        $groupRelation = new GroupRelation();
        $itemGroup->groupEntry = $groupRelation;

        $exception = null;

        try {
            $this->itemValidator->validate($item);
        } catch (ValidationException $exception) {
        }

        self::assertNotNull($exception);
        self::assertSame(
            [
                [
                    'property' => 'item.children[].groups[0].id',
                    'invalidvalue' => null,
                    'message' => 'no_relation',
                ],
                [
                    'property' => 'item.children[].groups[0].groupEntry.id',
                    'invalidvalue' => null,
                    'message' => 'no_relation',
                ],
            ],
            $exception->getViolations()
        );
    }
}
