<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;
use Redhotmagma\ConfiguratorApiBundle\Validator\UserValidator;

class UserValidatorTest extends TestCase
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserValidator
     */
    private $userValidator;

    public function setUp(): void
    {
        $this->structureValidator = $this->getMockBuilder(StructureValidator::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->userRepository = $this->getMockBuilder(UserRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->userValidator = new UserValidator($this->structureValidator, $this->userRepository);
    }

    public function testUserIsValid(): void
    {
        $this->structureValidator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $user = new User();
        $user->email = 'test@redhotmagma.de';
        $user->username = 'test';

        // the validation returns nothing, so there is no error when there is nothing returned
        self::assertNull($this->userValidator->validate($user));
    }

    public function testUserIsDuplicate(): void
    {
        $userEntity = new \Redhotmagma\ConfiguratorApiBundle\Entity\User();

        $this->structureValidator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->userRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($userEntity);

        $user = new User();
        $user->email = 'admin@redhotmagma.de';
        $user->username = 'admin';

        $this->expectException(ValidationException::class);

        $this->userValidator->validate($user);
    }
}
