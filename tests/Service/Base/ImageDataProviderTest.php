<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Base;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageDataProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageModel;

class ImageDataProviderTest extends TestCase
{
    protected const VECTORIZED_PNG_IMAGE = __DIR__ . '/_data/ImageDataProviderTest/vectorized_png.svg';

    private ImageDataProvider $imageDataProvider;

    protected function setUp(): void
    {
        $this->imageDataProvider = new ImageDataProvider();
    }

    public function testShouldCreateImageFromPathWithoutError(): void
    {
        $actual = $this->imageDataProvider->provide(self::VECTORIZED_PNG_IMAGE);

        self::assertInstanceOf(ImageModel::class, $actual);
    }
}
