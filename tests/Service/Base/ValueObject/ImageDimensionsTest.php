<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject\ImageDimensions;

class ImageDimensionsTest extends TestCase
{
    protected const VECTOR_IMAGE = __DIR__ . '/_data/ImageDimensionsTest/vectorized_png.svg';
    protected const PNG_IMAGE = __DIR__ . '/_data/ImageDimensionsTest/rasterized_image.png';

    /**
     * @dataProvider fileProvider
     */
    public function testShouldReturnDimensionsFromImagePath($filePath, $expected): void
    {
        $actual = ImageDimensions::fromPath($filePath);

        self::assertEquals($expected, $actual);
    }

    public function fileProvider(): array
    {
        return [
            'png image' => [self::PNG_IMAGE, [187, 234]],
            'svg image' => [self::VECTOR_IMAGE, [512, 512]],
        ];
    }

    public function testShouldReturnGivenDefaultDimensionsFromSVGImage(): void
    {
        $actual = ImageDimensions::fromPath(self::VECTOR_IMAGE, 10, 10);

        self::assertEquals([512, 512], $actual);
    }
}
