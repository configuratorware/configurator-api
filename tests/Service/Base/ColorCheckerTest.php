<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ColorChecker;

class ColorCheckerTest extends TestCase
{
    /**
     * @var ColorChecker
     */
    private $colorChecker;

    protected function setUp(): void
    {
        $this->colorChecker = new ColorChecker();
    }

    /**
     * @dataProvider provideCorrectHexColors
     */
    public function testCorrectHexColors($colorString, $normalizedHexColor)
    {
        self::assertTrue($this->colorChecker->isHexColor($colorString));
        self::assertSame($normalizedHexColor, $this->colorChecker->formatHexColor($colorString));
    }

    /**
     * @dataProvider provideFaultyHexColors
     */
    public function testFaultyHexColors($colorString)
    {
        self::assertFalse($this->colorChecker->isHexColor($colorString));
    }

    public function provideCorrectHexColors()
    {
        return [
            ['#000000', '#000000'],
            ['#ffffff', '#ffffff'],
            ['#AAAAAA', '#aaaaaa'],
            ['#01BC56', '#01bc56'],
            ['121212', '#121212'],
            ['cdcdcd', '#cdcdcd'],
            ['EFEFEF', '#efefef'],
            ['03bf58', '#03bf58'],
            ['#000', '#000000'],
            ['#fff', '#ffffff'],
            ['#AAA', '#aaaaaa'],
            ['#0B5', '#00bb55'],
            ['111', '#111111'],
            ['ccc', '#cccccc'],
            ['EEE', '#eeeeee'],
            ['0b5', '#00bb55'],
        ];
    }

    public function provideFaultyHexColors()
    {
        return [
            ['XYZ'],
            ['XYZXYZ'],
            ['###'],
            ['######'],
            ['@@@'],
            ['@@@@@@'],
            ['GGG'],
            ['hhhhhh'],
            ['1#2#3'],
            ['#000#'],
            ['red'],
            ['ORANGE'],
            ['#FAKE'],
        ];
    }
}
