<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageConverterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImagickFactory;
use Redhotmagma\ConfiguratorApiBundle\Vector\InkscapeFactory;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ImageConverterTest extends ApiTestCase
{
    /**
     * @var string
     */
    protected const TRANSPARENT_EPS_SOURCE = '/_data/imageConverter/eps_transparent_bg.EPS';

    /**
     * @var string
     */
    protected const TRANSPARENT_EPS_RESULT = '/_data/imageConverter/eps_transparent_bg_RESULT.png';

    /**
     * @var string
     */
    protected const SOLID_EPS_SOURCE = '/_data/imageConverter/eps_solid_bg.eps';

    /**
     * @var string
     */
    protected const SOLID_EPS_RESULT = '/_data/imageConverter/eps_solid_bg_RESULT.png';

    /**
     * @var string
     */
    protected const MULTI_LAYER_IMAGE = '/_data/imageConverter/multi_layer_image.tif';

    /**
     * @var ImageConverterService
     */
    private $imageConverterService;

    /**
     * @var ImagickFactory
     */
    private $imagickFactory;

    /**
     * @var Filesystem
     */
    private $filesystem;

    protected function setUp(): void
    {
        parent::setUp();
        $this->filesystem = new Filesystem();
        $inkscapeFactory = new InkscapeFactory();
        $this->imagickFactory = new ImagickFactory($this->filesystem, $inkscapeFactory::create());
        $this->imageConverterService = new ImageConverterService($this->imagickFactory, $this->filesystem);
    }

    public function tearDown(): void
    {
        $this->filesystem->remove(__DIR__ . self::SOLID_EPS_RESULT);
        $this->filesystem->remove(__DIR__ . self::TRANSPARENT_EPS_RESULT);
    }

    /**
     * testing if transparent background is created and colors are correct.
     *
     * @throws \ImagickException
     * @throws \ImagickPixelException
     */
    public function testTransparentBgEpsConversion()
    {
        $this->imageConverterService->scaleDown(
            __DIR__ . self::TRANSPARENT_EPS_SOURCE,
            __DIR__ . self::TRANSPARENT_EPS_RESULT,
            1000,
            312,
            'png'
        );

        $imagick = $this->imagickFactory->createFromSource(__DIR__ . '/' . self::TRANSPARENT_EPS_RESULT);

        // assert transparent bg part
        $color = $imagick->getImagePixelColor(5, 5)->getColor();
        self::assertEquals(0, $color['a']);

        // assert logo red part
        $color = $imagick->getImagePixelColor(100, 180)->getColor();
        self::assertEquals(1, $color['a']);
        self::assertEquals(236, $color['r']);
        self::assertEquals(29, $color['g']);
        self::assertEquals(43, $color['b']);
    }

    /**
     * testing if solid background is created and colors are correct.
     *
     * @throws \ImagickException
     * @throws \ImagickPixelException
     */
    public function testSolidBgEpsConversion()
    {
        $this->imageConverterService->scaleDown(
            __DIR__ . self::SOLID_EPS_SOURCE,
            __DIR__ . self::SOLID_EPS_RESULT,
            1000,
            312,
            'png'
        );

        $imagick = $this->imagickFactory->createFromSource(__DIR__ . self::SOLID_EPS_RESULT);

        // assert solid grey bg part
        $color = $imagick->getImagePixelColor(5, 5)->getColor();

        self::assertEquals(1, $color['a']);
        self::assertEquals(213, $color['r']);
        self::assertEquals(215, $color['g']);
        self::assertEquals(212, $color['b']);

        // assert logo red part
        $color = $imagick->getImagePixelColor(100, 180)->getColor();
        self::assertEquals(1, $color['a']);
        self::assertEquals(236, $color['r']);
        self::assertEquals(29, $color['g']);
        self::assertEquals(43, $color['b']);
    }

    /**
     * Multi layerd images should be converted to one image properly.
     */
    public function testMultiLayeredImageConversion()
    {
        $imagick = $this->imagickFactory->createFromSource(__DIR__ . self::MULTI_LAYER_IMAGE);
        self::assertNotNull($imagick);
    }
}
