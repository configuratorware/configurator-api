<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ZipCreator;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ZipModel;

class ZipCreatorTest extends TestCase
{
    /**
     * @var ZipCreator
     */
    private $zipCreator;

    protected function setUp(): void
    {
        $this->zipCreator = new ZipCreator();
    }

    public function testShouldCreateZipArchive()
    {
        $file = $this->zipCreator->createArchive($this->getExampleFiles());

        $fileExists = file_exists($file);

        self::assertTrue($fileExists, 'File was not generated');
    }

    public function testShouldOnlyZipExistingFiles()
    {
        $filesToZip = $this->getExampleFiles();
        $notExistingFilePath = 'i-dont-exist.html';
        $filesToZip[] = new ZipModel(__DIR__ . $notExistingFilePath, $notExistingFilePath);
        $file = $this->zipCreator->createArchive($filesToZip);
        $zip = new \ZipArchive();
        if ($zip->open($file)) {
            for ($i = 0; $i < $zip->numFiles; ++$i) {
                $filename = $zip->getNameIndex($i);
                $this->assertNotEquals($notExistingFilePath, $filename);
            }
        }
        $fileExists = file_exists($file);

        self::assertTrue($fileExists, 'File was not generated');
    }

    private function getExampleFiles()
    {
        $files = [];

        for ($i = 1; $i <= 3; ++$i) {
            $path = __DIR__ . '/_data/zip/' . $i . '.txt';
            $zip = new ZipModel($path, '');
            $files[] = $zip;
        }

        return $files;
    }
}
