<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Phake_IMock;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font as FontEntity;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Font\FontEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Font\FontSave;
use Redhotmagma\ConfiguratorApiBundle\Service\Font\FontStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\FontPathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font;
use Symfony\Component\Filesystem\Filesystem;

class FontSaveTest extends TestCase
{
    public FontSave $fontSave;

    /**
     * @var FontRepository|Phake_IMock
     * @Mock FontRepository
     */
    public FontRepository $fontRepository;

    /**
     * @var Filesystem|Phake_IMock
     * @Mock Filesystem
     */
    public Filesystem $filesystem;

    /**
     * @var FontEntityFromStructureConverter|Phake_IMock
     * @Mock FontEntityFromStructureConverter
     */
    public FontEntityFromStructureConverter $entityConverter;

    /**
     * @var FontStructureFromEntityConverter|Phake_IMock
     * @Mock FontStructureFromEntityConverter
     */
    public FontStructureFromEntityConverter $structureConverter;

    /**
     * @var FontPathsInterface|Phake_IMock
     * @Mock FontPathsInterface
     */
    public FontPathsInterface $fontPaths;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->fontSave = new FontSave(
            $this->filesystem,
            $this->fontRepository,
            $this->entityConverter,
            $this->structureConverter,
            $this->fontPaths
        );
    }

    public function testShouldUpdateWithoutRenaming()
    {
        $oldFontName = 'oldFont';
        $fontPath = 'somePath';
        $structure = new Font();
        $structure->id = 123;
        $structure->name = $oldFontName;
        $font = new FontEntity();
        $font->setFileNameBold($oldFontName . 'bold.ttf');
        $font->setFileNameBoldItalic($oldFontName . 'boldItalic.ttf');
        $font->setFileNameItalic($oldFontName . 'italic.ttf');
        $font->setFileNameRegular($oldFontName . '.ttf');
        $font->setName($oldFontName);
        $toSave = clone $font;

        \Phake::when($this->fontRepository)->findOneBy(['id' => $structure->id])->thenReturn($font);
        \Phake::when($this->entityConverter)->convertOne($structure, $font)->thenReturn($toSave);
        \Phake::when($this->fontRepository)->save($toSave)->thenReturn($toSave);
        \Phake::when($this->fontPaths)->getFontPath()->thenReturn($fontPath);

        $this->fontSave->save($structure);

        \Phake::verifyNoInteraction($this->filesystem);
    }

    public function testShouldUpdateAndRenameFontFiles()
    {
        $oldFontName = 'oldFont';
        $newFontName = 'newFont';
        $fontPath = 'somePath';
        $structure = new Font();
        $structure->id = 123;
        $structure->name = $newFontName;
        $font = new FontEntity();
        $font->setFileNameBold($oldFontName . 'bold.ttf');
        $font->setFileNameBoldItalic($oldFontName . 'boldItalic.ttf');
        $font->setFileNameItalic($oldFontName . 'italic.ttf');
        $font->setFileNameRegular($oldFontName . '.ttf');
        $font->setName($oldFontName);
        $toSave = clone $font;

        \Phake::when($this->fontRepository)->findOneBy(['id' => $structure->id])->thenReturn($font);
        \Phake::when($this->entityConverter)->convertOne($structure, $font)->thenReturn($toSave);
        \Phake::when($this->fontRepository)->save($toSave)->thenReturn($toSave);
        \Phake::when($this->fontPaths)->getFontPath()->thenReturn($fontPath);

        $this->fontSave->save($structure);
        $oldPath = $fontPath . '/' . $oldFontName;
        $newPath = $fontPath . '/' . $newFontName;
        \Phake::verify($this->filesystem)->rename($oldPath . 'bold.ttf', $newPath . 'bold.ttf');
        \Phake::verify($this->filesystem)->rename($oldPath . 'boldItalic.ttf', $newPath . 'boldItalic.ttf');
        \Phake::verify($this->filesystem)->rename($oldPath . 'italic.ttf', $newPath . 'italic.ttf');
        \Phake::verify($this->filesystem)->rename($oldPath . '.ttf', $newPath . '.ttf');
    }
}
