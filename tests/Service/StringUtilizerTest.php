<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service;

use PHPUnit\Framework\TestCase;

class StringUtilizerTest extends TestCase
{
    /**
     * @var StringUtilizer
     */
    private $stringUtilizer;

    protected function setUp(): void
    {
        $this->stringUtilizer = new StringUtilizer();
    }

    /**
     * @param string $identifier
     * @param string $expected
     *
     * @dataProvider providerShouldNormalizeIdentifier
     */
    public function testShouldNormalizeIdentifier(string $identifier, string $expected)
    {
        $actual = $this->stringUtilizer->normalizeIdentifier($identifier);

        self::assertSame($expected, $actual);
    }

    public function providerShouldNormalizeIdentifier()
    {
        return [
            ['grOot', 'groot'],
            ['grOot_grond-leGend', 'groot_grond-legend'],
            ['grO.ot_gr/ond-le\Ge+§nd', 'groot_grond-legend'],
            ['grO.ot_gr/ond-le\Ge+§nd_0', 'groot_grond-legend_0'],
            ['ABC_0123456789', 'abc_0123456789'],
        ];
    }
}
