<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequest;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class UserImageCopyTest extends ApiTestCase
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var UserImageCopy
     */
    private $userImageCopy;

    protected function setUp(): void
    {
        $this->root = vfsStream::setup();
        $this->filesystem = new Filesystem();
        $this->userImageCopy = new UserImageCopy(
            $this->filesystem, $this->root->url() . '/var/uploads/designer/userimages/', $this->root->url() . '/web'
        );
    }

    public function testShouldCreateImageCopies(): void
    {
        // set up original files
        $this->filesystem->dumpFile($this->root->url() . '/var/uploads/designer/userimages/originalfilehash/original_name.png', 'This is a mock file');
        $this->filesystem->dumpFile($this->root->url() . '/web/img/uploads/designer/preview/originalfilehash.png', 'This is a mock file');
        $this->filesystem->dumpFile($this->root->url() . '/web/img/uploads/designer/thumbnail/originalfilehash.png', 'This is a mock file');

        $designerUserImageRequest = new DesignerUserImageRequest();
        $designerUserImageRequest->fileName = 'originalfilehash';
        $newHash = $this->userImageCopy->createCopy($designerUserImageRequest);

        // check if copies where created
        self::assertFileExists($this->root->url() . '/var/uploads/designer/userimages/' . $newHash . '/original_name.png');
        self::assertFileExists($this->root->url() . '/web/img/uploads/designer/preview/' . $newHash . '.png');
        self::assertFileExists($this->root->url() . '/web/img/uploads/designer/thumbnail/' . $newHash . '.png');
    }
}
