<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Credential;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Service\Credential\CredentialChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserCredentials;
use Symfony\Component\Security\Core\User\InMemoryUser;

final class CredentialCheckerTest extends TestCase
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var UserCredentials|\Phake_IMock
     * @Mock UserCredentials
     */
    private $userCredentials;

    /**
     * @var CredentialChecker
     */
    private $credentialChecker;

    public function setUp(): void
    {
        $this->user = new User();
        \Phake::initAnnotations($this);
        \Phake::when($this->userCredentials)->getByUser($this->user)->thenReturn(['user_credential_1', 'user_credential_2', 'user_credential_3']);
        $this->credentialChecker = new CredentialChecker($this->userCredentials);
    }

    public function testShouldReturnTrueForSingleCredential(): void
    {
        self::assertTrue($this->credentialChecker->check($this->user, 'user_credential_2'));
    }

    public function testShouldReturnFalseForSingleCredential(): void
    {
        self::assertFalse($this->credentialChecker->check($this->user, 'not_returned_by_getByUser'));
    }

    public function testShouldReturnTrueForEmptyCredential(): void
    {
        self::assertTrue($this->credentialChecker->check($this->user, ''));
    }

    public function testShouldReturnFalseForNullUser(): void
    {
        self::assertTrue($this->credentialChecker->check(null, ''));
    }

    public function testShouldReturnTrueForMultipleCredentials(): void
    {
        self::assertTrue($this->credentialChecker->check($this->user, ['user_credential_3', 'not_returned_by_getByUser']));
    }

    public function testShouldReturnFalseForMultipleCredentials(): void
    {
        self::assertFalse($this->credentialChecker->check($this->user, ['not_returned_by_getByUser', 'not_returned_by_getByUser_other']));
    }

    public function testShouldCheckInMemoryUser()
    {
        $user = new InMemoryUser('test', 'test', ['ROLE_USER']);

        $actual = $this->credentialChecker->check($user);

        $this->assertTrue($actual);
    }

    public function testCannotCheckInMemoryUserWithCredentialSuccessfully()
    {
        $user = new InMemoryUser('test', 'test', ['ROLE_USER']);

        $actual = $this->credentialChecker->check($user, 'user_credential_2');

        $this->assertFalse($actual);
    }
}
