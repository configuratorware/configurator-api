<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Media;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Mime\FileBinaryMimeTypeGuesser;

/**
 * Class FixedFileBinaryMimeTypeGuesserTest.
 *
 * @author Daniel Klier <klier@redhotmagma.de>
 */
class FixedFileBinaryMimeTypeGuesserTest extends TestCase
{
    /**
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testGuessForSvg()
    {
        $guesser = new FileBinaryMimeTypeGuesser();
        if (!$guesser->isGuesserSupported()) {
            $this->markTestSkipped('Class is not supported on this OS or cannot execute shell commands');
        }

        $type = $guesser->guessMimeType(__DIR__ . '/_data/duck.svg');

        self::assertSame('image/svg+xml', $type);
    }
}
