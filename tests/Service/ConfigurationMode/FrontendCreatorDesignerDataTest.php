<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode;

use Phake;
use Phake_IMock;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureHelper;
use Redhotmagma\ApiBundle\Service\Helper\StringHelper;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette\FrontendColorPaletteStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDefaultFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\FrontendCreatorViewStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\FrontendDesignAreaStructureFromCreatorViewDesignAreaEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\FrontendDesignProductionMethodStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\SettingApi;
use Redhotmagma\ConfiguratorApiBundle\Service\UploadMaxSize;

class FrontendCreatorDesignerDataTest extends TestCase
{
    /**
     * @var DesignAreaRepository|Phake_IMock
     * @Mock DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var FrontendCreatorDesignerData
     */
    private $frontendCreatorDesignerData;

    /**
     * @var SettingApi|Phake_IMock
     * @Mock SettingApi
     */
    private $settingApi;

    protected function setUp(): void
    {
        parent::setUp();

        Phake::initAnnotations($this);
        Phake::when($this->settingApi)->frontendGetMaxZoom2d->thenReturn(100);

        $structureFromEntityConverter = new StructureFromEntityConverter(new StringHelper(), new StructureHelper());
        $frontendColorPaletteStructureFromEntityConverter = new FrontendColorPaletteStructureFromEntityConverter($structureFromEntityConverter);
        $frontendDesignProductionMethodStructureFromEntityConverter = new FrontendDesignProductionMethodStructureFromEntityConverter($frontendColorPaletteStructureFromEntityConverter, $structureFromEntityConverter);
        $frontendDesignAreaStructureFromEntityConverter = new FrontendDesignAreaStructureFromCreatorViewDesignAreaEntityConverter($frontendDesignProductionMethodStructureFromEntityConverter, $structureFromEntityConverter);
        $frontendCreatorViewStructureFromEntityConverter = new FrontendCreatorViewStructureFromEntityConverter($frontendDesignAreaStructureFromEntityConverter, $structureFromEntityConverter);
        $creatorViewDefaultFactory = new CreatorViewDefaultFactory($this->designAreaRepository);
        $this->frontendCreatorDesignerData = new FrontendCreatorDesignerData($frontendCreatorViewStructureFromEntityConverter, $this->settingApi, new UploadMaxSize(), $creatorViewDefaultFactory);
    }

    public function testShouldProvideDefaultView()
    {
        $designAreaIdentifiers = ['area1', 'area2'];
        $designAreas = [];
        foreach ($designAreaIdentifiers as $areaIdentifier) {
            $designArea = new DesignArea();
            $designArea->setIdentifier($areaIdentifier);
            $designAreas[] = $designArea;
        }
        Phake::when($this->designAreaRepository)->findByItemAndHasDesignProductionMethodRelation->thenReturn($designAreas);

        $actual = $this->frontendCreatorDesignerData->provideForItem(new Item());
        foreach ($designAreaIdentifiers as $areaIndex => $areaIdentifier) {
            $this->assertEquals($areaIdentifier, $actual->designViews[0]->designAreas[$areaIndex]->identifier);
        }
    }
}
