<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Attribute;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Cache\StructureCache;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Service\Attribute\AttributeStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attribute as AttributeStructure;

class AttributeStructureFromEntityConverterTest extends TestCase
{
    /**
     * @var AttributeStructureFromEntityConverter
     */
    private $converter;

    /**
     * @var  StructureFromEntityConverter|\Phake_IMock
     * @Mock StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var  StructureCache|\Phake_IMock
     * @Mock StructureCache
     */
    private $structureCache;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->converter = new AttributeStructureFromEntityConverter($this->structureFromEntityConverter, $this->structureCache);
    }

    public function testShouldConvertStructureWithoutCache()
    {
        $attribute = new Attribute();
        $expected = new AttributeStructure();

        \Phake::when($this->structureCache)->findStructureForEntity($attribute)->thenReturn(null)->thenReturn($expected);
        \Phake::when($this->structureFromEntityConverter)->convertOne($attribute, AttributeStructure::class)->thenReturn($expected);

        $actual = $this->converter->convertOne($attribute);

        self::assertSame($expected, $actual);

        \Phake::verify($this->structureFromEntityConverter)->convertOne($attribute, AttributeStructure::class);
    }

    public function testShouldConvertStructureWithCache()
    {
        $attribute = new Attribute();
        $expected = new AttributeStructure();

        \Phake::when($this->structureCache)->findStructureForEntity($attribute)->thenReturn($expected);

        $actual = $this->converter->convertOne($attribute);

        self::assertSame($expected, $actual);

        \Phake::verifyNoInteraction($this->structureFromEntityConverter);
    }
}
