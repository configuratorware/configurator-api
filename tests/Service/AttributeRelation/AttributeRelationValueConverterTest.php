<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationValueConverter;

class AttributeRelationValueConverterTest extends TestCase
{
    /**
     * @dataProvider provideValues
     */
    public function testConvertAttributeValue(string $value, string $dataType, $expected): void
    {
        $actual = AttributeRelationValueConverter::convertAttributeValue($value, $dataType);

        self::assertSame($expected, $actual);
    }

    public function provideValues(): array
    {
        return [
            ['true', 'boolean', true],
            ['false', 'boolean', false],
            ['1', 'boolean', true],
            ['0', 'boolean', false],
            ['1', 'number', 1.0],
            ['1.2', 'number', 1.2],
            ['1', 'integer', 1],
            ['1', 'string', '1'],
            ['1.0', 'string', '1.0'],
            ['string', 'string', 'string'],
        ];
    }
}
