INSERT IGNORE INTO channel (id, identifier, settings, is_default, date_created, date_updated, date_deleted,
                            user_created_id, user_updated_id, user_deleted_id, currency_id)
VALUES (1, '_default', '{}', 1, NOW(), NULL, '0001-01-01 00:00:00', 0, 0, 0, 1);

INSERT IGNORE INTO item(id, identifier, date_created, date_updated, date_deleted, parent_id)
VALUES (2000, 'pen', NOW(), NULL, '0001-01-01 00:00:00', NULL),
    (2001, 'pen_red', NOW(), NULL, '0001-01-01 00:00:00', 2000),
    (2002, 'pen_blue', NOW(), NULL, '0001-01-01 00:00:00', 2000);

INSERT IGNORE INTO itemprice (id, price, price_net, amountfrom, date_created, date_deleted, channel_id, item_id)
VALUES (5000, 26.18, 22.00, 1, NOW(), '0001-01-01 00:00:00', 1, 2000),
    (5001, 24.99, 21.00, 50, NOW(), '0001-01-01 00:00:00', 1, 2000),
    (5002, 39.27, 33.00, 1, NOW(), '0001-01-01 00:00:00', 1, 2001),
    (5003, 38.08, 32.00, 50, NOW(), '0001-01-01 00:00:00', 1, 2001),
    (5004, 52.36, 44.00, 1, NOW(), '0001-01-01 00:00:00', 1, 2002),
    (5005, 47.60, 40.00, 50, NOW(), '0001-01-01 00:00:00', 1, 2002);

INSERT IGNORE INTO item(id, identifier, date_created, date_updated, date_deleted, parent_id)
VALUES (2010, 'pencil', NOW(), NULL, '0001-01-01 00:00:00', NULL),
    (2011, 'pencil_red', NOW(), NULL, '0001-01-01 00:00:00', 2010),
    (2012, 'pencil_blue', NOW(), NULL, '0001-01-01 00:00:00', 2010);

INSERT IGNORE INTO itemprice (id, price, price_net, amountfrom, date_created, date_deleted, channel_id, item_id)
VALUES (5012, 39.27, 33.00, 1, NOW(), '0001-01-01 00:00:00', 1, 2011),
    (5013, 38.08, 32.00, 50, NOW(), '0001-01-01 00:00:00', 1, 2011),
    (5014, 52.36, 44.00, 1, NOW(), '0001-01-01 00:00:00', 1, 2012),
    (5015, 47.60, 40.00, 50, NOW(), '0001-01-01 00:00:00', 1, 2012);

INSERT IGNORE INTO item(id, identifier, date_created, date_updated, date_deleted, parent_id)
VALUES (2020, 'marker', NOW(), NULL, '0001-01-01 00:00:00', NULL),
    (2021, 'marker_red', NOW(), NULL, '0001-01-01 00:00:00', 2020),
    (2022, 'marker_blue', NOW(), NULL, '0001-01-01 00:00:00', 2020);

INSERT IGNORE INTO itemprice (id, price, price_net, amountfrom, date_created, date_deleted, channel_id, item_id)
VALUES (5020, 26.18, 22.00, 1, NOW(), '0001-01-01 00:00:00', 1, 2020),
    (5021, 24.99, 21.00, 50, NOW(), '0001-01-01 00:00:00', 1, 2020);

INSERT IGNORE INTO item(id, identifier, date_created, date_updated, date_deleted, parent_id)
VALUES (2030, 'calendar', NOW(), NULL, '0001-01-01 00:00:00', NULL);

INSERT IGNORE INTO itemprice (id, price, price_net, amountfrom, date_created, date_deleted, channel_id, item_id)
VALUES (2030, 11.90, 10.00, 1, NOW(), '0001-01-01 00:00:00', 1, 2030),
    (2031, 5.95, 5.00, 50, NOW(), '0001-01-01 00:00:00', 1, 2030);

INSERT IGNORE INTO design_area(id, identifier, sequence_number, height, width, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, option_id)
VALUES (2030, 'front', 1, 100, 100, NULL, '2019-02-04 15:13:13', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2030, NULL);

INSERT IGNORE INTO design_area_design_production_method (id, width, height, additional_data, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, design_area_id, design_production_method_id)
VALUES (2030, NULL, NULL, NULL, NULL, '2019-02-04 15:50:54', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2030,
        (SELECT id FROM `design_production_method` WHERE `identifier` = 'pad_printing')),
(2040, NULL, NULL, NULL, NULL, '2019-02-04 15:51:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2030,
 (SELECT id FROM `design_production_method` WHERE `identifier` = 'digital_printing'));

INSERT IGNORE INTO designer_production_calculation_type(id, identifier, color_amount_dependent, item_amount_dependent, selectable_by_user, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, design_production_method_id, price_per_item)
VALUES (2030, 'pad_printing_printing_cost', 1, 1, 0, '2019-02-04 15:50:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        (SELECT id FROM `design_production_method` WHERE `identifier` = 'pad_printing'), FALSE),
(2040, 'digital_printing_printing_cost', 1, 1, 0, '2019-02-04 15:50:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `design_production_method` WHERE `identifier` = 'digital_printing'), TRUE);

INSERT IGNORE INTO design_view(id, identifier, sequence_number, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, option_id)
VALUES (2030, 'front', 1, NULL, '2019-02-20 14:27:01', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2030, NULL);

INSERT IGNORE INTO design_view_design_area(id, base_shape, `position`, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, design_view_id, design_area_id)
VALUES (2030, '{"type":"Plane","scale":1,"rotation":{"x":0,"y":0,"z":0},"textureSize":{"width":2048,"height":2048}}', '{"x":200,"y":1024,"width":150,"height":150}', NULL, '2019-02-20 14:27:27', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2030, 2030);

INSERT IGNORE INTO `design_area_design_production_method_price` (`id`, `amount_from`, `color_amount_from`, `price`,
                                                                 `price_net`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`,
                                                                 `channel_id`, `designer_production_calculation_type_id`, `design_area_design_production_method_id`)
VALUES (2040, 1, 1, 1.19, 1.00, '2019-01-29 16:31:17', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2040, 2040);

INSERT IGNORE INTO `design_production_method_price` (`id`, `color_amount_from`, `amount_from`, `price`, `price_net`, `date_created`,
                                                     `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`,
                                                     `user_deleted_id`, `channel_id`,
                                                     `designer_production_calculation_type_id`)
VALUES (2030, 1, 1, 11.90, 10.00, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2030),
(2031, 1, 50, 5.9999, 5.5555, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2030),
(2032, 1, 500, 3.9999, 2.8099, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2030);

INSERT IGNORE INTO `imagegalleryimage` (`id`, `imagename`, `date_created`, `date_deleted`)
VALUES (1000, 'test.svg', '2019-01-25 11:05:13', '0001-01-01 00:00:00');

INSERT IGNORE INTO `image_gallery_image_price` (`id`, `channel_id`, `imagegalleryimage_id`, `price`, `price_net`, `date_created`, `date_deleted`)
VALUES (1000, 1, 1000, 12.0, 14.5, '2019-01-25 11:05:13', '0001-01-01 00:00:00');
