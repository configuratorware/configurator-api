<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;

class VatCalculationServiceTest extends TestCase
{
    /**
     * @var VatCalculationService
     */
    private $vatCalculationService;

    protected function setUp(): void
    {
        $this->vatCalculationService = new VatCalculationService(new FormatterService());
    }

    public function testGetPricesNetGross()
    {
        $channelSettings = [
            'currencySymbol' => '€',
            'vatrate' => 19,
            'pricesDataNetGross' => 'net',
            'pricesDisplayedNetGross' => 'gross',
        ];

        $this->vatCalculationService->setUpChannelSettings($channelSettings);

        $result = $this->vatCalculationService->getPrices(100.0);

        self::assertEquals(119, $result->getPrice(), 'Gross price value unexpected');
        self::assertEquals(100, $result->getPriceNet(), 'Net price value unexpected');
    }

    public function testGetPricesNetNet()
    {
        $channelSettings = [
            'currencySymbol' => '€',
            'vatrate' => 19,
            'pricesDataNetGross' => 'net',
            'pricesDisplayedNetGross' => 'net',
        ];

        $this->vatCalculationService->setUpChannelSettings($channelSettings);

        $result = $this->vatCalculationService->getPrices(100.0);

        self::assertEquals(100, $result->getPrice(), 'Gross price value unexpected');
        self::assertEquals(100, $result->getPriceNet(), 'Net price value unexpected');
    }

    public function testGetPricesGrossGross()
    {
        $channelSettings = [
            'currencySymbol' => '€',
            'vatrate' => 19,
            'pricesDataNetGross' => 'gross',
            'pricesDisplayedNetGross' => 'gross',
        ];

        $this->vatCalculationService->setUpChannelSettings($channelSettings);

        $result = $this->vatCalculationService->getPrices(100.0);

        self::assertEquals(100, $result->getPrice(), 'Gross price value unexpected');
        self::assertEquals(84.0336134454, $result->getPriceNet(), 'Net price value unexpected');
    }

    public function testGetPricesGrossNet()
    {
        $channelSettings = [
            'currencySymbol' => '€',
            'vatrate' => 19,
            'pricesDataNetGross' => 'gross',
            'pricesDisplayedNetGross' => 'net',
        ];

        $this->vatCalculationService->setUpChannelSettings($channelSettings);

        $result = $this->vatCalculationService->getPrices(100.0);

        self::assertEquals(84.0336134454, $result->getPrice(), 'Gross price value unexpected');
        self::assertEquals(84.0336134454, $result->getPriceNet(), 'Net price value unexpected');
    }

    public function testGetPrices()
    {
        $channelSettings = [
            'currencySymbol' => '€',
            'vatrate' => 23,
        ];

        $this->vatCalculationService->setUpChannelSettings($channelSettings);

        $result = $this->vatCalculationService->getPrices(100.0);

        self::assertEquals(100, $result->getPrice(), 'Gross price value unexpected');
        self::assertEquals(0, $result->getPriceNet(), 'Net price value unexpected');
        self::assertEquals(23, $result->getVatRate(), 'Vat Rate value unexpected');
    }
}
