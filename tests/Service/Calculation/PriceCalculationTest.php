<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureHelper;
use Redhotmagma\ApiBundle\Service\Helper\StringHelper;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOptionDeltaprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\EventListener\Calculation\CalculateDesignerListener;
use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ConfigurationProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\DesignerApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType\GlobalCalculationTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType\ItemPriceStrategy;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType\PriceStrategy;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ImageGalleryCalculationType\ImageGalleryCalculationTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\PositionStructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType\Calculator;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType\ItemPriceStrategy as ItemPriceStrategyProduction;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType\PriceStrategy as PriceStrategyProduction;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType\ProductionCalculationTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Models\PriceModel;
use Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage\FrontendImageGalleryImageStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResultPosition;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class PriceCalculationTest extends ApiTestCase
{
    /**
     * @var PriceCalculation
     */
    private $calculationService;

    /**
     * @var CalculateDesignerListener
     */
    private $calculateDesignerListener;

    /**
     * @var BulkSavings
     */
    private $bulkSavings;

    /**
     * @var GlobalChannelDiscount
     */
    private $channelDiscountService;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    public function setUp(): void
    {
        parent::setUp();

        if (!defined('C_CHANNEL')) {
            define('C_CHANNEL', '_default');
        }

        $channelSettings = [
            'currencySymbol' => '€',
            'vatrate' => 19,
            'pricesDataNetGross' => 'gross',
            'pricesDisplayedNetGross' => 'gross',
            'globalDiscountPercentage' => 20.00000,
        ];

        if (!defined('C_CHANNEL_SETTINGS')) {
            define('C_CHANNEL_SETTINGS', $channelSettings);
        }

        if (!defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', 'de_DE');
        }

        if (!defined('C_LANGUAGE_SETTINGS')) {
            define(
                'C_LANGUAGE_SETTINGS',
                json_decode(
                    '{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":".","thousandsseparator":",","currencysymbolposition":"left"}',
                    true
                )
            );
        }

        self::bootKernel();

        $this->itemRepository = $this->em->getRepository(\Redhotmagma\ConfiguratorApiBundle\Entity\Item::class);
        $settingRepository = $this->em->getRepository(Setting::class);
        $itemPriceRepository = $this->em->getRepository(Itemprice::class);
        $optionPriceRepository = $this->em->getRepository(OptionPrice::class);
        $itemOptionclassificationOptionDeltapriceRepository = $this->em->getRepository(ItemOptionclassificationOptionDeltaprice::class);
        $designAreaDesignProductionMethodRepository = $this->em->getRepository(DesignAreaDesignProductionMethod::class);
        $designerGlobalCalculationTypeRepository = $this->em->getRepository(DesignerGlobalCalculationType::class);
        $designerGlobalItemPriceRepository = $this->em->getRepository(DesignerGlobalItemPrice::class);
        $designerProductionCalculationTypeRepository = $this->em->getRepository(DesignerProductionCalculationType::class);
        $designAreaDesignProductionMethodPriceRepository = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class);
        $imageGalleryImageRepository = $this->em->getRepository(ImageGalleryImage::class);

        $formatterService = new FormatterService();
        $minimumOrderAmountCheck = new MinimumOrderAmountCheck($this->itemRepository, $designAreaDesignProductionMethodRepository);
        $minimumOrderAmount = new MinimumOrderAmount($minimumOrderAmountCheck);
        $vatCalculationService = new VatCalculationService(new FormatterService());

        $this->calculationService = new PriceCalculation(
            $settingRepository,
            $itemPriceRepository,
            $optionPriceRepository,
            $itemOptionclassificationOptionDeltapriceRepository,
            $formatterService,
            $minimumOrderAmount,
            $vatCalculationService,
            $this->itemRepository
        );

        $structureFromDataConverter = new StructureFromDataConverter();
        $structureFromEntityConverter = new StructureFromEntityConverter(
            new StringHelper(),
            new StructureHelper()
        );
        $frontendImageGalleryImageStructureFromEntityConverter = new FrontendImageGalleryImageStructureFromEntityConverter(
            $structureFromEntityConverter,
            [],
            new VatCalculationService($formatterService)
        );
        $itemPriceStrategy = new ItemPriceStrategy($designerGlobalItemPriceRepository);
        $itemPriceStrategyProduction = new ItemPriceStrategyProduction($designAreaDesignProductionMethodPriceRepository);
        $priceStrategy = new PriceStrategy();
        $priceStrategyProduction = new PriceStrategyProduction();
        $calculator = new Calculator();
        $configurationProvider = new ConfigurationProvider(
            $this->itemRepository,
            $imageGalleryImageRepository,
            $frontendImageGalleryImageStructureFromEntityConverter
        );
        $globalCalculationTypeApi = new GlobalCalculationTypeApi(
            $designerGlobalCalculationTypeRepository,
            $vatCalculationService,
            $itemPriceStrategy,
            $priceStrategy
        );
        $productionCalculationTypeApi = new ProductionCalculationTypeApi(
            $calculator,
            $designerProductionCalculationTypeRepository,
            $itemPriceStrategyProduction,
            $priceStrategyProduction,
            $vatCalculationService
        );
        $positionStructureFromDataConverter = new PositionStructureFromDataConverter($formatterService, $structureFromDataConverter);

        $this->itemRepository = $this->em
            ->getRepository(\Redhotmagma\ConfiguratorApiBundle\Entity\Item::class);
        $designerApi = new DesignerApi(
            $configurationProvider,
            $globalCalculationTypeApi,
            $formatterService,
            $this->itemRepository,
            $productionCalculationTypeApi,
            $positionStructureFromDataConverter,
            new ImageGalleryCalculationTypeApi()
        );

        $this->calculateDesignerListener = new CalculateDesignerListener($designerApi);

        $this->calculationService->getVatCalculationService()->setUpChannelSettings($channelSettings);

        $this->channelDiscountService = new GlobalChannelDiscount($formatterService);
        $this->channelDiscountService->setUpChannelSettings($channelSettings);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/setUp.sql'));
        $this->executeSql(file_get_contents(__DIR__ . '/../../Controller/_data/Item/softshell_creator_2d.sql'));
    }

    public function testShouldCalculateTheBasePrice()
    {
        $configuration = new Configuration();
        $configuration->optionclassifications = [];
        $configuration->item = new Item();
        $configuration->item->identifier = 'sheep';

        // create calculation result
        $expectedResult = new CalculationResult();
        $expectedResult->total = 199.0;
        $expectedResult->totalFormatted = '€ 199.00';
        $expectedResult->netTotal = 167.2268907563;
        $expectedResult->netTotalFormatted = '€ 167.23';
        $expectedResult->vatRate = 19.0;
        $expectedResult->netTotalFromGrossTotal = 167.2268907563;
        $expectedResult->netTotalFromGrossTotalFormatted = '€ 167.23';

        $actualResult = $this->calculationService->calculate($configuration);

        self::assertEquals($expectedResult, $actualResult);
    }

    public function testShouldCalculateTheBasePriceWithItemPrice()
    {
        $configuration = new Configuration();

        $item = new Item();
        $item->identifier = 'sheep';
        $configuration->item = $item;

        $optionclassifications = new OptionClassification();
        $optionclassifications->identifier = 'face';

        $option = new Option();
        $option->identifier = 'face_nature';
        $optionclassifications->selectedoptions = [$option];
        $configuration->optionclassifications = [$optionclassifications];

        // create calculation result
        $expectedResult = new CalculationResult();
        $expectedResult->total = 214.45;
        $expectedResult->totalFormatted = '€ 214.45';
        $expectedResult->netTotal = 180.21008403361347;
        $expectedResult->netTotalFormatted = '€ 180.21';
        $expectedResult->vatRate = 19.0;
        $expectedResult->netTotalFromGrossTotal = 180.21008403361344;
        $expectedResult->netTotalFromGrossTotalFormatted = '€ 180.21';

        $actualResult = $this->calculationService->calculate($configuration);

        self::assertEqualsWithDelta($expectedResult, $actualResult, 0.00000000002);
    }

    public function providerShouldCalculateTheBasePriceWithDelta()
    {
        return [
            [3.21, 15.33, '€ 15.33', 12.88, '€ 12.88', 19.0, 12.88, '€ 12.88'],
            ['-3.21', 8.91, '€ 8.91', 7.48, '€ 7.48', 19.0, 7.49, '€ 7.49'],
        ];
    }

    /**
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     */
    public function testShouldCalculateTheBasePriceWithDeltaPrice()
    {
        // set caclulation to deltaprice
        $this->executeSql("UPDATE setting SET calculationmethod = 'deltaprices'");

        $configuration = new Configuration();

        $item = new Item();
        $item->identifier = 'sheep';
        $configuration->item = $item;

        $optionclassifications = new OptionClassification();
        $optionclassifications->identifier = 'face';

        $option = new Option();
        $option->identifier = 'face_nature';
        $optionclassifications->selectedoptions = [$option];
        $configuration->optionclassifications = [$optionclassifications];

        // create calculation result
        $expectedResult = new CalculationResult();
        $expectedResult->total = 211.2;
        $expectedResult->totalFormatted = '€ 211.20';
        $expectedResult->netTotal = 177.4789915966;
        $expectedResult->netTotalFormatted = '€ 177.48';
        $expectedResult->vatRate = 19.0;
        $expectedResult->netTotalFromGrossTotal = 177.4789915966;
        $expectedResult->netTotalFromGrossTotalFormatted = '€ 177.48';

        $actualResult = $this->calculationService->calculate($configuration);

        self::assertEquals($expectedResult, $actualResult);

        // reset calculation method
        $this->executeSql("UPDATE setting SET calculationmethod = 'sumofall'");
    }

    public function testShouldCalculateTheBasePriceWithThreeOptions()
    {
        $configuration = new Configuration();

        $item = new Item();
        $item->identifier = 'sheep';
        $configuration->item = $item;

        $optionclassifications = new OptionClassification();
        $optionclassifications->identifier = 'face';

        $option = new Option();
        $option->identifier = 'face_nature';
        $option->amount = 3;
        $optionclassifications->selectedoptions = [$option];
        $configuration->optionclassifications = [$optionclassifications];

        // create calculation result
        $expectedResult = new CalculationResult();
        $expectedResult->total = 245.35;
        $expectedResult->totalFormatted = '€ 245.35';
        $expectedResult->netTotal = 206.1764705882;
        $expectedResult->netTotalFormatted = '€ 206.18';
        $expectedResult->vatRate = 19.0;
        $expectedResult->netTotalFromGrossTotal = 206.1764705882;
        $expectedResult->netTotalFromGrossTotalFormatted = '€ 206.18';

        $actualResult = $this->calculationService->calculate($configuration);

        self::assertEquals($expectedResult, $actualResult);
    }

    public function testShouldCalculateTheBasePriceWithThreeItems()
    {
        $configuration = $this->generateConfigurationForItemWithAmount(3);

        // create calculation result
        $expectedResult = new CalculationResult();
        $expectedResult->total = 107.10;
        $expectedResult->totalFormatted = '€ 107.10';
        $expectedResult->netTotal = 90.00;
        $expectedResult->netTotalFormatted = '€ 90.00';
        $expectedResult->vatRate = 19.0;
        $expectedResult->netTotalFromGrossTotal = 90.00;
        $expectedResult->netTotalFromGrossTotalFormatted = '€ 90.00';

        $actualResult = $this->calculationService->calculate($configuration);

        $this->assertEqualsWithDelta($expectedResult, $actualResult, 0.00000000002);
    }

    /**
     * @param array $itemAccumulates
     * @param array $selectedAmounts
     * @param array $expectedResult
     *
     * @dataProvider providerShouldCalculateDistributedAmountsItemPrice
     */
    public function testShouldCalculateDistributedAmountsItemPrice(
        array $itemAccumulates,
        array $selectedAmounts,
        PriceModel $expectedResult
    ) {
        foreach ($itemAccumulates as $itemIdentifier => $accumulates) {
            $currentItem = $this->itemRepository->findOneByIdentifier($itemIdentifier);
            $currentItem->setAccumulateAmounts($accumulates);
            $this->em->persist($currentItem);
            $this->em->flush();
        }

        $configuration = new Configuration();

        $item = new Item();
        $item->identifier = array_keys($itemAccumulates)[0]; // parent item
        $configuration->item = $item;
        $customData = new \stdClass();
        $customData->selectedAmounts = (object)$selectedAmounts;
        $configuration->customdata = $customData;

        $actualResult = $this->calculationService->getItemPrice($configuration);

        self::assertEquals($expectedResult, $actualResult);
    }

    /**
     * @param string $designData
     * @param float $expectedTotal
     *
     * @dataProvider providerShouldCalculateDesignPrice
     */
    public function testShouldCalculateDesignPrice($designData, $expectedTotal, $selectedAmounts)
    {
        $configuration = new Configuration();
        $configuration->optionclassifications = [];
        $configuration->item = new Item();
        $configuration->item->identifier = 'calendar';
        $customData = new \stdClass();
        $customData->selectedAmounts = (object)['calendar' => $selectedAmounts];
        $configuration->customdata = $customData;
        $configuration->designdata = json_decode($designData);

        $calculationResult = new CalculationResult();
        $calculationResult->total = 0;
        $calculationResult->netTotal = 0;

        $calculateEvent = new CalculateEvent($configuration);
        $calculateEvent->setCalculationResult($calculationResult);
        $this->calculateDesignerListener->onCalculate($calculateEvent);
        $actualResult = $calculateEvent->getCalculationResult();

        self::assertEquals($expectedTotal, $actualResult->total);
    }

    /**
     * @param string $designData
     * @param float $actualTotal
     * @param float $mockTotal
     * @param float $expectedBulkSavings
     *
     * @dataProvider providerShouldCalculateBulkSavings
     */
    public function testShouldCalculateBulkSavings($designData, $actualTotal, $mockTotal, $expectedBulkSavings)
    {
        $mockCalculationResult = new CalculationResult();
        $mockCalculationResult->total = $mockTotal;
        $mockCalculationResult->netTotal = 0;

        $calculationApi = \Phake::mock(CalculationApi::class);
        \Phake::when($calculationApi)->calculate->thenReturn($mockCalculationResult);

        $this->bulkSavings = new BulkSavings(
            $calculationApi,
            $this->itemRepository,
            $this->em->getRepository(Itemprice::class),
            $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)
        );

        $configuration = new Configuration();
        $configuration->optionclassifications = [];

        $configuration->item = new Item();
        $configuration->item->identifier = 'calendar';

        $customData = new \stdClass();
        $customData->selectedAmounts = (object)['calendar' => 10];
        $configuration->customdata = $customData;
        $configuration->designdata = json_decode($designData);

        $calculationResult = new CalculationResult();
        $calculationResult->total = $actualTotal;
        $calculationResult->netTotal = 0;

        $actualResult = $this->bulkSavings->addBulkBulkSavingsToCalculationResult($calculationResult, $configuration);

        self::assertEquals($expectedBulkSavings, $actualResult->bulkSavings[0]->savingAmount);
    }

    public function testShouldCalculateChannelDiscountTotal()
    {
        $configuration = new Configuration();

        $item = new Item();
        $item->identifier = 'sheep';
        $configuration->item = $item;

        $optionclassifications = new OptionClassification();
        $optionclassifications->identifier = 'face';

        $option = new Option();
        $option->identifier = 'face_nature';
        $option->amount = 3;
        $optionclassifications->selectedoptions = [$option];
        $configuration->optionclassifications = [$optionclassifications];

        $expectedResult = new CalculationResult();
        $expectedResult->total = 196.28;
        $expectedResult->totalFormatted = '€ 196.28';
        $expectedResult->netTotal = 164.9411764706;
        $expectedResult->netTotalFormatted = '€ 164.94';
        $expectedResult->vatRate = 19.0;
        $expectedResult->netTotalFromGrossTotal = 164.9411764706;
        $expectedResult->netTotalFromGrossTotalFormatted = '€ 164.94';

        $calculationResult = $this->calculationService->calculate($configuration);
        $actualResult = $this->channelDiscountService->updateTotals($calculationResult);

        self::assertEquals($expectedResult, $actualResult);
    }

    /**
     * @param string $designData
     * @param array $expectedPositionPrices
     *
     * @dataProvider providerShouldCalculateChannelDiscountPositions
     */
    public function testShouldCalculateChannelDiscountPositions($designData, $expectedPositionPrices)
    {
        $configuration = new Configuration();
        $configuration->optionclassifications = [];
        $configuration->item = new Item();
        $configuration->item->identifier = 'calendar';
        $configuration->designdata = json_decode($designData);

        $calculationResult = new CalculationResult();
        $calculationResult->total = 0;
        $calculationResult->netTotal = 0;

        $calculateEvent = new CalculateEvent($configuration);
        $calculateEvent->setCalculationResult($calculationResult);
        $this->calculateDesignerListener->onCalculate($calculateEvent);

        /** @var CalculationResultPosition $resultPosition */
        $resultPosition = $this->channelDiscountService->updatePositions($calculateEvent->getCalculationResult())->positions[1];
        $actualPositionPrices = [
            $resultPosition->price,
            $resultPosition->priceFormatted,
            $resultPosition->priceNet,
            $resultPosition->priceNetFormatted,
        ];
        self::assertEquals($expectedPositionPrices, $actualPositionPrices);
    }

    public function testShouldCalculateOptionPrice()
    {
        $configuration = $this->generateConfigurationForOptionPrice();

        // create calculation result
        $expectedResult = new CalculationResult();
        $expectedResult->total = 35.70;
        $expectedResult->totalFormatted = '€ 35.70';
        $expectedResult->netTotal = 30.00;
        $expectedResult->netTotalFormatted = '€ 30.00';
        $expectedResult->vatRate = 19.0;
        $expectedResult->netTotalFromGrossTotal = 30.0;
        $expectedResult->netTotalFromGrossTotalFormatted = '€ 30.00';

        $actualResult = $this->calculationService->calculate($configuration);

        self::assertEquals($expectedResult, $actualResult);
    }

    /**
     * @return Configuration
     */
    private function generateConfigurationForOptionPrice(): Configuration
    {
        $configuration = $this->generateConfigurationForItem();
        $configuration->optionclassifications = [
            $this->generateOptionClassifcation('softshell_body', 'softshell_red'),
            $this->generateOptionClassifcation('softshell_zipper', 'softshell_avocado'),
            $this->generateOptionClassifcation('softshell_application', 'softshell_redblack'),
        ];

        return $configuration;
    }

    /**
     * @param int $itemAmount
     *
     * @return Configuration
     */
    private function generateConfigurationForItemWithAmount(int $itemAmount): Configuration
    {
        $configuration = $this->generateConfigurationForItem();
        $configuration->optionclassifications = [
            $this->generateOptionClassifcation('softshell_body', 'softshell_red'),
            $this->generateOptionClassifcation('softshell_zipper', 'softshell_avocado'),
            $this->generateOptionClassifcation('softshell_application', 'softshell_redblack'),
        ];
        $configuration->customdata = new \stdClass();
        $configuration->customdata->selectedAmounts = ['softshell_creator_2d' => $itemAmount];

        return $configuration;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return Configuration
     */
    private function generateConfigurationForItem(string $itemIdentifier = 'softshell_creator_2d'): Configuration
    {
        $configuration = new Configuration();
        $configuration->item = new Item();
        $configuration->item->identifier = $itemIdentifier;

        return $configuration;
    }

    /**
     * @param string $optionClassificationIdentifier
     * @param string $optionIdentifier
     * @param int $optionAmount
     *
     * @return OptionClassification
     */
    private function generateOptionClassifcation(string $optionClassificationIdentifier, string $optionIdentifier, int $optionAmount = 1): OptionClassification
    {
        $optionClassification1 = new OptionClassification();
        $optionClassification1->identifier = $optionClassificationIdentifier;
        $option1 = new Option();
        $option1->identifier = $optionIdentifier;
        $option1->amount = $optionAmount;
        $optionClassification1->selectedoptions = [$option1];

        return $optionClassification1;
    }

    /**
     * @return array
     */
    public function providerShouldCalculateBulkSavings(): array
    {
        return [
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"something"}]},
                        "designProductionMethodIdentifier": "pad_printing"
                    }
                }',
                238,
                892.5,
                25,
            ],
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"something"}]},
                        "designProductionMethodIdentifier": "digital_printing"
                    }
                }',
                130.9,
                357,
                45,
            ],
        ];
    }

    /**
     * @return array
     */
    public function providerShouldCalculateDesignPrice(): array
    {
        return [
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"something"}]},
                        "designProductionMethodIdentifier": "pad_printing"
                    }
                }',
                119,
                10,
            ],
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"something"}]},
                        "designProductionMethodIdentifier": "digital_printing"
                    }
                }',
                11.9,
                10,
            ],
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"Text","content":"<div><span>test</span></div>"}]},
                        "designProductionMethodIdentifier": "pad_printing"
                    }
                }',
                1999.95,
                500,
            ],
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"Image","src":"/images/imagegallery/test.svg"}]}
                    }
                }',
                12,
                2,
            ],
        ];
    }

    /**
     * @return array
     */
    public function providerShouldCalculateDistributedAmountsItemPrice(): array
    {
        return [
            [
                [
                    'pen' => false,
                    'pen_red' => false,
                    'pen_blue' => false,
                ],
                [
                    'pen_red' => 100,
                    'pen_blue' => 100,
                ],
                PriceModel::fromPrices(8568, 7200, 19),
            ],
            [
                [
                    'pen' => false,
                    'pen_red' => false,
                    'pen_blue' => false,
                ],
                [
                    'pen_red' => 20,
                    'pen_blue' => 30,
                ],
                PriceModel::fromPrices(2356.2, 1980, 19),
            ],
            [
                [
                    'pen' => true,
                    'pen_red' => false,
                    'pen_blue' => false,
                ],
                [
                    'pen_red' => 20,
                    'pen_blue' => 30,
                ],
                PriceModel::fromPrices(1249.5, 1050, 19),
            ],

            // parent has no prices, should accumulate
            // fallback to child prices while using total amount for the bulk step
            [
                [
                    'pencil' => true,
                    'pencil_red' => false,
                    'pencil_blue' => false,
                ],
                [
                    'pencil_red' => 20,
                    'pencil_blue' => 30,
                ],
                PriceModel::fromPrices(2189.6, 1840, 19),
            ],
        ];
    }

    /**
     * @return array
     */
    public function providerShouldCalculateChannelDiscountPositions(): array
    {
        return [
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"something"}]},
                        "designProductionMethodIdentifier": "digital_printing"
                    }
                }',
                [
                    0.952,
                    '€ 0.95',
                    0.80,
                    '€ 0.80',
                ],
            ],
            [
                '{
                    "front": {
                        "canvasData": {"objects":[{"type":"something"}]},
                        "designProductionMethodIdentifier": "pad_printing"
                    }
                }',
                [
                    9.520000000000001,
                    '€ 9.52',
                    8.0,
                    '€ 8.00',
                ],
            ],
        ];
    }
}
