<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item as ItemEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionDeltapriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItempriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionPriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;

class NullPriceCalculationTest extends TestCase
{
    /**
     * @var PriceCalculation
     */
    private $calculationService;

    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var SettingRepository|\Phake_IMock
     * @Mock SettingRepository
     */
    private $settingRepository;

    /**
     * @var ItempriceRepository|\Phake_IMock
     * @Mock ItempriceRepository
     */
    private $itemPriceRepository;

    /**
     * @var OptionPriceRepository|\Phake_IMock
     * @Mock OptionPriceRepository
     */
    private $optionPriceRepository;

    /**
     * @var ItemOptionclassificationOptionDeltapriceRepository|\Phake_IMock
     * @Mock ItemOptionclassificationOptionDeltapriceRepository
     */
    private $itemOptionclassificationOptionDeltapriceRepository;

    /**
     * @var FormatterService|\Phake_IMock
     * @Mock FormatterService
     */
    private $formatterService;

    /**
     * @var MinimumOrderAmount|\Phake_IMock
     * @Mock MinimumOrderAmount
     */
    private $minimumOrderAmount;

    public function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        if (!defined('C_CHANNEL')) {
            define('C_CHANNEL', '_default');
        }

        $this->calculationService = new PriceCalculation(
            $this->settingRepository,
            $this->itemPriceRepository,
            $this->optionPriceRepository,
            $this->itemOptionclassificationOptionDeltapriceRepository,
            $this->formatterService,
            $this->minimumOrderAmount,
            new VatCalculationService(new FormatterService()),
            $this->itemRepository
        );
    }

    public function testShouldHandleNullPrice(): void
    {
        // $item->getPrice()->price is null and should lead to getItemPrice 0, so calculation does not break
        $configuration = new Configuration();
        $configuration->item = new Item();
        $configuration->item->identifier = 'revelio';
        $item = new ItemEntity();
        $item->addItemprice(new Itemprice());

        \Phake::when($this->itemRepository)->findOneByIdentifier->thenReturn($item);

        $expected = [
            'price' => 0.0,
            'netPrice' => 0.0,
        ];
        $actual = $this->calculationService->getItemPrice($configuration);
        $this->assertEquals($expected['price'], $actual->getPrice());
        $this->assertEquals($expected['netPrice'], $actual->getPriceNet());
    }
}
