<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodPriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\DesignAreaModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType\ItemPriceStrategy;

class ItemPriceStrategyTest extends TestCase
{
    /**
     * @var DesignAreaDesignProductionMethodPriceRepository|\Phake_IMock
     * @Mock DesignAreaDesignProductionMethodPriceRepository
     */
    private $designAreaDesignProductionMethodPriceRepository;

    /**
     * @var ItemPriceStrategy
     */
    private $itemPriceStrategy;

    public function setUp(): void
    {
        if (!defined('C_CHANNEL')) {
            define('C_CHANNEL', '_default');
        }

        \Phake::initAnnotations($this);

        $this->itemPriceStrategy = new ItemPriceStrategy($this->designAreaDesignProductionMethodPriceRepository);
    }

    /**
     * @param array $expected
     * @param int $itemAmount
     * @dataProvider providerShouldProvideCPricesStoredInCalculationType
     */
    public function testShouldProvideCPricesStoredInCalculationType(array $expected, int $itemAmount): void
    {
        $result = $this->itemPriceStrategy->providePrices(
            $this->createCalculationTypePricePerItemFalse(),
            $itemAmount,
            \Phake::mock(DesignAreaModel::class)
        );

        self::assertEquals($expected, $result);
    }

    public function providerShouldProvideCPricesStoredInCalculationType(): array
    {
        return [
            'for item amount amount 2' => [
                [
                    [
                        'color_amount_from' => 1,
                        'price' => 10.00,
                        'priceNet' => 9.00,
                    ],
                    [
                        'color_amount_from' => 2,
                        'price' => 8.00,
                        'priceNet' => 7.00,
                    ],
                ],
                2,
            ],
            'for item amount amount 7' => [
                [
                    [
                        'color_amount_from' => 1,
                        'price' => 5.00,
                        'priceNet' => 4.00,
                    ],
                    [
                        'color_amount_from' => 2,
                        'price' => 3.00,
                        'priceNet' => 2.00,
                    ],
                ],
                7,
            ],
        ];
    }

    private function createCalculationTypePricePerItemFalse(): DesignerProductionCalculationType
    {
        $calculationType = new DesignerProductionCalculationType();
        $calculationType->setPricePerItem(false);

        $channel = new Channel();
        $channel->setIdentifier('_default');

        foreach ($this->calculationTypePrices() as $price) {
            $entity = new DesignProductionMethodPrice();
            $entity->setChannel($channel);
            $entity->setAmountFrom($price['amount_from']);
            $entity->setColorAmountFrom($price['color_amount_from']);
            $entity->setPrice($price['price']);
            $entity->setPriceNet($price['price_net']);
            $calculationType->addDesignProductionMethodPrice($entity);
        }

        return $calculationType;
    }

    private function calculationTypePrices(): array
    {
        return [
            [
                'amount_from' => 1,
                'color_amount_from' => 1,
                'price' => '10.00',
                'price_net' => '9.00',
            ],
            [
                'amount_from' => 1,
                'color_amount_from' => 2,
                'price' => '8.00',
                'price_net' => '7.00',
            ],
            [
                'amount_from' => 5,
                'color_amount_from' => 1,
                'price' => '5.00',
                'price_net' => '4.00',
            ],
            [
                'amount_from' => 5,
                'color_amount_from' => 2,
                'price' => '3.00',
                'price_net' => '2.00',
            ],
        ];
    }
}
