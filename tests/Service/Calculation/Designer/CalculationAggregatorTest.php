<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;

class CalculationAggregatorTest extends TestCase
{
    /**
     * @var CalculationAggregator
     */
    private $calculationAggregator;

    /**
     * @var FormatterService|\Phake_IMock
     * @Mock FormatterService
     */
    private $formatterService;

    protected function setUp(): void
    {
        $channelSettings = [
            'currencySymbol' => '€',
        ];

        if (!defined('C_CHANNEL_SETTINGS')) {
            define('C_CHANNEL_SETTINGS', $channelSettings);
        }

        $this->formatterService = \Phake::mock(FormatterService::class);

        \Phake::when($this->formatterService)->formatPrice->thenReturnCallback(
            function ($price) {
                return '€ ' . (string)number_format($price, 2);
            }
        );

        $this->calculationAggregator = new CalculationAggregator($this->formatterService);
    }

    /**
     * @param array $positions
     * @param array $aggregatedPositions
     *
     * @dataProvider providerShouldAggregatePositions
     */
    public function testShouldAggregatePositions(
        array $positions,
        array $aggregatedPositions
    ): void {
        $calculationResult = new CalculationResult();
        $calculationResult->positions = $positions;

        $calculationResult = $this->calculationAggregator->aggregate($calculationResult);

        self::assertEquals($aggregatedPositions, $calculationResult->positions);
    }

    public function providerShouldAggregatePositions(): array
    {
        return [
            [
                [
                    (object)[
                        'title' => 'T-Shirt',
                        'calculationTypeIdentifier' => 'item',
                        'itemAmountDependent' => true,
                        'declareSeparately' => true,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 99.9,
                        'priceFormatted' => '€ 99.90',
                        'priceNet' => 83.9,
                        'priceNetFormatted' => '€ 83.90',
                    ],
                    (object)[
                        'title' => 'Einrichtungskosten (Druck)',
                        'calculationTypeIdentifier' => 'production_setup_cost',
                        'itemAmountDependent' => false,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 19.0,
                        'priceFormatted' => '€ 19.00',
                        'priceNet' => 15.97,
                        'priceNetFormatted' => '€ 15.97',
                    ],
                    (object)[
                        'title' => 'Druckkosten (Druck)',
                        'calculationTypeIdentifier' => 'production_printing_cost',
                        'itemAmountDependent' => true,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 9.9,
                        'priceFormatted' => '€ 9.90',
                        'priceNet' => 8.3,
                        'priceNetFormatted' => '€ 8.30',
                    ],
                    (object)[
                        'title' => 'Einrichtungskosten (Druck)',
                        'calculationTypeIdentifier' => 'production_setup_cost',
                        'itemAmountDependent' => false,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 76.0,
                        'priceFormatted' => '€ 76.00',
                        'priceNet' => 63.88,
                        'priceNetFormatted' => '€ 63.88',
                    ],
                    (object)[
                        'title' => 'Druckkosten (Druck)',
                        'calculationTypeIdentifier' => 'production_printing_cost',
                        'itemAmountDependent' => true,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 39.6,
                        'priceFormatted' => '€ 39.60',
                        'priceNet' => 33.2,
                        'priceNetFormatted' => '€ 33.20',
                    ],
                ],
                [
                    (object)[
                        'title' => 'T-Shirt',
                        'calculationTypeIdentifier' => 'item',
                        'itemAmountDependent' => true,
                        'declareSeparately' => true,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 99.9,
                        'priceFormatted' => '€ 99.90',
                        'priceNet' => 83.9,
                        'priceNetFormatted' => '€ 83.90',
                    ],
                    (object)[
                        'title' => '',
                        'calculationTypeIdentifier' => 'setup_cost',
                        'itemAmountDependent' => false,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 95.0,
                        'priceFormatted' => '€ 95.00',
                        'priceNet' => 79.85,
                        'priceNetFormatted' => '€ 79.85',
                    ],
                    (object)[
                        'title' => '',
                        'calculationTypeIdentifier' => 'production_cost',
                        'itemAmountDependent' => true,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 49.5,
                        'priceFormatted' => '€ 49.50',
                        'priceNet' => 41.5,
                        'priceNetFormatted' => '€ 41.50',
                    ],
                ],
            ],
            [
                [
                    (object)[
                        'title' => 'T-Shirt',
                        'calculationTypeIdentifier' => 'item',
                        'itemAmountDependent' => true,
                        'declareSeparately' => true,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 99.9,
                        'priceFormatted' => '€ 99.90',
                        'priceNet' => 83.9,
                        'priceNetFormatted' => '€ 83.90',
                    ],
                    (object)[
                        'title' => 'Einrichtungskosten (Siebdruck)',
                        'calculationTypeIdentifier' => 'production_setup_cost',
                        'itemAmountDependent' => false,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 19.0,
                        'priceFormatted' => '€ 19.00',
                        'priceNet' => 15.97,
                        'priceNetFormatted' => '€ 15.97',
                    ],
                    (object)[
                        'title' => 'Druckkosten (Druck)',
                        'calculationTypeIdentifier' => 'production_printing_cost',
                        'itemAmountDependent' => true,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 9.9,
                        'priceFormatted' => '€ 9.90',
                        'priceNet' => 8.3,
                        'priceNetFormatted' => '€ 8.30',
                    ],
                    (object)[
                        'title' => 'Einrichtungskosten (Tampondruck)',
                        'calculationTypeIdentifier' => 'production_setup_cost',
                        'itemAmountDependent' => false,
                        'declareSeparately' => true,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 76.0,
                        'priceFormatted' => '€ 76.00',
                        'priceNet' => 63.88,
                        'priceNetFormatted' => '€ 63.88',
                    ],
                    (object)[
                        'title' => 'Druckkosten (Druck)',
                        'calculationTypeIdentifier' => 'production_printing_cost',
                        'itemAmountDependent' => true,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 39.6,
                        'priceFormatted' => '€ 39.60',
                        'priceNet' => 33.2,
                        'priceNetFormatted' => '€ 33.20',
                    ],
                ],
                [
                    (object)[
                        'title' => 'T-Shirt',
                        'calculationTypeIdentifier' => 'item',
                        'itemAmountDependent' => true,
                        'declareSeparately' => true,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 99.9,
                        'priceFormatted' => '€ 99.90',
                        'priceNet' => 83.9,
                        'priceNetFormatted' => '€ 83.90',
                    ],
                    (object)[
                        'title' => 'Einrichtungskosten (Siebdruck)',
                        'calculationTypeIdentifier' => 'setup_cost',
                        'itemAmountDependent' => false,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 19.0,
                        'priceFormatted' => '€ 19.00',
                        'priceNet' => 15.97,
                        'priceNetFormatted' => '€ 15.97',
                    ],
                    (object)[
                        'title' => '',
                        'calculationTypeIdentifier' => 'production_cost',
                        'itemAmountDependent' => true,
                        'declareSeparately' => false,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 49.5,
                        'priceFormatted' => '€ 49.50',
                        'priceNet' => 41.5,
                        'priceNetFormatted' => '€ 41.50',
                    ],
                    (object)[
                        'title' => 'Einrichtungskosten (Tampondruck)',
                        'calculationTypeIdentifier' => 'setup_cost',
                        'itemAmountDependent' => false,
                        'declareSeparately' => true,
                        'selectableByUser' => false,
                        'selected' => true,
                        'price' => 76.0,
                        'priceFormatted' => '€ 76.00',
                        'priceNet' => 63.88,
                        'priceNetFormatted' => '€ 63.88',
                    ],
                ],
            ],
        ];
    }
}
