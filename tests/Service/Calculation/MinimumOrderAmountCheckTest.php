<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item as ItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\MinimumOrderAmountViolation;

class MinimumOrderAmountCheckTest extends TestCase
{
    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var DesignProductionMethodRepository|\Phake_IMock
     * @Mock DesignProductionMethodRepository
     */
    private $designProductionMethodRepository;

    /**
     * @var DesignAreaDesignProductionMethodRepository|\Phake_IMock
     * @Mock DesignAreaDesignProductionMethodRepository
     */
    private $designAreaDesignProductionMethodRepository;

    /**
     * @var MinimumOrderAmountCheck
     */
    private $minimumOrderAmountCheck;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->minimumOrderAmountCheck = new MinimumOrderAmountCheck(
            $this->itemRepository,
            $this->designAreaDesignProductionMethodRepository
        );
    }

    /**
     * @param string $itemIdentifier
     * @param array $selectedAmounts
     * @param array $expectedViolations
     * @param array $mockItemData
     * @param string $designProductionMethodIdentifier
     * @param string $designAreaIdentifier
     * @param int $designProductionMethodMinAmount
     * @param int $designAreaDesignProductionMethodMinAmount
     *
     * @dataProvider providerShouldThrowMinimumOrderAmountException
     * @dataProvider providerShouldThrowMinimumOrderAmountExceptionWithAccumulation
     * @dataProvider providerShouldThrowDesignProductionMethodViolation
     *
     * Test cases
     * <pre>
     *  | parent accumulates   |1|1|0|0| |1|1|1|1|1|1|1|1|0|0|0|0|0|0|0|0|
     *  | child accumulates    |-|-|-|-| |1|1|1|1|0|0|0|0|1|1|1|1|0|0|0|0|
     *  | parent has min value |1|0|1|0| |1|1|0|0|1|1|0|0|1|1|0|0|1|1|0|0|
     *  | child has min value  |-|-|-|-| |1|0|1|0|1|0|1|0|1|0|1|0|1|0|1|0|
     * </pre>
     */
    public function testShouldReturnItemAmountViolations(
        string $itemIdentifier,
        ?array $selectedAmounts,
        array $expectedViolations,
        array $mockItemData,
        string $designProductionMethodIdentifier = null,
        string $designAreaIdentifier = null,
        ?int $designProductionMethodMinAmount = null,
        ?int $designAreaDesignProductionMethodMinAmount = null
    ) {
        $childItems = new ArrayCollection();
        $parent = null;

        foreach ($mockItemData as $itemData) {
            $item = \Phake::mock(Item::class);
            \Phake::when($item)->getIdentifier()->thenReturn($itemData['identifier']);
            \Phake::when($item)->getTranslatedTitle()->thenReturn($itemData['translatedTitle']);
            \Phake::when($item)->getMinimumOrderAmount()->thenReturn($itemData['minimumOrderAmount']);
            \Phake::when($item)->getAccumulateAmounts()->thenReturn($itemData['accumulateAmounts'] ?? false);
            \Phake::when($item)->getItem()->thenReturn(new ArrayCollection());

            if (!empty($itemData['parent'])) {
                $parent = $this->itemRepository->findOneByIdentifier($itemData['parent']);
                \Phake::when($item)->getParent()->thenReturn($parent);
                $childItems->add($item);
            } else {
                \Phake::when($item)->getParent()->thenReturn(null);
            }

            \Phake::when($this->itemRepository)->findOneByIdentifier($itemData['identifier'])->thenReturn($item);
        }

        if ($parent) {
            \Phake::when($parent)->getItem()->thenReturn($childItems);
        }

        // create configuration structure
        $itemStructure = new ItemStructure();
        $itemStructure->identifier = $itemIdentifier;

        $configuration = new Configuration();
        $configuration->item = $itemStructure;
        $configuration->designdata = new \stdClass();

        if (!empty($designProductionMethodIdentifier)) {
            // fake design production method
            $productionMethod = \Phake::mock(DesignProductionMethod::class);
            \Phake::when($productionMethod)->getIdentifier()->thenReturn($designProductionMethodIdentifier);
            \Phake::when($productionMethod)->getTranslatedTitle()->thenReturn($designProductionMethodIdentifier);
            \Phake::when($productionMethod)->getMinimumOrderAmount()->thenReturn($designProductionMethodMinAmount);
            \Phake::when($this->designProductionMethodRepository)
                ->findOneByIdentifier($designProductionMethodIdentifier)
                ->thenReturn($productionMethod);

            // fake design area
            $designArea = \Phake::mock(DesignArea::class);
            \Phake::when($designArea)->getIdentifier()->thenReturn($designAreaIdentifier);
            \Phake::when($designArea)->getTranslatedTitle()->thenReturn($designAreaIdentifier);
            \Phake::when($designArea)->getItem()->thenReturn($item);

            // fake design area design production method
            $designAreaDesignProductionMethod = \Phake::mock(DesignAreaDesignProductionMethod::class);
            \Phake::when($designAreaDesignProductionMethod)->getMinimumOrderAmount()
                ->thenReturn($designAreaDesignProductionMethodMinAmount);
            \Phake::when($designAreaDesignProductionMethod)->getDesignArea()->thenReturn($designArea);
            \Phake::when($designAreaDesignProductionMethod)->getDesignProductionMethod()->thenReturn($productionMethod);
            \Phake::when($this->designAreaDesignProductionMethodRepository)
                ->findOneByItemAndDesignAreaAreaAndProductionMethod(
                    $itemIdentifier,
                    $designAreaIdentifier,
                    $designProductionMethodIdentifier
                )
                ->thenReturn($designAreaDesignProductionMethod);

            $configuration->designdata = json_decode(
                '{
                "' . $designAreaIdentifier . '": {
                    "canvasData": {},
                    "designProductionMethodIdentifier": "' . $designProductionMethodIdentifier . '"
                }
            }',
                false
            );
        }

        $violations = $this->minimumOrderAmountCheck->checkMinimumOrderAmountsForItems(
            $itemIdentifier,
            array_sum((array)$selectedAmounts),
            $selectedAmounts,
            $configuration
        );

        self::assertEquals(json_encode($expectedViolations), json_encode($violations));
        if (!empty($violations)) {
            self::assertEquals(MinimumOrderAmountViolation::class, get_class($violations[0]));
        }
    }

    /**
     * @return array
     */
    public function providerShouldThrowMinimumOrderAmountException(): array
    {
        return [
            // single item, no parent, simple check logic required
            [
                'pen',
                ['pen' => 6],
                [],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 5,
                        'parent' => null,
                    ],
                ],
            ],
            [
                'pen',
                ['pen' => 2],
                [
                    [
                        'itemIdentifier' => 'pen',
                        'itemTitle' => 'Pen',
                        'selectedAmount' => 2,
                        'minimumAmount' => 5,
                    ],
                ],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 5,
                        'parent' => null,
                    ],
                ],
            ],

            // single item, no parent, simple check logic required - but accumulateAmounts=true, without effect
            [
                'pen',
                ['pen' => 6],
                [],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 5,
                        'accumulateAmounts' => true,
                        'parent' => null,
                    ],
                ],
            ],
            [
                'pen',
                ['pen' => 2],
                [
                    [
                        'itemIdentifier' => 'pen',
                        'itemTitle' => 'Pen',
                        'selectedAmount' => 2,
                        'minimumAmount' => 5,
                    ],
                ],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 5,
                        'accumulateAmounts' => true,
                        'parent' => null,
                    ],
                ],
            ],

            // items with children for the default logic calculation
            [
                'pen',
                ['pen_red' => 1, 'pen_blue' => 1],
                [],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 5,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => 1,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => 1,
                        'parent' => 'pen',
                    ],
                ],
            ],
            [
                'pen',
                ['pen_red' => 3, 'pen_blue' => 11],
                [
                    [
                        'itemIdentifier' => 'pen_red',
                        'itemTitle' => 'Pen Red',
                        'selectedAmount' => 3,
                        'minimumAmount' => 12,
                    ],
                ],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => null,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => 12,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => 8,
                        'parent' => 'pen',
                    ],
                ],
            ],
            [
                'pen_red',
                ['pen_red' => 40],
                [
                    [
                        'itemIdentifier' => 'pen_red',
                        'itemTitle' => 'Pen Red',
                        'selectedAmount' => 40,
                        'minimumAmount' => 50,
                    ],
                ],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 50,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => 50,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => 50,
                        'parent' => 'pen',
                    ],
                ],
            ],
            [
                'pen_red',
                ['pen_red' => 40],
                [],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => null,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => 10,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => 10,
                        'parent' => 'pen',
                    ],
                ],
            ],
            [
                'pen_red',
                ['pen_red' => 40],
                [],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 10,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => null,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => null,
                        'parent' => 'pen',
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function providerShouldThrowMinimumOrderAmountExceptionWithAccumulation(): array
    {
        // items for the parent accumulate logic calculation
        return [
            [
                // parent accumulates, distribute amounts among children - criteria met
                'pencil',
                ['pencil_red' => 40, 'pencil_blue' => 60],
                [],
                [
                    [
                        'identifier' => 'pencil',
                        'translatedTitle' => 'Pencil',
                        'minimumOrderAmount' => 100,
                        'accumulateAmounts' => true,
                        'parent' => 10,
                    ],
                    [
                        'identifier' => 'pencil_red',
                        'translatedTitle' => 'Pencil Red',
                        'minimumOrderAmount' => 12,
                        'parent' => 'pencil',
                    ],
                    [
                        'identifier' => 'pencil_blue',
                        'translatedTitle' => 'Pencil Blue',
                        'minimumOrderAmount' => 8,
                        'parent' => 'pencil',
                    ],
                ],
            ],
            [
                // parent accumulates, distributed amounts among children - criteria not met
                'pencil',
                ['pencil_red' => 33, 'pencil_blue' => 55],
                [
                    [
                        'itemIdentifier' => 'pencil',
                        'itemTitle' => 'Pencil',
                        'selectedAmount' => 88,
                        'minimumAmount' => 100,
                    ],
                ],
                [
                    [
                        'identifier' => 'pencil',
                        'translatedTitle' => 'Pencil',
                        'minimumOrderAmount' => 100,
                        'accumulateAmounts' => true,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pencil_red',
                        'translatedTitle' => 'Pencil Red',
                        'minimumOrderAmount' => 11,
                        'parent' => 'pencil',
                    ],
                    [
                        'identifier' => 'pencil_blue',
                        'translatedTitle' => 'Pencil Blue',
                        'minimumOrderAmount' => 22,
                        'parent' => 'pencil',
                    ],
                ],
            ],
            [
                // red child should get min amounts from parent automatically, because value not set for itself
                'pencil',
                ['pencil_red' => 60, 'pencil_blue' => 80],
                [
                    [
                        'itemIdentifier' => 'pencil_red',
                        'itemTitle' => 'Pencil Red',
                        'selectedAmount' => 60,
                        'minimumAmount' => 100,
                    ],
                    [
                        'itemIdentifier' => 'pencil_blue',
                        'itemTitle' => 'Pencil Blue',
                        'selectedAmount' => 80,
                        'minimumAmount' => 100,
                    ],
                ],
                [
                    [
                        'identifier' => 'pencil',
                        'translatedTitle' => 'Pencil',
                        'minimumOrderAmount' => 100,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pencil_red',
                        'translatedTitle' => 'Pencil Red',
                        'minimumOrderAmount' => null,
                        'parent' => 'pencil',
                    ],
                    [
                        'identifier' => 'pencil_blue',
                        'translatedTitle' => 'Pencil Blue',
                        'minimumOrderAmount' => null,
                        'parent' => 'pencil',
                    ],
                ],
            ],
            [
                // no min amount set, so 1 should be used as minimum, even if 'accumulateAmounts' is not set
                'pencil',
                ['pencil_red' => 54, 'pencil_blue' => 0],
                [
                    [
                        'itemIdentifier' => 'pencil_blue',
                        'itemTitle' => 'Pencil Blue',
                        'selectedAmount' => 0,
                        'minimumAmount' => 1,
                    ],
                ],
                [
                    [
                        'identifier' => 'pencil',
                        'translatedTitle' => 'Pencil',
                        'minimumOrderAmount' => null,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pencil_red',
                        'translatedTitle' => 'Pencil Red',
                        'minimumOrderAmount' => null,
                        'parent' => 'pencil',
                    ],
                    [
                        'identifier' => 'pencil_blue',
                        'translatedTitle' => 'Pencil Blue',
                        'minimumOrderAmount' => null,
                        'parent' => 'pencil',
                    ],
                ],
            ],
        ];
    }

    public function providerShouldThrowDesignProductionMethodViolation()
    {
        return [
            [
                'pen',
                ['pen_red' => 3, 'pen_blue' => 11],
                [
                    [
                        'itemIdentifier' => 'pen_red',
                        'itemTitle' => 'Pen Red',
                        'selectedAmount' => 3,
                        'minimumAmount' => 300,
                    ],
                    [
                        'itemIdentifier' => 'pen_blue',
                        'itemTitle' => 'Pen Blue',
                        'selectedAmount' => 11,
                        'minimumAmount' => 300,
                    ],
                ],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => null,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => 12,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => 8,
                        'parent' => 'pen',
                    ],
                ],
                'prod_method_1',
                'area_identifier_1',
                200,
                300,
            ],
            [
                'pen',
                ['pen_red' => 3],
                [
                    [
                        'itemIdentifier' => 'pen',
                        'itemTitle' => 'Pen',
                        'selectedAmount' => 3,
                        'minimumAmount' => 1000,
                    ],
                ],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 1000,
                        'accumulateAmounts' => true,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => null,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => null,
                        'parent' => 'pen',
                    ],
                ],
                'prod_method_1',
                'area_identifier_1',
                200,
                300,
            ],
            [
                'pen',
                ['pen_red' => 3],
                [
                    [
                        'itemIdentifier' => 'pen',
                        'itemTitle' => 'Pen',
                        'selectedAmount' => 3,
                        'minimumAmount' => 200,
                    ],
                ],
                [
                    [
                        'identifier' => 'pen',
                        'translatedTitle' => 'Pen',
                        'minimumOrderAmount' => 100,
                        'accumulateAmounts' => true,
                        'parent' => null,
                    ],
                    [
                        'identifier' => 'pen_red',
                        'translatedTitle' => 'Pen Red',
                        'minimumOrderAmount' => null,
                        'parent' => 'pen',
                    ],
                    [
                        'identifier' => 'pen_blue',
                        'translatedTitle' => 'Pen Blue',
                        'minimumOrderAmount' => null,
                        'parent' => 'pen',
                    ],
                ],
                'prod_method_1',
                'area_identifier_1',
                200,
                100,
            ],
        ];
    }
}
