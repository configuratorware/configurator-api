<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassification\ItemOptionClassificationSorter;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification\FrontendOptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification\FrontendOptionClassificationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification as OptionClassificationStructure;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class FrontendOptionClassificationTest extends TestCase
{
    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var FrontendOptionClassificationStructureFromEntityConverter|\Phake_IMock
     * @Mock FrontendOptionClassificationStructureFromEntityConverter
     */
    private $optionClassificationStructureFromEntityConverter;

    private FrontendOptionClassification $frontendOptionClassification;

    /**
     * @var FrontendOptionStructureFromEntityConverter|\Phake_IMock
     * @Mock FrontendOptionStructureFromEntityConverter
     */
    private FrontendOptionStructureFromEntityConverter $optionStructureFromEntityConverter;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->frontendOptionClassification = new FrontendOptionClassification(
            $this->itemRepository,
            new ItemOptionClassificationSorter(),
            $this->optionClassificationStructureFromEntityConverter,
            $this->optionStructureFromEntityConverter,
            \Phake::mock(EventDispatcherInterface::class)
        );
    }

    /**
     * @param array $expectedOrder
     * @param bool $overwriteComponentOrder
     * @param array $components
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @dataProvider providerShouldGetByItemId
     */
    public function testShouldGetByItemId(array $expectedOrder, bool $overwriteComponentOrder, array $components): void
    {
        $item = new Item();
        $item->setOverwriteComponentOrder($overwriteComponentOrder);
        foreach ($components as $componentIdentifier => $componentState) {
            $componentEntity = $this->generateComponentEntity($componentState);
            $item->addItemOptionclassification($this->generateItemComponent($componentEntity, $componentState, $item));
            \Phake::when($this->optionClassificationStructureFromEntityConverter)->convertOne($componentEntity)->thenReturn($this->generateComponentStructure($componentState));
        }

        \Phake::when($this->itemRepository)->findItemByIdWithOptionClassifications->thenReturn($item);

        $actualStructures = $this->frontendOptionClassification->getByItemId(1);

        self::assertNotNull(array_column($actualStructures, 'creatorView'));
        self::assertEquals($expectedOrder, array_column($actualStructures, 'identifier'));
    }

    public function providerShouldGetByItemId(): array
    {
        return [
            [
                ['component_identifier_2', 'component_identifier_3', 'component_identifier_1'],
                true,
                [
                    [
                        'identifier' => 'component_identifier_2',
                        'sequence_number' => null,
                    ],
                    [
                        'identifier' => 'component_identifier_3',
                        'sequence_number' => null,
                    ],
                    [
                        'identifier' => 'component_identifier_1',
                        'sequence_number' => null,
                    ],
                ],
            ],
            [
                ['component_identifier_2', 'component_identifier_3', 'component_identifier_1'],
                false,
                [
                    [
                        'identifier' => 'component_identifier_2',
                        'sequence_number' => 3,
                    ],
                    [
                        'identifier' => 'component_identifier_3',
                        'sequence_number' => 2,
                    ],
                    [
                        'identifier' => 'component_identifier_1',
                        'sequence_number' => 1,
                    ],
                ],
            ],
            [
                ['component_identifier_1', 'component_identifier_2', 'component_identifier_3'],
                true,
                [
                    [
                        'identifier' => 'component_identifier_2',
                        'sequence_number' => 2,
                    ],
                    [
                        'identifier' => 'component_identifier_3',
                        'sequence_number' => 3,
                    ],
                    [
                        'identifier' => 'component_identifier_1',
                        'sequence_number' => 1,
                    ],
                ],
            ],
            [
                ['component_identifier_1', 'component_identifier_2', 'component_identifier_3'],
                true,
                [
                    [
                        'identifier' => 'component_identifier_3',
                        'sequence_number' => 20,
                    ],
                    [
                        'identifier' => 'component_identifier_2',
                        'sequence_number' => 10,
                    ],
                    [
                        'identifier' => 'component_identifier_1',
                        'sequence_number' => 3,
                    ],
                ],
            ],
        ];
    }

    private function generateComponentStructure(array $state): OptionClassificationStructure
    {
        $component = new OptionClassificationStructure();
        $component->identifier = $state['identifier'] ?? '';

        return $component;
    }

    private function generateItemComponent(Optionclassification $component, array $state, Item $item): ItemOptionclassification
    {
        $itemComponent = new ItemOptionclassification();
        $itemComponent->setOptionclassification($component);
        $itemComponent->setSequenceNumber($state['sequence_number']);
        $itemComponent->setIsMandatory(true);
        $itemComponent->setIsMultiselect(true);

        $creatorView = CreatorView::from(null, $component->getIdentifier() . '_view', $item, '01');
        $itemComponent->setCreatorView($creatorView);

        return $itemComponent;
    }

    private function generateComponentEntity(array $state): Optionclassification
    {
        $component = new Optionclassification();
        $component->setIdentifier($state['identifier']);

        return $component;
    }
}
