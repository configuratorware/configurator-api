<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback\TextureFilesCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Symfony\Component\Filesystem\Filesystem;

class TextureFilesCheckTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var TextureFilesCheck
     */
    private $textureFilesCheck;

    protected function setUp(): void
    {
        parent::setUp();

        $this->root = vfsStream::setup();
        $this->fileSystem = new Filesystem();
        $this->textureFilesCheck = new TextureFilesCheck(
            $this->fileSystem,
            $this->root->url()
        );
    }

    /**
     * @param string $parentItemIdentifier
     * @param string $fileIdentifier
     * @param string $materialsFileName
     * @param array $textures
     * @param bool $expected
     *
     * @dataProvider providerShouldCheckOnTextureFiles
     */
    public function testShouldCheckOnTextureFiles(
        string $parentItemIdentifier,
        string $fileIdentifier,
        string $materialsFileName,
        array $textures,
        bool $expected
    ) {
        $materialPath = 'content/3D/' . $parentItemIdentifier . '/materials/' . $fileIdentifier;

        $this->fileSystem->dumpFile(
            $this->root->url() . '/' . $materialPath . '/' . $materialsFileName,
            file_get_contents(__DIR__ . '/materials.mtl')
        );

        foreach ($textures as $texture) {
            $this->fileSystem->dumpFile($this->root->url() . '/' . $materialPath . '/' . $texture,
                'this is a test file');
        }

        $item = $this->getItem($parentItemIdentifier, $fileIdentifier);

        $result = $this->textureFilesCheck->isSatisfied($item, []);

        self::assertEquals($expected, $result);
    }

    public function providerShouldCheckOnTextureFiles()
    {
        return [
            ['parent', 'child', 'wrong.mtl', ['texture.jpg', 'opacity.jpg', 'roughness.jpg', 'metalness.jpg'], false],
            ['parent', 'child', 'materials.mtl', ['texture.jpg', 'opacity.jpg', 'roughness.jpg'], false],
            ['parent', 'child', 'materials.mtl', ['texture.jpg', 'opacity.jpg', 'roughness.jpg', 'metalness.jpg'], true],
        ];
    }

    private function getItem(string $parentItemIdentifier, string $fileIdentifier): Item
    {
        $item = new Item();
        $item->identifier = $fileIdentifier;
        $item->parentItemIdentifier = $parentItemIdentifier;
        $item->itemGroup = new \stdClass();
        $item->itemGroup->children = [];

        $child = new \stdClass();
        $child->identifier = $fileIdentifier;
        $child->children = null;

        for ($i = 0; $i < 3; ++$i) {
            $item->itemGroup->children[] = $child;
        }

        return $item;
    }
}
