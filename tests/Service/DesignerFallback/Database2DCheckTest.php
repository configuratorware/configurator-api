<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignViewDesignArea\BaseShape2dPositionCalculation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;

class Database2DCheckTest extends TestCase
{
    /**
     * @var Database2DCheck
     */
    private $database2DCheck;

    /**
     * @var BaseShape2dPositionCalculation|\Phake_IMock
     * @Mock BaseShape2dPositionCalculation
     */
    private $baseShape2dPositionCalculation;

    /**
     * @var DesignViewDesignArea|\Phake_IMock
     * @Mock DesignViewDesignArea
     */
    private $designViewDesignAreaEntity;

    /**
     * @var CreatorViewDesignArea|\Phake_IMock
     * @Mock CreatorViewDesignArea
     */
    private $creatorViewDesignAreaEntity;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->database2DCheck = new Database2DCheck($this->baseShape2dPositionCalculation);

        parent::setUp();
    }

    /**
     * @param string $baseShape
     * @param bool $expected
     * @dataProvider providerShouldCheckOnTextureFiles
     */
    public function testShouldCheckOn2DData(
        string $baseShape,
        bool $expected
    ) {
        \Phake::when($this->baseShape2dPositionCalculation)->calculateBaseShapePosition($this->designViewDesignAreaEntity)->thenReturn($this->designViewDesignAreaEntity);
        \Phake::when($this->designViewDesignAreaEntity)->getBaseShape->thenReturn(json_decode($baseShape));

        $result = $this->database2DCheck->isSatisfied(new Item(), [$this->designViewDesignAreaEntity]);

        self::assertEquals($expected, $result);
    }

    /**
     * @param string $baseShape
     * @param bool $expected
     * @dataProvider providerShouldCheckOnTextureFiles
     */
    public function testShouldCheckCreatorDesigner2DData(
        string $baseShape,
        bool $expected
    ) {
        \Phake::when($this->baseShape2dPositionCalculation)->calculateBaseShapePosition($this->creatorViewDesignAreaEntity)->thenReturn($this->creatorViewDesignAreaEntity);
        \Phake::when($this->creatorViewDesignAreaEntity)->getBaseShape->thenReturn(json_decode($baseShape));

        $result = $this->database2DCheck->isSatisfied(new Item(), [$this->creatorViewDesignAreaEntity]);

        self::assertEquals($expected, $result);
    }

    public function providerShouldCheckOnTextureFiles()
    {
        return [
            ['{"type": "","x":0,"y":0,"width":0,"height":0}', true],
            ['{"type": "Plane","x":100,"y":100,"width":1000,"height":100}', true],
            ['{"type": 123,"x":100,"y":100,"width":1000,"height":100}', false],
            ['{"type": "Plane","x":"test","y":100,"width":1000,"height":100}', false],
            ['{"type": "Plane","x":100,"y":"test","width":1000,"height":100}', false],
            ['{"type": "Plane","x":100,"y":100,"width":"test","height":100}', false],
            ['{"type": "Plane","x":100,"y":100,"width":1000,"height":"test"}', false],
            ['{"x":100,"y":100,"width":1000, "height":100}', false],
        ];
    }
}
