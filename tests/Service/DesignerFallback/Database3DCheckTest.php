<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;

class Database3DCheckTest extends TestCase
{
    /**
     * @var Database3DCheck
     */
    private $database3DCheck;

    /**
     * @var DesignViewDesignArea|\Phake_IMock
     * @Mock DesignViewDesignArea
     */
    private $designViewDesignAreaEntity;

    /**
     * @var CreatorViewDesignArea|\Phake_IMock
     * @Mock CreatorViewDesignArea
     */
    private $creatorViewDesignAreaEntity;

    protected function setUp(): void
    {
        parent::setUp();
        \Phake::initAnnotations($this);

        $this->database3DCheck = new Database3DCheck();
    }

    /**
     * @param string|null $baseShape
     * @param string|null $position
     * @param bool $expected
     * @dataProvider providerShouldCheckOnTextureFiles
     */
    public function testShouldCheckOn3DData(
        ?string $baseShape,
        ?string $position,
        bool $expected
    ) {
        if ($baseShape) {
            \Phake::when($this->designViewDesignAreaEntity)->getBaseShape->thenReturn(json_decode($baseShape));
        }

        if ($position) {
            \Phake::when($this->designViewDesignAreaEntity)->getPosition->thenReturn(json_decode($position));
        }

        $result = $this->database3DCheck->isSatisfied(new Item(), [$this->designViewDesignAreaEntity]);

        self::assertEquals($expected, $result);
    }

    /**
     * @param string|null $baseShape
     * @param string|null $position
     * @param bool $expected
     * @dataProvider providerShouldCheckOnTextureFiles
     */
    public function testShouldCheckOnCreatorDesigner3DData(
        ?string $baseShape,
        ?string $position,
        bool $expected
    ) {
        if ($baseShape) {
            \Phake::when($this->creatorViewDesignAreaEntity)->getBaseShape->thenReturn(json_decode($baseShape));
        }

        if ($position) {
            \Phake::when($this->creatorViewDesignAreaEntity)->getPosition->thenReturn(json_decode($position));
        }

        $result = $this->database3DCheck->isSatisfied(new Item(), [$this->creatorViewDesignAreaEntity]);

        self::assertEquals($expected, $result);
    }

    public function providerShouldCheckOnTextureFiles()
    {
        return [
            ['{"type": ""}', '{"x":0,"y":0,"width":0,"height":0}', true],
            ['{"type": "Plane"}', '{"x":100,"y":100,"width":1000,"height":100}', true],
            ['{"type": 123}', '{"x":100,"y":100,"width":1000,"height":100}', false],
            ['{"type": "Plane"}', '{"x":"test","y":100,"width":1000,"height":100}', false],
            ['{"type": "Plane"}', '{"x":100,"y":"test","width":1000,"height":100}', false],
            ['{"type": "Plane"}', '{"x":100,"y":100,"width":"test","height":100}', false],
            ['{"type": "Plane"}', '{"x":100,"y":100,"width":1000,"height":"test"}', false],
            [null, '{"x":100,"y":100,"width":1000, "height":100}', false],
        ];
    }
}
