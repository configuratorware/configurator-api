<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\ShareUrlResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ShareUrlTest extends ApiTestCase
{
    /**
     * @var ShareUrlResolver
     */
    private $shareUrl;

    /**
     * @var SettingRepository
     */
    private $settingRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->settingRepository = $this->em->getRepository(Setting::class);
        $this->shareUrl = new ShareUrlResolver($this->settingRepository);
    }

    /**
     * @param array $controlParameters
     * @param string $code
     * @param string $expected
     * @dataProvider providerShareUrl
     */
    public function testShouldGetShareUrlFromControlParameters(array $controlParameters, string $code, string $expected): void
    {
        $actual = $this->shareUrl->getShareUrl($code, new ControlParameters($controlParameters));

        self::assertEquals($expected, $actual);
    }

    public function providerShareUrl(): array
    {
        return [
            [
                [ControlParameters::SHARE_URL => 'www.redhotmagma.de?giveMeACode={code}&someParam=1&anotherParam=text'],
                '1337',
                'www.redhotmagma.de?giveMeACode=1337&someParam=1&anotherParam=text',
            ],
            [
                [ControlParameters::SHARE_URL => 'www.redhotmagma.de?giveMeACode=1337'],
                '1337',
                'www.redhotmagma.de?giveMeACode=1337',
            ],
            [
                [],
                '1337',
                'http://localhost:10030/code:1337',
            ],
        ];
    }
}
