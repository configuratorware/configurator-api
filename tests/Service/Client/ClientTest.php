<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;

class ClientTest extends TestCase
{
    /**
     * @var ClientRepository
     * @Mock ClientRepository
     */
    private $clientRepository;

    /**
     * @var Client
     */
    private $client;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->client = new Client($this->clientRepository);
    }

    public function testShouldGetClient()
    {
        $clientEntity = new \Redhotmagma\ConfiguratorApiBundle\Entity\Client();
        $clientEntity->setIdentifier(ClientRepository::DEFAULT_CLIENT_IDENTIFIER);
        $clientEntity->setCallToAction('requestOffer');

        if (!defined('C_CLIENT')) {
            define('C_CLIENT', $clientEntity->getIdentifier());
        }

        \Phake::when($this->clientRepository)->findOneByIdentifier($clientEntity->getIdentifier())->thenReturn($clientEntity);

        $actualClient = $this->client->getClient();
        self::assertEquals($clientEntity->getIdentifier(), $actualClient->identifier);
    }
}
