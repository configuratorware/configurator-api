<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;

class ClientTextGeneratorTest extends TestCase
{
    /**
     * @var ClientTextGenerator
     */
    private $clientTextGenerator;

    /**
     * @var LanguageRepository|\Phake_IMock
     * @Mock LanguageRepository
     */
    private $languageRepository;

    /**
     * @var ChannelRepository|\Phake_IMock
     * @Mock ChannelRepository
     */
    private $channelRepository;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->clientTextGenerator = new ClientTextGenerator($this->languageRepository, $this->channelRepository);
    }

    /**
     * @param array $channels
     * @param array $existingLinks
     * @param $existingLanguageKey
     * @param array $languages
     *
     * @dataProvider providerShouldCompleteClientTextMatrix
     */
    public function testShouldCompleteClientTextMatrix(
        array $channels,
        array $existingLinks,
        $existingLanguageKey,
        array $languages
    ): void {
        // given
        \Phake::when($this->languageRepository)->findAll()->thenReturn($this->getLanguages($languages));
        \Phake::when($this->channelRepository)->findAll()->thenReturn($this->getChannels($channels));
        $client = $this->getClient($channels, $existingLinks, $existingLanguageKey);

        // when
        $this->clientTextGenerator->generate($client);

        // then
        $resultLinks = [];
        foreach ($client->getClientText() as $clientText) {
            $resultLinks[] = $clientText->getTermsAndConditionsLink();
        }

        $expectedNewAmountOfTexts = count($channels) * count($languages);

        self::assertCount($expectedNewAmountOfTexts, $resultLinks);
    }

    public function providerShouldCompleteClientTextMatrix(): array
    {
        return [
            [
                [
                    'channel1',
                    'channel2',
                    'channel3',
                    'channel4',
                    'channel5',
                ],
                [
                    'link1',
                    'link2',
                    'link3',
                ],
                1,
                [
                    1 => 'en_EN',
                    2 => 'de_DE',
                ],
            ],
            // cases exist, where the id returned by an entity is a string
            [
                [
                    '01' => 'channel1',
                    '02' => 'channel2',
                    3 => 'channel3',
                    4 => 'channel4',
                    5 => 'channel5',
                ],
                [
                    'link1',
                    'link2',
                    'link3',
                ],
                '1',
                [
                    1 => 'en_EN',
                    2 => 'de_DE',
                ],
            ],
            [
                [
                    'channel1',
                    'channel2',
                    'channel3',
                    'channel4',
                    'channel5',
                ],
                [],
                1,
                [
                    1 => 'en_EN',
                    2 => 'de_DE',
                ],
            ],
            [
                [
                    'channel1',
                    'channel2',
                    'channel3',
                    'channel4',
                    'channel5',
                ],
                [],
                1,
                [
                    1 => 'en_EN',
                ],
            ],
        ];
    }

    public function testShouldGenerateDefaultClientTexts(): void
    {
        $languages = [
            1 => 'en_EN',
            2 => 'de_DE',
        ];
        \Phake::when($this->languageRepository)->findAll()->thenReturn($this->getLanguages($languages));

        $channelDefault = new Channel();
        $this->setPrivateProperty($channelDefault, 'id', 1);
        $channelDefault->setIdentifier(Channel::DEFAULT_IDENTIFIER);
        \Phake::when($this->channelRepository)->findOneByIdentifier(Channel::DEFAULT_IDENTIFIER)->thenReturn($channelDefault);
        $client = new Client();

        $this->clientTextGenerator->generate($client);
        $expectedAmountOfTexts = count($languages);

        self::assertCount($expectedAmountOfTexts, $client->getClientText());
    }

    private function getClient(array $channels, array $existingLinks, $existingLanguageKey): Client
    {
        $client = new Client();
        $this->setPrivateProperty($client, 'id', 1);

        foreach ($channels as $key => $channel) {
            $channel = new Channel();
            $id = $key + 1;
            if (is_string($key)) {
                $id = (string)$id;
            }

            $this->setPrivateProperty($channel, 'id', $id);

            $clientChannel = new ClientChannel();
            $clientChannel->setClient($client);
            $clientChannel->setChannel($channel);
            $client->addClientChannel($clientChannel);
        }

        $lang = new Language();
        $this->setPrivateProperty($lang, 'id', $existingLanguageKey);

        foreach ($client->getClientChannel() as $key => $clientChannel) {
            if (isset($existingLinks[$key])) {
                $text = new ClientText();
                $text->setTermsAndConditionsLink($existingLinks[$key]);
                $text->setClient($clientChannel->getClient());
                $text->setChannel($clientChannel->getChannel());
                $text->setLanguage($lang);
                $client->addClientText($text);
            }
        }

        return $client;
    }

    private function getLanguages(array $languagesTestData): ArrayCollection
    {
        $languages = new ArrayCollection();

        foreach ($languagesTestData as $key => $item) {
            $lang = new Language();
            $this->setPrivateProperty($lang, 'id', $key);
            $lang->setIso($item);
            $languages->add($lang);
        }

        return $languages;
    }

    private function getChannels(array $channelsTestData): ArrayCollection
    {
        $channels = new ArrayCollection();

        foreach ($channelsTestData as $key => $item) {
            $channel = new Channel();
            $this->setPrivateProperty($channel, 'id', $key);
            $channel->setIdentifier($item);
            $channels->add($channel);
        }

        return $channels;
    }

    private function setPrivateProperty($object, string $property, $value): void
    {
        $reflect = new \ReflectionClass($object);
        $prop = $reflect->getProperty($property);
        $prop->setAccessible(true);
        $prop->setValue($object, $value);
        $prop->setAccessible(false);
    }
}
