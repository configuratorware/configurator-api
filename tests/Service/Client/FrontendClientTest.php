<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientTextRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\FrontendClient;

class FrontendClientTest extends TestCase
{
    /**
     * @var ChannelRepository|\Phake_IMock
     * @Mock ChannelRepository
     */
    private $channelRepository;

    /**
     * @var ClientTextRepository|\Phake_IMock
     * @Mock ClientTextRepository
     */
    private $clientTextRepository;

    /**
     * @var ClientRepository|\Phake_IMock
     * @Mock ClientRepository
     */
    private $clientRepository;

    /**
     * @var FrontendClient
     */
    private $frontendClient;

    /**
     * @var LanguageRepository|\Phake_IMock
     * @Mock LanguageRepository
     */
    private $languageRepository;

    protected function setUp(): void
    {
        if (!defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', 'en_GB');
        }

        \Phake::initAnnotations($this);

        $defaultChannelIdentifier = ChannelRepository::DEFAULT_CHANNEL_IDENTIFIER;
        $defaultClientIdentifier = ClientRepository::DEFAULT_CLIENT_IDENTIFIER;
        \Phake::when($this->channelRepository)->findOneByIdentifier($defaultChannelIdentifier)->thenReturn($this->getChannel($defaultChannelIdentifier));
        \Phake::when($this->clientTextRepository)->findOneBy->thenReturn($this->getClientText());
        \Phake::when($this->clientRepository)->findOneByIdentifier($defaultClientIdentifier)->thenReturn($this->getClient($defaultClientIdentifier));
        \Phake::when($this->languageRepository)->findOneBy->thenReturn($this->getLanguage());

        $this->frontendClient = new FrontendClient(
            $this->clientRepository,
            $this->clientTextRepository,
            $this->channelRepository,
            $this->languageRepository
        );

        parent::setUp();
    }

    /**
     * @param string $clientIdentifier
     * @param string $channelIdentifier
     * @param string $expectedClient
     * @param string $expectedChannel
     * @param array $expectedTexts
     *
     * @dataProvider providerShouldReturnClientChannelCombination
     */
    public function testShouldReturnClientChannelCombination(
        string $clientIdentifier,
        string $channelIdentifier,
        string $expectedClient,
        string $expectedChannel,
        array $expectedTexts
    ) {
        // given
        \Phake::when($this->channelRepository)->findOneByIdentifier($channelIdentifier)->thenReturn($this->getChannel($channelIdentifier));
        \Phake::when($this->clientRepository)->findOneByIdentifier($clientIdentifier)->thenReturn($this->getClient($clientIdentifier));

        // when
        $clientStructure = $this->frontendClient->getClient($clientIdentifier, $channelIdentifier);

        // then
        self::assertEquals($expectedClient, $clientStructure->identifier);
        self::assertEquals($expectedChannel, $clientStructure->channel);
        self::assertEquals($expectedTexts['termsAndConditionsLink'], $clientStructure->texts->termsAndConditionsLink);
    }

    /**
     * @param string $clientIdentifier
     * @param string $channelIdentifier
     *
     * @dataProvider providerShouldThrowException
     */
    public function testShouldThrowException(string $clientIdentifier, string $channelIdentifier)
    {
        $this->expectException(ValidationException::class);

        // given
        \Phake::when($this->clientRepository)->findOneByIdentifier($clientIdentifier)->thenReturn($this->getClient($clientIdentifier));
        if ('i_do_not_exist' === $clientIdentifier) {
            \Phake::when($this->clientRepository)->findOneByIdentifier($clientIdentifier)->thenReturn(null);
        }

        \Phake::when($this->channelRepository)->findOneByIdentifier($channelIdentifier)->thenReturn($this->getChannel($channelIdentifier));
        if ('i_do_not_exist' === $channelIdentifier) {
            \Phake::when($this->channelRepository)->findOneByIdentifier($channelIdentifier)->thenReturn(null);
        }

        // when
        $this->frontendClient->getClient($clientIdentifier, $channelIdentifier);
    }

    public function providerShouldReturnClientChannelCombination()
    {
        return [
            ['redhotmagma', 'rhm_de_eur', 'redhotmagma', 'rhm_de_eur', ['termsAndConditionsLink' => 'http://..']],
            ['_default', 'rhm_de_eur', '_default', 'rhm_de_eur', ['termsAndConditionsLink' => 'http://..']],
            ['redhotmagma', '_default', 'redhotmagma', '_default', ['termsAndConditionsLink' => 'http://..']],
            ['_default', '_default', '_default', '_default', ['termsAndConditionsLink' => 'http://..']],
        ];
    }

    public function providerShouldThrowException()
    {
        return [
            ['someone_else', 'rhm_de_eur'],
            ['i_do_not_exist', 'rhm_de_eur'],
            ['someone_else', 'i_do_not_exist'],
        ];
    }

    private function getClient($identifier)
    {
        $client = \Phake::mock(Client::class);
        \Phake::when($client)->getIdentifier()->thenReturn($identifier);

        $clientChannelCollection = new ArrayCollection();

        // add channel relation if client redhotmagma is requested
        if ('redhotmagma' === $identifier) {
            $clientChannel = \Phake::mock(ClientChannel::class);
            \Phake::when($clientChannel)->getChannel()->thenReturn($this->getChannel('rhm_de_eur'));

            $clientChannelCollection->add($clientChannel);
        }
        \Phake::when($client)->getClientChannel()->thenReturn($clientChannelCollection);

        return $client;
    }

    private function getClientText()
    {
        $clientText = new ClientText();
        $clientText->setTermsAndConditionsLink('http://..');

        return $clientText;
    }

    private function getChannel($identifier)
    {
        $channel = \Phake::mock(Channel::class);
        \Phake::when($channel)->getIdentifier()->thenReturn($identifier);

        return $channel;
    }

    private function getLanguage()
    {
        return \Phake::mock(Language::class);
    }
}
