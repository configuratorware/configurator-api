<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DataTransfer;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\EntityDefaultsReset;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\DataTransfer\VisualizationDataTransfer;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewDelete;

class VisualizationDataTransferTest extends TestCase
{
    /**
     * @var VisualizationDataTransfer
     */
    private $dataTransfer;

    /**
     * @var CreatorViewDelete|\Phake_IMock
     * @Mock CreatorViewDelete
     */
    private $creatorViewDelete;

    /**
     * @var DesignViewDelete|\Phake_IMock
     * @Mock DesignViewDelete
     */
    private $designViewDelete;

    /**
     * @var DesignAreaRepository|\Phake_IMock
     * @Mock DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var EntityDefaultsReset|\Phake_IMock
     * @Mock EntityDefaultsReset
     */
    private $entityDefaultsReset;

    public function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->dataTransfer = new VisualizationDataTransfer(
            $this->designViewDelete,
            $this->designAreaRepository,
            $this->itemRepository,
            $this->entityDefaultsReset,
            $this->creatorViewDelete
        );
    }

    public function testShouldUpdateCreatorViewDirectory(): void
    {
        $configuratorImagesDir = 'base/dir/part/';
        $sourceItem = new Item();
        $sourceItem->setIdentifier('source_item');
        $sourceItem->setConfigurationMode(Item::CONFIGURATION_MODE_CREATOR_DESIGNER);
        $sourceItem->addCreatorView($this->createCreatorView($sourceItem, $configuratorImagesDir));
        $targetItemIdentifier = 'target_item';
        $targetItem = new Item();
        $targetItem->setConfigurationMode(Item::CONFIGURATION_MODE_CREATOR_DESIGNER);
        $targetItem->setIdentifier($targetItemIdentifier);

        $this->dataTransfer->transfer($sourceItem, $targetItem);
        $expectedDirectory = $configuratorImagesDir . $targetItemIdentifier;

        \Phake::verify($this->itemRepository)->save(\Phake::capture($savedItem));
        $this->assertEquals($savedItem->getCreatorView()[0]->getDirectory(), $expectedDirectory);
    }

    private function createCreatorView(Item $item, $configuratorImagesDir): CreatorView
    {
        $directory = $configuratorImagesDir . $item->getIdentifier();

        return CreatorView::from($directory, 'some_view_identtifier', $item, '1');
    }
}
