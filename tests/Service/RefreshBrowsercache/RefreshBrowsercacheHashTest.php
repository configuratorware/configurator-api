<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\RefreshBrowsercache;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\RefreshBrowsercache\RefreshBrowsercacheHash;

final class RefreshBrowsercacheHashTest extends TestCase
{
    private const RANDOM_HASH = 'RandomHash';
    private const DIFFERENT_RANDOM_HASH = 'DifferentRandomHash';

    public $refreshBrowsercache;

    public function setUp(): void
    {
        $this->refreshBrowsercache = new RefreshBrowsercacheHash();
    }

    public function testShouldSetHashForFrontendguiIndexInitially(): void
    {
        $resultWithRandomHash = $this->refreshBrowsercache->refresh($this->givenFrontendguiIndex, self::RANDOM_HASH);

        self::assertEquals($this->expectedFrontendguiIndex, $resultWithRandomHash);
    }

    public function testShouldRefreshFrontendguiIndexAfterRefresh(): void
    {
        $resultWithRandomHash = $this->refreshBrowsercache->refresh($this->givenFrontendguiIndex, self::RANDOM_HASH);
        $resultWithDifferentRandomHash = $this->refreshBrowsercache->refresh($resultWithRandomHash, self::DIFFERENT_RANDOM_HASH);

        self::assertEquals($this->expectedFrontendguiIndexWithDifferentHash, $resultWithDifferentRandomHash);
    }

    public function testShouldSetHashForAdminguiIndexInitially(): void
    {
        $resultWithRandomHash = $this->refreshBrowsercache->refresh($this->givenAdminguiIndex, self::RANDOM_HASH);

        self::assertEquals($this->expectedAdminguiIndex, $resultWithRandomHash);
    }

    public function testShouldRefreshAdminguiIndexAfterRefresh(): void
    {
        $resultWithRandomHash = $this->refreshBrowsercache->refresh($this->givenAdminguiIndex, self::RANDOM_HASH);
        $resultWithDifferentRandomHash = $this->refreshBrowsercache->refresh($resultWithRandomHash, self::DIFFERENT_RANDOM_HASH);

        self::assertEquals($this->expectedAdminguiIndexWithDifferentHash, $resultWithDifferentRandomHash);
    }

    private $givenFrontendguiIndex = <<<'HTML'
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico">
        <link href="/bundle.css?version=" rel="stylesheet">
        <title>configuratorware</title>
        <style>
            #root {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
            }
        </style>
    </head>
    <body>
    <div id="root"></div>
    
    <script src="/invoke.js?version="></script>
    <script src="/bundle.js?version="></script>
    
    </body>
    </html>
HTML;

    private $expectedFrontendguiIndex = <<<'HTML'
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico">
        <link href="/bundle.css?version=RandomHash" rel="stylesheet">
        <title>configuratorware</title>
        <style>
            #root {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
            }
        </style>
    </head>
    <body>
    <div id="root"></div>
    
    <script src="/invoke.js?version=RandomHash"></script>
    <script src="/bundle.js?version=RandomHash"></script>
    
    </body>
    </html>
HTML;

    private $expectedFrontendguiIndexWithDifferentHash = <<<'HTML'
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico">
        <link href="/bundle.css?version=DifferentRandomHash" rel="stylesheet">
        <title>configuratorware</title>
        <style>
            #root {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
            }
        </style>
    </head>
    <body>
    <div id="root"></div>
    
    <script src="/invoke.js?version=DifferentRandomHash"></script>
    <script src="/bundle.js?version=DifferentRandomHash"></script>
    
    </body>
    </html>
HTML;

    private $givenAdminguiIndex = <<<'HTML'
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico">
        <base href="/admin/">
        <link href="bundle.css?version=" rel="stylesheet">
        <title>configuratorware adminarea</title>
    </head>
    <body>
    <div id="root"></div>
    
    <script src="bundle.js?version="></script>
    </body>
    </html>
HTML;

    private $expectedAdminguiIndex = <<<'HTML'
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico">
        <base href="/admin/">
        <link href="bundle.css?version=RandomHash" rel="stylesheet">
        <title>configuratorware adminarea</title>
    </head>
    <body>
    <div id="root"></div>
    
    <script src="bundle.js?version=RandomHash"></script>
    </body>
    </html>
HTML;

    private $expectedAdminguiIndexWithDifferentHash = <<<'HTML'
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.ico">
        <base href="/admin/">
        <link href="bundle.css?version=DifferentRandomHash" rel="stylesheet">
        <title>configuratorware adminarea</title>
    </head>
    <body>
    <div id="root"></div>
    
    <script src="bundle.js?version=DifferentRandomHash"></script>
    </body>
    </html>
HTML;
}
