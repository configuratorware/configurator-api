<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DataCheck;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\DataCheck\ItemCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheckPlaceholder;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationMode;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemStatus;

class ItemCheckTest extends TestCase
{
    /**
     * @var ItemCheck
     */
    public $itemCheck;

    public function setUp(): void
    {
        $this->itemCheck = new ItemCheck();
    }

    /**
     * @param DataCheck[] $expected
     * @param string $title
     * @param array $configurationModes
     * @dataProvider providerShouldReturnEmptyItemTitleMessage
     */
    public function testShouldReturnEmptyItemTitleMessage(array $expected, string $title, array $configurationModes): void
    {
        $item = new Item();
        $item->title = $title;
        $item->configurationModes = $configurationModes;

        self::assertEquals($expected, $this->itemCheck->checkItem($item));
    }

    public function providerShouldReturnEmptyItemTitleMessage(): array
    {
        return [
            'empty_title' => [
                [
                    new DataCheck(
                        'item.title',
                        'notice',
                        []
                    ),
                    new DataCheck(
                        'item.configuration_mode.creator',
                        'neutral',
                        [
                            new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AVAILABLE, true),
                        ]
                    ),
                    new DataCheck(
                        'item.configuration_mode.designer',
                        'notice',
                        [
                            new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AVAILABLE, false),
                        ]
                    ),
                ],
                '',
                [
                    $this->createConfigurationMode('creator', true),
                    $this->createConfigurationMode('designer', false),
                ],
            ],
            'given_title' => [
                [
                    new DataCheck(
                        'item.title',
                        'neutral',
                        []
                    ),
                    new DataCheck(
                        'item.configuration_mode.creator',
                        'notice',
                        [
                            new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AVAILABLE, false),
                        ]
                    ),
                    new DataCheck(
                        'item.configuration_mode.designer',
                        'neutral',
                        [
                            new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AVAILABLE, true),
                        ]
                    ),
                ],
                'Produkt A',
                [
                    $this->createConfigurationMode('creator', false),
                    $this->createConfigurationMode('designer', true),
                ],
            ],
        ];
    }

    private function createConfigurationMode(string $identifier, bool $isAvailable): ConfigurationMode
    {
        $configurationMode = new ConfigurationMode();
        $configurationMode->identifier = $identifier;
        $configurationMode->itemStatus = new ItemStatus();
        $configurationMode->itemStatus->item_available = $isAvailable;

        return $configurationMode;
    }
}
