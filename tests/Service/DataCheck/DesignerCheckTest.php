<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DataCheck;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Service\DataCheck\DesignerCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheckPlaceholder;

class DesignerCheckTest extends TestCase
{
    /**
     * @var DesignerCheck
     */
    private $designerCheck;

    protected function setUp(): void
    {
        $this->designerCheck = new DesignerCheck();
    }

    /**
     * @param DataCheck[] $expected
     * @param Designview[] $designViews
     * @dataProvider providerShouldCheckDesignViews
     */
    public function testShouldCheckDesignViews(array $expected, array $designViews): void
    {
        self::assertEquals($expected, $this->designerCheck->checkDesignViews($designViews));
    }

    public function providerShouldCheckDesignViews(): array
    {
        return [
            [
                [
                    new DataCheck(
                        'designer.views',
                        'neutral',
                        [
                            new DataCheckPlaceholder('amount', 2),
                            new DataCheckPlaceholder('title', 'front'),
                            new DataCheckPlaceholder('title', 'back'),
                        ]
                    ),
                ],
                [
                    $this->createDesignView('front'),
                    $this->createDesignView('back'),
                ],
            ],
            [
                [
                    new DataCheck(
                        'designer.views',
                        'notice',
                        [
                            new DataCheckPlaceholder('amount', 0),
                        ]
                    ),
                ],
                [],
            ],
        ];
    }

    /**
     * @param DataCheck[] $expected
     * @param DesignArea[] $designAreas
     * @dataProvider providerShouldCheckDesignAreas
     */
    public function testShouldCheckDesignAreas(array $expected, array $designAreas)
    {
        self::assertEquals($expected, $this->designerCheck->checkDesignAreas($designAreas));
    }

    public function providerShouldCheckDesignAreas(): array
    {
        return [
            [
                [
                    new DataCheck(
                        'designer.design_areas',
                        'error',
                        [
                            new DataCheckPlaceholder('amount', 0),
                        ]
                    ),
                ],
                [],
            ],
            [
                [
                    new DataCheck(
                        'designer.design_areas',
                        'neutral',
                        [
                            new DataCheckPlaceholder('amount', 2),
                        ],
                        [
                            new DataCheck(
                                'designer.design_area',
                                'neutral',
                                [
                                    new DataCheckPlaceholder('title', 'sleeve'),
                                ],
                                [
                                    new DataCheck(
                                        'designer.design_area.production_method',
                                        'neutral',
                                        [
                                            new DataCheckPlaceholder('title', 'print'),
                                        ]
                                    ),
                                    new DataCheck(
                                        'designer.design_area.production_method',
                                        'neutral',
                                        [
                                            new DataCheckPlaceholder('title', 'embroidery'),
                                        ]
                                    ),
                                ]
                            ),
                            new DataCheck(
                                'designer.design_area',
                                'neutral',
                                [
                                    new DataCheckPlaceholder('title', 'chest'),
                                ],
                                [
                                    new DataCheck(
                                        'designer.design_area.production_method',
                                        'neutral',
                                        [
                                            new DataCheckPlaceholder('title', 'embroidery'),
                                        ]
                                    ),
                                ]
                            ),
                        ]
                    ),
                ],
                [
                    $this->createDesignArea('sleeve', ['print', 'embroidery']),
                    $this->createDesignArea('chest', ['embroidery']),
                ],
            ],
        ];
    }

    private function createDesignView(string $identifier): DesignView
    {
        return new DesignView($identifier);
    }

    private function createDesignArea(string $identifier, array $productionMethodIdentifiers): DesignArea
    {
        $designArea = new DesignArea();
        $designArea->setIdentifier($identifier);

        foreach ($productionMethodIdentifiers as $productionMethodIdentifier) {
            $areaProductionMethod = new DesignAreaDesignProductionMethod();
            $productionMethod = new DesignProductionMethod();
            $productionMethod->setIdentifier($productionMethodIdentifier);
            $areaProductionMethod->setDesignProductionMethod($productionMethod);
            $designArea->addDesignAreaDesignProductionMethod($areaProductionMethod);
        }

        return $designArea;
    }
}
