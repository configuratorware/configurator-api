<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DataCheck;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as ConfigurationEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DataCheck\CreatorCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheckPlaceholder;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CreatorCheckTest extends ApiTestCase
{
    /**
     * @var CreatorCheck
     */
    private $creatorCheck;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->configurationRepository = $this->em->getRepository(ConfigurationEntity::class);
        $this->creatorCheck = new CreatorCheck();
    }

    /**
     * @dataProvider providerShouldReturnNoDefaultConfigurationMessage
     */
    public function testShouldReturnNoDefaultConfigurationMessage(?ConfigurationEntity $configuration): void
    {
        $expected = [
            new DataCheck(
                'creator.default.configuration',
                DataCheck::TYPE_ERROR,
                []
            ),
        ];

        self::assertEquals($expected, $this->creatorCheck->checkDefaultConfiguration($configuration, [], []));
    }

    public function providerShouldReturnNoDefaultConfigurationMessage(): array
    {
        return [
            [
                null,
            ],
            [
                $this->createConfiguration('XYZABC', []),
            ],
        ];
    }

    public function testShouldReturnDefaultConfigurationFoundMessageWhenDefaultConfigurationExists(): void
    {
        $expected = [
            new DataCheck(
                'creator.default.configuration',
                'neutral',
                [],
                [
                    new DataCheck(
                        'creator.default.option',
                        'neutral',
                        [
                            new DataCheckPlaceholder('component_name', 'Coat'),
                            new DataCheckPlaceholder('option', 'Coat nature'),
                        ]
                    ),
                    new DataCheck(
                        'creator.default.option',
                        'neutral',
                        [
                            new DataCheckPlaceholder('component_name', 'Face'),
                            new DataCheckPlaceholder('option', 'Face nature'),
                        ]
                    ),
                    new DataCheck(
                        'creator.default.option',
                        'neutral',
                        [
                            new DataCheckPlaceholder('component_name', 'Legs'),
                            new DataCheckPlaceholder('option', 'Legs nature'),
                        ]
                    ),
                    new DataCheck(
                        'creator.default.option',
                        'neutral',
                        [
                            new DataCheckPlaceholder('component_name', 'Eyes'),
                            new DataCheckPlaceholder('option', 'Eyes yellow'),
                        ]
                    ),
                    new DataCheck(
                        'creator.default.option',
                        'neutral',
                        [
                            new DataCheckPlaceholder('component_name', 'Ear & eyelid'),
                            new DataCheckPlaceholder('option', 'Ear & eyelid nature'),
                        ]
                    ),
                ]
            ),
        ];

        $componentsByIdentifier = [
            'coat' => $this->createComponent('coat', 'Coat'),
            'face' => $this->createComponent('face', 'Face'),
            'legs' => $this->createComponent('legs', 'Legs'),
            'eyes' => $this->createComponent('eyes', 'Eyes'),
            'ear_eyelid' => $this->createComponent('ear_eyelid', 'Ear & eyelid'),
        ];

        $optionsByIdentifier = [
            'coat_nature' => $this->createOption('coat_nature', 'Coat nature'),
            'face_nature' => $this->createOption('face_nature', 'Face nature'),
            'legs_nature' => $this->createOption('legs_nature', 'Legs nature'),
            'eyes_yellow' => $this->createOption('eyes_yellow', 'Eyes yellow'),
            'ear_eyelid_nature' => $this->createOption('ear_eyelid_nature', 'Ear & eyelid nature'),
        ];

        $configuration = $this->configurationRepository->findOneById(1);

        self::assertEquals($expected, $this->creatorCheck->checkDefaultConfiguration($configuration, $componentsByIdentifier, $optionsByIdentifier));
    }

    /**
     * @param array $expected
     * @param array $optionsByComponentIdentifier
     * @param array $componentsByIdentifier
     * @dataProvider providerShouldCheckComponents
     */
    public function testShouldCheckComponents(array $expected, array $optionsByComponentIdentifier, array $componentsByIdentifier): void
    {
        self::assertEquals($expected, $this->creatorCheck->checkComponents($optionsByComponentIdentifier, $componentsByIdentifier));
    }

    public function providerShouldCheckComponents(): array
    {
        return [
            'no_components_message_when_no_component_is_found' => [
                [
                    new DataCheck(
                        'creator.component',
                        'error',
                        []
                    ),
                ],
                [],
                [],
            ],
            'component_set_message_when_component_is_set' => [
                [
                    new DataCheck(
                        'creator.component',
                        'neutral',
                        [new DataCheckPlaceholder('components_amount', 2)],
                        [
                            new DataCheck(
                                'creator.component.title',
                                'neutral',
                                [
                                    new DataCheckPlaceholder('component_name', 'Component 1'),
                                    new DataCheckPlaceholder('options_amount', 2),
                                ],
                                [
                                    new DataCheck(
                                        'creator.component.option',
                                        'neutral',
                                        [
                                            new DataCheckPlaceholder('component_name', 'Component 1'),
                                            new DataCheckPlaceholder('options_amount', 2),
                                            new DataCheckPlaceholder('options', 'Option 1, Option 2'),
                                        ]
                                    ),
                                ]
                            ),
                            new DataCheck(
                                'creator.component.title',
                                'neutral',
                                [
                                    new DataCheckPlaceholder('component_name', 'Component 2'),
                                    new DataCheckPlaceholder('options_amount', 1),
                                ],
                                [
                                    new DataCheck(
                                        'creator.component.option',
                                        'neutral',
                                        [
                                            new DataCheckPlaceholder('component_name', 'Component 2'),
                                            new DataCheckPlaceholder('options_amount', 1),
                                            new DataCheckPlaceholder('options', 'Option 1'),
                                        ]
                                    ),
                                ]
                            ),
                        ]
                    ),
                ],
                [
                    'component1' => [
                        $this->createOption('option1', 'Option 1'),
                        $this->createOption('option2', 'Option 2'),
                    ],
                    'component2' => [
                        $this->createOption('option1', 'Option 1'),
                    ],
                ],
                [
                    'component1' => $this->createComponent('component1', 'Component 1'),
                    'component2' => $this->createComponent('component2', 'Component 2'),
                ],
            ],
            'no_options_set_message_when_option_not_set' => [
                [
                    new DataCheck(
                        'creator.component',
                        'neutral',
                        [new DataCheckPlaceholder('components_amount', 1)],
                        [
                            new DataCheck(
                                'creator.component.title',
                                'error',
                                [
                                    new DataCheckPlaceholder('component_name', 'Component 1'),
                                    new DataCheckPlaceholder('options_amount', 0),
                                ],
                                [
                                    new DataCheck(
                                        'creator.component.option',
                                        'neutral',
                                        [
                                            new DataCheckPlaceholder('component_name', 'Component 1'),
                                            new DataCheckPlaceholder('options_amount', 0),
                                            new DataCheckPlaceholder('options', 'n/a'),
                                        ]
                                    ),
                                ]
                            ),
                        ]
                    ),
                ],
                ['component1' => []],
                ['component1' => $this->createComponent('component1', 'Component 1')],
            ],
        ];
    }

    /**
     * @param $expected
     * @param $rules
     * @dataProvider dataProviderShouldCheckOptionRules
     */
    public function testShouldReturnAmountOfRulesSetWhenNoRuleIsSet($expected, $rules): void
    {
        self::assertEquals($expected, $this->creatorCheck->checkRules([], $rules));
    }

    public function dataProviderShouldCheckOptionRules(): array
    {
        return [
            'amount_should_be_0_when_no_rule_set' => [
                [
                    new DataCheck(
                        'creator.rule',
                        'neutral',
                        [
                            new DataCheckPlaceholder('amount', 0),
                        ]
                    ),
                ],
                [],
            ],
            'amount_set_when_rules_found' => [
                [
                    new DataCheck(
                        'creator.rule',
                        'neutral',
                        [
                            new DataCheckPlaceholder('amount', 3),
                        ]
                    ),
                ],
                [
                    $this->createRule(),
                    $this->createRule(),
                    $this->createRule(),
                ],
            ],
        ];
    }

    private function createComponent(string $identifier, string $title): OptionClassification
    {
        $component = new OptionClassification();
        $component->identifier = $identifier;
        $component->title = $title;

        return $component;
    }

    private function createOption(string $identifier, string $title): Option
    {
        $option = new Option();
        $option->identifier = $identifier;
        $option->title = $title;

        return $option;
    }

    private function createRule(): Rule
    {
        return new Rule();
    }

    private function createConfiguration($code, $selectedOptions): ConfigurationEntity
    {
        $configuration = new ConfigurationEntity();
        $configuration->setCode($code);
        $configuration->setSelectedoptions($selectedOptions);

        return $configuration;
    }
}
