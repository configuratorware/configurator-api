<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Item;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item as ItemEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus as ItemStatusEntity;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemSave;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemStatusUpdate;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item as ItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus as ItemStatusStructure;

class ItemStatusUpdateTest extends TestCase
{
    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemStatusRepository|\Phake_IMock
     * @Mock ItemStatusRepository
     */
    private $itemStatusRepository;

    /**
     * @var ItemSave|\Phake_IMock
     * @Mock ItemSave
     */
    private $itemSave;

    /**
     * @var ItemStatusUpdate
     */
    private $service;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->service = new ItemStatusUpdate($this->itemRepository, $this->itemStatusRepository, $this->itemSave);
    }

    public function testSuccessfulUpdate(): void
    {
        $itemStructure = new ItemStructure();
        $itemStructure->id = 1;

        $itemStatusStructure = new ItemStatusStructure();
        $itemStatusStructure->id = 1;

        $itemEntity = new ItemEntity();
        $itemStatusEntity = new ItemStatusEntity();

        \Phake::when($this->itemRepository)->find->thenReturn($itemEntity);
        \Phake::when($this->itemStatusRepository)->find->thenReturn($itemStatusEntity);

        $this->service->updateItemStatus($itemStructure, $itemStatusStructure);

        \Phake::verify($this->itemRepository)->save($itemEntity);
    }

    public function testExceptionThrownSuccessfully(): void
    {
        $itemStructure = new ItemStructure();
        $itemStructure->id = 1;

        $itemStatusStructure = new ItemStatusStructure();
        $itemStatusStructure->id = 1;

        \Phake::when($this->itemRepository)->find->thenReturn(null);
        \Phake::when($this->itemStatusRepository)->find->thenReturn(null);

        $this->expectException(NotFoundException::class);

        $this->service->updateItemStatus($itemStructure, $itemStatusStructure);
    }
}
