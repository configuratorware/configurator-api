<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Doctrine\ORM\EntityRepository;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback;
use Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype;
use Redhotmagma\ConfiguratorApiBundle\Entity\Stockstatus;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ItemImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RulefeedbackRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuletypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemDuplicate;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\Paths;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemDuplicate as ItemDuplicateStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Itemtext;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\AttributeFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\AttributevalueFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ConfigurationFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ConfigurationtypeFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\CreatorViewFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\CreatorViewTextFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\DesignAreaDesignProductionMethodFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\DesignAreaFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\DesignAreaTextFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\DesignProductionMethodFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemAttributeFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemclassificationFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemgroupentryFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemgroupFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemItemclassificationFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemItemgroupentryFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemItemgroupFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemOptionclassificationFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemOptionclassificationOptionFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItempriceFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemtextFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\OptionclassificationFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\OptionFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\RuleFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\RuleTextFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\StockFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\Test\Factories;

class ItemDuplicateTest extends ApiTestCase
{
    use Factories;
    use VfsTestTrait;

    private ItemDuplicate $itemDuplicateService;

    private LanguageRepository $languageRepository;
    private ChannelRepository $channelRepository;
    private EntityRepository $stockstatusRepository;
    private RuletypeRepository $ruletypeRepository;
    private RulefeedbackRepository $rulefeedbackRepository;
    private ConfigurationRepository $configurationRepository;
    private ItemImageRepository $itemImageRepository;
    private vfsStreamDirectory $root;

    /**
     * @var Item|Proxy
     */
    private $item;

    /**
     * @var Item|Proxy
     */
    private $existingItem;

    /**
     * @var Configuration|Proxy
     */
    private $defaultConfiguration;

    /**
     * @var Configuration|Proxy
     */
    private $existingDefaultConfiguration;

    public function setUp(): void
    {
        $kernel = self::bootKernel();

        $em = $kernel->getContainer()->get('doctrine')->getManager();
        $this->languageRepository = $em->getRepository(Language::class);
        $this->channelRepository = $em->getRepository(Channel::class);
        $this->stockstatusRepository = $em->getRepository(Stockstatus::class);
        $this->ruletypeRepository = $em->getRepository(Ruletype::class);
        $this->rulefeedbackRepository = $em->getRepository(Rulefeedback::class);
        $this->configurationRepository = $em->getRepository(Configuration::class);
        $this->configurationTypeRepository = $em->getRepository(Configurationtype::class);

        $this->itemDuplicateService = new ItemDuplicate(
            $em,
            $em->getRepository(Item::class),
            $kernel->getContainer()->get(EntityFromStructureConverter::class),
            new Paths(
                'images/components',
                'images/printfiles',
                'images/item/view',
                'images/item/view_thumb',
                '/designer/fonts',
                'images/item/image',
                'images/item/thumb',
                'images/configuratorimages',
                '%kernel.project_dir%/license/license_file.txt',
                'vfs://app/_public',
                'images/options',
                'images/configurations'
            ),
            new ItemImageRepository(
                'vfs://app/_public',
                'images/item/image',
                'images/item/thumb'
            ),
            'vfs://app/_public',
            'vfs://app/_public/visualization_component/{configurationMode}/{itemIdentifier}',
            $this->configurationRepository,
            $kernel->getContainer()->get(ConfigurationCodeGenerator::class),
            $this->configurationTypeRepository
        );

        $structure = [
            '_public' => [
                'images' => [
                    'configuratorimages' => [
                        'test-item' => [
                            'test-image.jpg' => 'test-image-content',
                        ],
                    ],
                    'item' => [
                        'image' => [
                            'test-item.jpg' => 'test-item-image-content',
                        ],
                        'thumb' => [
                            'test-item.jpg' => 'test-item-thumb-content',
                        ],
                    ],
                ],
                'visualization_component' => [
                    'creator' => [
                        'test-item' => [
                            'test-file.gltf' => 'test-file-content',
                        ],
                    ],
                ],
            ],
        ];
        $this->root = vfsStream::setup('app', 0744, $structure);
    }

    public function testShouldDuplicateItem(): void
    {
        $this->createItemToDuplicate();
        $this->createExistingItem();
        $this->createDefaultConfigurationToDuplicate($this->item);
        $this->createExistingDefaultConfiguration($this->existingItem);

        $this->assertDirectoryNotExists('vfs://app/_public/images/configuratorimages/test-item-duplicate', 'Precondition fail: Item duplicate 2D creator directory should not exist');
        $this->assertFileNotExists('vfs://app/_public/images/item/image/test-item-duplicate.jpg', 'Precondition fail: Item duplicate image file should not exist');
        $this->assertFileNotExists('vfs://app/_public/images/item/thumb/test-item-duplicate.jpg', 'Precondition fail: Item duplicate thumb file should not exist');
        $this->assertDirectoryNotExists('vfs://app/_public/visualization_component/creator/test-item-duplicate', 'Precondition fail: Item duplicate creator directory should not exist');

        $itemDuplicate = new ItemDuplicateStructure();
        $itemDuplicate->identifier = 'test-item-duplicate';
        $itemText = new Itemtext();
        $itemText->title = 'test-title';
        $itemText->abstract = 'test-abstract';
        $itemText->description = 'test-description';
        $itemText->language = 'de_DE';
        $itemDuplicate->itemTexts[] = $itemText;

        $actual = $this->itemDuplicateService->duplicate(
            $this->item->object(),
            $itemDuplicate
        );

        $actualDefaultConfiguration = ConfigurationFactory::findBy(['item' => $actual])[0]->object();

        /**
         * Item assertions.
         */
        $this->assertEquals('test-item-duplicate', $actual->getIdentifier(), 'Identifier differs.');
        $this->assertEqualsWithDelta(new \DateTime(), $actual->getDateCreated(), 5, 'Created date differs.');
        $this->assertEquals('test-item-external-id', $actual->getExternalId(), 'External ID differs.');
        $this->assertTrue($actual->getConfigurable(), 'Configurable differs.');
        $this->assertFalse($actual->getDeactivated(), 'Deactivated differs.');
        $this->assertEquals(10, $actual->getMinimumOrderAmount(), 'Minimum order amount differs.');
        $this->assertTrue($actual->getAccumulateAmounts(), 'Accumulate amounts differs.');
        $this->assertEquals('test-configuration-mode', $actual->getConfigurationMode(), 'Configuration mode differs.');
        $this->assertEquals('test-call-to-action', $actual->getCallToAction(), 'Call to action differs.');
        $this->assertTrue($actual->getOverwriteComponentOrder(), 'Overwrite component order differs.');

        /**
         * Default configuration assertions.
         */
        $this->assertEquals($actualDefaultConfiguration->getName(), $this->defaultConfiguration->getName());
        $this->assertEquals($actualDefaultConfiguration->getPartslisthash(), $this->defaultConfiguration->getPartslisthash());
        $this->assertEquals($actualDefaultConfiguration->getConfigurationtype()->getIdentifier(), $this->defaultConfiguration->getConfigurationtype()->getIdentifier());
        $this->assertEquals($actualDefaultConfiguration->getSelectedoptions(), $this->defaultConfiguration->getSelectedoptions());

        /**
         * Itemtext assertions.
         */
        $this->assertCount(1, $actual->getItemtext(), 'Itemtext count differs.');
        $this->assertEquals('test-title', $actual->getItemtext()->first()->getTitle(), 'Title differs.');
        $this->assertEquals('test-abstract', $actual->getItemtext()->first()->getAbstract(), 'Abstract differs.');
        $this->assertEquals('test-description', $actual->getItemtext()->first()->getDescription(), 'Description differs.');

        /**
         * Itemprice assertions.
         */
        $this->assertCount(1, $actual->getItemprice(), 'Itemprice count differs.');
        $this->assertEquals(10.5, $actual->getItemprice()->first()->getPrice(), 'PriceModel differs.');
        $this->assertEquals(12, $actual->getItemprice()->first()->getPriceNet(), 'PriceModel net differs.');
        $this->assertEquals(10, $actual->getItemprice()->first()->getAmountfrom(), 'Amount from differs.');
        $this->assertEquals('_default', $actual->getItemprice()->first()->getChannel()->getIdentifier(), 'Channel differs.');

        /**
         * ItemAttribute assertions.
         */
        $this->assertCount(1, $actual->getItemAttribute(), 'ItemAttribute count differs.');
        $this->assertEquals(2, $actual->getItemAttribute()->first()->getSequencenumber(), 'Sequencenumber differs.');
        $this->assertEquals('test-attribute', $actual->getItemAttribute()->first()->getAttribute()->getIdentifier(), 'Attribute differs.');
        $this->assertEquals('test-attribute-value', $actual->getItemAttribute()->first()->getAttributevalue()->getValue(), 'Attribute value differs.');

        /**
         * ItemItemclassification assertions.
         */
        $this->assertCount(1, $actual->getItemItemclassification(), 'ItemItemclassification count differs.');
        $this->assertEquals('test-item-classification', $actual->getItemItemclassification()->first()->getItemclassification()->getIdentifier(), 'ItemItemclassification differs.');

        /**
         * ItemOptionclassification assertions.
         */
        $this->assertCount(1, $actual->getItemOptionclassification(), 'ItemOptionclassification count differs.');
        $actualItemOptionclassification = $actual->getItemOptionclassification()->first();
        $this->assertTrue($actualItemOptionclassification->getIsMultiselect(), 'ItemOptionclassification isMultiselect differs.');
        $this->assertEquals(10, $actualItemOptionclassification->getMinamount(), 'ItemOptionclassification minamount differs.');
        $this->assertEquals(20, $actualItemOptionclassification->getMaxamount(), 'ItemOptionclassification maxamount differs.');
        $this->assertTrue($actualItemOptionclassification->getIsMandatory(), 'ItemOptionclassification isMandatory differs.');
        $this->assertEquals(2, $actualItemOptionclassification->getSequencenumber(), 'ItemOptionclassification sequencenumber differs.');
        $this->assertEquals('test-creator-view', $actualItemOptionclassification->getCreatorView()->getIdentifier(), 'ItemOptionclassification CreatorView differs.');
        $this->assertEquals('test-option-classification', $actualItemOptionclassification->getOptionclassification()->getIdentifier(), 'Optionclassification differs.');

        $this->assertCount(2, $actualItemOptionclassification->getItemOptionclassificationOption(), 'ItemOptionclassificationOption count differs.');
        $actualItemOptionclassificationOptions = $actualItemOptionclassification->getItemOptionclassificationOption()->toArray();
        $this->assertTrue($actualItemOptionclassificationOptions[0]->getAmountisselectable(), 'ItemOptionclassificationOption 1 Amountisselectable differs.');
        $this->assertEquals('test-option', $actualItemOptionclassificationOptions[0]->getOption()->getIdentifier(), 'ItemOptionclassificationOption Option 1 differs.');
        $this->assertFalse($actualItemOptionclassificationOptions[1]->getAmountisselectable(), 'ItemOptionclassificationOption 2 Amountisselectable differs.');
        $this->assertEquals('test-option-2', $actualItemOptionclassificationOptions[1]->getOption()->getIdentifier(), 'ItemOptionclassificationOption Option 2 differs.');

        /**
         * Stock assertions.
         */
        $this->assertCount(1, $actual->getStock(), 'Stock count differs.');
        $actualStock = $actual->getStock()->first();
        $this->assertEquals(new \DateTime('2020-01-01'), $actualStock->getDeliverydate(), 'Stock delivery date differs.');
        $this->assertEquals('test-delivery-time', $actualStock->getDeliverytime(), 'Stock delivery time differs.');
        $this->assertEquals(20, $actual->getStock()->first()->getAmount(), 'Stock amount differs.');
        $this->assertEquals('_default', $actualStock->getChannel()->getIdentifier(), 'Stock channel differs.');
        $this->assertEquals('available', $actualStock->getStockstatus()->getIdentifier(), 'Stock status differs.');

        /**
         * Rule assertions.
         */
        $this->assertCount(1, $actual->getRule(), 'Rule count differs.');
        $actualRule = $actual->getRule()->first();
        $this->assertEquals('test-rule-data', $actualRule->getData(), 'Rule data differs.');
        $this->assertEquals('test-rule-action', $actualRule->getAction(), 'Rule action differs.');
        $this->assertTrue($actualRule->getOrderable(), 'Rule orderable differs.');
        $this->assertEquals('test-rule-title', $actualRule->getRuleText()->first()->getTitle(), 'Rule title differs.');
        $this->assertEquals('de_DE', $actualRule->getRuleText()->first()->getLanguage()->getIso(), 'Rule language differs.');
        $this->assertEquals('optiondependency', $actualRule->getRuletype()->getIdentifier(), 'Rule ruletype differs.');
        $this->assertEquals('silent', $actualRule->getRulefeedback()->getIdentifier(), 'Rule rulefeedback differs.');
        $this->assertEquals('test-rule-option', $actualRule->getOption()->getIdentifier(), 'Rule option differs.');

        /**
         * ItemItemGroup assertions.
         */
        $this->assertCount(1, $actual->getItemItemGroup(), 'ItemItemGroup count differs.');
        $actualItemItemGroup = $actual->getItemItemGroup()->first();
        $this->assertEquals('test-item-group', $actualItemItemGroup->getItemGroup()->getIdentifier(), 'Item group differs.');

        /**
         * ItemItemGroupEntry assertions.
         */
        $this->assertCount(1, $actual->getItemItemGroupEntry(), 'ItemItemGroupEntry count differs.');
        $actualItemItemGroupEntry = $actual->getItemItemGroupEntry()->first();
        $this->assertEquals('test-item-group-entry', $actualItemItemGroupEntry->getItemGroupEntry()->getIdentifier(), 'Item group entry differs.');
        $this->assertEquals('test-item-group-entry-item-group', $actualItemItemGroupEntry->getItemGroup()->getIdentifier(), 'Item group entry Item group differs.');

        /**
         * DesignArea assertions.
         */
        $this->assertCount(1, $actual->getDesignArea(), 'DesignArea count differs.');
        $actualDesignArea = $actual->getDesignArea()->first();
        $this->assertEquals('test-design-area', $actualDesignArea->getIdentifier(), 'Design area identifier differs.');
        $this->assertEquals(2, $actualDesignArea->getSequenceNumber(), 'Design area sequence number differs.');
        $this->assertEquals(10, $actualDesignArea->getHeight(), 'Design area height differs.');
        $this->assertEquals(20, $actualDesignArea->getWidth(), 'Design area width differs.');
        $expectedMask = new \stdClass();
        $expectedMask->identifier = 'test-mask';
        $this->assertEquals($expectedMask, $actualDesignArea->getMask(), 'Design area mask differs.');
        $this->assertEquals('test-custom-data', $actualDesignArea->getCustomData(), 'Design area custom data differs.');
        $this->assertCount(1, $actualDesignArea->getDesignAreaText(), 'DesignAreaText count differs.');
        $actualDesignAreaText = $actualDesignArea->getDesignAreaText()->first();
        $this->assertEquals('test-design-area-title', $actualDesignAreaText->getTitle(), 'Design area text title differs.');
        $this->assertEquals('de_DE', $actualDesignAreaText->getLanguage()->getIso(), 'Design area text language differs.');
        $actualDesignAreaDesignProductionMethod = $actualDesignArea->getDesignAreaDesignProductionMethod()->first();
        $this->assertEquals(10, $actualDesignAreaDesignProductionMethod->getWidth(), 'Design area design production method width differs.');
        $this->assertEquals(20, $actualDesignAreaDesignProductionMethod->getHeight(), 'Design area design production method height differs.');
        $this->assertEquals('test-custom-data', $actualDesignAreaDesignProductionMethod->getCustomData(), 'Design area design production method custom data differs.');
        $this->assertTrue($actualDesignAreaDesignProductionMethod->getAllowBulkNames(), 'Design area design production method allow bulk names differs.');
        $this->assertTrue($actualDesignAreaDesignProductionMethod->getIsDefault(), 'Design area design production method is default differs.');
        $this->assertEquals(10, $actualDesignAreaDesignProductionMethod->getMinimumOrderAmount(), 'Design area design production method minimum order amount differs.');
        $this->assertEquals(10, $actualDesignAreaDesignProductionMethod->getMaxElements(), 'Design area design production method max elements differs.');
        $this->assertEquals(5, $actualDesignAreaDesignProductionMethod->getMaxTexts(), 'Design area design production method max texts differs.');
        $this->assertEquals(2, $actualDesignAreaDesignProductionMethod->getMaxImages(), 'Design area design production method max images differs.');
        $this->assertTrue($actualDesignAreaDesignProductionMethod->getOneLineText(), 'Design area design production method one line text differs.');
        $this->assertTrue($actualDesignAreaDesignProductionMethod->getDesignElementsLocked(), 'Design area design production method design elements locked differs.');
        $this->assertEquals('test-design-production-method', $actualDesignAreaDesignProductionMethod->getDesignProductionMethod()->getIdentifier(), 'Design area design production method design production method differs.');

        /**
         * CreatorView assertions.
         */
        $this->assertCount(1, $actual->getCreatorView(), 'CreatorView count differs.');
        $actualCreatorView = $actual->getCreatorView()->first();
        $this->assertEquals('test-creator-view', $actualCreatorView->getIdentifier(), 'Creator view identifier differs.');
        $this->assertEquals('test-directory', $actualCreatorView->getDirectory(), 'Creator view directory differs.');
        $this->assertEquals(2, $actualCreatorView->getSequenceNumber(), 'Creator view sequence number differs.');
        $actualCreatorViewText = $actualCreatorView->getCreatorViewText()->first();
        $this->assertEquals('test-creator-view-title', $actualCreatorViewText->getTitle(), 'Creator view text title differs.');
        $this->assertEquals('de_DE', $actualCreatorViewText->getLanguage()->getIso(), 'Creator view text language differs.');

        /**
         * Files assertions.
         */
        $this->assertDirectoryExists('vfs://app/_public/images/configuratorimages/test-item-duplicate');
        $this->assertFileExists('vfs://app/_public/images/configuratorimages/test-item-duplicate/test-image.jpg');
        $this->assertFileExists('vfs://app/_public/images/item/image/test-item-duplicate.jpg');
        $this->assertFileExists('vfs://app/_public/images/item/thumb/test-item-duplicate.jpg');
        $this->assertDirectoryExists('vfs://app/_public/visualization_component/creator/test-item-duplicate');
        $this->assertFileExists('vfs://app/_public/visualization_component/creator/test-item-duplicate/test-file.gltf');
    }

    private function createItemToDuplicate(): void
    {
        $languageDE = $this->languageRepository->findOneByIso('de_DE');
        $channel = $this->channelRepository->findOneByIdentifier('_default');

        $this->item = ItemFactory::createOne([
            'identifier' => 'test-item',
            'externalId' => 'test-item-external-id',
            'configurable' => true,
            'deactivated' => false,
            'minimumOrderAmount' => 10,
            'accumulateAmounts' => true,
            'configurationMode' => 'test-configuration-mode',
            'callToAction' => 'test-call-to-action',
            'overwriteComponentOrder' => true,
        ]);
        $this->item->disableAutoRefresh();

        $this->item->addItemprice(ItempriceFactory::createOne([
            'item' => $this->item,
            'price' => 10.5,
            'price_net' => 12,
            'amountfrom' => 10,
            'channel' => $channel,
        ])->object());

        $this->item->addItemAttribute(ItemAttributeFactory::createOne([
            'sequencenumber' => 2,
            'attribute' => AttributeFactory::createOne([
                'identifier' => 'test-attribute',
            ]),
            'attributevalue' => AttributevalueFactory::createOne([
                'value' => 'test-attribute-value',
            ]),
            'item' => $this->item,
        ])->object());

        $this->item->addItemItemclassification(ItemItemclassificationFactory::createOne([
            'item' => $this->item,
            'itemclassification' => ItemclassificationFactory::createOne([
                'identifier' => 'test-item-classification',
            ]),
        ])->object());

        $itemOptionclassification = ItemOptionclassificationFactory::createOne([
            'is_multiselect' => true,
            'minamount' => 10,
            'maxamount' => 20,
            'is_mandatory' => true,
            'sequencenumber' => 2,
            'optionclassification' => OptionclassificationFactory::createOne([
                'identifier' => 'test-option-classification',
            ]),
            'creatorView' => CreatorViewFactory::createOne([
                'identifier' => 'test-creator-view',
            ]),
            'item' => $this->item,
        ])->object();

        $this->item->addItemOptionclassification($itemOptionclassification);

        ItemOptionclassificationOptionFactory::createOne([
            'amountisselectable' => true,
            'option' => OptionFactory::createOne([
                'identifier' => 'test-option',
            ]),
            'itemOptionclassification' => $itemOptionclassification,
        ]);
        ItemOptionclassificationOptionFactory::createOne([
            'amountisselectable' => false,
            'option' => OptionFactory::createOne([
                'identifier' => 'test-option-2',
            ]),
            'itemOptionclassification' => $itemOptionclassification,
        ]);

        $this->item->addStock(StockFactory::createOne([
            'deliverydate' => new \DateTime('2020-01-01'),
            'deliverytime' => 'test-delivery-time',
            'amount' => 20,
            'channel' => $channel,
            'stockstatus' => $this->stockstatusRepository->findOneByIdentifier('available'),
            'item' => $this->item,
        ])->object());

        $ruleText = RuleTextFactory::createOne([
            'title' => 'test-rule-title',
            'language' => $languageDE,
        ]);
        $rule = RuleFactory::createOne([
            'data' => 'test-rule-data',
            'action' => 'test-rule-action',
            'orderable' => true,
            'ruleText' => [$ruleText],
            'ruletype' => $this->ruletypeRepository->findOneByIdentifier('optiondependency'),
            'rulefeedback' => $this->rulefeedbackRepository->findOneByIdentifier('silent'),
            'option' => OptionFactory::createOne([
                'identifier' => 'test-rule-option',
            ]),
            'item' => $this->item,
        ])->object();
        $ruleText->setRule($rule);
        $this->item->addRule($rule);

        $this->item->addItemItemgroup(ItemItemgroupFactory::createOne([
            'item' => $this->item,
            'itemgroup' => ItemgroupFactory::createOne([
                'identifier' => 'test-item-group',
            ]),
        ])->object());

        $this->item->addItemItemgroupentry(ItemItemgroupentryFactory::createOne([
            'item' => $this->item,
            'itemgroupentry' => ItemgroupentryFactory::createOne([
                'identifier' => 'test-item-group-entry',
            ]),
            'itemgroup' => ItemgroupFactory::createOne([
                'identifier' => 'test-item-group-entry-item-group',
            ]),
        ])->object());

        $mask = new \stdClass();
        $mask->identifier = 'test-mask';
        $designArea = DesignAreaFactory::createOne([
            'identifier' => 'test-design-area',
            'sequence_number' => 2,
            'height' => 10,
            'width' => 20,
            'mask' => $mask,
            'custom_data' => 'test-custom-data',
            'option' => OptionFactory::createOne([
                'identifier' => 'test-design-area-option',
            ]),
            'item' => $this->item,
        ])->object();
        $designAreaText = DesignAreaTextFactory::createOne([
            'title' => 'test-design-area-title',
            'language' => $languageDE,
            'designArea' => $designArea,
        ])->object();
        $designArea->addDesignAreaText($designAreaText);

        $designAreaDesignProductionMethod = DesignAreaDesignProductionMethodFactory::createOne([
            'width' => 10,
            'height' => 20,
            'additional_data' => 'test-additional-data',
            'custom_data' => 'test-custom-data',
            'allow_bulk_names' => true,
            'is_default' => true,
            'minimum_order_amount' => 10,
            'max_elements' => 10,
            'max_texts' => 5,
            'max_images' => 2,
            'one_line_text' => true,
            'design_elements_locked' => true,
            'designProductionMethod' => DesignProductionMethodFactory::createOne([
                'identifier' => 'test-design-production-method',
            ]),
            'designArea' => $designArea,
        ])->object();
        $designArea->addDesignAreaDesignProductionMethod($designAreaDesignProductionMethod);

        $this->item->addDesignArea($designArea);

        $creatorView = CreatorViewFactory::createOne([
            'identifier' => 'test-creator-view',
            'directory' => 'test-directory',
            'sequence_number' => 2,
            'item' => $this->item,
        ])->object();

        $creatorViewText = CreatorViewTextFactory::createOne([
            'title' => 'test-creator-view-title',
            'language' => $languageDE,
            'creatorView' => $creatorView,
        ])->object();
        $creatorView->addCreatorViewText($creatorViewText);

        $this->item->addCreatorView($creatorView);

        $this->item->enableAutoRefresh();
        $this->item->save();
    }

    private function createExistingItem(): void
    {
        $languageDE = $this->languageRepository->findOneByIso('de_DE');

        $existing = ItemFactory::createOne([
            'identifier' => 'test-item-duplicate',
            'externalId' => 'test-item-external-id-old',
            'configurable' => false,
            'deactivated' => true,
            'minimumOrderAmount' => 5,
            'accumulateAmounts' => false,
            'configurationMode' => 'test-configuration-mode-old',
            'callToAction' => 'test-call-to-action-old',
            'overwriteComponentOrder' => false,
        ]);
        $existing->disableAutoRefresh();

        $existing->addItemtext(ItemtextFactory::createOne([
            'item' => $existing,
            'title' => 'test-title-old',
            'abstract' => 'test-abstract-old',
            'description' => 'test-description-old',
            'language' => $languageDE,
        ])->object());

        $existing->addItemprice(ItempriceFactory::createOne([
            'item' => $existing,
            'price' => 20,
            'price_net' => 25,
            'amountfrom' => 5,
        ])->object());

        $existing->addItemAttribute(ItemAttributeFactory::createOne([
            'sequencenumber' => 1,
            'attribute' => AttributeFactory::createOne([
                'identifier' => 'test-attribute-old',
            ]),
            'attributevalue' => AttributevalueFactory::createOne([
                'value' => 'test-attribute-value-old',
            ]),
            'item' => $existing,
        ])->object());

        $existing->addItemItemclassification(ItemItemclassificationFactory::createOne([
            'item' => $existing,
            'itemclassification' => ItemclassificationFactory::createOne([
                'identifier' => 'test-item-classification-old',
            ]),
        ])->object());

        $itemOptionclassification = ItemOptionclassificationFactory::createOne([
            'is_multiselect' => false,
            'minamount' => 5,
            'maxamount' => 10,
            'is_mandatory' => false,
            'sequencenumber' => 1,
            'optionclassification' => OptionclassificationFactory::createOne([
                'identifier' => 'test-option-classification-old',
            ]),
            'creatorView' => CreatorViewFactory::createOne([
                'identifier' => 'test-creator-view-old',
            ]),
            'item' => $existing,
        ])->object();

        $existing->addItemOptionclassification($itemOptionclassification);

        ItemOptionclassificationOptionFactory::createOne([
            'amountisselectable' => true,
            'option' => OptionFactory::createOne([
                'identifier' => 'test-option-old',
            ]),
            'itemOptionclassification' => $itemOptionclassification,
        ]);

        $existing->addStock(StockFactory::createOne([
            'deliverydate' => new \DateTime('2021-01-01'),
            'deliverytime' => 'test-delivery-time-old',
            'amount' => 10,
            'stockstatus' => $this->stockstatusRepository->findOneByIdentifier('limited_availability'),
            'item' => $existing,
        ])->object());

        $ruleText = RuleTextFactory::createOne([
            'title' => 'test-rule-title-old',
            'language' => $languageDE,
        ]);
        $rule = RuleFactory::createOne([
            'data' => 'test-rule-data-old',
            'action' => 'test-rule-action-old',
            'orderable' => false,
            'ruleText' => [$ruleText],
            'ruletype' => $this->ruletypeRepository->findOneByIdentifier('optionexclusion'),
            'rulefeedback' => $this->rulefeedbackRepository->findOneByIdentifier('error'),
            'option' => OptionFactory::createOne([
                'identifier' => 'test-rule-option-old',
            ]),
            'item' => $existing,
        ])->object();
        $ruleText->setRule($rule);
        $existing->addRule($rule);

        $existing->addItemItemgroup(ItemItemgroupFactory::createOne([
            'item' => $existing,
            'itemgroup' => ItemgroupFactory::createOne([
                'identifier' => 'test-item-group-old',
            ]),
        ])->object());

        $existing->addItemItemgroupentry(ItemItemgroupentryFactory::createOne([
            'item' => $existing,
            'itemgroupentry' => ItemgroupentryFactory::createOne([
                'identifier' => 'test-item-group-entry-old',
            ]),
            'itemgroup' => ItemgroupFactory::createOne([
                'identifier' => 'test-item-group-entry-item-group-old',
            ]),
        ])->object());

        $mask = new \stdClass();
        $mask->identifier = 'test-mask-old';
        $designArea = DesignAreaFactory::createOne([
            'identifier' => 'test-design-area-old',
            'sequence_number' => 1,
            'height' => 5,
            'width' => 10,
            'mask' => $mask,
            'custom_data' => 'test-custom-data-old',
            'option' => OptionFactory::createOne([
                'identifier' => 'test-design-area-option-old',
            ]),
            'item' => $existing,
        ])->object();
        $designAreaText = DesignAreaTextFactory::createOne([
            'title' => 'test-design-area-title-old',
            'language' => $languageDE,
            'designArea' => $designArea,
        ])->object();
        $designArea->addDesignAreaText($designAreaText);

        $designAreaDesignProductionMethod = DesignAreaDesignProductionMethodFactory::createOne([
            'width' => 5,
            'height' => 10,
            'additional_data' => 'test-additional-data-old',
            'custom_data' => 'test-custom-data-old',
            'allow_bulk_names' => false,
            'is_default' => false,
            'minimum_order_amount' => 5,
            'max_elements' => 8,
            'max_texts' => 3,
            'max_images' => 1,
            'one_line_text' => false,
            'design_elements_locked' => false,
            'designProductionMethod' => DesignProductionMethodFactory::createOne([
                'identifier' => 'test-design-production-method-old',
            ]),
            'designArea' => $designArea,
        ])->object();
        $designArea->addDesignAreaDesignProductionMethod($designAreaDesignProductionMethod);

        $existing->addDesignArea($designArea);

        $creatorView = CreatorViewFactory::createOne([
            'identifier' => 'test-creator-view-old',
            'directory' => 'test-directory-old',
            'sequence_number' => 1,
            'item' => $existing,
        ])->object();

        $creatorViewText = CreatorViewTextFactory::createOne([
            'title' => 'test-creator-view-title-old',
            'language' => $languageDE,
            'creatorView' => $creatorView,
        ])->object();
        $creatorView->addCreatorViewText($creatorViewText);

        $existing->addCreatorView($creatorView);

        $existing->enableAutoRefresh();
        $existing->save();

        $this->existingItem = $existing;
    }

    private function createDefaultConfigurationToDuplicate(Proxy $item)
    {
        $this->defaultConfiguration = ConfigurationFactory::createOne([
            'item' => $item->object(),
            'name' => 'default_configuration',
            'partsListHash' => 'f75ae11b8be57a6b4accf98e4232ed7224e8f9e2',
            'selectedOptions' => json_encode('{"mainboard":[{"identifier":"2110900","amount":1,"userNote":""}],"cpu":[{"identifier":"2114742","amount":1,"userNote":""}]}'),
            'configurationType' => ConfigurationtypeFactory::findBy(['identifier' => ConfigurationtypeRepository::DEFAULT_OPTIONS])[0]->object(),
        ])->object();
    }

    private function createExistingDefaultConfiguration(Proxy $item)
    {
        $this->existingDefaultConfiguration = ConfigurationFactory::createOne([
            'item' => $item->object(),
            'name' => 'existing_default_configuration',
            'partsListHash' => 'd882f6538335b9714af36698a42a03698d3223bb',
            'selectedOptions' => json_encode('{"raid_controller":[{"identifier":"raidcontroller_onboard","amount":1,"userNote":""}],"network_card":[{"identifier":"2115066","amount":1,"userNote":""}]}'),
            'configurationType' => ConfigurationtypeFactory::findBy(['identifier' => ConfigurationtypeRepository::DEFAULT_OPTIONS])[0],
        ])->object();
    }
}
