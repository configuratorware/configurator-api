<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Phake;
use Phake_IMock;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DefaultConfigurationGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\Default3dViewFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\AdminAreaItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ChildItemSave;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemSave;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item as ItemStructure;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

class ItemSaveTest extends TestCase
{
    use EntityTestTrait;

    /**
     * @var ChildItemSave|Phake_IMock
     * @Mock ChildItemSave
     */
    private $childItemSave;

    /**
     * @var CreatorViewDelete
     *
     * @Mock
     */
    private $creatorViewDelete;

    /**
     * @var Default3dViewFactory|Phake_IMock
     * @Mock Default3dViewFactory
     */
    private $default3dViewFactory;

    /**
     * @var DesignViewDelete
     *
     * @Mock
     */
    private $designViewDelete;

    /**
     * @var ItemRepository|Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemStatusRepository|Phake_IMock
     * @Mock ItemStatusRepository
     */
    private $itemStatusRepository;

    /**
     * @var ItemEntityFromStructureConverter|Phake_IMock
     * @Mock ItemEntityFromStructureConverter
     */
    private $itemEntityFromStructureConverter;

    /**
     * @var AdminAreaItemStructureFromEntityConverter|Phake_IMock
     * @Mock AdminAreaItemStructureFromEntityConverter
     */
    private $adminAreaItemStructureFromEntityConverter;

    /**
     * @var DefaultConfigurationGenerator|Phake_IMock
     * @Mock DefaultConfigurationGenerator
     */
    private $defaultConfigurationGenerator;

    /**
     * @var ItemSave
     */
    private $itemSave;

    public function setUp(): void
    {
        Phake::initAnnotations($this);

        $this->itemSave = new ItemSave(
            $this->childItemSave,
            $this->creatorViewDelete,
            $this->default3dViewFactory,
            $this->designViewDelete,
            $this->itemRepository,
            $this->itemStatusRepository,
            $this->itemEntityFromStructureConverter,
            $this->adminAreaItemStructureFromEntityConverter,
            $this->defaultConfigurationGenerator
        );

        Phake::when($this->itemStatusRepository)->findOneByIdentifier->thenReturn($this->createItemStatus('not_available'));
    }

    /**
     * @dataProvider providerShouldDeactivateItem
     *
     * @param string $expectedStatusIdentifier
     * @param ItemStatus|null $givenStatus
     */
    public function testShouldDeactivateItem(string $expectedStatusIdentifier, ?ItemStatus $givenStatus): void
    {
        Phake::when($this->itemEntityFromStructureConverter)->convertOne->thenReturn($this->createItem($givenStatus));
        Phake::when($this->itemRepository)->save->thenReturn($this->createItem($givenStatus));

        $this->itemSave->save(new ItemStructure());

        Phake::verify($this->itemRepository)->save(Phake::capture($item));

        self::assertEquals($expectedStatusIdentifier, $item->getItemStatus()->getIdentifier());
    }

    public function providerShouldDeactivateItem(): array
    {
        return [
            [
                'not_available',
                null,
            ],
            [
                'available',
                $this->createItemStatus('available'),
            ],
        ];
    }

    /**
     * @dataProvider providerShouldGenerateDesigner3DViews
     *
     * @param VisualizationMode|null $previousVisualizationMode
     * @param VisualizationMode $newVisualizationMode
     */
    public function testShouldGenerate3DDesignView(?VisualizationMode $previousVisualizationMode, VisualizationMode $newVisualizationMode): void
    {
        Phake::when($this->itemRepository)->findOneBy->thenReturn($this->createItem(null, $previousVisualizationMode));
        Phake::when($this->itemEntityFromStructureConverter)->convertOne->thenReturn($this->createItem(null, $newVisualizationMode));
        Phake::when($this->itemRepository)->save->thenReturn($this->createItem(null, $newVisualizationMode));

        $this->itemSave->save($this->createItemStructure(1));

        Phake::verify($this->default3dViewFactory)->create3dDefaultDesignView;
    }

    public function providerShouldGenerateDesigner3DViews(): array
    {
        return [
            'empty_to_3dVariant' => [
                null,
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
            ],
            '2dLayer_to_3dVariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
            ],
            '2dVariant_to_3dVariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
            ],
            '2dVariantOverlay_to_3dVariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
            ],
            '3dScene_to_3dVariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_SCENE),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
            ],
        ];
    }

    /**
     * @dataProvider providerShouldGenerateCreator3DViews
     *
     * @param VisualizationMode|null $previousVisualizationMode
     * @param VisualizationMode $newVisualizationMode
     */
    public function testShouldGenerate3CreatorView(?VisualizationMode $previousVisualizationMode, VisualizationMode $newVisualizationMode): void
    {
        Phake::when($this->itemRepository)->findOneBy->thenReturn($this->createItem(null, $previousVisualizationMode));
        Phake::when($this->itemEntityFromStructureConverter)->convertOne->thenReturn($this->createItem(null, $newVisualizationMode));
        Phake::when($this->itemRepository)->save->thenReturn($this->createItem(null, $newVisualizationMode));

        $this->itemSave->save($this->createItemStructure(1));

        Phake::verify($this->default3dViewFactory)->create3dDefaultCreatorView;
    }

    public function providerShouldGenerateCreator3DViews(): array
    {
        return [
            'empty_to_3dScene' => [
                null,
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_SCENE),
            ],
            '2dLayer_to_3dScene' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_SCENE),
            ],
            '2dVariant_to_3dScene' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_SCENE),
            ],
            '3dVariant_to_3dScene' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_SCENE),
            ],
        ];
    }

    /**
     * @dataProvider providerShouldNotGenerateDesignViews
     *
     * @param VisualizationMode $previousVisualizationMode
     * @param VisualizationMode $newVisualizationMode
     * @param string $existingViewType
     * @param array $views
     */
    public function testShouldNotGenerateDesignViews(VisualizationMode $previousVisualizationMode, VisualizationMode $newVisualizationMode, string $existingViewType, array $views): void
    {
        $item = $this->createItem(null, $newVisualizationMode, $existingViewType, $views);
        // sanity check
        self::assertCount(count($views), $item->getDesignView());

        Phake::when($this->itemRepository)->findOneBy->thenReturn($this->createItem(null, $previousVisualizationMode));
        Phake::when($this->itemEntityFromStructureConverter)->convertOne->thenReturn($item);
        Phake::when($this->itemRepository)->save->thenReturn($this->createItem(null, $newVisualizationMode));

        $this->itemSave->save($this->createItemStructure(1));

        Phake::verifyNoInteraction($this->default3dViewFactory);
        self::assertCount(count($views), $item->getDesignView());
    }

    public function providerShouldNotGenerateDesignViews(): array
    {
        return [
            'no_change_2dVariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
                'designView',
                ['1_2d', '2_2d'],
            ],
            'no_change_3dVariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_VARIANT),
                'designView',
                ['3d'],
            ],
            'no_change_2dVariantOverlay' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY),
                'designView',
                ['1_2d', '2_2d'],
            ],
            'no_change_2dVariant_2dVariantOverlay' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY),
                'designView',
                ['1_2d', '2_2d'],
            ],
            'no_change_2dVariantOverlay_2dVariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
                'designView',
                ['1_2d', '2_2d'],
            ],
        ];
    }

    /**
     * @dataProvider providerShouldTransferDesignToCreatorViews
     *
     * @param VisualizationMode $previousVisualizationMode
     * @param VisualizationMode $newVisualizationMode
     **/
    public function testShouldTransferDesignToCreatorViews(VisualizationMode $previousVisualizationMode, VisualizationMode $newVisualizationMode): void
    {
        $previousItem = $this->createItem(null, $previousVisualizationMode);
        $item = $this->createItem(null, $newVisualizationMode, 'designView', ['1_2d', '2_2d']);
        // sanity check
        self::assertCount(2, $item->getDesignView());
        self::assertCount(0, $item->getCreatorView());

        Phake::when($this->itemRepository)->findOneBy->thenReturn($previousItem);
        Phake::when($this->itemEntityFromStructureConverter)->convertOne->thenReturn($item);
        Phake::when($this->itemRepository)->save->thenReturn($this->createItem(null, $newVisualizationMode));

        $this->itemSave->save($this->createItemStructure(1));

        Phake::verify($this->designViewDelete, Phake::times(2))->delete;
        self::assertCount(0, $item->getDesignView());
        self::assertCount(2, $item->getCreatorView());
    }

    public function providerShouldTransferDesignToCreatorViews(): array
    {
        return [
            '2dvariant_2dLayer' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
            ],
            '2dvariantoverlay_2dLayer' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
            ],
        ];
    }

    /**
     * @dataProvider providerShouldNotGenerateCreatorViews
     *
     * @param VisualizationMode $previousVisualizationMode
     * @param VisualizationMode $newVisualizationMode
     * @param string $existingViewType
     * @param array $views
     */
    public function testShouldNotGenerateCreatorViews(VisualizationMode $previousVisualizationMode, VisualizationMode $newVisualizationMode, string $existingViewType, array $views): void
    {
        $item = $this->createItem(null, $newVisualizationMode, $existingViewType, $views);
        // sanity check
        self::assertCount(count($views), $item->getCreatorView());

        Phake::when($this->itemRepository)->findOneBy->thenReturn($this->createItem(null, $previousVisualizationMode));
        Phake::when($this->itemEntityFromStructureConverter)->convertOne->thenReturn($item);
        Phake::when($this->itemRepository)->save->thenReturn($this->createItem(null, $newVisualizationMode));

        $this->itemSave->save($this->createItemStructure(1));

        Phake::verifyNoInteraction($this->default3dViewFactory);
        self::assertCount(count($views), $item->getCreatorView());
    }

    public function providerShouldNotGenerateCreatorViews(): array
    {
        return [
            'no_change_2dLayer' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
                'creatorView',
                ['1_2d', '2_2d'],
            ],
            'no_change_3dScene' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_SCENE),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_3D_SCENE),
                'creatorView',
                ['3d'],
            ],
        ];
    }

    /**
     * @dataProvider providerShouldTransferCreatorToDesignViews
     *
     * @param VisualizationMode $previousVisualizationMode
     * @param VisualizationMode $newVisualizationMode
     **/
    public function testShouldTransferCreatorToDesignViews(VisualizationMode $previousVisualizationMode, VisualizationMode $newVisualizationMode): void
    {
        $previousItem = $this->createItem(null, $previousVisualizationMode);
        $item = $this->createItem(null, $newVisualizationMode, 'creatorView', ['1_2d', '2_2d']);
        // sanity check
        self::assertCount(0, $item->getDesignView());
        self::assertCount(2, $item->getCreatorView());

        Phake::when($this->itemRepository)->findOneBy->thenReturn($previousItem);
        Phake::when($this->itemEntityFromStructureConverter)->convertOne->thenReturn($item);
        Phake::when($this->itemRepository)->save->thenReturn($this->createItem(null, $newVisualizationMode));

        $this->itemSave->save($this->createItemStructure(1));

        Phake::verify($this->creatorViewDelete, Phake::times(2))->delete;
        self::assertCount(2, $item->getDesignView());
        self::assertCount(2, $item->getCreatorView());
        foreach ($item->getCreatorView() as $creatorView) {
            EntityListener::DATE_DELETED_DEFAULT !== $creatorView->getDateDeleted()->format('Y-m-d H:i:s');
        }
    }

    public function providerShouldTransferCreatorToDesignViews(): array
    {
        return [
            '2dLayer_2dvariant' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT),
            ],
            '2dLayer_2dvariantoverlay' => [
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_LAYER),
                $this->createVisualizationMode(VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY),
            ],
        ];
    }

    private function createItemStructure(int $id): ItemStructure
    {
        $item = new ItemStructure();
        $item->id = $id;

        return $item;
    }

    private function createItem(?ItemStatus $status = null, ?VisualizationMode $visualization = null, string $viewType = '', array $views = []): Item
    {
        $item = new Item();
        $item->setItemStatus($status);
        $item->setVisualizationMode($visualization);

        foreach ($views as $key => $viewIdentifier) {
            if ('designView' === $viewType) {
                $designView = (new DesignView())->setIdentifier($viewIdentifier)->setItem($item);
                $this->setPrivateId($designView, $key);
                $item->addDesignView($designView);
            }

            if ('creatorView' === $viewType) {
                $creatorView = CreatorView::from(null, $viewIdentifier, $item, '01');
                $this->setPrivateId($creatorView, $key);
                $item->addCreatorView($creatorView);
            }
        }

        return $item;
    }

    private function createItemStatus(string $identifier): ItemStatus
    {
        $itemStatus = new ItemStatus();
        $itemStatus->setIdentifier($identifier);

        return $itemStatus;
    }

    private function createVisualizationMode(string $identifier): VisualizationMode
    {
        $visualizationMode = new VisualizationMode();
        $visualizationMode->setIdentifier($identifier);

        return $visualizationMode;
    }
}
