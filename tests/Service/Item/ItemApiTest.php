<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Phake;
use Phake_IMock;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\InspirationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\AdminAreaItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemListStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemDuplicate;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemImage;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemSave;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemStatusUpdate;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemDuplicate as ItemDuplicateStructure;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Validator\TraceableValidator;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ItemApiTest extends KernelTestCase
{
    /**
     * @var ItemRepository|Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemDuplicate|Phake_IMock
     * @Mock ItemDuplicate
     */
    private $itemDuplicate;

    private ItemApi $itemApi;

    public function setUp(): void
    {
        Phake::initAnnotations($this);

        $kernel = self::bootKernel();
        $validator = $kernel->getContainer()->get(TraceableValidator::class);
        $this->itemApi = new ItemApi(
            Phake::mock(AdminAreaItemStructureFromEntityConverter::class),
            Phake::mock(FrontendItemListStructureFromEntityConverter::class),
            Phake::mock(EventDispatcherInterface::class),
            Phake::mock(InspirationRepository::class),
            Phake::mock(ItemDelete::class),
            Phake::mock(ItemImage::class),
            $this->itemRepository,
            Phake::mock(ItemSave::class),
            Phake::mock(ItemStatusUpdate::class),
            $validator,
            $this->itemDuplicate
        );
    }

    public function testCannotDuplicateNotFoundItem(): void
    {
        $this->expectException(NotFoundException::class);

        Phake::when($this->itemRepository)->find(9)->thenReturn(null);

        $itemDuplicate = new ItemDuplicateStructure();

        $this->itemApi->duplicate(9, $itemDuplicate);
    }

    public function testCannotDuplicateWithInvalidDuplicateItem(): void
    {
        $this->expectException(ValidationException::class);

        $item = new Item();
        Phake::when($this->itemRepository)->find(9)->thenReturn($item);

        $itemDuplicate = new ItemDuplicateStructure();

        $this->itemApi->duplicate(9, $itemDuplicate);
    }

    public function testShouldDuplicateItem(): void
    {
        $item = new Item();
        Phake::when($this->itemRepository)->find(9)->thenReturn($item);

        $itemDuplicate = new ItemDuplicateStructure();
        $itemDuplicate->identifier = 'duplicateIdentifier';

        $this->itemApi->duplicate(9, $itemDuplicate);

        Phake::verify($this->itemDuplicate)->duplicate($item, $itemDuplicate);
    }
}
