<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Cache\StructureCache;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue\AttributeValueStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue as AttributeValueStructure;

class AttributeValueStructureFromEntityConverterTest extends TestCase
{
    /**
     * @var AttributeValueStructureFromEntityConverter
     */
    private $converter;

    /**
     * @var  StructureFromEntityConverter|\Phake_IMock
     * @Mock StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var  StructureCache|\Phake_IMock
     * @Mock StructureCache
     */
    private $structureCache;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->converter = new AttributeValueStructureFromEntityConverter($this->structureFromEntityConverter, $this->structureCache);
    }

    public function testShouldConvertStructureWithoutCache()
    {
        $attributeValue = new Attributevalue();
        $expected = new AttributeValueStructure();

        \Phake::when($this->structureCache)->findStructureForEntity($attributeValue)->thenReturn(null)->thenReturn($expected);
        \Phake::when($this->structureFromEntityConverter)->convertOne($attributeValue, AttributeValueStructure::class)->thenReturn($expected);

        $actual = $this->converter->convertOne($attributeValue);

        self::assertSame($expected, $actual);

        \Phake::verify($this->structureFromEntityConverter)->convertOne($attributeValue, AttributeValueStructure::class);
    }

    public function testShouldConvertStructureWithCache()
    {
        $attribute = new Attributevalue();
        $expected = new AttributeValueStructure();

        \Phake::when($this->structureCache)->findStructureForEntity($attribute)->thenReturn($expected);

        $actual = $this->converter->convertOne($attribute);

        self::assertSame($expected, $actual);

        \Phake::verifyNoInteraction($this->structureFromEntityConverter);
    }
}
