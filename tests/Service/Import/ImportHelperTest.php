<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ImportHelperTest extends ApiTestCase
{
    /**
     * @var LicenseFileValidator|\Phake_IMock
     * @Mock LicenseFileValidator
     */
    protected $licenseFileValidator;

    /**
     * @var ImportHelper
     */
    private $importHelper;

    protected function setUp(): void
    {
        parent::setUp();
        \Phake::initAnnotations($this);
        $this->importHelper = new ImportHelper(
            $this->em->getConnection(),
            new ImportLogger(new NullLogger()),
            $this->licenseFileValidator
        );
    }

    /**
     * @param string|null $license
     * @param array $itemData
     * @param int $expectedVisualizationModeId
     *
     * @dataProvider providerShouldFindVisualizationMode
     */
    public function testShouldFindVisualizationMode(?string $license, array $itemData, int $expectedVisualizationModeId)
    {
        \Phake::when($this->licenseFileValidator)->getValidProduct()->thenReturn($license);

        $actualVisualizationModeId = $this->importHelper->getVisualizationModeId($itemData);
        self::assertEquals($expectedVisualizationModeId, $actualVisualizationModeId);
    }

    public function providerShouldFindVisualizationMode()
    {
        return [
            ['creator+designer', ['itemIdentifier' => 'non_existing_item'], 1],
            ['creator', ['itemIdentifier' => 'non_existing_item'], 1],
            ['designer', ['itemIdentifier' => 'non_existing_item'], 2],
            [null, ['itemIdentifier' => 'sheep'], 3],
            [null, ['itemIdentifier' => 'non_existing_item', 'visualizationModeIdentifier' => '3dScene'], 3],
            [null, ['itemIdentifier' => 'non_existing_item', 'visualizationModeIdentifier' => '3dVariant'], 4],
        ];
    }
}
