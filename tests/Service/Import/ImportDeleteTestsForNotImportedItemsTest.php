<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Doctrine\ORM\EntityManager;
use org\bovigo\vfs\vfsStream;
use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ColorChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\AttributeHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DefaultConfigurationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DeleteHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DesignHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\GroupHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\MediaHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\OptionClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\OptionHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportWorker;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\Paths;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @BackupDatabase
 */
class ImportDeleteTestsForNotImportedItemsTest extends ApiTestCase
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ImportOptions
     */
    private $importOptions;

    /**
     * @var ConfigurationCodeGenerator|\Phake_IMock
     * @Mock ConfigurationCodeGenerator
     */
    private $configurationCodeGenerator;

    /**
     * @var ImportWorker
     */
    private $importWorker;

    /**
     * @var LicenseFileValidator|\Phake_IMock
     * @Mock LicenseFileValidator
     */
    protected $licenseFileValidator;

    public function setUp(): void
    {
        parent::setUp();
        self::bootKernel();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/setUp_for_ImportDeleteTestsForNotImportedItems.sql'));

        \Phake::initAnnotations($this);
        \Phake::when($this->configurationCodeGenerator)->generateCode->thenReturn('TESTCODE1');

        $this->executeSql(file_get_contents(__DIR__ . '/_data/setUp.sql'));

        $this->fileSystem = new Filesystem();
        $this->rootPath = vfsStream::setup(
            'root',
            null,
            ['images' => ['item' => ['image' => [], 'thumb' => []]]]
        )->url();

        $this->setDefaultImportOptions();

        $connection = $this->em->getConnection();
        $testLogger = new ImportLogger(new NullLogger());
        $importHelper = new ImportHelper($this->em->getConnection(), $testLogger, $this->licenseFileValidator);

        $paths = new Paths('/images/components', '', 'images/item/view', '/images/item/view_thumb', '', 'images/item/image', 'images/item/thumb', '', '', $this->rootPath, 'images/options', '');

        $attributeHandler = new AttributeHandler($connection, $importHelper);
        $mediaHandler = new MediaHandler($paths, $paths, $this->fileSystem, $paths, $testLogger, $paths);
        $optionHandler = new OptionHandler($connection, $importHelper, $attributeHandler, $mediaHandler, $testLogger);

        $this->importWorker = new ImportWorker(
            $connection,
            $attributeHandler,
            new OptionClassificationHandler($connection, $importHelper, $attributeHandler, $optionHandler, $mediaHandler, $testLogger),
            new DefaultConfigurationHandler($connection, $importHelper, $this->configurationCodeGenerator),
            new DeleteHandler($connection, $importHelper),
            new DesignHandler($connection, $importHelper, $testLogger, new ColorChecker()),
            $testLogger,
            $mediaHandler,
            $importHelper,
            new ItemHandler($connection, new GroupHandler($connection, $importHelper), $importHelper, $testLogger),
            new ImportValidator($testLogger),
            new ItemClassificationHandler($connection, $importHelper)
        );
    }

    /**
     * Set default import options.
     */
    private function setDefaultImportOptions(): void
    {
        $this->importOptions = new ImportOptions();
        $this->importOptions->deleteNonExistingItems = false;
        $this->importOptions->deleteNonExistingAttributeRelations = false;
        $this->importOptions->deleteNonExistingItemPrices = false;
        $this->importOptions->deleteNonExistingDesignAreas = false;
        $this->importOptions->deleteNonExistingDesignAreaVisualData = false;
        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethodPrices = false;
        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethod = false;
        $this->importOptions->deleteNonExistingDesignViewThumbnails = false;
        $this->importOptions->moveProcessedFile = false;
    }

    public function testDeleteNonExistingItems(): void
    {
        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertNotNull($item);

        $this->importOptions->deleteNonExistingItems = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithoutValidItem());

        $this->em->flush();
        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertNull($item);
    }

    public function testDeleteNonExistingAttributeRelations(): void
    {
        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertNotEmpty($item->getItemAttribute());

        $this->importOptions->deleteNonExistingAttributeRelations = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithoutRelations());

        $this->em->flush();
        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertEmpty($item->getItemAttribute());
    }

    public function testDeleteNonExistingItemPrices(): void
    {
        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertNotEmpty($item->getItemprice());

        $this->importOptions->deleteNonExistingItemPrices = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithoutRelations());

        $this->em->flush();
        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertEmpty($item->getItemprice());
    }

    public function testDeleteNonExistingDesignAreas(): void
    {
        $designView = $this->em->getRepository(DesignView::class)->findOneByIdentifier('demo_view');
        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        $designAreaText = $this->em->getRepository(DesignAreaText::class)->findOneByTitle('demo_area_text');
        self::assertNotEmpty($designView->getDesignViewDesignArea());
        self::assertNotNull($designArea);
        self::assertNotNull($designAreaText);

        $this->importOptions->deleteNonExistingDesignAreas = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithoutRelations());

        $this->em->flush();
        $this->em->clear();

        $designView = $this->em->getRepository(DesignView::class)->findOneByIdentifier('demo_view');
        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        $designAreaText = $this->em->getRepository(DesignAreaText::class)->findOneByTitle('demo_area_text');
        self::assertEmpty($designView->getDesignViewDesignArea());
        self::assertNull($designArea);
        self::assertNull($designAreaText);
    }

    public function testDeleteNonExistingDesignAreaVisualData(): void
    {
        $designView = $this->em->getRepository(DesignView::class)->findOneByIdentifier('demo_view');
        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        self::assertNotEmpty($designView->getDesignViewDesignArea());
        self::assertNotNull($designArea);

        $this->importOptions->deleteNonExistingDesignAreaVisualData = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithoutRelations());

        $this->em->flush();
        $this->em->clear();

        $designView = $this->em->getRepository(DesignView::class)->findOneByIdentifier('demo_view');
        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        self::assertEmpty($designView->getDesignViewDesignArea());
        self::assertNotNull($designArea);
    }

    public function testDeleteNonExistingDesignAreaDesignProductionMethodPrices(): void
    {
        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        self::assertNotNull($designArea);
        self::assertNotEmpty($designArea->getDesignAreaDesignProductionMethod());
        self::assertNotEmpty(
            $designArea->getDesignAreaDesignProductionMethod()->first()->getDesignAreaDesignProductionMethodPrice()
        );

        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethodPrices = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithDesignAreaOnly());

        $this->em->flush();
        $this->em->clear();

        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        self::assertNotNull($designArea);
        self::assertNotEmpty($designArea->getDesignAreaDesignProductionMethod());
        self::assertEmpty(
            $designArea->getDesignAreaDesignProductionMethod()->first()->getDesignAreaDesignProductionMethodPrice()
        );
    }

    public function testDeleteNonExistingDesignAreaDesignProductionMethod(): void
    {
        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        self::assertNotNull($designArea);
        self::assertNotEmpty($designArea->getDesignAreaDesignProductionMethod());

        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethod = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithDesignAreaOnly());

        $this->em->flush();
        $this->em->clear();

        $designArea = $this->em->getRepository(DesignArea::class)->findOneByIdentifier('demo_area');
        self::assertNotNull($designArea);
        self::assertEmpty($designArea->getDesignAreaDesignProductionMethod());
    }

    public function testDeleteNonExistingComponentRelations(): void
    {
        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertNotEmpty($item->getItemOptionclassification());

        $this->importOptions->deleteNonExistingComponentRelations = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithoutRelations());

        $this->em->flush();
        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertEmpty($item->getItemOptionclassification());
    }

    public function testDeleteNonExistingOptions(): void
    {
        $this->importOptions->deleteNonExistingComponentRelations = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithComponentWithoutOption());

        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertFalse($item->getItemOptionclassification()->first()->getItemOptionclassificationOption()->filter(
            static function (ItemOptionclassificationOption $itemOption) {
                'demo_option' === $itemOption->getOption()->getIdentifier();
            }
        )->first());
    }

    public function testDeleteNonExistingItemClassificationRelations(): void
    {
        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertNotEmpty($item->getItemItemclassification());

        $this->importOptions->deleteNonExistingItemClassificationRelations = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithoutRelations());

        $this->em->flush();
        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertEmpty($item->getItemItemclassification());
    }

    public function testDeleteNonExistingOptionDeltaPrices(): void
    {
        $this->importOptions->deleteNonExistingComponentRelations = true;
        $this->importWorker->import($this->importOptions, $this->getImportDataWithValidItemWithComponentAndOptionWithoutDeltaPrices());

        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('demo_item');
        self::assertFalse($item->getItemOptionclassification()->first()->getItemOptionclassificationOption()->first()->getItemOptionclassificationOptionDeltaprice()->first());
    }

    /**
     * Not existing item.
     *
     * @return array|array[]
     */
    private function getImportDataWithoutValidItem(): array
    {
        return [
            [
                'itemIdentifier' => 'fake_identifier',
                'children' => [],
                'texts' => [],
                'images' => [],
                'minimumOrderAmount' => 1,
                'accumulateAmounts' => false,
                'prices' => [],
                'attributes' => [],
                'itemStatuses' => [],
                'designAreas' => [],
                'components' => [],
            ],
        ];
    }

    /**
     * Existing item but without any relations.
     *
     * @return array|array[]
     */
    private function getImportDataWithValidItemWithoutRelations(): array
    {
        return [
            [
                'itemIdentifier' => 'demo_item',
                'children' => [],
                'texts' => [],
                'images' => [],
                'minimumOrderAmount' => 1,
                'accumulateAmounts' => false,
                'prices' => [],
                'attributes' => [],
                'itemStatuses' => [],
                'designAreas' => [],
                'components' => [],
                'itemClassifications' => [],
            ],
        ];
    }

    /**
     * Existing item with valid but empty design area.
     *
     * @return array|array[]
     */
    private function getImportDataWithValidItemWithDesignAreaOnly(): array
    {
        return [
            [
                'itemIdentifier' => 'demo_item',
                'children' => [],
                'texts' => [],
                'images' => [],
                'minimumOrderAmount' => 1,
                'accumulateAmounts' => false,
                'prices' => [],
                'attributes' => [],
                'itemStatuses' => [],
                'designAreas' => [
                    [
                        'designAreaIdentifier' => 'demo_area',
                        'texts' => [],
                        'visualData' => [],
                        'heightMillimeter' => 1,
                        'widthMillimeter' => 1,
                        'customData' => [],
                        'designProductionMethods' => [],
                    ],
                ],
                'components' => [],
            ],
        ];
    }

    /**
     * @return array
     */
    private function getImportDataWithValidItemWithComponentWithoutOption(): array
    {
        return [
            [
                'itemIdentifier' => 'demo_item',
                'children' => [],
                'texts' => [],
                'images' => [],
                'minimumOrderAmount' => 1,
                'accumulateAmounts' => false,
                'prices' => [],
                'attributes' => [],
                'itemStatuses' => [],
                'designAreas' => [],
                'components' => [
                    [
                        'componentIdentifier' => 'demo_component',
                        'options' => [
                            [
                                'optionIdentifier' => 'new_option',
                                'deltaPrices' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function getImportDataWithValidItemWithComponentAndOptionWithoutDeltaPrices(): array
    {
        return [
            [
                'itemIdentifier' => 'demo_item',
                'children' => [],
                'texts' => [],
                'images' => [],
                'minimumOrderAmount' => 1,
                'accumulateAmounts' => false,
                'prices' => [],
                'attributes' => [],
                'itemStatuses' => [],
                'designAreas' => [],
                'components' => [
                    [
                        'componentIdentifier' => 'demo_component',
                        'options' => [
                            [
                                'optionIdentifier' => 'demo_option',
                                'deltaPrices' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
