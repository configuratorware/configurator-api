<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @BackupDatabase
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class ImportWorkerUpdateTest extends ImportWorkerBaseTest
{
    public function setUp(): void
    {
        parent::setUp();
        $this->importWorker->import($this->importOptions, $this->getTestData('import_data_update_test'));
    }

    public function testItemImportUpdate()
    {
        $this->assertUpdated();
        $this->assertAreaUpdated();
        $this->assertColorUpdated();
        $this->assertDesignAreaUpdated();
    }

    private function assertUpdated()
    {
        $importedItem = $this->em->getRepository(Item::class)
            ->findOneBy(['identifier' => 't_shirt'], ['id' => 'desc']);
        /* @var $importedItem Item */

        // new item inserted
        self::assertNotNull($importedItem);
        self::assertEquals(33, $importedItem->getMinimumOrderAmount());
        self::assertEquals(false, $importedItem->getAccumulateAmounts());

        // already present item remains
        $fixtureItem = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 'sheep']);
        self::assertNotNull($fixtureItem);
    }

    private function assertAreaUpdated()
    {
        /* @var DesignArea $designArea */
        $designArea = $this->em->getRepository(DesignArea::class)
            ->findOneBy(['identifier' => 'front_top'], ['id' => 'desc']);
        self::assertNotNull($designArea);
        self::assertEquals(40, $designArea->getHeight());
        self::assertEquals(400, $designArea->getWidth());
        self::assertEquals([(object)['test' => 'testvalue']], $designArea->getCustomData());

        /* @var DesignAreaDesignProductionMethod $designAreaProductionMethod */
        $designAreaProductionMethod = $designArea->getDesignAreaDesignProductionMethod()->last();
        self::assertEquals(40, $designAreaProductionMethod->getHeight());
        self::assertEquals(420, $designAreaProductionMethod->getWidth());
        self::assertJsonStringEqualsJsonString(
            '{"engravingBackgroundColors":[{"itemIdentifier":"t_shirt_red_s","colorHex": "#223344"}]}',
            json_encode($designAreaProductionMethod->getAdditionalData())
        );

        /* @var DesignAreaDesignProductionMethodPrice $price */
        $price = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findOneBy(['price' => 4.99]);
        self::assertNotNull($price);
        self::assertEquals(3, $price->getAmountFrom());
        self::assertEquals(2, $price->getColorAmountFrom());
    }

    /**
     * Test if new default color is present after update, use hex value.
     */
    public function assertColorUpdated()
    {
        /* @var $designAreaProdMethod DesignAreaDesignProductionMethod */
        $designAreaProdMethod = $this->em->getRepository(DesignAreaDesignProductionMethod::class)
            ->findOneBy([], ['date_updated' => 'desc']);
        self::assertNotNull($designAreaProdMethod);
        self::assertJsonStringEqualsJsonString(
            '{"t_shirt_red_s":{"default":"black"}}',
            json_encode($designAreaProdMethod->getDefaultColors())
        );
        self::assertEquals(200, $designAreaProdMethod->getMinimumOrderAmount());
    }

    public function assertDesignAreaUpdated()
    {
        /* @var $entities DesignViewDesignArea[] */
        $entities = $this->em->getRepository(DesignViewDesignArea::class)->findAll();
        self::assertNotNull($entities);
        self::assertCount(2, $entities);

        /* @var $defaultEntities DesignViewDesignArea[] */
        $defaultEntities = array_values(
            array_filter(
                $entities,
                static function (DesignViewDesignArea $entity) {
                    return $entity->getIsDefault();
                }
            )
        );

        self::assertCount(1, $defaultEntities);
        self::assertSame('side', $defaultEntities[0]->getDesignView()->getIdentifier());
    }
}
