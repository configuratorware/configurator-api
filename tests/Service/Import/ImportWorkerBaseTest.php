<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ColorChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\AttributeHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DefaultConfigurationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DeleteHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DesignHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\GroupHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\MediaHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\OptionClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\OptionHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\Paths;
use Symfony\Component\Dotenv\Exception\PathException;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
abstract class ImportWorkerBaseTest extends ApiTestCase
{
    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @var ImportWorker
     */
    protected $importWorker;

    /**
     * @var ImportOptions
     */
    protected $importOptions;

    /**
     * @var vfsStreamDirectory
     */
    protected $rootPath;

    /**
     * @var array
     */
    protected $testData = [];

    /**
     * @var string
     */
    protected $viewThumbnailPath;

    /**
     * @var string
     */
    protected $itemDetailImagePath;

    /**
     * @var string
     */
    protected $itemThumbnailImagePath;

    /**
     * @var string
     */
    protected $optionThumbnailImagePath;

    /**
     * @var string
     */
    protected $componentThumbnailImagePath;

    /**
     * @var ConfigurationCodeGenerator|\Phake_IMock
     * @Mock ConfigurationCodeGenerator
     */
    protected $configurationCodeGenerator;

    /**
     * @var LicenseFileValidator|\Phake_IMock
     * @Mock LicenseFileValidator
     */
    protected $licenseFileValidator;

    /**
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function setUp(): void
    {
        parent::setUp();
        \Phake::initAnnotations($this);

        \Phake::when($this->configurationCodeGenerator)->generateCode->thenReturn('TESTCODE1');

        $this->executeSql(file_get_contents(__DIR__ . '/_data/setUp.sql'));

        $this->viewThumbnailPath = static::$kernel->getContainer()->getParameter('design_view_thumbnail_path');
        $this->itemDetailImagePath = static::$kernel->getContainer()->getParameter('item_detail_image_path');
        $this->itemThumbnailImagePath = static::$kernel->getContainer()->getParameter('item_thumbnail_path');
        $this->componentThumbnailImagePath = static::$kernel->getContainer()->getParameter('component_thumbnail_path');
        $this->optionThumbnailImagePath = static::$kernel->getContainer()->getParameter('option_thumbnail_path');

        $this->fileSystem = new Filesystem();
        $this->rootPath = vfsStream::setup(
            'root',
            null,
            ['images' => ['item' => ['image' => [], 'thumb' => []]]]
        )->url();

        $this->setImportOptions();

        $connection = $this->em->getConnection();
        $testLogger = new ImportLogger(new NullLogger());
        $importHelper = new ImportHelper($connection, $testLogger, $this->licenseFileValidator);

        $paths = new Paths('/images/components', '', 'images/item/view', '/images/item/view_thumb', '', 'images/item/image', 'images/item/thumb', '', '', $this->rootPath, 'images/options', '');

        $attributeHandler = new AttributeHandler($connection, $importHelper);
        $mediaHandler = new MediaHandler($paths, $paths, $this->fileSystem, $paths, $testLogger, $paths);
        $optionHandler = new OptionHandler($connection, $importHelper, $attributeHandler, $mediaHandler, $testLogger);

        $this->importWorker = new ImportWorker(
            $connection,
            $attributeHandler,
            new OptionClassificationHandler($connection, $importHelper, $attributeHandler, $optionHandler, $mediaHandler, $testLogger),
            new DefaultConfigurationHandler($connection, $importHelper, $this->configurationCodeGenerator),
            new DeleteHandler($connection, $importHelper),
            new DesignHandler($connection, $importHelper, $testLogger, new ColorChecker()),
            $testLogger,
            $mediaHandler,
            $importHelper,
            new ItemHandler($connection, new GroupHandler($connection, $importHelper), $importHelper, $testLogger),
            new ImportValidator($testLogger),
            new ItemClassificationHandler($connection, $importHelper)
        );

        $this->importWorker->import($this->importOptions, $this->getTestData('import_data_insert_test'));
        sleep(1);
    }

    /**
     * Set default import options.
     */
    protected function setImportOptions(): void
    {
        $this->importOptions = new ImportOptions();
        $this->importOptions->deleteNonExistingItems = false;
        $this->importOptions->deleteNonExistingAttributeRelations = false;
        $this->importOptions->deleteNonExistingItemPrices = false;
        $this->importOptions->deleteNonExistingDesignAreas = false;
        $this->importOptions->deleteNonExistingDesignAreaVisualData = false;
        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethodPrices = false;
        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethod = false;
        $this->importOptions->deleteNonExistingDesignViewThumbnails = false;
        $this->importOptions->moveProcessedFile = false;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function addTestData(): void
    {
        if (!isset($this->testData['channel']) &&
            !$this->em->getRepository(Channel::class)
                ->findOneBy(['identifier' => 'de_eur'])
        ) {
            $channel = new Channel();
            $channel->setIdentifier('de_eur');
            $this->em->persist($channel);

            $this->testData['channel'] = $channel;
        }

        if (!isset($this->testData['prodmethod']) &&
            !$this->em->getRepository(DesignProductionMethod::class)
                ->findOneBy(['identifier' => 'offset_print'])
        ) {
            $prodMethod = new DesignProductionMethod();
            $prodMethod->setIdentifier('offset_print');
            $this->em->persist($prodMethod);

            $this->testData['prodmethod'] = $prodMethod;
        }

        $this->em->flush();
    }

    /**
     * @param string $fileName
     *
     * @return array
     */
    protected function getTestData(string $fileName): array
    {
        $filePath = __DIR__ . '/_data/testdata/' . $fileName . '.json';
        if (!file_exists($filePath)) {
            throw new PathException('Test data file "' . $filePath . '" is not present');
        }

        return json_decode(file_get_contents($filePath), true);
    }
}
