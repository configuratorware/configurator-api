<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImageUrl;

class ImageUrlTest extends TestCase
{
    public function testShouldDecodeUrl()
    {
        $url = 'https://redhotmagma.de/ä ö';
        $actual = ImageUrl::fromString(urlencode($url))->imageUrl;

        self::assertEquals($url, $actual);
    }

    public function testShouldReturnNullForEmptyUrl()
    {
        $actual = ImageUrl::fromString('')->imageUrl;

        self::assertNull($actual);
    }

    public function testShouldTrimUrl()
    {
        $url = 'https://redhotmagma.de/';
        $actual = ImageUrl::fromString(' ' . $url . ' ')->imageUrl;

        self::assertEquals($url, $actual);
    }
}
