INSERT INTO `design_production_method` (`id`, `identifier`, `options`, `custom_data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`)
VALUES (100, 'offset_print', '{"minFontSize": 5}', NULL, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL);

INSERT INTO `design_production_method_color_palette` (`id`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `color_palette_id`, `design_production_method_id`)
VALUES (100, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 100);

INSERT INTO `designer_production_calculation_type` (`id`, `identifier`, `color_amount_dependent`, `item_amount_dependent`, `selectable_by_user`, `declare_separately`, `bulk_name_dependent`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_production_method_id`)
VALUES (1, 'productionPrice', 1, 1, 0, 0, 0, '2001-01-01 00:00:00', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 100);

INSERT IGNORE INTO channel (id, identifier, settings, is_default, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, currency_id)
VALUES (100, 'de_eur', '{}', 1, NOW(), NULL, '0001-01-01 00:00:00', 0, 0, 0, 1),
    (101, 'chf', '{}', 1, NOW(), NULL, '0001-01-01 00:00:00', 0, 0, 0, 1);
