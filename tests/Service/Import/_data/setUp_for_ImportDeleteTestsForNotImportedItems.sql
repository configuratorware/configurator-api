INSERT INTO item (id, identifier, minimum_order_amount, accumulate_amounts, date_created, date_updated, date_deleted)
VALUES (101, 'demo_item', 1, 1, '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00');

INSERT INTO attribute (id, identifier, attributedatatype, date_created, date_updated, date_deleted, user_created_id)
VALUES (101, 'demo_attribute', 'string', '2020-07-31 14:34:21', NULL, '0001-01-01 00:00:00', 0);

INSERT INTO attributevalue (id, value, date_created, date_updated, date_deleted, user_created_id)
VALUES (101, 'demo_value', '2020-07-31 14:34:21', NULL, '0001-01-01 00:00:00', 0);

INSERT INTO item_attribute (id, date_created, date_updated, date_deleted, user_deleted_id, attribute_id,
                            attributevalue_id, item_id)
VALUES (101, '2020-07-31 14:34:21', NULL, '0001-01-01 00:00:00', 0, 101, 101, 101);

INSERT INTO design_view (id, identifier, sequence_number, date_created, date_updated, date_deleted, item_id)
VALUES (101, 'demo_view', 1, '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00', 101);

INSERT INTO design_area (id, identifier, sequence_number, date_created, date_updated, date_deleted, item_id)
VALUES (101, 'demo_area', 1, '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00', 101);

INSERT INTO design_area_text (id, title, date_created, date_updated, date_deleted, design_area_id, language_id)
VALUES (101, 'demo_area_text', '2020-07-30 14:09:01', '2020-07-30 14:09:01', '0001-01-01 00:00:00', 101, 1);

INSERT INTO design_view_design_area (id, date_created, date_updated, date_deleted, design_view_id, design_area_id)
VALUES (1, '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00', 101, 101);

INSERT INTO design_production_method (id, identifier, date_created, date_updated, date_deleted)
VALUES (101, 'demo_printing', '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00');

INSERT INTO design_area_design_production_method (id, date_created, date_updated, date_deleted, design_area_id, design_production_method_id)
VALUES (101, '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00', 101, 101);

INSERT INTO design_area_design_production_method_price (id, amount_from, color_amount_from, price, price_net, date_created, date_updated, date_deleted, channel_id, designer_production_calculation_type_id, design_area_design_production_method_id)
VALUES (101, 1, 1, 1, 1, '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00', 1, 1, 101);

INSERT INTO itemprice (id, price, price_net, amountfrom, date_created, date_updated, date_deleted, channel_id, item_id)
VALUES (101, 2, 3, 1, '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00', 1, 101);

INSERT INTO optionclassification (id, identifier, sequencenumber, date_created, date_updated, date_deleted)
VALUES (101, 'demo_component', 1, '2020-07-30 14:09:46', NULL, '0001-01-01 00:00:00');

INSERT INTO `option` (id, identifier, date_created, date_updated, date_deleted)
VALUES (101, 'demo_option', '2020-07-30 14:09:46', NULL, '0001-01-01 00:00:00');

INSERT INTO item_optionclassification (id, date_created, date_updated, date_deleted, item_id, optionclassification_id)
VALUES (101, '2020-07-30 14:09:46', NULL, '0001-01-01 00:00:00', 101, 101);

INSERT INTO item_optionclassification_option (id, date_created, date_updated, date_deleted, item_optionclassification_id, option_id)
VALUES (101, '2020-07-30 14:09:46', NULL, '0001-01-01 00:00:00', 101, 101);

INSERT INTO `itemoptionclassificationoptiondeltaprice` (`id`, `channel_id`, `item_optionclassification_option_id`, `price`, `price_net`, `date_created`, `date_deleted`)
VALUES (101, 1, 101, 3.90000, 3.28000, '2020-11-18 15:03:19', '0001-01-01 00:00:00');

INSERT INTO itemclassification (id, identifier, date_created, date_updated, date_deleted)
VALUES (101, 'demo_classification', '2020-07-30 14:09:01', NULL, '0001-01-01 00:00:00');

INSERT INTO item_itemclassification (id, date_created, date_updated, date_deleted, item_id, itemclassification_id)
VALUES (101, '2020-07-30 14:09:46', NULL, '0001-01-01 00:00:00', 101, 101);
