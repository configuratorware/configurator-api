<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLegacyTransformer;

class ImportLegacyTransformerTest extends TestCase
{
    /**
     * @var ItemStatusRepository|\Phake_IMock
     * @Mock ItemStatusRepository
     */
    private $itemStatusRepository;

    private $transformer;

    public function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->transformer = new ImportLegacyTransformer($this->itemStatusRepository);
    }

    /**
     * @dataProvider providerShouldTransform
     *
     * @param array $expected
     * @param array $given
     */
    public function testShouldTransform(array $expected, array $given): void
    {
        \Phake::when($this->itemStatusRepository)->findBy(['item_available' => true])->thenReturn($this->getAvailableItemStatuses());

        self::assertEquals($expected, $this->transformer->transform($given));
    }

    public function providerShouldTransform(): array
    {
        return [
            [
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'configurationMode' => 'designer',
                        'itemStatusIdentifier' => 'is_available',
                        'images' => [
                            'detailImage' => 'http://...',
                            'thumbnail' => 'http://...',
                        ],
                        'children' => [
                            [
                                'itemIdentifier' => 't_shirt',
                                'images' => [
                                    'detailImage' => 'http://...',
                                    'thumbnail' => 'http://...',
                                ],
                            ],
                        ],
                        'texts' => ['needs_to_stay'],
                        'attributes' => ['needs_to_stay'],
                        'prices' => ['needs_to_stay'],
                        'designAreas' => ['needs_to_stay'],
                        'components' => [['needs_to_stay']],
                    ],
                ],
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'itemStatuses' => [
                            [
                                'configurationModeIdentifier' => 'creator',
                                'available' => false,
                            ],
                            [
                                'configurationModeIdentifier' => 'designer',
                                'available' => true,
                            ],
                        ],
                        'images' => [
                            'detailImage' => 'http://...',
                            'thumbnail' => 'http://...',
                        ],
                        'children' => [
                            [
                                'itemIdentifier' => 't_shirt',
                                'images' => [
                                    'detailImage' => 'http://...',
                                    'thumbnail' => 'http://...',
                                ],
                            ],
                        ],
                        'texts' => ['needs_to_stay'],
                        'attributes' => ['needs_to_stay'],
                        'prices' => ['needs_to_stay'],
                        'designAreas' => ['needs_to_stay'],
                        'components' => [['needs_to_stay']],
                    ],
                ],
            ],
            [
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'configurationMode' => 'creator+designer',
                        'itemStatusIdentifier' => 'is_available',
                        'images' => [
                            'detailImage' => 'http://...',
                            'thumbnail' => 'http://...',
                        ],
                        'children' => [
                            [
                                'itemIdentifier' => 't_shirt',
                                'images' => [
                                    'detailImage' => 'http://...',
                                    'thumbnail' => 'http://...',
                                ],
                            ],
                        ],
                        'components' => [
                            [
                                'componentIdentifier' => 'hood',
                                'sequencenumber' => 1,
                                'options' => [
                                    [
                                        'optionIdentifier' => 'red_riding',
                                        'prices' => [
                                            [
                                                'channelIdentifier' => '_default',
                                                'bulkPrices' => [
                                                    [
                                                        'itemAmountFrom' => 1,
                                                        'price' => 0,
                                                        'priceNet' => 0,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'itemStatuses' => [
                            [
                                'configurationModeIdentifier' => 'creator',
                                'available' => true,
                            ],
                            [
                                'configurationModeIdentifier' => 'designer',
                                'available' => true,
                            ],
                        ],
                        'images' => [
                            'itemImage' => 'http://...',
                            'thumbnail' => 'http://...',
                        ],
                        'children' => [
                            [
                                'itemIdentifier' => 't_shirt',
                                'images' => [
                                    'itemImage' => 'http://...',
                                    'thumbnail' => 'http://...',
                                ],
                            ],
                        ],
                        'components' => [
                            [
                                'componentIdentifier' => 'hood',
                                'sequencenumber' => 1,
                                'options' => [
                                    [
                                        'optionIdentifier' => 'red_riding',
                                        'prices' => [
                                            [
                                                'channelIdentifier' => '_default',
                                                'prices' => [
                                                    [
                                                        'itemAmountFrom' => 1,
                                                        'price' => 0,
                                                        'priceNet' => 0,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            [
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'configurationMode' => 'creator',
                        'itemStatusIdentifier' => 'is_available',
                    ],
                ],
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'itemStatuses' => [
                            [
                                'configurationModeIdentifier' => 'creator',
                                'available' => true,
                            ],
                            [
                                'configurationModeIdentifier' => 'designer',
                                'available' => false,
                            ],
                        ],
                    ],
                ],
            ],
            [
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'configurationMode' => null,
                        'itemStatusIdentifier' => 'not_available',
                    ],
                ],
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'itemStatuses' => [
                            [
                                'configurationModeIdentifier' => 'creator',
                                'available' => false,
                            ],
                            [
                                'configurationModeIdentifier' => 'designer',
                                'available' => false,
                            ],
                        ],
                    ],
                ],
            ],
            [
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'configurationMode' => 'creator+designer',
                        'itemStatusIdentifier' => 'is_available',
                    ],
                ],
                [
                    [
                        'itemIdentifier' => 't_shirt',
                        'configurationMode' => 'creator+designer',
                        'itemStatusIdentifier' => 'is_available',
                        'itemStatuses' => [
                            [
                                'configurationModeIdentifier' => 'creator',
                                'available' => false,
                            ],
                            [
                                'configurationModeIdentifier' => 'designer',
                                'available' => false,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return ItemStatus[]
     */
    private function getAvailableItemStatuses(): array
    {
        $itemStatus = new ItemStatus();
        $itemStatus->setIdentifier('is_available');
        $itemStatus->setItemAvailable(true);

        return [$itemStatus];
    }
}
