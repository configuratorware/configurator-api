<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ImportFileReaderValidTest extends ApiTestCase
{
    /**
     * @var ImporterFileReader
     */
    private $importReader;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $testBaseDir;

    /**
     * @var ImportOptions
     */
    private $importOptions;

    /**
     * Test files with date modification time to keep them in order for directory read tests.
     *
     * @var array
     */
    private static $validTestFiles = [
        'validTestData.json' => '2020-01-05',
        'validTestData.XML' => '2020-01-03',
    ];

    protected function setUp(): void
    {
        parent::setUp();

        $container = static::$kernel->getContainer();
        /* @var $container ContainerInterface */
        $this->importReader = $container->get('Redhotmagma\ConfiguratorApiBundle\Service\Import\ImporterFileReader');
        $this->fileSystem = new Filesystem();

        $this->testBaseDir = __DIR__ . '/../../../var/test_files_valid';

        foreach (static::$validTestFiles as $contentName => $timeStr) {
            $this->fileSystem->copy(
                __DIR__ . '/_data/' . $contentName,
                $this->testBaseDir . '/' . $contentName,
                true
            );
            $this->fileSystem->touch(
                $this->testBaseDir . '/' . $contentName,
                (new \DateTime($timeStr))->getTimestamp()
            );
        }

        $this->importOptions = new ImportOptions();
    }

    protected function tearDown(): void
    {
        foreach (static::$validTestFiles as $contentName => $timeSubtract) {
            $this->fileSystem->remove($this->testBaseDir . '/' . $contentName);
        }
        $this->fileSystem->remove($this->testBaseDir);
        parent::tearDown();
    }

    public function testReadValidJsonFileFromUrl()
    {
        $this->markTestSkipped('loading from url has to be fixed for jenkins context');
        $content = $this->importReader->readSource('http://127.0.0.1/validTestData.json', $this->importOptions);
        self::assertTrue($this->importReader->isJson($content));
        self::assertNotEmpty($content);
    }

    public function testReadValidXmlFileFromUrl()
    {
        $this->markTestSkipped('loading from url has to be fixed for jenkins context');
        $content = $this->importReader->readSource('http://127.0.0.1/validTestData.XML', $this->importOptions);
        self::assertTrue($this->importReader->isXML($content));
        self::assertNotEmpty($content);
    }

    public function testReadValidJsonFileFromPath()
    {
        $content = $this->importReader->readSource($this->testBaseDir . '/validTestData.json', $this->importOptions);
        self::assertTrue($this->importReader->isJson($content));
        self::assertNotEmpty($content);
    }

    public function testReadValidXmlFileFromPath()
    {
        $content = $this->importReader->readSource($this->testBaseDir . '/validTestData.XML', $this->importOptions);
        self::assertTrue($this->importReader->isXML($content));
        self::assertNotEmpty($content);
    }

    public function testReadValidFilesFromDirectory()
    {
        // read XML
        $content = $this->importReader->readSource($this->testBaseDir, $this->importOptions);
        self::assertTrue(
            $this->importReader->isXML($content),
            'Content is not a valid XML:' . chr(10) . chr(13) . $content
        );
        self::assertNotEmpty($content);

        // read json
        $content = $this->importReader->readSource($this->testBaseDir, $this->importOptions);
        self::assertTrue(
            $this->importReader->isJson($content),
            'Content is not a valid JSON:' . chr(10) . chr(13) . $content
        );
        self::assertNotEmpty($content);
    }
}
