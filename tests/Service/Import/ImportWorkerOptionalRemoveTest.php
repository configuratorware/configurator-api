<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext;
use Symfony\Component\Finder\Finder;

/**
 * @BackupDatabase
 * @SuppressWarnings(PHPMD.ShortVariable)
 */
class ImportWorkerOptionalRemoveTest extends ImportWorkerBaseTest
{
    public function testItemPriceRemove()
    {
        $price1 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 16.7]);
        self::assertNotNull($price1);

        $price2 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 6.7]);
        self::assertNotNull($price2);

        $this->importOptions->deleteNonExistingItemPrices = true;
        $this->importWorker->import($this->importOptions, $this->getTestData('import_data_remove_itemPrice'));

        $price1 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 16.7]);
        self::assertNull($price1);

        $price2 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 6.7]);
        self::assertNull($price2);
    }

    public function testComponentRelationRemoval()
    {
        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);

        $itemOptionClassification = $item->getItemOptionclassification();
        self::assertNotEmpty($itemOptionClassification);
        $itemOptionClassification = $itemOptionClassification->first();
        self::assertNotEmpty($itemOptionClassification->getItemOptionclassificationOption());
        $itemOptionClassificationOption = $itemOptionClassification->getItemOptionclassificationOption()->first();

        $this->importOptions->deleteNonExistingComponentRelations = true;
        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_remove_componentRelations')
        );

        $this->em->flush();
        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        $itemOptionClassification = $item->getItemOptionclassification();
        self::assertEmpty($itemOptionClassification);
        $itemOptionClassification = $itemOptionClassification->first();
        self::assertFalse($itemOptionClassification);
        $itemOptionClassificationOption = $this->em->getRepository(ItemOptionclassificationOption::class)
            ->find($itemOptionClassificationOption->getId());
        self::assertEmpty($itemOptionClassificationOption);
    }

    public function testComponentRelationRemovalOptionOnly()
    {
        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);

        $itemOptionClassification = $item->getItemOptionclassification();
        self::assertNotEmpty($itemOptionClassification);
        $itemOptionClassification = $itemOptionClassification->first();
        self::assertNotEmpty($itemOptionClassification->getItemOptionclassificationOption());
        $itemOptionClassificationOption = $itemOptionClassification->getItemOptionclassificationOption()->first();
        self::assertSame('red', $itemOptionClassificationOption->getOption()->getIdentifier());

        $this->importOptions->deleteNonExistingComponentRelations = true;
        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_remove_ComponentRelations_option_only')
        );

        $this->em->flush();
        $this->em->clear();

        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        $itemOptionClassification = $item->getItemOptionclassification();
        self::assertNotEmpty($itemOptionClassification);
        $itemOptionClassification = $itemOptionClassification->first();
        self::assertNotNull($itemOptionClassification);
        $itemOptionClassificationOption = $this->em->getRepository(ItemOptionclassificationOption::class)
            ->find($itemOptionClassificationOption->getId());
        self::assertEmpty($itemOptionClassificationOption);
    }

    public function testAttributeRelationRemove()
    {
        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        $attribute = $this->em->getRepository(Attribute::class)->findOneBy(['identifier' => 'tag']);
        $attributeValue = $this->em->getRepository(Attributevalue::class)->findOneBy(['value' => 'summer']);

        $itemAttribute = $this->em->getRepository(ItemAttribute::class)->findOneBy([
            'item' => $item,
            'attribute' => $attribute,
            'attributevalue' => $attributeValue,
        ]);
        self::assertNotNull($itemAttribute);

        $this->importOptions->deleteNonExistingAttributeRelations = true;
        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_remove_itemAttributeRelation')
        );

        $itemAttribute = $this->em->getRepository(ItemAttribute::class)->findOneBy([
            'item' => $item,
            'attribute' => $attribute,
            'attributevalue' => $attributeValue,
        ]);
        self::assertNull($itemAttribute);
    }

    public function testDesignAreaRemove()
    {
        $designArea = $this->em->getRepository(DesignArea::class)->findOneBy(['identifier' => 'front_top']);
        self::assertNotNull($designArea);

        $designAreaDesignProductionMethod = $this->em
            ->getRepository(DesignAreaDesignProductionMethod::class)
            ->findOneBy(['width' => 320, 'height' => 30]);
        self::assertNotNull($designAreaDesignProductionMethod);

        $designAreaDesignProductionMethodPrice = $this->em
            ->getRepository(DesignAreaDesignProductionMethodPrice::class)
            ->findOneBy(['price' => 1.99]);
        self::assertNotNull($designAreaDesignProductionMethodPrice);

        $designAreaText = $this->em->getRepository(DesignAreaText::class)->findOneBy(['title' => 'front top']);
        self::assertNotNull($designAreaText);

        $designView = $this->em->getRepository(DesignView::class)->findOneBy(['identifier' => 'front']);
        self::assertNotNull($designView);

        $designViewDesignArea = $this->em->getRepository(DesignViewDesignArea::class)
            ->findOneBy(['designView' => $designView, 'designArea' => $designArea]);
        self::assertNotNull($designViewDesignArea);

        $designView = $this->em->getRepository(DesignView::class)->findOneBy(['identifier' => 'front']);
        self::assertNotNull($designView);

        $this->importOptions->deleteNonExistingDesignAreas = true;
        $this->importWorker->import($this->importOptions, $this->getTestData('import_data_remove_designArea'));

        $designArea = $this->em->getRepository(DesignArea::class)->findOneBy(['identifier' => 'front_top']);
        self::assertNull($designArea);

        $designAreaDesignProductionMethod = $this->em->getRepository(DesignAreaDesignProductionMethod::class)
            ->findOneBy(['width' => 320, 'height' => 30]);
        self::assertNull($designAreaDesignProductionMethod);

        $designAreaDesignProductionMethodPrice = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)
            ->findOneBy(['price' => 1.99]);
        self::assertNull($designAreaDesignProductionMethodPrice);

        $designAreaText = $this->em->getRepository(DesignAreaText::class)->findOneBy(['title' => 'front top']);
        self::assertNull($designAreaText);

        $designViewDesignArea = $this->em->getRepository(DesignViewDesignArea::class)
            ->findOneBy(['designView' => $designView, 'designArea' => $designArea]);
        self::assertNull($designViewDesignArea);
    }

    public function testDesignAreaVisualDataRemove()
    {
        $designArea = $this->em->getRepository(DesignArea::class)->findOneBy(['identifier' => 'front_top']);
        self::assertNotNull($designArea);

        $designView = $this->em->getRepository(DesignView::class)->findOneBy(['identifier' => 'front']);
        self::assertNotNull($designView);

        $designViewText = $this->em->getRepository(DesignViewText::class)->findOneBy(['title' => 'Front view']);
        self::assertNotNull($designViewText);

        $designViewDesignArea = $this->em->getRepository(DesignViewDesignArea::class)
            ->findOneBy(['designView' => $designView, 'designArea' => $designArea]);
        self::assertNotNull($designViewDesignArea);

        $this->importOptions->deleteNonExistingDesignAreaVisualData = true;
        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_remove_designAreaVisualData')
        );

        $designViewDesignArea = $this->em->getRepository(DesignViewDesignArea::class)
            ->findOneBy(['designView' => $designView, 'designArea' => $designArea]);
        self::assertNull($designViewDesignArea);
    }

    public function testDesignProductionMethodPriceRemove()
    {
        $price = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)
            ->findOneBy(['price' => 1.99]);
        self::assertNotNull($price);

        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethodPrices = true;
        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_remove_designProductionMethodPrice')
        );

        $price = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)
            ->findOneBy(['price' => 1.99]);
        self::assertNull($price);
    }

    public function testDesignAreaDesignProductionMethodRemoval()
    {
        $designProductionMethod = $this->em->getRepository(DesignProductionMethod::class)
            ->findOneBy(['identifier' => 'offset_print']);
        self::assertNotNull($designProductionMethod);

        $designArea = $this->em->getRepository(DesignArea::class)->findOneBy(['identifier' => 'front_top']);
        self::assertNotNull($designArea);

        $designAreaDesignProductionMethod = $this->em->getRepository(DesignAreaDesignProductionMethod::class)
            ->findOneBy([
                'designProductionMethod' => $designProductionMethod,
                'designArea' => $designArea,
            ]);
        self::assertNotNull($designAreaDesignProductionMethod);

        $designAreaDesignProductionMethodPrice = $this->em
            ->getRepository(DesignAreaDesignProductionMethodPrice::class)
            ->findOneBy(['price' => 1.99]);
        self::assertNotNull($designAreaDesignProductionMethodPrice);

        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethod = true;
        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_remove_designAreaDesignProductionMethod')
        );

        $designAreaDesignProductionMethod = $this->em->getRepository(DesignAreaDesignProductionMethod::class)
            ->findOneBy([
                'designProductionMethod' => $designProductionMethod,
                'designArea' => $designArea,
            ]);
        self::assertNull($designAreaDesignProductionMethod);

        $designAreaDesignProductionMethodPrice = $this->em
            ->getRepository(DesignAreaDesignProductionMethodPrice::class)
            ->findOneBy(['price' => 1.99]);
        self::assertNull($designAreaDesignProductionMethodPrice);
    }

    public function testItemRemove()
    {
        /* tests before import */
        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        self::assertNotNull($item);

        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        $attribute = $this->em->getRepository(Attribute::class)->findOneBy(['identifier' => 'tag']);
        $attributeValue = $this->em->getRepository(Attributevalue::class)->findOneBy(['value' => 'summer']);
        $itemAttribute = $this->em->getRepository(ItemAttribute::class)->findOneBy([
            'item' => $item,
            'attribute' => $attribute,
            'attributevalue' => $attributeValue,
        ]);
        self::assertNotNull($itemAttribute);

        $price1 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 16.7]);
        self::assertNotNull($price1);

        $price2 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 6.7]);
        self::assertNotNull($price2);

        $itemText = $this->em->getRepository(Itemtext::class)->findOneBy(['title' => 'T-shirt']);
        self::assertNotNull($itemText);

        $itemGroup = $this->em->getRepository(Itemgroup::class)->findOneBy(['identifier' => 'color']);
        $childItem = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt_red_s']);
        $itemGroupEntry = $this->em->getRepository(ItemItemgroupentry::class)->findOneBy([
            'item' => $childItem,
            'itemgroup' => $itemGroup,
        ]);
        self::assertNotNull($childItem);
        self::assertNotNull($itemGroupEntry);

        /* importing and removing */
        $this->importOptions->deleteNonExistingItems = true;
        $this->importWorker->import($this->importOptions, []);

        /* tests after import & remove */
        $itemAttribute = $this->em->getRepository(ItemAttribute::class)->findOneBy([
            'item' => $item,
            'attribute' => $attribute,
            'attributevalue' => $attributeValue,
        ]);
        self::assertNull($itemAttribute);

        $price1 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 16.7]);
        self::assertNull($price1);

        $price2 = $this->em->getRepository(Itemprice::class)->findOneBy(['price_net' => 6.7]);
        self::assertNull($price2);

        $itemText = $this->em->getRepository(Itemtext::class)->findOneBy(['title' => 'T-shirt']);
        self::assertNull($itemText);

        $itemGroupEntry = $this->em->getRepository(ItemItemgroupentry::class)->findOneBy([
            'item' => $childItem,
            'itemgroup' => $itemGroup,
        ]);
        self::assertNull($itemGroupEntry);

        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        self::assertNull($item);

        $childItem = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt_red_s']);
        self::assertNull($childItem);
    }

    public function testDesignViewThumbnailRemove()
    {
        $existingFiles = [];
        $finder = new Finder();
        $finder->files()->in($this->rootPath . '/' . $this->viewThumbnailPath);
        foreach ($finder as $file) {
            $existingFiles[] = $file->getPath() . '/' . $file->getFilename();
        }

        self::assertNotEmpty($existingFiles, 'Test requires imported files before.');

        /* importing and removing */
        $this->importOptions->deleteNonExistingDesignViewThumbnails = true;
        $this->importWorker->import(
            $this->importOptions,
            [$this->getTestData('import_data_remove_designView_thumbnail')]
        );

        foreach ($existingFiles as $file) {
            self::assertFileNotExists($file);
        }
    }

    /**
     * @dataProvider providerRemoveImages
     */
    public function testImageRemove(string $dir, string $importOption)
    {
        $existingFiles = [];
        $finder = new Finder();
        $finder->files()->in($this->rootPath . '/' . $this->$dir);
        foreach ($finder as $file) {
            $existingFiles[] = $file->getPath() . '/' . $file->getFilename();
        }

        self::assertNotEmpty($existingFiles, 'Test requires imported files before.');

        /* importing and removing */
        $this->importOptions->$importOption = true;
        $this->importWorker->import(
            $this->importOptions,
            [$this->getTestData('import_data_removed_images')]
        );

        foreach ($existingFiles as $file) {
            self::assertFileNotExists($file);
        }
    }

    /**
     * @dataProvider providerRemoveImages
     */
    public function testImageNotRemoved(string $dir, string $importOption)
    {
        $existingFiles = [];
        $finder = new Finder();
        $finder->files()->in($this->rootPath . '/' . $this->$dir);
        foreach ($finder as $file) {
            $existingFiles[] = $file->getPath() . '/' . $file->getFilename();
        }

        self::assertNotEmpty($existingFiles, 'Test requires imported files before.');

        /* importing and removing */
        $this->importOptions->$importOption = true;
        $this->importWorker->import(
            $this->importOptions,
            [$this->getTestData('import_data_insert_test')]
        );

        foreach ($existingFiles as $file) {
            self::assertFileExists($file);
        }
    }

    /**
     * @return array
     */
    public function providerRemoveImages(): array
    {
        return [
            [
                'itemDetailImagePath',
                'deleteNonExistingItemImages',
            ],
            [
                'itemThumbnailImagePath',
                'deleteNonExistingItemImages',
            ],
            [
                'componentThumbnailImagePath',
                'deleteNonExistingComponentThumbnails',
            ],
            [
                'optionThumbnailImagePath',
                'deleteNonExistingOptionThumbnails',
            ],
        ];
    }
}
