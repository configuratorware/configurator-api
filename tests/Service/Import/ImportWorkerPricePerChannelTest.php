<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;

/**
 * @BackupDatabase
 */
class ImportWorkerPricePerChannelTest extends ImportWorkerBaseTest
{
    /**
     * @var ChannelRepository
     */
    private $channelRepo;

    /**
     * @var Channel
     */
    private $eurChannel;

    /**
     * @var Channel
     */
    private $chfChannel;

    public function setUp(): void
    {
        parent::setUp();

        $this->channelRepo = $this->em->getRepository(Channel::class);
        $this->eurChannel = $this->channelRepo->findBy(['identifier' => 'de_eur']);
        $this->chfChannel = $this->channelRepo->findBy(['identifier' => 'chf']);

        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_insert_test_pricePerChannel')
        );
    }

    public function testChannelPriceImported()
    {
        // check chf prices
        $chfItemPrices = $this->em->getRepository(Itemprice::class)->findBy(['channel' => $this->chfChannel]);
        self::assertNotEmpty($chfItemPrices);
        $chfMethodPrices = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findBy(['channel' => $this->chfChannel]);
        self::assertNotEmpty($chfMethodPrices);

        // check eur prices
        $eurItemPrices = $this->em->getRepository(Itemprice::class)->findBy(['channel' => $this->eurChannel]);
        self::assertNotEmpty($eurItemPrices);
        $eurMethodPrices = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findBy(['channel' => $this->eurChannel]);
        self::assertNotEmpty($eurMethodPrices);
    }

    public function testOnlyChannelPricesShouldBeRemoved()
    {
        $this->importOptions->priceChannel = 'chf';
        $this->importOptions->deleteNonExistingItemPrices = true;
        $this->importOptions->deleteNonExistingDesignAreaDesignProductionMethodPrices = true;

        /*
         * this hack is required because deletes are based on time, datetime granularity in MySQL is 1 sec, so in the
         * test everything happens in the same second and an object can't be updated and deleted in the same second
         */
        sleep(1);

        $this->importWorker->import($this->importOptions, $this->getTestData('import_data_remove_itemPrice'));

        // check chf prices deleted
        $chfItemPrices = $this->em->getRepository(Itemprice::class)->findBy(['channel' => $this->chfChannel]);
        self::assertEmpty($chfItemPrices);

        $chfMethodPrices = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findBy(['channel' => $this->chfChannel]);
        self::assertEmpty($chfMethodPrices);

        // check eur prices not deleted
        $eurItemPrices = $this->em->getRepository(Itemprice::class)->findBy(['channel' => $this->eurChannel]);
        self::assertNotEmpty($eurItemPrices);

        $eurMethodPrices = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findBy(['channel' => $this->eurChannel]);
        self::assertNotEmpty($eurMethodPrices);
    }

    public function testOnlyChannelPricesShouldBeUpdated()
    {
        $this->importOptions->priceChannel = 'chf';
        $this->importWorker->import(
            $this->importOptions,
            $this->getTestData('import_data_update_test_pricePerChannel')
        );

        // check chf prices updated
        $chfItemPrices = $this->em->getRepository(Itemprice::class)->findBy(['channel' => $this->chfChannel]);

        self::assertNotEmpty($chfItemPrices);
        foreach ($chfItemPrices as $price) {
            self::assertEquals(55, $price->getPrice());
            self::assertEquals(55, $price->getPriceNet());
        }

        $chfMethodPrices = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findBy(['channel' => $this->chfChannel]);
        self::assertNotEmpty($chfMethodPrices);
        foreach ($chfMethodPrices as $price) {
            self::assertEquals(55, $price->getPrice());
            self::assertEquals(55, $price->getPriceNet());
        }

        // check eur prices not updated
        $eurItemPrices = $this->em->getRepository(Itemprice::class)->findBy(['channel' => $this->eurChannel]);
        self::assertNotEmpty($eurItemPrices);
        foreach ($eurItemPrices as $price) {
            self::assertNotEquals(55, $price->getPrice());
            self::assertNotEquals(55, $price->getPriceNet());
        }

        $eurMethodPrices = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findBy(['channel' => $this->eurChannel]);
        self::assertNotEmpty($eurMethodPrices);
        foreach ($eurMethodPrices as $price) {
            self::assertNotEquals(55, $price->getPrice());
            self::assertNotEquals(55, $price->getPriceNet());
        }
    }
}
