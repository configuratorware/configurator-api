<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import\Handler;

use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\GroupHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

final class ItemHandlerTest extends ApiTestCase
{
    /**
     * @var ItemHandler
     */
    private $itemHandler;

    /**
     * @var ItemRepository;
     */
    private $itemRepository;

    /**
     * @var LicenseFileValidator|\Phake_IMock
     * @Mock LicenseFileValidator
     */
    protected $licenseFileValidator;

    public function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->itemRepository = $this->em->getRepository(Item::class);

        $connection = $this->em->getConnection();
        $logger = new ImportLogger(new NullLogger());
        $importHelper = new ImportHelper($connection, $logger, $this->licenseFileValidator);
        $groupHandler = new GroupHandler($connection, $importHelper);

        $this->itemHandler = new ItemHandler($connection, $groupHandler, $importHelper, $logger);
    }

    /**
     * @param array $itemImportFormat
     *
     * @dataProvider providerShouldInitItem
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function testShouldInitItem(array $itemImportFormat): void
    {
        $this->itemHandler->processItem($itemImportFormat);

        /** @var Item $itemEntity */
        $itemEntity = $this->itemRepository->findOneByIdentifier('testShouldProcessItem');

        self::assertEquals(1, $itemEntity->getMinimumOrderAmount());
        self::assertEquals(true, $itemEntity->getAccumulateAmounts());
        self::assertEquals('2dLayer', $itemEntity->getVisualizationMode()->getIdentifier());
        self::assertEquals('creator', $itemEntity->getConfigurationMode());
        self::assertEquals('available', $itemEntity->getItemStatus()->getIdentifier());
        self::assertEquals(true, $itemEntity->getOverwriteComponentOrder());
    }

    public function providerShouldInitItem(): \Generator
    {
        yield [
            [
                'itemIdentifier' => 'testShouldProcessItem',
                'minimumOrderAmount' => 1,
                'accumulateAmounts' => true,
                'visualizationModeIdentifier' => '2dLayer',
                'configurationMode' => 'creator',
                'itemStatusIdentifier' => 'available',
                'overwriteComponentOrder' => true,
            ],
        ];
    }

    /**
     * @param array $itemImportFormat
     *
     * @dataProvider providerShouldUpdateItemMinimum
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function testShouldUpdateItemMinimum(array $itemImportFormat): void
    {
        $this->executeSql('
INSERT INTO `item` (`identifier`, `minimum_order_amount`, `accumulate_amounts`, `date_created`, `date_deleted`, `visualization_mode_id`, `item_status_id`, `configuration_mode`, `overwrite_component_order`)
VALUES ("testShouldProcessItem", NULL, 0, NOW(), "0001-01-01 00:00:00", 1, 2, "creator", 1);
');

        $this->itemHandler->processItem($itemImportFormat);

        /** @var Item $itemEntity */
        $itemEntity = $this->itemRepository->findOneByIdentifier('testShouldProcessItem');

        self::assertEquals(1, $itemEntity->getMinimumOrderAmount());
        self::assertTrue($itemEntity->getAccumulateAmounts());
        self::assertEquals('2dLayer', $itemEntity->getVisualizationMode()->getIdentifier());
        self::assertEquals('creator', $itemEntity->getConfigurationMode());
        self::assertEquals('available', $itemEntity->getItemStatus()->getIdentifier());
        self::assertTrue($itemEntity->getOverwriteComponentOrder());
    }

    public function providerShouldUpdateItemMinimum(): \Generator
    {
        yield [
            [
                'itemIdentifier' => 'testShouldProcessItem',
                'minimumOrderAmount' => 1,
                'accumulateAmounts' => true,
            ],
        ];
    }

    public function testShouldFindItem(): void
    {
        $this->executeSql('
            INSERT INTO `item` (`identifier`, `minimum_order_amount`, `accumulate_amounts`, `date_created`, `date_deleted`, `visualization_mode_id`, `item_status_id`, `configuration_mode`, `overwrite_component_order`)
            VALUES ("testShouldFindItem", NULL, 0, NOW(), "0001-01-01 00:00:00", 1, 2, "creator", 1);
        ');

        $item = $this->itemHandler->findItem('testShouldFindItem');

        self::assertSame('testShouldFindItem', $item['identifier']);
        self::assertSame('creator', $item['configuration_mode']);
    }
}
