INSERT INTO `option_classification_attribute` (`optionclassification_id`, `attribute_id`, `attributevalue_id`,
                                               `date_created`, `date_updated`, `date_deleted`)
VALUES (5050, 100, 100, '2022-01-14 08:38:22', '2022-01-14 08:48:25', '0001-01-01 00:00:00'),
       (5050, 101, 100, '2022-01-14 08:38:22', '2022-01-14 08:48:25', '0001-01-01 00:00:00');

INSERT INTO `optionclassification` (`id`, `parent_id`, `identifier`, `sequencenumber`, `date_created`, `date_updated`,
                                    `date_deleted`)
VALUES (5050, NULL, 'some_component', 1, '2022-01-13 18:17:21', '2922-01-14 08:48:25', '0001-01-01 00:00:00');

INSERT INTO attribute (id, identifier, attributedatatype, date_created, date_updated, date_deleted, write_protected)
VALUES (100, 'attribute_writeable', 'string', '2020-07-31 14:34:21', NULL, '0001-01-01 00:00:00', false),
       (101, 'attribute_protected', 'string', '2020-07-31 14:34:21', NULL, '0001-01-01 00:00:00', true);

INSERT INTO attributevalue (id, value, date_created, date_updated, date_deleted, user_created_id)
VALUES (100, 'demo_value', '2020-07-31 14:34:21', NULL, '0001-01-01 00:00:00', 0);

INSERT INTO `item` (`id`, `identifier`, `date_created`, `date_updated`, `date_deleted`)
VALUES (5050, 'some_item', '2022-01-13 18:17:21', '2922-01-14 08:48:25', '0001-01-01 00:00:00');

INSERT INTO item_optionclassification (id, item_id, optionclassification_id, date_created, date_updated, date_deleted)
VALUES (5050, 5050,5050,'2020-07-31 14:34:21', NULL, '0001-01-01 00:00:00');