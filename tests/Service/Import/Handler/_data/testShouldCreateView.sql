DELETE FROM `design_area`;
INSERT INTO `design_area` (`id`, `identifier`, `sequence_number`, `height`, `width`, `custom_data`, `date_created`,
                           `date_deleted`, `item_id`, `option_id`, `mask`)
VALUES (1, 'front_center', 1, 60, 50, NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 1, NULL, '{ "data": "design_area" }');

DELETE FROM `item`;
INSERT INTO `item` (`id`, `identifier`, `minimum_order_amount`, `date_created`, `date_deleted`, `parent_id`,
                    `visualization_mode_id`, `item_status_id`, `configuration_mode`)
VALUES (1, 'demo_hoodie', 1, '2019-06-07 14:53:02', '0001-01-01 00:00:00', NULL, 2, 2, 'designer');
