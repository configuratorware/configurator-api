<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import\Handler;

use Psr\Log\NullLogger;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DeleteHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

final class DeleteHandlerTest extends ApiTestCase
{
    private DeleteHandler $deleteHandler;

    /**
     * @var LicenseFileValidator|\Phake_IMock
     * @Mock LicenseFileValidator
     */
    protected $licenseFileValidator;

    public function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $connection = $this->em->getConnection();
        $logger = new ImportLogger(new NullLogger());
        $importHelper = new ImportHelper($connection, $logger, $this->licenseFileValidator);

        $this->deleteHandler = new DeleteHandler($connection, $importHelper);
    }

    /**
     * @dataProvider fixtureProvider
     */
    public function testShouldDeleteNonExistingAttributeRelations(string $fixtureFile, string $entityName): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/' . $fixtureFile));
        $this->deleteHandler->deleteNonExistingAttributeRelations(new \DateTime(), true, ['some_item']);

        $sql = sprintf('SELECT * FROM %s WHERE attribute_id = 100', $entityName);
        $componentAttributeRelation = $this->em->getConnection()->executeQuery($sql)->fetchAllAssociative();
        $dateDeleted = $componentAttributeRelation[0]['date_deleted'];
        self::assertNotEquals(EntityListener::DATE_DELETED_DEFAULT, $dateDeleted);
    }

    /**
     * @dataProvider fixtureProvider
     */
    public function testCannotDeleteWriteProtectedAttributeRelations(string $fixtureFile, string $entityName): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/' . $fixtureFile));
        $this->deleteHandler->deleteNonExistingAttributeRelations(new \DateTime(), false, ['some_item']);

        $sql = sprintf('SELECT * FROM %s WHERE attribute_id = 101', $entityName);
        $componentAttributeRelation = $this->em->getConnection()->executeQuery($sql)->fetchAllAssociative();
        $dateDeleted = $componentAttributeRelation[0]['date_deleted'];
        self::assertEquals(EntityListener::DATE_DELETED_DEFAULT, $dateDeleted);
    }

    public function fixtureProvider(): array
    {
        return [
            'item_attributes' => [
                'testShouldDeleteNonExistingItemAttributeRelations_setup.sql',
                'item_attribute',
            ],
            'option_classification_attributes' => [
                'testShouldDeleteNonExistingOptionClassificationAttributeRelations_setup.sql',
                'option_classification_attribute',
            ],
            'option_attributes' => [
                'testShouldDeleteNonExistingOptionAttributeRelations_setup.sql',
                'option_attribute',
            ],
        ];
    }
}
