<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import\Handler;

use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ColorChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DesignHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DesignHandlerTest extends ApiTestCase
{
    /**
     * @var DesignHandler
     */
    private $designHandler;

    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var DesignAreaDesignProductionMethodRepository
     */
    private $designAreaDesignProductionMethodRepository;

    /**
     * @var LicenseFileValidator|\Phake_IMock
     * @Mock LicenseFileValidator
     */
    protected $licenseFileValidator;

    public function setUp(): void
    {
        parent::setUp();
        \Phake::initAnnotations($this);

        $this->creatorViewRepository = $this->em->getRepository(CreatorView::class);
        $this->designAreaDesignProductionMethodRepository = $this->em->getRepository(DesignAreaDesignProductionMethod::class);

        $connection = $this->em->getConnection();
        $logger = new ImportLogger(new NullLogger());
        $importHelper = new ImportHelper($connection, $logger, $this->licenseFileValidator);

        $this->designHandler = new DesignHandler($connection, $importHelper, $logger, new ColorChecker());
    }

    /**
     * @param $visualData
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     *
     * @dataProvider providerShouldCreateView
     */
    public function testShouldCreateView($visualData): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/testShouldCreateView.sql'));

        $this->designHandler->processDesignAreaCreatorViewData($visualData, 1, 1);

        $creatorView = $this->creatorViewRepository->findOneByIdentifier('front_view');

        self::assertCount(2, $creatorView->getCreatorViewText());
        self::assertSame('XYZ', $creatorView->getCreatorViewDesignArea()->first()->getBaseShape()->rotation->order);
        self::assertSame(144, $creatorView->getCreatorViewDesignArea()->first()->getPosition()->height);
        self::assertSame(72, $creatorView->getCreatorViewDesignArea()->first()->getPosition()->y);
    }

    public function providerShouldCreateView(): \Generator
    {
        yield [
            [
                'designViewIdentifier' => 'front_view',
                'baseShape' => '{"type":"Plane","width":144,"height":144,"x":-179.99999999999997,"y":-68.18181818181799,"rotation":{"x":0,"y":0,"z":0,"order":"XYZ"}}',
                'default' => false,
                'texts' => [
                    [
                        'iso' => 'en_GB',
                        'title' => 'Front view',
                    ],
                    [
                        'iso' => 'de_DE',
                        'title' => 'Vorderansicht view',
                    ],
                   ],
                'customData' => null,
                'position' => [
                    'x' => 72,
                    'y' => 72,
                ],
                'widthPixels' => 144,
                'heightPixels' => 144,
            ],
        ];
    }

    /**
     * @dataProvider providerShouldProcessDesignProductionMethods
     */
    public function testShouldProcessDesignProductionMethods($designProductionMethod): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/testShouldProcessDesignProductionMethods.sql'));

        $this->designHandler->processDesignProductionMethods($designProductionMethod, 1, 1, '_default');

        $areaProductionMethods = $this->designAreaDesignProductionMethodRepository->findBy(['designArea' => 1]);

        [$price1, $price2] = $areaProductionMethods[0]->getDesignAreaDesignProductionMethodPrice();

        self::assertSame(1.19, (float) $price1->getPrice());
        self::assertSame(2.38, (float) $price2->getPrice());
    }

    public function providerShouldProcessDesignProductionMethods(): \Generator
    {
        yield [
            [
                'designProductionMethodIdentifier' => 'screen_printing',
                'widthMillimeter' => 0,
                'heightMillimeter' => 0,
                'prices' => [
                    [
                        'channelIdentifier' => '_default',
                        'calculationTypes' => [
                            [
                                'typeIdentifier' => 'printing_costs_screen_printing',
                                'bulkPrices' => [
                                    [
                                        'itemAmountFrom' => 1,
                                        'colorAmountFrom' => 1,
                                        'price' => 1.19,
                                        'priceNet' => 1,
                                    ],
                                    [
                                        'itemAmountFrom' => 5,
                                        'colorAmountFrom' => 1,
                                        'price' => 2.38,
                                        'priceNet' => 2,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider providerCouldNotCreateProductionMethodPrices
     */
    public function testCouldNotCreateProductionMethodPrices($designProductionMethod): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/testShouldProcessDesignProductionMethods.sql'));

        $this->designHandler->processDesignProductionMethods($designProductionMethod, 1, 1, '_default');

        $areaProductionMethods = $this->designAreaDesignProductionMethodRepository->findBy(['designArea' => 1]);

        self::assertCount(0, $areaProductionMethods[0]->getDesignAreaDesignProductionMethodPrice());
    }

    public function providerCouldNotCreateProductionMethodPrices(): \Generator
    {
        yield [
            [
                'designProductionMethodIdentifier' => 'screen_printing',
                'widthMillimeter' => 0,
                'heightMillimeter' => 0,
                'prices' => [
                    [
                        'channelIdentifier' => '_default',
                        'calculationTypes' => [
                            [
                                'typeIdentifier' => 'non_existent_key',
                                'bulkPrices' => [
                                    [
                                        'itemAmountFrom' => 1,
                                        'colorAmountFrom' => 1,
                                        'price' => 1.19,
                                        'priceNet' => 1,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
