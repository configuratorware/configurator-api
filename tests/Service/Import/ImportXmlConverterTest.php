<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use DMS\PHPUnitExtensions\ArraySubset\ArraySubsetAsserts;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ImportXmlConverterTest extends ApiTestCase
{
    use ArraySubsetAsserts;

    /**
     * @var ImportXmlConverter
     */
    private $importXmlConverter;

    /**
     * @var string
     */
    private $xmlString;

    /**
     * @var array
     */
    private $jsonData;

    protected function setUp(): void
    {
        parent::setUp();

        $this->importXmlConverter = new ImportXmlConverter();

        $this->xmlString = file_get_contents(__DIR__ . '/_data/validTestData.XML');
        $this->jsonData = json_decode(file_get_contents(__DIR__ . '/_data/validTestData.json'), true);
    }

    public function testXmlConverter()
    {
        $result = $this->importXmlConverter->convert($this->xmlString);

        $xmlResultItem = $result[0];
        $jsonResultItem = $this->jsonData[0];

        self::assertNotEmpty($result);
        self::assertEquals(count($result), 2);
        self::assertArraySubset($xmlResultItem, $this->getTestItemArray());
        self::assertArraySubset($this->getTestItemArray(), $xmlResultItem);

        $comparableRootSubsets = ['attributes', 'texts', 'images', 'prices', 'children', 'components'];
        foreach ($comparableRootSubsets as $subsetKey) {
            self::assertArraySubset(
                $xmlResultItem[$subsetKey],
                $jsonResultItem[$subsetKey],
                false,
                $subsetKey
            );
        }

        $comparableRootValues = ['itemIdentifier', 'minimumOrderAmount'];
        foreach ($comparableRootValues as $subsetKey) {
            self::assertEquals($xmlResultItem[$subsetKey], $jsonResultItem[$subsetKey], $subsetKey);
        }

        $comparableDesignAreasSubsets = ['designProductionMethods', 'texts', 'visualizationBackgroundColors'];
        foreach ($comparableDesignAreasSubsets as $subsetKey) {
            self::assertArraySubset(
                $xmlResultItem['designAreas'][0][$subsetKey],
                $jsonResultItem['designAreas'][0][$subsetKey],
                false,
                $subsetKey
            );
        }
    }

    /**
     * @return array
     * @SuppressWarnings(PHPMD)
     */
    protected function getTestItemArray()
    {
        return [
            'attributes' => [
                    0 => [
                            'attributeIdentifier' => 'tag',
                            'attributeValues' => [
                                    0 => [
                                            'translations' => [
                                                    0 => [
                                                            'iso' => 'en_GB',
                                                            'translation' => 'Summer',
                                                        ],
                                                ],
                                            'value' => 'summer',
                                        ],
                                    1 => [
                                            'translations' => [
                                                    0 => [
                                                            'iso' => 'en_US',
                                                            'translation' => 'Summer2',
                                                        ],
                                                ],
                                            'value' => 'summer',
                                        ],
                                ],
                            'texts' => [
                                    0 => [
                                            'iso' => 'en_GB',
                                            'title' => 'Tag',
                                        ],
                                ],
                            'type' => 'string',
                        ],
                ],
            'children' => [
                    0 => [
                            'attributes' => [
                                    0 => [
                                            'attributeIdentifier' => 'tag',
                                            'attributeValues' => [
                                                    0 => [
                                                            'translations' => [
                                                                    0 => [
                                                                            'iso' => 'en_GB',
                                                                            'translation' => 'Winter',
                                                                        ],
                                                                    1 => [
                                                                            'iso' => 'de_DE',
                                                                            'translation' => 'Winter',
                                                                        ],
                                                                ],
                                                            'value' => 'winter',
                                                        ],
                                                ],
                                            'texts' => [
                                                    0 => [
                                                            'iso' => 'en_GB',
                                                            'title' => 'Tag',
                                                        ],
                                                ],
                                            'type' => 'string',
                                        ],
                                ],
                            'childGroups' => [
                                    0 => [
                                            'group' => [
                                                    'groupIdentifier' => 'color',
                                                    'texts' => [
                                                            0 => [
                                                                    'iso' => 'en_GB',
                                                                    'title' => 'Color',
                                                                ],
                                                        ],
                                                ],
                                            'groupValue' => [
                                                    'groupValueIdentifier' => 'red',
                                                    'texts' => [
                                                            0 => [
                                                                    'iso' => 'en_GB',
                                                                    'title' => 'Red',
                                                                ],
                                                        ],
                                                ],
                                        ],
                                    1 => [
                                            'group' => [
                                                    'groupIdentifier' => 'size',
                                                    'texts' => [
                                                            0 => [
                                                                    'iso' => 'en_GB',
                                                                    'title' => 'Size',
                                                                ],
                                                        ],
                                                ],
                                            'groupValue' => [
                                                    'groupValueIdentifier' => 's',
                                                    'texts' => [
                                                            0 => [
                                                                    'iso' => 'en_GB',
                                                                    'title' => 'S',
                                                                ],
                                                        ],
                                                ],
                                        ],
                                ],
                            'images' => [
                                    'detailImage' => 'http://localhost/tests/sample-1.jpg',
                                    'thumbnail' => 'http://localhost/tests/sample-1.jpg',
                                ],
                            'minimumOrderAmount' => 1,
                            'accumulateAmounts' => true,
                            'itemIdentifier' => 't_shirt_red_s',
                            'prices' => [
                                    0 => [
                                            'bulkPrices' => [
                                                    0 => [
                                                            'itemAmountFrom' => 1,
                                                            'price' => 19.9,
                                                            'priceNet' => 16.7,
                                                        ],
                                                ],
                                            'channelIdentifier' => 'de_eur',
                                        ],
                                ],
                            'texts' => [
                                    0 => [
                                            'description' => '',
                                            'iso' => 'en_GB',
                                            'title' => 'T-shirt, red, s',
                                        ],
                                ],
                        ],
                ],
            'designAreas' => [
                    0 => [
                            'customData' => '[]',
                            'designAreaIdentifier' => 'front_top',
                            'designProductionMethods' => [
                                    0 => [
                                            'designProductionMethodIdentifier' => 'offset_print',
                                            'heightMillimeter' => 30,
                                            'prices' => [
                                                    0 => [
                                                            'calculationTypes' => [
                                                                    0 => [
                                                                            'bulkPrices' => [
                                                                                    0 => [
                                                                                            'colorAmountFrom' => 1,
                                                                                            'itemAmountFrom' => 1,
                                                                                            'price' => 1.99,
                                                                                            'priceNet' => 1.67,
                                                                                        ],
                                                                                ],
                                                                            'typeIdentifier' => 'productionPrice',
                                                                        ],
                                                                ],
                                                            'channelIdentifier' => 'de_eur',
                                                        ],
                                                ],
                                            'widthMillimeter' => 320,
                                            'allowBulkNames' => true,
                                            'default' => true,
                                            'minimumOrderAmount' => 100,
                                        ],
                                ],
                            'heightMillimeter' => 30,
                            'texts' => [
                                    0 => [
                                            'iso' => 'de_DE',
                                            'title' => 'front top',
                                        ],
                                ],
                            'visualData' => [
                                    0 => [
                                            'customData' => '[]',
                                            'designViewIdentifier' => 'front',
                                            'default' => true,
                                            'heightPixels' => 20,
                                            'viewThumbnails' => [
                                                    0 => [
                                                            'itemIdentifier' => 't_shirt_red_s',
                                                            'viewThumbnailURL' => 'http://localhost/tests/sample-1.jpg',
                                                        ],
                                                ],
                                            'mediaURLs' => [
                                                    0 => [
                                                            'itemIdentifier' => 't_shirt_red_s',
                                                            'mediaURL' => 'http://localhost/tests/sample-1.jpg',
                                                        ],
                                                ],
                                            'position' => [
                                                    'x' => 400,
                                                    'y' => 200,
                                                ],
                                            'texts' => [
                                                    0 => [
                                                            'iso' => 'en_GB',
                                                            'title' => 'Front view',
                                                        ],
                                                ],
                                            'widthPixels' => 200,
                                        ],
                                ],
                            'visualizationBackgroundColors' => [
                                    0 => [
                                            'itemIdentifier' => 't_shirt_red_s',
                                            'color' => '#FF0000',
                                        ],
                                ],
                            'widthMillimeter' => 300,
                        ],
                ],
            'images' => [
                    'detailImage' => 'http://localhost/tests/sample-1.jpg',
                    'thumbnail' => 'http://localhost/tests/sample-1.jpg',
                ],
            'minimumOrderAmount' => 1,
            'accumulateAmounts' => true,
            'itemIdentifier' => 't_shirt',
            'configurationMode' => 'designer',
            'itemStatusIdentifier' => 'available',
            'visualizationModeIdentifier' => '2dVariant',
            'itemStatuses' => [
                    0 => [
                            'available' => false,
                            'configurationModeIdentifier' => 'creator',
                        ],
                    1 => [
                            'available' => true,
                            'configurationModeIdentifier' => 'designer',
                        ],
                ],
            'prices' => [
                    0 => [
                            'bulkPrices' => [
                                    0 => [
                                            'itemAmountFrom' => 1,
                                            'price' => 19.9,
                                            'priceNet' => 16.7,
                                        ],
                                ],
                            'channelIdentifier' => 'de_eur',
                        ],
                    1 => [
                            'bulkPrices' => [
                                    0 => [
                                            'itemAmountFrom' => 10,
                                            'price' => 9.9,
                                            'priceNet' => 6.7,
                                        ],
                                ],
                            'channelIdentifier' => 'de_eur',
                        ],
                ],
            'texts' => [
                    0 => [
                            'description' => '',
                            'iso' => 'en_GB',
                            'title' => 'T-shirt',
                        ],
                ],
            'components' => [
                0 => [
                        'componentIdentifier' => 'main_color',
                        'isMultiselect' => true,
                        'sequencenumber' => 3,
                        'images' => [
                                'thumbnail' => 'http://localhost/tests/sample-1.jpg',
                            ],
                        'texts' => [
                                0 => [
                                        'iso' => 'en_GB',
                                        'title' => 'Main Color',
                                    ],
                                1 => [
                                        'iso' => 'de_DE',
                                        'title' => 'Hauptfarbe',
                                    ],
                            ],
                        'attributes' => [
                                0 => [
                                        'attributeIdentifier' => 'category_type',
                                        'attributeValues' => [
                                                0 => [
                                                        'translations' => [
                                                                0 => [
                                                                        'iso' => 'en_GB',
                                                                        'translation' => 'Fabrics',
                                                                    ],
                                                                1 => [
                                                                        'iso' => 'de_DE',
                                                                        'translation' => 'Stoffe',
                                                                    ],
                                                            ],
                                                        'value' => 'fabrics',
                                                    ],
                                            ],
                                        'texts' => [
                                                0 => [
                                                        'iso' => 'en_GB',
                                                        'title' => 'Category Type',
                                                    ],
                                                1 => [
                                                        'iso' => 'de_DE',
                                                        'title' => 'Kategorie Typ',
                                                    ],
                                            ],
                                        'type' => 'string',
                                    ],
                            ],
                        'defaultSelections' => [
                                [
                                    'optionIdentifier' => 'red',
                                    'amount' => 1,
                                ],
                            ],
                        'options' => [
                                0 => [
                                        'optionIdentifier' => 'red',
                                        'amountIsSelectable' => false,
                                        'inputValidationType' => 'SomeValidator',
                                        'hasTextinput' => true,
                                        'sequencenumber' => 2,
                                        'images' => [
                                                'thumbnail' => 'http://localhost/tests/sample-1.jpg',
                                                'detailImage' => 'http://localhost/tests/sample-1.jpg',
                                            ],
                                        'texts' => [
                                                0 => [
                                                        'iso' => 'en_GB',
                                                        'title' => 'Red',
                                                    ],
                                                1 => [
                                                        'iso' => 'de_DE',
                                                        'title' => 'Rot',
                                                    ],
                                            ],
                                        'prices' => [
                                                0 => [
                                                    'prices' => [
                                                            0 => [
                                                                    'amountFrom' => 1,
                                                                    'price' => 5.1,
                                                                    'priceNet' => 4.29,
                                                                ],
                                                        ],
                                                    'channelIdentifier' => 'de_eur',
                                                ],
                                            ],
                                        'deltaPrices' => [
                                                0 => [
                                                    'prices' => [
                                                            0 => [
                                                                    'price' => 3.9,
                                                                    'priceNet' => 3.28,
                                                                ],
                                                        ],
                                                    'channelIdentifier' => 'de_eur',
                                                ],
                                            ],
                                        'attributes' => [
                                                0 => [
                                                        'attributeIdentifier' => 'color_filter',
                                                        'attributeValues' => [
                                                                0 => [
                                                                        'translations' => [
                                                                                0 => [
                                                                                        'iso' => 'en_GB',
                                                                                        'translation' => 'Warm Colors',
                                                                                    ],
                                                                                1 => [
                                                                                        'iso' => 'de_DE',
                                                                                        'translation' => 'Warme Farben',
                                                                                    ],
                                                                            ],
                                                                        'value' => 'warm_colors',
                                                                    ],
                                                            ],
                                                        'texts' => [
                                                                0 => [
                                                                        'iso' => 'en_GB',
                                                                        'title' => 'Color Filter',
                                                                    ],
                                                                1 => [
                                                                        'iso' => 'de_DE',
                                                                        'title' => 'Farb Filter',
                                                                    ],
                                                            ],
                                                        'type' => 'string',
                                                    ],
                                            ],
                                    ],
                            ],
                    ],
            ],
        ];
    }
}
