<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\AttributeHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DefaultConfigurationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DeleteHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DesignHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\MediaHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\OptionClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportWorker;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;

class ImportWorkerTest extends TestCase
{
    /**
     * @var Connection
     *
     * @Mock
     */
    private $connection;

    /**
     * @var AttributeHandler
     *
     * @Mock
     */
    private $attributeHandler;

    /**
     * @var OptionClassificationHandler
     *
     * @Mock
     */
    private $optionClassificationHandler;

    /**
     * @var DefaultConfigurationHandler
     *
     * @Mock
     */
    private $defaultConfigurationHandler;

    /**
     * @var ItemClassificationHandler
     *
     * @Mock
     */
    private $itemClassificationHandler;

    /**
     * @var DeleteHandler
     *
     * @Mock
     */
    private $deleteHandler;

    /**
     * @var DesignHandler
     *
     * @Mock
     */
    private $designHandler;

    /**
     * @var MediaHandler
     *
     * @Mock
     */
    private $mediaHandler;

    /**
     * @var ItemHandler
     *
     * @Mock
     */
    private $itemHandler;

    /**
     * @var ImportValidator
     *
     * @Mock
     */
    private $validator;

    /***
     * @var ImportWorker
     */
    private $importWorker;

    /**
     * @var LicenseFileValidator|\Phake_IMock
     * @Mock LicenseFileValidator
     */
    protected $licenseFileValidator;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        \Phake::when($this->validator)->validateItem->thenReturn(true);
        \Phake::when($this->itemHandler)->processItem->thenReturn(8);
        \Phake::when($this->designHandler)->processDesignArea->thenReturn(30);

        $testLogger = new ImportLogger(new NullLogger());
        $importHelper = new ImportHelper($this->connection, $testLogger, $this->licenseFileValidator);

        $this->importWorker = new ImportWorker(
            $this->connection,
            $this->attributeHandler,
            $this->optionClassificationHandler,
            $this->defaultConfigurationHandler,
            $this->deleteHandler,
            $this->designHandler,
            $testLogger,
            $this->mediaHandler,
            $importHelper,
            $this->itemHandler,
            $this->validator,
            $this->itemClassificationHandler
        );
    }

    /**
     * @param $expectedCreatorViewToProcess
     * @param $givenImportData
     *
     * @dataProvider providerShouldImportCreatorViews
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function testShouldImportCreatorViews($expectedCreatorViewToProcess, $givenImportData): void
    {
        $this->importWorker->import(new ImportOptions(), $givenImportData);

        \Phake::verify($this->designHandler)->processDesignAreaCreatorViewData($expectedCreatorViewToProcess, 30, 8);
    }

    public function providerShouldImportCreatorViews(): \Generator
    {
        $visualData = [
            'designViewIdentifier' => 'front_view',
            'baseShape' => '{"type":"Plane"}',
            'default' => false,
            'texts' => [],
            'customData' => null,
            'position' => [
                'x' => 72,
                'y' => 72,
            ],
            'widthPixels' => 144,
            'heightPixels' => 144,
        ];

        yield [
            $visualData,
            [
                [
                    'itemIdentifier' => 'testShouldImportCreatorViews',
                    'texts' => [],
                    'prices' => [],
                    'attributes' => [],
                    'images' => [],
                    'minimumOrderAmount' => 1,
                    'accumulateAmounts' => true,
                    'configurationMode' => 'creator+designer',
                    'designAreas' => [
                        [
                            'designAreaIdentifier' => '',
                            'texts' => [],
                            'visualData' => [
                                $visualData,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $expectedDesignViewToProcess
     * @param $givenImportData
     *
     * @dataProvider providerWithEmptyConfigurationMode
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function testShouldImportDesignViews($expectedDesignViewToProcess, $givenImportData): void
    {
        \Phake::when($this->designHandler)->processDesignAreaViewData($expectedDesignViewToProcess, 8)->thenReturn(2);

        $this->importWorker->import(new ImportOptions(), $givenImportData);

        \Phake::verify($this->designHandler)->processDesignViewDesignArea($expectedDesignViewToProcess, 30, 2);
    }

    /**
     * @param $expectedCreatorViewToProcess
     * @param $givenImportData
     *
     * @dataProvider providerWithEmptyConfigurationMode
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function testShouldImportCreatorViewWithItemInDB($expectedCreatorViewToProcess, $givenImportData): void
    {
        // db returns item with configuration mode
        \Phake::when($this->itemHandler)->findItem('providerWithEmptyConfigurationMode')->thenReturn(['configuration_mode' => 'creator+designer']);

        $this->importWorker->import(new ImportOptions(), $givenImportData);

        \Phake::verify($this->designHandler)->processDesignAreaCreatorViewData($expectedCreatorViewToProcess, 30, 8);
    }

    public function providerWithEmptyConfigurationMode(): \Generator
    {
        $visualData = [
            'designViewIdentifier' => 'front_view',
            'baseShape' => '{"type":"Plane"}',
            'default' => false,
            'texts' => [],
            'customData' => null,
            'position' => [
                'x' => 72,
                'y' => 72,
            ],
            'widthPixels' => 144,
            'heightPixels' => 144,
        ];

        yield [
            $visualData,
            [
                [
                    'itemIdentifier' => 'providerWithEmptyConfigurationMode',
                    'texts' => [],
                    'prices' => [],
                    'attributes' => [],
                    'images' => [],
                    'minimumOrderAmount' => 1,
                    'accumulateAmounts' => true,
                    'configurationMode' => null,
                    'designAreas' => [
                        [
                            'designAreaIdentifier' => '',
                            'texts' => [],
                            'visualData' => [
                                $visualData,
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
