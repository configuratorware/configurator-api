<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ImportFileReaderMalformedTest extends ApiTestCase
{
    /**
     * @var ImporterFileReader
     */
    private $importReader;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $testBaseDir;

    /**
     * @var ImportOptions
     */
    private $importOptions;

    /**
     * Test files with date modification time to keep them in order for directory read tests.
     *
     * @var array
     */
    private static $malformedTestFiles = [
        'malformedTestData.XML' => 200,
        'malformedTestData.json' => 100,
    ];

    protected function setUp(): void
    {
        parent::setUp();

        $container = static::$kernel->getContainer();
        /* @var $container ContainerInterface */
        $this->importReader = $container->get('Redhotmagma\ConfiguratorApiBundle\Service\Import\ImporterFileReader');
        $this->fileSystem = new Filesystem();

        $this->testBaseDir = __DIR__ . '/../../../var/test_files_malformed';

        foreach (static::$malformedTestFiles as $contentName => $timeSubtract) {
            $this->fileSystem->copy(
                __DIR__ . '/_data/' . $contentName,
                $this->testBaseDir . '/' . $contentName,
                true
            );
            $this->fileSystem->touch(
                $this->testBaseDir . '/' . $contentName,
                time() - $timeSubtract
            );
        }

        $this->importOptions = new ImportOptions();
    }

    protected function tearDown(): void
    {
        foreach (static::$malformedTestFiles as $contentName => $timeSubtract) {
            $this->fileSystem->remove($this->testBaseDir . '/' . $contentName);
        }
        parent::tearDown();
    }

    public function testReadMalformedJsonFileFromUrl()
    {
        $this->markTestSkipped('loading from url has to be fixed for jenkins context');
        $content = $this->importReader->readSource('http://127.0.0.1/malformedTestData.json', $this->importOptions);
        self::assertFalse($this->importReader->isJson($content));
        self::assertNotEmpty($content);
    }

    public function testReadMalformedXmlFileFromUrl()
    {
        $this->markTestSkipped('loading from url has to be fixed for jenkins context');
        $content = $this->importReader->readSource('http://127.0.0.1/malformedTestData.XML', $this->importOptions);
        self::assertFalse($this->importReader->isXML($content));
        self::assertNotEmpty($content);
    }

    public function testReadMalformedJsonFileFromPath()
    {
        $content = $this->importReader->readSource(
            $this->testBaseDir . '/malformedTestData.json',
            $this->importOptions
        );
        self::assertFalse($this->importReader->isJson($content));
        self::assertNotEmpty($content);
    }

    public function testReadMalformedXmlFileFromPath()
    {
        $content = $this->importReader->readSource(
            $this->testBaseDir . '/malformedTestData.XML',
            $this->importOptions
        );
        self::assertFalse($this->importReader->isXML($content));
        self::assertNotEmpty($content);
    }

    public function testReadMalformedFilesFromDirectory()
    {
        // read json in mod date order
        $content = $this->importReader->readSource($this->testBaseDir, $this->importOptions);
        self::assertFalse($this->importReader->isJson($content));
        self::assertNotEmpty($content);

        // read remaining XML
        $content = $this->importReader->readSource($this->testBaseDir, $this->importOptions);
        self::assertFalse($this->importReader->isXML($content));
        self::assertNotEmpty($content);
    }
}
