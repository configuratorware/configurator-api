<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevaluetranslation;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassificationtext;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationAttribute;

/**
 * @BackupDatabase
 */
class ImportWorkerInsertTest extends ImportWorkerBaseTest
{
    public function testInserts(): void
    {
        $this->assertItemImportInsert();
        $this->assertItemTextImport();
        $this->assertGroupImport();
        $this->assertAreaImport();
        $this->assertDesignAreaProductionMethodImport();
        $this->assertAttributeImport();
        $this->assertViewImport();
        $this->assertDesignViewDesignAreaImportDefaults();
        $this->assertImageImport();
        $this->assertColorImport();
        $this->assertComponentImport();
        $this->assertOptionImport();
        $this->assertComponentRelations();
        $this->assertItemOptionclassificationOptionSequenceNumber();
        $this->assertDefaultOptionsSaved();
        $this->assertItemClassification();
        $this->assertItemClassificationRelations();
        $this->assertItemClassificationTexts();
    }

    public function assertItemImportInsert(): void
    {
        $importedItem = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        /* @var $importedItem Item */
        self::assertNotNull($importedItem);
        self::assertEquals(22, $importedItem->getMinimumOrderAmount());
        self::assertTrue($importedItem->getAccumulateAmounts());
        self::assertEquals('designer', $importedItem->getConfigurationMode());
        self::assertEquals('available', $importedItem->getItemStatus()->getIdentifier());
        self::assertEquals('2dVariant', $importedItem->getVisualizationMode()->getIdentifier());
        self::assertTrue($importedItem->getOverwriteComponentOrder());
        self::assertTrue($importedItem->getOverrideOptionOrder());

        $fixtureItem = $this->em->getRepository(Item::class)
            ->findOneBy(['identifier' => 'sheep']);
        self::assertNotNull($fixtureItem);
    }

    public function assertItemTextImport(): void
    {
        $importedText = $this->em->getRepository(Itemtext::class)->findOneBy(['title' => 'T-shirt']);
        self::assertNotNull($importedText);
    }

    public function assertGroupImport(): void
    {
        $group = $this->em->getRepository(Itemgroup::class)
            ->findOneBy(['identifier' => 'color']);
        self::assertNotNull($group);
        self::assertEquals('color', $group->getIdentifier());

        $itemgroup = $this->em->getRepository(ItemItemgroup::class)
            ->findOneBy(['itemgroup' => $group]);
        self::assertNotNull($itemgroup);
    }

    public function assertAreaImport(): void
    {
        /* @var DesignArea $designArea */
        $designArea = $this->em->getRepository(DesignArea::class)
            ->findOneBy(['identifier' => 'front_top']);
        self::assertNotNull($designArea);
        self::assertEquals(30, $designArea->getHeight());
        self::assertEquals(300, $designArea->getWidth());
        self::assertEquals(null, $designArea->getCustomData());

        /* @var DesignAreaDesignProductionMethod $designAreaProductionMethod */
        $designAreaProductionMethod = $designArea->getDesignAreaDesignProductionMethod()->last();
        self::assertEquals(320, $designAreaProductionMethod->getWidth());
        self::assertEquals(30, $designAreaProductionMethod->getHeight());
        self::assertEquals(100, $designAreaProductionMethod->getMinimumOrderAmount());

        /* @var DesignAreaDesignProductionMethodPrice $price */
        $price = $this->em->getRepository(DesignAreaDesignProductionMethodPrice::class)->findOneBy(['price' => 1.99]);
        self::assertNotNull($price);
        self::assertEquals(1, $price->getAmountFrom());
        self::assertEquals(1, $price->getColorAmountFrom());
    }

    public function assertDesignAreaProductionMethodImport(): void
    {
        /* @var DesignAreaDesignProductionMethod $method */
        $method = $this->em->getRepository(DesignAreaDesignProductionMethod::class)->findOneBy(['is_default' => true]);

        self::assertTrue($method->getIsDefault());
        self::assertTrue($method->getAllowBulkNames());
        self::assertTrue($method->getDesignElementsLocked());
        self::assertEquals(320, $method->getWidth());
        self::assertEquals(30, $method->getHeight());
        self::assertEquals(5, $method->getMaxElements());
        self::assertEquals(2, $method->getMaxImages());
        self::assertEquals(3, $method->getMaxTexts());
        self::assertJsonStringEqualsJsonString(
            '{"engravingBackgroundColors":[{"itemIdentifier":"t_shirt_red_s","colorHex": "#FF0000"}]}',
            json_encode($method->getAdditionalData())
        );
    }

    public function assertAttributeImport(): void
    {
        $attribute = $this->em->getRepository(Attribute::class)/* @var $attribute Attribute */
        ->findOneBy(['identifier' => 'tag']);
        self::assertNotNull($attribute);
        self::assertEquals('tag', $attribute->getIdentifier());

        $translation = $this->em->getRepository(Attributevaluetranslation::class)
            ->findOneBy(['translation' => 'Summer']);
        self::assertNotNull($translation);
        self::assertEquals('Summer', $translation->getTranslation());
    }

    public function assertViewImport(): void
    {
        $view = $this->em->getRepository(DesignView::class)/* @var $view DesignView */
        ->findOneBy(['identifier' => 'front']);
        self::assertNotNull($view);

        $text = $this->em->getRepository(DesignViewText::class)
            ->findOneBy(['title' => 'Front view']);
        self::assertNotNull($text);
    }

    public function assertDesignViewDesignAreaImportDefaults(): void
    {
        /* @var $entities DesignViewDesignArea[] */
        $entities = $this->em->getRepository(DesignViewDesignArea::class)->findAll();
        self::assertNotNull($entities);
        self::assertCount(2, $entities);

        /* @var $defaultEntities DesignViewDesignArea[] */
        $defaultEntities = array_values(
            array_filter(
                $entities,
                static function (DesignViewDesignArea $entity) {
                    return $entity->getIsDefault();
                }
            )
        );

        self::assertCount(1, $defaultEntities);
        self::assertSame('front', $defaultEntities[0]->getDesignView()->getIdentifier());
    }

    public function assertImageImport(): void
    {
        self::assertFileExists($this->rootPath . '/images/item/image/t_shirt.jpg');
        self::assertFileExists($this->rootPath . '/images/item/image/t_shirt_red_s.jpg');
        self::assertFileExists($this->rootPath . '/images/item/thumb/t_shirt.jpg');
        self::assertFileExists($this->rootPath . '/images/item/thumb/t_shirt_red_s.jpg');
        self::assertFileExists($this->rootPath . '/images/item/view/t_shirt/front/t_shirt_red_s.jpg');
        self::assertFileExists($this->rootPath . '/' . $this->viewThumbnailPath . '/t_shirt/t_shirt_red_s/front.jpg');
    }

    /**
     * Test if default color is present after import - select color by color identifier and palette identifier.
     */
    public function assertColorImport(): void
    {
        /* @var $designAreaProdMethods DesignAreaDesignProductionMethod */
        $designAreaProdMethods = $this->em->getRepository(DesignAreaDesignProductionMethod::class)
            ->findOneBy([], ['date_updated' => 'desc']);
        self::assertNotNull($designAreaProdMethods);
        self::assertJsonStringEqualsJsonString(
            '{"t_shirt_red_s":{"default":"yellow"}}',
            json_encode($designAreaProdMethods->getDefaultColors())
        );
    }

    /**
     * Test if image is overwritten according toy the importOption settings.
     */
    public function testImageOverwrite(): void
    {
        $detailImagePath = $this->rootPath . '/images/item/image/t_shirt.jpg';
        $itemThumbPath = $this->rootPath . '/images/item/thumb/t_shirt.jpg';
        $this->fileSystem->dumpFile($detailImagePath, 'test_content');
        $this->fileSystem->dumpFile($itemThumbPath, 'test_content');

        $this->importOptions->overwritedetailImages = false;
        $this->importWorker->import($this->importOptions, $this->getTestData('import_data_insert_test'));

        self::assertStringEqualsFile($detailImagePath, 'test_content');
        self::assertStringEqualsFile($itemThumbPath, 'test_content');

        $this->importOptions->overwriteItemImages = true;
        $this->importWorker->import($this->importOptions, $this->getTestData('import_data_insert_test'));

        self::assertStringNotEqualsFile($detailImagePath, 'test_content');
        self::assertStringNotEqualsFile($itemThumbPath, 'test_content');
    }

    public function assertComponentImport(): void
    {
        /* @var $component Optionclassification */
        /* @var $componentAttribute OptionClassificationAttribute */

        $component = $this->em->getRepository(Optionclassification::class)->findOneByIdentifier('main_color');
        self::assertNotNull($component);
        self::assertSame('3', $component->getSequencenumber());
        self::assertSame(true, $component->getHiddenInFrontend());

        $componentAttribute = $component->getOptionClassificationAttribute()->first();
        self::assertNotNull($componentAttribute);
        $attribute = $componentAttribute->getAttribute();
        self::assertSame('category_type', $attribute->getIdentifier());

        $attributeValue = $componentAttribute->getAttributevalue();
        self::assertSame('fabrics', $attributeValue->getValue());

        $component = $this->em->getRepository(Optionclassification::class)->findOneByIdentifier('collar_color');
        self::assertSame(false, $component->getHiddenInFrontend());
    }

    public function assertOptionImport(): void
    {
        /* @var $option Option */
        /* @var $optionAttribute OptionAttribute */

        $option = $this->em->getRepository(Option::class)->findOneByIdentifier('orange');
        self::assertNotNull($option);
        self::assertSame('1', $option->getSequencenumber());

        $optionAttribute = $option->getOptionAttribute()->first();
        self::assertNotNull($optionAttribute);
        $attribute = $optionAttribute->getAttribute();
        self::assertSame('color_filter', $attribute->getIdentifier());

        $attributeValue = $optionAttribute->getAttributevalue();
        self::assertSame('warm_colors', $attributeValue->getValue());
    }

    public function assertComponentRelations(): void
    {
        /* @var $item Item */
        /* @var $itemOptionClassification ItemOptionclassification */
        /* @var $itemOptionClassificationOption ItemOptionclassificationOption */

        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);

        $itemOptionClassifications = $item->getItemOptionclassification();
        self::assertNotEmpty($itemOptionClassifications);

        $itemOptionClassification = $itemOptionClassifications->first();
        self::assertTrue($itemOptionClassification->getIsMultiselect());
        self::assertEquals(3, $itemOptionClassification->getSequenceNumber());
        self::assertEquals(5, $itemOptionClassification->getMinamount());
        self::assertEquals(10, $itemOptionClassification->getMaxamount());
        self::assertEquals(false, $itemOptionClassification->getIsMandatory());
        self::assertNotEmpty($itemOptionClassification->getItemOptionclassificationOption());

        $itemOptionClassificationOption = $itemOptionClassification->getItemOptionclassificationOption()->first();
        self::assertNotNull($itemOptionClassificationOption->getOption());
        self::assertSame('red', $itemOptionClassificationOption->getOption()->getIdentifier());
        self::assertTrue($itemOptionClassificationOption->getAmountisselectable());
    }

    public function assertItemOptionclassificationOptionSequenceNumber(): void
    {
        $item = $this->em->getRepository(Item::class)->findOneByIdentifier('t_shirt');
        $optionClassification = $this->em->getRepository(Optionclassification::class)->findOneByIdentifier('main_color');
        $itemOptionClassification = $this->em->getRepository(ItemOptionclassification::class)
            ->findOneBy(['item' => $item, 'optionclassification' => $optionClassification]);

        $itemOptionClassificationOptions = $this->em->getRepository(ItemOptionclassificationOption::class)
            ->findBy(['itemOptionclassification' => $itemOptionClassification], ['sequenceNumber' => 'ASC']);

        self::assertEquals('blue', $itemOptionClassificationOptions[0]->getOption()->getIdentifier());
        self::assertEquals(0, $itemOptionClassificationOptions[0]->getSequenceNumber());
        self::assertEquals('red', $itemOptionClassificationOptions[1]->getOption()->getIdentifier());
        self::assertEquals(1, $itemOptionClassificationOptions[1]->getSequenceNumber());
        self::assertEquals('orange', $itemOptionClassificationOptions[2]->getOption()->getIdentifier());
        self::assertEquals(2, $itemOptionClassificationOptions[2]->getSequenceNumber());
    }

    public function assertDefaultOptionsSaved(): void
    {
        /* @var $configuration Configuration */
        $configuration = $this->em->getRepository(Configuration::class)->findOneByCode('TESTCODE1');
        self::assertSame(
            [
                'main_color' => [['identifier' => 'red', 'amount' => 1, 'inputText' => 'Just some text']],
                'collar_color' => [['identifier' => 'apple_red', 'amount' => 1]],
                'sleeve_color' => [['identifier' => 'red', 'amount' => 1]],
                'sleeve_type' => [['identifier' => 'longsleeve', 'amount' => 1]],
            ],
            $configuration->getSelectedoptions()
        );
        self::assertNull($configuration->getClient());
    }

    public function assertItemClassification(): void
    {
        $itemClassification = $this->em->getRepository(Itemclassification::class)->findOneByIdentifier('animal');
        self::assertNotNull($itemClassification);
        self::assertSame(1, $itemClassification->getSequencenumber());
    }

    public function assertItemClassificationRelations(): void
    {
        $item = $this->em->getRepository(Item::class)->findOneBy(['identifier' => 't_shirt']);
        $itemItemClassifacation = $item->getItemItemclassification();
        self::assertSame(2, count($itemItemClassifacation));
    }

    public function assertItemClassificationTexts(): void
    {
        $importedText = $this->em->getRepository(Itemclassificationtext::class)->findOneBy(['title' => 'mammal']);
        self::assertNotNull($importedText);
    }
}
