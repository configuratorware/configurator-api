<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use org\bovigo\vfs\vfsStream;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ImportFileStorageTest extends ApiTestCase
{
    /**
     * @var ImportFileStorage
     */
    private $importFileStorage;

    /**
     * @var string
     */
    private $root;

    /**
     * @var Filesystem
     */
    private $filesystem;

    private $oldRemovableFilepath;

    private $keepFilepath;
    private $movableFilepath1;
    private $movableFilepath2;

    protected function setUp(): void
    {
        parent::setUp();

        $this->filesystem = new Filesystem();
        $this->root = vfsStream::setup()->url() . '/importlog';

        $this->importFileStorage = new ImportFileStorage(
            $this->root,
            $this->filesystem
        );

        $this->fileSetup();
    }

    private function fileSetup()
    {
        $this->movableFilepath1 = $this->root . '/1/movable.xml';
        $this->filesystem->copy(__DIR__ . '/_data/validTestData.XML', $this->movableFilepath1);
        $this->filesystem->touch($this->movableFilepath1);

        $this->movableFilepath2 = $this->root . '/2/movable.xml';
        $this->filesystem->copy(__DIR__ . '/_data/validTestData.XML', $this->movableFilepath2);
        $this->filesystem->touch($this->movableFilepath2);

        $this->keepFilepath = $this->root . '/keep.json';
        $this->filesystem->copy(__DIR__ . '/_data/validTestData.json', $this->keepFilepath);
        $this->filesystem->touch($this->keepFilepath);

        $this->oldRemovableFilepath = $this->root . '/old.json';
        $this->filesystem->copy(__DIR__ . '/_data/validTestData.json', $this->oldRemovableFilepath, true);
        $this->filesystem->touch($this->oldRemovableFilepath, (new \DateTime('-2 weeks'))->getTimestamp());
    }

    public function testFileMoving()
    {
        self::assertFileExists($this->movableFilepath1);
        $this->importFileStorage->moveFileToLogDir($this->movableFilepath1);
        self::assertFileNotExists($this->movableFilepath1);
        self::assertFileExists($this->root . '/movable.xml');

        self::assertFileExists($this->movableFilepath2);
        $this->importFileStorage->moveFileToLogDir($this->movableFilepath2);
        self::assertFileNotExists($this->movableFilepath2);
        self::assertFileExists($this->root . '/movable_1.xml');
    }

    /**
     * @depends testFileMoving
     */
    public function testOldFileRemoval()
    {
        // more test files
        $this->importFileStorage->moveFileToLogDir($this->movableFilepath1);
        $this->importFileStorage->moveFileToLogDir($this->movableFilepath2);

        // remove old files
        $this->importFileStorage->removeOldImportFiles();

        self::assertFileNotExists($this->oldRemovableFilepath);

        $assertFilesExist = [
            $this->keepFilepath,
            $this->root . '/movable.xml',
            $this->root . '/movable_1.xml',
        ];
        foreach ($assertFilesExist as $assertFile) {
            self::assertFileExists($assertFile);
        }
    }

    public function testFileSaveFromUrl()
    {
        $contents = [];
        $contents[] = 'test file content 1';
        $contents[] = 'test file content 2';
        $contents[] = 'test file content 3';
        foreach ($contents as $content) {
            $this->importFileStorage->saveContentToLogDir($content);
        }

        $finder = new Finder();
        $finder->in($this->root)->files()->name('/^url_import_\d+(_\d+)?.log$/');
        self::assertTrue($finder->hasResults());
        self::assertEquals(count($contents), $finder->count());

        $iteration = 0;
        foreach ($finder as $file) {
            $filePath = $file->getPath() . '/' . $file->getFilename();
            self::assertFileExists($filePath);
            self::assertEquals($contents[$iteration++], file_get_contents($filePath));
        }
    }
}
