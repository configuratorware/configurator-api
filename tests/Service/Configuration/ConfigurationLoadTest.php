<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client as ClientDTO;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationImageProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DefaultConfigurationGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\FrontendConfigurationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\MinimumOrderAmount;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemAvailability;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item as FrontendItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification as OptionClassificationStructure;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\ComponentImageRepositoryMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\ItemImageRepositoryMock;

class ConfigurationLoadTest extends TestCase
{
    /**
     * @var ConfigurationRepository
     * @Mock
     */
    private $configurationRepository;

    /**
     * @var ConfigurationValidator
     * @Mock
     */
    private $configurationValidator;

    /**
     * @var DefaultConfigurationGenerator
     * @Mock
     */
    private $defaultConfigurationGenerator;

    /**
     * @var FrontendConfigurationStructureFromEntityConverter
     * @Mock
     */
    private $structureFromEntityConverter;

    /**
     * @var ItemAvailability
     * @Mock
     */
    private $itemAvailability;

    /**
     * @var ItemRepository
     * @Mock
     */
    private $itemRepository;

    /**
     * @var MinimumOrderAmount
     * @Mock
     */
    private $minimumOrderAmount;

    /**
     * @var SettingRepository
     * @Mock
     */
    private $settingRepository;

    /**
     * @var Client
     * @Mock
     */
    private $client;

    /**
     * @var ConfigurationLoad
     */
    private $configurationLoad;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $clientEntity = (new ClientEntity())->setIdentifier('_default')->setCallToAction('addToCart');
        $defaultClient = ClientDTO::fromEntity($clientEntity);

        \Phake::when($this->client)->getClient()->thenReturn($defaultClient);
        \Phake::when($this->settingRepository)->findAll()->thenReturn([(new Setting())->setConfigurationsClientRestricted(true)]);

        $this->configurationLoad = new ConfigurationLoad(
            $this->configurationRepository,
            $this->configurationValidator,
            $this->defaultConfigurationGenerator,
            $this->structureFromEntityConverter,
            $this->itemAvailability,
            new ConfigurationImageProvider(new ComponentImageRepositoryMock(), new ItemImageRepositoryMock(), new ItemImageRepositoryMock()),
            $this->itemRepository,
            $this->minimumOrderAmount,
            $this->settingRepository,
            $this->client
        );
    }

    /**
     * @param string|null $configurationCode
     * @param Configuration|null $configurationEntity
     * @param bool $availablity
     *
     * @dataProvider providerLoadByCodeShouldReturnNull
     */
    public function testLoadByCodeShouldReturnNull(string $configurationCode, ?Configuration $configurationEntity, bool $availablity): void
    {
        \Phake::when($this->configurationRepository)->findOneByCode($configurationCode)->thenReturn($configurationEntity);

        if (null !== $configurationEntity) {
            \Phake::when($this->itemAvailability)->checkAvailability($configurationEntity->getItem())->thenReturn($availablity);
        }

        self::assertNull($this->configurationLoad->loadByCode($configurationCode));

        if (null !== $configurationEntity && null !== $configurationEntity->getItem()) {
            \Phake::verify($this->itemAvailability)->checkAvailability;
        }
    }

    public function providerLoadByCodeShouldReturnNull(): array
    {
        return [
            [
                '',
                null,
                false,
            ],
            [
                'testCode',
                new Configuration(),
                false,
            ],
            [
                'testCode',
                (new Configuration())->setItem(new Item()),
                false,
            ],
            [
                'testCode',
                $this->getClientConfiguration(),
                true,
            ],
        ];
    }

    private function getClientConfiguration(): Configuration
    {
        $configuration = (new Configuration())->setItem(new Item());
        $client = (new ClientEntity())->setIdentifier('_not_default');
        $configuration->setClient($client);

        return $configuration;
    }

    public function testShouldReturnConfiguration(): void
    {
        $configurationCode = 'testCode';
        $this->specifyDefaultMockReturns($configurationCode);

        $result = $this->configurationLoad->loadByCode($configurationCode);

        \Phake::verify($this->structureFromEntityConverter)->convertOne;
        \Phake::verify($this->configurationValidator)->validateStructure;

        self::assertInstanceOf(FrontendConfigurationStructure::class, $result);
    }

    public function testShouldReturnConfigurationWithParentImages(): void
    {
        $configurationCode = 'testCode';
        $this->specifyDefaultMockReturns($configurationCode);

        $result = $this->configurationLoad->loadByCode($configurationCode);

        self::assertEquals('/images/item/image/softshell.png', $result->item->detailImage);
        self::assertEquals('/images/item/thumb/softshell.png', $result->item->thumbnail);
    }

    public function testShouldReturnConfigurationWithChildImages(): void
    {
        $configurationCode = 'testCode';
        $this->specifyDefaultMockReturns($configurationCode);

        $result = $this->configurationLoad->loadByCode($configurationCode);

        self::assertItemGroup($result->item->itemGroup);
    }

    public function testShouldReturnConfigurationWithComponentImage(): void
    {
        $configurationCode = 'testCode';
        $this->specifyDefaultMockReturns($configurationCode);

        $result = $this->configurationLoad->loadByCode($configurationCode);

        self::assertEquals('/components/optionClassification1.png', $result->optionclassifications[0]->thumbnail);
    }

    private function specifyDefaultMockReturns(string $configurationCode): void
    {
        $configurationEntity = (new Configuration())->setItem($this->generateItem(
            'softshell',
            ['softshell_designer_2d_red_m', 'softshell_designer_2d_red_l']
        ));
        \Phake::when($this->configurationRepository)->findOneByCode($configurationCode)->thenReturn($configurationEntity);
        \Phake::when($this->itemAvailability)->checkAvailability->thenReturn(true);

        $configurationStructure = new FrontendConfigurationStructure();
        $configurationStructure->item = new FrontendItemStructure();
        $configurationStructure->item->itemGroup = $this->generateGroup(
            'color',
            [
                $this->generateGroupItem(
                    'softshell_designer_2d_red_m',
                    $this->generateGroup('size',
                        [
                            $this->generateGroupItem('softshell_designer_2d_red_m', null),
                            $this->generateGroupItem('softshell_designer_2d_red_l', null),
                        ]
                    )
                ),
            ]
        );

        $optionClassification = (new OptionClassificationStructure());
        $optionClassification->identifier = 'optionClassification1';
        $configurationStructure->optionclassifications = [$optionClassification];

        \Phake::when($this->structureFromEntityConverter)->convertOne->thenReturn($configurationStructure);
        \Phake::when($this->configurationValidator)->validateStructure->thenReturn($configurationStructure);
    }

    private function assertItemGroup($group): void
    {
        if (is_object($group) && property_exists($group, 'children')) {
            foreach ($group->children as $child) {
                self::assertEquals('/images/item/image/' . $child->identifier . '.png', $child->detailImage);
                self::assertEquals('/images/item/thumb/' . $child->identifier . '.png', $child->thumbnail);
                if (property_exists($child, 'itemGroup')) {
                    self::assertItemGroup($child->itemGroup);
                }
            }
        }
    }

    private function generateItem(string $parentIdentifier, array $childIdentifiers): Item
    {
        $item = (new Item())->setIdentifier($parentIdentifier);

        foreach ($childIdentifiers as $childIdentifier) {
            $item->addItem((new Item())->setIdentifier($childIdentifier));
        }

        $itemOptionClassification = (new ItemOptionclassification())->setOptionclassification((new Optionclassification())->setIdentifier('optionClassification1'));

        $item->addItemOptionclassification($itemOptionClassification);

        return $item;
    }

    private function generateGroup(string $identifier, $children): \stdClass
    {
        $group = new \stdClass();
        $group->identifier = $identifier;
        $group->children = $children;

        return $group;
    }

    private function generateGroupItem(string $identifier, $group): \stdClass
    {
        $item = new \stdClass();
        $item->identifier = $identifier;
        $item->itemGroup = $group;

        return $item;
    }
}
