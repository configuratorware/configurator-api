<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CsvGeneratorTest extends ApiTestCase
{
    /**
     * @var CsvGenerator
     */
    private $csvGenerator;

    /**
     * @var string
     */
    private $projectDir;

    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/CsvGeneratorSetUp.sql'));

        $container = static::$kernel->getContainer();
        $this->csvGenerator = $container->get('Redhotmagma\ConfiguratorApiBundle\Service\Configuration\CsvGenerator');

        $this->projectDir = rtrim(static::$kernel->getProjectDir(), '/');
    }

    public function testShouldGenerateBulkNameCsv()
    {
        $fileResult = $this->csvGenerator->generateBulkNameCsv('bulk_name_test');

        self::assertInstanceOf(
            FileResult::class,
            $fileResult,
            'Bulk name CSV Generation did not succeed.'
        );

        self::assertFileEquals(
            __DIR__ . '/_data/DocumentGenerator_expected_bulkName_result.csv',
            $fileResult->filePath
        );
    }

    public function testShouldGenerateBulkNameCsvFromNestedNamelist()
    {
        $fileResult = $this->csvGenerator->generateBulkNameCsv('bulk_name_test_2');

        self::assertInstanceOf(
            FileResult::class,
            $fileResult,
            'Bulk name CSV Generation did not succeed.'
        );

        self::assertFileEquals(
            __DIR__ . '/_data/DocumentGenerator_expected_bulkName_result_2.csv',
            $fileResult->filePath
        );
    }
}
