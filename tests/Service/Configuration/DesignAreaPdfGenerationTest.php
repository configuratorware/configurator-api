<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Imagick;
use ImagickException;
use org\bovigo\vfs\vfsStream;
use Psr\Log\NullLogger;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignAreaPrintfilePathInterface;
use Redhotmagma\ConfiguratorApiBundle\Vector\LegacyInkscapeConverter;
use StdClass;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DesignAreaPdfGenerationTest extends ApiTestCase
{
    /**
     * Sample files to compare generated file contents to.
     *
     * @var string
     */
    private const FILE_SAMPLES_FOLDER = __DIR__ . '/DesignAreaPdfGenerator/expected/';

    /**
     * @var DesignAreaPdfGenerator
     */
    private $designAreaPdfGenerator;

    /**
     * {@inheritDoc}
     */
    public function setUp(): void
    {
        parent::setUp();
        vfsStream::setup();
        vfsStream::copyFromFileSystem(__DIR__ . '/DesignAreaPdfGenerator/svg');

        $fs = new Filesystem();
        $this->designAreaPdfGenerator = new DesignAreaPdfGenerator(
            new DesignAreaPrintfileMock(),
            $fs,
            new NullLogger(),
            new LegacyInkscapeConverter($fs),
            new PrintDesignDataProvider('')
        );
    }

    /**
     * @param array $designAreaData
     * @param       $expectedFileCount
     *
     * @dataProvider provideDesignAreaData
     *
     * @throws ImagickException
     */
    public function testDesignAreaPdfCreation(array $designAreaData, int $expectedFileCount): void
    {
        $result = $this->designAreaPdfGenerator->generateFiles($this->createMockConfiguration($designAreaData));

        self::assertCount($expectedFileCount, $result);

        foreach ($result as $file) {
            self::assertFileExists($file->getPath());
            self::assertTrue(
                $this->isVisuallySameImage(self::FILE_SAMPLES_FOLDER . $file->getName(), $file->getPath()),
                'The two image does not look the same'
            );
        }
    }

    /**
     * Check if there is no visual difference between two images.
     *
     * @param string $expectedFilePath
     * @param string $testFilePath
     *
     * @return bool
     *
     * @throws ImagickException
     */
    private function isVisuallySameImage(string $expectedFilePath, string $testFilePath): bool
    {
        $assertedImagick = new Imagick();
        $assertedImagick->readImageBlob(file_get_contents($expectedFilePath));
        $assertedImagick->resetIterator();
        $assertedImagick = $assertedImagick->appendImages(true);

        $testImagick = new Imagick();
        $testImagick->readImageBlob(file_get_contents($testFilePath));
        $testImagick->resetIterator();
        $testImagick = $testImagick->appendImages(true);

        return 0.0 === $assertedImagick->compareImages($testImagick, 1)[1];
    }

    /**
     * @return array
     */
    public function provideDesignAreaData(): array
    {
        return [
            [
                [
                    'with_text' => [
                        'width' => 40,
                        'height' => 7,
                        'varying_dimensions' => [
                            'width' => 0,
                            'height' => 0,
                        ],
                    ],
                ],
                1,
            ],
            [
                [
                    'auf_stirn' => ['width' => 50, 'height' => 100],
                ],
                1,
            ],
            [
                [
                    'auf_stirn' => ['width' => 50, 'height' => 100],
                    'linke_seite' => ['width' => 60, 'height' => 30],
                ],
                2,
            ],
            [
                [
                    'auf_stirn' => ['width' => 50, 'height' => 100],
                    'linke_seite' => ['width' => 60, 'height' => 30],
                    'rechte_seite' => ['width' => 80, 'height' => 40],
                ],
                2,
            ],
            [
                [
                    'rechte_seite' => ['width' => 80, 'height' => 40],
                ],
                0,
            ],
            [
                [],
                0,
            ],
        ];
    }

    /**
     * Init mock data for test runs.
     *
     * @param array $designAreaData
     *
     * @return Configuration
     */
    private function createMockConfiguration(array $designAreaData): Configuration
    {
        $configuration = new Configuration();
        $item = new Item();
        $designData = new StdClass();

        foreach ($designAreaData as $identifier => $size) {
            $designArea = new DesignArea();
            $designArea->setIdentifier($identifier);
            $designArea->setWidth($size['width']);
            $designArea->setHeight($size['height']);

            $designData->$identifier = new StdClass();
            $designData->$identifier->designProductionMethodIdentifier = 'test_production_method';
            if (isset($size['varying_dimensions'])) {
                $designProductionMethod = new DesignProductionMethod();
                $designProductionMethod->setIdentifier('test_production_method');
                $areaMethod = new DesignAreaDesignProductionMethod();
                $areaMethod->setDesignProductionMethod($designProductionMethod);
                $areaMethod->setWidth($size['varying_dimensions']['width']);
                $areaMethod->setHeight($size['varying_dimensions']['height']);
                $designArea->addDesignAreaDesignProductionMethod($areaMethod);
            }

            $item->addDesignArea($designArea);
        }

        $configuration->setCode('test_code');
        $configuration->setItem($item);
        $configuration->setDesigndata($designData);

        return $configuration;
    }
}

final class DesignAreaPrintfileMock implements DesignAreaPrintfilePathInterface
{
    public function getDesignAreaPrintfilePath(): string
    {
        return 'vfs://root';
    }
}
