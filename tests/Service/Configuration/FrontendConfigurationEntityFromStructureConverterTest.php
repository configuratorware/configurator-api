<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\FrontendConfigurationEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item as FrontendItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option as FrontendOptionStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification as FrontendOptionClassificationStructure;

class FrontendConfigurationEntityFromStructureConverterTest extends TestCase
{
    /**
     * @Mock
     *
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @Mock
     *
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @Mock
     *
     * @var ConfigurationtypeRepository
     */
    private $configurationTypeRepository;

    /**
     * @Mock
     *
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @Mock
     *
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var FrontendConfigurationEntityFromStructureConverter
     */
    private $frontendConfigurationEntityFromStructureConverter;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->frontendConfigurationEntityFromStructureConverter = new FrontendConfigurationEntityFromStructureConverter($this->channelRepository, $this->clientRepository, $this->configurationTypeRepository, $this->itemRepository, $this->languageRepository);
    }

    /**
     * @dataProvider providerShouldCheckGeneratedHash
     */
    public function testShouldCheckGeneratedHash(string $expectedPartslistHash, FrontendConfigurationStructure $configuration): void
    {
        $entity = $this->frontendConfigurationEntityFromStructureConverter->convertOne($configuration);

        self::assertSame($expectedPartslistHash, $entity->getPartslisthash());
    }

    public function providerShouldCheckGeneratedHash(): \Generator
    {
        yield ['c6a661e9047114b873cb5ceb16cd58758424a001', $this->generateConfiguration(['component_1', 'component_2'], ['option_1' => 1, 'option_2' => 2])];
        yield ['2c7f642f32249c970700739bfe9c1bb544ae380d', $this->generateConfiguration(['component_1'], ['option_1' => 1, 'option_2' => 1])];
    }

    /**
     * @return FrontendConfigurationStructure
     */
    private function generateConfiguration($components, $options): FrontendConfigurationStructure
    {
        $configuration = new FrontendConfigurationStructure();

        $item = new FrontendItemStructure();
        $item->identifier = 'test';
        $configuration->item = $item;
        $configuration->optionclassifications = [];

        foreach ($components as $componentIdentifier) {
            $component = new FrontendOptionClassificationStructure();
            $component->identifier = $componentIdentifier;
            $component->selectedoptions = [];

            foreach ($options as $optionIdentifier => $optionAmount) {
                $option = new FrontendOptionStructure();
                $option->identifier = $optionIdentifier;
                $option->amount = $optionAmount;
                $component->selectedoptions[] = $option;
            }

            $configuration->optionclassifications[] = $component;
        }

        return $configuration;
    }
}
