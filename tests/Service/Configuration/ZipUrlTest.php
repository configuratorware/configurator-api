<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ZipUrlResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ZipUrlTest extends ApiTestCase
{
    /**
     * @var ZipUrlResolver
     */
    private $zipUrl;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var UrlGenerator|\Phake_IMock
     * @Mock UrlGenerator
     */
    private $router;

    private const RETURN_STRING = 'generated zip url';

    protected function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);
        \Phake::when($this->router)->generate->thenReturn(self::RETURN_STRING);
        $this->languageRepository = $this->em->getRepository(Language::class);
        $this->zipUrl = new ZipUrlResolver($this->router, $this->languageRepository);
    }

    /**
     * @param array $controlParameters
     * @param string $code
     *
     * @dataProvider providerShareUrl
     */
    public function testShouldGetZipUrlFromControlParameters(array $controlParameters, string $code): void
    {
        $actual = $this->zipUrl->getProductionZipUrl($code, new ControlParameters($controlParameters));

        $expectedParameters = $controlParameters;
        $expectedParameters['configurationCode'] = $code;
        $expectedParameters['_language'] = 'en_GB';

        \Phake::verify($this->router)->generate('redhotmagma_configurator_frontend_configurations_zip', $expectedParameters, UrlGeneratorInterface::ABSOLUTE_URL);
        self::assertEquals(self::RETURN_STRING, $actual);
    }

    public function providerShareUrl(): array
    {
        return [
            [
                [
                    ControlParameters::SHARE_URL => 'www.redhotmagma.de?giveMeACode={code}&someParam=1&anotherParam=text',
                    ControlParameters::LANGUAGE => '_klingon',
                    ControlParameters::HIDE_PRICES => 1,
                ],
                '1337',
            ],
            [
                [
                    ControlParameters::CHANNEL => '_some_channel',
                    ControlParameters::CLIENT => '_client',
                ],
                '1337',
            ],
            [
                [],
                '1337',
            ],
        ];
    }
}
