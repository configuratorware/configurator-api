<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use org\bovigo\vfs\vfsStream;
use Phake;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\CsvGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ZipGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\ConfigurationDocumentApi;

final class ZipGeneratorTest extends TestCase
{
    public function setUp(): void
    {
        vfsStream::setup();
    }

    public function testShouldAddUserImageFromUploads(): void
    {
        $directory = vfsStream::create([
            'uploads' => [
                '3957dba72cc8ade9f5f91089ff7b7ce7' => [
                    'test.png' => 'I am test content.',
                ],
            ],
            'webDocumentRoot' => [
                'images' => [
                    'imagegallery' => [
                        'd3d9446802a44259755d38e6d163e820.svg' => 'I am test content.',
                    ],
                ],
            ],
        ]);

        $zipGenerator = $this->createZipGenerator($directory->url() . '/uploads/');

        $userImages = $zipGenerator->getUserImages('configuration-code');

        self::assertCount(2, $userImages);

        self::assertSame($userImages[0]->getPath(), 'vfs://root/uploads/3957dba72cc8ade9f5f91089ff7b7ce7/test.png');
        self::assertSame($userImages[0]->getName(), 'design_area_upload_0_test.png');

        self::assertSame($userImages[1]->getPath(), 'vfs://root/webDocumentRoot/images/imagegallery/d3d9446802a44259755d38e6d163e820.svg');
        self::assertSame($userImages[1]->getName(), 'design_area_image_gallery_1_d3d9446802a44259755d38e6d163e820.svg');
    }

    private function createZipGenerator(string $privateImagesPath): ZipGenerator
    {
        $configurationRepository = Phake::mock(ConfigurationRepository::class);
        Phake::when($configurationRepository)->findOneByCode('configuration-code')->thenReturn($this->generateConfiguration());

        return new ZipGenerator(
            $configurationRepository,
            Phake::mock(ConfigurationDocumentApi::class),
            Phake::mock(CsvGenerator::class),
            $privateImagesPath,
            ['url_path' => '/images/imagegallery'],
            'vfs://root/webDocumentRoot'
        );
    }

    private function generateConfiguration(): Configuration
    {
        $configuration = new Configuration();

        $configuration->setDesigndata($this->designData);

        return $configuration;
    }

    private string $designData = <<<JSON
{
  "design_area_upload": {
    "canvasData": {
      "objects": [
        {
          "type": "Image",
          "x": 266,
          "y": 83,
          "rotation": 0,
          "scaling": 0.08394678695088634,
          "uuid": "d72130fe-13e8-4bd0-a145-b6e6700e61b9",
          "src": "/img/uploads/designer/preview/3957dba72cc8ade9f5f91089ff7b7ce7.svg",
          "imageData": {
            "compatibilityInfoHash": {
              "colorPalettes": [
                "standard"
              ],
              "vectorsRequired": true,
              "maxColorAmount": "1"
            },
            "colorPreviewRequired": false,
            "operations": {
              "clipping": null,
              "clippingColor": null,
              "vectorize": true,
              "vectorizeThreshold": 1,
              "vectorizeColorsMap": {
                "#931638": {
                  "identifier": "{\"color\":\"hks_17\",\"palette\":\"standard\"}",
                  "value": "#a71f3c"
                }
              },
              "vectorizeMaxColorAmount": 16,
              "vectorizeOriginalColorsMap": {
                "#931638": {
                  "identifier": "{\"color\":\"hks_17\",\"palette\":\"standard\"}",
                  "value": "#a71f3c"
                },
                "#dd9cbe": {
                  "identifier": "{\"color\":\"hks_21\",\"palette\":\"standard\"}",
                  "value": "#ff8997"
                },
                "#b33c77": {
                  "identifier": "{\"color\":\"hks_29\",\"palette\":\"standard\"}",
                  "value": "#9b348e"
                }
              }
            },
            "identifier": "610cbe6a-caf1-45c5-b9d2-c16e08cba9de",
            "incomplete": false,
            "fileName": "3957dba72cc8ade9f5f91089ff7b7ce7",
            "originalFileName": "Logo_GF_rot.eps",
            "original": {
              "url": null,
              "format": "EPT",
              "mimeType": "image/x-ept",
              "width": 2048,
              "height": 1328,
              "additionalData": {}
            },
            "preview": {
              "url": "/img/uploads/designer/preview/3957dba72cc8ade9f5f91089ff7b7ce7.svg",
              "format": "SVG",
              "mimeType": "image/svg+xml",
              "width": 2048,
              "height": 1328,
              "additionalData": {
                "maxPotentialColorAmount": 16
              }
            },
            "thumbnail": {
              "url": "/img/uploads/designer/thumbnail/3957dba72cc8ade9f5f91089ff7b7ce7.png",
              "format": "PNG",
              "mimeType": "image/png",
              "width": 160,
              "height": 160,
              "additionalData": {},
              "isThumbnailBright": true
            },
            "message": null,
            "displayColorPreview": true,
            "imageColorsWereEdited": true,
            "colorPreviewEnabled": true
          },
          "metric": {
            "x": 22,
            "y": 7,
            "width": 14,
            "height": 9
          }
        }
      ],
      "size": {
        "width": 532,
        "height": 166,
        "dpi": 300
      }
    },
    "designProductionMethodIdentifier": "digitalprint",
    "colorAmount": 1
  },
  "design_area_image_gallery": {
    "canvasData": {
      "objects": [
        {
          "type": "Text"
        },
        {
          "type": "Image",
          "x": 2358.200131814677,
          "y": 1124.7240017754823,
          "rotation": 0,
          "scaling": 1,
          "uuid": "e1f5662e-faa6-4ecb-97ca-79c8b45c1768",
          "src": "/images/imagegallery/d3d9446802a44259755d38e6d163e820.svg",
          "imageData": {
            "fileName": "d3d9446802a44259755d38e6d163e820",
            "identifier": "a76cd067-12d4-4735-ac63-8fcd49c8978d",
            "originalFileName": "/images/imagegallery/d3d9446802a44259755d38e6d163e820.svg",
            "original": {
              "url": null,
              "format": "SVG",
              "mimeType": "image/svg+xml",
              "width": 290,
              "height": 290
            },
            "preview": {
              "url": "/images/imagegallery/d3d9446802a44259755d38e6d163e820.svg",
              "format": "SVG",
              "mimeType": "image/svg+xml",
              "width": 290,
              "height": 290
            },
            "thumbnail": {
              "url": "/images/imagegallery/d3d9446802a44259755d38e6d163e820_t.png",
              "format": "PNG",
              "mimeType": "image/png",
              "width": 160,
              "height": 160
            },
            "message": null,
            "gallery": true,
            "isPlaceHolderImage": false
          }
        }
      ],
      "size": {
        "width": 3308,
        "height": 1536,
        "dpi": 300
      }
    },
    "designProductionMethodIdentifier": "digitalprint",
    "colorAmount": 2
  },
  "design_area_placeholder": {
    "canvasData": {
      "objects": [
        {
          "type": "Text"
        },
        {
          "type": "Image",
          "x": 35.5,
          "y": 71,
          "rotation": 0,
          "scaling": 1.4791666666666667,
          "uuid": "b8c4d333-87fa-4b12-b732-9ba54112684e",
          "src": "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0OCIgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4Ij48cGF0aCBkPSJNNDIgMzhWMTBjMC0yLjIxLTEuNzktNC00LTRIMTBjLTIuMjEgMC00IDEuNzktNCA0djI4YzAgMi4yMSAxLjc5IDQgNCA0aDI4YzIuMjEgMCA0LTEuNzkgNC00ek0xNyAyN2w1IDYuMDFMMjkgMjRsOSAxMkgxMGw3LTl6Ii8+PC9zdmc+",
          "imageData": {
            "fileName": "designer_placeholder_logo.svg",
            "identifier": "124ef622-f87a-470a-9069-6b866457935d",
            "originalFileName": "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0OCIgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4Ij48cGF0aCBkPSJNNDIgMzhWMTBjMC0yLjIxLTEuNzktNC00LTRIMTBjLTIuMjEgMC00IDEuNzktNCA0djI4YzAgMi4yMSAxLjc5IDQgNCA0aDI4YzIuMjEgMCA0LTEuNzkgNC00ek0xNyAyN2w1IDYuMDFMMjkgMjRsOSAxMkgxMGw3LTl6Ii8+PC9zdmc+",
            "original": {
              "url": null,
              "format": "SVG",
              "mimeType": "image/svg+xml",
              "width": 290,
              "height": 290
            },
            "preview": {
              "url": "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0OCIgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4Ij48cGF0aCBkPSJNNDIgMzhWMTBjMC0yLjIxLTEuNzktNC00LTRIMTBjLTIuMjEgMC00IDEuNzktNCA0djI4YzAgMi4yMSAxLjc5IDQgNCA0aDI4YzIuMjEgMCA0LTEuNzkgNC00ek0xNyAyN2w1IDYuMDFMMjkgMjRsOSAxMkgxMGw3LTl6Ii8+PC9zdmc+",
              "format": "SVG",
              "mimeType": "image/svg+xml",
              "width": 290,
              "height": 290
            },
            "thumbnail": {
              "url": "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI0OCIgaGVpZ2h0PSI0OCIgdmlld0JveD0iMCAwIDQ4IDQ4Ij48cGF0aCBkPSJNNDIgMzhWMTBjMC0yLjIxLTEuNzktNC00LTRIMTBjLTIuMjEgMC00IDEuNzktNCA0djI4YzAgMi4yMSAxLjc5IDQgNCA0aDI4YzIuMjEgMCA0LTEuNzkgNC00ek0xNyAyN2w1IDYuMDFMMjkgMjRsOSAxMkgxMGw3LTl6Ii8+PC9zdmc+",
              "format": "PNG",
              "mimeType": "image/png",
              "width": 160,
              "height": 160
            },
            "message": null,
            "gallery": false,
            "isPlaceHolderImage": true,
            "operations": {}
          },
          "metric": {
            "x": 22,
            "y": 6,
            "width": 6,
            "height": 6
          }
        }
      ],
      "size": {
        "width": 532,
        "height": 142,
        "dpi": 300
      }
    },
    "designProductionMethodIdentifier": "digitalprint",
    "colorAmount": 1
  }
}
JSON;
}
