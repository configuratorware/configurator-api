<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

class MinimumOrderAmountTest extends TestCase
{
    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var MinimumOrderAmount
     */
    private $minimumOrderAmount;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->minimumOrderAmount = new MinimumOrderAmount($this->itemRepository);
    }

    /**
     * @param string $itemIdentifier
     * @param array $itemData
     * @param array $expectedSelectedAmounts
     *
     * @dataProvider providerShouldPreFillSelectedAmountsWithoutAccumulation
     * @dataProvider providerShouldPreFillSelectedAmountsWhenAccumulating
     */
    public function testShouldPreFillSelectedAmounts(
        string $itemIdentifier,
        array $itemData,
        array $expectedSelectedAmounts
    ) {
        foreach ($itemData as $identifier => $properties) {
            $item = \Phake::mock(Item::class);
            \Phake::when($item)->getIdentifier()->thenReturn($identifier);
            \Phake::when($item)->getMinimumOrderAmount()->thenReturn($properties['minimumOrderAmount']);
            \Phake::when($item)->getAccumulateAmounts()->thenReturn($properties['accumulateAmounts']);

            if ('pen' === $identifier) {
                \Phake::when($item)->getParent()->thenReturn(null);
            } else {
                \Phake::when($item)->getParent()->thenReturn($this->itemRepository->findOneByIdentifier('pen'));
            }

            \Phake::when($this->itemRepository)->findOneByIdentifier($identifier)->thenReturn($item);
        }

        $configuration = new Configuration();
        $configuration->item = new \stdClass();
        $configuration->item->identifier = $itemIdentifier;

        if (count($itemData) > 1) {
            $configuration->item->itemGroup = new \stdClass();
            $configuration->item->itemGroup->children = [];
            $configuration->item->itemGroup->children[0] = new \stdClass();
            $configuration->item->itemGroup->children[0]->identifier = 'pen_red';
        }

        $configuration = $this->minimumOrderAmount->addSelectedAmountsToStructure($configuration, $itemIdentifier);

        self::assertEquals(
            json_encode($expectedSelectedAmounts),
            json_encode($configuration->customdata->selectedAmounts)
        );
    }

    public function providerShouldPreFillSelectedAmountsWithoutAccumulation(): array
    {
        return [
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 1],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen_blue',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 5, 'accumulateAmounts' => false],
                ],
                ['pen_blue' => 5],
            ],
            [
                'pen_red',
                [
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 12, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 12],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 1],
            ],
        ];
    }

    public function providerShouldPreFillSelectedAmountsWhenAccumulating(): array
    {
        return [
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 1],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 18, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 12, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 13, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 18],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 7, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 8],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 7, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => 12, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 12],
            ],
            [
                'pen',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 1],
            ],

            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 1],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 18, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 12, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 13, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 18],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 7, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 8],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 7, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => true],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 20, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 20],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => true],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => 10, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 10],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => 12, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 12],
            ],
            [
                'pen_red',
                [
                    'pen' => ['minimumOrderAmount' => null, 'accumulateAmounts' => true],
                    'pen_red' => ['minimumOrderAmount' => null, 'accumulateAmounts' => false],
                    'pen_blue' => ['minimumOrderAmount' => 8, 'accumulateAmounts' => false],
                ],
                ['pen_red' => 1],
            ],
        ];
    }
}
