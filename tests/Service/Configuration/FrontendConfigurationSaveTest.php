<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeLength;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationScreenshot;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\FrontendConfigurationEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\FrontendConfigurationSave;
use Redhotmagma\ConfiguratorApiBundle\Service\PrintFile\PrintFileSave;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSaveData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

class FrontendConfigurationSaveTest extends TestCase
{
    use EntityTestTrait;

    /**
     * @Mock
     *
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @Mock
     *
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @Mock
     *
     * @var ConfigurationCodeGenerator
     */
    private $configurationCodeGenerator;

    /**
     * @Mock
     *
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @Mock
     *
     * @var ConfigurationtypeRepository
     */
    private $configurationTypeRepository;

    /**
     * @Mock
     *
     * @var ConfigurationScreenshot
     */
    private $configurationScreenshot;

    /**
     * @Mock
     *
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @Mock
     *
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @Mock
     *
     * @var PrintFileSave
     */
    private $printFileSave;

    /**
     * @var FrontendConfigurationSave
     */
    private $frontendConfigurationSave;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $configurationCodeLength = new ConfigurationCodeLength([]);
        $frontendConfigurationEntityFromStructureConverter = new FrontendConfigurationEntityFromStructureConverter($this->channelRepository, $this->clientRepository, $this->configurationTypeRepository, $this->itemRepository, $this->languageRepository);
        $this->frontendConfigurationSave = new FrontendConfigurationSave($configurationCodeLength, $this->configurationCodeGenerator, $this->configurationRepository, $this->configurationScreenshot, $this->configurationTypeRepository, $frontendConfigurationEntityFromStructureConverter, $this->itemRepository, $this->printFileSave);

        \Phake::when($this->configurationCodeGenerator)->generateCode->thenReturn('GENERATED_CODE');

        \Phake::when($this->configurationTypeRepository)->findOneByIdentifier(ConfigurationtypeRepository::PARTSLIST)->thenReturn((new Configurationtype())->setIdentifier(ConfigurationtypeRepository::PARTSLIST));
        \Phake::when($this->configurationTypeRepository)->findOneByIdentifier(ConfigurationtypeRepository::DEFAULT_OPTIONS)->thenReturn((new Configurationtype())->setIdentifier(ConfigurationtypeRepository::DEFAULT_OPTIONS));
        \Phake::when($this->configurationTypeRepository)->findOneByIdentifier(ConfigurationtypeRepository::CART)->thenReturn((new Configurationtype())->setIdentifier(ConfigurationtypeRepository::CART));

        \Phake::when($this->clientRepository)->findOneByIdentifier('custom_client')->thenReturn($this->createEntityWithId(Client::class, 3));
        \Phake::when($this->channelRepository)->findOneByIdentifier('custom_channel')->thenReturn($this->createEntityWithId(Channel::class, 2));
        \Phake::when($this->languageRepository)->findOneByIso('fr_FR')->thenReturn($this->createEntityWithId(Language::class, 4));
    }

    public function testShouldSaveCustomerInfoToConfiguration(): void
    {
        $configurationSaveData = $this->generateConfigurationSaveData('cart');

        $entity = $this->frontendConfigurationSave->save($configurationSaveData);

        self::assertSame(3, $entity->getClient()->getId());
        self::assertSame(2, $entity->getChannel()->getId());
        self::assertSame(4, $entity->getLanguage()->getId());
    }

    public function testShouldNotSaveCustomerInfoToConfiguration(): void
    {
        $configurationSaveData = $this->generateConfigurationSaveData(ConfigurationtypeRepository::DEFAULT_OPTIONS);

        $entity = $this->frontendConfigurationSave->save($configurationSaveData);

        self::assertNull($entity->getClient());
        self::assertNull($entity->getChannel());
        self::assertNull($entity->getLanguage());
    }

    public function testShouldNotChangeCustomerInfo(): void
    {
        $configurationEntity = new Configuration();
        $configurationEntity->setCode('GIVEN_CODE');
        $configurationEntity->setChannel((new Channel())->setIdentifier('keep_me'));
        $configurationEntity->setClient((new Client())->setIdentifier('keep_me'));
        $configurationEntity->setLanguage((new Language())->setIso('keep_me'));

        \Phake::when($this->configurationRepository)->findOneByCode('GIVEN_CODE')->thenReturn($configurationEntity);

        $configurationSaveData = $this->generateConfigurationSaveData('cart', 'GIVEN_CODE');

        $entity = $this->frontendConfigurationSave->save($configurationSaveData);

        self::assertSame('keep_me', $entity->getClient()->getIdentifier());
        self::assertSame('keep_me', $entity->getChannel()->getIdentifier());
        self::assertSame('keep_me', $entity->getLanguage()->getIso());
    }

    public function testShouldSaveCalculationResult(): void
    {
        $configurationSaveData = $this->generateConfigurationSaveData('cart', 'GIVEN_CODE');

        $entity = $this->frontendConfigurationSave->save($configurationSaveData, new CalculationResult());

        self::assertSame('{"total":0,"totalFormatted":"","netTotal":0,"netTotalFormatted":"","netTotalFromGrossTotal":0,"netTotalFromGrossTotalFormatted":"","vatRate":0,"positions":[],"bulkSavings":[]}', $entity->getCalculationResult());
    }

    /**
     * @param string $configurationType
     * @param string|null $code
     *
     * @return ConfigurationSaveData
     */
    private function generateConfigurationSaveData(string $configurationType, string $code = null): ConfigurationSaveData
    {
        $configurationSaveData = new ConfigurationSaveData();

        $configuration = new FrontendConfigurationStructure();

        $item = new Item();
        $item->identifier = 'test';

        $configuration->item = $item;

        $configuration->code = $code;
        $configuration->configurationType = $configurationType;
        $configuration->channel = 'custom_channel';
        $configuration->client = 'custom_client';
        $configuration->language = 'fr_FR';
        $configuration->optionclassifications = [];
        $configuration->_metadata = new \stdClass();
        $configuration->_metadata->changedProperties = [];

        $configurationSaveData->configurationType = $configurationType;
        $configurationSaveData->configuration = $configuration;

        return $configurationSaveData;
    }

    private function createEntityWithId(string $className, int $id): object
    {
        $entity = new $className();
        $this->setPrivateId($entity, $id);

        return $entity;
    }
}
