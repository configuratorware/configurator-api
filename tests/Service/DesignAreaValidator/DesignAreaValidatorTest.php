<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DesignAreaValidator;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerProductionCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignAreaValidator;

class DesignAreaValidatorTest extends TestCase
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var DesignAreaValidator
     */
    private $designAreaValidator;

    /**
     * @var StructureValidator|\Phake_IMock
     * @Mock StructureValidator
     */
    private $structureValidator;

    /**
     * @var DesignAreaRepository|\Phake_IMock
     * @Mock DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var DesignerProductionCalculationTypeRepository|\Phake_IMock
     * @Mock DesignerProductionCalculationTypeRepository
     */
    private $designerProductionCalculationTypeRepository;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        \Phake::when($this->structureValidator)->validate->thenReturn([]);
        \phake::when($this->designerProductionCalculationTypeRepository)->getByDesignProductionMethod->thenReturn([]);

        $this->structureFromDataConverter = new StructureFromDataConverter();
        $this->designAreaValidator = new DesignAreaValidator($this->structureValidator, $this->designAreaRepository, $this->designerProductionCalculationTypeRepository);
    }

    public function testBulkPriceDuplicationViolation(): void
    {
        $data = json_decode($this->getBulkPriceDuplicationTestData());
        $structure = $this->structureFromDataConverter->convert($data, DesignAreaStructure::class);

        try {
            $this->designAreaValidator->validate($structure);
        } catch (ValidationException $exception) {
        }

        self::assertNotNull($exception);
        self::assertSame(
            [
                [
                    'property' => 'designProductionMethod[0].calculationTypes[0].prices[1]',
                    'invalidvalue' => '{"channel":1,"colorAmountFrom":"1","amountFrom":"1"}',
                    'message' => 'duplicate_bulk_price',
                ],
            ],
            $exception->getViolations()
        );
    }

    private function getBulkPriceDuplicationTestData(): string
    {
        return <<<JSON
{
    "id": 1,
    "item": {
        "id": 1,
        "identifier": "sheep",
        "title": "Sheep"
    },
    "identifier": "front",
    "texts": [
        {
            "id": 1,
            "title": "Clip en changed",
            "language": "en_GB"
        },
        {
            "id":2,
            "title": "Clip de changed",
            "language": "de_DE"
        }
    ],
    "height": 20,
    "width": 30,
    "designProductionMethods": [
        {
            "id": 1,
            "identifier": "screen_printing",
            "title": "Screen Printing",
            "mask": null,
            "height": 0,
            "width": 0,
            "isDefault": true,
            "options": {
                "minFontSize": 4,
                "maxColorAmount": 4,
                "visualizationEffect": null,
                "hasEngravingBackgroundColors": false,
                "vectorsRequired": true
            },
            "minimumOrderAmount": 1,
            "calculationTypes": [
                {
                    "id": 0,
                    "identifier": "printing_costs_screen_printing",
                    "colorAmountDependent": false,
                    "itemAmountDependent": true,
                    "selectableByUser": false,
                    "title": "",
                    "prices": [
                        {
                            "id": null,
                            "channel": "1",
                            "colorAmountFrom": 1,
                            "amountFrom": 1,
                            "price": "0.25000",
                            "priceNet": "0.00000"
                        },
                        {
                            "id": null,
                            "channel": 1,
                            "colorAmountFrom": "1",
                            "amountFrom": "1",
                            "price": "1",
                            "priceNet": "1"
                        },
                        {
                            "id": null,
                            "channel": 1,
                            "colorAmountFrom": null,
                            "amountFrom": null,
                            "price": null,
                            "priceNet": null
                        }
                    ],
                    "bulkNameDependent": false
                }
            ],
            "customData": null,
            "allowBulkNames": false,
            "additionalData": null,
            "defaultColors": null
        }
    ]
}
JSON;
    }
}
