<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOptionDeltaprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionDeltapriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern\ConstructionPatternSave;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConstructionPattern as ConstructionPatternStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item as ItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableComponent;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOption;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOptionDeltaPrice;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ConstructionPatternSaveTest extends ApiTestCase
{
    /**
     * @var ConstructionPatternSave
     */
    private $constructionPatternSave;

    /**
     * @var ItemOptionclassificationRepository
     */
    private $itemOptionClassificationRepository;

    /**
     * @var ItemOptionclassificationOptionRepository
     */
    private $itemOptionClassificationOptionRepository;

    /**
     * @var ItemOptionclassificationOptionDeltapriceRepository
     */
    private $itemOptionClassificationOptionDeltaPriceRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->itemOptionClassificationRepository = $this->em->getRepository(ItemOptionclassification::class);
        $this->itemOptionClassificationOptionRepository = $this->em->getRepository(ItemOptionclassificationOption::class);
        $this->itemOptionClassificationOptionDeltaPriceRepository = $this->em->getRepository(ItemOptionclassificationOptionDeltaprice::class);

        $channelRepository = $this->em->getRepository(Channel::class);
        $itemRepository = $this->em->getRepository(Item::class);
        $optionRepository = $this->em->getRepository(Option::class);
        $optionClassificationRepository = $this->em->getRepository(Optionclassification::class);
        $creatorViewRepository = $this->em->getRepository(CreatorView::class);

        $this->constructionPatternSave = new ConstructionPatternSave($channelRepository, $itemRepository, $optionRepository, $optionClassificationRepository, $creatorViewRepository);
    }

    /**
     * @param array $givenPatternState
     *
     * @dataProvider providerShouldCreate
     */
    public function testShouldCreate(array $givenPatternState): void
    {
        $this->executeSql($this->itemSQL);

        $this->constructionPatternSave->save($this->generateConstructionPattern($givenPatternState));

        $itemComponents = $this->itemOptionClassificationRepository->findBy(['item' => 2003]);
        foreach ($itemComponents as $component) {
            self::assertNotNull($component->getCreatorView());
        }
        self::assertCount(2, $itemComponents);
        self::assertCount(2, $this->itemOptionClassificationOptionRepository->findBy(['option' => 2001]));
        self::assertNotNull($this->itemOptionClassificationOptionRepository->findBy(['option' => 2002]));
    }

    public function providerShouldCreate(): \Generator
    {
        yield [
            [
                'item' => 2003,
                'components' => [
                    [
                        'id' => 2001,
                        'creatorViewId' => 3003,
                        'options' => [
                            [
                                'id' => 2001,
                                'prices' => [],
                            ],
                            [
                                'id' => 2002,
                                'prices' => [],
                            ],
                        ],
                    ],
                    [
                        'id' => 2002,
                        'creatorViewId' => 3002,
                        'options' => [
                            [
                                'id' => 2001,
                                'prices' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param array $givenPatternState
     *
     * @dataProvider providerShouldDelete
     */
    public function testShouldDelete(array $givenPatternState): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/../../Controller/_data/Item/softshell_creator_2d.sql'));

        $this->constructionPatternSave->save($this->generateConstructionPattern($givenPatternState));

        $itemComponents = $this->itemOptionClassificationRepository->findBy(['item' => 2003]);
        foreach ($itemComponents as $component) {
            self::assertNull($component->getCreatorView());
        }
        self::assertCount(1, $itemComponents);
        self::assertCount(1, $this->itemOptionClassificationOptionRepository->findBy(['itemOptionclassification' => 2001]));
        self::assertCount(0, $this->itemOptionClassificationOptionRepository->findBy(['itemOptionclassification' => 2002]));
        self::assertCount(0, $this->itemOptionClassificationOptionRepository->findBy(['itemOptionclassification' => 2003]));
    }

    public function providerShouldDelete(): \Generator
    {
        yield [
            [
                'item' => 2003,
                'components' => [
                    [
                        'id' => 2001,
                        'options' => [
                            [
                                'id' => 2001,
                                'prices' => [],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param array $givenPatternState
     *
     * @dataProvider providerShouldCreateDeltaPrice
     */
    public function testShouldCreateDeltaPrice(array $givenPatternState): void
    {
        $this->executeSql($this->itemSQL);

        $this->constructionPatternSave->save($this->generateConstructionPattern($givenPatternState));

        $itemOptionClassificationOption = $this->itemOptionClassificationOptionRepository->findOneBy(['option' => 2001]);

        self::assertCount(1, $itemOptionClassificationOption->getItemOptionclassificationOptionDeltaprice());
    }

    public function providerShouldCreateDeltaPrice(): \Generator
    {
        yield [
            [
                'item' => 2003,
                'components' => [
                    [
                        'id' => 2001,
                        'options' => [
                            [
                                'id' => 2001,
                                'prices' => [
                                    [
                                        'channel' => 1,
                                        'price' => 11.90,
                                        'priceNet' => 10.00,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param array $givenPatternState
     *
     * @dataProvider providerShouldDeleteDeltaPrices
     */
    public function testShouldDeleteDeltaPrices(array $givenPatternState): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/../../Controller/_data/Item/softshell_creator_2d.sql'));

        $this->constructionPatternSave->save($this->generateConstructionPattern($givenPatternState));

        self::assertNull($this->itemOptionClassificationOptionDeltaPriceRepository->find(2000));

        self::assertNotNull($this->itemOptionClassificationOptionDeltaPriceRepository->find(2001));
    }

    public function providerShouldDeleteDeltaPrices(): \Generator
    {
        yield [
            [
                'item' => 2003,
                'components' => [
                    [
                        'id' => 2001,
                        'options' => [
                            [
                                'id' => 2001,
                                'prices' => [],
                            ],
                        ],
                    ],
                    [
                        'id' => 2002,
                        'options' => [
                            [
                                'id' => 2002,
                                'prices' => [
                                    [
                                        'id' => 2002,
                                        'channel' => 1,
                                        'price' => 11.90,
                                        'priceNet' => 10.00,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    private function generateConstructionPattern(array $state): ConstructionPatternStructure
    {
        $constructionPattern = new ConstructionPatternStructure();
        $constructionPattern->item = new ItemStructure();
        $constructionPattern->item->id = $state['item'];

        foreach ($state['components'] as $component) {
            $selectableOptions = [];
            foreach ($component['options'] as $option) {
                $deltaPrices = [];
                foreach ($option['prices'] as $price) {
                    $deltaPrices[] = new SelectableOptionDeltaPrice($price['id'] ?? null, $price['price'], $price['priceNet'], $price['channel']);
                }

                $selectableOptions[] = new SelectableOption($option['id'], null, null, null, false, null, $deltaPrices);
            }

            $constructionPattern->selectableComponents[] = new SelectableComponent($component['id'], null, null, null, $selectableOptions, true, false, $component['creatorViewId'] ?? null);
        }

        return $constructionPattern;
    }

    private $itemSQL = <<<'SQL'
INSERT IGNORE INTO item (id, identifier,  minimum_order_amount, accumulate_amounts, date_created, date_updated, date_deleted, visualization_mode_id, item_status_id, configuration_mode, overwrite_component_order)
VALUES (2003,  'softshell_creator_2d', 1, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1, 2, 'creator', 0);

INSERT IGNORE INTO optionclassification (id, identifier, sequencenumber, date_created, date_updated, date_deleted, hidden_in_frontend)
VALUES (2001,  'softshell_body', 1, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (2002, 'softshell_zipper', 3, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (2003, 'softshell_application', 4, NOW(), NOW(), '0001-01-01 00:00:00', 0);

INSERT IGNORE INTO `option` (id, parent_id, sequencenumber, identifier, date_created, date_updated, date_deleted)
VALUES (2001, null, 1, 'softshell_red', NOW(), NOW(), '0001-01-01 00:00:00'),
       (2002, null, 2, 'softshell_avocado', NOW(), NOW(), '0001-01-01 00:00:00'),
       (2007, null, 1, 'softshell_redblack', NOW(), NOW(), '0001-01-01 00:00:00'),
       (2008, null, 2, 'softshell_whitegreen', NOW(), NOW(), '0001-01-01 00:00:00');
       
INSERT IGNORE INTO `creator_view` (`id`, `item_id`, `identifier`, `sequence_number`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3002, 2003, 'front_view', 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3003, 2003, 'side_view', 2, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');
SQL;
}
