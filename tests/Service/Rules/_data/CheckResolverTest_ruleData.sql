INSERT IGNORE INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id,
                        user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (1, '{"maxamount":5,"optionclassification_identifier":"face"}', NULL, NULL, '2017-06-21 10:39:46', NULL,
        '0001-01-01 00:00:00', NULL, NULL, NULL, 4, 1, 1, null),
       (2, '{"minamount":2,"optionclassification_identifier":"face"}', NULL, NULL, '2017-06-21 10:39:55', NULL,
        '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 1, 1, null),
       (6, '{"option_identifier":"face_nature"}', NULL, 1, '2017-06-23 08:15:01', NULL, '0001-01-01 00:00:00', NULL,
        NULL, NULL, 1, 1, 1, 1),
       (7, '{"option_identifier":"face_brown"}', NULL, 1, '2017-06-23 08:15:01', NULL, '0001-01-01 00:00:00', NULL,
        NULL, NULL, 1, 1, 1, 1),
       (8, '{"option_identifier":"face_brown"}', NULL, 1, '2017-06-23 08:15:01', NULL, '0001-01-01 00:00:00', NULL,
        NULL, NULL, 2, 1, 1, 5),
       (9, '{"option_identifier":"face_nature"}', NULL, 1, '2017-06-23 08:15:01', NULL, '0001-01-01 00:00:00', NULL,
        NULL, NULL, 2, 1, 1, 5);

#### Attributes and rule for attributematch test
INSERT IGNORE INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id,
                        user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (10, '{"attribute_identifier":"color"}', NULL, NULL, '2017-10-06 10:57:09', NULL, '0001-01-01 00:00:00', 1, NULL,
        NULL, 5, 1, 1, NULL);

INSERT IGNORE INTO attribute(id, identifier, externalid, attributedatatype, date_created, date_updated, date_deleted,
                             user_created_id, user_updated_id, user_deleted_id)
VALUES (103, 'color', NULL, 'string', '2017-10-06 11:01:19', NULL, '0001-01-01 00:00:00', 0, NULL, NULL);

INSERT IGNORE INTO attributevalue(id, value, date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                  user_deleted_id)
VALUES (104, 'black', '2017-10-06 11:01:55', NULL, '0001-01-01 00:00:00', 0, NULL, NULL),
       (105, 'white', '2017-10-06 11:02:02', NULL, '0001-01-01 00:00:00', 0, NULL, NULL);


INSERT IGNORE INTO attributetext(id, title, date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                 user_deleted_id, attribute_id, language_id)
VALUES (106, 'Farbe', '2017-10-06 11:01:19', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 1);

INSERT IGNORE INTO option_attribute(date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                    user_deleted_id, attribute_id, attributevalue_id, option_id)
VALUES ('2017-10-06 11:05:15', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 105, 1), # coat_nature, white
       ('2017-10-06 11:05:28', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 105, 2), # coat_white, white
       ('2017-10-06 11:05:44', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 105, 4), # face_nature, white
       ('2017-10-06 11:05:53', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 105, 5), # face_white, white
       ('2017-10-06 11:06:11', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 104, 3), # coat_red, black
       ('2017-10-06 11:06:21', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 104, 6); # face_brown, black

#### Attributes for attributematch test with multiple attribute values
INSERT IGNORE INTO option_attribute(date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                    user_deleted_id, attribute_id, attributevalue_id, option_id)
VALUES ('2017-10-06 11:06:21', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 104, 12), # eyes_red, black
       ('2017-10-06 11:06:21', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 103, 105, 12); # eyes_red, white

#### Attributes and rule for attributemaxsum test
INSERT IGNORE INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id,
                        user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (11, '{"attribute_identifier":"menge", "maxamount":100}', NULL, NULL, NOW(), NULL, '0001-01-01 00:00:00', 1,
        NULL, NULL, 6, 1, 1, NULL);

INSERT IGNORE INTO attribute(id, identifier, externalid, attributedatatype, date_created, date_updated, date_deleted,
                             user_created_id, user_updated_id, user_deleted_id)
VALUES (4, 'menge', NULL, 'number', NOW(), NULL, '0001-01-01 00:00:00', 0, NULL, NULL);

INSERT IGNORE INTO attributevalue(id, value, date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                  user_deleted_id)
VALUES (6, 100, NOW(), NULL, '0001-01-01 00:00:00', 0, NULL, NULL);

INSERT IGNORE INTO option_attribute(date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                    user_deleted_id, attribute_id, attributevalue_id, option_id)
VALUES (NOW(), NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 4, 6, 3),
       (NOW(), NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 4, 6, 12),
       (NOW(), NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 4, 6, 15);

INSERT INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id,
                 user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (12, '{"maxamount":20}', 'error', NULL, '2017-10-23 17:45:33', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 7,
        1, NULL, 13);


#### defaultoption rule
INSERT INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id,
                 user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (13, '{"option_identifier":"ear_eyelid_white"}', NULL, NULL, '2018-02-14 09:10:46', NULL, '0001-01-01 00:00:00',
        NULL, NULL, NULL, 8, 2, 1, NULL);

#### data for additional option selection test
#### add a category accessories, with an option ribbon
INSERT INTO optionclassification (id, identifier, sequencenumber, date_created)
VALUES (10010, 'accessories', 100, NOW());
INSERT INTO item_optionclassification (id, item_id, optionclassification_id, date_created)
VALUES (10010, 1, 10010, NOW());
INSERT INTO `option` (id, identifier, date_created)
VALUES (10010, 'ribbon', NOW());
INSERT INTO item_optionclassification_option (item_optionclassification_id, option_id, date_created)
VALUES (10010, 10010, NOW());

#### rule for eyes blue: it should have the ribbon as an additional option
INSERT INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id,
                 user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (14, '{"option_identifier":"ribbon"}', NULL, NULL, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 9, 2, 1,
        11);


#### AttributeValueGroupAutoSwitch
INSERT INTO item(id, identifier, externalid, configurable, deactivated, date_created, date_updated, date_deleted,
                 user_created_id, user_updated_id, user_deleted_id, parent_id)
VALUES (2017, 'rack', NULL, 1, 0, '2018-06-04 15:04:57', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL);

INSERT INTO `option`(id, identifier, externalid, deactivated, date_created, date_updated, date_deleted, user_created_id,
                     user_updated_id, user_deleted_id, parent_id)
VALUES (2016, 'board_short_default', NULL, 0, '2018-06-04 15:07:45', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL),
       (2017, 'board_short_premium', NULL, 0, '2018-06-04 15:08:24', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL),
       (2018, 'board_long_default', NULL, 0, '2018-06-04 15:09:37', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL),
       (2019, 'board_long_premium', NULL, 0, '2018-06-04 15:10:08', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL);

INSERT INTO itemclassification(id, parent_id, identifier, sequencenumber, date_created, date_updated, date_deleted,
                               user_created_id, user_updated_id, user_deleted_id)
VALUES (207, NULL, 'section1', 1, '2018-06-04 15:05:22', NULL, '0001-01-01 00:00:00', 0, NULL, NULL),
       (208, NULL, 'section2', 2, '2018-06-04 15:05:37', NULL, '0001-01-01 00:00:00', 0, NULL, NULL);

INSERT INTO optionclassification(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                 user_deleted_id)
VALUES (206, '2018-06-04 15:05:22', NULL, '0001-01-01 00:00:00', 0, NULL, NULL),
       (207, '2018-06-04 15:05:38', NULL, '0001-01-01 00:00:00', 0, NULL, NULL);

INSERT INTO item_optionclassification(id, is_multiselect, minamount, maxamount, is_mandatory, date_created,
                                      date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id,
                                      item_id, optionclassification_id)
VALUES (206, 1, NULL, NULL, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, 0, NULL, 2017, 206),
       (207, 1, NULL, NULL, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, 0, NULL, 2017, 207);


INSERT INTO item_optionclassification_option(id, amountisselectable, date_created, date_updated, date_deleted,
                                             user_created_id, user_updated_id, user_deleted_id,
                                             item_optionclassification_id, option_id)
VALUES (2016, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 206, 2016),
       (2017, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 206, 2017),
       (2018, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 206, 2018),
       (2019, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 206, 2019),
       (2020, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 207, 2016),
       (2021, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 207, 2017),
       (2022, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 207, 2018),
       (2023, 0, '2018-06-04 15:14:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 207, 2019);


INSERT INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id,
                 user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (201, '{"attribute_identifier":"productline","apply_per_option_classification":false}', NULL, NULL,
        '2018-06-05 13:50:14', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 10, 2, 2017, NULL);

INSERT INTO configuration(id, externaluser_id, code, name, selectedoptions, designdata, customdata, partslisthash,
                          date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id,
                          configurationtype_id, item_id)
VALUES (202, NULL, 'defaultoptions_rack', 'defaultoptions_rack',
        '{"section1":[{"identifier":"board_short_default","amount":1},{"identifier":"board_long_default","amount":3}],"section2":[{"identifier":"board_short_default","amount":3},{"identifier":"board_long_default","amount":1}]}',
        NULL, NULL, NULL, '2018-06-04 15:23:49', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2017);

INSERT INTO attribute(id, identifier, externalid, attributedatatype, is_compatibility_attribute, hide_in_frontend,
                      date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (202, 'productline', NULL, 'string', NULL, NULL, '2018-06-04 15:06:30', NULL, '0001-01-01 00:00:00', 0, NULL,
        NULL);

INSERT INTO attributevalue(id, value, date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                           user_deleted_id)
VALUES (203, 'default', '2018-06-04 15:06:49', NULL, '0001-01-01 00:00:00', 0, NULL, NULL),
       (204, 'premium', '2018-06-04 15:07:04', NULL, '0001-01-01 00:00:00', 0, NULL, NULL);

INSERT INTO option_attribute(id, sequencenumber, date_created, date_updated, date_deleted, user_created_id,
                             user_updated_id, user_deleted_id, attribute_id, attributevalue_id, option_id)
VALUES (203, NULL, '2018-06-04 15:08:24', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 202, 203, 2016),
       (204, NULL, '2018-06-04 15:09:03', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 202, 204, 2017),
       (205, NULL, '2018-06-04 15:09:37', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 202, 203, 2018),
       (206, NULL, '2018-06-04 15:10:08', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 202, 204, 2019);

INSERT INTO option_pool(id, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                        user_deleted_id)
VALUES (201, 'board_short', '2018-06-05 09:45:25', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
       (202, 'board_long', '2018-06-05 09:45:37', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO option_option_pool(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                               user_deleted_id, option_id, option_pool_id)
VALUES (201, '2018-06-05 09:45:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2016, 201),
       (202, '2018-06-05 09:45:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2017, 201),
       (203, '2018-06-05 09:46:01', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2018, 202),
       (204, '2018-06-05 09:46:08', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2019, 202);
