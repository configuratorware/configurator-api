<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Rules;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\EventListener\Rules\RunChecksForRulesListener;
use Redhotmagma\ConfiguratorApiBundle\Events\Rules\RunChecksForRulesEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\ActionResolver;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\RulesRetriever;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use redhotmagma\SymfonyTestUtils\Fixture\FixturesIO;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CheckResolverTest extends ApiTestCase
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var CheckFactory
     */
    private $checkFactory;

    /**
     * @var ActionResolver
     */
    private $actionResolver;

    /**
     * @var RulesRetriever
     */
    private $rulesRetriever;

    /**
     * @var RuleRepository
     */
    private $ruleRepository;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    protected function setUp(): void
    {
        parent::setUp();

        if (!defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', 'en_GB');
        }
        if (!defined('C_CHANNEL')) {
            define('C_CHANNEL', '_default');
        }
        $channelSettings = [
            'currencySymbol' => '€',
        ];

        if (!defined('C_CHANNEL_SETTINGS')) {
            define('C_CHANNEL_SETTINGS', $channelSettings);
        }
        if (!defined('C_LANGUAGE_SETTINGS')) {
            define(
                'C_LANGUAGE_SETTINGS',
                [
                    'id' => 1,
                    'iso' => 'en_GB',
                    'dateformat' => 'm/d/Y',
                    'pricedecimals' => 2,
                    'decimalpoint' => '.',
                    'thousandsseparator' => ',',
                    'currencysymbolposition' => 'left',
                ]
            );
        }
        if (false === defined('CALCULATION_PRICES_NET')) {
            define('CALCULATION_PRICES_NET', false);
        }

        $container = static::$kernel->getContainer();

        $this->checkFactory = $container->get('Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckFactory');
        $this->actionResolver = $container->get('Redhotmagma\ConfiguratorApiBundle\Service\Rules\ActionResolver');
        $this->itemRepository = $this->em->getRepository(Item::class);
        $this->optionRepository = $this->em->getRepository(Option::class);
        $this->ruleRepository = $this->em->getRepository(Rule::class);
        $this->rulesRetriever = $container->get('Redhotmagma\ConfiguratorApiBundle\Service\Rules\RulesRetriever');
        $this->configurationRepository = $this->em->getRepository(\Redhotmagma\ConfiguratorApiBundle\Entity\Configuration::class);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/CheckResolverTest_ruleData.sql'));

        // undelete options
        $this->executeSql("UPDATE item_optionclassification io set io.date_deleted = '0001-01-01 00:00:00' WHERE io.id = 5;
UPDATE item_optionclassification_option ioo set ioo.date_deleted = '0001-01-01 00:00:00' WHERE ioo.item_optionclassification_id = 5;");
    }

    public function testItemCheckShouldReturnCheckResults(): void
    {
        $item = $this->itemRepository->findOneById(1);

        $converter = new StructureFromDataConverter();
        $configurationData = json_decode('{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();
        self::assertInstanceOf('Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResults', $checkResults);
    }

    public function testGlobalCheckShouldReturnCheckResults(): void
    {
        $converter = new StructureFromDataConverter();
        $configurationData = json_decode('{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->ruleRepository->getSortedGlobalRules();

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertInstanceOf('Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResults', $checkResults);
    }

    public function testItemdependencyTestShouldPass(): void
    {
        $item = $this->optionRepository->findOneById(1);

        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(true, $checkResults->check_results[0]->status);
    }

    public function testItemdependencyTestShouldFail(): void
    {
        $item = $this->optionRepository->findOneById(1);

        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_white","title":"{face_white}","abstract":"{face_nature}","description":"{face_white}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(false, $checkResults->check_results[1]->status);
    }

    public function testItemexclusionTestShouldPass(): void
    {
        $item = $this->optionRepository->findOneById(1);
        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(true, $checkResults->check_results[0]->status);
    }

    public function testItemexclusionTestShouldFail(): void
    {
        $item = $this->optionRepository->findOneById(5);
        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(false, $checkResults->check_results[1]->status);
    }

    public function testItemattributematchTestShouldPass(): void
    {
        $item = $this->itemRepository->findOneById(1);
        $converter = new StructureFromDataConverter();

        $configurationData = FixturesIO::json($this)->read('testItemattributematchTestShouldPass.configurationData');
        $configuration = $converter->convert(json_decode($configurationData), Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(true, $checkResults->check_results[2]->status);
    }

    public function testItemattributematchTestShouldFail(): void
    {
        $item = $this->itemRepository->findOneById(1);
        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","shareUrl":null,"name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":null,"deliverytime":"","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","imageIdentifier":"coat_nature","title":"{coat_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"12.45","priceFormatted":"12,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"coat","title":"Fell","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_brown","imageIdentifier":"face_nature","title":"{face_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"15.45","priceFormatted":"15,45 €","stock":{},"amount":2,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"face","title":"Gesicht","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","imageIdentifier":"legs_nature","title":"{legs_nature}","abstract":"","description":"","attributes":[],"price":"18.45","priceFormatted":"18,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"legs","title":"Beine","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","imageIdentifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"","description":"","attributes":[],"price":"22.45","priceFormatted":"22,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"eyes","title":"Augen","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","imageIdentifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"","description":"","attributes":[],"price":"25.45","priceFormatted":"25,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"","attributes":[]}],"visualizationData":null,"designdata":[],"information":{"channelSettings":{"currencySymbol":"€","channel":"_default"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"},"settings":{"visualizationcomponent":"layer","visualizationsettings":null,"calculationmethod":"sumofall","shareurl":null,"defaultmailsenderaddress":null},"codeSnippets":[{"code":"<!-- This is a code snippet -->"}],"validation_result":{"success":true,"data":{"coat":{"selectedoptions":true,"options":[{"option":"coat_nature","valid":true}]},"face":{"selectedoptions":true,"options":[{"option":"face_nature","valid":true}]},"legs":{"selectedoptions":true,"options":[{"option":"legs_nature","valid":true}]},"eyes":{"selectedoptions":true,"options":[{"option":"eyes_yellow","valid":true}]},"ear_eyelid":{"selectedoptions":true,"options":[{"option":"ear_eyelid_nature","valid":true}]}}}},"customdata":[],"previewImageURL":"//localhost:10030/images/configurations/defaultoptions_1.png","date_created":{"date":"2017-10-05 15:59:16.000000","timezone_type":3,"timezone":"Europe/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(false, $checkResults->check_results[1]->status);
        self::assertEquals('coat_nature', $checkResults->check_results[1]->conflicting_options[0]->identifier);
        self::assertEquals('face_brown', $checkResults->check_results[1]->conflicting_options[1]->identifier);
    }

    public function testItemattributematchTestMessageShouldContainTitle(): void
    {
        $expectedTitle = 'Farbe';
        $item = $this->itemRepository->findOneById(1);
        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","shareUrl":null,"name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":null,"deliverytime":"","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","imageIdentifier":"coat_nature","title":"{coat_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"12.45","priceFormatted":"12,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"coat","title":"Fell","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_brown","imageIdentifier":"face_nature","title":"{face_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"15.45","priceFormatted":"15,45 €","stock":{},"amount":2,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"face","title":"Gesicht","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","imageIdentifier":"legs_nature","title":"{legs_nature}","abstract":"","description":"","attributes":[],"price":"18.45","priceFormatted":"18,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"legs","title":"Beine","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","imageIdentifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"","description":"","attributes":[],"price":"22.45","priceFormatted":"22,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"eyes","title":"Augen","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","imageIdentifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"","description":"","attributes":[],"price":"25.45","priceFormatted":"25,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"","attributes":[]}],"visualizationData":null,"designdata":[],"information":{"channelSettings":{"currencySymbol":"€","channel":"_default"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"},"settings":{"visualizationcomponent":"layer","visualizationsettings":null,"calculationmethod":"sumofall","shareurl":null,"defaultmailsenderaddress":null},"codeSnippets":[{"code":"<!-- This is a code snippet -->"}],"validation_result":{"success":true,"data":{"coat":{"selectedoptions":true,"options":[{"option":"coat_nature","valid":true}]},"face":{"selectedoptions":true,"options":[{"option":"face_nature","valid":true}]},"legs":{"selectedoptions":true,"options":[{"option":"legs_nature","valid":true}]},"eyes":{"selectedoptions":true,"options":[{"option":"eyes_yellow","valid":true}]},"ear_eyelid":{"selectedoptions":true,"options":[{"option":"ear_eyelid_nature","valid":true}]}}}},"customdata":[],"previewImageURL":"//localhost:10030/images/configurations/defaultoptions_1.png","date_created":{"date":"2017-10-05 15:59:16.000000","timezone_type":3,"timezone":"Europe/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertStringContainsString($expectedTitle, $checkResults->check_results[1]->message);
    }

    public function testItemattributemaxsumTestShouldPass(): void
    {
        $item = $this->itemRepository->findOneById(1);
        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","shareUrl":null,"name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":null,"deliverytime":"","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","imageIdentifier":"coat_nature","title":"{coat_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"12.45","priceFormatted":"12,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"coat","title":"Fell","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","imageIdentifier":"face_nature","title":"{face_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"15.45","priceFormatted":"15,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"face","title":"Gesicht","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","imageIdentifier":"legs_nature","title":"{legs_nature}","abstract":"","description":"","attributes":[],"price":"18.45","priceFormatted":"18,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"legs","title":"Beine","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","imageIdentifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"","description":"","attributes":[],"price":"22.45","priceFormatted":"22,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"eyes","title":"Augen","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","imageIdentifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"","description":"","attributes":[],"price":"25.45","priceFormatted":"25,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"","attributes":[]}],"visualizationData":null,"designdata":[],"information":{"channelSettings":{"currencySymbol":"€","channel":"_default"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"},"settings":{"visualizationcomponent":"layer","visualizationsettings":null,"calculationmethod":"sumofall","shareurl":null,"defaultmailsenderaddress":null},"codeSnippets":[{"code":"<!-- This is a code snippet -->"}],"validation_result":{"success":true,"data":{"coat":{"selectedoptions":true,"options":[{"option":"coat_nature","valid":true}]},"face":{"selectedoptions":true,"options":[{"option":"face_nature","valid":true}]},"legs":{"selectedoptions":true,"options":[{"option":"legs_nature","valid":true}]},"eyes":{"selectedoptions":true,"options":[{"option":"eyes_yellow","valid":true}]},"ear_eyelid":{"selectedoptions":true,"options":[{"option":"ear_eyelid_nature","valid":true}]}}}},"customdata":[],"previewImageURL":"//localhost:10030/images/configurations/defaultoptions_1.png","date_created":{"date":"2017-10-05 15:59:16.000000","timezone_type":3,"timezone":"Europe/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(true, $checkResults->check_results[3]->status);
    }

    public function testItemattributemaxsumTestShouldFail(): void
    {
        $item = $this->itemRepository->findOneById(1);
        $converter = new StructureFromDataConverter();

        $configurationData = json_decode('{"code":"defaultoptions_1","shareUrl":null,"name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":null,"deliverytime":"","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_red","imageIdentifier":"coat_nature","title":"{coat_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"12.45","priceFormatted":"12,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"coat","title":"Fell","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","imageIdentifier":"face_nature","title":"{face_nature}","abstract":"","description":"","attributes":[{"identifier":"color","title":"{color}","attributedatatype":"string","values":["white"]}],"price":"15.45","priceFormatted":"15,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"face","title":"Gesicht","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","imageIdentifier":"legs_nature","title":"{legs_nature}","abstract":"","description":"","attributes":[],"price":"18.45","priceFormatted":"18,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"legs","title":"Beine","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_red","imageIdentifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"","description":"","attributes":[],"price":"22.45","priceFormatted":"22,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"eyes","title":"Augen","description":"","attributes":[]},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","imageIdentifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"","description":"","attributes":[],"price":"25.45","priceFormatted":"25,45 €","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null}],"optionsCount":"3","identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"","attributes":[]}],"visualizationData":null,"designdata":[],"information":{"channelSettings":{"currencySymbol":"€","channel":"_default"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"},"settings":{"visualizationcomponent":"layer","visualizationsettings":null,"calculationmethod":"sumofall","shareurl":null,"defaultmailsenderaddress":null},"codeSnippets":[{"code":"<!-- This is a code snippet -->"}],"validation_result":{"success":true,"data":{"coat":{"selectedoptions":true,"options":[{"option":"coat_nature","valid":true}]},"face":{"selectedoptions":true,"options":[{"option":"face_nature","valid":true}]},"legs":{"selectedoptions":true,"options":[{"option":"legs_nature","valid":true}]},"eyes":{"selectedoptions":true,"options":[{"option":"eyes_yellow","valid":true}]},"ear_eyelid":{"selectedoptions":true,"options":[{"option":"ear_eyelid_nature","valid":true}]}}}},"customdata":[],"previewImageURL":"//localhost:10030/images/configurations/defaultoptions_1.png","date_created":{"date":"2017-10-05 15:59:16.000000","timezone_type":3,"timezone":"Europe/Paris"}}');
        $configuration = $converter->convert($configurationData, Configuration::class);

        $rules = $this->rulesRetriever->getFilteredRules($item, $configuration);

        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
        $listener->onRunChecksForRules($event);
        $checkResults = $event->getCheckResults();

        self::assertEquals(false, $checkResults->check_results[2]->status);
    }

    public function testSwitchOptionsCheckShouldFail(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedOptions":{"face":{"identifier":"face_white","title":"{face_white}","abstract":"{face_white}","description":"{face_white}","attributes":[],"price":"17.45","stock":{},"amount":1,"amount_is_selectable":null}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals(false, $receivedResult->check_results->status, 'content mismatch');
    }

    public function testCannotSwitchOptionsWithAttributeMismatch(): void
    {
        $postdata = FixturesIO::json($this)->read('testCannotSwitchOptionsWithAttributeMismatch.configurationData');
        $expectedRuleResult = FixturesIO::json($this)->read('testCannotSwitchOptionsWithAttributeMismatch.expected');

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());
        $ruleResult = $this->findCheckResult($receivedResult->check_results, 'itemattributematch');

        self::assertEquals(false, $receivedResult->check_results->status, 'unexpected check status');
        self::assertNotNull($ruleResult, 'itemattributematch rule result not found');
        self::assertJsonStringEqualsJsonString($expectedRuleResult, json_encode($ruleResult));
    }

    public function testOptionsFrontendListChecksHasFailedCheck(): void
    {
        $postdata = FixturesIO::json($this)->read('testOptionsFrontendListChecksHasFailedCheck.configurationData');

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/options/list/sheep/face', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals('face_brown', $receivedResult[2]->check_results->check_results[1]->checked_option->identifier);
        self::assertEquals(false, $receivedResult[1]->check_results->status, 'content mismatch');
    }

    public function testSwitchOptionsMinamountCheckShouldFail(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedOptions":{"face":{"identifier":"face_brown","title":"{face_brown}","abstract":"{face_brown}","description":"{face_white}","attributes":[],"price":"17.45","stock":{},"amount":1,"amount_is_selectable":null}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals(false, $receivedResult->check_results->status, 'content mismatch');
    }

    public function testSwitchOptionsMaxamountCheckShouldFail(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedOptions":{"face":{"identifier":"face_brown","title":"{face_brown}","abstract":"{face_brown}","description":"{face_white}","attributes":[],"price":"17.45","stock":{},"amount":10,"amount_is_selectable":null}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals(false, $receivedResult->check_results->status, 'content mismatch');
    }

    public function testSwitchOptionsOptionMaxamountCheckShouldFail(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedOptions":{"ear_eyelid":{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":"17.45","stock":{},"amount":23,"amount_is_selectable":null}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals(false, $receivedResult->check_results->status, 'content mismatch');
    }

    public function testSwitchOptionsAutoSelectDefaultOption(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedOptions":{"face":{"identifier":"face_nature","amount":2}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals('ear_eyelid_white',
            $receivedResult->optionclassifications[4]->selectedoptions[0]->identifier, 'default option not selected');
    }

    public function testSwitchOptionsAddAdditionalOption(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":2,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"},{"is_multiselect":false,"selectedoptions":null,"identifier":"accessories","title":"Zubehör","description":""}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedOptions":{"eyes":{"identifier":"eyes_blue","amount":1}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals('ribbon',
            $receivedResult->optionclassifications[5]->selectedoptions[0]->identifier,
            'additional option not selected');
    }

    public function testSwitchOptionsRemoveAdditionalOption(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","shareUrl":null,"name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"identifier":"tags","title":null,"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}],"_metadata":{"changedProperties":["identifier","attributedatatype","values"]}}],"price":null,"netPrice":null,"stock":{"status":null,"deliverytime":null,"traffic_light":null,"id":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}]},"_metadata":{"changedProperties":["identifier","title","abstract","description","attributes","price","stock"]}},"optionclassifications":[{"is_multiselect":false,"is_mandatory":null,"selectedoptions":[{"identifier":"coat_nature","imageIdentifier":null,"title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"priceFormatted":null,"stock":{"status":null,"deliverytime":null,"traffic_light":null,"id":[]},"amount":1,"amount_is_selectable":null,"detailImageExists":null,"_metadata":{"changedProperties":["identifier","title","abstract","description","attributes","price","stock","amount","amount_is_selectable"]}}],"optionsCount":null,"identifier":"coat","title":"Fell","description":"{coat}","attributes":null,"_metadata":{"changedProperties":["is_multiselect","selectedoptions","identifier","title","description"]}},{"is_multiselect":false,"is_mandatory":null,"selectedoptions":[{"identifier":"face_nature","imageIdentifier":null,"title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"priceFormatted":null,"stock":{"status":null,"deliverytime":null,"traffic_light":null,"id":[]},"amount":2,"amount_is_selectable":null,"detailImageExists":null,"_metadata":{"changedProperties":["identifier","title","abstract","description","attributes","price","stock","amount","amount_is_selectable"]}}],"optionsCount":null,"identifier":"face","title":"Gesicht","description":"{face}","attributes":null,"_metadata":{"changedProperties":["is_multiselect","selectedoptions","identifier","title","description"]}},{"is_multiselect":false,"is_mandatory":null,"selectedoptions":[{"identifier":"legs_nature","imageIdentifier":null,"title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"priceFormatted":null,"stock":{"status":null,"deliverytime":null,"traffic_light":null,"id":[]},"amount":1,"amount_is_selectable":null,"detailImageExists":null,"_metadata":{"changedProperties":["identifier","title","abstract","description","attributes","price","stock","amount","amount_is_selectable"]}}],"optionsCount":null,"identifier":"legs","title":"Beine","description":"{legs}","attributes":null,"_metadata":{"changedProperties":["is_multiselect","selectedoptions","identifier","title","description"]}},{"is_multiselect":false,"is_mandatory":null,"selectedoptions":[{"identifier":"eyes_blue","imageIdentifier":"eyes_blue","title":"{eyes_blue}","abstract":"","description":"","attributes":[],"price":23.45,"priceFormatted":"23,45 \u20ac","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null,"netPrice":null}],"optionsCount":null,"identifier":"eyes","title":"Augen","description":"{eyes}","attributes":null,"_metadata":{"changedProperties":["is_multiselect","selectedoptions","identifier","title","description"]}},{"is_multiselect":false,"is_mandatory":null,"selectedoptions":[{"identifier":"ear_eyelid_white","imageIdentifier":"ear_eyelid_white","title":"{ear_eyelid_white}","abstract":"","description":"","attributes":[],"price":26.45,"priceFormatted":"26,45 \u20ac","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null,"netPrice":null}],"optionsCount":null,"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}","attributes":null,"_metadata":{"changedProperties":["is_multiselect","selectedoptions","identifier","title","description"]}},{"is_multiselect":false,"is_mandatory":null,"selectedoptions":[{"identifier":"ribbon","imageIdentifier":"ribbon","title":"{ribbon}","abstract":"","description":"","attributes":[],"price":0,"priceFormatted":"0,00 \u20ac","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null,"netPrice":null}],"optionsCount":null,"identifier":"accessories","title":"Zubeh\u00f6r","description":"","attributes":null,"_metadata":{"changedProperties":["is_multiselect","selectedoptions","identifier","title","description"]}}],"visualizationData":null,"designdata":null,"information":{"channelSettings":{"currencySymbol":"\u20ac"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"customdata":null,"partslisthash":null,"previewImageURL":null,"date_created":{"_metadata":{"changedProperties":[]},"date":"2018-02-16 13:52:43.000000","timezone_type":3,"timezone":"Europe\/Paris"},"_metadata":{"changedProperties":["code","name","item","optionclassifications","visualizationData","information","date_created"]},"check_results":{"status":true,"check_results":[{"status":true,"rule_type":"defaultoption","checked_option":null,"conflicting_options":null,"message":null,"data":{"switchOptions":{"ear_eyelid":[{"identifier":"ear_eyelid_white","amount":1}]}}}]}},"switchedOptions":{"eyes":{"identifier":"eyes_yellow","amount":1}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEmpty($receivedResult->optionclassifications[5]->selectedoptions,
            'additional option still selected');
    }

    public function testSwitchOptionsShouldHandleMultipleSwitchedOptionsForSameComponent(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","description":"{face_white}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedOptions":{"face":[{"identifier":"face_nature","amount":2},{"identifier":"face_white","amount":2}]}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        $checkResults = $receivedResult->check_results->check_results;
        $latest = array_pop($checkResults);

        self::assertEquals(false, $latest->status);
        self::assertEquals('face_white', $latest->checked_option->identifier);
        self::assertEquals('face_nature', $latest->conflicting_options[0]->identifier);
    }

    public function testSwitchOptionsAttributeValueGroupAutoSwitchSuccess(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_rack","shareUrl":null,"name":"defaultoptions_rack","item":{"identifier":"rack","title":"{rack}","abstract":"","description":"","attributes":[],"price":null,"netPrice":null,"stock":[],"prices":[],"itemclassifications":[]},"optionclassifications":[{"is_multiselect":true,"is_mandatory":false,"selectedoptions":[{"identifier":"board_short_default","imageIdentifier":"board_short_default","title":"{board_short_default}","abstract":"","description":"","attributes":[{"identifier":"productline","title":"{productline}","attributedatatype":"string","values":[{"value":"default","translated_value":"default"}]}],"price":0,"priceFormatted":"€ 0.00","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null,"netPrice":null},{"identifier":"board_long_default","imageIdentifier":"board_long_default","title":"{board_long_default}","abstract":"","description":"","attributes":[{"identifier":"productline","title":"{productline}","attributedatatype":"string","values":[{"value":"default","translated_value":"default"}]}],"price":0,"priceFormatted":"€ 0.00","stock":{},"amount":3,"amount_is_selectable":false,"detailImageExists":null,"netPrice":null}],"optionsCount":"4","identifier":"section1","title":"{section1}","description":"","attributes":[]},{"is_multiselect":true,"is_mandatory":false,"selectedoptions":[{"identifier":"board_short_default","imageIdentifier":"board_short_default","title":"{board_short_default}","abstract":"","description":"","attributes":[{"identifier":"productline","title":"{productline}","attributedatatype":"string","values":[{"value":"default","translated_value":"default"}]}],"price":0,"priceFormatted":"€ 0.00","stock":{},"amount":3,"amount_is_selectable":false,"detailImageExists":null,"netPrice":null},{"identifier":"board_long_default","imageIdentifier":"board_long_default","title":"{board_long_default}","abstract":"","description":"","attributes":[{"identifier":"productline","title":"{productline}","attributedatatype":"string","values":[{"value":"default","translated_value":"default"}]}],"price":0,"priceFormatted":"€ 0.00","stock":{},"amount":1,"amount_is_selectable":false,"detailImageExists":null,"netPrice":null}],"optionsCount":"4","identifier":"section2","title":"{section2}","description":"","attributes":[]}],"visualizationData":null,"designdata":null,"information":{"channelSettings":{"currencySymbol":"€","channel":"_default"},"languageSettings":{"id":"1","iso":"en_GB","dateformat":"m/d/Y","pricedecimals":2,"decimalpoint":".","thousandsseparator":",","currencysymbolposition":"left","user_created_id":"0"},"settings":{"visualizationcomponent":"layer","visualizationsettings":null,"calculationmethod":"sumofall","shareurl":null,"defaultmailsenderaddress":null,"customsettings":null},"codeSnippets":[{"code":"<!-- This is a code snippet -->"}],"validation_result":{"success":true,"data":{"section1":{"selectedoptions":true,"options":[{"option":"board_short_default","valid":true,"message":""},{"option":"board_long_default","valid":true,"message":""}]},"section2":{"selectedoptions":true,"options":[{"option":"board_short_default","valid":true,"message":""},{"option":"board_long_default","valid":true,"message":""}]}}}},"customdata":null,"partslisthash":null,"previewImageURL":"//localhost:10030/images/configurations/defaultoptions_rack.png","configurationType":"defaultoptions","date_created":{"date":"2018-06-04 15:23:49.000000","timezone_type":3,"timezone":"Europe/Paris"},"images":[]},"switchedOptions":{"section1":{"identifier":"board_long_premium","amount":3}}}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [],
            ['CONTENT_TYPE' => 'application/json'], $postdata);

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        foreach ($receivedResult->optionclassifications as $optionClassification) {
            foreach ($optionClassification->selectedoptions as $selectedOption) {
                self::assertStringEndsWith('premium', $selectedOption->identifier, 'premium option not selected');
            }
        }
    }

    private function findCheckResult(\stdClass $checkResults, string $ruleTypeIdentifier): ?\stdClass
    {
        $foundCheckResult = null;

        if (isset($checkResults->check_results) && is_array($checkResults->check_results)) {
            foreach ($checkResults->check_results as $checkResult) {
                if (isset($checkResult->rule_type) && $checkResult->rule_type === $ruleTypeIdentifier) {
                    $foundCheckResult = $checkResult;
                }
            }
        }

        return $foundCheckResult;
    }
}
