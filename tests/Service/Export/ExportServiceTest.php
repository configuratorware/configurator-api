<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Export;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Phake;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\Export\ExportItemService;
use Redhotmagma\ConfiguratorApiBundle\Service\Export\ExportService;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class ExportServiceTest extends TestCase
{
    /**
     * @var ExportItemService|\Phake_IMock
     * @Mock ExportItemService
     */
    private $exportItemService;
    private Filesystem $filesystem;
    private ExportService $exportService;
    private vfsStreamDirectory $root;
    private string $outputFolder;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->root = vfsStream::setup('root');
        $this->filesystem = new Filesystem();
        $this->exportService = new ExportService(vfsStream::url('root/export_temp'), $this->exportItemService);
        $this->outputFolder = vfsStream::url('root/export_temp/export');
        $this->filesystem->mkdir($this->outputFolder);
    }

    public function testExportWithoutCombine()
    {
        $itemIdentifiers = ['item1', 'item2'];

        $this->filesystem->mkdir($this->outputFolder);

        // Manually create the JSON files
        $this->filesystem->dumpFile($this->outputFolder . '/item1.json', json_encode(['item1' => 'data']));
        $this->filesystem->dumpFile($this->outputFolder . '/item2.json', json_encode(['item2' => 'data']));

        Phake::when($this->exportItemService)->exportItems($itemIdentifiers, $this->outputFolder, false)->thenReturn(null);

        $response = $this->exportService->export($itemIdentifiers, false);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('application/zip', $response->headers->get('content-type'));

        $zipContent = $response->getContent();
        $zipFile = tempnam(sys_get_temp_dir(), 'export_zip');
        file_put_contents($zipFile, $zipContent);

        $zip = new \ZipArchive();
        $this->assertTrue($zip->open($zipFile));

        $tmpZipFile = $this->outputFolder . 'export.zip';
        $this->assertFalse($this->filesystem->exists($tmpZipFile));
    }

    public function testExportWithCombine()
    {
        $itemIdentifiers = ['item1', 'item2'];
        $combinedFile = $this->outputFolder . '/combined.json';
        $this->filesystem->dumpFile($combinedFile, json_encode(['item1', 'item2']));

        Phake::when($this->exportItemService)->exportItems($itemIdentifiers, $this->outputFolder, true)->thenReturn(null);

        $response = $this->exportService->export($itemIdentifiers, true);

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('application/json', $response->headers->get('content-type'));

        $jsonContent = $response->getContent();
        $this->assertJsonStringEqualsJsonString(json_encode(['item1', 'item2']), $jsonContent);
    }
}
