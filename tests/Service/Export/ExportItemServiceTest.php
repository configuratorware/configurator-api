<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\Export;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Phake;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\DefaultOptionCollection;
use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\ItemDTO;
use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\PriceCollection;
use Redhotmagma\ConfiguratorApiBundle\Exporter\Repository\ExporterRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Export\ExportItemService;
use Symfony\Component\Filesystem\Filesystem;

class ExportItemServiceTest extends TestCase
{
    /**
     * @var ExporterRepository|\Phake_IMock
     * @Mock ExporterRepository
     */
    private $exporterRepository;
    private Filesystem $filesystem;
    private ExportItemService $exportItemService;
    private vfsStreamDirectory $root;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->root = vfsStream::setup();
        $this->filesystem = new Filesystem();
        $this->exportItemService = new ExportItemService($this->exporterRepository);
    }

    public function testExportItemsWithoutCombine()
    {
        $itemIdentifiers = ['item1', 'item2'];
        $outputFolder = vfsStream::url('root/output');
        mkdir($outputFolder);

        $itemData = [
            ItemDTO::from(['identifier' => 'item1', 'item_itemclassification' => [], 'itemtext' => [], 'itemAttribute' => [], 'itemprice' => []]),
            ItemDTO::from(['identifier' => 'item2', 'item_itemclassification' => [], 'itemtext' => [], 'itemAttribute' => [], 'itemprice' => []]),
        ];

        Phake::when($this->exporterRepository)->loadItemdata($itemIdentifiers)->thenReturn($itemData);
        Phake::when($this->exporterRepository)->findPricesByComponentAndOption(Phake::anyParameters())->thenReturn(PriceCollection::from([]));
        Phake::when($this->exporterRepository)->loadComponentsFor(Phake::anyParameters())->thenReturn([]);
        Phake::when($this->exporterRepository)->findDefaultOptionsFor(Phake::anyParameters())->thenReturn(DefaultOptionCollection::from([]));

        $this->exportItemService->exportItems($itemIdentifiers, $outputFolder, false);

        $item1Content = file_get_contents(vfsStream::url('root/output/item1.json'));
        $item2Content = file_get_contents(vfsStream::url('root/output/item2.json'));

        $this->assertJsonStringEqualsJsonString(json_encode([$itemData[0]]), $item1Content);
        $this->assertJsonStringEqualsJsonString(json_encode([$itemData[1]]), $item2Content);
    }

    public function testExportItemsWithCombine()
    {
        $itemIdentifiers = ['item1', 'item2'];
        $outputFolder = vfsStream::url('root/output');
        mkdir($outputFolder);

        $itemData = [
            ItemDTO::from(['identifier' => 'item1', 'item_itemclassification' => [], 'itemtext' => [], 'itemAttribute' => [], 'itemprice' => []]),
            ItemDTO::from(['identifier' => 'item2', 'item_itemclassification' => [], 'itemtext' => [], 'itemAttribute' => [], 'itemprice' => []]),
        ];

        Phake::when($this->exporterRepository)->loadItemdata($itemIdentifiers)->thenReturn($itemData);
        Phake::when($this->exporterRepository)->findPricesByComponentAndOption(Phake::anyParameters())->thenReturn(PriceCollection::from([]));
        Phake::when($this->exporterRepository)->loadComponentsFor(Phake::anyParameters())->thenReturn([]);
        Phake::when($this->exporterRepository)->findDefaultOptionsFor(Phake::anyParameters())->thenReturn(DefaultOptionCollection::from([]));

        $this->exportItemService->exportItems($itemIdentifiers, $outputFolder, true);

        $files = scandir($outputFolder);
        $jsonFile = null;
        foreach ($files as $file) {
            if ('json' === pathinfo($file, PATHINFO_EXTENSION)) {
                $jsonFile = $file;

                break;
            }
        }

        $this->assertNotNull($jsonFile, 'No JSON file found in the output folder');
        $jsonContent = file_get_contents($outputFolder . '/' . $jsonFile);

        $this->assertJsonStringEqualsJsonString(json_encode($itemData), $jsonContent);
    }
}
