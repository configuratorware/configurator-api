INSERT INTO `configuration` (`id`, `externaluser_id`, `code`, `name`, `selectedoptions`, `designdata`, `customdata`, `partslisthash`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `configurationtype_id`, `item_id`)
VALUES (5005, NULL, 'default_hoodie', 'default_hoodie', '{}', '{\"1\":{\"canvasData\":{\"objects\":[{\"type\":\"Text\",\"x\":472.5,\"y\":472.5,\"rotation\":326.64767082111814,\"scaling\":4.723037981538016,\"content\":\"<divstyle=\\\"font-family:Arial,Helvetica,sans-serif;\\\"><spanstyle=\\\"font-size:18px;font-family:\\\"ComicSansMS\\\",cursive,sans-serif;font-style:italic;font-weight:bold;color:rgb(0,120,184);\\\"data-color=\\\"blue\\\">My<spandata-color=\\\"red\\\"style=\\\"color:rgb(247,52,41);\\\">Text 1<\/span><\/span><\/div>\",\"style\":{\"colors\":[{\"identifier\":\"blue\",\"value\":\"rgb(0,120,184)\"},{\"identifier\":\"red\",\"value\":\"rgb(247,52,41)\"}],\"fontSize\":\"18px\",\"fontFamily\":\"\\\"ComicSansMS\\\",cursive,sans-serif\",\"fontWeight\":\"bold\",\"fontStyle\":\"italic\"},\"metric\":{\"x\":40,\"y\":40,\"width\":30,\"height\":8},\"hasBackgroundColor\":\"true\"},{\"type\":\"Text\",\"x\":100,\"y\":100,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"content\":\"<spanstyle=\\\"font-size:16px\\\">MyText 2<\/span>\"},{\"type\":\"Image\",\"x\":40,\"y\":80,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"imageData\":{\"operations\":{\"vectorizeColorsMap\":{\"#e30d1e\":{\"identifier\":\"{\\\"color\\\":\\\"red\\\",\\\"palette\\\":\\\"default\\\"}\",\"value\":\"#ec2b26\"},\"#fcd017\":{\"identifier\":\"{\\\"color\\\":\\\"yellow-orange\\\",\\\"palette\\\":\\\"default\\\"}\",\"value\":\"#fcd107\"}}}}}]},\"designProductionMethodIdentifier\":\"screen_printing\"},\"2\":{\"canvasData\":{\"objects\":[{\"type\":\"Text\",\"x\":148,\"y\":118.5,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"content\":\"<spanstyle=\\\"font-size:16px\\\">MyText 3<\/span>\"}]},\"designProductionMethodIdentifier\":\"screen_printing\",\"colorAmount\":5}}', '{\"selectedAmounts\":{\"hoodie_blue_s\":999,\"hoodie_red_m\":88},\"userSelectedCalculation\":{\"global_gift_packaging\":true},\"comment\":\"Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.\"}', NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5005),
(5006, NULL, 'no_conf_hoodie', 'default_hoodie', '{}', '', '{\"selectedAmounts\":{\"hoodie\":2}}', NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5005),
(5007, NULL, 'bulk_name_test', 'bulk name test', '{}', '', '{"bulkNames": ["Johann Strauß","Mika Häkkinen","Vaqif Səmədoğlu","Gérard Depardieu","Sinéad O’Connor","Antonín Dvořák","Đorđe Balašević","Արամ Խաչատրյան","Вагиф Сәмәдоғлу","章子怡","王菲","أم كلثوم","ედუარდ შევარდნაძე","Γιώργος Νταλάρας","András Sütő","Björk Guðmundsdóttir","이설희","李安","ธงไชย แม็คอินไตย์","Ђорђе Балашевић"],"selectedAmounts": {"S-012192103920": 500},"partslistCode": "MF39N9"}', NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5005),
(5008, NULL, 'low_order_amount', 'low_order_amount', '{}', '{\"1\":{\"canvasData\":{\"objects\":[{\"type\":\"Text\",\"x\":472.5,\"y\":472.5,\"rotation\":326.64767082111814,\"scaling\":4.723037981538016,\"content\":\"<divstyle=\\\"font-family:Arial,Helvetica,sans-serif;\\\"><spanstyle=\\\"font-size:18px;font-family:\\\"ComicSansMS\\\",cursive,sans-serif;font-style:italic;font-weight:bold;color:rgb(0,120,184);\\\"data-color=\\\"blue\\\">My<spandata-color=\\\"red\\\"style=\\\"color:rgb(247,52,41);\\\">Text 1<\/span><\/span><\/div>\",\"style\":{\"colors\":[{\"identifier\":\"blue\",\"value\":\"rgb(0,120,184)\"},{\"identifier\":\"red\",\"value\":\"rgb(247,52,41)\"}],\"fontSize\":\"18px\",\"fontFamily\":\"\\\"ComicSansMS\\\",cursive,sans-serif\",\"fontWeight\":\"bold\",\"fontStyle\":\"italic\"},\"metric\":{\"x\":40,\"y\":40,\"width\":30,\"height\":8}},{\"type\":\"Text\",\"x\":100,\"y\":100,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"content\":\"<spanstyle=\\\"font-size:16px\\\">MyText 2<\/span>\"},{\"type\":\"Image\",\"x\":40,\"y\":80,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"imageData\":{\"operations\":{\"vectorizeColorsMap\":{\"#e30d1e\":{\"identifier\":\"{\\\"color\\\":\\\"red\\\",\\\"palette\\\":\\\"default\\\"}\",\"value\":\"#ec2b26\"},\"#fcd017\":{\"identifier\":\"{\\\"color\\\":\\\"yellow-orange\\\",\\\"palette\\\":\\\"default\\\"}\",\"value\":\"#fcd107\"}}}}}]},\"designProductionMethodIdentifier\":\"transfer_printing\"},\"2\":{\"canvasData\":{\"objects\":[{\"type\":\"Text\",\"x\":148,\"y\":118.5,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"content\":\"<spanstyle=\\\"font-size:16px\\\">MyText 3<\/span>\"}]},\"designProductionMethodIdentifier\":\"transfer_printing\",\"colorAmount\":5}}', '{\"selectedAmounts\":{\"hoodie_blue_s\":1,\"hoodie_red_m\":1},\"userSelectedCalculation\":{\"global_gift_packaging\":true}}', NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5005);

INSERT INTO item(id, identifier, externalid, configurable, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id, item_status_id)
VALUES (5005, 'hoodie', NULL, NULL, 0, '2019-02-01 11:00:38', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2),
(5006, 'hoodie_red_s', NULL, NULL, 0, '2019-02-01 11:00:57', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL),
(5007, 'hoodie_red_m', NULL, NULL, 0, '2019-02-01 11:01:12', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL),
(5008, 'hoodie_red_l', NULL, NULL, 0, '2019-02-01 11:01:19', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL),
(5009, 'hoodie_blue_s', NULL, NULL, 0, '2019-02-01 11:01:32', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL),
(5010, 'hoodie_blue_m', NULL, NULL, 0, '2019-02-01 11:01:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL),
(5011, 'hoodie_blue_l', NULL, NULL, 0, '2019-02-01 11:01:45', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL);

REPLACE INTO itemgroup(id, identifier, sequencenumber, configurationvariant, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (1, 'color', 1, 0, '2019-01-30 15:30:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(2, 'size', 2, 0, '2019-01-30 15:30:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(3, 'shape', 3, 0, '2019-01-30 15:30:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

REPLACE INTO itemgrouptranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, itemgroup_id, language_id)
VALUES (1, 'Color', '2019-01-30 15:34:13', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
(2, 'Farbe', '2019-01-30 15:34:20', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
(3, 'Size', '2019-01-30 15:34:29', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
(4, 'Größe', '2019-01-30 15:34:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2);

INSERT INTO itemgroupentry(id, identifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (1, 'red', 1, '2019-01-30 15:30:28', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(2, 'blue', 2, '2019-01-30 15:30:36', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(3, 's', 3, '2019-01-30 15:30:56', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(4, 'm', 4, '2019-01-30 15:31:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(5, 'l', 5, '2019-01-30 15:31:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6, 'round', 6, '2019-01-30 15:31:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO itemgroupentrytranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, itemgroupentry_id, language_id)
VALUES (1, 'Red', '2019-01-30 15:36:58', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
(2, 'Rot', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
(3, 'Blue', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
(4, 'Blau', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2),
(5, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 1),
(6, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 2),
(7, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 4, 1),
(8, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 4, 2),
(9, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5, 1),
(10, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5, 2);

INSERT INTO item_itemgroup(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, itemgroup_id)
VALUES (5000, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 1),
(5001, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 2),
(5002, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 1),
(5003, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 2),
(5004, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 1),
(5005, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 2),
(5006, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 1),
(5007, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 2),
(5008, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 1),
(5009, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 2),
(5010, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 1),
(5011, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 2);

INSERT INTO item_itemgroupentry(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, itemgroupentry_id, itemgroup_id)
VALUES (5000, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 1, 1),
(5001, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 3, 2),
(5002, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 1, 1),
(5003, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 4, 2),
(5004, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 1, 1),
(5005, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 5, 2),
(5006, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 2, 1),
(5007, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 3, 2),
(5008, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 2, 1),
(5009, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 4, 2),
(5010, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 2, 1),
(5011, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 5, 2);

INSERT INTO `item_configuration_mode_item_status` (`id`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `item_status_id`, `configuration_mode_id`)
VALUES (5005, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 5005, 2, 2),
    (5006, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 5006, 2, 2),
    (5007, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 5007, 2, 2),
    (5008, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 5008, 2, 2),
    (5009, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 5009, 2, 2),
    (5010, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 5010, 2, 2),
    (5011, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 5011, 2, 2);

INSERT INTO itemprice(price, price_net, amountfrom, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, channel_id, item_id)
VALUES (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5005),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5005),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5006),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5006),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5007),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5007),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5008),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5008),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5009),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5009),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5010),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5010),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5011),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5011);

INSERT INTO design_area(id, identifier, sequence_number, height, width, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, option_id)
VALUES (1, '1', 1, 100, 100, NULL, '2019-02-04 15:13:13', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL),
(2, '2', 1, 100, 100, NULL, '2019-02-04 15:13:26', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL);

INSERT INTO design_area_design_production_method(id, width, height, additional_data, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, design_area_id, design_production_method_id)
VALUES (1, NULL, NULL, NULL, NULL, '2019-02-04 15:50:54', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
(2, NULL, NULL, NULL, NULL, '2019-02-04 15:51:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
(3, NULL, NULL, NULL, NULL, '2019-02-04 15:51:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 3),
(4, NULL, NULL, NULL, NULL, '2019-02-04 15:51:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 3);

INSERT INTO designer_production_calculation_type(id, identifier, color_amount_dependent, item_amount_dependent, selectable_by_user, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, design_production_method_id)
VALUES (1, 'setup_cost', 1, 0, 0, '2019-02-04 15:49:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1),
(2, 'printing_cost', 1, 1, 0, '2019-02-04 15:50:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1);

INSERT INTO design_view(id, identifier, sequence_number, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, option_id)
VALUES (1, '3d', 1, NULL, '2019-02-20 14:27:01', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL);

INSERT INTO design_view_design_area(id, base_shape, `position`, custom_data, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, design_view_id, design_area_id)
VALUES (1, '{"type":"Plane","scale":1,"rotation":{"x":0,"y":0,"z":0},"textureSize":{"width":2048,"height":2048}}', '{"x":200,"y":1024,"width":150,"height":150}', NULL, '2019-02-20 14:27:27', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
(2, '{"type":"Plane","scale":1,"rotation":{"x":0,"y":0,"z":0},"textureSize":{"width":2048,"height":2048}}', '{"x":1500,"y":1024,"width":150,"height":150}', NULL, '2019-02-20 14:27:34', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2);

INSERT INTO design_area_text(id, title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, design_area_id, language_id)
VALUES (1, '1', '2019-02-20 14:30:48', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
(2, 'Vorderseite', '2019-02-20 14:30:58', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
(3, '2', '2019-02-20 14:31:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
(4, 'Ärmel', '2019-02-20 14:31:18', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2);
