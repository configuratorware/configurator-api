<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument;

use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client as ClientDTO;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeLength;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationImageProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DefaultConfigurationGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\FrontendConfigurationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\MinimumOrderAmount;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\DataCollector;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\DocumentGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemAvailability;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputValidation;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckResolver;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\RulesRetriever;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\Cache\Adapter\NullAdapter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\ComponentImageRepositoryMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\ItemImageRepositoryMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DocumentGeneratorTest extends ApiTestCase
{
    /**
     * @var array
     */
    public const C_CLIENT_SETTINGS_SAMPLE = [
        'theme' => ['highlightColor' => '#ed1c29'],
        'email' => '',
        'contactName' => 'RHM',
        'contactStreet' => 'Badergasse 8',
        'contactPostCode' => '70372',
        'contactCity' => 'Stuttgart',
        'contactPhone' => '+49 (0) 711 / 520 882 60',
        'contactEmail' => 'info@redhotmagma.de',
        'pdfHeader' => '<span>info@redhotmagma.de<span><br/><span>+49 (0) 711 / 520 882 60</span>',
        'pdfFooter' => '<span>RHM, Badergasse 8, 70372Stuttgart</span>',
    ];

    private ConfigurationLoad $configurationLoad;

    private ConfigurationRepository $configurationRepository;

    private SettingRepository $settingRepository;

    private DataCollector $dataCollector;

    private DocumentGenerator $documentGenerator;

    private Filesystem $fileSystem;

    private string $projectDir;

    /**
     * @var Client|\Phake_IMock
     * @Mock Client
     */
    private $client;

    /**
     * @var Request|\Phake_IMock
     * @Mock Request
     */
    private $request;

    protected function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/DocumentGeneratorSetUp.sql'));

        $container = static::$kernel->getContainer();
        /** @var EventDispatcherInterface $eventDispatcher */
        $eventDispatcher = $container->get('event_dispatcher');

        $this->configurationRepository = $this->em->getRepository(Configuration::class);
        $itemRepository = $this->em->getRepository(Item::class);
        $optionRepository = $this->em->getRepository(Option::class);
        $ruleRepository = $this->em->getRepository(Rule::class);
        $configurationTypeRepository = $this->em->getRepository(Configurationtype::class);
        $this->settingRepository = $this->em->getRepository(Setting::class);

        $rulesRetriever = new RulesRetriever($ruleRepository, new NullAdapter());
        $checkResolver = new CheckResolver($eventDispatcher, $ruleRepository, $optionRepository, $rulesRetriever);
        $optionInputValidation = new OptionInputValidation($optionRepository);
        $configurationValidator = new ConfigurationValidator(
            $checkResolver,
            $itemRepository,
            $optionRepository,
            $eventDispatcher,
            $optionInputValidation
        );
        $configurationCodeGenerator = new ConfigurationCodeGenerator(
            new ConfigurationCodeLength([]),
            $this->configurationRepository
        );
        $defaultConfigurationGenerator = new DefaultConfigurationGenerator(
            $this->configurationRepository,
            $configurationCodeGenerator,
            $configurationTypeRepository
        );
        $frontendConfigurationStructureFromEntityConverter = new FrontendConfigurationStructureFromEntityConverter($eventDispatcher);
        $itemAvailability = new ItemAvailability($eventDispatcher);
        $minimumOrderAmount = new MinimumOrderAmount($itemRepository);

        $clientEntity = (new ClientEntity())->setIdentifier('_default')->setCallToAction('addToCart');
        $defaultClient = ClientDTO::fromEntity($clientEntity);

        \Phake::when($this->client)->getClient()->thenReturn($defaultClient);

        $this->configurationLoad = new ConfigurationLoad(
            $this->configurationRepository,
            $configurationValidator,
            $defaultConfigurationGenerator,
            $frontendConfigurationStructureFromEntityConverter,
            $itemAvailability,
            new ConfigurationImageProvider(new ComponentImageRepositoryMock(), new ItemImageRepositoryMock(), new ItemImageRepositoryMock()),
            $itemRepository,
            $minimumOrderAmount,
            $this->settingRepository,
            $this->client
        );

        $this->dataCollector = $container->get(DataCollector::class);
        $this->documentGenerator = $container->get(DocumentGenerator::class);

        $this->fileSystem = new Filesystem();
        $this->projectDir = rtrim(static::$kernel->getProjectDir(), '/');

        // fake request data for MediaHelper class
        \Phake::when($this->request)->server->thenReturn(['SERVER_NAME' => 'test']);
        $container->get('request_stack')->push($this->request);

        if (!defined('C_CHANNEL')) {
            define('C_CHANNEL', '_default');
        }

        if (!defined('C_CHANNEL_SETTINGS')) {
            define(
                'C_CHANNEL_SETTINGS',
                [
                    'currencySymbol' => '€',
                    'vatrate' => 19,
                    'pricesDataNetGross' => 'gross',
                    'pricesDisplayedNetGross' => 'gross',
                ]
            );
        }

        $container->get('translator')->setLocale('en_GB');
        if (!defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', 'en_GB');
        }

        if (!defined('C_LANGUAGE_SETTINGS')) {
            define(
                'C_LANGUAGE_SETTINGS',
                [
                    'id' => 1,
                    'iso' => 'en_GB',
                    'dateformat' => 'm/d/Y',
                    'pricedecimals' => 2,
                    'decimalpoint' => '.',
                    'thousandsseparator' => ',',
                    'currencysymbolposition' => 'left',
                ]
            );
        }

        if (!defined('C_CLIENT')) {
            define('C_CLIENT', ClientRepository::DEFAULT_CLIENT_IDENTIFIER);
        }

        if (!defined('C_CLIENT_SETTINGS')) {
            define(
                'C_CLIENT_SETTINGS',
                self::C_CLIENT_SETTINGS_SAMPLE
            );
        }

        if (false === defined('CALCULATION_PRICES_NET')) {
            define('CALCULATION_PRICES_NET', false);
        }
    }

    public function testShouldGenerateFile()
    {
        $dataSet = $this->getDataSet('defaultoptions_1', 'production');

        $temporaryHtmlFile = $this->documentGenerator->generateHtml($dataSet);

        self::assertFileExists($temporaryHtmlFile->getPath(), 'File not Uploaded');
    }

    public function testShouldGenerateHtml()
    {
        $dataSet = $this->getDataSet('defaultoptions_1', 'production');

        $temporaryHtmlFile = $this->documentGenerator->generateHtml($dataSet);

        self::assertFileExists($temporaryHtmlFile->getPath(), 'File not Uploaded');

        self::assertNotEquals(
            strpos(file_get_contents($temporaryHtmlFile->getPath()), '<html'),
            false,
            'Temporary File does not contain html.'
        );
    }

    public function testShouldGenerateNoConfHoodieHtml()
    {
        $dataSet = $this->getDataSet('no_conf_hoodie', 'user');

        $temporaryHtmlFile = $this->documentGenerator->generateHtml($dataSet);

        self::assertSame(
            str_replace(' ', '', $this->getExpectedOutput(__DIR__ . '/_data/DocumentGenerator-no_conf_hoodie.html')),
            str_replace(' ', '', file_get_contents($temporaryHtmlFile->getPath())),
            'Generated HTML File differs.'
        );
    }

    public function testShouldGenerateDefaultHoodieHtml()
    {
        $coverPath = $this->projectDir . '/tests/Controller/_data/media/images/item/image/hoodie_red_m.png';
        $this->fileSystem->dumpFile($coverPath, 'This is a test file.');

        $path = $this->projectDir . '/tests/Controller/_data/media/images/configurations/default_hoodie';
        $images = ['/item_1/view_1.png', '/item_1/view_2.png', '/item_2/view_3.png'];
        $png = file_get_contents(__DIR__ . '/_data/logo_redhotmagma.png');
        foreach ($images as $image) {
            $this->fileSystem->dumpFile($path . $image, $png);
        }

        $dataSet = $this->getDataSet('default_hoodie', 'production');

        $temporaryHtmlFile = $this->documentGenerator->generateHtml($dataSet);

        self::assertSame(
            str_replace(' ', '', $this->getExpectedOutput(__DIR__ . '/_data/DocumentGenerator-default_hoodie.html')),
            str_replace(' ', '', file_get_contents($temporaryHtmlFile->getPath())),
            'Generated HTML File differs.'
        );

        $this->fileSystem->remove($coverPath);
        $this->fileSystem->remove($path);
    }

    public function testShouldGenerateWidgetHtml()
    {
        $coverPath = $this->projectDir . '/tests/Controller/_data/media/images/item/image/hoodie_red_m.jpg';
        $this->fileSystem->dumpFile($coverPath, 'This is a test file.');

        $dataSet = $this->getDataSet('default_hoodie', 'widget');

        $temporaryHtmlFile = $this->documentGenerator->generateHtml($dataSet);

        self::assertSame(
            str_replace(' ', '', $this->getExpectedOutput(__DIR__ . '/_data/DocumentGenerator-widget_hoodie.html')),
            str_replace(' ', '', file_get_contents($temporaryHtmlFile->getPath())),
            'Generated HTML File differs.'
        );

        $this->fileSystem->remove($coverPath);
    }

    private function getExpectedOutput($filePath): string
    {
        $expected = file_get_contents($filePath);
        $expected = str_replace('§§TODAY§§', date(C_LANGUAGE_SETTINGS['dateformat']), $expected);
        $expected = str_replace('§§CURRENT_DIR§§', $this->projectDir, $expected);

        return $expected;
    }

    private function getDataSet(string $configurationCode, string $documentType)
    {
        $dataSet = $this->dataCollector->create(
            $this->configurationLoad->loadByCode($configurationCode),
            $this->configurationRepository->findOneBy(['code' => $configurationCode]),
            $documentType,
            new ControlParameters([])
        );

        $dataSet['documentInfo'] = array_replace(
            $dataSet['documentInfo'],
            self::C_CLIENT_SETTINGS_SAMPLE
        );

        return $dataSet;
    }

    public function testLowOrderAmountShouldStillGenerateHtml()
    {
        $dataSet = $this->getDataSet('low_order_amount', 'user');

        $temporaryHtmlFile = $this->documentGenerator->generateHtml($dataSet);

        self::assertFileExists($temporaryHtmlFile->getPath(), 'File not Uploaded');

        self::assertNotEquals(
            strpos(file_get_contents($temporaryHtmlFile->getPath()), '<html'),
            false,
            'Temporary File does not contain html.'
        );
    }

    public function testAddComponentAndOptionStructures()
    {
        $dataSet = $this->getDataSet('defaultoptions_1', 'production');
        self::assertEquals('coat', $dataSet['componentInfos'][0]->component->identifier);
        self::assertEquals('coat_nature', $dataSet['componentInfos'][0]->component->selectedoptions[0]->identifier);
    }
}
