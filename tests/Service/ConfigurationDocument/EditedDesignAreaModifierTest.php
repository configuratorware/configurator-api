<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument;

use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\PrintDesignDataProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\EditedDesignAreaModifier;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\EditedDesignArea;
use Symfony\Component\Filesystem\Filesystem;

class EditedDesignAreaModifierTest extends TestCase
{
    /**
     * @var  Filesystem|\Phake_IMock
     * @Mock Filesystem
     */
    protected $filesystem;

    /**
     * @var  PrintDesignDataProvider|\Phake_IMock
     * @Mock PrintDesignDataProvider
     */
    protected $printDesignDataProvider;

    /**
     * @var  LoggerInterface|\Phake_IMock
     * @Mock LoggerInterface
     */
    protected $logger;

    private EditedDesignAreaModifier $imageProvider;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->imageProvider = new EditedDesignAreaModifier(
            $this->filesystem,
            $this->printDesignDataProvider,
            $this->logger
        );
    }

    public function testShouldLogExceptions(): void
    {
        \Phake::when($this->printDesignDataProvider)
            ->provide(self::anything())
            ->thenThrow(new \Exception('provider error'));

        $designArea = new EditedDesignArea();
        $designArea->images = ['path1'];
        $designAreaList = [$designArea];

        $actual = $this->imageProvider->change($designAreaList);

        self::assertEmpty($actual[0]->images[0]);
        \Phake::verify($this->logger)->error('provider error', self::anything());
    }

    public function testShouldReturnEmptyStringOnMissingContent(): void
    {
        \Phake::when($this->printDesignDataProvider)->provide('path1')->thenReturn('');

        $designArea = new EditedDesignArea();
        $designArea->images = ['path1'];
        $designAreaList = [$designArea];

        $actual = $this->imageProvider->change($designAreaList);

        self::assertEmpty($actual[0]->images[0]);
        \Phake::verify($this->filesystem, \Phake::never())->tempnam(self::anything());
    }

    public function testShouldCreateAndCleanTempFilesForImageConversion(): void
    {
        \Phake::when($this->filesystem)->tempnam(self::anything(), self::anything())->thenReturn('tempName');
        \Phake::when($this->printDesignDataProvider)->provide('path1')->thenReturn('svg content');
        $expectedSvgContents = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>svg content';

        $designArea = new EditedDesignArea();
        $designArea->images = ['path1'];
        $designAreaList = [$designArea];

        $this->imageProvider->change($designAreaList);

        \Phake::verify($this->filesystem)->dumpFile('tempName.svg', $expectedSvgContents);
        \Phake::verify($this->filesystem)->remove('tempName.svg');
    }
}
