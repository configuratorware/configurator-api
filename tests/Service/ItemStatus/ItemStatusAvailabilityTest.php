<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus\ItemStatusAvailability;

class ItemStatusAvailabilityTest extends TestCase
{
    /**
     * @var ItemStatusAvailability
     */
    private $itemStatusAvailability;

    public function setUp(): void
    {
        $this->itemStatusAvailability = new ItemStatusAvailability();
    }

    /**
     * @param Item $item
     * @dataProvider providerShouldReturnTrue
     */
    public function testShouldReturnTrue(Item $item): void
    {
        self::assertTrue($this->itemStatusAvailability->checkItemAvailability($item),
            'Item availability is not correctly checked to true');
    }

    public function providerShouldReturnTrue(): array
    {
        return [
            [$this->createParent(true)],
            [$this->createChild(true)],
        ];
    }

    /**
     * @param Item $item
     * @dataProvider providerShouldReturnFalse
     */
    public function testShouldReturnFalse(Item $item): void
    {
        self::assertFalse($this->itemStatusAvailability->checkItemAvailability($item),
            'Item availability is not correctly checked to false');
    }

    public function providerShouldReturnFalse(): array
    {
        return [
            [$this->createParent(false)],
            [$this->createChild(false)],
        ];
    }

    private function createChild(bool $isParentAvailable): Item
    {
        return (new Item())->setParent($this->createParent($isParentAvailable));
    }

    private function createParent(bool $isAvailable): Item
    {
        $itemStatus = (new ItemStatus())->setItemAvailable($isAvailable);

        $item = new Item();

        $item->setItemStatus($itemStatus);

        return $item;
    }
}
