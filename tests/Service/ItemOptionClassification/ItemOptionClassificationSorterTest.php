<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassification;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassification\ItemOptionClassificationSorter;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

class ItemOptionClassificationSorterTest extends TestCase
{
    use EntityTestTrait;

    private $itemOptionClassificationSorter;

    public function setUp(): void
    {
        $this->itemOptionClassificationSorter = new ItemOptionClassificationSorter();
    }

    /**
     * @param array $expectedOrder
     * @param ItemOptionclassification[] $itemOptionClassifications
     *
     * @dataProvider providerShouldSortAsc
     */
    public function testShouldSortAsc(array $expectedOrder, array $itemOptionClassifications): void
    {
        $this->itemOptionClassificationSorter->sortAsc($itemOptionClassifications);

        $actualOrder = [];
        foreach ($itemOptionClassifications as $itemOptionClassification) {
            $actualOrder[] = $itemOptionClassification->getId();
        }

        self::assertEquals($expectedOrder, $actualOrder);
    }

    public function providerShouldSortAsc(): array
    {
        return [
            [
                [1, 2, 3],
                [
                    $this->generateItemComponent(3, 20),
                    $this->generateItemComponent(2, 10),
                    $this->generateItemComponent(1, 3),
                ],
            ],
            [
                [3, 4, 5, 8],
                [
                    $this->generateItemComponent(5, 10),
                    $this->generateItemComponent(3, 2),
                    $this->generateItemComponent(4, 3),
                    $this->generateItemComponent(8, null),
                ],
            ],
        ];
    }

    private function generateItemComponent(int $id, ?int $sequenceNumber): ItemOptionclassification
    {
        $itemComponent = new ItemOptionclassification();
        $this->setPrivateId($itemComponent, $id);
        $itemComponent->setSequenceNumber($sequenceNumber);

        return $itemComponent;
    }
}
