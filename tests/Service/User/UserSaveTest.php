<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\User as UserEntity;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Role\RoleStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserSave;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class UserSaveTest extends ApiTestCase
{
    /**
     * @var UserSave
     */
    private $userSave;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    /**
     * @var UserEntityFromStructureConverter|\Phake_IMock
     * @Mock UserEntityFromStructureConverter
     */
    private $userEntityFromStructureConverter;

    /**
     * @var UserStructureFromEntityConverter|\Phake_IMock
     * @Mock UserStructureFromEntityConverter
     */
    private $userStructureFromEntityConverter;

    /**
     * @var RoleStructureFromEntityConverter|\Phake_IMock
     * @Mock RoleStructureFromEntityConverter
     */
    private $roleStructureFromEntityConverter;

    public function setUp(): void
    {
        parent::setUp();

        \Phake::initAnnotations($this);
        $this->userRepository = $this->em->getRepository(UserEntity::class);
        $this->roleRepository = $this->em->getRepository(Role::class);
        $this->passwordHasher = $this->getContainer()->get(UserPasswordHasherInterface::class);

        $this->userSave = new UserSave(
            $this->userRepository,
            $this->roleRepository,
            $this->userEntityFromStructureConverter,
            $this->userStructureFromEntityConverter,
            $this->roleStructureFromEntityConverter
        );
    }

    public function testShouldOverwritePasswordIfUserExists(): void
    {
        $changedPassword = 'changed_password';
        $user = $this->createUserStructure();
        $user->password = $changedPassword;

        $userToSave = $this->userRepository->findOneByUsername('admin');
        self::assertTrue($this->passwordHasher->isPasswordValid($userToSave, 'admin'));

        $userToSave->setPassword($changedPassword, $this->passwordHasher);
        \Phake::when($this->userEntityFromStructureConverter)->convertOne->thenReturn($userToSave);

        $this->userSave->saveAdminUser($user);

        $userStored = $this->userRepository->findOneByUsername('admin');
        self::assertTrue($this->passwordHasher->isPasswordValid($userStored, $changedPassword));
    }

    public function testShouldReturnRandomPasswordOnSuccessfulRegistrationWhenPasswordIsNotInformed(): void
    {
        $user = $this->createUserStructure();
        $user->username = 'new_admin';

        $mock = \Phake::partialMock(
            UserSave::class,
            $this->userRepository,
            $this->roleRepository,
            $this->userEntityFromStructureConverter,
            $this->userStructureFromEntityConverter,
            $this->roleStructureFromEntityConverter
        );

        \Phake::when($mock)->generateRandomPassword()->thenReturn('test');
        \Phake::when($this->userEntityFromStructureConverter)->convertOne($user,
            null)->thenReturn($this->createUserEntity($user));

        $result = $this->userSave->saveAdminUser($user);

        self::assertIsString($result);
        self::assertEquals(UserSave::RANDOM_PASSWORD_SIZE, strlen($result));
    }

    public function testShouldReturnPasswordOnSuccessfulRegistrationWhenPasswordIsInformed(): void
    {
        $user = $this->createUserStructure();
        $user->username = 'new_admin';
        $user->password = 'some_password';

        \Phake::when($this->userEntityFromStructureConverter)->convertOne($user,
            null)->thenReturn($this->createUserEntity($user));

        $result = $this->userSave->saveAdminUser($user);

        self::assertEquals($user->password, $result);
    }

    private function createUserStructure()
    {
        $user = new User();

        $user->username = 'admin';
        $user->email = 'admin@admin.de';
        $user->active = true;
        $user->firstname = 'Admin';
        $user->lastname = 'Admin';

        return $user;
    }

    private function createUserEntity(User $user)
    {
        $userEntity = new UserEntity();

        $userEntity->setUsername($user->username);
        $userEntity->setEmail($user->email);
        $userEntity->setIsActive($user->active);
        $userEntity->setFirstname($user->firstname);
        $userEntity->setLastname($user->lastname);
        $userEntity->setPassword($user->password ?: \Phake::anyParameters(), $this->passwordHasher);
        $userEntity->setDateCreated(new \DateTime('2020-01-01 00:00:00'));
        $userEntity->setDateDeleted(new \DateTime('0001-01-01 00:00:00'));

        return $userEntity;
    }
}
