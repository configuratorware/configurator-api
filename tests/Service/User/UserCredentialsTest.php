<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\User;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Credential;
use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Entity\UserRole;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserCredentials;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LicensePathInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\User\InMemoryUser;

class UserCredentialsTest extends TestCase
{
    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var UserCredentials
     */
    private $userCredentials;

    public function setUp(): void
    {
        $this->root = vfsStream::setup();
        $this->fs = new Filesystem();

        $this->userCredentials = new UserCredentials(new LicenseFileValidator(new LicencePathMock(), new LicenseChecker(), $this->fs));
    }

    /**
     * @param array $expectedCredentials
     * @param array $userCredentials
     * @param string $licenseFile
     * @dataProvider providerShouldGetCredentialsByUser
     */
    public function testShouldGetCredentialsByUser(array $expectedCredentials, array $userCredentials, string $licenseFile): void
    {
        $this->fs->dumpFile($this->root->url() . LicencePathMock::LICENSE_FILE, $licenseFile);

        $this->assertEquals($expectedCredentials, $this->userCredentials->getByUser($this->createUser($userCredentials)));
    }

    public function providerShouldGetCredentialsByUser(): array
    {
        return [
            'user has credential + feature is licensed' => [
                ['clients', 'items', 'users'],
                ['clients', 'items', 'users'],
                'creator###configuratorware###clients' . PHP_EOL . '7c6154d1266a88e33e038049b13876a5391859e491a3d5891e7f2de54f41ce31',
            ],
            'user has credential + feature is  NOT licensed' => [
                ['items', 'users'],
                ['clients', 'items', 'users'],
                'creator###configuratorware' . PHP_EOL . '88ea744498630f02f3b003d15b2b5dc08ffee50aa1428a4a5f6aa1828721887f',
            ],
            'user has not the credential + feature is licensed' => [
                ['items', 'users'],
                ['items', 'users'],
                'creator###configuratorware###clients' . PHP_EOL . '7c6154d1266a88e33e038049b13876a5391859e491a3d5891e7f2de54f41ce31',
            ],
        ];
    }

    public function testShouldFindEmptyArrayForNonCwUser(): void
    {
        $user = new InMemoryUser('test', 'test');
        $actual = $this->userCredentials->getByUser($user);

        $this->assertEmpty($actual);
    }

    private function createUser(array $credentials): User
    {
        $role = new Role();
        $role->setRole('ROLE_ADMIN');

        foreach ($credentials as $credential) {
            $credentialEntity = new Credential();
            $credentialEntity->setArea($credential);
            $roleCredentialEntity = new RoleCredential();
            $roleCredentialEntity->setRole($role);
            $roleCredentialEntity->setCredential($credentialEntity);
            $role->addRoleCredential($roleCredentialEntity);
        }

        $user = new User();
        $userRole = new UserRole();
        $userRole->setRole($role);
        $user->addUserRole($userRole);

        return $user;
    }
}

final class LicencePathMock implements LicensePathInterface
{
    public const LICENSE_FILE = '/license.txt';

    public function getLicenseFilePath(): string
    {
        return 'vfs://root' . self::LICENSE_FILE;
    }
}
