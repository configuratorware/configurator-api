<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewFileRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\LayerImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewSave;
use Redhotmagma\ConfiguratorApiBundle\Structure\CreatorView as CreatorViewStructure;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

final class CreatorViewSaveTest extends TestCase
{
    use EntityTestTrait;

    /**
     * @var DesignAreaRepository|\Phake_IMock
     * @Mock DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var LanguageRepository|\Phake_IMock
     * @Mock LanguageRepository
     */
    private $languageRepository;

    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var LayerImageRepositoryInterface|\Phake_IMock
     * @Mock LayerImageRepositoryInterface
     */
    private $layerImageRepository;

    /**
     * @var CreatorViewRepository|\Phake_IMock
     * @Mock CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var CreatorViewFileRepositoryInterface|\Phake_IMock
     * @Mock CreatorViewFileRepositoryInterface
     */
    private $creatorViewFileRepository;

    /**
     * @var CreatorViewSave
     */
    private $creatorViewSave;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $converter = new CreatorViewEntityFromStructureConverter($this->designAreaRepository, $this->languageRepository, $this->itemRepository);
        $this->creatorViewSave = new CreatorViewSave($converter, $this->creatorViewRepository, $this->creatorViewFileRepository, $this->itemRepository, $this->layerImageRepository);
    }

    public function testShouldCreateView(): void
    {
        $creatorViewId = 1;
        $creatorViewEntity = CreatorView::from(null, 'front_name_changed', $this->generateItem(), '01');
        $this->setPrivateId($creatorViewEntity, $creatorViewId);

        $givenStructure = $this->generateCreatorViewStructure(null, 'front', '01');

        $item = $this->generateItem();
        \Phake::when($this->itemRepository)->find(1)->thenReturn($item);
        \Phake::when($this->creatorViewRepository)->save->thenReturn($creatorViewEntity);

        $this->creatorViewSave->save($givenStructure);

        \Phake::verify($this->creatorViewFileRepository, \Phake::never())->saveView;
        \Phake::verify($this->layerImageRepository)->createStores(\Phake::capture($capturedView), \Phake::equalTo($item));

        self::assertEquals('front', $capturedView->getIdentifier());
    }

    /**
     * @param CreatorView $expectedView
     * @param CreatorViewStructure $givenStructure
     *
     * @dataProvider providerShouldSaveExistingView
     */
    public function testShouldSaveExistingView(CreatorView $expectedView, CreatorViewStructure $givenStructure): void
    {
        $this->setPrivateId($expectedView, $givenStructure->id);

        $item = $this->generateItem();
        \Phake::when($this->itemRepository)->find(1)->thenReturn($item);
        \Phake::when($this->creatorViewRepository)->find($givenStructure->id)->thenReturn($expectedView);
        \Phake::when($this->creatorViewRepository)->save->thenReturn($expectedView);

        $this->creatorViewSave->save($givenStructure);

        \Phake::verify($this->layerImageRepository, \Phake::never())->createStores;
        \Phake::verify($this->creatorViewFileRepository)->save(\Phake::equalTo($expectedView));
    }

    public function providerShouldSaveExistingView(): \Generator
    {
        yield [CreatorView::from('item_1/01_front', 'front_name_changed', $this->generateItem(), '03'), $this->generateCreatorViewStructure(2, 'front_name_changed', '03')];
        yield [CreatorView::from('item_1/01_front', 'front_name_changed', $this->generateItem(), '01'), $this->generateCreatorViewStructure(2, 'front_name_changed', '01')];
    }

    /**
     * @return Item
     */
    private function generateItem(): Item
    {
        $itemOptionClassification1 = new ItemOptionclassification();
        $itemOptionClassification1->setOptionclassification((new Optionclassification())->setIdentifier('mock_component_1'));

        $itemOptionClassification2 = new ItemOptionclassification();
        $itemOptionClassification2->setOptionclassification((new Optionclassification())->setIdentifier('mock_component_2'));

        $item = new Item();
        $this->setPrivateId($item, 1);
        $item->setIdentifier('item_1');
        $itemOptionClassification1->setItem($item);
        $itemOptionClassification2->setItem($item);
        $item->addItemOptionclassification($itemOptionClassification1);
        $item->addItemOptionclassification($itemOptionClassification2);

        return $item;
    }

    private function generateCreatorViewStructure($id, $identifier, $sequenceNumber): CreatorViewStructure
    {
        return new CreatorViewStructure($id, $identifier, $sequenceNumber, 1);
    }
}
