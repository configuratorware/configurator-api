<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationApi;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as ConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadCreatorViewThumbnail;

final class CreatorViewImageTest extends TestCase
{
    /**
     * @var CreatorViewRepository|\Phake_IMock
     * @Mock CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var CreatorViewImage
     */
    private $creatorViewImage;

    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;

    /**
     * @var CreatorViewThumbnailRepositoryInterface|\Phake_IMock
     * @Mock CreatorViewThumbnailRepositoryInterface
     */
    private $creatorViewThumbnailRepository;

    /**
     * @var ConfigurationApi|\Phake_IMock
     * @Mock ConfigurationApi
     */
    private $configurationApi;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->creatorViewImage = new CreatorViewImage($this->creatorViewRepository, $this->itemRepository, $this->creatorViewThumbnailRepository, $this->configurationApi);
    }

    public function testShouldUploadThumbnail(): void
    {
        $creatorViewId = 1;
        $item = $this->generateItem();
        $expectedCreatorView = CreatorView::from('item_1/02_mock_view_2', 'mock_view_2', $item, '02');

        \Phake::when($this->itemRepository)->findOneByIdentifier('item_1')->thenReturn($item);
        \Phake::when($this->creatorViewRepository)->find($creatorViewId)->thenReturn($expectedCreatorView);

        $uploadViewThumbnail = UploadCreatorViewThumbnail::from($creatorViewId, 'thumb2.png', '/app/public/thumb2.png');

        $this->creatorViewImage->uploadThumbnail($uploadViewThumbnail);

        $expectedThumbnail = CreatorViewThumbnail::from(null, 'mock_view_2', 'item_1', '/app/public/thumb2.png', 'thumb2.png');

        \Phake::verify($this->creatorViewThumbnailRepository)->saveThumbnail(\Phake::equalTo($expectedThumbnail), \Phake::equalTo($item), \Phake::equalTo($expectedCreatorView));
    }

    public function testShouldDeleteThumbnailForView(): void
    {
        $creatorViewId = 1;
        $item = $this->generateItem();
        $expectedCreatorView = CreatorView::from('item_1/02_mock_view_2', 'mock_view_2', $item, '02');

        \Phake::when($this->itemRepository)->findOneByIdentifier('item_1')->thenReturn($item);
        \Phake::when($this->creatorViewRepository)->find($creatorViewId)->thenReturn($expectedCreatorView);

        $creatorViewThumbnails = [
            CreatorViewThumbnail::from('/thumb1.png', 'mock_view_1', 'item_1', '/app/public/thumb1.png', 'thumb1.png'),
            CreatorViewThumbnail::from('/thumb2.png', 'mock_view_2', 'item_1', '/app/public/thumb2.png', 'thumb2.png'),
        ];

        \Phake::when($this->creatorViewThumbnailRepository)->findThumbnails($item)->thenReturn($creatorViewThumbnails);

        $this->creatorViewImage->deleteThumbnail($creatorViewId);

        $expectedThumbnail = CreatorViewThumbnail::from('/thumb2.png', 'mock_view_2', 'item_1', '/app/public/thumb2.png', 'thumb2.png');

        \Phake::verify($this->creatorViewThumbnailRepository)->deleteThumbnail(\Phake::equalTo($expectedThumbnail));
    }

    public function testShouldCreateLayerImageIdentifierMap(): void
    {
        $frontendConfiguration = $this->generateConfigurationStructure('02_some_view_2', ['/images/configuratorimages/item1/some_view_2/01_some_component_1/item1_black.png']);
        $item = $this->generateItem();
        \Phake::when($this->configurationApi)->loadByItemIdentifier($item->getIdentifier())->thenReturn($frontendConfiguration);

        $expected = $frontendConfiguration->visualizationData->viewImages;
        $actual = $this->creatorViewImage->createLayerImageIdentifierMap($item);

        self::assertEquals($expected, $actual);
    }

    public function testShouldHandleViewImagesNull(): void
    {
        $frontendConfiguration = new ConfigurationStructure();
        $frontendConfiguration->visualizationData = new \stdClass();
        $item = $this->generateItem();
        \Phake::when($this->configurationApi)->loadByItemIdentifier($item->getIdentifier())->thenReturn($frontendConfiguration);

        $expected = null;
        $actual = $this->creatorViewImage->createLayerImageIdentifierMap($item);

        self::assertEquals($expected, $actual);
    }

    /**
     * @param string $viewMapKey
     * @param array $imageUrls
     *
     * @return ConfigurationStructure
     */
    private function generateConfigurationStructure(string $viewMapKey, array $imageUrls): ConfigurationStructure
    {
        $frontendConfiguration = new ConfigurationStructure();
        $frontendConfiguration->visualizationData = new \stdClass();
        $frontendConfiguration->visualizationData->viewImages = new \stdClass();
        $frontendConfiguration->visualizationData->viewImages->$viewMapKey = $imageUrls;

        return $frontendConfiguration;
    }

    /**
     * @return Item
     */
    private function generateItem(): Item
    {
        $item = new Item();
        $item->setIdentifier('item_1');

        return $item;
    }
}
