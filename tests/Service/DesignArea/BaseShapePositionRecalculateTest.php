<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\BaseShapePositionRecalculate;

class BaseShapePositionRecalculateTest extends TestCase
{
    /**
     * @var BaseShapePositionRecalculate
     */
    private $baseShapePositionRecalculate;

    protected function setUp(): void
    {
        $this->baseShapePositionRecalculate = new BaseShapePositionRecalculate();
    }

    /**
     * @param $widthRatio
     * @param $heightRatio
     * @param $visualization
     * @param $givenBaseShape
     * @param $givenPosition
     * @param $expectedBaseShape
     * @param $expectedPosition
     * @dataProvider  providerShouldUpdateDesignArea
     */
    public function testShouldUpdateDesignArea(
        $widthRatio,
        $heightRatio,
        $visualization,
        $givenBaseShape,
        $givenPosition,
        $expectedBaseShape,
        $expectedPosition
    ): void {
        $designArea = $this->createDesignArea($givenBaseShape, $givenPosition);

        $this->baseShapePositionRecalculate->update(
            $designArea,
            $visualization,
            $widthRatio,
            $heightRatio
        );

        /** @var DesignViewDesignArea $viewArea */
        foreach ($designArea->getDesignViewDesignArea() as $viewArea) {
            self::assertEquals($expectedBaseShape, json_encode($viewArea->getBaseShape()));
            self::assertEquals($expectedPosition, json_encode($viewArea->getPosition()));
        }
    }

    public function providerShouldUpdateDesignArea(): array
    {
        return [
            [
                1,
                0.5,
                VisualizationMode::IDENTIFIER_2D_VARIANT,
                '{"type":"Plane","x":-250,"y":250,"width":500,"height":500}',
                '{"x":250,"y":250,"width":500,"height":500}',
                '{"type":"Plane","x":-250,"y":250,"width":500,"height":250}',
                '{"x":250,"y":125,"width":500,"height":250}',
            ],
            [
                1.5,
                1,
                VisualizationMode::IDENTIFIER_2D_VARIANT,
                '{"type":"Plane","x":-250,"y":250,"width":500,"height":500}',
                '{"x":250,"y":250,"width":500,"height":500}',
                '{"type":"Plane","x":-250,"y":250,"width":750,"height":500}',
                '{"x":375,"y":250,"width":750,"height":500}',
            ],
            [
                0.5,
                1.5,
                VisualizationMode::IDENTIFIER_2D_VARIANT,
                '{"type":"Plane","x":-250,"y":250,"width":500,"height":500}',
                '{"x":250,"y":250,"width":500,"height":500}',
                '{"type":"Plane","x":-250,"y":250,"width":250,"height":750}',
                '{"x":125,"y":375,"width":250,"height":750}',
            ],
            [
                0.5,
                1.5,
                VisualizationMode::IDENTIFIER_3D_VARIANT,
                '{"type":"Plane","x":-250,"y":250,"width":500,"height":500}',
                '{"x":250,"y":250,"width":500,"height":500}',
                '{"type":"Plane","x":-250,"y":250,"width":250,"height":750}',
                '{"x":250,"y":250,"width":250,"height":750}',
            ],
            [
                1,
                0.5,
                VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY,
                '{"type":"Plane","x":-250,"y":250,"width":500,"height":500}',
                '{"x":250,"y":250,"width":500,"height":500}',
                '{"type":"Plane","x":-250,"y":250,"width":500,"height":250}',
                '{"x":250,"y":125,"width":500,"height":250}',
            ],
        ];
    }

    /**
     * @param $widthRatio
     * @param $heightRatio
     * @param $visualization
     * @param $givenBaseShape
     * @param $givenPosition
     * @param $expectedBaseShape
     * @param $expectedPosition
     * @dataProvider  providerShouldNotUpdateDesignArea
     */
    public function testShouldNotUpdateDesignArea(
        $widthRatio,
        $heightRatio,
        $visualization,
        $givenBaseShape,
        $givenPosition,
        $expectedBaseShape,
        $expectedPosition
    ): void {
        $designArea = $this->createDesignArea($givenBaseShape, $givenPosition);

        $this->baseShapePositionRecalculate->update(
            $designArea,
            $visualization,
            $widthRatio,
            $heightRatio
        );

        /** @var DesignViewDesignArea $viewArea */
        foreach ($designArea->getDesignViewDesignArea() as $viewArea) {
            self::assertEquals(
                $expectedBaseShape,
                null === $viewArea->getBaseShape() ? $viewArea->getBaseShape() : json_encode($viewArea->getBaseShape())
            );
            self::assertEquals(
                $expectedPosition,
                null === $viewArea->getPosition() ? $viewArea->getPosition() : json_encode($viewArea->getPosition())
            );
        }
    }

    public function providerShouldNotUpdateDesignArea(): array
    {
        return [
            [
                1,
                1,
                VisualizationMode::IDENTIFIER_2D_VARIANT,
                '{"type":"Plane","x":-135.5,"y":135.5,"width":500,"height":500}',
                '{"x":500,"y":500,"width":500,"height":500}',
                '{"type":"Plane","x":-135.5,"y":135.5,"width":500,"height":500}',
                '{"x":500,"y":500,"width":500,"height":500}',
            ],
            [
                1,
                1,
                VisualizationMode::IDENTIFIER_3D_VARIANT,
                '{"type":"Plane"}',
                '{"width":1066,"height":2399,"x":990,"y":844,"angle":3.14}',
                '{"type":"Plane"}',
                '{"width":1066,"height":2399,"x":990,"y":844,"angle":3.14}',
            ],
            [
                1,
                1,
                VisualizationMode::IDENTIFIER_2D_VARIANT,
                null,
                null,
                null,
                null,
            ],
            [
                1,
                1,
                VisualizationMode::IDENTIFIER_2D_VARIANT_OVERLAY,
                '{"type":"Plane","x":-135.5,"y":135.5,"width":500,"height":500}',
                '{"x":500,"y":500,"width":500,"height":500}',
                '{"type":"Plane","x":-135.5,"y":135.5,"width":500,"height":500}',
                '{"x":500,"y":500,"width":500,"height":500}',
            ],
        ];
    }

    private function createDesignArea(
        ?string $baseShape,
        ?string $position
    ): DesignArea {
        $designArea = new DesignArea();

        for ($i = 0; $i < 2; ++$i) {
            $designViewDesignArea = new DesignViewDesignArea();
            $designViewDesignArea->setBaseShape($baseShape);
            $designViewDesignArea->setPosition($position);
            $designArea->addDesignViewDesignArea($designViewDesignArea);
        }

        return $designArea;
    }
}
