<?php

declare(strict_types=1);

use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\ApiVersionReader;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ApiVersionTest extends ApiTestCase
{
    /**
     * Existing valid composer.lock with entry configuratorware/configurator-api.
     */
    public function testWithValidFile()
    {
        $apiVersionReader = new ApiVersionReader(__DIR__ . '/_data/valid');
        self::assertSame('dev-master', $apiVersionReader->getApiVersion());
        self::assertSame('eb0037e893d6a32724c11a5c0b673454c0674983', $apiVersionReader->getApireferenceHash());
    }

    /**
     * Existing valid composer.lock with deprecated entry redhotmagma/configuratorapibundle.
     */
    public function testWithValidFileDeprecated()
    {
        $apiVersionReader = new ApiVersionReader(__DIR__ . '/_data/valid-deprecated');
        self::assertSame('dev-master', $apiVersionReader->getApiVersion());
        self::assertSame('eb0037e893d6a32724c11a5c0b673454c0674983', $apiVersionReader->getApireferenceHash());
    }

    /**
     * Malformed composer.lock.
     */
    public function testWithInvalidFile()
    {
        $apiVersionReader = new ApiVersionReader(__DIR__ . '/_data/malformed');
        self::assertSame('unknown', $apiVersionReader->getApiVersion());
        self::assertSame('unknown', $apiVersionReader->getApireferenceHash());
    }

    /**
     * Compsoer.lock doesn't exist.
     */
    public function testWithMissingFile()
    {
        $apiVersionReader = new ApiVersionReader(__DIR__ . '/_data/nofile');
        self::assertSame('unknown', $apiVersionReader->getApiVersion());
        self::assertSame('unknown', $apiVersionReader->getApireferenceHash());
    }
}
