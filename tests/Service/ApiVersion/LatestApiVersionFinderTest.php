<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\LatestApiVersionFinder;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\ReleasedApiVersionPackagistApiRepository;

class LatestApiVersionFinderTest extends TestCase
{
    /**
     * @param $releasedVersions
     * @param $expectedLatest
     * @dataProvider providerShouldFindLatestVersion
     */
    public function testShouldFindLatestVersion($releasedVersions, $expectedLatest): void
    {
        $repository = new ReleasedApiVersionPackagistApiRepository($releasedVersions);

        $latestVersionFinder = new LatestApiVersionFinder($repository);

        self::assertEquals($expectedLatest, $latestVersionFinder->findStable()->getTag());
    }

    public function providerShouldFindLatestVersion(): array
    {
        return [
            [
                ['dev-master', '1.3.0'],
                '1.3.0',
            ],
            [
                ['dev-master', '1.3.0', '1.20.0'],
                '1.20.0',
            ],
        ];
    }

    /**
     * @param $releasedVersions
     * @dataProvider providerShouldNotFindLatestVersion
     */
    public function testShouldNotFindLatestVersion($releasedVersions): void
    {
        $repository = new ReleasedApiVersionPackagistApiRepository($releasedVersions);

        $latestVersionFinder = new LatestApiVersionFinder($repository);

        self::assertNull($latestVersionFinder->findStable());
    }

    public function providerShouldNotFindLatestVersion(): array
    {
        return [
            [
                ['dev-master'],
            ],
            [
                [],
            ],
        ];
    }
}
