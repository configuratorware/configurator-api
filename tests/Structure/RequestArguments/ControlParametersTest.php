<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use PHPUnit\Framework\TestCase;

class ControlParametersTest extends TestCase
{
    public function testShouldCheckAdminMode(): void
    {
        $controlParameters = new ControlParameters([], true);

        self::assertTrue($controlParameters->isAdminMode());
    }

    /**
     * @param array $parameters
     * @param string $baseUrl
     * @param string $expected
     * @dataProvider providerShouldMerge
     */
    public function testShouldMerge(array $parameters, string $baseUrl, string $expected): void
    {
        $controlParameters = new ControlParameters($parameters);

        self::assertEquals($expected, $controlParameters->mergeGetParametersToUrl($baseUrl));
    }

    public function providerShouldMerge(): array
    {
        return [
            [
                [
                    ControlParameters::LANGUAGE => 'de_DE',
                    ControlParameters::CHANNEL => 'eur',
                    ControlParameters::CLIENT => 'at',
                ],
                'www.redhotmagma.de',
                'www.redhotmagma.de?_language=de_DE&_channel=eur&_client=at',
            ],
            [
                [
                    ControlParameters::LANGUAGE => 'de_DE',
                    ControlParameters::CHANNEL => 'eur',
                    ControlParameters::CLIENT => 'at',
                ],
                'www.redhotmagma.de?_language=de_DE',
                'www.redhotmagma.de?_language=de_DE&_channel=eur&_client=at',
            ],
            [
                [
                    ControlParameters::LANGUAGE => 'de_DE',
                    ControlParameters::CHANNEL => 'eur',
                    ControlParameters::CLIENT => 'at',
                ],
                'www.redhotmagma.de?utm_medium=shareUrl',
                'www.redhotmagma.de?utm_medium=shareUrl&_language=de_DE&_channel=eur&_client=at',
            ],
            [
                [
                    ControlParameters::LANGUAGE => 'de_DE',
                    ControlParameters::CHANNEL => 'eur',
                    ControlParameters::CLIENT => 'at',
                ],
                'www.redhotmagma.de?',
                'www.redhotmagma.de?_language=de_DE&_channel=eur&_client=at',
            ],
            [
                [],
                'www.redhotmagma.de?utm_medium=shareUrl',
                'www.redhotmagma.de?utm_medium=shareUrl',
            ],
            [
                [
                    'something_else' => 'nothing_expected',
                    ControlParameters::CHANNEL => 'eur',
                    ControlParameters::CLIENT => 'at',
                ],
                'www.redhotmagma.de',
                'www.redhotmagma.de?_channel=eur&_client=at',
            ],
        ];
    }

    public function testShouldReturnParameter(): void
    {
        $expected = 'www.redhotmagma.de';
        $controlParamters = new ControlParameters([ControlParameters::SHARE_URL => $expected]);

        self::assertEquals($expected, $controlParamters->getParameter(ControlParameters::SHARE_URL));
    }

    public function testShouldFilterParameters(): void
    {
        $original = new ControlParameters([
            ControlParameters::ADMIN_MODE => true,
            ControlParameters::CHANNEL => 'eur',
            ControlParameters::CLIENT => 'de',
            ControlParameters::LANGUAGE => 'en_GB',
            ControlParameters::HIDE_PRICES => true,
            ControlParameters::SHARE_URL => 'www.some-url.com',
        ], true);

        $expected = new ControlParameters([
            ControlParameters::ADMIN_MODE => true,
            ControlParameters::CHANNEL => 'eur',
            ControlParameters::CLIENT => 'de',
            ControlParameters::LANGUAGE => 'en_GB',
            ControlParameters::SHARE_URL => 'www.some-url.com',
        ], true);

        $actual = $original->filter(static function (string $parameterName) {
            return ControlParameters::HIDE_PRICES !== $parameterName;
        });

        self::assertEquals($actual, $expected);
        self::assertEquals(true, $original->getParameter(ControlParameters::HIDE_PRICES), 'Cannot change original parameters');
    }
}
