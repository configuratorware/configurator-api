<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Structure\Internal;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;

/**
 * @internal
 */
final class FileResultTest extends TestCase
{
    public function testShouldCreateFileResult(): void
    {
        $fileResult = new FileResult('/path/to/file.txt', 'filename.txt', 'text/plain');

        self::assertSame('/path/to/file.txt', $fileResult->filePath);
        self::assertSame('filename.txt', $fileResult->fileName);
        self::assertSame('text/plain', $fileResult->mimeType);
    }

    /**
     * @param $givenFileName
     *
     * @dataProvider providerShouldStripSpecialCharacters
     */
    public function testShouldStripSpecialCharacters($givenFileName): void
    {
        $fileResult = new FileResult('/path/to/file.txt', $givenFileName);

        self::assertSame('filename.txt', $fileResult->fileName);
    }

    public function providerShouldStripSpecialCharacters(): \Generator
    {
        yield ['file/name.txt'];
        yield ['file\name.txt'];
        yield ['file\/name.txt'];
    }

    /**
     * @param $givenFileName
     *
     * @dataProvider providerShouldReplaceSpacesWithDash
     */
    public function testShouldReplaceSpacesWithDash($givenFileName): void
    {
        $fileResult = new FileResult('/path/to/file.txt', $givenFileName);

        self::assertSame('file_name.txt', $fileResult->fileName);
    }

    public function providerShouldReplaceSpacesWithDash(): \Generator
    {
        yield ['file name.txt'];
        yield ['file  name.txt'];
        yield ['file  \ /  name.txt'];
    }
}
