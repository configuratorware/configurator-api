<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\EventListener;

use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\StoppableEventInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\EventListener\ReceiveOffer\OfferRequestCustomerEventListener;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\CustomerEmailDataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\OfferRequestCustomerEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\CustomerOfferEmail;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\OfferRequestMailData;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\ReceiveOfferSender;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class OfferRequestCustomerEventListenerTest extends TestCase
{
    private OfferRequestCustomerEventListener $listener;

    /**
     * @var ReceiveOfferSender|\Phake_IMock
     * @Mock ReceiveOfferSender
     */
    private $receiveOfferSender;

    /**
     * @var Logger|\Phake_IMock
     * @Mock Logger
     */
    private $logger;

    /**
     * @var EventDispatcherInterface|\Phake_IMock
     * @Mock EventDispatcherInterface
     */
    private $eventDispatcher;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        \Phake::when($this->eventDispatcher)->dispatch->thenReturn(\Phake::mock(StoppableEventInterface::class));

        $this->listener = new OfferRequestCustomerEventListener(
            $this->receiveOfferSender,
            $this->logger,
            $this->eventDispatcher
        );
    }

    public function testShouldDispatchCustomerDataCollectionEventAndSendEmail(): void
    {
        $configuration = new Configuration();
        $configuration->setCode('CODE');

        $event = new OfferRequestCustomerEvent(
            $this->createClient(),
            $this->createReceiveOfferRequest(),
            new ControlParameters(),
            new OfferRequestMailData($configuration, '', '')
        );

        $this->listener->onRequest($event);

        \Phake::verify($this->eventDispatcher)->dispatch(\Phake::capture($event), 'configuratorware.receive_offer.customer_email_data_collection');
        $this->assertInstanceOf(CustomerEmailDataCollectionEvent::class, $event);

        $expectedEmail = $this->createCustomerOfferEmail();
        \Phake::verify($this->receiveOfferSender)->sendCustomerEmail($expectedEmail, $event->getHtmlTemplate(), $event->getTextTemplate(), $event->getMailAttachments());
    }

    public function testShouldDispatchCustomerDataCollectionEventAndNotSendEmail(): void
    {
        $configuration = new Configuration();
        $configuration->setCode('CODE');

        $event = new OfferRequestCustomerEvent(
            $this->createClient(false),
            $this->createReceiveOfferRequest(),
            new ControlParameters(),
            new OfferRequestMailData($configuration, '', null)
        );

        $this->listener->onRequest($event);

        \Phake::verify($this->eventDispatcher)->dispatch(\Phake::capture($event), 'configuratorware.receive_offer.customer_email_data_collection');
        $this->assertInstanceOf(CustomerEmailDataCollectionEvent::class, $event);

        \Phake::verify($this->receiveOfferSender, \Phake::never())->sendCustomerEmail(\Phake::anyParameters());
    }

    private function createReceiveOfferRequest(): ReceiveOfferRequest
    {
        $structure = new ReceiveOfferRequest();
        $structure->configurationCode = 'CODE';
        $structure->emailAddress = 'customer@test.local';
        $structure->name = 'John Customer';

        return $structure;
    }

    private function createClient($sendToCustomer = true): Client
    {
        $client = new ClientEntity();
        $client->setFromEmailAddress('from@test.local');
        $client->setToEmailAddresses('to@test.local');
        $client->setSendOfferRequestMailToCustomer($sendToCustomer);

        return Client::fromEntity($client);
    }

    private function createCustomerOfferEmail(): CustomerOfferEmail
    {
        return new CustomerOfferEmail(
            'offerRequestCustomerEmail.subject',
            'from@test.local',
            'CODE',
            'customer@test.local',
            'John Customer',
            ''
        );
    }
}
