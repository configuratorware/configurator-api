<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\EventListener\Rules;

use Doctrine\Persistence\Event\LifecycleEventArgs;
use Phake_IMock;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\EventListener\Rules\DeleteRulesCacheSubscriber;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Symfony\Contracts\Cache\CacheInterface;

class DeleteRulesCacheSubscriberTest extends TestCase
{
    /**
     * @var  ItemRepository|Phake_IMock
     */
    private $itemRepository;

    private $cache;

    private DeleteRulesCacheSubscriber $subscriber;

    public function setUp(): void
    {
        $this->itemRepository = \Phake::mock(ItemRepository::class);
        $this->cache = $this->createMock(CacheInterface::class);

        $this->subscriber = new DeleteRulesCacheSubscriber($this->itemRepository, $this->cache);
    }

    /**
     * @dataProvider provideMethods
     */
    public function testShouldDeleteCacheForItem(string $method)
    {
        $args = \Phake::mock(LifecycleEventArgs::class);
        $item = new Item();
        $item->setIdentifier('some_item_identifier');
        $rule = new Rule();
        $rule->setItem($item);

        \Phake::when($args)->getObject()->thenReturn($rule);

        $this->cache->expects(self::once())
            ->method('delete')
            ->with('rules_exist_item_some_item_identifier');

        $this->subscriber->$method($args);
    }

    /**
     * @dataProvider provideMethods
     */
    public function testShouldDeleteCacheForAllItem(string $method)
    {
        $args = \Phake::mock(LifecycleEventArgs::class);
        $item1 = \Phake::mock(Item::class);
        \Phake::when($item1)->getIdentifier()->thenReturn('item_identifier1');
        $item2 = \Phake::mock(Item::class);
        \Phake::when($item2)->getIdentifier()->thenReturn('item_identifier2');

        \Phake::when($args)->getObject()->thenReturn(new Rule());
        \Phake::when($this->itemRepository)->findAll()->thenReturn([$item1, $item2]);

        $this->cache->expects($this->at(0))
            ->method('delete')
            ->with('rules_exist_item_item_identifier1');
        $this->cache->expects($this->at(1))
            ->method('delete')
            ->with('rules_exist_item_item_identifier2');

        $this->subscriber->$method($args);
    }

    public function provideMethods(): array
    {
        return [
            ['postPersist'],
            ['postUpdate'],
            ['postRemove'],
        ];
    }
}
