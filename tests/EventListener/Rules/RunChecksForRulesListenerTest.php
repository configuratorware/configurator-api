<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\EventListener\Rules;

use Phake_IMock;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as ConfigurationEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\EventListener\Rules\RunChecksForRulesListener;
use Redhotmagma\ConfiguratorApiBundle\Events\Rules\RunChecksForRulesEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\ActionResolver;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResults;

class RunChecksForRulesListenerTest extends TestCase
{
    /**
     * @var  CheckFactory|Phake_IMock
     * @Mock CheckFactory
     */
    private $checkFactory;

    /**
     * @var  ActionResolver|Phake_IMock
     * @Mock ActionResolver
     */
    private $actionResolver;

    /**
     * @var  ConfigurationRepository|Phake_IMock
     * @Mock ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var RunChecksForRulesListener
     */
    private $listener;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);
        $this->listener = new RunChecksForRulesListener($this->checkFactory, $this->actionResolver, $this->configurationRepository);
    }

    public function testShouldRunChecksForRules()
    {
        $defaultConfiguration = new ConfigurationEntity();
        $configuration = new Configuration();
        $updatedConfiguration = clone $configuration;
        $updatedConfiguration->customdata = (object)['got updated']; // must be different from empty configuration for assertion
        $configuration->item = new Item();
        $configuration->item->identifier = $configurationidentifier = 'some_configuration';
        $rule = new Rule();
        $rules = [$rule];
        $event = new RunChecksForRulesEvent($configuration, $rules, false);
        $checkResult = new CheckResult();
        $checkResult->status = true;

        $expected = new CheckResults();
        $expected->status = true;
        $expected->check_results = [$checkResult];
        $expected->configuration = $updatedConfiguration;

        $check = \Phake::mock(Check::class);
        \Phake::when($check)->execute($rule, $configuration, $defaultConfiguration)->thenReturn($checkResult);
        \Phake::when($this->configurationRepository)->getBaseconfigurationByItemIdentifier($configurationidentifier)->thenReturn($defaultConfiguration);
        \Phake::when($this->checkFactory)->create($rule, false)->thenReturn($check);
        \Phake::when($this->actionResolver)->executeRuleActions($rule, $checkResult, $configuration)->thenReturn($updatedConfiguration);

        $this->listener->onRunChecksForRules($event);

        self::assertEquals($expected, $event->getCheckResults());
    }

    public function testShouldNotLoadBaseConfigurationWhenNoRulesAreGiven()
    {
        $configuration = new Configuration();

        $event = new RunChecksForRulesEvent($configuration, [], false);
        $this->listener->onRunChecksForRules($event);

        $expected = new CheckResults();
        $expected->status = true;
        $expected->check_results = [];
        $expected->configuration = $configuration;

        self::assertEquals($expected, $event->getCheckResults());

        \Phake::verifyNoInteraction($this->configurationRepository);
    }
}
