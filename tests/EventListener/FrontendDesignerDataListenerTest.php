<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\EventListener;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\EventListener\ConfigurationMode\FrontendDesignerDataListener;
use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationMode\FrontendDesignerDataEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\FrontendCreatorDesignerData;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\FrontendDesignerData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item as ItemStructure;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\KernelAwareTestTrait;

class FrontendDesignerDataListenerTest extends TestCase
{
    use KernelAwareTestTrait;

    private FrontendDesignerDataListener $listener;

    /**
     * @var ItemRepository|\Phake_IMock
     * @Mock ItemRepository
     */
    private $itemRepository;
    /**
     * @var FrontendCreatorDesignerData|\Phake_IMock
     * @Mock FrontendCreatorDesignerData
     */
    private $frontendCreatorDesignerData;

    /**
     * @var FrontendDesignerData|\Phake_IMock
     * @Mock FrontendDesignerData
     */
    private $frontendDesignerData;

    /**
     * @var Item|\Phake_IMock
     * @Mock Item
     */
    private $item;

    public function setUp(): void
    {
        parent::setUp();
        \Phake::initAnnotations($this);
        \Phake::when($this->itemRepository)->findOneForConfigurationMode(\Phake::anyParameters())->thenReturn($this->item);
        \Phake::when($this->item)->getParent()->thenReturn(null);
        $this->listener = new FrontendDesignerDataListener($this->itemRepository, $this->frontendCreatorDesignerData, $this->frontendDesignerData);
    }

    public function testShouldGetDesignerData()
    {
        \Phake::when($this->item)->getConfigurationMode()->thenReturn(Item::CONFIGURATION_MODE_DESIGNER);

        $configuration = new Configuration();
        $configuration->item = new ItemStructure();
        $configuration->item->identifier = 'designer_item';
        $event = new FrontendDesignerDataEvent($configuration);

        $this->listener->onGetDesignerData($event);

        \Phake::verify($this->frontendDesignerData)->provideForItem($this->item);
        \Phake::verifyNoInteraction($this->frontendCreatorDesignerData);
    }

    public function testShouldGetCreatorData()
    {
        \Phake::when($this->item)->getConfigurationMode()->thenReturn(Item::CONFIGURATION_MODE_CREATOR_DESIGNER);

        $configuration = new Configuration();
        $configuration->item = new ItemStructure();
        $configuration->item->identifier = 'creator_designer_item';
        $event = new FrontendDesignerDataEvent($configuration);

        $this->listener->onGetDesignerData($event);

        \Phake::verify($this->frontendCreatorDesignerData)->provideForItem($this->item);
        \Phake::verifyNoInteraction($this->frontendDesignerData);
    }
}
