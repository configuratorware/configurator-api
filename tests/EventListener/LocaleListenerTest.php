<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\EventListener;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\EventListener\LocaleListener;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\KernelAwareTestTrait;

class LocaleListenerTest extends TestCase
{
    use KernelAwareTestTrait;

    /**
     * @var LocaleListener
     */
    private $listener;

    /**
     * @var Request|\Phake_IMock
     * @Mock Request
     */
    private $request;

    public function setUp(): void
    {
        parent::setUp();
        \Phake::initAnnotations($this);

        $languageRepository = $this->getRepository(Language::class);
        $this->listener = new LocaleListener($languageRepository);
    }

    /**
     * @runInSeparateProcess
     * @preserveGlobalState disabled
     *
     * @param $expectedIso
     * @param $expectedSettings
     * @param $givenLocale
     *
     * @throws \Exception
     *
     * @dataProvider providerShouldSetConstants
     */
    public function testShouldSetConstants($expectedIso, $expectedSettings, $givenLocale)
    {
        \Phake::when($this->request)->get(ControlParameters::LANGUAGE)->thenReturn($givenLocale);

        $event = new RequestEvent($this->createKernel(), $this->request, HttpKernelInterface::MASTER_REQUEST);

        $this->listener->onKernelRequest($event);

        self::assertEquals($expectedIso, C_LANGUAGE_ISO);
        self::assertEquals($expectedSettings, C_LANGUAGE_SETTINGS);
    }

    public function providerShouldSetConstants()
    {
        return [
            [
                // expected C_LANGUAGE_ISO
                'de_DE',
                // expected C_LANGUAGE_SETTINGS
                [
                    'id' => '2',
                    'iso' => 'de_DE',
                    'dateformat' => 'd.m.Y',
                    'pricedecimals' => 2,
                    'decimalpoint' => ',',
                    'thousandsseparator' => '.',
                    'currencysymbolposition' => 'right',
                    'user_created_id' => 0,
                    'numberdecimals' => null,
                ],
                // given iso
                'de_DE',
            ],
            [
                // expected C_LANGUAGE_ISO
                'en_GB',
                // expected C_LANGUAGE_SETTINGS
                [
                    'id' => '1',
                    'iso' => 'en_GB',
                    'dateformat' => 'm/d/Y',
                    'pricedecimals' => 2,
                    'decimalpoint' => '.',
                    'thousandsseparator' => ',',
                    'currencysymbolposition' => 'left',
                    'user_created_id' => 0,
                    'numberdecimals' => null,
                ],
                // given iso
                'en_GB',
            ],
            [
                // expected C_LANGUAGE_ISO
                'en_GB',
                // expected C_LANGUAGE_SETTINGS
                [
                    'id' => '1',
                    'iso' => 'en_GB',
                    'dateformat' => 'm/d/Y',
                    'pricedecimals' => 2,
                    'decimalpoint' => '.',
                    'thousandsseparator' => ',',
                    'currencysymbolposition' => 'left',
                    'user_created_id' => 0,
                    'numberdecimals' => null,
                ],
                // given iso
                null,
            ],
            [
                // expected C_LANGUAGE_ISO
                'en_GB',
                // expected C_LANGUAGE_SETTINGS
                [
                    'id' => '1',
                    'iso' => 'en_GB',
                    'dateformat' => 'm/d/Y',
                    'pricedecimals' => 2,
                    'decimalpoint' => '.',
                    'thousandsseparator' => ',',
                    'currencysymbolposition' => 'left',
                    'user_created_id' => 0,
                    'numberdecimals' => null,
                ],
                // given iso
                'non_EXISTING',
            ],
        ];
    }
}
