<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\EventListener;

use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Psr\EventDispatcher\StoppableEventInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\EventListener\ReceiveOffer\ReceiveOfferRequestEventListener;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\EmailDataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\ReceiveOfferRequestEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\OfferRequestMailData;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\ReceiveOfferEmail;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\ReceiveOfferSender;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ReceiveOfferRequestEventListenerTest extends TestCase
{
    private ReceiveOfferRequestEventListener $listener;

    /**
     * @var ReceiveOfferSender|\Phake_IMock
     * @Mock ReceiveOfferSender
     */
    private $receiveOfferSender;

    /**
     * @var Logger|\Phake_IMock
     * @Mock Logger
     */
    private $logger;

    /**
     * @var EventDispatcherInterface|\Phake_IMock
     * @Mock EventDispatcherInterface
     */
    private $eventDispatcher;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        \Phake::when($this->eventDispatcher)->dispatch->thenReturn(\Phake::mock(StoppableEventInterface::class));

        $this->listener = new ReceiveOfferRequestEventListener(
            $this->receiveOfferSender,
            $this->logger,
            $this->eventDispatcher
        );
    }

    public function testShouldDispatchDataCollectionEventAndSendEmail(): void
    {
        $configuration = new Configuration();
        $configuration->setCode('CODE');

        $event = new ReceiveOfferRequestEvent(
            $this->createReceiveOfferRequest(),
            new ControlParameters(),
            $this->createClient(),
            new OfferRequestMailData($configuration, '', '')
        );

        $this->listener->onRequest($event);

        \Phake::verify($this->eventDispatcher)->dispatch(\Phake::capture($event), 'configuratorware.receive_offer.email_data_collection');
        $this->assertInstanceOf(EmailDataCollectionEvent::class, $event);

        $expectedEmail = $this->createOfferEmail();
        \Phake::verify($this->receiveOfferSender)->send($expectedEmail, $event->getHtmlTemplate(), $event->getTextTemplate(), $event->getMailAttachments());
    }

    private function createReceiveOfferRequest(): ReceiveOfferRequest
    {
        $structure = new ReceiveOfferRequest();
        $structure->configurationCode = 'CODE';
        $structure->emailAddress = 'customer@test.local';
        $structure->name = 'John Customer';

        return $structure;
    }

    private function createClient(): Client
    {
        $client = new ClientEntity();
        $client->setFromEmailAddress('from@test.local');
        $client->setToEmailAddresses('to@test.local');

        return Client::fromEntity($client);
    }

    private function createOfferEmail(): ReceiveOfferEmail
    {
        return new ReceiveOfferEmail(
            'receiveOffersRequestMail.subject',
            'from@test.local',
            ['to@test.local'],
            [],
            [],
            'CODE',
            'customer@test.local',
            'John Customer',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            '',
            ''
        );
    }
}
