<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\Paths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class PathsTest extends ApiTestCase
{
    private $paths;
    private $projectDir;

    public function setUp(): void
    {
        parent::setUp();
        $this->paths = self::$kernel->getContainer()->get(Paths::class);
        $this->projectDir = self::$kernel->getContainer()->getParameter('redhotmagma_configurator_api.project_dir');
    }

    /**
     * @param string $expectedPath
     * @param string $method
     *
     * @dataProvider providerShouldVerifyPathsRelative
     */
    public function testShouldVerifyPathsRelative(string $expectedPath, string $method): void
    {
        self::assertSame($expectedPath, $this->paths->$method());
    }

    public function providerShouldVerifyPathsRelative(): \Generator
    {
        yield ['images/components', 'getComponentThumbnailPathRelative'];
        yield ['designer/fonts', 'getFontPathRelative'];
        yield ['images/item/view', 'getDesignViewImagePathRelative'];
        yield ['images/item/view_thumb', 'getDesignViewThumbnailPathRelative'];
        yield ['images/configuratorimages', 'getLayerImagePathRelative'];
        yield ['images/item/image', 'getItemDetailImagePathRelative'];
        yield ['images/item/thumb', 'getItemThumbnailPathRelative'];
        yield ['images/options', 'getOptionThumbnailPathRelative'];
    }

    /**
     * @param string $expectedPath
     * @param string $method
     *
     * @dataProvider providerShouldVerifyPaths
     */
    public function testShouldVerifyPaths(string $expectedPath, string $method): void
    {
        self::assertSame($this->projectDir . $expectedPath, $this->paths->$method());
    }

    public function providerShouldVerifyPaths(): \Generator
    {
        yield ['/tests/Controller/_data/media/images/components', 'getComponentThumbnailPath'];
        yield ['/tests/Controller/_data/media/designer/fonts', 'getFontPath'];
        yield ['/tests/Controller/_data/License/license.txt', 'getLicenseFilePath'];
        yield ['/tests/Controller/_data/media/images/item/view', 'getDesignViewImagePath'];
        yield ['/tests/Controller/_data/media/images/printfiles', 'getDesignAreaPrintfilePath'];
        yield ['/tests/Controller/_data/media/images/item/view_thumb', 'getDesignViewThumbnailPath'];
        yield ['/tests/Controller/_data/media/images/configuratorimages', 'getLayerImagePath'];
        yield ['/tests/Controller/_data/media/images/item/image', 'getItemDetailImagePath'];
        yield ['/tests/Controller/_data/media/images/item/thumb', 'getItemThumbnailPath'];
        yield ['/tests/Controller/_data/media/images/options', 'getOptionThumbnailPath'];
    }

    /**
     * @dataProvider providerShouldGetClientPaths
     */
    public function testShouldGetClientPaths(string $expectedPath, string $method): void
    {
        self::assertSame($this->projectDir . $expectedPath, $this->paths->$method('clientIdentifier'));
    }

    public function providerShouldGetClientPaths(): \Generator
    {
        yield ['/tests/Controller/_data/media/client/clientIdentifier/font', 'getClientFontPath'];
        yield ['/tests/Controller/_data/media/client/clientIdentifier/logo', 'getClientLogoPath'];
    }

    /**
     * @dataProvider providerShouldGetClientPathsRelative
     */
    public function testShouldGetClientPathsRelative(string $expectedPath, string $method): void
    {
        self::assertSame($expectedPath, $this->paths->$method('clientIdentifier'));
    }

    public function providerShouldGetClientPathsRelative(): \Generator
    {
        yield ['client/clientIdentifier/font', 'getClientFontPathRelative'];
        yield ['client/clientIdentifier/logo', 'getClientLogoPathRelative'];
    }
}
