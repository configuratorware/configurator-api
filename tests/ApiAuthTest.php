<?php

namespace Redhotmagma\ConfiguratorApiBundle;

use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ApiAuthTest extends ApiTestCase
{
    public function testAuth(): void
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"username": "admin", "password": "admin"}'
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertStringContainsString('token', $response->getContent(), 'No token found in response');
    }
}
