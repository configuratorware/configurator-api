<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ConfigurationLoadDataResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConfigurationLoadArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ConfigurationLoadDataResolverTest extends TestCase
{
    private ConfigurationLoadDataResolver $resolver;

    protected function setUp(): void
    {
        $this->resolver = new ConfigurationLoadDataResolver();
    }

    public function testShouldResolveConfigurationLoadData(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', ConfigurationLoadArguments::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertTrue($supports);
    }

    public function testCannotSupportOtherClasses(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', \stdClass::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertFalse($supports);
    }

    public function testShouldResolveArgument(): void
    {
        $query = ['_no_check' => '1', '_switch_options' => '{"coat":[{"identifier":"coat_red", "amount": 1}]}'];
        $request = new Request($query, [], [], [], [], []);
        $metaData = new ArgumentMetadata('', ConfigurationLoadArguments::class, false, false, null, true);

        $switchOption = new \stdClass();
        $switchOption->identifier = 'coat_red';
        $switchOption->amount = 1;
        $expected = ConfigurationLoadArguments::from(['coat' => [$switchOption]], true);

        $generator = $this->resolver->resolve($request, $metaData);
        $result = $generator->current();

        self::assertEquals(ConfigurationLoadArguments::class, get_class($result));
        self::assertEquals($expected->noCheck, $result->noCheck);
        self::assertEquals($expected->switchedOptions, $result->switchedOptions);
    }

    public function testShouldResolveDefaultArguments(): void
    {
        $request = new Request([], [], [], [], [], []);
        $metaData = new ArgumentMetadata('', ConfigurationLoadArguments::class, false, false, null, true);

        $expected = ConfigurationLoadArguments::from();

        $generator = $this->resolver->resolve($request, $metaData);
        $result = $generator->current();

        self::assertEquals(ConfigurationLoadArguments::class, get_class($result));
        self::assertEquals($expected->noCheck, $result->noCheck);
        self::assertEquals($expected->switchedOptions, $result->switchedOptions);
    }
}
