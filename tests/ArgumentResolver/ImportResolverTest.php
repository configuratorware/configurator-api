<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ImportResolver;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConnectorImportArguments;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ImportResolverTest extends TestCase
{
    /**
     * @var ImportResolver
     */
    private $importResolver;

    /**
     * @var ArgumentMetadata|\Phake_IMock
     * @Mock ArgumentMetadata
     */
    private $argumentMetaData;

    /**
     * @var Request|\Phake_IMock
     * @Mock Request
     */
    private $request;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->importResolver = new ImportResolver();
        \Phake::when($this->request)->getContent()->thenReturn('DUMMY CONTENT');
        \Phake::when($this->argumentMetaData)->getType()->thenReturn(ConnectorImportArguments::class);
    }

    public function testShouldResolveConnectorImportArguments(): void
    {
        self::assertTrue($this->importResolver->supports($this->request, $this->argumentMetaData));
    }

    /**
     * @param array $givenRequestOptions
     *
     * @dataProvider provideOptionData
     */
    public function testImportOptionCreation(array $givenRequestOptions, array $expectedOptionResultArr)
    {
        $this->request->query = new ParameterBag($givenRequestOptions);
        $expectedOptionResult = $this->createImportOptions($expectedOptionResultArr);

        $resolver = $this->importResolver->resolve(
            $this->request,
            $this->argumentMetaData
        );
        $result = $resolver->current();
        /* @var $result ConnectorImportArguments */

        self::assertSame(ConnectorImportArguments::class, get_class($result));
        self::assertEquals($expectedOptionResult, $result->importOptions);
        self::assertEquals('DUMMY CONTENT', $result->importBody);
    }

    /**
     * @return array
     */
    public function provideOptionData(): array
    {
        return [
            [
                [
                    'deleteNonExistingItems' => 1,
                    'deleteNonExistingAttributeRelations' => 1,
                    'deleteNonExistingItemPrices' => 1,
                    'deleteNonExistingDesignAreas' => 1,
                    'deleteNonExistingDesignAreaVisualData' => 1,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => 1,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => 1,
                    'deleteNonExistingComponentRelations' => 1,
                    'overwriteItemImages' => 1,
                ],
                [
                    'deleteNonExistingItems' => true,
                    'deleteNonExistingAttributeRelations' => true,
                    'deleteNonExistingItemPrices' => true,
                    'deleteNonExistingDesignAreas' => true,
                    'deleteNonExistingDesignAreaVisualData' => true,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => true,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => true,
                    'deleteNonExistingComponentRelations' => true,
                    'overwriteItemImages' => true,
                ],
            ],
            [
                [
                    'deleteNonExistingItems' => '1',
                    'deleteNonExistingAttributeRelations' => '1',
                    'deleteNonExistingItemPrices' => '1',
                    'deleteNonExistingDesignAreas' => '1',
                    'deleteNonExistingDesignAreaVisualData' => '1',
                    'deleteNonExistingDesignAreaDesignProductionMethod' => '1',
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => '1',
                    'deleteNonExistingComponentRelations' => '1',
                    'overwriteItemImages' => '1',
                ],
                [
                    'deleteNonExistingItems' => true,
                    'deleteNonExistingAttributeRelations' => true,
                    'deleteNonExistingItemPrices' => true,
                    'deleteNonExistingDesignAreas' => true,
                    'deleteNonExistingDesignAreaVisualData' => true,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => true,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => true,
                    'deleteNonExistingComponentRelations' => true,
                    'overwriteItemImages' => true,
                ],
            ],
            [
                [
                    'deleteNonExistingItems' => 'true',
                    'deleteNonExistingAttributeRelations' => 'true',
                    'deleteNonExistingItemPrices' => 'true',
                    'deleteNonExistingDesignAreas' => 'true',
                    'deleteNonExistingDesignAreaVisualData' => 'true',
                    'deleteNonExistingDesignAreaDesignProductionMethod' => 'true',
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => 'true',
                    'deleteNonExistingComponentRelations' => 'true',
                    'overwriteItemImages' => 'true',
                ],
                [
                    'deleteNonExistingItems' => true,
                    'deleteNonExistingAttributeRelations' => true,
                    'deleteNonExistingItemPrices' => true,
                    'deleteNonExistingDesignAreas' => true,
                    'deleteNonExistingDesignAreaVisualData' => true,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => true,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => true,
                    'deleteNonExistingComponentRelations' => true,
                    'overwriteItemImages' => true,
                ],
            ],
            [
                [
                    'deleteNonExistingItems' => 0,
                    'deleteNonExistingAttributeRelations' => 0,
                    'deleteNonExistingItemPrices' => 0,
                    'deleteNonExistingDesignAreas' => 0,
                    'deleteNonExistingDesignAreaVisualData' => 0,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => 0,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => 0,
                    'deleteNonExistingComponentRelations' => 0,
                    'overwriteItemImages' => 0,
                ],
                [
                    'deleteNonExistingItems' => false,
                    'deleteNonExistingAttributeRelations' => false,
                    'deleteNonExistingItemPrices' => false,
                    'deleteNonExistingDesignAreas' => false,
                    'deleteNonExistingDesignAreaVisualData' => false,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => false,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => false,
                    'deleteNonExistingComponentRelations' => false,
                    'overwriteItemImages' => false,
                ],
            ],
            [
                [
                    'deleteNonExistingItems' => '0',
                    'deleteNonExistingAttributeRelations' => '0',
                    'deleteNonExistingItemPrices' => '0',
                    'deleteNonExistingDesignAreas' => '0',
                    'deleteNonExistingDesignAreaVisualData' => '0',
                    'deleteNonExistingDesignAreaDesignProductionMethod' => '0',
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => '0',
                    'deleteNonExistingComponentRelations' => '0',
                    'overwriteItemImages' => '0',
                ],
                [
                    'deleteNonExistingItems' => false,
                    'deleteNonExistingAttributeRelations' => false,
                    'deleteNonExistingItemPrices' => false,
                    'deleteNonExistingDesignAreas' => false,
                    'deleteNonExistingDesignAreaVisualData' => false,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => false,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => false,
                    'deleteNonExistingComponentRelations' => false,
                    'overwriteItemImages' => false,
                ],
            ],
            [
                [
                    'deleteNonExistingItems' => 'false',
                    'deleteNonExistingAttributeRelations' => 'false',
                    'deleteNonExistingItemPrices' => 'false',
                    'deleteNonExistingDesignAreas' => 'false',
                    'deleteNonExistingDesignAreaVisualData' => 'false',
                    'deleteNonExistingDesignAreaDesignProductionMethod' => 'false',
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => 'false',
                    'deleteNonExistingComponentRelations' => 'false',
                    'overwriteItemImages' => 'false',
                ],
                [
                    'deleteNonExistingItems' => false,
                    'deleteNonExistingAttributeRelations' => false,
                    'deleteNonExistingItemPrices' => false,
                    'deleteNonExistingDesignAreas' => false,
                    'deleteNonExistingDesignAreaVisualData' => false,
                    'deleteNonExistingDesignAreaDesignProductionMethod' => false,
                    'deleteNonExistingDesignAreaDesignProductionMethodPrices' => false,
                    'deleteNonExistingComponentRelations' => false,
                    'overwriteItemImages' => false,
                ],
            ],
        ];
    }

    /**
     * @param array $options
     *
     * @return ImportOptions
     */
    private function createImportOptions(array $options): ImportOptions
    {
        $optionObj = new ImportOptions();
        foreach ($options as $key => $value) {
            $optionObj->$key = $value;
        }

        return $optionObj;
    }
}
