<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ConfigurationInfoResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConfigurationInfoArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ConfigurationInfoResolverTest extends TestCase
{
    private ConfigurationInfoResolver $resolver;

    protected function setUp(): void
    {
        $this->resolver = new ConfigurationInfoResolver();
    }

    public function testShouldResolveConfigurationInfoARguments(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', ConfigurationInfoArguments::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertTrue($supports);
    }

    public function testCannotSupportOtherClasses(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', \stdClass::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertFalse($supports);
    }

    public function testShouldResolveArgument(): void
    {
        $query = ['_extended_data' => '1'];
        $request = new Request($query, [], [], [], [], []);
        $metaData = new ArgumentMetadata('', ConfigurationInfoArguments::class, false, false, null, true);

        $expected = new ConfigurationInfoArguments();
        $expected->extendedData = true;

        $generator = $this->resolver->resolve($request, $metaData);
        $result = $generator->current();

        self::assertEquals(ConfigurationInfoArguments::class, get_class($result));
        self::assertEquals($expected->extendedData, $result->extendedData);
    }

    public function testShouldResolveDefaultArguments(): void
    {
        $request = new Request([], [], [], [], [], []);
        $metaData = new ArgumentMetadata('', ConfigurationInfoArguments::class, false, false, null, true);

        $expected = new ConfigurationInfoArguments();

        $generator = $this->resolver->resolve($request, $metaData);
        $result = $generator->current();

        self::assertEquals(ConfigurationInfoArguments::class, get_class($result));
        self::assertEquals($expected->extendedData, $result->extendedData);
    }
}
