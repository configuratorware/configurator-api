<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ItemStatusResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ItemStatusResolverTest extends TestCase
{
    /**
     * @var ItemStatusResolver
     */
    private $resolver;

    protected function setUp(): void
    {
        $this->resolver = new ItemStatusResolver(new StructureFromDataConverter());
    }

    public function testShouldResolveItems(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', ItemStatus::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertTrue($supports);
    }

    public function testCannotSupportOtherClasses(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', \stdClass::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertFalse($supports);
    }

    public function testShouldResolveArgument(): void
    {
        $request = new Request([], [], [], [], [], [], '{"itemStatusId":1}');
        $metaData = new ArgumentMetadata('', ItemStatus::class, false, false, null, true);

        $expected = new ItemStatus();
        $expected->id = 1;

        $generator = $this->resolver->resolve($request, $metaData);
        $result = $generator->current();

        self::assertEquals(ItemStatus::class, get_class($result));
        self::assertEquals($expected->id, $result->id);
    }

    /**
     * @dataProvider provideDataForExceptionTesting
     *
     * @param string $requestContent
     */
    public function testShouldThrowInvalidArgumentException(string $requestContent): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $request = new Request([], [], [], [], [], [], $requestContent);
        $metaData = new ArgumentMetadata('', ItemStatus::class, true, false, null, true);

        $generator = $this->resolver->resolve($request, $metaData);
        $generator->current();
    }

    public function provideDataForExceptionTesting(): array
    {
        return [
            ['{}'],
            ['{"itemStatusId": "fake"}'],
            ['{"itemStatusId": ["fake"]}'],
        ];
    }
}
