<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ItemClassificationFilterResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ItemClassificationFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ItemClassificationFilterResolverTest extends TestCase
{
    public function testShouldResolveWithItemClassificationIds(): void
    {
        $expected = new ItemClassificationFilter();
        $expected->setItemClassificationsIds([1, 3]);

        $request = new Request();
        $request->query->set('item-classifications', '[1,3]');

        $resolver = new ItemClassificationFilterResolver();
        $actual = $resolver->resolve(
            $request,
            new ArgumentMetadata(
                'itemClassificationFilter',
                ItemClassificationFilter::class,
                false,
                false,
                null,
                false
            )
        );

        self::assertEquals($expected, $actual->current());
    }
}
