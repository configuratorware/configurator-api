<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ItemDuplicateResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemDuplicate;
use Redhotmagma\ConfiguratorApiBundle\Structure\Itemtext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ItemDuplicateResolverTest extends TestCase
{
    private ItemDuplicateResolver $resolver;

    protected function setUp(): void
    {
        $this->resolver = new ItemDuplicateResolver(new StructureFromDataConverter());
    }

    public function testShouldSupportItemDuplicate(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', ItemDuplicate::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertTrue($supports);
    }

    public function testCannotSupportOtherClasses(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', \stdClass::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertFalse($supports);
    }

    /**
     * @dataProvider provideDataForResolverTesting
     */
    public function testShouldResolveItemDuplicate(string $requestContent, ItemDuplicate $expected): void
    {
        $request = new Request([], [], [], [], [], [], $requestContent);
        $metaData = new ArgumentMetadata('', ItemDuplicate::class, true, false, null, true);

        $actual = $this->resolver->resolve($request, $metaData);

        self::assertEquals($expected, $actual->current());
    }

    public function provideDataForResolverTesting(): array
    {
        $itemDuplicate1 = new ItemDuplicate();
        $itemDuplicate1->identifier = 'duplicate1';
        $itemDuplicate1->_metadata = new \stdClass();
        $itemDuplicate1->_metadata->changedProperties = ['identifier'];

        $itemDuplicate2 = new ItemDuplicate();
        $itemDuplicate2->identifier = 'duplicate2';
        $itemText1 = new Itemtext();
        $itemText1->language = 'de_DE';
        $itemText1->title = 'Title';
        $itemText1->abstract = 'Abstract';
        $itemText1->description = 'Description';
        $itemText1->_metadata = new \stdClass();
        $itemText1->_metadata->changedProperties = ['language', 'title', 'abstract', 'description'];
        $itemText2 = new Itemtext();
        $itemText2->language = 'en_GB';
        $itemText2->title = 'Title EN';
        $itemText2->_metadata = new \stdClass();
        $itemText2->_metadata->changedProperties = ['language', 'title'];
        $itemDuplicate2->itemTexts = [$itemText1, $itemText2];
        $itemDuplicate2->_metadata = new \stdClass();
        $itemDuplicate2->_metadata->changedProperties = ['identifier', 'itemTexts'];

        return [
            ['{"identifier": "duplicate1"}', $itemDuplicate1],
            ['{"identifier": "duplicate2", "itemTexts": [{"language": "de_DE", "title": "Title", "abstract": "Abstract", "description": "Description"}, {"language": "en_GB", "title": "Title EN"}]}', $itemDuplicate2],
        ];
    }
}
