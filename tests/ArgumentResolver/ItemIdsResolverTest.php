<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\ArgumentResolver\ItemIdsResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ItemIdsResolverTest extends TestCase
{
    /**
     * @var ItemIdsResolver
     */
    private $resolver;

    protected function setUp(): void
    {
        $this->resolver = new ItemIdsResolver(new StructureFromDataConverter());
    }

    public function testShouldResolveItems(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', Item::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertTrue($supports);
    }

    public function testShouldResolveOnlyVariadic(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', Item::class, false, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertFalse($supports);
    }

    public function testCannotSupportOtherClasses(): void
    {
        $request = new Request();
        $metadata = new ArgumentMetadata('', \stdClass::class, true, false, null, true);

        $supports = $this->resolver->supports($request, $metadata);

        self::assertFalse($supports);
    }

    public function testShouldResolveArgument(): void
    {
        $request = new Request([], [], [], [], [], [], '{"itemIds":[1]}');
        $metaData = new ArgumentMetadata('', Item::class, true, false, null, true);

        $expected = new Item();
        $expected->id = 1;

        $generator = $this->resolver->resolve($request, $metaData);
        $result = $generator->current();

        self::assertEquals(Item::class, get_class($result));
        self::assertEquals($expected->id, $result->id);
    }

    /**
     * @dataProvider provideDataForExceptionTesting
     *
     * @param string $requestContent
     */
    public function testShouldThrowInvalidArgumentException(string $requestContent): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $request = new Request([], [], [], [], [], [], $requestContent);
        $metaData = new ArgumentMetadata('', Item::class, true, false, null, true);

        $generator = $this->resolver->resolve($request, $metaData);
        $generator->current();
    }

    public function provideDataForExceptionTesting(): array
    {
        return [
            ['{}'],
            ['{"itemIds": "fake"}'],
            ['{"itemIds": ["fake"]}'],
        ];
    }
}
