<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;

class ConfiguratorwareExportItemCommandTest extends KernelTestCase
{
    private vfsStreamDirectory $root;

    public function testShouldCreateExportFile()
    {
        $this->root = vfsStream::setup();

        $kernel = self::bootKernel();
        $application = new Application($kernel);
        $fs = new Filesystem();
        $fs->mkdir(__DIR__ . '/exported');
        $command = $application->find('configuratorware:export');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            '--output-folder' => $this->root->url(),
        ]);

        $commandTester->assertCommandIsSuccessful();
        $exportFile = $this->root->url() . '/sheep.json';
        $this->assertFileExists($exportFile);
        $actual = json_decode(file_get_contents($exportFile));
        $expected = json_decode(file_get_contents(__DIR__ . '/exported/expected.json'));
        self::assertEqualsCanonicalizing($expected, $actual);
    }
}
