<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityHelper;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureHelper;
use Redhotmagma\ApiBundle\Service\Helper\StringHelper;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Language\FrontendLanguageStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Language\LanguageApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Language\LanguageEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Language\LanguageSave;
use Redhotmagma\ConfiguratorApiBundle\Service\Language\LanguageStructureFromEntityConverter;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DefaultLanguageCommandTest extends ApiTestCase
{
    /**
     * @var DefaultLanguageCommand
     */
    private $command;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->languageRepository = $this->em->getRepository(Language::class);
        $EntityFromStructureConverter = new EntityFromStructureConverter(new EntityHelper(new StringHelper()));
        $structureFromEntityConverter = new StructureFromEntityConverter(new StringHelper(), new StructureHelper());

        $languageEntityFromStructureConverter = new LanguageEntityFromStructureConverter($EntityFromStructureConverter);
        $languageStructureFromEntityConverter = new LanguageStructureFromEntityConverter($structureFromEntityConverter);
        $languageApi = new LanguageApi(
            $this->languageRepository,
            new FrontendLanguageStructureFromEntityConverter($structureFromEntityConverter),
            new LanguageSave(
                $this->languageRepository,
                $languageEntityFromStructureConverter,
                $languageStructureFromEntityConverter
            ),
            $languageStructureFromEntityConverter
        );

        $this->command = new DefaultLanguageCommand($this->languageRepository, $languageApi);
    }

    /**
     * @param string $iso
     * @dataProvider providerShouldSetLanguageAsDefault
     */
    public function testShouldSetLanguageAsDefault(string $iso)
    {
        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:default-language');

        $tester = new CommandTester($command);

        $tester->execute(['command' => $command->getName(), 'iso' => $iso]);

        $default = $this->languageRepository->findOneBy(['is_default' => true]);

        self::assertEquals($iso, $default->getIso());
    }

    public function providerShouldSetLanguageAsDefault()
    {
        return [
            [
                'de_DE',
            ],
            [
                'en_GB',
            ],
        ];
    }
}
