<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CreateEnvFileCommandTest extends ApiTestCase
{
    /**
     * @var CreateEnvFileCommand
     */
    private $command;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var vfsStreamDirectory
     */
    private $projectDir;

    public function setUp(): void
    {
        parent::setUp();

        $this->filesystem = new Filesystem();
        $this->projectDir = vfsStream::setup()->url();

        $this->command = new CreateEnvFileCommand($this->filesystem, $this->projectDir);
    }

    public function tearDown(): void
    {
        $this->filesystem->remove($this->projectDir . '/.env.local');
    }

    public function testShouldCreateNonEmptyFile()
    {
        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:create-env-file');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'username' => 'username',
            'password' => 'password',
            'host' => 'host',
            'port' => 'port',
            'db_name' => 'db_name',
        ]);

        $filepath = $this->projectDir . '/.env.local';
        self::assertFileExists($filepath);
        self::assertStringEqualsFile(
            $filepath,
            'DATABASE_URL=mysql://username:password@host:port/db_name
APP_ENV=prod'
        );
    }

    public function testShouldCreateWithEnv()
    {
        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:create-env-file');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'username' => 'username',
            'password' => 'password',
            'host' => 'host',
            'port' => 'port',
            'db_name' => 'db_name',
            'env' => 'dev',
        ]);

        $filepath = $this->projectDir . '/.env.local';
        self::assertFileExists($filepath);
        self::assertStringEqualsFile(
            $filepath,
            'DATABASE_URL=mysql://username:password@host:port/db_name
APP_ENV=dev'
        );
    }

    public function testShouldThrowExceptionWhenFileAlreadyExists()
    {
        $this->filesystem->dumpFile($this->projectDir . '/.env.local', '');

        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:create-env-file');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'username' => 'username',
            'password' => 'password',
            'host' => 'host',
            'port' => 'port',
            'db_name' => 'db_name',
        ]);

        self::assertEquals(
            'A .env.local file already exists and won\'t be overwritten',
            trim($commandTester->getDisplay())
        );
        self::assertEquals(0, $commandTester->getStatusCode());
    }
}
