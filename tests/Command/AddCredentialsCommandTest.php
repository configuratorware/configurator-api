<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Entity\Credential;
use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential;
use Redhotmagma\ConfiguratorApiBundle\Repository\CredentialRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseApi;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseChecker;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @BackupDatabase
 */
class AddCredentialsCommandTest extends ApiTestCase
{
    /**
     * @var AddCredentialsCommand
     */
    private $command;

    /**
     * @var CredentialRepository
     */
    private $credentialRepository;

    /**
     * @var LicenseApi|\Phake_IMock
     * @Mock LicenseApi
     */
    private $licenceApi;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var LicenseChecker
     */
    private $licenseChecker;

    /**
     * @var array
     */
    private $credentials;

    public function setUp(): void
    {
        parent::setUp();
        \Phake::initAnnotations($this);

        $this->credentialRepository = $this->em->getRepository(Credential::class);
        $this->roleRepository = $this->em->getRepository(Role::class);
        $this->licenseChecker = new LicenseChecker();
        $this->credentials = static::$kernel->getContainer()->getParameter('credentials');

        $this->command = new AddCredentialsCommand(
            $this->credentialRepository,
            $this->licenceApi,
            $this->roleRepository,
            $this->licenseChecker,
            $this->credentials
        );
    }

    public function testShouldSaveCredentials()
    {
        $amountNewCredentials = 1;
        $amountNewRoleCredentials = 1;

        $this->executeSql('DELETE FROM credential WHERE area = \'items\';');

        $roleCredentialRepository = $this->em->getRepository(RoleCredential::class);

        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:add-credentials');

        $expectedCredential = count($this->credentialRepository->findAll()) + $amountNewCredentials;
        $expectedRoleCredential = count($roleCredentialRepository->findAll()) + $amountNewRoleCredentials;

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'license' => 'creator',
        ]);

        $actualCredential = count($this->credentialRepository->findAll());
        $actualRoleCredential = count($roleCredentialRepository->findAll());

        self::assertEquals($expectedCredential, $actualCredential);
        self::assertEquals($expectedRoleCredential, $actualRoleCredential);
    }

    public function testShouldNotSaveCredentialsWhenThereIsNoNewCredential()
    {
        $amountNewCredentials = 0;
        $amountNewRoleCredentials = 0;

        $roleCredentialRepository = $this->em->getRepository(RoleCredential::class);

        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:add-credentials');

        $expectedCredential = count($this->credentialRepository->findAll()) + $amountNewCredentials;
        $expectedRoleCredential = count($roleCredentialRepository->findAll()) + $amountNewRoleCredentials;

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'license' => 'creator',
        ]);

        $actualCredential = count($this->credentialRepository->findAll());
        $actualRoleCredential = count($roleCredentialRepository->findAll());

        self::assertEquals($expectedCredential, $actualCredential);
        self::assertEquals($expectedRoleCredential, $actualRoleCredential);
    }

    public function testShouldNotSaveCredentialsWhenConfigurationModeIsInvalidForLicense()
    {
        $amountNewCredentials = 0;
        $amountNewRoleCredentials = 0;

        $this->executeSql('DELETE FROM credential WHERE area = \'design_production_methods\';');

        $roleCredentialRepository = $this->em->getRepository(RoleCredential::class);

        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:add-credentials');

        $expectedCredential = count($this->credentialRepository->findAll()) + $amountNewCredentials;
        $expectedRoleCredential = count($roleCredentialRepository->findAll()) + $amountNewRoleCredentials;

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'license' => 'creator',
        ]);

        $actualCredential = count($this->credentialRepository->findAll());
        $actualRoleCredential = count($roleCredentialRepository->findAll());

        self::assertEquals($expectedCredential, $actualCredential);
        self::assertEquals($expectedRoleCredential, $actualRoleCredential);
    }

    public function testShouldThrowExceptionWhenLicenseIsInvalid()
    {
        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:add-credentials');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'license' => 'some_license',
        ]);

        self::assertEquals(
            'License is not valid. Expected combination of following parts, separated by \'+\': \'designer\', \'creator\', \'finder\'',
            trim($commandTester->getDisplay())
        );
        self::assertEquals(0, $commandTester->getStatusCode());
    }

    public function testShouldThrowExceptionWhenNoLicenseIsGiven()
    {
        $application = new Application();
        $application->add($this->command);

        $command = $application->find('configuratorware:add-credentials');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command' => $command->getName(),
            'license' => '',
        ]);

        self::assertEquals(
            'License is not valid. Expected combination of following parts, separated by \'+\': \'designer\', \'creator\', \'finder\'',
            trim($commandTester->getDisplay())
        );
        self::assertEquals(0, $commandTester->getStatusCode());
    }
}
