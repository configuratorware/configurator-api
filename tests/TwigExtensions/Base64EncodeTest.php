<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\TwigExtensions;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\TwigExtensions\Base64Encode;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

class Base64EncodeTest extends TestCase
{
    /**
     * @var MimeTypeGuesserInterface
     *
     * @Mock
     */
    private $mimeTypeGuesser;

    protected function setUp(): void
    {
        vfsStream::setup();
        \Phake::initAnnotations($this);
    }

    public function testShouldBase64Encode(): void
    {
        $vfsRoot = vfsStream::create(['image.jpg' => 'I am a test file']);
        $givenFilename = $vfsRoot->url() . '/image.jpg';
        \Phake::when($this->mimeTypeGuesser)->guessMimeType($givenFilename)->thenReturn('image/jpeg');

        $base64Encode = new Base64Encode($this->mimeTypeGuesser);
        $encoded = $base64Encode($givenFilename);

        self::assertEquals('data:image/jpeg;base64,SSBhbSBhIHRlc3QgZmlsZQ==', $encoded);
    }

    public function testShouldThrowMimeTypeNotGuessable(): void
    {
        $vfsRoot = vfsStream::create(['image.jpg' => 'I am a test file']);
        $givenFilename = $vfsRoot->url() . '/image.jpg';
        \Phake::when($this->mimeTypeGuesser)->guessMimeType($givenFilename)->thenReturn(null);

        $this->expectException(FileException::class);
        $this->expectExceptionMessage('Could not guess mimetype for File: vfs://root/image.jpg.');

        $base64Encode = new Base64Encode($this->mimeTypeGuesser);
        $base64Encode($givenFilename);
    }

    public function testShouldReturnAlreadyBase64Encoded(): void
    {
        $givenFilename = 'data:image/jpeg;base64,SSBhbSBhIHRlc3QgZmlsZQ==';

        $base64Encode = new Base64Encode($this->mimeTypeGuesser);
        $encoded = $base64Encode($givenFilename);

        self::assertEquals('data:image/jpeg;base64,SSBhbSBhIHRlc3QgZmlsZQ==', $encoded);
    }

    public function testShouldThrowFileNotFound(): void
    {
        $vfsRoot = vfsStream::create([]);
        $givenFilename = $vfsRoot->url() . '/image.jpg';

        $this->expectException(FileException::class);
        $this->expectExceptionMessage('File: vfs://root/image.jpg could not be found.');

        $base64Encode = new Base64Encode($this->mimeTypeGuesser);
        $base64Encode($givenFilename);
    }
}
