<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test;

use Framework\Kernel;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\KernelInterface;

trait KernelAwareTestTrait
{
    /**
     * @var KernelInterface
     */
    private $kernel;

    /**
     * @var Container
     */
    private $container;

    /**
     * @var Application
     */
    private $application;

    /**
     * @param string $entityClass
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    protected function getRepository($entityClass)
    {
        return $this->get('doctrine')->getManager()->getRepository($entityClass);
    }

    /**
     * Returns the service with $id.
     *
     * @param mixed $id
     *
     * @return object
     */
    protected function get($id)
    {
        return $this->getContainer()->get($id);
    }

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function getContainer()
    {
        if (!$this->container) {
            $this->setupKernel();
        }

        return $this->container;
    }

    /**
     * @return Application
     */
    protected function getApplication()
    {
        if (!$this->application) {
            $this->setupKernel();
        }

        return $this->application;
    }

    /**
     * Creates a Kernel.
     *
     * Available options:
     *
     *  * environment
     *  * debug
     *
     * @note You shoudl usually not need this method but rather call {@link * get()} to obtain a service and leave kernel instantiation to the trait.
     *
     * @param array $options An array of options
     *
     * @return KernelInterface A KernelInterface instance
     */
    protected function createKernel(array $options = [])
    {
        return new Kernel(
            isset($options['environment']) ? $options['environment'] : 'test',
            isset($options['debug']) ? $options['debug'] : true
        );
    }

    /**
     * Shuts the kernel down if it was used in the test.
     *
     * @after
     */
    protected function tearDownSymfonyKernel()
    {
        if (null !== $this->kernel) {
            $this->kernel->shutdown();
        }
    }

    /**
     * Called lazy, when needed.
     */
    private function setupKernel()
    {
        $this->kernel = $this->createKernel(['environment' => $this->getEnvironment()]);
        $this->kernel->boot();

        $this->container = $this->kernel->getContainer();
        $this->application = new Application($this->kernel);
    }

    protected function getEnvironment()
    {
        return 'test';
    }
}
