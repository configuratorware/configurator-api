<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test;

use redhotmagma\SymfonyTestUtils\Fixture\TestHttpResponse;
use Symfony\Component\HttpFoundation\Response;

class Fixtures extends \redhotmagma\SymfonyTestUtils\Fixture\Fixtures
{
    /**
     * @param TestHttpResponse          $expected
     * @param TestHttpResponse|Response $actual
     * @param string                    $message
     * @param string                    $accessToken
     */
    public static function assertResponseEquals(TestHttpResponse $expected, $actual, string $message = '', string $accessToken = '')
    {
        try {
            array_walk_recursive($expected->body, static function (&$item) {
                if (is_string($item)) {
                    $item = preg_replace('/date_.*/', '', $item);
                }

                return $item;
            });
            $expected->body = self::removeDates($expected->body);
            $content = json_decode($actual->getContent(), true);
            $content = self::removeDates($content);
            $actual->setContent(json_encode($content));
        } catch (\Throwable $exception) {
        }
        parent::assertResponseEquals($expected, $actual, $message);
    }

    private static function removeDates(array $content): array
    {
        array_walk_recursive($content, static function (&$item) {
            if (is_string($item)) {
                $item = preg_replace('date_.*/', '', $item);
            }
        });

        return $content;
    }
}
