<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest;

use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit\Framework\AssertionFailedError;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\PathsMock;

trait VfsTestTrait
{
    public static $doMock = false;

    /**
     * @param $path
     *
     * @return string
     */
    public static function mockedRealPath($path): string
    {
        if (self::$doMock) {
            return $path;
        }

        return realpath($path);
    }

    /**
     * @before()
     * check if annotation is set and enable mock
     */
    public function mockRealPath(): void
    {
        if ($this->mockForMethod() || $this->mockForClass()) {
            self::$doMock = true;
            PathsMock::$useVfsStorage = true;
        }
        $this->registerMock();
    }

    /**
     * @after()
     * disable the mock after the test
     */
    public function stopMocking()
    {
        self::$doMock = false;
        PathsMock::$useVfsStorage = false;
    }

    /**
     * @return bool
     */
    private function mockForMethod(): bool
    {
        $testMethod = $this->getName(false);

        try {
            $reader = new AnnotationReader();
            $ro = new \ReflectionObject($this);
            $rm = $ro->getMethod($testMethod);

            return null !== $reader->getMethodAnnotation($rm, MockPaths::class);
        } catch (\ReflectionException $e) {
            throw new AssertionFailedError(sprintf('Test precondition failed: Test method "%s" does not exist.', $testMethod));
        }
    }

    private function registerMock(): void
    {
        $self = static::class;

        if (\function_exists('Redhotmagma\ConfiguratorApiBundle\Service\Media\realpath')) {
            return;
        }
        eval(<<<EOPHP
namespace Redhotmagma\ConfiguratorApiBundle\Service\Media;

function realpath(\$path)
{
    return \\$self::mockedRealPath(\$path);
}
EOPHP
        );
    }

    /**
     * @return bool
     */
    private function mockForClass(): bool
    {
        $reader = new AnnotationReader();
        $ro = new \ReflectionObject($this);

        return null !== $reader->getClassAnnotation($ro, MockPaths::class);
    }
}
