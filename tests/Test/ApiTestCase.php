<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test;

use DAMA\DoctrineTestBundle\Doctrine\DBAL\StaticDriver;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use redhotmagma\SymfonyTestUtils\Fixture\ResponseFixtureTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApiTestCase
 * offers common functions for API testing.
 */
class ApiTestCase extends WebTestCase
{
    use ResponseFixtureTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var array
     */
    private static $token = [];

    /**
     * setup test, get entitymanager.
     */
    protected function setUp(): void
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()->get('doctrine')->getManager();
    }

    /**
     * @param array $options
     * @param array $server
     *
     * @return KernelBrowser
     */
    protected static function createClient(array $options = [], array $server = []): KernelBrowser
    {
        if (static::$booted) {
            static::ensureKernelShutdown();
        }

        return parent::createClient($options, $server);
    }

    /**
     * Create a client with a default Authorization header.
     *
     * @param string $username
     * @param string $password
     * @param string $loginUrl
     * @param bool $forceRefreshToken
     *
     * @return KernelBrowser
     */
    protected function createAuthenticatedClient(
        $username = 'admin',
        $password = 'admin',
        $loginUrl = '/api/login_check',
        $forceRefreshToken = false
    ) {
        $client = static::createClient();

        $token = $this->getToken($username, $password, $loginUrl, $forceRefreshToken);
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $token));

        return $client;
    }

    /**
     * get json data from response object and compare to expected data.
     *
     * @param Response $response
     * @param string $expected
     */
    protected function compareResponseContent($response, $expected)
    {
        $expectedJson = json_decode($expected);
        $responseJson = json_decode($response->getContent());

        self::assertEquals($expectedJson, $responseJson, 'JSON content mismatch');
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception
     */
    protected function resetAutoIncrementAllTables(): void
    {
        StaticDriver::rollBack();

        $tables = $this->em->getConnection()->getSchemaManager()->listTables();

        /** @var Connection $connection */
        $connection = $this->em->getConnection();

        foreach ($tables as $table) {
            $tableName = $table->getName();

            if (in_array($tableName, ['migration_versions'])) {
                continue;
            }

            $fetchResult = $connection->fetchAssociative("SELECT MAX(`id`) as max_id FROM `$tableName`");
            $autoIncrementer = $fetchResult['max_id'] + 1;
            $this->executeSql("ALTER TABLE `$tableName` AUTO_INCREMENT = $autoIncrementer");
        }

        StaticDriver::beginTransaction();
    }

    /**
     * @param string $sql
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function executeSql(string $sql): void
    {
        /** @var Connection $connection */
        $connection = $this->em->getConnection();
        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=0;' . $sql);
        $connection->executeStatement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * @param string $username
     * @param string $password
     * @param string $loginUrl
     * @param bool $forceRefreshToken
     *
     * @return mixed
     */
    private function getToken(string $username = 'admin', string $password = 'admin', string $loginUrl = '/api/login_check', bool $forceRefreshToken = false)
    {
        if (!isset(self::$token[$username]) || $forceRefreshToken) {
            $client = static::createClient();
            $client->insulate();

            $client->request(
                'POST',
                $loginUrl,
                [],
                [],
                ['CONTENT_TYPE' => 'application/json'],
                json_encode(['username' => $username, 'password' => $password], JSON_THROW_ON_ERROR)
            );

            $response = $client->getResponse();

            $data = json_decode($response->getContent(), true);
            self::$token[$username] = $data['token'] ?? null;
        }

        return self::$token[$username];
    }
}
