<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test;

trait EntityTestTrait
{
    /**
     * @param $object
     * @param $value
     *
     * @throws \ReflectionException
     */
    private function setPrivateId($object, $value): void
    {
        $reflect = new \ReflectionClass($object);
        $prop = $reflect->getProperty('id');
        $prop->setAccessible(true);
        $prop->setValue($object, $value);
        $prop->setAccessible(false);
    }
}
