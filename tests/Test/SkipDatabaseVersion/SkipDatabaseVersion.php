<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion;

/**
 * @Annotation
 */
final class SkipDatabaseVersion
{
    /**
     * @var string
     */
    private $version;

    public function __construct(array $parameters)
    {
        $this->version = $parameters['version'] ?? $parameters['value'];
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }
}
