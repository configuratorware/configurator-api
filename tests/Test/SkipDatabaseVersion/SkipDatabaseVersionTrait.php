<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion;

use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit\Framework\AssertionFailedError;

trait SkipDatabaseVersionTrait
{
    /**
     * @before()
     */
    public function skip(): void
    {
        $annotation = $this->readAnnotation();

        if (null === $annotation) {
            return;
        }

        $version = $annotation->getVersion();
        if (isset($_SERVER['DB_SERVER_VERSION']) && $_SERVER['DB_SERVER_VERSION'] === $version) {
            $this->markTestSkipped("Minor Database issue with $version");
        }
    }

    private function readAnnotation(): ?SkipDatabaseVersion
    {
        $testMethod = $this->getName(false);

        try {
            $reader = new AnnotationReader();
            $ro = new \ReflectionObject($this);
            $rm = $ro->getMethod($testMethod);

            return $reader->getMethodAnnotation($rm, SkipDatabaseVersion::class);
        } catch (\ReflectionException $e) {
            throw new AssertionFailedError(sprintf('Test precondition failed: Test method "%s" does not exist.', $testMethod));
        }
    }
}
