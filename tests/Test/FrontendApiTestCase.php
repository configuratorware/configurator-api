<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Test;

/**
 * Class FrontendApiTestCase
 * offers common functions for API testing.
 */
class FrontendApiTestCase extends ApiTestCase
{
    /**
     * Create a client with a default Authorization header.
     *
     * @param string $username
     * @param string $password
     * @param string $loginUrl
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthenticatedClient(
        $username = 'admin',
        $password = 'admin',
        $loginUrl = '/frontendapi/login_check'
    ) {
        $client = parent::createAuthenticatedClient($username, $password, $loginUrl);

        return $client;
    }
}
