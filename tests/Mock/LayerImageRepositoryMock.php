<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Mock;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\LayerImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\LayerImage;

class LayerImageRepositoryMock implements LayerImageRepositoryInterface
{
    private const VIEWS = ['mock_view_1', 'mock_view_2'];

    /**
     * @{@inheritDoc}
     */
    public function findLayerImages(Item $item): array
    {
        $itemIdentifier = $item->getIdentifier();
        $layerImages = [];

        foreach (self::VIEWS as $view) {
            /** @var ItemOptionclassification $itemOptionClassification */
            foreach ($item->getItemOptionclassification() as $itemOptionClassification) {
                $componentIdentifier = $itemOptionClassification->getOptionclassification()->getIdentifier();
                /** @var ItemOptionclassificationOption $itemOption */
                foreach ($itemOptionClassification->getItemOptionclassificationOption() as $itemOption) {
                    $optionIdentifier = $itemOption->getOption()->getIdentifier();
                    $url = '/' . $itemIdentifier . '/' . $view . '/' . $componentIdentifier . '/' . $optionIdentifier . '.jpg';

                    $layerImages[] = LayerImage::from($url, $componentIdentifier, $optionIdentifier, $view, '/app' . $url, $optionIdentifier . '.jpg', $itemOptionClassification->getSequenceNumber(), '0');
                }
            }
        }

        $layerImages[] = LayerImage::from('some/possible/naming/issue', 'comp', 'opt', 'view', '/app/some/possible/naming/issue', 'issue.jpg', null, '99');

        return $layerImages;
    }

    public function findLayerImagesForComponent(Item $item, Optionclassification $component): array
    {
        return array_filter($this->findLayerImages($item), static function (LayerImage $layerImage) use ($component) {
            return $layerImage->getComponentIdentifier() === $component->getIdentifier();
        });
    }

    /**
     * @{@inheritDoc}
     */
    public function findLayerImagesForView(Item $item, CreatorView $creatorView): array
    {
        $itemIdentifier = $item->getIdentifier();
        $layerImages = [];

        foreach (self::VIEWS as $view) {
            if ($view === $creatorView->getIdentifier()) {
                /** @var ItemOptionclassification $itemOptionClassification */
                foreach ($item->getItemOptionclassification() as $itemOptionClassification) {
                    $componentIdentifier = $itemOptionClassification->getOptionclassification()->getIdentifier();
                    /** @var ItemOptionclassificationOption $itemOption */
                    foreach ($itemOptionClassification->getItemOptionclassificationOption() as $itemOption) {
                        $optionIdentifier = $itemOption->getOption()->getIdentifier();
                        $url = '/' . $itemIdentifier . '/' . $view . '/' . $componentIdentifier . '/' . $optionIdentifier . '.jpg';

                        $layerImages[] = LayerImage::from($url, $componentIdentifier, $optionIdentifier, $view, '/app' . $url, $optionIdentifier . '.jpg', $itemOptionClassification->getSequenceNumber(), $creatorView->getSequenceNumber());
                    }
                }
            }
        }

        $layerImages[] = LayerImage::from('some/possible/naming/issue', 'comp', 'opt', 'view', '/app/some/possible/naming/issue', 'issue.jpg', null, '99');

        return $layerImages;
    }

    public function findLayerImage(Item $item, Optionclassification $component, Option $option, CreatorView $creatorView): LayerImage
    {
        $itemIdentifier = $item->getIdentifier();
        $componentIdentifier = $component->getIdentifier();
        $optionIdentifier = $option->getIdentifier();
        $view = $creatorView->getIdentifier();
        $url = '/' . $itemIdentifier . '/' . $view . '/' . $componentIdentifier . '/' . $optionIdentifier . 'jpg';

        return LayerImage::from($url, $componentIdentifier, $optionIdentifier, $view, '/app' . $url, $optionIdentifier . 'jpg', $component->getSequencenumber(), $creatorView->getSequenceNumber());
    }

    public function saveLayerImage(LayerImage $layerImage, Item $item): void
    {
        // dummy method
    }

    public function deleteLayerImage(LayerImage $layerImage, Item $item): void
    {
        // dummy method
    }

    public function createStores(CreatorView $view, Item $item): string
    {
        return $view->getItemIdentifier() . DIRECTORY_SEPARATOR . $view->getSequenceNumber() . '_' . $view->getIdentifier();
    }
}
