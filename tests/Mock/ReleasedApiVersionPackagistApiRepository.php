<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Mock;

use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\ReleasedApiVersion;
use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\ReleasedApiVersionRepositoryInterface;

class ReleasedApiVersionPackagistApiRepository implements ReleasedApiVersionRepositoryInterface
{
    private $tags = ['dev-master', '1.0.3', '1.9.0', '1.15.9', '1.15.12'];

    public function __construct(array $tags = null)
    {
        if (null !== $tags) {
            $this->tags = $tags;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function findVersions(): array
    {
        $versions = [];

        foreach ($this->tags as $tag) {
            $versions[] = ReleasedApiVersion::fromState($tag);
        }

        return $versions;
    }
}
