<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Mock;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;

class CreatorViewThumbnailRepositoryMock implements CreatorViewThumbnailRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function findThumbnails(Item $item): array
    {
        return [
            CreatorViewThumbnail::from('/view_thumb_base_path/mock_view_1.png', 'mock_view_1', $item->getIdentifier(), '/app/view_thumb_base_path/mock_view_1.png', 'mock_view_1.png'),
            CreatorViewThumbnail::from('/view_thumb_base_path/mock_view_2.jpg', 'mock_view_2', $item->getIdentifier(), '/app/view_thumb_base_path/mock_view_2.png', 'mock_view_2.png'),
        ];
    }

    public function saveThumbnail(CreatorViewThumbnail $creatorViewThumbnail, Item $item, CreatorView $creatorView): void
    {
        // dummy method
    }

    public function deleteThumbnail(CreatorViewThumbnail $creatorViewThumbnail): void
    {
        // dummy method
    }
}
