<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Mock;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ItemDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ItemThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemThumbnail;

class ItemImageRepositoryMock implements ItemDetailImageRepositoryInterface, ItemThumbnailRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function findDetailImages(Item $item): array
    {
        $images = [];
        $images[] = ItemDetailImage::from('/images/item/image/' . $item->getIdentifier() . '.png', $item->getIdentifier());

        foreach ($item->getItem() as $child) {
            $images[] = ItemDetailImage::from('/images/item/image/' . $child->getIdentifier() . '.png', $child->getIdentifier());
        }

        return $images;
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnails(Item $item): array
    {
        $images = [];
        $images[] = ItemThumbnail::from('/images/item/thumb/' . $item->getIdentifier() . '.png', $item->getIdentifier());

        foreach ($item->getItem() as $child) {
            $images[] = ItemThumbnail::from('/images/item/thumb/' . $child->getIdentifier() . '.png', $child->getIdentifier());
        }

        return $images;
    }
}
