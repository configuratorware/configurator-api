<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Mock;

use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ComponentThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\ComponentThumbnail;

class ComponentImageRepositoryMock implements ComponentThumbnailRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function findThumbnails(array $components): array
    {
        $thumbnails = [];
        foreach ($components as $component) {
            $path = '/components/' . $component->getIdentifier() . '.png';

            $thumbnails[] = ComponentThumbnail::from($path, $component->getIdentifier(), 'app' . $path, $component->getIdentifier() . '.png');
        }

        return $thumbnails;
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnail(Optionclassification $component): ComponentThumbnail
    {
        $path = '/components/' . $component->getIdentifier() . '.png';

        return ComponentThumbnail::from($path, $component->getIdentifier(), 'app' . $path, $component->getIdentifier() . '.png');
    }

    /**
     * {@inheritDoc}
     */
    public function saveThumbnail(ComponentThumbnail $thumbnail, Optionclassification $component): void
    {
        // dummy method
    }

    /**
     * {@inheritDoc}
     */
    public function deleteThumbnail(ComponentThumbnail $thumbnail): void
    {
        // dummy method
    }
}
