<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Mock;

use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ClientResourcePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ComponentImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignAreaPrintfilePathInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignViewImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\FontPathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ItemImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LayerImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LicensePathInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\OptionImagePathsInterface;

class PathsMock implements ClientResourcePathsInterface, ComponentImagePathsInterface, DesignAreaPrintfilePathInterface, FontPathsInterface, DesignViewImagePathsInterface, ItemImagePathsInterface, LayerImagePathsInterface, LicensePathInterface, OptionImagePathsInterface
{
    public static $useVfsStorage = false;

    public const MOCKED_LICENSE_PATH = 'vfs://root/license/license.txt';
    public const MOCKED_BASE_PATH = 'vfs://root';

    /**
     * @var string
     */
    private $fontPath;

    /**
     * @var string
     */
    private $componentThumbnailPath;

    /**
     * @var string
     */
    private $designAreaPrintfilePath;

    /**
     * @var string
     */
    private $designViewImagePath;

    /**
     * @var string
     */
    private $designViewThumbnailPath;

    /**
     * @var string
     */
    private $itemDetailImagePath;

    /**
     * @var string
     */
    private $itemThumbnailPath;

    /**
     * @var string
     */
    private $licenseFilePath;

    /**
     * @var string
     */
    private $layerImagePath;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var string
     */
    private $optionImagePath;

    /**
     * @param string $componentThumbnailPath
     * @param string $designAreaPrintfilePath
     * @param string $designViewImagePath
     * @param string $designViewThumbnailPath
     * @param string $fontPath
     * @param string $itemDetailImagePath
     * @param string $itemThumbnailPath
     * @param string $layerImagePath
     * @param string $licenseFilePath
     * @param string $mediaBasePath
     * @param string $optionImagePath
     */
    public function __construct(string $componentThumbnailPath, string $designAreaPrintfilePath, string $designViewImagePath, string $designViewThumbnailPath, string $fontPath, string $itemDetailImagePath, string $itemThumbnailPath, string $layerImagePath, string $licenseFilePath, string $mediaBasePath, string $optionImagePath)
    {
        $this->componentThumbnailPath = trim($componentThumbnailPath, '/');
        $this->designAreaPrintfilePath = $designAreaPrintfilePath;
        $this->designViewImagePath = trim($designViewImagePath, '/');
        $this->designViewThumbnailPath = trim($designViewThumbnailPath, '/');
        $this->fontPath = trim($fontPath, '/');
        $this->itemDetailImagePath = trim($itemDetailImagePath, '/');
        $this->itemThumbnailPath = trim($itemThumbnailPath, '/');
        $this->licenseFilePath = $licenseFilePath;
        $this->layerImagePath = trim($layerImagePath, '/');
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
        $this->optionImagePath = trim($optionImagePath, '/');
    }

    public function getClientFontPath(string $clientIdentifier): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->getClientFontPathRelative($clientIdentifier);
    }

    public function getClientFontPathRelative(string $clientIdentifier): string
    {
        return 'client/' . $clientIdentifier . '/font';
    }

    public function getClientLogoPath(string $clientIdentifier): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->getClientLogoPathRelative($clientIdentifier);
    }

    public function getClientLogoPathRelative(string $clientIdentifier): string
    {
        return 'client/' . $clientIdentifier . '/logo';
    }

    public function getComponentThumbnailPath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->componentThumbnailPath;
    }

    public function getComponentThumbnailPathRelative(): string
    {
        return $this->componentThumbnailPath;
    }

    public function getDesignAreaPrintfilePath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->designAreaPrintfilePath;
    }

    public function getDesignViewImagePath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->designViewImagePath;
    }

    public function getDesignViewImagePathRelative(): string
    {
        return $this->designViewImagePath;
    }

    public function getDesignViewThumbnailPath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->designViewThumbnailPath;
    }

    public function getDesignViewThumbnailPathRelative(): string
    {
        return $this->designViewThumbnailPath;
    }

    public function getFontPath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->fontPath;
    }

    public function getFontPathRelative(): string
    {
        return $this->fontPath;
    }

    public function getItemDetailImagePath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->itemDetailImagePath;
    }

    public function getItemDetailImagePathRelative(): string
    {
        return $this->itemDetailImagePath;
    }

    public function getItemThumbnailPath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->itemThumbnailPath;
    }

    public function getItemThumbnailPathRelative(): string
    {
        return $this->itemThumbnailPath;
    }

    public function getLayerImagePath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->layerImagePath;
    }

    public function getLayerImagePathRelative(): string
    {
        return $this->layerImagePath;
    }

    public function getLicenseFilePath(): string
    {
        if (self::$useVfsStorage) {
            return self::MOCKED_LICENSE_PATH;
        }

        return $this->licenseFilePath;
    }

    public function getOptionThumbnailPath(): string
    {
        return $this->getMediaBasePath() . DIRECTORY_SEPARATOR . $this->optionImagePath;
    }

    public function getOptionThumbnailPathRelative(): string
    {
        return $this->optionImagePath;
    }

    private function getMediaBasePath(): string
    {
        if (self::$useVfsStorage) {
            return self::MOCKED_BASE_PATH;
        }

        return $this->mediaBasePath;
    }
}
