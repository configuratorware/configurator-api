<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Mock;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionThumbnail;

/**
 * @internal
 */
final class OptionImageRepositoryMock implements OptionDetailImageRepositoryInterface, OptionThumbnailRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function findDetailImage(Option $option): OptionDetailImage
    {
        return $this->findDetailImageByIdentifier($option->getIdentifier());
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImageByIdentifier(string $optionIdentifier): OptionDetailImage
    {
        $filename = $optionIdentifier . '_fullsize.jpg';
        $path = '/option/' . $filename;

        return OptionDetailImage::from($path, $optionIdentifier, '/app' . $path, $filename);
    }

    public function saveDetailImage(OptionDetailImage $detailImage, Option $option): void
    {
        // dummy method
    }

    public function deleteDetailImage(OptionDetailImage $detailImage): void
    {
        // dummy method
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImages(array $options): array
    {
        $detailImages = [];
        foreach ($options as $option) {
            $filename = $option->getIdentifier() . '_fullsize.jpg';
            $path = '/option/' . $filename;

            $detailImages[] = OptionDetailImage::from($path, $option->getIdentifier(), '/app' . $path, $filename);
        }

        return $detailImages;
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImagesByIdentifiers(array $optionIdentifiers): array
    {
        $detailImages = [];
        foreach ($optionIdentifiers as $optionIdentifier) {
            $filename = $optionIdentifier . '_fullsize.jpg';
            $path = '/option/' . $filename;

            $detailImages[] = OptionDetailImage::from($path, $optionIdentifier, '/app' . $path, $filename);
        }

        return $detailImages;
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnail(Option $option): OptionThumbnail
    {
        return $this->findThumbnailByIdentifier($option->getIdentifier());
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnailByIdentifier(string $optionIdentifier): OptionThumbnail
    {
        $filename = $optionIdentifier . '.jpg';
        $path = '/option/' . $filename;

        return OptionThumbnail::from($path, $optionIdentifier, '/app' . $path, $filename);
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnails(array $options): array
    {
        $thumbnails = [];
        foreach ($options as $option) {
            $filename = $option->getIdentifier() . '.jpg';
            $path = '/option/' . $filename;

            $thumbnails[] = OptionThumbnail::from($path, $option->getIdentifier(), '/app' . $path, $filename);
        }

        return $thumbnails;
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnailsByIdentifiers(array $optionIdentifiers): array
    {
        $thumbnails = [];
        foreach ($optionIdentifiers as $optionIdentifier) {
            $filename = $optionIdentifier . '.jpg';
            $path = '/option/' . $filename;

            $thumbnails[] = OptionThumbnail::from($path, $optionIdentifier, '/app' . $path, $filename);
        }

        return $thumbnails;
    }

    public function saveThumbnail(OptionThumbnail $optionThumbnail, Option $option): void
    {
        // dummy method
    }

    public function deleteThumbnail(OptionThumbnail $optionThumbnail): void
    {
        // dummy method
    }
}
