<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Vector;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\PdfConversionFailed;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;
use Redhotmagma\ConfiguratorApiBundle\Vector\Html;
use Redhotmagma\ConfiguratorApiBundle\Vector\WeasyprintHtmlPdfConverter;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class WeasyprintHtmlPdfConverterTest extends TestCase
{
    /**
     * @var Filesystem
     *
     * @Mock
     */
    private $fs;

    /**
     * @var WeasyprintHtmlPdfConverter
     */
    private $pdfConverter;

    /**
     * @var ProcessFactory
     *
     * @Mock
     */
    private $processFactory;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->pdfConverter = new WeasyprintHtmlPdfConverter($this->processFactory);
    }

    public function testShouldConvertHtmlToPdf(): void
    {
        $html = Html::withContent('<html></html>');

        $process = $this->mockProcess(true);

        $pdf = $this->pdfConverter->htmlToPdf($html);

        \Phake::verify($process)->run;
        self::assertStringContainsString('/tmp/conf_doc_pdf', $pdf->getPath());
    }

    public function testCannotConvertHtmlToPdf(): void
    {
        $html = Html::withContent('<html></html>');

        $this->mockProcess(false);

        $this->expectException(PdfConversionFailed::class);

        $this->pdfConverter->htmlToPdf($html);
    }

    private function mockProcess(bool $isSuccessful): Process
    {
        $process = \Phake::mock(Process::class);
        \Phake::when($process)->isSuccessful()->thenReturn($isSuccessful);
        \Phake::when($this->processFactory)->create->thenReturn($process);

        return $process;
    }
}
