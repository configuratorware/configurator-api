<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Vector;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Vector\InkscapeConverter;
use Redhotmagma\ConfiguratorApiBundle\Vector\InkscapeFactory;
use Redhotmagma\ConfiguratorApiBundle\Vector\LegacyInkscapeConverter;

class InkscapeFactoryTest extends TestCase
{
    public function testShouldProvideNewInkscapeConverter(): void
    {
        if (!InkscapeFactory::isNewInkscapeVersion()) {
            $this->markTestSkipped('test can only run for new inkscape version');
        }
        $this->assertInstanceOf(InkscapeConverter::class, InkscapeFactory::create());
    }

    public function testShouldProvideLegacyInkscapeConverter(): void
    {
        if (InkscapeFactory::isNewInkscapeVersion()) {
            $this->markTestSkipped('test can only run for old inkscape version');
        }
        $this->assertInstanceOf(LegacyInkscapeConverter::class, InkscapeFactory::create());
    }
}
