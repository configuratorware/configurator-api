<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Vector;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Vector\InkscapeConverter;
use Redhotmagma\ConfiguratorApiBundle\Vector\InkscapeFactory;
use Redhotmagma\ConfiguratorApiBundle\Vector\LegacyInkscapeConverter;
use Redhotmagma\ConfiguratorApiBundle\Vector\Svg;
use Redhotmagma\ConfiguratorApiBundle\Vector\Vector;
use Symfony\Component\Filesystem\Filesystem;

class InkscapeVectorConverterTest extends TestCase
{
    private const TEST_FILE_DIR = __DIR__ . '/_data';

    /**
     * @var LegacyInkscapeConverter
     */
    private $inkscapeVectorConversion;

    protected function setUp(): void
    {
        if (InkscapeFactory::isNewInkscapeVersion()) {
            $this->inkscapeVectorConversion = new InkscapeConverter(new Filesystem());
        } else {
            $this->inkscapeVectorConversion = new LegacyInkscapeConverter(new Filesystem());
        }
    }

    /**
     * @param string $testFilePath
     * @dataProvider providerConvertVectorToSvg
     */
    public function testShouldConvertVectorToSvg(string $testFilePath): void
    {
        $vector = Vector::withPath($testFilePath);
        $svg = $this->inkscapeVectorConversion->vectorToSvg($vector);

        self::assertNotEmpty($svg->getContent());
        self::assertNotEquals(false, simplexml_load_string($svg->getContent()), 'SVG file content is an invalid XML');
    }

    public function testCouldNotConvertVectorToSvg(): void
    {
        $this->expectException(FileException::class);

        $vector = Vector::withPath(self::TEST_FILE_DIR . '/no_such_file');
        $this->inkscapeVectorConversion->vectorToSvg($vector);
    }

    public function providerConvertVectorToSvg(): array
    {
        return [
            [self::TEST_FILE_DIR . '/ai_mehrfarbig_transparenz_klein.ai'],
            [self::TEST_FILE_DIR . '/eps_mehrfarbig_transparenz_klein.eps'],
            [self::TEST_FILE_DIR . '/pdf_mehrfarbig_transparenz_klein.pdf'],
            [self::TEST_FILE_DIR . '/svg_mehrfarbig_transparenz_klein.svg'],
        ];
    }

    public function testShouldConvertVectorToPng(): void
    {
        $vector = Vector::withPath(self::TEST_FILE_DIR . '/eps_mehrfarbig_transparenz_klein.eps');

        $png = $this->inkscapeVectorConversion->vectorToPng($vector);

        self::assertSame('png', pathinfo($png->getPath(), PATHINFO_EXTENSION));
    }

    public function testShouldConvertSvgToPng(): void
    {
        $svg = Svg::withContent(file_get_contents(self::TEST_FILE_DIR . '/svg_mehrfarbig_transparenz_klein.svg'));

        $png = $this->inkscapeVectorConversion->svgToPng($svg);

        self::assertSame('png', pathinfo($png->getPath(), PATHINFO_EXTENSION));
    }

    public function testShouldConvertSvgToPdf(): void
    {
        $svg = Svg::withContent(file_get_contents(self::TEST_FILE_DIR . '/svg_mehrfarbig_transparenz_klein.svg'));

        $pdf = $this->inkscapeVectorConversion->svgToPdf($svg);

        self::assertSame('pdf', pathinfo($pdf->getPath(), PATHINFO_EXTENSION));
    }
}
