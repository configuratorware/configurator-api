<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Vector;

use Dompdf\Dompdf;
use Dompdf\Options;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\PdfConversionFailed;
use Redhotmagma\ConfiguratorApiBundle\Factory\DompdfFactory;
use Redhotmagma\ConfiguratorApiBundle\Vector\DompdfHtmlPdfConverter;
use Redhotmagma\ConfiguratorApiBundle\Vector\Html;
use Symfony\Component\Filesystem\Filesystem;

class DompdfHtmlPdfConverterTest extends TestCase
{
    /**
     * @var DompdfFactory
     *
     * @Mock
     */
    private $dompdfFactory;

    /**
     * @var Filesystem
     *
     * @Mock
     */
    private $fs;

    /**
     * @var DompdfHtmlPdfConverter
     */
    private $pdfConverter;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->pdfConverter = new DompdfHtmlPdfConverter($this->dompdfFactory, $this->fs);
    }

    public function testShouldConvertHtmlToPdf(): void
    {
        $htmlContent = '<html>this is a test</html>';
        $dompdf = $this->mockDompdf('I am a pdf');

        $pdf = $this->pdfConverter->htmlToPdf(Html::withContent($htmlContent));

        \Phake::verify($dompdf)->loadHtml($htmlContent);
        \Phake::verify($dompdf)->render();
        \Phake::verify($this->fs)->dumpFile(\Phake::capture($path), \Phake::equalTo('I am a pdf'));
        self::assertStringContainsString('/tmp/conf_doc_pdf', $path);
        self::assertStringContainsString('/tmp/conf_doc_pdf', $pdf->getPath());
    }

    public function testCannotConvertHtmlToPdf(): void
    {
        $html = Html::withContent('<html></html>');

        $this->mockDompdf(null);

        $this->expectException(PdfConversionFailed::class);

        $this->pdfConverter->htmlToPdf($html);
    }

    /**
     * @return Dompdf
     */
    private function mockDompdf(?string $output): Dompdf
    {
        $dompdf = \Phake::mock(Dompdf::class);
        \Phake::when($this->dompdfFactory)->create()->thenReturn($dompdf);
        \Phake::when($dompdf)->output()->thenReturn($output);
        \Phake::when($dompdf)->getOptions()->thenReturn(new Options());

        return $dompdf;
    }
}
