<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class AdminModeTest extends ApiTestCase
{
    /**
     * @var ConfigurationRepository
     */
    protected $repository;

    protected $connection;

    protected function setUp(): void
    {
        parent::setUp();

        $this->connection = $this->em->getConnection();

        $this->repository = $this->em->getRepository(Configuration::class);

        $_SERVER['HTTP_HOST'] = 'testshost.tld';
    }

    protected function tearDown(): void
    {
        $this->connection->close();
    }

    public function testGetAdminModeHash()
    {
        $client = $this->createAuthenticatedClient();
        $client->insulate();

        $client->request('GET', '/api/adminmode/gethash');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('ThisTokenIsNotSecretChangeIt', $result->hash, 'hash not found');
    }

    public function testLoadNonConfigurableItemByIdentifierInAdminMode()
    {
        // set sheep as not configurable
        $this->executeSql('UPDATE item SET configurable = 0, item_status_id = (SELECT id FROM item_status WHERE identifier = "not_available") WHERE identifier ="sheep";');

        $client = static::createClient();
        $client->insulate();

        $client->request('GET', '/frontendapi/configuration/loadbyitemidentifier/sheep?_admin_mode=ThisTokenIsNotSecretChangeIt');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('sheep', $result->item->identifier, 'JSON content mismatch');
        self::assertEquals(true, $result->item->configurationModes[0]->itemStatus->item_available, 'JSON content mismatch');

        // set sheep to configurable again
        $this->executeSql('UPDATE item SET configurable = 1, item_status_id = (SELECT id from item_status where identifier = "available") WHERE identifier ="sheep";');
    }

    public function testLoadItemWithoutDefaultConfigurationByIdentifierInAdminMode()
    {
        // set sheep default configuration as deleted
        $this->executeSql('UPDATE configuration SET date_deleted = NOW() WHERE name ="defaultoptions_sheep";');

        $client = static::createClient();
        $client->insulate();

        $client->request('GET', '/frontendapi/configuration/loadbyitemidentifier/sheep?_admin_mode=ThisTokenIsNotSecretChangeIt');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('sheep', $result->item->identifier, 'JSON content mismatch');

        // set sheep to configurable again
        $this->executeSql('UPDATE configuration SET date_deleted = NOW() WHERE item_id = 1 AND configurationtype_id = 2;');
        $this->executeSql('UPDATE configuration SET date_deleted = "0001-01-01 00:00:00" WHERE name ="defaultoptions_sheep";');
    }
}
