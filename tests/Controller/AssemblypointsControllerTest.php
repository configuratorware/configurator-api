<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint;
use Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class AssemblypointsControllerTest extends ApiTestCase
{
    /**
     * @var AssemblypointRepository
     */
    private $assemblypointRepository;

    protected function setUp(): void
    {
        parent::setUp();

        // create base data to be able to run the test
        $this->executeSql(file_get_contents(__DIR__ . '/_data/AssemblypointsControllerTest_saveDependencyData.sql'));

        $this->assemblypointRepository = $this->em->getRepository(Assemblypoint::class);
    }

    public function testGetbyoptionAction()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/assemblypoints/getbyoption/1');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testSavebyoptionAction()
    {
        $postdata = '{"id":1,"identifier":"coat_nature","visualizationdata":[{"assemblyPoints":[{"id":null,"identifier":"face","option_id":"1","assemblypointimageelement_id":1,"x":438,"y":226,"rotation":10}]}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/assemblypoints/savebyoption',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $entity = $this->assemblypointRepository->find(2);

        self::assertInstanceOf(Assemblypoint::class, $entity);
        self::assertEquals(10, $entity->getRotation());
    }

    public function testUpdateSavebyoptionAction()
    {
        $postdata = '{"id":1,"identifier":"coat_nature","visualizationdata":[{"assemblyPoints":[{"id":1,"identifier":"face","option_id":"1","assemblypointimageelement_id":1,"x":438,"y":226,"rotation":20}]}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/assemblypoints/savebyoption',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $entity = $this->assemblypointRepository->find(1);

        self::assertInstanceOf(Assemblypoint::class, $entity);
        self::assertEquals(20, $entity->getRotation());
    }

    public function testListoptionsAction()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/assemblypoints/listoptions');

        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testListsampleoptionsAction()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET',
            '/api/assemblypoints/listsampleoptions/?optionclassification=face&imageelement=face&limit=1&offset=0');

        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }
}
