<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\UserImage\DesignerUserImageUpload;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class UserImagesControllerTest extends ApiTestCase
{
    /**
     * Base dir to load files from.
     */
    public const MEDIA_DIR = __DIR__ . '/_data/media';

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $imageUploadTestDir;

    public function setUp(): void
    {
        parent::setUp();
        $this->fileSystem = new Filesystem();

        // create test dir for images before upload
        $this->imageUploadTestDir = __DIR__ . '/_data/ImageUpload/test';
        $this->fileSystem->mkdir($this->imageUploadTestDir, 0755);
    }

    protected function tearDown(): void
    {
        $this->removeContents(self::MEDIA_DIR . '/img/uploads/designer', self::MEDIA_DIR . '/img/uploads/designer');
    }

    public function testUpload()
    {
        $this->fileSystem->copy(
            __DIR__ . '/_data/media/duck.svg',
            __DIR__ . '/_data/media/duck_upload.svg'
        );

        $userImage = new UploadedFile(
            __DIR__ . '/_data/media/duck_upload.svg',
            'duck.svg',
            'image/svg+xml'
        );

        $client = self::createClient();

        $client->request(
            'POST',
            '/frontendapi/userimages/upload',
            [],
            ['userImage' => $userImage]
        );

        $response = $client->getResponse();
        $imageData = json_decode($response->getContent());

        $conversionErrors = $imageData->errors ?? [];
        self::assertEmpty($conversionErrors, implode(',', $conversionErrors));

        self::assertFileExists(self::MEDIA_DIR . $imageData->original, 'Original file not Uploaded');
        self::assertFileExists(self::MEDIA_DIR . $imageData->preview, 'Preview file not Uploaded');
        self::assertFileExists(self::MEDIA_DIR . $imageData->thumb, 'Thumbnail file not Uploaded');

        $this->fileSystem->remove([
            self::MEDIA_DIR . $imageData->original,
            self::MEDIA_DIR . $imageData->preview,
            self::MEDIA_DIR . $imageData->thumb,
        ]);
    }

    public function testUploadFileConversion()
    {
        $this->fileSystem->copy(
            __DIR__ . '/_data/media/white_logo_transparent_bg_CMYK.pdf',
            __DIR__ . '/_data/media/white_logo_transparent_bg_CMYK_upload.pdf'
        );

        $userImage = new UploadedFile(
            __DIR__ . '/_data/media/white_logo_transparent_bg_CMYK_upload.pdf',
            'white_logo_transparent_bg_CMYK_upload.pdf',
            'application/pdf'
        );

        $client = self::createClient();

        $client->request(
            'POST',
            '/frontendapi/userimages/upload',
            [],
            ['userImage' => $userImage]
        );

        $content = json_decode($client->getResponse()->getContent());

        $conversionErrors = $content->errors ?? [];
        self::assertEmpty($conversionErrors, implode(',', $conversionErrors));

        $previewImage = new \Imagick(self::MEDIA_DIR . $content->preview);

        self::assertTrue(
            (new \ImagickPixel('rgba(255,255,255,0)'))->isSimilar($previewImage->getImagePixelColor(0, 0), 0),
            'The image colors differ from the expected result.'
        );

        self::assertTrue(
            (new \ImagickPixel('rgba(255,255,255,1)'))->isSimilar($previewImage->getImagePixelColor(200, 200), 0),
            'The image colors differ from the expected result.'
        );

        $this->fileSystem->remove([
            self::MEDIA_DIR . $content->original,
            self::MEDIA_DIR . $content->preview,
            self::MEDIA_DIR . $content->thumb,
        ]);
    }

    /**
     * @param string $filePath
     * @param string $fileName
     * @param int $expectedWidth
     * @param int $expectedHeight
     * @param string|null $operation
     * @param string $command
     * @param string $bgColor
     *
     * @throws \ImagickException
     *
     * @dataProvider provideSamplesForDesignerEditImage
     */
    public function testDesignerEditImage(
        string $filePath,
        string $fileName,
        int $expectedWidth,
        int $expectedHeight,
        ?string $operation,
        string $command,
        string $bgColor,
        string $previewExtension
    ) {
        $client = self::createClient();

        // copy file that it can be removed
        $testFilePath = $this->imageUploadTestDir . '/' . $fileName;
        $this->fileSystem->copy($filePath, $testFilePath);
        $userImage = new UploadedFile(
            $testFilePath,
            $fileName,
            'image/png'
        );

        // Setup Json for different scenarios
        if (null !== $operation) {
            $userImageEditDataArray = [
                'operations' => [
                    [
                        'identifier' => $operation,
                        'config' => [
                            'command' => $command,
                            'backgroundColor' => $bgColor,
                        ],
                    ],
                ],
            ];
        }

        // send request with image binary and edit operations
        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => isset($userImageEditDataArray) ? json_encode($userImageEditDataArray) : '{}'],
            ['userImage' => $userImage]
        );

        $content = json_decode($client->getResponse()->getContent());

        self::assertTrue(isset($content->fileName), $fileName . ':' . var_export($content, true));

        // Save Target Paths to array for check
        $designerBaseDir = self::MEDIA_DIR . '/img/uploads/designer';
        $sources = [
            'thumbnail' => $designerBaseDir . '/thumbnail/' . $content->fileName . '.png',
            'preview' => $designerBaseDir . '/preview/' . $content->fileName . $previewExtension,
            'original' => $designerBaseDir . '/original/' . $content->fileName . '/' . $fileName,
        ];

        // Check if file exists
        foreach ($sources as $key => $source) {
            self::assertFileExists(
                $source,
                sprintf(
                    'File %s does not exist in %s. Operated with %s > %s',
                    $fileName,
                    $key,
                    $operation,
                    $command
                )
            );

            // vector images are not scaled
            if ('preview' === $key && '.svg' !== $previewExtension) {
                $imagick = new \Imagick($source);
                self::assertGreaterThanOrEqual(
                    $expectedWidth,
                    $imagick->getImageWidth(),
                    'Preview file width is too small for ' . $fileName
                );
                self::assertGreaterThanOrEqual(
                    $expectedHeight,
                    $imagick->getImageHeight(),
                    'Preview file height is too small for ' . $fileName
                );
            }
        }

        // edit previously uploaded file using filename
        $userImageEditDataArray['fileName'] = $content->fileName;
        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => json_encode($userImageEditDataArray)]
        );

        $editResponse = $client->getResponse();

        // Check if edit without sending file was possible
        self::assertSame(
            Response::HTTP_OK,
            $editResponse->getStatusCode(),
            'HTTP status code not ok for ' . $fileName
        );
    }

    public function provideSamplesForDesignerEditImage()
    {
        $providedFiles = [];

        // read files from test folder
        $finder = new Finder();
        $finder->files()
            ->in(__DIR__ . '/_data/ImageUpload')
            ->depth('== 0');

        $expectedWidth = 500;
        $expectedHeight = 200;
        $operation = 'clipping';
        $command = 'replace';
        $bgColor = '30';

        // create test data based on files
        foreach ($finder as $file) {
            $filePath = $file->getPath() . '/' . $file->getFilename();
            $fileName = pathinfo($filePath, PATHINFO_FILENAME);
            $fileNameWithExtension = pathinfo($filePath, PATHINFO_BASENAME);
            $extension = pathinfo($filePath, PATHINFO_EXTENSION);

            // different parameters for small raster images
            if ('mehrfarbig_transparenz_klein' === $fileName) {
                $expectedWidth = 124;
                $expectedHeight = 34;
            }

            // rotate jpeg with exif data
            if ('exif_redhotmagma' === $fileName) {
                $expectedWidth = 75;
                $expectedHeight = 100;
                $operation = null;
            }

            // different parameters for other color format
            if ('logo_redhotmagma' === $fileName) {
                $command = 'flood';
                $bgColor = '30';
            }

            // only when flooding a vector format, it will be converted to png
            $previewExtension = in_array($extension, DesignerUserImageUpload::VECTOR_FORMATS) && ('flood' !== $command || null === $operation) ? '.svg' : '.png';

            // expect "dirty" vector files (containing raster images) to be converted to png
            if (in_array($fileNameWithExtension, ['logo_redhotmagma.ai', 'logo_redhotmagma.pdf', '{hoodie}_default_hoodie.pdf'])) {
                $previewExtension = '.png';
            }

            // add image file and default test parameters
            $providedFiles[] = [
                $filePath,
                $file->getFilename(), // file name
                $expectedWidth,
                $expectedHeight,
                $operation,
                $command,
                $bgColor,
                $previewExtension,
            ];
        }

        return $providedFiles;
    }

    /**
     * @param string $fileName
     * @param string $mime
     * @param string $testFilePath
     * @param string $uploadPath
     *
     * @throws \ImagickException
     *
     * @dataProvider providerPreScalableImage
     */
    public function testVectorImagePreScaling(string $fileName, string $mime, string $testFilePath, string $uploadPath)
    {
        $this->fileSystem->copy($testFilePath, $uploadPath);

        /* upload new file and check prescaling */

        $userImage = new UploadedFile(
            $uploadPath,
            $fileName,
            $mime
        );

        $client = self::createClient();

        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => '{}'],
            ['userImage' => $userImage]
        );

        $response = $client->getResponse();

        self::assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getContent());

        $filePath = self::MEDIA_DIR . '/img/uploads/designer/preview/' . $content->fileName . '.png';
        self::assertFileExists($filePath);

        $imagick = new \Imagick($filePath);
        self::assertGreaterThan(250, $imagick->getImageHeight());
        self::assertGreaterThan(500, $imagick->getImageWidth());

        /* edit previously uploaded file using filename and check prescaling */

        // json parameters for editing an uploaded image
        $userImageEditDataArray = [
            'fileName' => $content->fileName,
            'operations' => [
                [
                    'identifier' => 'clipping',
                    'config' => [
                        'command' => 'flood',
                        'backgroundColor' => '#ffffff',
                    ],
                ],
            ],
        ];

        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => json_encode($userImageEditDataArray)]
        );

        $response = $client->getResponse();
        self::assertEquals(200, $response->getStatusCode());

        $imagick = new \Imagick($filePath);
        self::assertGreaterThan(250, $imagick->getImageHeight());
        self::assertGreaterThan(500, $imagick->getImageWidth());

        $this->fileSystem->remove($uploadPath);
    }

    /**
     * @param string $fileName
     * @param string $mime
     * @param string $testFilePath
     * @param string $uploadPath
     *
     * @throws \ImagickException
     *
     * @dataProvider providerNonScalableImage
     */
    public function testSkippingOfVectorImagePreScaling(
        string $fileName,
        string $mime,
        string $testFilePath,
        string $uploadPath
    ) {
        $this->fileSystem->copy($testFilePath, $uploadPath);

        /* upload new file and check prescaling */

        $userImage = new UploadedFile(
            $uploadPath,
            $fileName,
            $mime
        );

        $client = self::createClient();

        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => '{}'],
            ['userImage' => $userImage]
        );

        $response = $client->getResponse();
        self::assertEquals(200, $response->getStatusCode());

        $content = json_decode($response->getContent());

        $filePath = self::MEDIA_DIR . '/img/uploads/designer/preview/' . $content->fileName . '.png';
        self::assertFileExists($filePath);

        $imagick = new \Imagick($filePath);
        self::assertLessThan(100, $imagick->getImageHeight());
        self::assertLessThan(200, $imagick->getImageWidth());

        /* edit previously uploaded file using filename and check prescaling */

        // json parameters for editing an uploaded image
        $userImageEditDataArray = [
            'fileName' => $content->fileName,
            'operations' => [
                [
                    'identifier' => 'clipping',
                    'config' => [
                        'command' => 'flood',
                        'backgroundColor' => '#ffffff',
                    ],
                ],
            ],
        ];

        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => json_encode($userImageEditDataArray)]
        );

        $response = $client->getResponse();
        self::assertEquals(200, $response->getStatusCode());

        $imagick = new \Imagick($filePath);
        self::assertLessThan(100, $imagick->getImageHeight());
        self::assertLessThan(200, $imagick->getImageWidth());

        $this->fileSystem->remove($uploadPath);
    }

    /**
     * @return array
     */
    public function providerPreScalableImage()
    {
        return array_map(
            function ($file) {
                return [
                    $file[0],
                    $file[1],
                    self::MEDIA_DIR . '/image_scaling/prescalable/' . $file[0],
                    self::MEDIA_DIR . '/image_scaling/prescalable/upload_scalable_' . $file[0],
                ];
            },
            [
                ['logo_redhotmagma.ai', 'application/pdf'],
                ['logo_redhotmagma.pdf', 'iapplication/pdf'],
                ['{hoodie}_default_hoodie.pdf', 'application/pdf'],
            ]
        );
    }

    /**
     * @return array
     */
    public function providerNonScalableImage()
    {
        return array_map(
            function ($file) {
                return [
                    $file[0],
                    $file[1],
                    self::MEDIA_DIR . '/image_scaling/non_scalable/' . $file[0],
                    self::MEDIA_DIR . '/image_scaling/non_scalable/upload_nonscalable_' . $file[0],
                ];
            },
            [
                ['mehrfarbig_transparenz_klein.gif', 'image/gif'],
                ['mehrfarbig_transparenz_klein.jpg', 'image/jpeg'],
                ['mehrfarbig_transparenz_klein.png', 'image/png'],
            ]
        );
    }

    private function removeContents($directory, $exclude)
    {
        foreach (glob("{$directory}/*") as $file) {
            try {
                if (is_dir($file)) {
                    $this->removeContents($file, $exclude);
                } else {
                    $this->fileSystem->remove($file);
                }
            } catch (\Exception $e) {
                sprintf('Exception: %s', $e->getMessage());
            }
        }
        if ($directory !== $exclude) {
            rmdir($directory);
        }
    }

    /**
     * Test for message when a vector file upload contains embedded image or text.
     *
     * @param string $fileName
     * @param string $mime
     *
     * @dataProvider providerVectorPathWithEmbeddedObjects
     */
    public function testNotificationMessageOnVectorGraphicsContainingEmbeddedObject(string $fileName, string $mime)
    {
        $filePath = self::MEDIA_DIR . '/vector/embedded_object_in_vector_file/' . $fileName;
        $tempFile = $this->fileSystem->tempnam(sys_get_temp_dir(), 'temp_vector_file');
        copy($filePath, $tempFile);
        $userImage = new UploadedFile($tempFile, $fileName, $mime);

        $client = self::createClient();
        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => '{}'],
            ['userImage' => $userImage]
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());

        $responseContent = json_decode($response->getContent());
        self::assertSame('vector_not_paths_only', $responseContent->message);
    }

    /**
     * Test for NULL message when a vector file upload doesn't contain embedded image or text.
     *
     * @param string $fileName
     * @param string $mime
     *
     * @dataProvider providerVectorPathWithoutEmbeddedObjects
     */
    public function testNotificationMessageOnVectorGraphicsNotContainingEmbeddedObject(string $fileName, string $mime)
    {
        $filePath = self::MEDIA_DIR . '/vector/path_only/' . $fileName;
        $tempFile = $this->fileSystem->tempnam(sys_get_temp_dir(), 'temp_vector_file');
        copy($filePath, $tempFile);
        $userImage = new UploadedFile($tempFile, $fileName, $mime);

        $client = self::createClient();
        $client->request(
            'POST',
            '/frontendapi/userimages/designer/edit',
            ['userImageEditData' => '{}'],
            ['userImage' => $userImage]
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());

        $responseContent = json_decode($response->getContent());
        self::assertNull($responseContent->message);
    }

    /**
     * Files containing embedded image or text.
     *
     * @return array
     */
    public function providerVectorPathWithEmbeddedObjects(): array
    {
        return [
            ['Arial_mit_Umlaute_AI.ai', 'application/pdf'],
            ['Arial_mit_Umlaute_EPS.eps', 'image/x-eps'],
            ['Arial_mit_Umlaute_PDF.pdf', 'application/pdf'],
            ['OpenSans_mit_Umlaute_EPS.eps', 'image/x-eps'],
            ['OpenSans_mit_Umlaute_PDF.ai', 'application/pdf'],
            ['OpenSans_mit_Umlaute_PDF.pdf', 'application/pdf'],
            ['MusterAI.ai', 'application/pdf'],
            ['MusterEPS.eps', 'image/x-eps'],
            ['MusterPDF.pdf', 'application/pdf'],
        ];
    }

    /**
     * Files not containing embedded objects.
     *
     * @return array
     */
    public function providerVectorPathWithoutEmbeddedObjects(): array
    {
        return [
            ['Pfad_Arial_mit_Umlaute_AI.ai', 'application/pdf'],
            ['Pfad_Arial_mit_Umlaute_EPS.eps', 'image/x-eps'],
            ['Pfad_Arial_mit_Umlaute_PDF.pdf', 'application/pdf'],
            ['Pfad_OpenSans_mit_Umlaute_AI.ai', 'application/pdf'],
            ['Pfad_OpenSans_mit_Umlaute_EPS.eps', 'image/x-eps'],
            ['Pfad_OpenSans_mit_Umlaute_PDF.pdf', 'application/pdf'],
        ];
    }
}
