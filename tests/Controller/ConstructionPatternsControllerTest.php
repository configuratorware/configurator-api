<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ConstructionPatternsControllerTest extends ApiTestCase
{
    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/constructionpatterns/1');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @BackupDatabase
     */
    public function testSave()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/constructionpatterns', [], [], ['CONTENT_TYPE' => 'application/json'],
            file_get_contents(__DIR__ . '/_fixtures/ConstructionPatternsControllerTest/testSave.given.json'));
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }
}
