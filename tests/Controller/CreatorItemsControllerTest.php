<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CreatorItemsControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemGroup/setUp.sql'));
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/creatoritems?query=item_1');
        $response = $client->getResponse();
        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }
}
