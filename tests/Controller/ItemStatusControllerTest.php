<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ItemStatusControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/itemstatus');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response,
            '{"metadata":{"totalcount":"2"},"data":[{"id":1,"identifier":"not_available","item_available":false},{"id":2,"identifier":"available","item_available":true}]}'
        );
    }
}
