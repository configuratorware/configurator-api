<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ExportControllerTest extends ApiTestCase
{
    public function testExportJson()
    {
        $postdata = '{"item_identifiers": ["sheep"],"combine": true}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/items/export',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );
        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testExportZip()
    {
        $postdata = '{"item_identifiers": ["sheep"],"combine": false}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/items/export',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertEquals('application/zip', $response->headers->get('content-type'));

        $zipContent = $response->getContent();
        $zipFile = tempnam(sys_get_temp_dir(), 'export_zip');
        file_put_contents($zipFile, $zipContent);

        $zip = new \ZipArchive();
        if (true === $zip->open($zipFile)) {
            $zip->extractTo(sys_get_temp_dir());
            $zip->close();
        } else {
            $this->fail('Failed to open the zip file');
        }

        $extractedFiles = scandir(sys_get_temp_dir());
        $jsonFile = null;
        foreach ($extractedFiles as $file) {
            if ('json' === pathinfo($file, PATHINFO_EXTENSION)) {
                $jsonFile = sys_get_temp_dir() . '/' . $file;

                break;
            }
        }

        $this->assertNotNull($jsonFile, 'No JSON file found in the zip archive');
        $jsonContent = file_get_contents($jsonFile);
        $expectedContent = json_encode(json_decode(file_get_contents(__DIR__ . '/_fixtures/ExportControllerTest/testExportJson.expected.json'))->body);

        $this->assertJsonStringEqualsJsonString($expectedContent, $jsonContent);

        unlink($zipFile);
        unlink($jsonFile);
    }
}
