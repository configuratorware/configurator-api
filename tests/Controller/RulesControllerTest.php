<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class RulesControllerTest extends ApiTestCase
{
    /**
     * @var RuleRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/RulesControllerTest_ruleData_up.sql'));

        $this->repository = $this->em->getRepository(Rule::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/rules');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('optionclassificationmaxamount', $result->data[0]->ruleType->identifier);
        self::assertEquals(5, $result->data[0]->data->maxamount);
        self::assertEquals('The maximum amount of options in the category "face" is "5".', $result->data[0]->title);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/rules/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('optionclassificationmaxamount', $result->ruleType->identifier);
        self::assertEquals(5, $result->data->maxamount);
    }

    public function testSave()
    {
        $postdata =
            '{"id":null,"data":{"maxamount":5,"optionclassification_identifier":"face"},"ruleType":{"id":"4","identifier":"optionclassificationmaxamount","availablerulefields":["maxamount","optionclassification_identifier"]},"item":{"id":"1"},"option":null}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/rules', [], [], ['CONTENT_TYPE' => 'application/json'], $postdata);
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/rules/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        self::assertEquals(null, $this->repository->findOneById(1));
    }

    public function testSaveExclusionRule()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/rules', [], [], ['CONTENT_TYPE' => 'application/json'], $this->exclusionRuleSave);

        $rules = $this->repository->findBy(['item' => 1, 'ruletype' => 2]);
        self::assertCount(2, $rules);
    }

    public function testShouldChangeExclusionRule()
    {
        $this->executeSql('INSERT INTO rule (id, ruletype_id, rulefeedback_id, item_id, option_id, data, date_created, date_deleted, user_created_id, reverse_rule_id) VALUES (3, 2, 1, 1, 7, \'{"option_identifier":"coat_red"}\',  "2021-01-22 13:07:23", "0001-01-01 00:00:00", 1, 4), (4, 2, 1, 1, 3, \'{"option_identifier":"legs_nature"}\',  "2021-01-22 13:07:23", "0001-01-01 00:00:00", 1, 3)');
        // sanity check
        $rules = $this->repository->findBy(['item' => 1, 'ruletype' => 2]);
        self::assertCount(2, $rules);

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/rules', [], [], ['CONTENT_TYPE' => 'application/json'], $this->exclusionRuleChange);
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $rules = $this->repository->findBy(['item' => 1, 'ruletype' => 2]);
        self::assertCount(2, $rules);
    }

    /**
     * @param $id
     *
     * @dataProvider providerShouldDeleteExclusionRule
     */
    public function testDeleteExclusionRule($id)
    {
        $this->executeSql('INSERT INTO rule (id, ruletype_id, rulefeedback_id, item_id, option_id, data, date_created, date_deleted, user_created_id, reverse_rule_id) VALUES (3, 2, 1, 1, 7, \'{"option_identifier":"coat_red"}\',  "2021-01-22 13:07:23", "0001-01-01 00:00:00", 1, 4), (4, 2, 1, 1, 3, \'{"option_identifier":"legs_nature"}\',  "2021-01-22 13:07:23", "0001-01-01 00:00:00", 1, 3)');
        // sanity check
        $rules = $this->repository->findBy(['item' => 1, 'ruletype' => 2]);
        self::assertCount(2, $rules);

        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', "/api/rules/$id");
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $rules = $this->repository->findBy(['item' => 1, 'ruletype' => 2]);
        self::assertCount(0, $rules);
    }

    public function providerShouldDeleteExclusionRule()
    {
        yield [3];
        yield [4];
    }

    private $exclusionRuleSave = <<<JSON
{
  "id": null,
  "item": 1,
  "option": {
    "id": 7,
    "identifier": "legs_nature",
    "configurable": null,
    "deactivated": false,
    "translated_title": "{legs_nature}",
    "translated_abstract": "",
    "translated_description": "",
    "texts": [],
    "attributes": [],
    "price": null,
    "prices": [
      {
        "id": 7,
        "price": "18.45",
        "channel": "1",
        "amountfrom": 1
      }
    ],
    "stock": [],
    "visualizationdata": null,
    "amount_is_selectable": null,
    "label": "{legs_nature} (legs_nature)"
  },
  "ruleType": {
    "id": 2,
    "identifier": "optionexclusion",
    "translated_title": "Expliziter Ausschluss zweier Optinen",
    "availablerulefields": [
      "option_identifier"
    ]
  },
  "data": {
    "option_identifier": "coat_red"
  }
}
JSON;

    private $exclusionRuleChange = <<<JSON
{
  "id": 3,
  "item": 1,
  "option": {
    "id": 7,
    "identifier": "legs_nature",
    "configurable": null,
    "deactivated": false,
    "translated_title": "{legs_nature}",
    "translated_abstract": "",
    "translated_description": "",
    "texts": [],
    "attributes": [],
    "price": null,
    "prices": [
      {
        "id": 7,
        "price": "18.45",
        "channel": "1",
        "amountfrom": 1
      }
    ],
    "stock": [],
    "visualizationdata": null,
    "amount_is_selectable": null,
    "label": "{legs_nature} (legs_nature)"
  },
  "ruleType": {
    "id": 2,
    "identifier": "optionexclusion",
    "translated_title": "Expliziter Ausschluss zweier Optinen",
    "availablerulefields": [
      "option_identifier"
    ]
  },
  "data": {
    "option_identifier": "coat_white"
  }
}
JSON;
}
