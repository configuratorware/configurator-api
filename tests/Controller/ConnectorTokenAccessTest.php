<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ConnectorTokenAccessTest extends ApiTestCase
{
    private const UNAUTHENTICATED = '{"code":401,"message":"Authentication failed"}';

    public function setUp(): void
    {
        parent::setUp();
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ConnectorTokenAccess/setUp.sql'));
    }

    public function testConnectorWithoutToken(): void
    {
        $client = self::createClient();
        $client->request('GET', '/connector/itemstatus/sheep');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        self::assertEquals(self::UNAUTHENTICATED, $response->getContent());
    }

    public function testConnectorWithWrongToken(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/connector/itemstatus/sheep',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => 'fake_token']
        );
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        self::assertEquals(self::UNAUTHENTICATED, $response->getContent());
    }

    public function testConnectorWithGoodTokenAndWrongCredentials(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/connector/itemstatus/sheep',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => 'test_api_token_no_credentials']
        );
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_UNAUTHORIZED, $response->getStatusCode());
        self::assertEquals(self::UNAUTHENTICATED, $response->getContent());
    }

    public function testConnectorWithGoodTokenAndGoodCredentials(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/connector/itemstatus/sheep',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => 'test_api_token_good_credentials']
        );
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());
        self::assertSame('[{"itemIdentifier":"sheep","configurable":true}]', $response->getContent());
    }

    public function testConnectorWithGoodTokenAndGoodCredentialsAndNotExistingItem(): void
    {
        $client = self::createClient();
        $client->request(
            'GET',
            '/connector/itemstatus/demo_sheep',
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => 'test_api_token_good_credentials']
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }
}
