<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class OptionPoolsControllerTest extends ApiTestCase
{
    /** @var Repository */
    protected $repository;

    /** @var EntityManager */
    protected $em;

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/OptionPoolsControllerTest_setUp.sql'));

        $this->repository = $this->em->getRepository(OptionPool::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/optionpools');

        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/optionpools/3001');

        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testSave()
    {
        $postdata = '{"identifier":"option_pool_test_2","options":[{"id":"3001"},{"id":"3002"}],"texts":[{"id":"1","language":"de_DE","title":"de_title","description":"de_descr"},{"language":"en_GB","title":"en_title","description":"en_descr"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/optionpools',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        $optionPool = $this->repository->findOneBy(['identifier' => 'option_pool_test_2']);

        self::assertInstanceOf(OptionPool::class, $optionPool);
    }

    public function testSaveTextWithTitle()
    {
        $postdata = '{"identifier":"option_pool_test_texts_only_title","options":[{"id":"3001"},{"id":"3002"}],"texts":[{"id":"1","language":"de_DE","title":"de_title"},{"language":"en_GB","title":"en_title"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/optionpools',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        /** @var OptionPool $optionPool */
        $optionPool = $this->repository->findOneBy(['identifier' => 'option_pool_test_texts_only_title']);
        $texts = $optionPool->getOptionPoolText();
        self::assertCount(2, $texts);
    }

    public function testSaveNotUnique()
    {
        $postdata = '{"identifier":"option_pool_test","options":[{"id":"3001"},{"id":"3002"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/optionpools',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode(), 'HTTP status code should be 400');
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/optionpools/3001');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $optionPool = $this->repository->findOneBy(['identifier' => 'option_pool_test_2']);

        self::assertEquals(null, $optionPool);
    }
}
