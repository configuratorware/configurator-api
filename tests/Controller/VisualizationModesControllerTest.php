<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use redhotmagma\SymfonyTestUtils\Fixture\TestHttpResponse;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class VisualizationModesControllerTest extends ApiTestCase
{
    /**
     * @param string $configurationMode
     * @dataProvider providerShouldList
     */
    public function testShouldList(string $configurationMode): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', "api/visualization_modes/$configurationMode");
        $response = $client->getResponse();

        Fixtures::assertResponseEquals(
            TestHttpResponse::fromFileContent(file_get_contents(__DIR__ . "/_fixtures/VisualizationModeControllerTest/testShouldList.$configurationMode.expected.json")),
            $response
        );
    }

    public function providerShouldList(): array
    {
        return [
            ['creator'],
            ['designer'],
            ['creator+designer'],
        ];
    }
}
