<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Currency;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CurrenciesControllerTest extends ApiTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Currency::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/currencies');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response,
            '{"metadata":{"totalcount":"1"},"data":[{"id":1,"iso":"eur","symbol":"\u20ac"}]}');
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/currencies/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"id":1,"iso":"eur","symbol":"\u20ac"}');
    }

    public function testSave()
    {
        $postdata = '{"iso":"gbp","symbol":"\u00a3"}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/currencies',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $currency = $this->repository->findOneByIso('gbp');

        self::assertInstanceOf(Currency::class, $currency);

        $expectedResult = json_decode($postdata);
        $receivedResult = json_decode($response->getContent());

        self::assertEquals($expectedResult->iso, $receivedResult->iso, 'content mismatch');
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/currencies/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $entity = $this->repository->findOneById(2);

        self::assertEquals(null, $entity);
    }
}
