<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ItemClassificationsControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Itemclassification::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/itemclassifications');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/itemclassifications/1');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testSave()
    {
        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/itemclassifications',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            file_get_contents(__DIR__ . '/_fixtures/ItemClassificationsControllerTest/testSave.given.json')
        );

        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/itemclassifications/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $itemclassification = $this->repository->findOneByIdentifier('mammal');

        self::assertEquals(null, $itemclassification);
    }

    public function testFrontendList(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemClassificationsController/frontendListSetUp.sql'));

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/frontendapi/item-classifications');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemClassificationsController/frontendListTearDown.sql'));
    }
}
