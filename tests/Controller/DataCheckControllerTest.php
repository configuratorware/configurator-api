<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DataCheckControllerTest extends ApiTestCase
{
    public function testShouldCheckSheep(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', 'api/data_checks/sheep');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     * @BackupDatabase
     */
    public function testShouldCheckDemoHoodie(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_hoodie.sql'));

        $client = $this->createAuthenticatedClient();
        $client->request('GET', 'api/data_checks/demo_hoodie');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }
}
