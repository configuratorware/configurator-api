<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypePrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypeText;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DesignerGlobalCalculationTypesControllerTest extends ApiTestCase
{
    /**
     * @var ConfigurationRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(DesignerGlobalCalculationType::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignerGlobalCalculationTypes/setUp.sql'));
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', 'api/designerglobalcalculationtypes');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designerglobalcalculationtypes/2');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testCreate()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignerGlobalCalculationTypes/createJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designerglobalcalculationtypes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $testInstance = $this->repository->findOneBy(['identifier' => 'setup_test']);

        self::assertInstanceOf(
            DesignerGlobalCalculationType::class,
            $testInstance,
            'DesignerGlobalCalculationType has not been created.'
        );
        self::assertInstanceOf(
            DesignerGlobalCalculationTypeText::class,
            $testInstance->getDesignerGlobalCalculationTypeText()[0],
            'Text has not been created.'
        );
        self::assertInstanceOf(
            DesignerGlobalCalculationTypePrice::class,
            $testInstance->getDesignerGlobalCalculationTypePrice()[0],
            'DPrice has not been created.'
        );
    }

    public function testUpdate()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignerGlobalCalculationTypes/updateJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designerglobalcalculationtypes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $testInstance = $this->repository->findOneBy(['id' => 2]);

        self::assertEquals(
            !false,
            strpos($testInstance->getDesignerGlobalCalculationTypeText()[0]->getTitle(), 'changed'),
            'Value has not been changed'
        );
        self::assertNull(
            $testInstance->getDesignerGlobalCalculationTypePrice()[0],
            'PriceModel has not been removed'
        );
    }

    public function testDelete()
    {
        $testId = $this->repository->findOneBy(['identifier' => 'setup'])->getId();

        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', 'api/designerglobalcalculationtypes/' . $testId);
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $testFailed = false;

        $sql = file_get_contents(__DIR__ . '/_data/DesignerGlobalCalculationTypes/deleteCheck.sql');
        $result = $this->em->getConnection()->query($sql)->fetchAll();

        foreach ($result as $rows) {
            foreach (array_values($rows) as $data) {
                if ('0001-01-01 00:00:00' === $data) {
                    $testFailed = true;

                    break;
                }
            }
            if (true == $testFailed) {
                break;
            }
        }

        self::assertFalse($testFailed, 'Not all Design View Relations have been marked as date_deleted.');
    }
}
