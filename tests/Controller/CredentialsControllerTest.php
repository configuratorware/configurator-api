<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Credential;
use Redhotmagma\ConfiguratorApiBundle\Repository\CredentialRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CredentialsControllerTest extends ApiTestCase
{
    /**
     * @var CredentialRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Credential::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/credentials');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }
}
