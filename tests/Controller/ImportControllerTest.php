<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @BackupDatabase
 */
class ImportControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Import/setUp.sql'));
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function tearDown(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Import/tearDown.sql'));
    }

    public function testImportJson()
    {
        $payload = file_get_contents(__DIR__ . '/_data/Import/endpoint_test.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/connector/import',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => 'test_api_token_good_credentials',
            ],
            $payload
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
    }

    public function testImportXML()
    {
        $payload = file_get_contents(__DIR__ . '/_data/Import/endpoint_test.xml');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/connector/import',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/xml',
                'HTTP_X-AUTH-TOKEN' => 'test_api_token_good_credentials',
            ],
            $payload
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
    }

    public function testImportFails()
    {
        $payload = file_get_contents(__DIR__ . '/_data/Import/endpoint_test_currupted.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/connector/import',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X-AUTH-TOKEN' => 'test_api_token_good_credentials',
            ],
            $payload
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode(), 'HTTP status code should be 400');
    }
}
