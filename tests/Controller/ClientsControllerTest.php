<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\PathsMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * @MockPaths
 */
class ClientsControllerTest extends ApiTestCase
{
    use VfsTestTrait;

    /**
     * @var ClientRepository
     */
    private $repository;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var vfsStreamDirectory
     */
    private $vfsRoot;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Client::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Client/setUp.sql'));

        $this->filesystem = new Filesystem();
        $this->vfsRoot = vfsStream::setup();
        $this->filesystem->copy(__DIR__ . '/_data/License/license.txt', PathsMock::MOCKED_LICENSE_PATH);
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('GET', '/api/clients?limit=10');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('GET', '/api/clients/3');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testGetCurrent(): void
    {
        $client = $this->createAuthenticatedClient('rhm');

        $client->request('GET', '/api/clients/current');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testGetDefault(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('GET', '/api/clients/default');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testGetCurrentNoClientPresent(): void
    {
        $client = $this->createAuthenticatedClient('no_client', 'admin', '/api/login_check', true);
        $client->request('GET', '/api/clients/current');

        self::assertSame(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public function testGetCurrentWithNotClientUser(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/clients/current');

        self::assertSame(Response::HTTP_UNAUTHORIZED, $client->getResponse()->getStatusCode());
    }

    public function testSave(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/clients',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            file_get_contents(__DIR__ . '/_data/Client/saveClient_postData.json')
        );

        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testSaveCurrent()
    {
        $postData = '{"id": 2, "identifier":"test_current_client_identifier", "pdfHeaderHtml":"test_current_client_pdf_header", "callToAction":"addToCart"}';

        $client = $this->createAuthenticatedClient('rhm');
        $client->request('POST', '/api/clients/current', [], [], ['CONTENT_TYPE' => 'application/json'], $postData);

        self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $clientEntity = $this->repository->findOneBy(['pdf_header_html' => 'test_current_client_pdf_header']);

        self::assertInstanceOf(Client::class, $clientEntity);

        // check if identifier is unchanged, still the original correct one
        self::assertSame('rhm', $clientEntity->getIdentifier());
    }

    public function testSaveWithClientText()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Client/setUpClientText.sql'));

        $client = $this->createAuthenticatedClient();

        $client->request(
            'POST',
            '/api/clients',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            file_get_contents(__DIR__ . '/_data/Client/saveClientAllowed.json')
        );

        self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'HTTP status code not ok');

        $clientEntity = $this->repository->findOneBy(['identifier' => 'rhm']);

        foreach ($clientEntity->getClientText() as $text) {
            self::assertStringEndsWith('link', $text->getTermsAndConditionsLink(), 'link was not updated');
        }
    }

    public function testSaveDefaultIdentfier()
    {
        $postData = '{"id": 1, "identifier":"_default_changed"}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/clients', [], [], ['CONTENT_TYPE' => 'application/json'], $postData);

        self::assertSame(Response::HTTP_INTERNAL_SERVER_ERROR, $client->getResponse()->getStatusCode());

        $clientEntity = $this->repository->findOneBy(['identifier' => '_default']);

        self::assertInstanceOf(Client::class, $clientEntity);
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();

        $client->request('DELETE', '/api/clients/3');

        self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'HTTP status code not ok');

        self::assertEquals(null, $this->repository->findOneBy(['identifier' => 'someone_else']));
    }

    public function testFrontend()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Client/setUpClientFrontend.sql'));

        $client = $this->createAuthenticatedClient();

        $client->request('GET', '/frontendapi/client');

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @dataProvider provideFontUploadTestData
     */
    public function testFontUpload(string $postUrl, array $fontData, int $expectedStatusCode, string $expectedResultContent): void
    {
        vfsStream::copyFromFileSystem(__DIR__ . '/_data/Client');

        $fontFile = new UploadedFile($this->vfsRoot->url() . '/' . $fontData[0], ...$fontData);

        $client = $this->createAuthenticatedClient();
        $client->request('POST', $postUrl, [], ['font' => $fontFile]);

        $response = $client->getResponse();

        self::assertSame($expectedStatusCode, $response->getStatusCode());
        self::assertSame($expectedResultContent, $response->getContent());
        if (200 === $response->getStatusCode()) {
            self::assertFileExists($this->vfsRoot->url() . '/client/_default/font/' . $fontData[0]);
        }
    }

    /**
     * @return array
     */
    public function provideFontUploadTestData(): array
    {
        return [
            // valid upload
            [
                '/api/clients/upload/font/1',
                [
                    'test_font.ttf',
                    'font/ttf',
                    null,
                ],
                200,
                '',
            ],
            // invalid font file
            [
                '/api/clients/upload/font/1',
                [
                    'test_font.txt',
                    'text/plain',
                    null,
                ],
                400,
                '{"errors":[{"property":"uploaded_file","invalidvalue":"test_font.txt","message":"file_type_not_allowed"}],"success":false,"code":"validation_error"}',
            ],
            // not existing client
            [
                '/api/clients/upload/font/555',
                [
                    'test_font.ttf',
                    'font/ttf',
                    null,
                ],
                404,
                '{"errors":[],"success":false,"code":"not_found"}',
            ],
        ];
    }

    /**
     * @param $postUrl
     * @param $logoData
     * @param $expectedStatusCode
     * @param $expectedResultContent
     *
     * @dataProvider provideLogoUploadTestData
     */
    public function testLogoUpload(string $postUrl, array $logoData, int $expectedStatusCode, string $expectedResultContent): void
    {
        vfsStream::copyFromFileSystem(__DIR__ . '/_data/Client');
        $this->filesystem->copy(__DIR__ . '/_data/License/license.txt', PathsMock::MOCKED_LICENSE_PATH);

        $fontFile = new UploadedFile($this->vfsRoot->url() . '/' . $logoData[0], ...$logoData);

        $client = $this->createAuthenticatedClient();
        $client->request('POST', $postUrl, [], ['logo' => $fontFile]);

        $response = $client->getResponse();
        $content = $response->getContent();

        self::assertSame($expectedStatusCode, $response->getStatusCode());
        self::assertSame($expectedResultContent, $response->getContent());
        if (200 === $response->getStatusCode()) {
            self::assertFileExists($this->vfsRoot->url() . '/client/_default/logo/' . $logoData[0]);
        }
    }

    /**
     * @return array
     */
    public function provideLogoUploadTestData(): array
    {
        return [
            // valid upload
            [
                '/api/clients/upload/logo/1',
                [
                    'rhm_logo.jpg',
                    'image/jpeg',
                    null,
                ],
                200,
                '',
            ],
            // invalid font file
            [
                '/api/clients/upload/logo/1',
                [
                    'test_font.txt',
                    'text/plain',
                    null,
                ],
                400,
                '{"errors":[{"property":"uploaded_file","invalidvalue":"test_font.txt","message":"file_type_not_allowed"}],"success":false,"code":"validation_error"}',
            ],
            // not existing client
            [
                '/api/clients/upload/logo/555',
                [
                    'rhm_logo.jpg',
                    'image/jpeg',
                    null,
                ],
                404,
                '{"errors":[],"success":false,"code":"not_found"}',
            ],
        ];
    }
}
