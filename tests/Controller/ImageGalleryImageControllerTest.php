<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ImageGalleryImageControllerTest extends ApiTestCase
{
    protected ImageGalleryImageRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        // create data to be able to run the test
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ImageGalleryImage/setUp.sql'));
        $this->repository = $this->em->getRepository(ImageGalleryImage::class);
    }

    public function testFrontendListTags()
    {
        $expected = file_get_contents(__DIR__ . '/_data/ImageGalleryImage/frontendListTags.json');

        $client = $this->createAuthenticatedClient();

        $client->request(
            'GET',
            '/frontendapi/imagegalleryimage/tags',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, $expected);
    }

    public function testFrontendList()
    {
        $expected = file_get_contents(__DIR__ . '/_data/ImageGalleryImage/frontendList.json');

        $client = $this->createAuthenticatedClient();

        $client->request(
            'GET',
            '/frontendapi/imagegalleryimage?tags=Tag1,Tag2&title=Im',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, $expected);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/imagegalleryimages');

        $response = $client->getResponse();
        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/imagegalleryimages/101?_language=de_DE');

        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testSave()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/ImageGalleryImage/save.json');

        $client = $this->createAuthenticatedClient();

        $client->request(
            'POST',
            '/api/imagegalleryimages',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testSaveSequenceNumber()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/ImageGalleryImage/saveSequenceNumber.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/imagegalleryimages/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->repository->find(101);
        self::assertNotNull($testInstance);
        self::assertEquals(
            100,
            $testInstance->getSequenceNumber(),
            'Sequence Number has not been updated.'
        );
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();

        $client->request('DELETE', '/api/imagegalleryimages/101');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $imageGalleryImage = $this->repository->find(101);

        self::assertEquals(null, $imageGalleryImage);
    }
}
