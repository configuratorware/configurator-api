<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CalculationControllerTest extends ApiTestCase
{
    /**
     * Path for SQL files.
     *
     * @var string
     */
    public const SETUP_FILE_PATH = __DIR__ . '/_data/DesignerCalculation/';

    /**
     * Path for expected result json files.
     *
     * @var string
     */
    public const TEST_FILE_PATH = __DIR__ . '/_data/CalculationController/';

    /**
     * Test basic calculation.
     */
    public function testCalculate()
    {
        $postData = file_get_contents(
            self::TEST_FILE_PATH . '/CalculationControllerTest_configurationData.json'
        );

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/frontendapi/calculation',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertJsonStringEqualsJsonFile(
            self::TEST_FILE_PATH . 'expected_calculation_result_for_Calculate.json',
            $response->getContent(),
            'JSON content mismatch'
        );
    }

    /**
     * Test with user selected calculations added.
     */
    public function testCalculateDesignerUserSelected()
    {
        $postData = file_get_contents(self::TEST_FILE_PATH . 'designerCalculationUserSelected.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/frontendapi/calculation',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertJsonStringEqualsJsonFile(
            self::TEST_FILE_PATH . 'expected_calculation_result_for_UserSelected.json',
            $response->getContent(),
            'JSON content mismatch'
        );
    }

    /**
     * Test without user selected calculations.
     */
    public function testCalculateDesignerNotUserSelected()
    {
        $postData = file_get_contents(self::TEST_FILE_PATH . 'designerCalculationNotUserSelected.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/frontendapi/calculation',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertJsonStringEqualsJsonFile(
            self::TEST_FILE_PATH . 'expected_calculation_result_for_NotUserSelected.json',
            $response->getContent(),
            'JSON content mismatch'
        );
    }

    /**
     * Testing with bulk name dependent global and production calculation.
     */
    public function testCalculateWithBulkNameDependentType()
    {
        $postData = file_get_contents(self::TEST_FILE_PATH . 'designerCalculationBulkNameDependent.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/frontendapi/calculation',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertJsonStringEqualsJsonFile(
            self::TEST_FILE_PATH . 'expected_calculation_result_for_BulkNameDependentType.json',
            $response->getContent(),
            'JSON content mismatch'
        );
    }

    /**
     * Testing after bulkNameDependent calculation is deleted.
     */
    public function testCalculateWithBulkNameDependentAfterBulkNameTypeDelete()
    {
        // delete bulk name types and try the calculation

        /* @var $globalCalculationType DesignerGlobalCalculationType */
        $globalCalculationType = $this->em
            ->getRepository(DesignerGlobalCalculationType::class)
            ->find(3);

        /* @var $designerProductionCalculationType DesignerProductionCalculationType */
        $designerProductionCalculationType = $this->em
            ->getRepository(DesignerProductionCalculationType::class)
            ->find(3);

        $globalCalculationType->setDateDeleted(new \DateTime());
        $designerProductionCalculationType->setDateDeleted(new \DateTime());

        $this->em->persist($designerProductionCalculationType);
        $this->em->persist($globalCalculationType);
        $this->em->flush();

        $postData = file_get_contents(self::TEST_FILE_PATH . 'designerCalculationBulkNameDependent.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/frontendapi/calculation',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertJsonStringEqualsJsonFile(
            self::TEST_FILE_PATH . 'expected_calculation_result_for_UserSelected.json',
            $response->getContent(),
            'JSON content mismatch'
        );
    }

    /**
     * Test with ImageGallery Images calculations added.
     */
    public function testCalculateDesignerWithImageGalleryImages()
    {
        $postData = file_get_contents(self::TEST_FILE_PATH . 'designerCalculationWithImageGalleryImages.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/frontendapi/calculation',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertJsonStringEqualsJsonFile(
            self::TEST_FILE_PATH . 'expected_calculation_result_for_ImageGalleryImages.json',
            $response->getContent(),
            'JSON content mismatch'
        );
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->executeSql(file_get_contents(self::SETUP_FILE_PATH . 'setUp.sql'));
    }
}
