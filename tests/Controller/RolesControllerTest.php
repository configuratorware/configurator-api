<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class RolesControllerTest extends ApiTestCase
{
    /**
     * @var RoleRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Role::class);
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/roles');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/roles/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testSave(): void
    {
        $postdata = '{"name":"superuser","role":"ROLE_SUPERUSER","credentials":[1,2,3]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/roles',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $role = $this->repository->findOneByName('superuser');

        self::assertInstanceOf(Role::class, $role);

        $expectedResult = json_decode($postdata);
        $receivedResult = json_decode($response->getContent());

        self::assertEquals($expectedResult->role, $receivedResult->role, 'content mismatch');
    }

    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/roles/3');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertEquals(null, $this->repository->find(3));
    }
}
