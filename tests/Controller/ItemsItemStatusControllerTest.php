<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ItemsItemStatusControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemsItemStatus/setUp.sql'));
    }

    public function testConnectorListParents(): void
    {
        $itemIdentifiers = 'test_item_available,test_item_available_creator,test_item_available_designer,,,test_item_unavailable';

        $client = self::createClient();
        $client->request(
            'GET',
            '/connector/itemstatus/' . $itemIdentifiers,
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => 'test_api_token_good_credentials']
        );
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testConnectorListChildren(): void
    {
        $itemIdentifiers = 'test_child_available,test_child_unavailable';

        $client = self::createClient();
        $client->request(
            'GET',
            '/connector/itemstatus/' . $itemIdentifiers,
            [],
            [],
            ['HTTP_X-AUTH-TOKEN' => 'test_api_token_good_credentials']
        );
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }
}
