<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Tag;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class TagsControllerTest extends ApiTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        // create data to be able to run the test
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Tags/setUp.sql'));
        $this->repository = $this->em->getRepository(Tag::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/tags');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            '{"metadata":{"totalcount":"2"},"data":[{"id":"1","translated_title":"","is_multiselect":false,"is_background":false,"sequenceNumber":0,"translations":[{"id":"1","translation":"Test","language":"de_DE"}]},{"id":"2","translated_title":"","is_multiselect":false,"is_background":false,"sequenceNumber":0,"translations":[]}]}'
        );
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/tags/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            '{"id":"1","translated_title":"","is_multiselect":false,"is_background":false,"sequenceNumber":0,"translations":[{"id":"1","translation":"Test","language":"de_DE"}]}'
        );
    }

    public function testSave()
    {
        $postdata = '{"is_multiselet":0,"is_background":0,"sequenceNumber":333,"translations":[{"language":"en_GB","translation":"my test"},{"language":"de_DE","translation":"Mein Test"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/tags',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        self::assertInstanceOf(Tag::class, $this->repository->findOneBy(['sequence_number' => 333]));
    }

    public function testSaveSequenceNumber()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/Tags/saveSequenceNumber.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/tags/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->repository->findOneBy(['id' => 2]);

        self::assertEquals(
            200,
            $testInstance->getSequenceNumber(),
            'Sequence Number has not been updated.'
        );
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/tags/2');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $tag = $this->repository->findOneById(2);

        self::assertEquals(null, $tag);
    }
}
