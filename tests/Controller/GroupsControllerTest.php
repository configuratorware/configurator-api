<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class GroupsControllerTest extends ApiTestCase
{
    /**
     * @var ItemgroupRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Itemgroup::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/groups?limit=10');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            '{"metadata":{"totalcount":"2"},"data":[{"id":1,"identifier":"color","translation":null,"translations":[{"id":1,"language":"en_GB","translation":"Color"},{"id":2,"language":"de_DE","translation":"Farbe"}]},{"id":2,"identifier":"size","translation":null,"translations":[{"id":3,"language":"en_GB","translation":"Size"},{"id":4,"language":"de_DE","translation":"Gr\u00f6\u00dfe"}]}]}'
        );
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/groups/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            '{"id":1,"identifier":"color","translation":null,"translations":[{"id":1,"language":"en_GB","translation":"Color"},{"id":2,"language":"de_DE","translation":"Farbe"}]}'
        );
    }

    public function testSave()
    {
        $postData = '{"identifier":"shape","translation":null,"translations":[{"language":"en_GB","translation":"Shape"},{"language":"de_DE","translation":"Form"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/groups',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $group = $this->repository->findOneBy(['identifier' => 'shape']);
        self::assertEquals(3, $group->getSequenceNumber());

        self::assertInstanceOf(Itemgroup::class, $group);
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/groups/2');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $group = $this->repository->findOneBy(['identifier' => 'size']);

        self::assertEquals(null, $group);
    }

    public function testUpdate()
    {
        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/groups',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            file_get_contents(__DIR__ . '/_fixtures/GroupsControllerTest/testUpdate.given.json')
        );
        $group = $this->repository->findOneBy(['identifier' => 'color']);

        self::assertEquals(1, $group->getSequenceNumber());
        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testSaveSequenceNumbers()
    {
        $postData = '[{"id":1,"sequencenumber":100},{"id":2,"sequencenumber":101}]';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/groups/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $group = $this->repository->findOneBy(['id' => 1]);

        self::assertInstanceOf(Itemgroup::class, $group);
        self::assertEquals(100, $group->getSequencenumber());
    }
}
