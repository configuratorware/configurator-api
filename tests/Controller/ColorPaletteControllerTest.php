<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Color;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPaletteText;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorText;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorPaletteRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * Tests for {@link ColorPaletteController}.
 */
class ColorPaletteControllerTest extends ApiTestCase
{
    /**
     * @var ColorPaletteRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(ColorPalette::class);
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/colorpalettes');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDetail(): void
    {
        $sql = file_get_contents(__DIR__ . '/_data/ColorPalette/updateColors.sql');
        $this->executeSql($sql);

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/colorpalettes/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testCreate(): void
    {
        $postdata = file_get_contents(__DIR__ . '/_data/ColorPalette/create.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/colorpalettes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        /**
         * @var ColorPalette $colorPalette
         */
        $colorPalette = $this->repository->findOneBy(['identifier' => '900000']);

        self::assertInstanceOf(
            ColorPalette::class,
            $colorPalette,
            'Color Palette has not been created.'
        );

        self::assertInstanceOf(
            ColorPaletteText::class,
            $colorPalette->getColorPaletteText()[0],
            'ColorPaletteText Palette has not been created.'
        );
        self::assertInstanceOf(
            Color::class,
            $colorPalette->getColor()[0],
            'Color has not been created.'
        );
        self::assertInstanceOf(
            ColorText::class,
            $colorPalette->getColor()[0]->getColorText()[0],
            'ColorText Palette has not been created.'
        );
        self::assertInstanceOf(
            Color::class,
            $colorPalette->getDefaultColor(),
            'Default color has not been created.'
        );
    }

    public function testDeleteDefaultColor(): void
    {
        $postdata = file_get_contents(__DIR__ . '/_data/ColorPalette/deleteDefault.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/colorpalettes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        /**
         * @var ColorPalette $colorPalette
         */
        $colorPalette = $this->repository->findOneBy(['identifier' => '900000']);

        self::assertNull(
            $colorPalette->getDefaultColor(),
            'Default color has not been deleted.'
        );
    }

    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', 'api/colorpalettes/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $testFailed = false;

        $sql = file_get_contents(__DIR__ . '/_data/ColorPalette/deleteCheck.sql');
        $result = $this->em->getConnection()->query($sql)->fetchAll();

        foreach ($result as $rows) {
            foreach (array_values($rows) as $data) {
                if ('0001-01-01 00:00:00' === $data) {
                    $testFailed = true;

                    break;
                }
            }
            if (true == $testFailed) {
                break;
            }
        }

        self::assertFalse($testFailed, 'Not all ColorPalette Relations have been marked as date_deleted.');
    }

    public function testShouldSaveColorSequence()
    {
        $headers = ['CONTENT_TYPE' => 'application/json'];
        $uri = 'api/colorpalettes/1/colors/savesequencenumbers';
        $data = file_get_contents(__DIR__ . '/_data/ColorPalette/saveSequenceNumber.json');
        $expected = json_decode(file_get_contents(__DIR__ . '/_data/ColorPalette/expectedSequenceNumber.json'), true);

        $client = $this->createAuthenticatedClient();
        $client->request(Request::METHOD_POST, '' . $uri . '', [], [], $headers, $data);
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $actual = array_map(static function (Color $color) {
            return ['id' => $color->getId(), 'sequence_number' => $color->getSequenceNumber()];
        }, $this->em->getRepository(Color::class)->findById([1, 2, 3]));

        self::assertCount(3, $actual);
        self::assertEquals($expected, $actual);
    }
}
