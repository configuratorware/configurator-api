<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use org\bovigo\vfs\vfsStream;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Mock\PathsMock;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * @MockPaths
 */
class SettingsControllerTest extends ApiTestCase
{
    use VfsTestTrait;

    public function setUp(): void
    {
        parent::setUp();
        $this->filesystem = new Filesystem();
        $this->vfsRoot = vfsStream::setup();
    }

    public function testDetail()
    {
        $this->filesystem->copy(__DIR__ . '/_data/License/license.txt', PathsMock::MOCKED_LICENSE_PATH);
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/settings/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response,
            '{"id":1,"maxZoom2d":200,"customCss":null,"defaultVisualizationModeLabel":"2d","showItemIdentifier":false, "calculationMethod": "sumofall", "shareurl": "http://localhost:10030/code:{code}", "configurationsClientRestricted": false}');
    }

    public function testDetailNoClientsFeature()
    {
        $this->filesystem->copy(__DIR__ . '/_data/License/license_no_clients_feature.txt', PathsMock::MOCKED_LICENSE_PATH);
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/settings/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response,
            '{"id":1,"maxZoom2d":200,"customCss":null,"defaultVisualizationModeLabel":"2d","showItemIdentifier":false, "calculationMethod": "sumofall", "shareurl": "http://localhost:10030/code:{code}"}');
    }

    public function testSave()
    {
        $this->filesystem->copy(__DIR__ . '/_data/License/license.txt', PathsMock::MOCKED_LICENSE_PATH);
        $client = $this->createAuthenticatedClient();

        $post = '{"id":1,"maxZoom2d":250,"customCss":"html body { margin: 0; padding: 0; }","defaultVisualizationModeLabel":"3d","showItemIdentifier":true, "calculationMethod": "deltaprices", "shareurl": "http://configuratorware.de/code:{code}", "configurationsClientRestricted": true}';
        $client->request('POST', 'api/settings', [], [], ['CONTENT_TYPE' => 'application/json'], $post);

        self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode(), 'HTTP status code not ok');

        $client->request('GET', '/api/settings/1');
        $this->compareResponseContent($client->getResponse(), $post);

        $client->restart();
        // reset the data
        $client->request('POST', 'api/settings', [], [], ['CONTENT_TYPE' => 'application/json'],
            '{"id":1,"maxZoom2d":200,"customCss":null,"defaultVisualizationModeLabel":"2d","showItemIdentifier":false, "calculationMethod": "deltaprices", "shareurl": "http://configuratorware.de/code:{code}", "configurationsClientRestricted": true}'
        );
    }
}
