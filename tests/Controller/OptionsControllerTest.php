<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use redhotmagma\SymfonyTestUtils\Fixture\ResponseFixtureTrait;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class OptionsControllerTest extends ApiTestCase
{
    use ResponseFixtureTrait;

    /**
     * @var \Doctrine\Persistence\ObjectRepository|\Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Option::class);
    }

    public function testFrontendList(): void
    {
        $client = $this->createAuthenticatedClient();

        $postdata =
            '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}}';

        $client->request(
            'POST',
            '/frontendapi/options/list/sheep/coat/',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testFrontendMatchingOptions(): void
    {
        $client = static::createClient();
        $postdata =
            '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"1","language":"en_GB","title":"tags"},{"id":"2","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","priceNet":"167.22","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","priceNet":"158.82","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","priceNet":"10.46","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","priceNet":"12.98","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","priceNet":"15.50","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","priceNet":"18.87","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","priceNet":"21.93","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}}';

        $client->request(
            'POST',
            '/frontendapi/options/matchingoptions/coat/',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testFrontenddetail(): void
    {
        $client = static::createClient();

        $postdata =
            '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"1","language":"en_GB","title":"tags"},{"id":"2","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}}';

        $client->request(
            'POST',
            '/frontendapi/options/detail/coat_red/',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testList(): void
    {
        $client = static::createAuthenticatedClient();
        $client->request('GET', '/api/options?limit=10');

        $expected = $this->loadResponseFixture();
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testDetail(): void
    {
        $client = static::createAuthenticatedClient();
        $client->request('GET', '/api/options/1');

        $expected = $this->loadResponseFixture();
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testSave(): void
    {
        $postdata = '{"identifier":"coat_yellow","configurable":true,"deactivated":false,"texts":[{"id":5,"language":"en_GB","title":"test","abstract":"","description":""},{"id":6,"language":"de_DE","title":"Test","abstract":"","description":""}],"attributes":[{"attribute_id":1,"identifier":"new","attributedatatype":"string","values":[1,2]}],"prices":[{"price":"19.00", "priceNet":"15.96","channel":1,"amountfrom":1},{"price":"15.12", "priceNet":"19.00","channel":1,"amountfrom":10}],"stock":[{"status":"available","deliverytime":"+3 weeks","channel":1}],"itemclassifications":[1]}';

        $client = static::createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/options',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $option = $this->repository->findOneByIdentifier('coat_yellow');

        self::assertInstanceOf(Option::class, $option);
    }

    public function testDelete(): void
    {
        $client = static::createAuthenticatedClient();
        $client->request('DELETE', '/api/options/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertNull($this->repository->find(1));
    }

    public function testGroupedFrontendList(): void
    {
        // create data to be able to run the test
        $this->executeSql(file_get_contents(__DIR__ . '/_data/OptionsControllerTest_groupData_up.sql'));

        $client = static::createClient();
        $postdata =
            '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"1","language":"en_GB","title":"tags"},{"id":"2","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","priceNet":"167.22","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","priceNet":"158.82","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","priceNet":"12.98","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","priceNet":"15.50","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","priceNet":"18.87","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","priceNet":"21.39","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}}';

        $client->request(
            'POST',
            '/frontendapi/options/list/sheep/coat/',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testSavesequencenumbers(): void
    {
        $postdata = '[{"id":1,"sequencenumber":100},{"id":2,"sequencenumber":101}]';

        $client = static::createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/options/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $option = $this->repository->findOneById(1);

        self::assertInstanceOf(Option::class, $option);
        self::assertEquals(100, $option->getSequencenumber());
    }

    /**
     * Test for previous bug of option being present multiple times on the same item, causing random
     * `item_optionclassification_option` data to be used for `amount_is_selectable` value.
     */
    public function testCorrectAmountIsSelectableValueOnMultipleUsageOfSameOption(): void
    {
        // setup test data to produce the bug: amount_is_selectable = true instead of false
        $this->executeSql('
            INSERT INTO item_optionclassification_option 
                (id, amountisselectable, date_created, date_deleted, item_optionclassification_id, option_id) 
            VALUES 
                   (555, 1, \'2020-07-23 16:29:54\',  \'0001-01-01 00:00:00\', 5, 3);');

        $postdata =
            '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"K\u00f6nnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"2","language":"en_GB","title":"tags"},{"id":"1","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle m\u00f6glichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle m\u00f6glichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"date_created":{"date":"2017-06-22 09:44:49.000000","timezone_type":3,"timezone":"Europe\/Paris"}}}';

        $client = static::createClient();
        $client->request(
            'POST',
            '/frontendapi/options/list/sheep/coat/',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());

        $data = json_decode($response->getContent());
        self::assertFalse($data[2]->amount_is_selectable);
    }

    public function testInputValidationList(): void
    {
        $client = static::createAuthenticatedClient();
        $client->request('GET', '/api/options/inputvalidationtypes');

        $expected = $this->loadResponseFixture();
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($expected, $response);
    }
}
