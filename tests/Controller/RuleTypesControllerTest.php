<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class RuleTypesControllerTest extends ApiTestCase
{
    public function testFilteredList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/ruletypes?filters[ruletype.optionrule]=1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"metadata":{"totalcount":"4"},"data":[{"id":1,"identifier":"optiondependency","translated_title":"One option depends on another","availablerulefields":["option_identifier"]},{"id":2,"identifier":"optionexclusion","translated_title":"One option excludes another","availablerulefields":["option_identifier"]},{"id":7,"identifier":"optionmaxamount","translated_title":"Maximum amount of one option","availablerulefields":["maxamount"]},{"id":9,"identifier":"additionaloption","translated_title":"Automatic selection of an option","availablerulefields":["option_identifier"]}]}');
    }
}
