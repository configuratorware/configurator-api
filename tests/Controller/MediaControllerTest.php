<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * Class MediaControllerTest.
 *
 * @author Daniel Klier <klier@redhotmagma.de>
 */
class MediaControllerTest extends ApiTestCase
{
    /**
     * Base dir to load files from.
     */
    public const MEDIA_DIR = __DIR__ . '/_data/media';

    public const CACHE_DIR = __DIR__ . '/_data/media_cache';

    protected function setUp(): void
    {
        if (!is_dir(self::CACHE_DIR)) {
            mkdir(self::CACHE_DIR, 0777, true);
        }
        $this->clearCacheDir();
    }

    protected function tearDown(): void
    {
        $this->clearCacheDir();
        if (is_dir(self::CACHE_DIR)) {
            rmdir(self::CACHE_DIR);
        }
    }

    public function testItShouldOnlyAllowConfiguredPathPattern()
    {
        $valid = [
            '/media/rhm_bild_rot.png', '/media/deep/deeper/test_100x100.gif', '/media/path-test+img_2.jpeg',
        ];
        $invalid = ['/media/img/../rhm_bild_rot.png', '/media/.htaccess', '/media/some_file'];

        $client = $this->createAuthenticatedClient();

        foreach ($valid as $validPath) {
            $client->request('GET', $validPath);
            self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        }

        foreach ($invalid as $invalidPath) {
            $client->request('GET', $invalidPath);
            self::assertSame(Response::HTTP_FORBIDDEN, $client->getResponse()->getStatusCode());
        }
    }

    // TODO: move to the bottom again
    public function testItShouldNotAllowBreakingOutOfTheBaseDir()
    {
        $pathOutsideDir = self::MEDIA_DIR . '/../test.txt';

        try {
            $paths = [
                '/../test.txt',
                '/%2e./test.txt',
                '/.%2e/test.txt',
                '/%2e%2e/test.txt',
                '/..%2ftest.txt',
                '/%2e%2e%2ftest.txt',
            ];
            file_put_contents($pathOutsideDir, 'Test');

            $client = $this->createAuthenticatedClient();

            foreach ($paths as $path) {
                self::assertNoBreakout($client, $path, 'Test');
            }
        } catch (\Exception $e) {
            throw $e;
        } finally {
            unlink($pathOutsideDir);
        }
    }

    private function assertNoBreakout(KernelBrowser $client, string $path, string $content)
    {
        $client->request('GET', '/media' . $path);
        $response = $client->getResponse();
        self::assertNotSame(Response::HTTP_INTERNAL_SERVER_ERROR, $response->getStatusCode(), 'Server must not throw an error');
        self::assertNotSame(Response::HTTP_OK, $response->getStatusCode(), 'Status code must not be 200 (OK)');
        self::assertNotSame('Test', $response->getContent());
    }

    /**
     * Test normal flow for files that exist.
     *
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testGetWhenFilesExist()
    {
        $this->performTestForExistingFile('rhm_bild_rot.png', 'image/png');
        $this->performTestForExistingFile('deep/deeper/test_100x100.gif', 'image/gif');
        $this->performTestForExistingFile('duck.svg', 'image/svg+xml');
    }

    /**
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testGetWithScale()
    {
        $fileName = 'rhm_bild_rot.png';
        [$orgWidth, $orgHeight] = getimagesize(self::MEDIA_DIR . '/' . $fileName);

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/media/' . $fileName . '?scale=25');
        $png = $client->getResponse()->getContent();
        [$width, $height] = getimagesizefromstring($png);

        self::assertEquals((int)$orgWidth * 0.25, $width);
        self::assertEquals((int)$orgHeight * 0.25, $height);
    }

    /**
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testGetWithMobileParam()
    {
        $fileName = 'rhm_bild_rot.png';
        [$orgWidth, $orgHeight] = getimagesize(self::MEDIA_DIR . '/' . $fileName);

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/media/' . $fileName . '?mobile=1');
        $png = $client->getResponse()->getContent();
        [$width, $height] = getimagesizefromstring($png);

        self::assertEquals((int)$orgWidth * 0.5, $width);
        self::assertEquals((int)$orgHeight * 0.5, $height);
    }

    /**
     * Verify that controller generates a 404 response if the file was not found.
     *
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testGetOfNonExstingFileShouldReturn404()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/media/i/do/not/exist.jpeg');

        $response = $client->getResponse();

        // Assert status code 404
        self::assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * Verify that controller generates a 304 response for matching ETag.
     *
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testGetShouldSend304ResponseIfETagMatches()
    {
        $reqFile = '/media/rhm_bild_rot.png';

        $client = $this->createAuthenticatedClient();
        $client->request('GET', $reqFile);

        $response = $client->getResponse();

        // Assert status code 200
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $etag = $response->headers->get('ETag');

        $client->request('GET', $reqFile, [], [], ['HTTP_IF_NONE_MATCH' => [$etag]]);
        $response = $client->getResponse();

        // Assert status code 304
        self::assertSame(Response::HTTP_NOT_MODIFIED, $response->getStatusCode(), 'HTTP status code not ok');
    }

    /**
     * Verify that controller generates a 400 response for bad parameters.
     *
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testGetShouldSend400ResponseForBadParameters()
    {
        $reqFile = '/media/rhm_bild_rot.png?scale=0';

        $client = $this->createAuthenticatedClient();
        $client->request('GET', $reqFile);

        $response = $client->getResponse();

        // Assert status code 400
        self::assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode(), 'HTTP status code not ok');

        $reqFile = '/media/rhm_bild_rot.png?scale=101';

        $client = $this->createAuthenticatedClient();
        $client->request('GET', $reqFile);

        $response = $client->getResponse();

        // Assert status code 400
        self::assertSame(Response::HTTP_BAD_REQUEST, $response->getStatusCode(), 'HTTP status code not ok');
    }

    /**
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function testItShouldCacheGeneratedFiles()
    {
        $reqFile = '/media/rhm_bild_rot.png?scale=50';

        $client = $this->createAuthenticatedClient();
        $client->request('GET', $reqFile);

        $response = $client->getResponse();

        self::assertFileExists(self::CACHE_DIR . '/50_rhm_bild_rot.png');
    }

    /**
     * @author Daniel Klier <klier@redhotmagma.de>
     *
     * @param string $fileName
     * @param string $contentType
     */
    private function performTestForExistingFile(string $fileName, string $contentType)
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/media/' . $fileName);

        $response = $client->getResponse();

        // Assert status code 200
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        // Assert correct content type
        self::assertSame($contentType, $response->headers->get('Content-Type'), 'Incorrect Content-Type');
        // Assert Access-Control-Allow-Origin header
        self::assertSame('*', $response->headers->get('Access-Control-Allow-Origin'), 'Access-Control-Allow-Origin header');
        // Verify ETag is sent
        self::assertRegExp('/[0-9A-Fa-f]+-[0-9A-Fa-f]+/', $response->headers->get('ETag'), 'Should send ETag header');
        // Assert correct file contents
        self::assertStringEqualsFile(
            self::MEDIA_DIR . '/' . $fileName,
            $response->getContent(),
            'File contents do not match'
        );
    }

    /**
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    private function clearCacheDir()
    {
        $dir = self::CACHE_DIR;

        if (is_dir($dir)) {
            $files = new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS),
                \RecursiveIteratorIterator::CHILD_FIRST
            );

            foreach ($files as $fileinfo) {
                $todo = ($fileinfo->isDir() ? 'rmdir' : 'unlink');
                $todo($fileinfo->getRealPath());
            }
        }
    }
}
