<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ChannelsControllerTest extends ApiTestCase
{
    /**
     * @var ChannelRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Channel::class);

        $sql = file_get_contents(__DIR__ . '/_data/Channel/setUp.sql');
        $this->executeSql($sql);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/channels');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, file_get_contents(__DIR__ . '/_data/Channel/list.json'));
    }

    public function testListRestrictedToClientUser()
    {
        $client = $this->createAuthenticatedClient('rhm');
        $client->request('GET', '/api/channels');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            file_get_contents(__DIR__ . '/_data/Channel/clientRestrictedList.json')
        );
    }

    public function testDetailClientAllowed()
    {
        $client = $this->createAuthenticatedClient('rhm');
        $client->request('GET', '/api/channels/2');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            file_get_contents(__DIR__ . '/_data/Channel/clientRestrictedDetail.json')
        );
    }

    public function testDetailClientForbidden()
    {
        $client = $this->createAuthenticatedClient('rhm');
        $client->request('GET', '/api/channels/3');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode(), 'HTTP status code not ok');
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient('admin');
        $client->request('GET', '/api/channels/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            '{"id":1,"identifier":"_default","settings":[],"currency":{"id":1,"iso":"eur","symbol":"\u20ac"}, "globalDiscountPercentage": null}'
        );
    }

    public function testSave()
    {
        $postdata = '{"identifier":"testchannel","currency":1}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/channels',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $channel = $this->repository->findOneByIdentifier('testchannel');

        self::assertInstanceOf(Channel::class, $channel);

        $expectedResult = json_decode($postdata);
        $receivedResult = json_decode($response->getContent());

        self::assertEquals($expectedResult->identifier, $receivedResult->identifier, 'content mismatch');
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/channels/2');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $channel = $this->repository->findOneById(2);

        self::assertEquals(null, $channel);
    }
}
