<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Doctrine\DBAL\Connection;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class OptionClassificationsControllerTest extends ApiTestCase
{
    /**
     * @var OptionclassificationRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Optionclassification::class);
    }

    protected function tearDown(): void
    {
        // clean up test data
        $conn = $this->em->getConnection();
        $conn->executeQuery('DELETE FROM optionclassification where identifier = "test_sequence_number"');
        $conn->close();
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/optionclassifications');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/optionclassifications/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testListWithOptionsForItem(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/frontendapi/items/sheep/optionclassificationswithoptions');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testSave(): void
    {
        $postdata =
            '{"parent_id":null,"is_multiselect":false,"identifier":"feet","sequencenumber":1,"hiddenInFrontend":false,"texts":[{"language":"en_GB","title":"feet","description":""},{"language":"de_DE","title":"Füße","description":""}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/optionclassifications',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $itemclassification = $this->repository->findOneByIdentifier('feet');

        self::assertInstanceOf(Optionclassification::class, $itemclassification);
    }

    public function testAutomaticSequenceNumberCreationWithoutSequenceNumber(): void
    {
        /* @var $conn Connection */

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/optionclassifications',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"identifier":"test_sequence_number"}'
        );

        // test sequence number value
        $entity = $this->repository->findOneByIdentifier('test_sequence_number');
        self::assertInstanceOf(Optionclassification::class, $entity);
        self::assertEquals(6, $entity->getSequencenumber());

        // test position in listing
        $client->request('GET', '/api/optionclassifications');
        $response = $client->getResponse();
        self::assertSame(200, $response->getStatusCode());
        $listData = json_decode($response->getContent());

        self::assertNotEmpty($listData->data[5]);
        self::assertSame('test_sequence_number', $listData->data[5]->identifier);
    }

    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/optionclassifications/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertNull($this->repository->findOneByIdentifier('feet'));
    }

    public function testSavesequencenumbers(): void
    {
        $postdata = '[{"id":1,"sequencenumber":100},{"id":2,"sequencenumber":101}]';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/optionclassifications/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $optionClassification = $this->repository->findOneById(1);

        self::assertInstanceOf(Optionclassification::class, $optionClassification);
        self::assertEquals(100, $optionClassification->getSequencenumber());
    }
}
