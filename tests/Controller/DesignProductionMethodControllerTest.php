<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodText;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DesignProductionMethodControllerTest extends ApiTestCase
{
    /**
     * @var DesignProductionMethodRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(DesignProductionMethod::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignProductionMethod/setUp.sql'));
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function tearDown(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignProductionMethod/tearDown.sql'));
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designproductionmethods');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designproductionmethods/1');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testCreate(): void
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignProductionMethod/createJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designproductionmethods',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        /** @var DesignProductionMethod $testInstance */
        $testInstance = $this->repository->findOneBy(['identifier' => 'test_create']);

        self::assertInstanceOf(
            DesignProductionMethod::class,
            $testInstance,
            'Design Production Method has not been created.'
        );
        self::assertInstanceOf(
            DesignProductionMethodText::class,
            $testInstance->getDesignProductionMethodText()[0],
            'Design Production Method Palette has not been created.'
        );
        self::assertInstanceOf(
            DesignProductionMethodColorPalette::class,
            $testInstance->getDesignProductionMethodColorPalette()[0],
            'Color Palette relation to Design Production Method has not been created.'
        );
        self::assertInstanceOf(
            DesignerProductionCalculationType::class,
            $testInstance->getDesignerProductionCalculationType()[0],
            'CalculationType relation to Design Production Method has not been created.'
        );
    }

    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', 'api/designproductionmethods/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $testFailed = false;

        $sql = file_get_contents(__DIR__ . '/_data/DesignProductionMethod/deleteCheck.sql');
        $result = $this->em->getConnection()->query($sql)->fetchAll();

        foreach ($result as $rows) {
            foreach (array_values($rows) as $data) {
                if ('0001-01-01 00:00:00' == $data) {
                    $testFailed = true;

                    break 2;
                }
            }
        }

        self::assertFalse($testFailed, 'Not all Design Production Method Relations have been marked as date_deleted.');
    }
}
