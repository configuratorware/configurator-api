<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DesignAreaControllerTest extends ApiTestCase
{
    /**
     * @var DesignAreaRepository
     */
    private $repository;

    /**
     * @var \Symfony\Component\Filesystem\Filesystem
     */
    private $fileSystem;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(DesignArea::class);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignArea/setUp.sql'));

        $this->fileSystem = new Filesystem();
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designareas');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testListFilter()
    {
        $client = $this->createAuthenticatedClient();
        $filters = [
            '?filters[designarea.item.itemtext.title]=sheep',
            '?filters[designarea.option.optionText.title]=coat',
        ];

        foreach ($filters as $filter) {
            $client->request('GET', '/api/designareas' . $filter);
            $response = $client->getResponse();

            self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

            $content = json_decode($response->getContent());
            self::assertNotEquals($content->metadata->totalcount, '0', 'There is no Result matching the search.');
        }
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designareas/1');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testCreate()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignArea/createJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designareas',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        /** @var DesignArea $testInstance */
        $testInstance = $this->repository->findOneBy(['identifier' => 'test_create']);

        self::assertInstanceOf(
            DesignArea::class,
            $testInstance,
            'Design Area has not been created.'
        );
        self::assertInstanceOf(
            DesignAreaText::class,
            $testInstance->getDesignAreaText()[0],
            'Design Area Text has not been created.'
        );
        self::assertInstanceOf(
            DesignAreaDesignProductionMethod::class,
            $testInstance->getDesignAreaDesignProductionMethod()[0],
            'Design Production Method relation to Design Area has not been created.'
        );
        self::assertInstanceOf(
            DesignAreaDesignProductionMethodPrice::class,
            $testInstance->getDesignAreaDesignProductionMethod()[0]->getDesignAreaDesignProductionMethodPrice()[0],
            'Design Production Method PriceModel from to Design Area has not been created.'
        );
    }

    public function testUpdate()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignArea/updateJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designareas',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->repository->findOneBy(['identifier' => 'front']);

        self::assertEquals(
            strpos($testInstance->getDesignAreaText()[0]->getTitle(), 'changed'),
            !false,
            'Value has not been changed'
        );
        self::assertNull(
            $testInstance->getDesignAreaDesignProductionMethod()[0],
            'Design Production Method has not been removed vom Design Production Method'
        );
    }

    public function testSaveSequenceNumber()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignArea/saveSequenceNumberJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designareas/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->repository->findOneBy(['identifier' => 'front']);

        self::assertEquals(
            100,
            $testInstance->getSequenceNumber(),
            'Sequence Number has not been updated.'
        );
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', 'api/designareas/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $testFailed = false;

        $sql = file_get_contents(__DIR__ . '/_data/DesignArea/deleteCheck.sql');
        $result = $this->em->getConnection()->query($sql)->fetchAll();

        foreach ($result as $rows) {
            foreach (array_values($rows) as $data) {
                if ('0001-01-01 00:00:00' === $data) {
                    $testFailed = true;

                    break;
                }
            }
            if (true == $testFailed) {
                break;
            }
        }

        self::assertFalse($testFailed, 'Not all Design Area Relations have been marked as date_deleted.');
    }

    /**
     * @param string $url
     * @param string $fileName
     *
     * @dataProvider maskUploadDataProvider
     */
    public function testMaskUpload(string $url, string $fileName)
    {
        $maskFile = new UploadedFile(
            __DIR__ . '/_data/DesignArea/rhm_test.svg',
            'test.svg',
            'image/svg+xml'
        );

        $client = $this->createAuthenticatedClient();
        $client->insulate(false);

        $client->request(
            'POST',
            $url,
            [],
            ['mask' => $maskFile]
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        self::assertFileExists($fileName, 'File has not been created');

        $this->fileSystem->remove(__DIR__ . '/_data/media/designer/masks');
    }

    public function maskUploadDataProvider()
    {
        return [
            [
                'api/design_areas/1/design_production_methods/1/upload_mask',
                __DIR__ . '/_data/media/designer/masks/sheep/front_screen_printing.svg',
            ],
            [
                'api/design_areas/1/upload_mask',
                __DIR__ . '/_data/media/designer/masks/sheep/front.svg',
            ],
        ];
    }

    /**
     * @param string $post
     * @param int $expectedRowCount
     *
     * @dataProvider editIsDefaultDataProvider
     */
    public function testEditIsDefault(string $post, int $expectedRowCount)
    {
        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designareas',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $post
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $dbConnection = $this->em->getConnection();
        $result = $dbConnection->query('SELECT * FROM design_area_design_production_method WHERE design_area_id = 1 AND is_default = TRUE')->fetchAll();
        $dbConnection->close();

        self::assertEquals($expectedRowCount, count($result));
    }

    public function editIsDefaultDataProvider()
    {
        return [
            [
                '{"id":1,"item":{"id":1},"identifier":"front_center","height":1,"width":1,"texts":[],"designProductionMethods":[{"id":1,"isDefault":true,"calculationTypes":[]},{"id":2,"isDefault":false,"calculationTypes":[]}]}',
                1,
            ],
            [
                '{"id":1,"item":{"id":1},"identifier":"front_center","height":1,"width":1,"texts":[],"designProductionMethods":[{"id":1,"isDefault":true,"calculationTypes":[]},{"id":2,"isDefault":true,"calculationTypes":[]}]}',
                1,
            ],
            [
                '{"id":1,"item":{"id":1},"identifier":"front_center","height":1,"width":1,"texts":[],"designProductionMethods":[{"id":1,"isDefault":false,"calculationTypes":[]},{"id":2,"isDefault":false,"calculationTypes":[]}]}',
                0,
            ],
        ];
    }
}
