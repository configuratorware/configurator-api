<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Doctrine\DBAL\DBALException;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use redhotmagma\SymfonyTestUtils\Fixture\TestHttpResponse;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion\SkipDatabaseVersion;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion\SkipDatabaseVersionTrait;

class ItemsControllerTest extends ApiTestCase
{
    use SkipDatabaseVersionTrait;

    protected $repository;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    protected function setUp(): void
    {
        parent::setUp();

        $this->fileSystem = new Filesystem();
        $this->repository = $this->em->getRepository(Item::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/items');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testListWithMultipleQueryParameters()
    {
        $this->executeSql('INSERT INTO item(id, identifier, accumulate_amounts, date_created, overwrite_component_order)
                                VALUES (2, \'demo_hoodie\', 1, \'2019-06-07 14:53:02\', 1),
                                       (3, \'demo_pen\', 1, \'2019-06-07 14:53:02\', 0)');

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/items?query=hood,pen');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @SkipDatabaseVersion("mariadb-10.7.3")
     *
     * @param string $configurationMode
     * @dataProvider providerShouldListByConfigurationMode
     *
     * @throws DBALException
     */
    public function testShouldListByConfigurationMode(string $configurationMode): void
    {
        if ('designer' === $configurationMode) {
            $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_hoodie.sql'));
        }

        $client = $this->createAuthenticatedClient();
        $client->request('GET', "/api/items/$configurationMode");
        $response = $client->getResponse();

        Fixtures::assertResponseEquals(
            TestHttpResponse::fromFileContent(file_get_contents(__DIR__ . "/_fixtures/ItemsControllerTest/testShouldListByConfigurationMode.$configurationMode.expected.json")),
            $response
        );
    }

    public function providerShouldListByConfigurationMode(): array
    {
        return [
            ['creator'],
            ['designer'],
        ];
    }

    public function testShouldListInspiration(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_hoodie.sql'));
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/softshell_creator_2d.sql'));
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Inspiration/setUp.sql'));

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/items/inspiration');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testItemSearchShouldFindByChild(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/setUp.sql'));

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/items?query=blue');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/items/1');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testSave(): void
    {
        $client = $this->createAuthenticatedClient();

        $this->resetAutoIncrementAllTables();

        $client->request('POST', '/api/items', [], [], ['CONTENT_TYPE' => 'application/json'],
            file_get_contents(__DIR__ . '/_fixtures/ItemsControllerTest/testSave.given.json')
        );

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    /**
     * @depends testSave
     */
    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/items/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertNull($this->repository->find(1));
    }

    public function testGetconfigurableitemsbyparent(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/frontendapi/items/getconfigurableitemsbyparent/');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $items = \json_decode($response->getContent());

        self::assertEquals('sheep', $items[0]->identifier);
    }

    public function testItemGroupingDetail(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/setUp.sql'));

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/items/5005');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testItemGroupingSave(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/setUp.sql'));

        $postData =
            '{"id":5005,"identifier":"hoodie","settings":{"visualizationCreator":"3d","visualizationDesigner":"3d"},"deactivated":false,"translated_title":"{hoodie}","translated_abstract":"","translated_description":"","texts":[],"attributes":[],"price":null,"prices":[],"stock":[],"itemclassifications":[],"children":[{"id":5006,"identifier":"hoodie_red_s","groups":[{"id":"1","identifier":"color","translation":"Color","groupEntry":{"id":"1","identifier":"red","translation":"Red","translations":null}},{"id":"5005","identifier":"shape","groupEntry":{"id":"6","identifier":"round"}}]}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/items',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @throws DBALException
     */
    public function testDesignerFallback2DCheckSuccess(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/designerFallbackSetUp.sql'));

        $this->dump2DFiles('demo_hoodie', 'images/item/view/{parentIdentifier}/{view}/{childIdentifier}.jpg');

        $client = self::createClient();
        $client->request('GET', '/frontendapi/items/getconfigurableitemsbyparent/demo_hoodie');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);

        $this->fileSystem->remove([__DIR__ . '/_data/media/images/item/view/demo_hoodie']);
    }

    /**
     * @throws DBALException
     */
    public function testDesignerFallback3DCheckSuccess(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/designerFallbackSetUp.sql'));

        $this->dump3DFiles('demo_pen', 'content/3D/{parentIdentifier}/materials/{childIdentifier}');

        $client = self::createClient();
        $client->request('GET', '/frontendapi/items/getconfigurableitemsbyparent/demo_pen');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);

        $this->fileSystem->remove([__DIR__ . '/_data/media/content/3D']);
    }

    /**
     * There is no call to get no-child articles separate, therefore the list of items is compared.
     *
     * @throws DBALException
     */
    public function testDesignerFallbackLayerCheckSuccess(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/designerFallbackSetUp.sql'));

        $client = self::createClient();
        $client->request('GET', '/frontendapi/items/getconfigurableitemsbyparent');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not found');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testShouldGetFrontendItemList(): void
    {
        $client = self::createClient();
        $client->request('GET', '/frontendapi/items');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not found');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testShouldGetFilteredFrontendItemList(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/frontendListSetUp.sql'));

        $client = self::createClient();
        $client->request('GET', '/frontendapi/items?item-classifications=[1,1002]');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not found');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/frontendListTearDown.sql'));
    }

    public function testShouldGetPaginatedFrontendItemList(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/frontendListSetUp.sql'));

        $client = self::createClient();
        $client->request('GET', '/frontendapi/items?offset=1&limit=2');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not found');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/frontendListTearDown.sql'));
    }

    private function dump3DFiles(string $parentIdentifier, string $pathPartTpl): void
    {
        $this->fileSystem->dumpFile(__DIR__ . "/_data/media/content/3D/$parentIdentifier/model.obj",
            'this is a test file');
        $this->fileSystem->dumpFile(__DIR__ . "/_data/media/content/3D/$parentIdentifier/cameraviews.dae",
            'this is a test file');

        $textures = ['texture_1.jpg', 'texture_2.jpg', 'texture_3.jpg'];

        $sql = "SELECT a.identifier AS child, b.identifier AS parent,  c.identifier as view FROM item a JOIN item b ON a.parent_id = b.id JOIN design_view AS c ON a.parent_id = c.item_id WHERE b.identifier = '$parentIdentifier';";
        $dbConnection = $this->em->getConnection();
        $items = $dbConnection->fetchAllAssociative($sql);

        foreach ($items as $item) {
            $pathPartTemp = str_replace('{parentIdentifier}', $item['parent'], $pathPartTpl);
            $pathPartTemp = str_replace('{childIdentifier}', $item['child'], $pathPartTemp);

            $materials = '';
            foreach ($textures as $texture) {
                $materials .= '    ' . uniqid('map_') . ' ' . $texture . PHP_EOL;
                $this->fileSystem->dumpFile(__DIR__ . "/_data/media/$pathPartTemp/$texture", 'this is a test file');
            }

            $this->fileSystem->dumpFile(__DIR__ . "/_data/media/$pathPartTemp/materials.mtl", $materials);
        }
    }

    private function dump2DFiles(string $parentIdentifier, string $pathPartTpl): void
    {
        $sql = "SELECT a.identifier AS child, b.identifier AS parent,  c.identifier as view FROM item a JOIN item b ON a.parent_id = b.id JOIN design_view AS c ON a.parent_id = c.item_id WHERE b.identifier = '$parentIdentifier';";
        $dbConnection = $this->em->getConnection();
        $items = $dbConnection->fetchAllAssociative($sql);

        foreach ($items as $item) {
            $pathPartTemp = str_replace('{parentIdentifier}', $item['parent'], $pathPartTpl);
            $pathPartTemp = str_replace('{view}', $item['view'], $pathPartTemp);
            $pathPartTemp = str_replace('{childIdentifier}', $item['child'], $pathPartTemp);
            $this->fileSystem->dumpFile(__DIR__ . "/_data/media/$pathPartTemp", 'this is a test file');
        }
    }
}
