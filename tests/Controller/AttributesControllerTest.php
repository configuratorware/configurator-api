<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class AttributesControllerTest extends ApiTestCase
{
    /**
     * @var AttributeRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Attribute::class);
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/attributes');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"metadata":{"totalcount":"1"},"data":[
        {"id":1,"identifier":"tags","writeProtected":false,"translated_title":"tags","externalid":null,
        "attributedatatype":"string","texts":[{"id":1,"language":"en_GB","title":"tags"},{"id":2,"language":"de_DE",
        "title":"Tags"}]}]}');
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/attributes/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"id":1,"identifier":"tags","writeProtected":false,
        "translated_title":"tags","externalid":null,"attributedatatype":"string",
        "texts":[{"id":1,"language":"en_GB","title":"tags"},{"id":2,"language":"de_DE","title":"Tags"}]}');
    }

    public function testSave(): void
    {
        $postdata = '{"identifier":"testattribute","externalid":null,"writeProtected":true,
        "attributedatatype":"boolean","texts":[{"language":"en_GB","title":"test"},
        {"language":"de_DE","title":"Test"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/attributes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $attribute = $this->repository->findOneByIdentifier('testattribute');

        self::assertInstanceOf(Attribute::class, $attribute);

        $expectedResult = json_decode($postdata);
        $receivedResult = json_decode($response->getContent());

        self::assertEquals($expectedResult->identifier, $receivedResult->identifier, 'content mismatch');
        self::assertEquals($expectedResult->writeProtected, $receivedResult->writeProtected, 'content mismatch');
    }

    public function testSaveWriteProtectedFalse(): void
    {
        $postdata = '{"identifier":"testattribute","externalid":null,"writeProtected":false,
        "attributedatatype":"boolean","texts":[{"language":"en_GB","title":"test"},
        {"language":"de_DE","title":"Test"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/attributes',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $attribute = $this->repository->findOneByIdentifier('testattribute');

        self::assertInstanceOf(Attribute::class, $attribute);

        $expectedResult = json_decode($postdata);
        $receivedResult = json_decode($response->getContent());

        self::assertEquals($expectedResult->identifier, $receivedResult->identifier, 'content mismatch');
        self::assertEquals(false, $receivedResult->writeProtected, 'content mismatch');
    }

    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/attributes/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertNull($this->repository->find(1));
    }
}
