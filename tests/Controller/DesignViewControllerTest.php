<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use org\bovigo\vfs\vfsStream;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewText;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * @MockPaths
 */
class DesignViewControllerTest extends ApiTestCase
{
    use VfsTestTrait;

    /**
     * @var \Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @var \Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewDesignAreaRepository
     */
    private $relationsRepository;

    /**
     * @var
     */
    private $connection;

    /**
     * @var \org\bovigo\vfs\vfsStreamDirectory
     */
    private $vfsRoot;

    protected function setUp(): void
    {
        parent::setUp();
        $this->vfsRoot = vfsStream::setup();

        $this->designViewRepository = $this->em->getRepository(DesignView::class);
        $this->relationsRepository = $this->em->getRepository(DesignViewDesignArea::class);
        $this->connection = $this->em->getConnection();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignView/setUp.sql'));
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designviews');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        $this->compareResponseContent(
            $response,
            file_get_contents(__DIR__ . '/_data/DesignView/response/list.json')
        );
    }

    public function testListFilter()
    {
        $client = $this->createAuthenticatedClient();
        $filters = [
            '?filters[designview.item.itemtext.title]=sheep',
            '?filters[designview.option.optionText.title]=coat',
        ];

        foreach ($filters as $filter) {
            $client->request('GET', '/api/designviews' . $filter);
            $response = $client->getResponse();

            self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

            $content = json_decode($response->getContent());

            self::assertNotEquals(
                $content->metadata->totalcount,
                '0',
                'There is no Result matching the search: ' . $filter
            );
        }
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designviews/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            file_get_contents(__DIR__ . '/_data/DesignView/response/detail.json')
        );
    }

    public function testCreate()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignView/post/createJson.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designviews',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->designViewRepository->findOneBy(['identifier' => 'test_create']);

        self::assertInstanceOf(
            DesignView::class,
            $testInstance,
            'Design View has not been created.'
        );
        self::assertInstanceOf(
            DesignViewText::class,
            $testInstance->getDesignViewText()[0],
            'Design View Text has not been created.'
        );
        self::assertInstanceOf(
            DesignViewDesignArea::class,
            $testInstance->getDesignViewDesignArea()[0],
            'Design Area relation to Design View has not been created.'
        );
    }

    public function testUpdate()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignView/post/updateJson.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designviews',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->designViewRepository->findOneBy(['identifier' => 'front']);

        self::assertEquals(
            !false,
            strpos($testInstance->getDesignViewText()->first()->getTitle(), 'changed'),
            'Value has not been changed'
        );
        self::assertFalse(
            $testInstance->getDesignViewDesignArea()->first(),
            'Design Area has not been removed vom Design Production Method'
        );
    }

    public function testSaveSequenceNumber()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/DesignView/post/saveSequenceNumberJson.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designviews/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->designViewRepository->findOneBy(['identifier' => 'front']);

        self::assertEquals(
            100,
            $testInstance->getSequenceNumber(),
            'Sequence Number has not been updated.'
        );
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', 'api/designviews/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $testFailed = false;

        $sql = file_get_contents(__DIR__ . '/_data/DesignView/deleteCheck.sql');
        $result = $this->em->getConnection()->query($sql)->fetchAll();

        foreach ($result as $rows) {
            foreach (array_values($rows) as $data) {
                if ('0001-01-01 00:00:00' === $data) {
                    $testFailed = true;

                    break;
                }
            }
            if (true == $testFailed) {
                break;
            }
        }

        self::assertFalse($testFailed, 'Not all Design View Relations have been marked as date_deleted.');
    }

    public function testSetDefaultDesingViewForArea()
    {
        /* check for no defaults set */
        $relations = $this->relationsRepository->findBy(['is_default' => true]);
        self::assertEmpty($relations); // no defaults set yet

        /* set first default */
        $postData = file_get_contents(__DIR__ . '/_data/DesignView/post/updateDefaultDesignView_1.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designviews',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());

        $relations = $this->relationsRepository->findBy(['is_default' => true]);
        self::assertCount(1, $relations);
        self::assertSame(1, (int)$relations[0]->getDesignArea()->getId()); // area one
        self::assertSame(1, (int)$relations[0]->getDesignView()->getId()); // view one is default
    }

    public function testOverrideDefaultDesignViewForArea()
    {
        // set a default design area for a view
        $this->executeSql('
            UPDATE design_view_design_area SET is_default=1 WHERE design_view_id=1 AND design_area_id=1
        ');

        // assert base data is correct
        $relations = $this->relationsRepository->findBy(['is_default' => true]);
        self::assertSame(1, (int)$relations[0]->getDesignView()->getId()); // view two is default
        self::assertSame(1, (int)$relations[0]->getDesignArea()->getId()); // area one

        $postData = file_get_contents(__DIR__ . '/_data/DesignView/post/updateDefaultDesignView_2.json');

        $this->relationsRepository->clear();

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designviews',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());

        // check if default is changed
        $relations = $this->relationsRepository->findBy(['is_default' => true]);
        self::assertCount(1, $relations);
        self::assertSame(1, (int)$relations[0]->getDesignArea()->getId()); // area one
        self::assertSame(2, (int)$relations[0]->getDesignView()->getId()); // view two is default
    }

    public function testRemoveDefaultDesignViewForArea()
    {
        // set a default design area for a view
        $this->executeSql('
            UPDATE design_view_design_area SET is_default=1 WHERE design_view_id=2 AND design_area_id=1
        ');

        // assert base data is correct
        $relations = $this->relationsRepository->findBy(['is_default' => true]);
        self::assertSame(1, (int)$relations[0]->getDesignArea()->getId()); // area one
        self::assertSame(2, (int)$relations[0]->getDesignView()->getId()); // view two is default

        $postData = file_get_contents(__DIR__ . '/_data/DesignView/post/updateDefaultDesignView_remove.json');
        $this->relationsRepository->clear();

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designviews',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());

        $relations = $this->relationsRepository->findBy(['is_default' => true]);

        // check if default is removed
        self::assertEmpty($relations); // no defaults found
    }
}
