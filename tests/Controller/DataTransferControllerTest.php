<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @BackupDatabase
 */
class DataTransferControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/DataTransfer/setUp.sql'));
    }

    /**
     * @dataProvider providerShouldCopy
     */
    public function testShouldCopyAllData(array $items, array $checks)
    {
        list($sourceItemId, $targetItemId) = $items;

        $post = '{"sourceItemId": ' . $sourceItemId . ', "targetItemId": ' . $targetItemId . ', "designAreas": true, "visualizationData": true, "itemStatus": true}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            'api/data_transfer/design_data',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $post
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        foreach ($checks as $check) {
            $compareField = $check['compareField'];
            $inStatementTarget = str_replace('§itemId', $targetItemId, $check['inStatement']);
            $inStatementSource = str_replace('§itemId', $sourceItemId, $check['inStatement']);

            // check if all old relations got deleted
            $existing = $this->get($check['table'], $check['whereIdentifier'], $inStatementTarget, 'existing');
            foreach ($existing as $item) {
                self::assertNotEquals('0001-01-01 00:00:00', $item['date_deleted']);
            }

            // check if new relations got added
            $target = $this->get($check['table'], $check['whereIdentifier'], $inStatementTarget, 'added');
            $source = $this->get($check['table'], $check['whereIdentifier'], $inStatementSource);

            foreach ($source as $item) {
                self::assertNotFalse(
                    array_search($item[$compareField], array_column($target, $compareField)),
                    $item[$compareField] . ' was not found in target'
                );
            }

            self::assertEquals(count($source), count($target));
        }

        // check if the status has been changed by source Item
        $statuses = $this->get('item_configuration_mode_item_status', 'item_id', $targetItemId);
        foreach ($statuses as $status) {
            self::assertEquals(2, $status['item_status_id']);
        }
    }

    public function providerShouldCopy(): array
    {
        return [
            [
                [15, 2],
                [
                    [
                        'table' => 'design_area',
                        'whereIdentifier' => 'item_id',
                        'inStatement' => '§itemId',
                        'compareField' => 'identifier',
                    ],
                    [
                        'table' => 'design_area_text',
                        'whereIdentifier' => 'design_area_id',
                        'inStatement' => '(SELECT id FROM design_area WHERE item_id = §itemId)',
                        'compareField' => 'title',
                    ],
                    [
                        'table' => 'design_area_design_production_method',
                        'whereIdentifier' => 'design_area_id',
                        'inStatement' => '(SELECT id FROM design_area WHERE item_id = §itemId)',
                        'compareField' => 'width',
                    ],
                    [
                        'table' => 'design_area_design_production_method_price',
                        'whereIdentifier' => 'design_area_design_production_method_id',
                        'inStatement' => 'SELECT id FROM design_area_design_production_method WHERE design_area_id IN(SELECT id FROM design_area WHERE item_id = §itemId)',
                        'compareField' => 'price',
                    ],
                    [
                        'table' => 'design_view',
                        'whereIdentifier' => 'item_id',
                        'inStatement' => '§itemId',
                        'compareField' => 'identifier',
                    ],
                    [
                        'table' => 'design_view_text',
                        'whereIdentifier' => 'design_view_id',
                        'inStatement' => '(SELECT id FROM design_view WHERE item_id = §itemId)',
                        'compareField' => 'title',
                    ],
                    [
                        'table' => 'design_view_design_area',
                        'whereIdentifier' => 'design_view_id',
                        'inStatement' => '(SELECT id FROM design_view WHERE item_id = §itemId)',
                        'compareField' => 'position',
                    ],
                ],
            ],
            [
                [25, 20],
                [
                    [
                        'table' => 'design_area',
                        'whereIdentifier' => 'item_id',
                        'inStatement' => '§itemId',
                        'compareField' => 'identifier',
                    ],
                    [
                        'table' => 'design_area_text',
                        'whereIdentifier' => 'design_area_id',
                        'inStatement' => '(SELECT id FROM design_area WHERE item_id = §itemId)',
                        'compareField' => 'title',
                    ],
                    [
                        'table' => 'design_area_design_production_method',
                        'whereIdentifier' => 'design_area_id',
                        'inStatement' => '(SELECT id FROM design_area WHERE item_id = §itemId)',
                        'compareField' => 'width',
                    ],
                    [
                        'table' => 'design_area_design_production_method_price',
                        'whereIdentifier' => 'design_area_design_production_method_id',
                        'inStatement' => 'SELECT id FROM design_area_design_production_method WHERE design_area_id IN(SELECT id FROM design_area WHERE item_id = §itemId)',
                        'compareField' => 'price',
                    ],
                    [
                        'table' => 'creator_view',
                        'whereIdentifier' => 'item_id',
                        'inStatement' => '§itemId',
                        'compareField' => 'identifier',
                    ],
                    [
                        'table' => 'creator_view_text',
                        'whereIdentifier' => 'creator_view_id',
                        'inStatement' => '(SELECT id FROM creator_view WHERE item_id = §itemId)',
                        'compareField' => 'title',
                    ],
                    [
                        'table' => 'creator_view_design_area',
                        'whereIdentifier' => 'creator_view_id',
                        'inStatement' => '(SELECT id FROM creator_view WHERE item_id = §itemId)',
                        'compareField' => 'position',
                    ],
                ],
            ],
        ];
    }

    private function get(string $table, string $whereIdentifier, string $inStatement, string $mode = '')
    {
        $sql = 'SELECT * FROM ' . $table . ' WHERE ' . $whereIdentifier . ' IN(' . $inStatement . ')';

        if ('existing' === $mode || 'added' === $mode) {
            $sql .= ' AND date_created < "' . (new \DateTimeImmutable('-30 minutes'))->format('Y-m-d H:i:s') . '"';
        }

        if ('added' === $mode) {
            $sql = str_replace('date_created < ', 'date_created > ', $sql);
        }

        return $this->em->getConnection()->executeQuery($sql)->fetchAllAssociative();
    }
}
