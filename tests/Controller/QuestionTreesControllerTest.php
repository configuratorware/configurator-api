<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Repository\QuestionTreeRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

/**
 * @BackupDatabase
 */
class QuestionTreesControllerTest extends ApiTestCase
{
    private QuestionTreeRepository $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(QuestionTree::class);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/QuestionTree/question_tree_data.sql'));
    }

    public function testShouldShowFrontendQuestionTree(): void
    {
        $client = self::createClient();
        $client->request('GET', '/frontendapi/questiontrees/simple_tree', ['_language' => 'de_DE']);

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testShouldShowQuestionTreeDetails(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/questiontrees/11', ['_language' => 'de_DE']);

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testShouldListQuestionTrees(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/questiontrees');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testShouldCreateNewQuestionTree(): void
    {
        $postdata = file_get_contents(__DIR__ . '/_data/QuestionTree/create.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/questiontrees',
            ['_language' => 'de_DE'],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testShouldUpdateQuestionTree(): void
    {
        $postdata = file_get_contents(__DIR__ . '/_data/QuestionTree/update.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/questiontrees',
            ['_language' => 'de_DE'],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testShouldDeleteQuestionTree(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/questiontrees/10%2C11');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $deletedEntity = $this->repository->find(10);
        $secondDeletedEntity = $this->repository->find(10);

        self::assertNull($deletedEntity, 'QuestionTree has not been marked as deleted.');
        self::assertNull($secondDeletedEntity, 'comma-separated QuestionTree has not been marked as deleted.');
    }

    public function testShouldUploadDataFile(): void
    {
        $workingDir = __DIR__ . '/_data/QuestionTree/';
        $fileName = 'upload.json';
        $prefix = 'cp_';

        copy($workingDir . $fileName, $workingDir . $prefix . $fileName);

        $dataFile = new UploadedFile(
            $workingDir . $prefix . $fileName,
            $prefix . $fileName,
            'application/json',
            null
        );

        $client = $this->createAuthenticatedClient();
        $client->insulate(false);

        $client->request(
            'POST',
            '/api/questiontrees/1/data/1/upload',
            [],
            ['file' => $dataFile]
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
    }
}
