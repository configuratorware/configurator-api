<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\DataCollector\MessageDataCollector;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ReceiveOffersControllerTest extends ApiTestCase
{
    public function testShouldSendEmail()
    {
        $client = static::createClient();
        $client->enableProfiler();

        $json = file_get_contents(__DIR__ . '/_data/ReceiveOffers/request.json');

        $client->request(Request::METHOD_POST, '/frontendapi/receiveoffers/request', [], [], [], $json);

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        /** @var MessageDataCollector $messageDataCollector */
        $messageDataCollector = $client->getProfile()->getCollector('mailer');
        self::assertEquals(1, count($messageDataCollector->getEvents()->getMessages()), 'Email has not been sent.');
    }
}
