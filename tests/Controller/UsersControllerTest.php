<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class UsersControllerTest extends ApiTestCase
{
    protected $connection;

    protected $userRepository;

    public function setUp(): void
    {
        parent::setUp();

        $this->connection = $this->em->getConnection();
        $this->userRepository = $this->em->getRepository(User::class);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/UsersController/setUp.sql'));
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/users');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $expected = $this->loadResponseFixture();
        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/users/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"id":1,"username":"admin","firstname":null,"lastname":null,"email": "admin@redhotmagma.de","password":null,"active":true,"roles":[{"id":1,"name":"admin","role":null,"credentials":null}]}');
    }

    public function testSave(): void
    {
        $postdata = '{"username":"testsave","firstname":"David","lastname":"Lightman","email":"test@redhotmagma.de","password":"magma", "roles":[{"id":1,"name":"admin"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/users',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $user = $this->userRepository->findUserByUsername('testsave');

        self::assertInstanceOf(User::class, $user);
    }

    public function testDelete(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/UsersController/setUp_for_delete.sql'));

        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/users/235');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertNull($this->userRepository->findUserByUsername('username_for_delete1'));
        self::assertFalse($this->connection->fetchOne('SELECT id FROM `user_role` WHERE `date_deleted` = "0001-01-01 00:00:00" AND `user_id` = 235'), 'user_role not deleted');
        self::assertFalse($this->connection->fetchOne('SELECT id FROM `client_user` WHERE `date_deleted` = "0001-01-01 00:00:00" AND `user_id` = 235'), 'client_user not deleted');
    }

    public function testUserPasswordIsChanged(): void
    {
        $passwordHasher = $this->getContainer()->get(UserPasswordHasherInterface::class);

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/users/change_password',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            '{"oldPassword":"admin","newPassword":"mypass","newPasswordRepeat":"mypass"}'
        );

        $user = $this->userRepository->find(1);
        self::assertTrue($passwordHasher->isPasswordValid($user, 'mypass'));
    }

    public function testApiUserList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/apiusers');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"metadata":{"totalcount":"1"},"data":[{"id":234,"username":"apikey_user","email":null,"active":true,"api_key":"testkey"}]}');
    }

    public function testApiUserDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/apiusers/234');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"id":234,"username":"apikey_user", "email": null,"active":true,"api_key":"testkey"}');
    }

    /**
     * @dataProvider providerApiUserSave
     */
    public function testApiUserSave(string $apiKey): void
    {
        $payload = [
            'username' => 'test_key_user_save',
            'firstname' => 'Gordon',
            'lastname' => 'Freeman',
            'email' => 'gfreeman@blackmesa.com',
            'password' => 'crowbar',
            'api_key' => $apiKey,
        ];

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/apiusers', [], [], ['CONTENT_TYPE' => 'application/json'], json_encode($payload));

        self::assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        /** @var User $user */
        $user = $this->userRepository->findUserByUsername('test_key_user_save');

        self::assertInstanceOf(User::class, $user);
        self::assertNotEquals('abcdefgh', $user->getApiKey()); // the key from the post should not have been saved in the database
        self::assertNotEmpty($user->getApiKey()); // a new key should have been created, we cannot compare a specific string, since it is random
        self::assertSame('ROLE_CONNECTOR', $user->getUserRole()->first()->getRole()->getRole());
    }

    public function providerApiUserSave(): \Generator
    {
        yield 'api key populated by caller' => ['abcdefgh'];
        yield 'api key empty' => [''];
    }

    public function testApiuserDelete()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/UsersController/setUp_for_delete.sql'));

        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/apiusers/236');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $user = $this->userRepository->findUserByUsername('username_for_delete2');

        self::assertEquals(null, $user);
    }

    public function testApiuserRefreshKey()
    {
        // assert "testkey" is present
        $user = $this->userRepository->findUserByUsername('apikey_user');
        self::assertEquals('testkey', $user->getApiKey());

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/apiusers/234/refreshkey');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode());

        $this->userRepository->clear();

        // assert "testkey" is changed, so not present anymore
        $user = $this->userRepository->findUserByUsername('apikey_user');
        self::assertNotEquals('testkey', $user->getApiKey());
    }
}
