INSERT IGNORE INTO configuration
(
    externaluser_id,
    code,
    name,
    selectedoptions,
    designdata,
    customdata,
    date_created,
    date_updated,
    date_deleted,
    user_created_id,
    user_updated_id,
    user_deleted_id,
    configurationtype_id,
    item_id,
    finder_result
)
VALUES
(
    NULL,
    'luke_skywalker',
    'Luke Skywalker',
    '{"coat":[{"identifier":"coat_nature"}],"face":[{"identifier":"face_nature","inputText":"beautiful face"}],"legs":[{"identifier":"legs_nature"}],"eyes":[{"identifier":"eyes_yellow"}],"ear_eyelid":[{"identifier":"ear_eyelid_nature"}]}',
    NULL,
    '{"parentCode":"darth_vader"}',
    NULL,
    NULL,
    '0001-01-01 00:00:00',
    0,
    NULL,
    NULL,
    1,
    1,
    '{"articlenumbers":[23,42,1337],"other_finder_data":[{"identifier":"finderResult"}]}'
);


INSERT IGNORE INTO configuration
(
    externaluser_id,
    code,
    name,
    selectedoptions,
    designdata,
    customdata,
    date_created,
    date_updated,
    date_deleted,
    user_created_id,
    user_updated_id,
    user_deleted_id,
    configurationtype_id,
    item_id
)
VALUES
(
    NULL,
    'my_design',
    'my_design',
    NULL,
    '{"front":{"canvasData":{"objects":[{"type":"Image","x":392.8180473372781,"y":391.42011834319527,"rotation":0,"scaling":0.2790752636987475,"src":"http://localhost:10030/images/imagegallery/c4ca4238a0b923820dcc509a6f75849b.jpeg","metric":{"x":33,"y":33,"width":47,"height":47}},{"type":"Text","x":521.4275147928995,"y":592.7218934911242,"rotation":339.01817495342334,"scaling":7.879963906869409,"content":"<div style=\\"font-family: Arial, Helvetica, sans-serif;\\"><span style=\\"font-size: 16px; color: rgb(247, 52, 41); font-weight: normal; font-style: italic; font-family: &quot;Comic Sans MS&quot;, cursive, sans-serif;\\" data-color=\\"red\\">My Text</span></div>","style":{"colors":[{"identifier":"red","value":"rgb(247, 52, 41)"}],"fontSize":"16px","fontFamily":"\\"Comic Sans MS\\", cursive, sans-serif","fontWeight":"normal","fontStyle":"italic"},"metric":{"x":44,"y":50,"width":42,"height":12}}],"size":{"width":945,"height":945,"dpi":300}},"designProductionMethodIdentifier":"print"},"sleeve":{"canvasData":null,"designProductionMethodIdentifier":"print"}}',
    '{"parentCode":"darth_vader"}',
    NOW(),
    NULL,
    '0001-01-01 00:00:00',
    0,
    NULL,
    NULL,
    2,
    5005
);