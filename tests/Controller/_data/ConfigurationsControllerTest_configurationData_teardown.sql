DELETE
FROM design_area
WHERE id IN (501, 502);
DELETE
FROM `user`
WHERE id = 555;
DELETE
FROM `user_role`
WHERE id = 555;
DELETE
FROM `client`
WHERE id = 555;
DELETE
FROM `client_user`
WHERE id = 555;
DELETE
FROM `configuration`
WHERE id IN (555, 556);

DELETE
FROM role_credential
WHERE role_id = (SELECT id FROM `role` WHERE role = 'ROLE_CLIENT')
  AND credential_id = (SELECT id FROM `credential` WHERE area = 'configurations');