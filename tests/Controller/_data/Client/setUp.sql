INSERT INTO `user`(id, externalid, username, firstname, lastname, email, is_active, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, password)
VALUES
       (3, NULL, 'no_client', NULL, NULL, 'johndoe@redhotmagma.de', 1, '2019-02-11 14:13:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, '$2y$13$rIzIMoKMC72bY0llDmG/5uFSV6MI602c3bAdjrddzwhjWr6AOh/rq');

INSERT INTO user_role(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, user_id, role_id)
VALUES
       (2, '2019-02-11 14:15:43', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT id FROM user WHERE username = 'rhm'), 2),
       (3, '2019-02-11 14:15:43', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 2);

INSERT INTO client(id, identifier, email, theme, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id,
                   contact_name, contact_email, contact_phone, contact_city, contact_post_code, contact_street, call_to_action, pdf_header_html, pdf_footer_html, pdf_header_text, pdf_footer_text, send_offer_request_mail_to_customer)
VALUES
   (2, 'rhm', NULL, NULL, '2019-02-11 14:13:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 'RHM', '+49 711 52088260', 'info@redhotmagma.de', 'Stuttgart', 'Badergasse 8', '70372', 'addToCart', 'RHM <br> +49 711 52088260', 'Badergasse 8, 70372 Stuttgart', 'RHM <br> +49 711 52088260', 'Badergasse 8, 70372 Stuttgart', 1),
   (3, 'someone_else', NULL, NULL, '2019-02-11 14:13:49', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 'Some One Else', 'someone@el.se', '+55555555', 'SomeCity', '123456', 'Street', null, 'Some One Else <br> +55555555', 'Street, 123456 SomeCity', 'Some One Else <br> +55555555', 'Street, 123456 SomeCity', 0);

INSERT INTO client_user(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, client_id, user_id)
VALUES(1, '2019-02-11 14:14:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, (SELECT id FROM user WHERE username = 'rhm'));

INSERT INTO `channel` (`id`, `identifier`, `settings`, `date_created`, `date_deleted`, `currency_id`, `global_discount_percentage`)
VALUES (2, 'rhm_de_eur', '{}', '2019-08-30 10:51:12', '0001-01-01 00:00:00', 1, 20);

INSERT INTO `client_channel` (`date_created`, `date_deleted`, `client_id`, `channel_id`)
VALUES ('2019-08-30 10:51:47', '0001-01-01 00:00:00', 2, 2);

DELETE FROM `client_text`;
