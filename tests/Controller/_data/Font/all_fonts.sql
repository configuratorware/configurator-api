INSERT INTO font
(name, active, is_default, sequence_number, date_created)
VALUES
('Font 1',  1, NULL, 99, NOW()),
('Font 2',  1, NULL, 99, NOW()),
('Font 3',  1, NULL, 99, NOW()),
('Font 4',  1, NULL, 99, NOW()),
('Font 5',  1, NULL, 99, NOW()),
('Font 6',  1, NULL, 99, NOW()),
('Font 7',  1, NULL, 99, NOW()),
('Font 8',  1, NULL, 99, NOW()),
('Font 9',  1, NULL, 99, NOW()),
('Font 10', 1, NULL, 99, NOW());
