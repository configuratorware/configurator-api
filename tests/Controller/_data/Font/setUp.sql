INSERT INTO `font`
(`id`, `name`, `active`, `is_default`, `file_name_regular`, `file_name_bold`, `file_name_italic`, `file_name_bold_italic`, `sequence_number`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`)
VALUES
(1, 'OpenSans',  1, NULL, 'OpenSans-Regular.ttf','OpenSans-Bold.ttf',   'OpenSans-Italic.ttf',   'OpenSans-Regular.ttf', 2, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL),
(2, 'Times',     1, 1,    'Times-Regular.ttf',   'Times-Bold.ttf',      'Times-Italic.ttf',      'Times-Regular.ttf',    1, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL),
(3, 'ComicSans', 1, NULL, 'Comic-Sans.ttf',      'Comic-Sans-Bold.ttf', 'Comic-Sans-Italic.ttf', 'Comic-Sans-BI.ttf',    4, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL),
(4, 'Helvetica', 1, NULL, 'Helvetica.ttf',       'Helvetica-Bold.ttf',  'Helvetica-Italic.ttf',  'Helvetica-BI.ttf',     3, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL),
(5, 'Arial',     0, NULL, 'Arial.ttf',           'Arial-Bold.ttf',      'Arial-Italic.ttf',      'Arial-BI.ttf',         4, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL);
