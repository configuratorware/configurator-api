INSERT INTO itemgroupentry(id, identifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES
    (1, 'red', 1, '2019-01-30 15:30:28', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
    (2, 'blue', 2, '2019-01-30 15:30:36', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
    (3, 's', 3, '2019-01-30 15:30:56', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
    (4, 'm', 4, '2019-01-30 15:31:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
    (5, 'l', 5, '2019-01-30 15:31:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO itemgroupentrytranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, itemgroupentry_id, language_id)
VALUES
  (1, 'Red', '2019-01-30 15:36:58', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
  (2, 'Rot', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
  (3, 'Blue', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
  (4, 'Blau', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2),
  (5, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 1),
  (6, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 2),
  (7, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 4, 1),
  (8, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 4, 2),
  (9, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5, 1),
  (10, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5, 2);