INSERT IGNORE INTO item (id, identifier, accumulate_amounts, deactivated, date_created, date_updated, date_deleted, user_created_id,
                         user_updated_id, user_deleted_id, parent_id, visualization_mode_id, item_status_id, configuration_mode)
VALUES (4001, 'item_group_item_1', 1, false, '2018-06-04 15:07:45', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL, 4, 2, 'creator'),
(4002, 'item_group_item_2', 1, false, '2018-06-04 15:08:24', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL, 4, 2, 'creator'),
(4003, 'item_group_item_3', 1, false, '2018-06-04 15:07:45', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL, 4, 2, 'creator'),
(4004, 'item_group_item_4', 1, false, '2018-06-04 15:08:24', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL, 4, 2, 'creator'),
(1000001, 'mary-kate', 1, false, '2018-06-04 15:08:24', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL, 4, 2, 'creator'),
(1000002, 'ashley', 1, false, '2018-06-04 15:08:24', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL, 4, 2, 'creator');

INSERT INTO item_pool(id, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (4001, 'configurationvariant_test_1', '2018-08-13 16:17:55', NULL, '0001-01-01 00:00:00', 0, NULL, NULL),
(1000001, 'configurationvariant', '2017-07-18 13:55:11', NULL, '0001-01-01 00:00:00', 0, NULL, NULL);

INSERT IGNORE INTO item_item_pool(id, date_created, date_deleted, user_created_id, item_id, item_pool_id)
VALUES (4001, '2018-08-13 16:17:55', '0001-01-01 00:00:00', 1, 4001, 4001),
    (4002, '2018-08-14 08:41:08', '0001-01-01 00:00:00', 1, 4002, 4001),
(1000001, '2018-08-13 16:17:55', '0001-01-01 00:00:00', 1, 1000001, 1000001),
(1000002, '2018-08-13 16:17:55', '0001-01-01 00:00:00', 1, 1000002, 1000001);
