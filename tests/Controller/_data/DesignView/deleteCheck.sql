SELECT
  `a`.`date_deleted` AS design_view_deleted,
  `b`.`date_deleted` AS text_deleted,
  `d`.`date_deleted` AS design_view_deleted
FROM `design_view` AS a
       LEFT JOIN `design_view_text` AS b ON `a`.`id` = `b`.`design_view_id`
       LEFT JOIN `design_view_design_area` AS d ON `a`.`id` = `d`.`design_view_id`
WHERE `a`.`id` = 1;