INSERT INTO `design_view` (`id`, `identifier`, `custom_data`, `date_created`, `date_updated`, `date_deleted`,
                           `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `option_id`,
                           `sequence_number`)
VALUES (1, 'front', 'string', '2019-01-04 11:47:54', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, NULL, 10),
(2, 'rear', 'string', '2019-01-04 11:49:16', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, NULL, 1, 20);


INSERT INTO `design_view_design_area` (`id`, `base_shape`, `position`, `custom_data`, `date_created`, `date_updated`,
                                       `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`,
                                       `design_view_id`, `design_area_id`)
VALUES (1, 'curved', 'left', '{\"data\":\"object\"}', '2019-01-04 11:47:54', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1),
(2, 'curved', 'right', '{\"data\":\"object\"}', '2019-01-04 11:49:16', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1),
(3, 'curved', 'left', '{\"data\":\"object\"}', '2019-01-04 11:49:16', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 2);


INSERT INTO `design_view_text` (`id`, `title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`,
                                `user_updated_id`, `user_deleted_id`, `design_view_id`, `language_id`)
VALUES (1, 'Front', '2019-01-04 11:47:54', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1),
(2, 'Vorne', '2019-01-04 11:47:54', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2),
(3, 'Rear', '2019-01-04 11:49:16', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1),
(4, 'Hinten', '2019-01-04 11:49:16', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 2);


INSERT INTO `design_area` (`id`, `identifier`, `height`, `width`, `custom_data`, `date_created`, `date_updated`,
                           `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`,
                           `option_id`, `sequence_number`)
VALUES (1, 'test_create', 400, 250, '[1,2,3]', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, NULL, 10),
(2, 'test_list', 200, 200, '[1,2,3]', '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, NULL, 1, 20);


INSERT INTO `design_area_design_production_method` (`id`, `width`, `height`, `custom_data`, `date_created`,
                                                    `date_updated`, `date_deleted`, `user_created_id`,
                                                    `user_updated_id`, `user_deleted_id`, `design_area_id`,
                                                    `design_production_method_id`)
VALUES (1, 300, 200, '{"active": true, "modifier":"EXP"}', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL,
        1, 1),
(2, 180, 130, 'Hint: be careful', '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1);


INSERT INTO `design_area_text` (`id`, `title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`,
                                `user_updated_id`, `user_deleted_id`, `design_area_id`, `language_id`)
VALUES (1, 'Clip en', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1),
(2, 'Clip de', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2),
(3, 'Shaft en', '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1),
(4, 'Shaft de', '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 2);


INSERT INTO `option_text` (`id`, `title`, `abstract`, `description`, `searchtext`, `data`, `date_created`,
                           `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`,
                           `option_id`, `language_id`)
VALUES (NULL, 'coat', NULL, 'test', NULL, NULL, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1);


INSERT INTO `item` (`id`, `identifier`, `externalid`, `configurable`, `deactivated`, `minimum_order_amount`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `parent_id`, `visualization_mode_id`)
VALUES (555, 'demo_sofa', NULL, NULL, NULL, 1, '2019-08-02 16:22:10', '2019-08-02 16:22:10', '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 1);

