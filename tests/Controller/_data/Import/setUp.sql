INSERT INTO `designer_production_calculation_type` (`id`, `identifier`, `color_amount_dependent`, `item_amount_dependent`, `selectable_by_user`, `declare_separately`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_production_method_id`)
VALUES (NULL, 'printing_costs_digital_printing', 0, 1, 0, 0, '2019-04-30 13:41:51', '2019-04-30 16:18:33', '0001-01-01 00:00:00', 0, NULL, NULL,
        (SELECT id FROM `design_production_method` WHERE identifier = 'digital_printing'));

INSERT IGNORE INTO `user`(id, username, email, is_active, date_created, date_deleted, api_key)
VALUES (453, 'auth_test_with_credentials', 'test1@test.com', 1, '2020-05-11 14:13:05', '0001-01-01 00:00:00', 'test_api_token_good_credentials');

INSERT IGNORE INTO user_role(id, date_created, date_deleted, user_id, role_id)
VALUES (453, '2019-02-11 14:15:43', '0001-01-01 00:00:00', 453, (SELECT id FROM role WHERE name = "connector"));
