INSERT IGNORE INTO design_area (id, identifier, sequence_number, height, width, date_created, date_updated, date_deleted, item_id)
VALUES (501, 'chest', 1, 300, 200, '2020-01-28 03:39:05', '2020-01-28 03:39:05', '0001-01-01 00:00:00', 1),
(502, 'fur', 1, 100, 50, '2020-01-28 03:39:05', '2020-01-28 03:39:05', '0001-01-01 00:00:00', 1);

INSERT INTO `user_role`(id, date_created, date_deleted, user_id, role_id)
VALUES (555, '2019-02-11 14:15:43', '0001-01-01 00:00:00', (SELECT id FROM user WHERE username = 'rhm'), (SELECT id FROM role WHERE name = 'client'));

INSERT INTO `client`(`id`, identifier, date_created, date_updated)
VALUES (555, 'rhm', '2019-02-11 14:13:22', '0001-01-01 00:00:00');

INSERT INTO `client_user` (id, date_created, date_deleted, client_id, user_id)
VALUES (555, '2019-02-11 14:14:10', '0001-01-01 00:00:00', 555, 2);

INSERT INTO `role_credential` (`date_created`, `date_deleted`, `role_id`, `credential_id`)
VALUES ('2019-06-17 06:31:08', '0001-01-01 00:00:00', (SELECT id FROM `role` WHERE role = 'ROLE_CLIENT'), (SELECT id FROM `credential` WHERE area = 'configurations'));

INSERT IGNORE INTO configuration(id, externaluser_id, code, name, selectedoptions, designdata, customdata, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, configurationtype_id, item_id, client_id)
VALUES (555, NULL, '123456', 'My first Sheep', '{"coat":[{"identifier":"coat_nature"}],"face":[{"identifier":"face_nature"}],"legs":[{"identifier":"legs_nature"}],"eyes":[{"identifier":"eyes_yellow"}],"ear_eyelid":[{"identifier":"ear_eyelid_nature"}]}', NULL, '{"bulkNames": ["Johann Strauß","Mika Häkkinen","Vaqif Səmədoğlu","Gérard Depardieu","Sinéad O’Connor","Antonín Dvořák","Đorđe Balašević","Արամ Խաչատրյան","Вагиф Сәмәдоғлу","章子怡","王菲","أم كلثوم","ედუარდ შევარდნაძე","Γιώργος Νταλάρας","András Sütő","Björk Guðmundsdóttir","이설희","李安","ธงไชย แม็คอินไตย์","Ђорђе Балашевић"]}', '2017-08-08 12:03:02', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 1, 1, 1),
       (556, NULL, 'client_config', 'Client\'s first Sheep', '{"coat":[{"identifier":"coat_nature"}],"face":[{"identifier":"face_nature"}],"legs":[{"identifier":"legs_nature"}],"eyes":[{"identifier":"eyes_yellow"}],"ear_eyelid":[{"identifier":"ear_eyelid_nature"}]}', NULL, NULL, '2017-08-08 12:03:02', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 1, 1, 555);
