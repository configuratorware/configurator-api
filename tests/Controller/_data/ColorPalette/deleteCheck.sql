SELECT `a`.`date_deleted` AS palette_deleted,
    `b`.`date_deleted`    AS palette_text_deleted,
    `c`.`date_deleted`    AS color_deleted,
    `d`.`date_deleted`    AS color_text_deleted,
    `e`.`date_deleted`    AS design_production_method_relation_deleted
FROM `color_palette` AS a
         LEFT JOIN `color_palette_text` AS b ON `a`.`id` = `b`.`color_palette_id`
         LEFT JOIN `color` AS c ON `a`.`id` = `c`.`color_palette_id`
         LEFT JOIN `color_text` AS d ON `c`.`id` = `d`.`color_id`
         LEFT JOIN `design_production_method_color_palette` AS e ON `a`.`id` = `e`.`color_palette_id`
WHERE `a`.`identifier` = 'default'
