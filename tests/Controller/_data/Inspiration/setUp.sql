INSERT INTO `inspiration` (`id`, `item_id`, `active`, `configuration_code`, `thumbnail`, `date_created`, `date_deleted`)
VALUES
(1, (SELECT id from item where identifier = 'softshell_creator_2d'), 1, 'KQWUTQ', '/images/inspirations/thumbnail/1.png', '2021-04-23 14:50:58', '0001-01-01 00:00:00'),
(2, (SELECT id from item where identifier = 'softshell_creator_2d'), 0, NULL, NULL, '2021-04-23 14:50:58', '0001-01-01 00:00:00'),
(3, (SELECT id from item where identifier = 'demo_hoodie'), 1, 'PQWKWT', NULL, '2021-04-23 14:50:58', '0001-01-01 00:00:00'),
(4, (SELECT id from item where identifier = 'demo_hoodie'), 0, NULL, NULL, '2021-04-23 14:50:58', '0001-01-01 00:00:00'),
(5, (SELECT id from item where identifier = 'demo_hoodie'), 0, NULL, NULL, '2021-04-23 14:50:58', '0001-01-01 00:00:00'),
(6, (SELECT id from item where identifier = 'demo_hoodie'), 0, NULL, NULL, '2021-04-23 14:50:58', '0001-01-01 00:00:00');

INSERT INTO `inspiration_text` (`id`, `language_id`, `inspiration_id`, `title`, `date_created`, `date_deleted`)
VALUES
(1, 1, 1, 'en', '2021-04-23 14:50:58', '0001-01-01 00:00:00'),
(2, 2, 1, 'de', '2021-04-23 14:51:21', '0001-01-01 00:00:00');
