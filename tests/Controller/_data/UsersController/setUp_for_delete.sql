INSERT INTO `user` (id, username, firstname, lastname, email, is_active, date_created, date_updated, date_deleted,  password, api_key) VALUES
(235, 'username_for_delete1', null, null, null, 1, '2020-02-05 17:23:13', '2020-02-06 11:17:00', '0001-01-01 00:00:00', '', null),
(236, 'username_for_delete2', null, null, null, 1, '2020-02-05 17:23:13', '2020-02-06 11:17:00', '0001-01-01 00:00:00', '', 'otherkey');

INSERT INTO `user_role` (`user_id`, `role_id`, `date_created`, `date_deleted`)
VALUES
(235, (SELECT id FROM role WHERE role = 'ROLE_CLIENT' LIMIT 1), NOW(), '0001-01-01 00:00:00');

INSERT INTO `client` (`identifier`, `date_created`, `date_deleted`)
VALUES
('client_for_client_user_delete',  NOW(), '0001-01-01 00:00:00');

INSERT INTO `client_user` (`client_id`, `user_id`, `date_created`, `date_deleted`)
VALUES
((SELECT id FROM client WHERE identifier = 'client_for_client_user_delete' LIMIT 1), 235, NOW(), '0001-01-01 00:00:00');
