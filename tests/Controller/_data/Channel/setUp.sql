INSERT INTO `user_role`(date_created, date_deleted, user_id, role_id)
VALUES ('2019-02-11 14:15:43', '0001-01-01 00:00:00', (SELECT id FROM user WHERE username = 'rhm'), 2);

INSERT INTO `role_credential` (`date_created`, `date_deleted`, `role_id`, `credential_id`)
VALUES ('2019-06-17 06:31:08', '0001-01-01 00:00:00', (SELECT id FROM `role` WHERE role = 'ROLE_CLIENT'), (SELECT id FROM `credential` WHERE area = 'channels'));

INSERT INTO `client`(`id`, identifier, date_created, date_updated)
VALUES (2, 'rhm', '2019-02-11 14:13:22', '0001-01-01 00:00:00');

INSERT INTO `channel` (`id`, `identifier`, `settings`, `date_created`, `date_deleted`, `currency_id`, `global_discount_percentage`)
VALUES (2, 'rhm_de_eur', '{}', '2019-08-30 10:51:12', '0001-01-01 00:00:00', 1, 20),
    (3, 'unrelated', '{}', '2019-08-30 10:51:12', '0001-01-01 00:00:00', 1, null);

INSERT INTO `client_channel` (`date_created`, `date_deleted`, `client_id`, `channel_id`)
VALUES ('2019-08-30 10:51:47', '0001-01-01 00:00:00', 2, 2);

INSERT INTO `client_user` (date_created, date_deleted, client_id, user_id)
VALUES ('2019-02-11 14:14:10', '0001-01-01 00:00:00', 2, (SELECT id FROM user WHERE username = 'rhm'));
