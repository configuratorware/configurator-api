{
  "id": 1,
  "item": {
    "id": 1,
    "identifier": "sheep",
    "title": "Sheep"
  },
  "identifier": "front",
  "texts": [
    {
      "id": 1,
      "title": "Clip en changed",
      "language": "en_GB"
    },
     {
       "id":2,
       "title": "Clip de changed",
       "language": "de_DE"
     }
  ],
  "height": 20,
  "width": 30,
  "designProductionMethods": [],
  "customData": {}
}
