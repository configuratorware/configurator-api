INSERT INTO `design_area` (`id`, `identifier`, `mask`, `height`, `width`, `custom_data`, `date_created`, `date_updated`,
                           `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`,
                           `option_id`, `sequence_number`)
VALUES (1, 'front', '{"data":"design_area"}', 400, 250, '[1,2,3]', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, NULL, 10),
(2, 'back', '{"data":"design_area"}', 200, 200, '[1,2,3]', '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, NULL, 1, 20);

INSERT INTO `design_area_design_production_method` (`id`, `mask`, `width`, `height`, `custom_data`, `minimum_order_amount`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`,
                                                    `user_updated_id`, `user_deleted_id`, `design_area_id`, `design_production_method_id`, `allow_bulk_names`, `default_colors`, `is_default`, `design_elements_locked`, `max_elements`, `max_images`, `max_texts` )
VALUES (1, '{"data":"design_area_design_production_method"}', 300, 200, '{"active": true, "modifier":"EXP"}', 20, '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1, 1, '{"default": "red"}', 0, 1, 4, 1, 3),
(2, '{"data":"design_area_design_production_method"}', 180, 130, 'Hint: be careful', NULL, '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1, 0, NULL, 0, 0, 4, 2, 2),
(3, '{"data":"design_area_design_production_method"}', 180, 130, 'Hint: be careful 2', NULL, '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2, 0, NULL, 0, 4, NULL, NULL, NULL),
(4, '{"data":"design_area_design_production_method"}', 180, 130, 'Hint: be careful 3', NULL, '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 2, 0, NULL, 1, 0, 4, 1, 3);

INSERT INTO `design_area_text` (`id`, `title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`,
                                `user_updated_id`, `user_deleted_id`, `design_area_id`, `language_id`)
VALUES (1, 'Clip en', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1),
(2, 'Clip de', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2),
(3, 'Shaft en', '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1),
(4, 'Shaft de', '2018-12-21 14:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 2);


INSERT INTO `design_view` (`id`, `identifier`, `custom_data`, `date_created`, `date_updated`, `date_deleted`,
                           `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `option_id`,
                           `sequence_number`)
VALUES (1, 'front', 'string', '2019-01-04 11:47:54', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, NULL, 10);

INSERT INTO `design_view_design_area` (`id`, `base_shape`, `position`, `custom_data`, `date_created`, `date_updated`,
                                       `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`,
                                       `design_view_id`, `design_area_id`)
VALUES (1, 'ball', 1, '{"active": true,"modifier":"EXP"}', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL,  1, 1);

INSERT INTO `option_text` (`id`, `title`, `abstract`, `description`, `searchtext`, `data`, `date_created`,
                           `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`,
                           `option_id`, `language_id`)
VALUES (NULL, 'coat', NULL, 'test', NULL, NULL, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1);


INSERT INTO `design_area_design_production_method_price` (`id`, `amount_from`, `color_amount_from`, `price`,
                                                          `price_net`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`,
                                                          `channel_id`, `designer_production_calculation_type_id`, `design_area_design_production_method_id`)
VALUES (1, 1, 1, 1.00, 1.19, '2019-01-29 16:31:17', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1, 1),
(2, 1, 3, 2.39, 2.00, '2019-01-29 16:31:17', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1, 1),
(3, 100, 1, 0.00, 0.00, '2019-01-29 16:31:17', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1, 1),
(4, 100, 3, 0.00, 0.00, '2019-01-29 16:31:17', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1, 1),
(5, 1, 1, 0.59, 0.50, '2019-01-29 16:31:17', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2, 1),
(6, 100, 1, 0.11, 0.10, '2019-01-29 16:31:17', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2, 1);

INSERT INTO `designer_production_calculation_type` (`id`, `identifier`, `color_amount_dependent`,
                                                    `item_amount_dependent`, `selectable_by_user`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`,
                                                    `user_updated_id`, `user_deleted_id`, `design_production_method_id`)
VALUES (1, 'print', 1, 1, 0, '2019-01-29 16:24:00', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1),
(2, 'finish', 0, 1, 0, '2019-01-29 16:24:00', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1);


INSERT INTO `designer_production_calculation_type_text` (`id`, `title`, `description`, `date_created`, `date_updated`,
                                                         `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `language_id`,
                                                         `designer_production_calculation_type_id`)
VALUES (1, 'Print', NULL, '2019-01-29 16:24:00', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1),
(2, 'Druck', NULL, '2019-01-29 16:24:00', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1),
(3, 'Finish', NULL, '2019-01-29 16:24:00', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2),
(4, 'Nachbearbeitung', NULL, '2019-01-29 16:24:00', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 2);
