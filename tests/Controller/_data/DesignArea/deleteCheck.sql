SELECT
  `a`.`date_deleted` AS design_area_deleted,
  `b`.`date_deleted` AS text_deleted,
  `c`.`date_deleted` AS production_method_deleted,
  `d`.`date_deleted` AS production_method_price_deleted,
  `e`.`date_deleted` AS design_view_deleted
FROM `design_area` AS a
       LEFT JOIN `design_area_text` AS b ON `a`.`id` = `b`.`design_area_id`
       LEFT JOIN `design_area_design_production_method` AS c ON `a`.`id` = `c`.`design_area_id`
       LEFT JOIN `design_area_design_production_method_price` AS d ON `c`.`id` = `d`.`design_area_design_production_method_id`
       LEFT JOIN `design_view_design_area` AS e ON `a`.`id` = `e`.`design_area_id`
WHERE `a`.`id` = 1;