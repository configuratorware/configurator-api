INSERT INTO question_tree(id, identifier, date_created)
VALUES (1, 'tree_1', NOW()),
       (2, 'tree_2', NOW()),
       (3, 'tree_3', NOW()),
       (4, 'tree_4', NOW()),
       (5, 'tree_5', NOW()),
       (6, 'tree_6', NOW()),
       (7, 'tree_7', NOW()),
       (8, 'tree_8', NOW()),
       (9, 'tree_9', NOW()),
       (10, 'tree_10', NOW()),
       (11, 'simple_tree', NOW());

INSERT INTO question_tree_text(id, language_id, question_tree_id, title, date_created)
VALUES (1, (SELECT id FROM `language` WHERE iso = 'de_DE' LIMIT 1), 1, 'Question Tree 1', NOW()),
       (2, (SELECT id FROM `language` WHERE iso = 'de_DE' LIMIT 1), 11, 'Fragebaum', NOW()),
       (3, (SELECT id FROM `language` WHERE iso = 'en_GB' LIMIT 1), 11, 'Question Tree', NOW());


INSERT INTO question_tree_data(id, language_id, question_tree_id, file_name, `data`, date_created)
VALUES (1, (SELECT id FROM `language` WHERE iso = 'de_DE' LIMIT 1), 11, 'german.json', '{"key": "german value"}', NOW()),
       (2, (SELECT id FROM `language` WHERE iso = 'en_GB' LIMIT 1), 11, 'english.json', '{"key": "english value"}', NOW()),
       (3, (SELECT id FROM `language` WHERE iso = 'de_DE' LIMIT 1), 1, 'german.json', '{"key": "german value"}', NOW());
