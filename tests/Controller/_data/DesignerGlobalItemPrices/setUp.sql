INSERT INTO designer_global_calculation_type(id, identifier, item_amount_dependent, price_per_item, selectable_by_user, image_dependent, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
    VALUES(1, 'gift_package', 1, 1, 1, NULL, '2019-01-29 09:06:09', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO designer_global_item_price(id, amount_from, price, price_net, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, channel_id, designer_global_calculation_type_id, item_id)
    VALUES(1, 1, 11.90, 10.00, '2019-01-29 09:05:30', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1, 1);

INSERT INTO designer_global_item_price(id, amount_from, price, price_net, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, channel_id, designer_global_calculation_type_id, item_id)
    VALUES(2, 10, 5.95, 5.00, '2019-01-29 09:08:56', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1, 1);