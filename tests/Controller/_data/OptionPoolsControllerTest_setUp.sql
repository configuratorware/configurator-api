INSERT INTO `option`(id, identifier, externalid, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id)
    VALUES(3001, 'option_pool_test_option_1', NULL, 0, '2018-06-04 15:07:45', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL);
INSERT INTO `option`(id, identifier, externalid, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id)
    VALUES(3002, 'option_pool_test_option_2', NULL, 0, '2018-06-04 15:08:24', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL);

INSERT INTO option_pool(id, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
    VALUES(3001, 'option_pool_test', '2018-06-05 09:45:25', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO option_option_pool(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, option_id, option_pool_id)
    VALUES(3001, '2018-06-05 09:45:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3001, 3001);
INSERT INTO option_option_pool(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, option_id, option_pool_id)
    VALUES(3002, '2018-06-05 09:45:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3002, 3001);

INSERT INTO `option_pool_text` (`id`, `option_pool_id`, `language_id`, `date_created`, `date_deleted`, `user_created_id`, `title`, `description`)
    VALUES
(3001, 3001, 2, '2021-09-22 16:11:21','0001-01-01 00:00:00', 1, 'titleDE', 'descrDE'),
(3002, 3001, 2, '2021-09-22 16:11:21','0001-01-01 00:00:00', 1, 'titleEN', 'descrEN');

