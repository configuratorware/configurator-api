INSERT INTO item(id, identifier, externalid, configurable, deactivated, date_created, date_updated, date_deleted,
                 user_created_id, user_updated_id, user_deleted_id, parent_id, item_status_id)
VALUES (5005, 'hoodie', NULL, NULL, false, '2019-02-01 11:00:38', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL,
        2),
       (5006, 'hoodie_red_s', NULL, NULL, false, '2019-02-01 11:00:57', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        5005, NULL),
       (5007, 'hoodie_red_m', NULL, NULL, false, '2019-02-01 11:01:12', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        5005, NULL),
       (5008, 'hoodie_red_l', NULL, NULL, false, '2019-02-01 11:01:19', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        5005, NULL),
       (5009, 'hoodie_blue_s', NULL, NULL, false, '2019-02-01 11:01:32', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        5005, NULL),
       (5010, 'hoodie_blue_m', NULL, NULL, false, '2019-02-01 11:01:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        5005, NULL),
       (5011, 'hoodie_blue_l', NULL, NULL, false, '2019-02-01 11:01:45', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        5005, NULL);

INSERT INTO itemprice(price, price_net, amountfrom, date_created, date_updated, date_deleted, user_created_id,
                      user_updated_id, user_deleted_id, channel_id, item_id)
VALUES (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5005),
       (8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5005),
       (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5006),
       (8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5006),
       (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5007),
       (8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5007),
       (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5008),
       (8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5008),
       (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5009),
       (8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5009),
       (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5010),
       (8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5010),
       (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5011),
       (8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5011);

INSERT INTO design_area(id, identifier, sequence_number, height, width, custom_data, date_created, date_updated,
                        date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, option_id)
VALUES (1, 'front', 1, 100, 100, NULL, '2019-02-04 15:13:13', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005,
        NULL),
       (2, 'sleeve', 1, 100, 100, NULL, '2019-02-04 15:13:26', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005,
        NULL);

INSERT INTO design_area_design_production_method(id, width, height, additional_data, custom_data, date_created,
                                                 date_updated, date_deleted, user_created_id, user_updated_id,
                                                 user_deleted_id, design_area_id, design_production_method_id,
                                                 allow_bulk_names)
VALUES (1, NULL, NULL, NULL, NULL, '2019-02-04 15:50:54', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1, 1),
       (2, NULL, NULL, NULL, NULL, '2019-02-04 15:51:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1, 1);

INSERT INTO designer_production_calculation_type(id, identifier, color_amount_dependent, item_amount_dependent,
                                                 selectable_by_user, date_created, date_updated, date_deleted,
                                                 user_created_id, user_updated_id, user_deleted_id,
                                                 design_production_method_id, bulk_name_dependent)
VALUES (1, 'setup_cost', 1, 0, 0, '2019-02-04 15:49:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 0),
       (2, 'printing_cost', 1, 1, 0, '2019-02-04 15:50:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 0),
       (3, 'print_cost_for_bulk_name', 0, 0, 0, '2019-02-04 15:50:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1,
        1);

INSERT INTO design_production_method_price(id, color_amount_from, price, price_net, date_created, date_updated,
                                           date_deleted, user_created_id, user_updated_id, user_deleted_id, channel_id,
                                           designer_production_calculation_type_id)
VALUES (1, 1, 19.00, 15.97, '2019-02-04 15:57:56', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
       (2, 1, 33.33, 33.00, '2019-02-04 15:57:56', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 3);

INSERT INTO designer_production_calculation_type_text(id, title, description, date_created, date_updated, date_deleted,
                                                      user_created_id, user_updated_id, user_deleted_id, language_id,
                                                      designer_production_calculation_type_id)
VALUES (1, 'Setup cost (print)', NULL, '2019-02-04 15:59:50', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
       (2, 'Einrichtungskosten (Druck)', NULL, '2019-02-04 16:00:11', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2,
        1),
       (3, 'Printing cost (print)', NULL, '2019-02-04 16:00:38', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
       (4, 'Druckkosten (Druck)', NULL, '2019-02-04 16:01:04', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2),
       (5, 'Production cost for bulk names', NULL, '2019-02-04 16:01:04', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        1, 3),
       (6, 'Produktionskosten für Bulk-Namen', NULL, '2019-02-04 16:01:04', NULL, '0001-01-01 00:00:00', NULL, NULL,
        NULL, 2, 3);

INSERT INTO design_area_design_production_method_price(id, amount_from, color_amount_from, price, price_net,
                                                       date_created, date_updated, date_deleted, user_created_id,
                                                       user_updated_id, user_deleted_id, channel_id,
                                                       designer_production_calculation_type_id,
                                                       design_area_design_production_method_id)
VALUES (1, 1, 1, 0.99, 0.83, '2019-02-04 16:28:23', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2, 1),
       (2, 100, 1, 0.89, 0.75, '2019-02-04 16:29:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2, 1),
       (3, 1, 1, 0.99, 0.83, '2019-02-04 16:29:21', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2, 2),
       (4, 100, 1, 0.89, 0.75, '2019-02-04 16:29:37', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2, 2),
       (6, 1, 1, 51.22, 51.02, '2019-02-04 16:29:37', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 3, 1),
       (7, 1, 1, 53.22, 53.02, '2019-02-04 16:29:37', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 3, 2);

INSERT INTO designer_global_calculation_type(id, identifier, item_amount_dependent, price_per_item, selectable_by_user, image_dependent,
                                             date_created, date_updated, date_deleted, user_created_id, user_updated_id,
                                             user_deleted_id, bulk_name_dependent)
VALUES (1, 'gift_packaging', 1, 1, 1, NULL, '2019-02-05 08:43:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 0),
       (2, 'logo_cost', 0, 0, 1, 'image_data_dependent', '2019-02-05 08:44:31', NULL, '0001-01-01 00:00:00', NULL, NULL,
        NULL, 0),
       (3, 'print_bulk_name_cost', 0, 0, 1, NULL, '2019-02-05 08:44:31', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1),
       (4, 'user_image', 0, 0, 0, 'user_image_dependent', '2019-02-05 08:44:31', NULL, '0001-01-01 00:00:00', NULL, NULL,
        NULL, 0),
       (5, 'gallery_image', 0, 0, 0, 'gallery_image_dependent', '2019-02-05 08:44:31', NULL, '0001-01-01 00:00:00', NULL,
        NULL, NULL, 0);

INSERT INTO designer_global_calculation_type_text(id, title, description, date_created, date_updated, date_deleted,
                                                  user_created_id, user_updated_id, user_deleted_id, language_id,
                                                  designer_global_calculation_type_id)
VALUES (1, 'Gift packaging', NULL, '2019-02-05 08:45:47', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
       (2, 'Geschenkverpackung', NULL, '2019-02-05 08:45:58', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
       (3, 'Logo cost', NULL, '2019-02-05 08:46:09', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
       (4, 'Logokosten', NULL, '2019-02-05 08:46:19', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2),
       (5, 'Global bulk name printing cost', NULL, '2019-02-05 08:46:19', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL,
        1, 3),
       (6, 'Globale Druckkosten für Massennamen', NULL, '2019-02-05 08:46:19', NULL, '0001-01-01 00:00:00', NULL, NULL,
        NULL, 2, 3);

INSERT INTO designer_global_item_price(id, amount_from, price, price_net, date_created, date_updated, date_deleted,
                                       user_created_id, user_updated_id, user_deleted_id, channel_id,
                                       designer_global_calculation_type_id, item_id)
VALUES (1, 1, 1.99, 1.67, '2019-02-05 08:42:50', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1, 5005),
       (2, 100, 1.89, 1.59, '2019-02-05 08:47:30', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1, 5005);

INSERT INTO designer_global_calculation_type_price(id, price, price_net, date_created, date_updated, date_deleted,
                                                   user_created_id, user_updated_id, user_deleted_id, channel_id,
                                                   designer_global_calculation_type_id)
VALUES (1, 19.00, 15.97, '2019-02-05 08:49:49', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
       (2, 40.44, 40.00, '2019-02-05 08:49:49', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 3),
       (3, 33.44, 33.00, '2019-02-05 08:49:49', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 4),
       (4, 55.44, 55.00, '2019-02-05 08:49:49', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 5);


INSERT IGNORE INTO `imagegalleryimage` (`id`, `imagename`, `date_created`, `date_deleted`)
VALUES (101, 'test.svg', '2019-01-25 11:05:13', '0001-01-01 00:00:00'),
       (102, 'test2.svg', '2019-01-25 11:05:13', '0001-01-01 00:00:00');

INSERT IGNORE INTO `image_gallery_image_price` (`id`, `channel_id`, `imagegalleryimage_id`, `price`, `price_net`,
                                                `date_created`, `date_deleted`)
VALUES (101, 1, 101, 12.0, 14.5, '2019-01-25 11:05:13', '0001-01-01 00:00:00'),
       (102, 1, 102, 15.5, 17.3, '2019-01-25 11:05:13', '0001-01-01 00:00:00');
