INSERT INTO `item` (`identifier`, `minimum_order_amount`,
                    `date_created`, `date_deleted`, `parent_id`, item_status_id)
VALUES ('test_item_available', 1, NOW(), '0001-01-01 00:00:00', NULL, 2),
('test_item_available_creator', 1, NOW(), '0001-01-01 00:00:00', NULL, 2),
('test_item_available_designer', 1, NOW(), '0001-01-01 00:00:00', NULL, 2),
    ('test_item_unavailable', 1, NOW(), '0001-01-01 00:00:00', NULL, 1),
('test_child_available', 1, NOW(), '0001-01-01 00:00:00',
 (SELECT id FROM `item` AS i WHERE i.identifier = 'test_item_available' LIMIT 1), NULL),
('test_child_unavailable', 1, NOW(), '0001-01-01 00:00:00',
 (SELECT id FROM `item` AS i WHERE i.identifier = 'test_item_unavailable' LIMIT 1), NULL);


INSERT IGNORE INTO `user`(id, username, email, is_active, date_created, date_deleted, api_key)
VALUES (453, 'auth_test_with_credentials', 'test1@test.com', 1, '2020-05-11 14:13:05', '0001-01-01 00:00:00',
        'test_api_token_good_credentials');

INSERT IGNORE INTO user_role(id, date_created, date_deleted, user_id, role_id)
VALUES (453, '2019-02-11 14:15:43', '0001-01-01 00:00:00', 453, (SELECT id FROM role WHERE name = "connector"));
