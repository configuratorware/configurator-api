DELETE FROM tag;

INSERT
INTO tag(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES
(1, '2017-10-13 15:29:39', NULL, '0001-01-01 00:00:00', 1, NULL, NULL),
(2, '2017-10-13 15:29:39', NULL, '0001-01-01 00:00:00', 1, NULL, NULL);

INSERT
INTO tagtranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, tag_id, language_id)
VALUES
(1, 'Test', '2017-10-13 15:29:48', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2);
