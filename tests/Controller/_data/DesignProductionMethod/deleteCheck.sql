SELECT `a`.`date_deleted` AS method_deleted,
    `b`.`date_deleted`    AS method_text_deleted,
    `c`.`date_deleted`    AS color_palette_deleted
FROM `design_production_method` AS a
         LEFT JOIN `design_production_method_text` AS b ON `a`.`id` = `b`.`design_production_method_id`
         LEFT JOIN `design_production_method_color_palette` AS c ON `a`.`id` = `c`.`design_production_method_id`
WHERE `a`.`identifier` = 'screen_printing';
