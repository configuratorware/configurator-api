DELETE FROM `designer_production_calculation_type`;
INSERT INTO `designer_production_calculation_type` (`id`, `identifier`, `price_per_item`, `color_amount_dependent`,
                                                    `item_amount_dependent`, `selectable_by_user`, `date_created`,
                                                    `date_updated`, `date_deleted`, `user_created_id`,
                                                    `user_updated_id`, `user_deleted_id`, `design_production_method_id`,
                                                    `declare_separately`)
VALUES (1, 'mashine_setup_XL', 0, 1, NULL, NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 0),
(2, 'mashine_setup_M', 0, 1, NULL, NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1),
(3, 'mashine_setup_S', 0, 1, NULL, NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, NULL);

DELETE FROM `designer_production_calculation_type_text`;
INSERT INTO `designer_production_calculation_type_text` (`id`, `title`, `description`, `date_created`, `date_updated`,
                                                         `date_deleted`, `user_created_id`, `user_updated_id`,
                                                         `user_deleted_id`, `language_id`,
                                                         `designer_production_calculation_type_id`)
VALUES (1, 'Mashine Setup XL', NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 1),
(2, 'Einrichtung Maschine XL', NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 1),
(3, 'Mashine Setup M', NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 2),
(4, 'Einrichtung Maschine M', NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 2),
(5, 'Mashine Setup S', NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 3),
(6, 'Einrichtung Maschine S', NULL, '2019-01-25 11:05:13', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 2, 3);

DELETE FROM `design_production_method_price`;
INSERT INTO `design_production_method_price` (`id`, `color_amount_from`, `price`, `price_net`, `date_created`,
                                              `date_deleted`, `channel_id`, `designer_production_calculation_type_id`,
                                              `amount_from`)
VALUES (1, 1, 287.50, 250.00, '2019-01-25 11:05:13', '0001-01-01 00:00:00', 1, 1, 1),
(2, 10, 28.75, 25.00, '2019-01-25 11:05:13', '0001-01-01 00:00:00', 1, 1, 1),
(3, 1, 28.75, 25.00, '2019-01-25 11:05:13', '0001-01-01 00:00:00', 1, 2, 1),
(4, 10, 23.80, 20.00, '2019-01-25 11:05:13', '0001-01-01 00:00:00', 1, 2, 1),
(5, 1, 33.80, 30.00, '2019-01-25 11:05:13', '0001-01-01 00:00:00', 1, 3, 1);

INSERT INTO `design_area` (`id`, `identifier`, `mask`, `height`, `width`, `custom_data`, `date_created`, `date_updated`,
                           `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`,
                           `option_id`, `sequence_number`)
VALUES (1, 'front', '{"data":"design_area"}', 400, 250, '[1,2,3]', '2018-12-21 14:44:18', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, NULL, 10);

INSERT INTO `design_area_design_production_method` (id, date_created, date_updated, date_deleted,
                                                    design_area_id, design_production_method_id)
VALUES (123, '2020-02-10 17:51:49', '2020-02-10 17:51:49', '0001-01-01 00:00:00', 1, 1);
