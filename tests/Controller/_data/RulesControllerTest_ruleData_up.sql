INSERT INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (1, '{"maxamount":5,"optionclassification_identifier":"face"}', NULL, NULL, '2017-06-21 10:39:46', NULL, '0001-01-01 00:00:00', NULL, 0, NULL, 4, 1, 1, NULL);
INSERT INTO rule(id, data, action, orderable, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, ruletype_id, rulefeedback_id, item_id, option_id)
VALUES (2, '{"minamount":2,"optionclassification_identifier":"face"}', NULL, NULL, '2017-06-21 10:39:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 1, 1, NULL);

INSERT INTO rule_text(id, title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, rule_id, language_id)
VALUES (1, 'The maximum amount of options in the category "face" is "5".', '2018-09-18 16:02:26', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 1, 1);
INSERT INTO rule_text(id, title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, rule_id, language_id)
VALUES (2, 'In der Kategorie "Gesicht" dürfen maximal "5" Optionen gewählt sein.', '2018-09-18 16:02:26', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 1, 2);
INSERT INTO rule_text(id, title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, rule_id, language_id)
VALUES (3, 'The minimum amount of options in the category "face" is "2".', '2018-09-18 16:02:26', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 2, 1);
INSERT INTO rule_text(id, title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, rule_id, language_id)
VALUES (4, 'In der Kategorie "Gesicht" müssen mindestens "2" Optionen gewählt sein.', '2018-09-18 16:02:26', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 2, 2);
