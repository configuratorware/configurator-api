INSERT IGNORE INTO assemblypointimageelement(id, elementidentifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, optionclassification_id, is_root)
VALUES (1, NULL, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1);

INSERT IGNORE INTO assemblypointimageelement(id, elementidentifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, optionclassification_id, is_root)
VALUES (2, NULL, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 0);

INSERT IGNORE INTO assemblypointimageelementrelation(id, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, assemblypointimageelement_id, parent_assemblypointimageelement_id)
VALUES (1, 0, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1);

INSERT IGNORE INTO assemblypointimageelementlayer(id, layeridentifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, assemblypointimageelement_id)
VALUES(1, NULL, 0, NULL, NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1);

INSERT IGNORE INTO assemblypointimageelementlayer(id, layeridentifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, assemblypointimageelement_id)
VALUES(2, NULL, 0, NULL, NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2);

INSERT IGNORE INTO assemblypoint(id, x, y, rotation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, assemblypointimageelement_id, option_id)
VALUES (1, -540, 227, 0, NULL, NULL, '0001-01-01 00:00:00', NULL, 0, NULL, 2, 1);