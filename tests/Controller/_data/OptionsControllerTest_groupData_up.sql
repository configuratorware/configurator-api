INSERT INTO itemgroup(id, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, sequencenumber, configurationvariant) VALUES (2000002, 'superhero', '2017-07-18 13:55:11', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL, 0);

INSERT INTO `option`(id, identifier, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id) VALUES(2000000, 'coat_superhero', NULL, '0001-01-01 00:00:00', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL);
INSERT INTO `option`(id, identifier, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id) VALUES(2000001, 'coat_superman', NULL, '0001-01-01 00:00:00', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 2000000);
INSERT INTO `option`(id, identifier, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id) VALUES(2000002, 'coat_batman', NULL, '0001-01-01 00:00:00', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 2000000);

INSERT INTO item_optionclassification_option(id, amountisselectable, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_optionclassification_id, option_id) VALUES(2000001, false, '2017-07-18 15:44:07', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 1, 2000001);
INSERT INTO item_optionclassification_option(id, amountisselectable, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_optionclassification_id, option_id) VALUES(2000002, false, '2017-07-18 15:44:18', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 1, 2000002);

INSERT INTO option_group(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, option_id, group_id) VALUES(2000001, '0001-01-01 00:00:00', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2000001, 2000002);
INSERT INTO option_group(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, option_id, group_id) VALUES(2000002, '0001-01-01 00:00:00', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2000002, 2000002);

INSERT INTO itemgroupentry(id, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, sequencenumber) VALUES (2000001, 'superman', '2017-07-18 13:55:11', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL);
INSERT INTO itemgroupentry(id, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, sequencenumber) VALUES (2000002, 'batman', '2017-07-18 13:55:11', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, NULL);

INSERT INTO option_group_entry(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, option_id, group_id, group_entry_id) VALUES(2000001, '2017-07-18 15:44:18', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 2000001, 2000002, 2000001);
INSERT INTO option_group_entry(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, option_id, group_id, group_entry_id) VALUES(2000002, '2017-07-18 15:44:18', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 2000002, 2000002, 2000002);
