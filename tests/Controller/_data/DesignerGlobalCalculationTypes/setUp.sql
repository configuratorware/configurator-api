INSERT INTO `designer_global_calculation_type` (`id`, `identifier`, `item_amount_dependent`, `selectable_by_user`, `image_dependent`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`)
VALUES
	(1,'shipping',1,0,'gallery_image_dependent','2019-01-24 09:03:37',NULL,'0001-01-01 00:00:00',0,NULL,NULL),
	(2,'setup',0,0,NULL,'2019-01-24 09:03:37',NULL,'0001-01-01 00:00:00',0,NULL,NULL);

INSERT INTO `designer_global_calculation_type_price` (`id`, `price`, `price_net`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `channel_id`, `designer_global_calculation_type_id`)
VALUES
	(1,59.50,40.00,'2019-01-24 09:03:37',NULL,'0001-01-01 00:00:00',0,NULL,NULL,1,2);

INSERT INTO `designer_global_calculation_type_text` (`id`, `title`, `description`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `language_id`, `designer_global_calculation_type_id`)
VALUES
	(1,'Shipping Costs','We charge €6.90 per 20.','2019-01-24 09:03:37',NULL,'0001-01-01 00:00:00',0,NULL,NULL,1,1),
	(2,'Versandkosten','Je 20 Stk werden 6,90€ berechnet.','2019-01-24 09:03:37',NULL,'0001-01-01 00:00:00',0,NULL,NULL,2,1),
	(3,'Setup','This is charged only once.','2019-01-24 09:03:37',NULL,'0001-01-01 00:00:00',0,NULL,NULL,1,2),
	(4,'Einrichtung','Einmalige Berechnung.','2019-01-24 09:03:37',NULL,'0001-01-01 00:00:00',0,NULL,NULL,2,2);