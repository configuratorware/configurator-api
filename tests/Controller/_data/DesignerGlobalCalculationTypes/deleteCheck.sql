SELECT
  `a`.`date_deleted` AS base_deleted,
  `b`.`date_deleted` AS text_deleted,
  `c`.`date_deleted` AS price_deleted
FROM `designer_global_calculation_type` AS `a`
JOIN `designer_global_calculation_type_text` AS `b` ON `b`.`designer_global_calculation_type_id` = `a`.`id`
JOIN `designer_global_calculation_type_price`AS `c` ON `c`.`designer_global_calculation_type_id` = `a`.`id`
WHERE `identifier` IN ('setup');
