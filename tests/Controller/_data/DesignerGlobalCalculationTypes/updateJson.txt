{
    "id": 2,
    "identifier": "setup_test",
    "itemAmountDependent": false,
    "selectableByUser": false,
    "imageDependent" : "gallery_image_dependent",
    "bulkNameDependent": true,
    "texts": [
        {
            "id": 3,
            "title": "Setup changed",
            "description": "This is charged only once.",
            "language": "en_GB"
        },
        {
            "id": 4,
            "title": "Einrichtung changed",
            "description": "Einmalige Berechnung.",
            "language": "de_DE"
        }
    ],
    "prices": []
}