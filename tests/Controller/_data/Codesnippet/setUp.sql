INSERT INTO `user_role`(date_created, date_deleted, user_id, role_id)
VALUES ('2019-02-11 14:15:43', '0001-01-01 00:00:00', (SELECT id FROM user WHERE username = 'rhm'), (SELECT id from role WHERE role = 'ROLE_CLIENT'));

INSERT INTO `role_credential` (`date_created`, `date_deleted`, `role_id`, `credential_id`)
VALUES ('2019-06-17 06:31:08', '0001-01-01 00:00:00', (SELECT id from role WHERE role = 'ROLE_CLIENT'), (SELECT id FROM credential WHERE area = 'code_snippets'));

INSERT INTO `client`(`id`,identifier, date_created, date_updated, send_offer_request_mail_to_customer)
VALUES (2, 'rhm', '2019-02-11 14:13:22', '0001-01-01 00:00:00', false);

INSERT INTO `client_user` (date_created, date_deleted, client_id, user_id)
VALUES ('2019-02-11 14:14:10', '0001-01-01 00:00:00', (SELECT id FROM `client` WHERE identifier = 'rhm'), (SELECT id FROM user WHERE username = 'rhm'));

INSERT INTO `codesnippet` (`id`, `identifier`, `code`, `date_created`, `date_deleted`, `client_id`, `language_id`)
VALUES (2, 'test_client', 'test', '2019-01-01 00:00:00', '0001-01-01 00:00:00', (SELECT id FROM `client` WHERE identifier = 'rhm'),(SELECT id FROM `language` WHERE iso = 'de_DE'));
