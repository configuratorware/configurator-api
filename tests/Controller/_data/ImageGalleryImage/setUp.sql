INSERT
INTO imagegalleryimage(id, imagename, thumbname, sequence_number, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, printname)
VALUES
(101, 'test.jpg', 'test_t.jpg', 80, '2017-10-13 12:09:02', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 'test_t.jpg'),
(102, 'myimage.jpg', 'myimage_t.jpg', 40, '2017-10-13 14:03:26', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 'test_t.jpg');

INSERT
INTO imagegalleryimagetext(id, title, description, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, imagegalleryimage_id)
VALUES
(101, 'Image1', NULL, '2017-10-13 13:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 101),
(102, 'Image2', NULL, '2017-10-13 13:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 1, 102);

INSERT
INTO tag(id, sequence_number, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES
(101, 100, '2017-10-13 15:29:39', NULL, '0001-01-01 00:00:00', 1, NULL, NULL),
(102, 50, '2017-10-13 15:29:39', NULL, '0001-01-01 00:00:00', 1, NULL, NULL);

INSERT
INTO tagtranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, tag_id, language_id)
VALUES
(101, 'Tag1', '2017-10-13 15:29:48', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 101, 1),
(102, 'Tag2', '2017-10-13 13:52:19', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 102, 1);

INSERT
INTO imagegalleryimage_tag(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, tag_id, imagegalleryimage_id)
VALUES
(101, '2017-10-13 15:06:07', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 101, 101),
(102, '2017-10-13 15:06:07', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 102, 101),
(103, '2017-10-13 15:06:07', NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 102, 102);

INSERT INTO `image_gallery_image_price` (`id`, `channel_id`, `imagegalleryimage_id`, `price`, `price_net`, `date_created`, `date_updated`, `date_deleted`)
VALUES
    (101, 1, 101, 59.00000, 64.00000, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
    (102, 1, 102, 54.00000, 60.00000, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');
