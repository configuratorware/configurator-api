INSERT IGNORE INTO `user`(id, username, email, is_active, date_created, date_deleted, api_key) VALUES
(453,  'auth_test_with_credentials', 'test1@test.com', 1, '2020-05-11 14:13:05', '0001-01-01 00:00:00', 'test_api_token_good_credentials'),
(454,  'auth_test_user_no_credentials', 'test2@test.com', 1, '2020-05-11 14:13:05', '0001-01-01 00:00:00', 'test_api_token_no_credentials');

INSERT IGNORE INTO user_role(id, date_created,  date_deleted, user_id, role_id)
VALUES
       (453, '2019-02-11 14:15:43', '0001-01-01 00:00:00', 453, (SELECT id FROM role WHERE name="connector")),
       (454, '2019-02-11 14:15:43', '0001-01-01 00:00:00', 454, (SELECT id FROM role WHERE name="client"));
