INSERT IGNORE INTO item (id, identifier,  minimum_order_amount, accumulate_amounts, date_created, date_updated, date_deleted, visualization_mode_id, item_status_id, configuration_mode, overwrite_component_order)
VALUES (2003,  'softshell_creator_2d', 1, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1, 2, 'creator', 0);

INSERT INTO `creator_view` (`id`,`item_id`, `identifier`, `directory`, `sequence_number`, `custom_data`, `date_created`,  `date_deleted`)
VALUES
(1, 2003, 'front', 'softshell_creator_2d/01_front', '01', NULL, NOW(), '0001-01-01 00:00:00');

INSERT IGNORE INTO itemprice (id, channel_id, item_id, price, price_net, amountfrom, date_created, date_updated, date_deleted)
VALUES (2200, 1, 2003, 0.00000, 0.00000, 1,  NOW(), NOW(), '0001-01-01 00:00:00');

INSERT IGNORE INTO optionclassification (id, identifier, sequencenumber, date_created, date_updated, date_deleted, hidden_in_frontend)
VALUES (2001,  'softshell_body', 1, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (2002, 'softshell_zipper', 3, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (2003, 'softshell_application', 4, NOW(), NOW(), '0001-01-01 00:00:00', 0);

INSERT IGNORE INTO `option` (id, parent_id, sequencenumber, identifier, date_created, date_updated, date_deleted)
VALUES (2001, null, 1, 'softshell_red', NOW(), NOW(), '0001-01-01 00:00:00'),
       (2002, null, 2, 'softshell_avocado', NOW(), NOW(), '0001-01-01 00:00:00'),
       (2007, null, 1, 'softshell_redblack', NOW(), NOW(), '0001-01-01 00:00:00'),
       (2008, null, 2, 'softshell_whitegreen', NOW(), NOW(), '0001-01-01 00:00:00');

INSERT IGNORE INTO option_price (id, channel_id, option_id, price, price_net, amountfrom, date_created, date_updated, date_deleted, user_created_id)
VALUES (2001, 1, 2001, 11.90000, 10.00000, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1),
       (2002, 1, 2002, 11.90000, 10.00000, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1),
       (2003, 1, 2007, 11.90000, 10.00000, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1),
       (2004, 1, 2008, 11.90000, 10.00000, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1);

INSERT IGNORE INTO item_optionclassification (id, item_id, optionclassification_id, is_multiselect, minamount, maxamount, is_mandatory, creator_view_id, date_created, date_updated, date_deleted, sequence_number)
VALUES (2001, 2003, 2001, 0, null, null, 1, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1),
       (2002, 2003, 2002, 0, null, null, 1, null, NOW(), NOW(), '0001-01-01 00:00:00', 2),
       (2003, 2003, 2003, 0, null, null, 1, null, NOW(), NOW(), '0001-01-01 00:00:00', 3);

INSERT IGNORE INTO item_optionclassification_option (id, item_optionclassification_id, option_id, amountisselectable, date_created, date_updated, date_deleted)
VALUES (2001, 2001, 2001, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (2002, 2001, 2002, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (2003, 2002, 2001, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (2004, 2002, 2002, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (2005, 2003, 2001, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (2006, 2003, 2007, 0, NOW(), NOW(), '0001-01-01 00:00:00');

INSERT INTO `itemoptionclassificationoptiondeltaprice` (`id`,`channel_id`, `item_optionclassification_option_id`, `price`, `price_net`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(2001, 1, 2001, 10.00000, NULL, NOW(), NOW(), '0001-01-01 00:00:00'),
(2002, 1, 2004, 20.00000, NULL, NOW(), NOW(), '0001-01-01 00:00:00');


INSERT INTO `option_text` (`option_id`, `language_id`, `title`, `abstract`, `description`, `searchtext`, `data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`)
VALUES
(2001, 1, 'Red', NULL, NULL, NULL, NULL, '2021-09-06 14:44:11', '2021-09-06 14:44:18', '0001-01-01 00:00:00', NULL, NULL, NULL),
(2001, 2, 'Rot', NULL, NULL, NULL, NULL, '2021-09-06 14:44:11', '2021-09-06 14:44:18', '0001-01-01 00:00:00', NULL, NULL, NULL),
(2002, 1, 'Avocado', NULL, NULL, NULL, NULL, '2021-09-06 14:44:13', '2021-09-06 14:44:19', '0001-01-01 00:00:00', NULL, NULL, NULL),
(2002, 2, 'Avocado', NULL, NULL, NULL, NULL, '2021-09-06 14:44:13', '2021-09-06 14:44:19', '0001-01-01 00:00:00', NULL, NULL, NULL),
(2007, 1, 'Red/Black', NULL, NULL, NULL, NULL, '2021-09-06 14:44:23', '2021-09-06 14:44:23', '0001-01-01 00:00:00', NULL, NULL, NULL),
(2007, 2, 'Rot/Schwarz', NULL, NULL, NULL, NULL, '2021-09-06 14:44:23', '2021-09-06 14:44:23', '0001-01-01 00:00:00', NULL, NULL, NULL),
(2008, 1, 'Green/White', NULL, NULL, NULL, NULL, '2021-09-06 14:44:25', '2021-09-06 14:44:25', '0001-01-01 00:00:00', NULL, NULL, NULL),
(2008, 2, 'Grün/Weiß', NULL, NULL, NULL, NULL, '2021-09-06 14:44:25', '2021-09-06 14:44:25', '0001-01-01 00:00:00', NULL, NULL, NULL);
