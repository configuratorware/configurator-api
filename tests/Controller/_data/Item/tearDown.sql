DELETE FROM itemgroup;
DELETE FROM itemgrouptranslation;
DELETE FROM itemgroupentry;
DELETE FROM itemgroupentrytranslation;
DELETE FROM item WHERE id IN (5005, 5006, 5007, 5008, 5009, 5010, 5011);
DELETE FROM configuration WHERE id IN (5005, 5006, 5007, 5008, 5009, 5010, 5011);
DELETE FROM item_itemgroup WHERE id IN (5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009, 5010, 5011);
DELETE FROM item_itemgroupentry WHERE id IN (5000, 5001, 5002, 5003, 5004, 5005, 5006, 5007, 5008, 5009, 5010, 5011);
