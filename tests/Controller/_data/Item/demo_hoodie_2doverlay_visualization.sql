DELETE FROM `design_area`;
INSERT INTO `design_area` (`id`, `identifier`, `sequence_number`, `height`, `width`, `custom_data`, `date_created`,
                           `date_deleted`, `item_id`, `option_id`, `mask`)
VALUES (1, 'front_center', 1, 60, 50, NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, NULL, '{ "data": "design_area" }'),
(2, 'rear_center', 2, 100, 1000, NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, NULL, '{ "data": "design_area" }'),
(3, 'sleeve', 3, 100, 1000, NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, NULL, NULL);

INSERT INTO `design_area_text` ( `title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_area_id`, `language_id`)
VALUES ('Front center', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
('Vorderseite mittig', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
('Rear center', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
('Rückseite Mitte', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2),
('Sleeve', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 1),
('Ärmel', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 2);

DELETE FROM `design_view`;
INSERT INTO `design_view` (`id`, `identifier`, `sequence_number`, `custom_data`, `date_created`,
                           `date_deleted`, `item_id`, `option_id`)
VALUES (1, 'front_view', 1, NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, NULL),
(2, 'rear_view', 2, NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, NULL),
(3, 'side_view', 3, NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, NULL);

INSERT INTO `design_view_text` (`id`, `title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_view_id`, `language_id`)
VALUES (1, 'Front view', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
(2, 'Vorderansicht', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
(3, 'Rear view', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
(4, 'Rückansicht', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2),
(5, 'Side view', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 1),
(6, 'Seitenansicht', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 2);

DELETE FROM `design_view_design_area`;
INSERT INTO `design_view_design_area` (`id`, `base_shape`, `position`, `custom_data`, `date_created`, `date_deleted`, `design_view_id`, `design_area_id`, `is_default`)
VALUES (1, '{\"type\": \"Plane\", \"x\":990,\"y\":844,\"width\":876,\"height\":390}', '{\"x\":100,\"y\":100,\"width\":500,\"height\":600}', NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 1, 1, 0),
(2, '{\"type\": \"Plane\", \"x\":990,\"y\":844,\"width\":876,\"height\":390}', '{\"x\":100,\"y\":100,\"width\":1000,\"height\":100}', NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, 2, 0),
(3, '{\"type\": \"Plane\", \"x\":990,\"y\":844,\"width\":876,\"height\":390}', '{\"x\":100,\"y\":100,\"width\":1000,\"height\":100}', NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 1, 3, 1),
(4, '{\"type\": \"Plane\", \"x\":990,\"y\":844,\"width\":876,\"height\":390}', '{\"x\":100,\"y\":100,\"width\":1000,\"height\":100}', NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 3, 3, 0),
(5, '{\"type\": \"Plane\", \"x\":990,\"y\":844,\"width\":876,\"height\":390}', '{\"x\":100,\"y\":100,\"width\":1000,\"height\":100}', NULL, '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, 3, 0);

INSERT INTO `design_area_design_production_method` (`id`, `design_area_id`, `design_production_method_id`, `additional_data`, `width`, `height`, `is_default`, `minimum_order_amount`, `date_created`, `date_deleted`, `mask`, `design_elements_locked`, `max_elements`, `max_images`, `max_texts` )
VALUES (1, 1, 1, '{"engravingBackgroundColors": [{"itemIdentifier": "demo_hoodie_blue_s","colorHex": "#0000FF"}]}', 0, 0, 1, 1, '2020-11-11 10:59:10', '0001-01-01 00:00:00', '{ "data": "design_area_design_production_method" }', 1, 4, 1, 3),
(2, 1, 6, '{"engravingBackgroundColors": [{"itemIdentifier": "demo_hoodie_blue_s","colorHex": "#0000FF"}]}', 0, 0, 0, 1, '2020-11-11 10:59:10', '0001-01-01 00:00:00', '{ "data": "design_area_design_production_method" }', 0, 4, 2, 2),
(3, 2, 5, null, 0, 0, 0, 1, '2020-11-11 10:59:10', '0001-01-01 00:00:00', NULL, 1, 4, NULL, NULL);

DELETE FROM `item`;
INSERT INTO `item` (`id`, `identifier`, `minimum_order_amount`, `date_created`, `date_deleted`, `parent_id`,
                    `visualization_mode_id`, `item_status_id`, `configuration_mode`)
VALUES (2, 'demo_hoodie', 1, '2019-06-07 14:53:02', '0001-01-01 00:00:00', NULL, 5, 2, 'designer'),
(3, 'demo_hoodie_blue_s', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(4, 'demo_hoodie_blue_m', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(5, 'demo_hoodie_blue_l', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(6, 'demo_hoodie_blue_xl', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(7, 'demo_hoodie_green_s', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(8, 'demo_hoodie_green_m', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(9, 'demo_hoodie_green_l', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(10, 'demo_hoodie_green_xl', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(11, 'demo_hoodie_red_s', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(12, 'demo_hoodie_red_m', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(13, 'demo_hoodie_red_l', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL),
(14, 'demo_hoodie_red_xl', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, NULL, NULL, NULL);

DELETE FROM `itemgroup`;
INSERT INTO `itemgroup` (`id`, `identifier`, `sequencenumber`, `configurationvariant`, `date_created`, `date_deleted`)
VALUES (1, 'color', 1, 0, '2019-06-07 14:53:03', '0001-01-01 00:00:00'),
    (2, 'size', 2, 0, '2019-06-07 14:53:03', '0001-01-01 00:00:00');

DELETE FROM `itemgroupentry`;
INSERT INTO `itemgroupentry` (`id`, `identifier`, `sequencenumber`, `date_created`, `date_deleted`)
VALUES (1, 'blue', 1, '2019-06-07 14:53:03', '0001-01-01 00:00:00'),
    (2, 's', 2, '2019-06-07 14:53:03', '0001-01-01 00:00:00'),
    (3, 'm', 3, '2019-06-07 14:53:03', '0001-01-01 00:00:00'),
    (4, 'l', 4, '2019-06-07 14:53:03', '0001-01-01 00:00:00'),
    (5, 'xl', 5, '2019-06-07 14:53:03', '0001-01-01 00:00:00'),
    (6, 'green', 6, '2019-06-07 14:53:03', '0001-01-01 00:00:00'),
    (7, 'red', 7, '2019-06-07 14:53:03', '0001-01-01 00:00:00');

DELETE FROM `item_itemgroup`;
INSERT INTO `item_itemgroup` (`id`, `date_created`, `date_deleted`, `item_id`, `itemgroup_id`)
VALUES (1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, 1),
    (2, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 2, 2),
    (3, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 3, 1),
    (4, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 3, 2),
    (5, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 4, 1),
    (6, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 4, 2),
    (7, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 5, 1),
    (8, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 5, 2),
    (9, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 6, 1),
    (10, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 6, 2),
    (11, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 7, 1),
    (12, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 7, 2),
    (13, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 8, 1),
    (14, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 8, 2),
    (15, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 9, 1),
    (16, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 9, 2),
    (17, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 10, 1),
    (18, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 10, 2),
    (19, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 11, 1),
    (20, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 11, 2),
    (21, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 12, 1),
    (22, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 12, 2),
    (23, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 13, 1),
    (24, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 13, 2);

DELETE FROM `item_itemgroupentry`;
INSERT INTO `item_itemgroupentry` (`id`, `date_created`, `date_deleted`, `item_id`, `itemgroupentry_id`, `itemgroup_id`)
VALUES (1, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 3, 1, 1),
    (2, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 3, 2, 2),
    (3, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 4, 1, 1),
    (4, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 4, 3, 2),
    (5, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 5, 1, 1),
    (6, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 5, 4, 2),
    (7, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 6, 1, 1),
    (8, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 6, 5, 2),
    (9, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 7, 6, 1),
    (10, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 7, 2, 2),
    (11, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 8, 6, 1),
    (12, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 8, 3, 2),
    (13, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 9, 6, 1),
    (14, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 9, 4, 2),
    (15, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 10, 6, 1),
    (16, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 10, 5, 2),
    (17, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 11, 7, 1),
    (18, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 11, 2, 2),
    (19, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 12, 7, 1),
    (20, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 12, 3, 2),
    (21, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 13, 7, 1),
    (22, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 13, 4, 2),
    (23, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 14, 7, 1),
    (24, '2019-06-07 14:53:03', '0001-01-01 00:00:00', 14, 5, 2);

DELETE FROM `itemtext`;
INSERT INTO `itemtext` (`id`, `title`, `date_created`, `date_deleted`, `item_id`, `language_id`)
VALUES (1, 'Demo Hoodie', '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, 1),
(2, 'Demo Kaputzenpullover', '2019-06-07 14:53:02', '0001-01-01 00:00:00', 2, 2),
(3, 'Demo Hoodie Blue S', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 3, 1),
(4, 'Demo Kaputzenpullover Blau S', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 3, 2),
(5, 'Demo Hoodie Blue M', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 4, 1),
(6, 'Demo Kaputzenpullover Blau M', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 4, 2),
(7, 'Demo Hoodie Blue L', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 5, 1),
(8, 'Demo Kaputzenpullover Blau L', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 5, 2),
(9, 'Demo Hoodie Blue XL', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 6, 1),
(10, 'Demo Kaputzenpullover Blau XL', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 6, 2),
(11, 'Demo Hoodie Green S', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 7, 1),
(12, 'Demo Kaputzenpullover Grün S', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 7, 2),
(13, 'Demo Hoodie Green M', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 8, 1),
(14, 'Demo Kaputzenpullover Grün M', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 8, 2),
(15, 'Demo Hoodie Green L', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 9, 1),
(16, 'Demo Kaputzenpullover Grün L', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 9, 2),
(17, 'Demo Hoodie Green XL', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 10, 1),
(18, 'Demo Kaputzenpullover Grün XL', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 10, 2),
(19, 'Demo Hoodie Red S', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 11, 1),
(20, 'Demo Kaputzenpullover Rot S', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 11, 2),
(21, 'Demo Hoodie Red M', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 12, 1),
(22, 'Demo Kaputzenpullover Rot M', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 12, 2),
(23, 'Demo Hoodie Rot L', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 13, 1),
(24, 'Demo Kaputzenpullover Red L', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 13, 2),
(25, 'Demo Hoodie Red XL', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 14, 1),
(26, 'Demo Kaputzenpullover Red XL', '2019-06-07 14:53:03', '0001-01-01 00:00:00', 14, 2);
