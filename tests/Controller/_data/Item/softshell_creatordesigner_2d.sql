INSERT IGNORE INTO `item` (`id`, `identifier`, `minimum_order_amount`, `accumulate_amounts`, `date_created`, `date_updated`, `date_deleted`, `visualization_mode_id`, `item_status_id`, `configuration_mode`)
VALUES
(3003, 'softshell_creatordesigner_2d', 1, 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00', 1, 2, 'creator+designer');

INSERT INTO `configuration` (`id`, `configurationtype_id`, `item_id`, `client_id`, `externaluser_id`, `code`, `name`, `selectedoptions`, `designdata`, `customdata`, `partslisthash`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3002, 2, 3003, NULL, NULL, '960XCH2HN', NULL, '{\"softshell_body\":[{\"identifier\":\"softshell_red\",\"amount\":1}],\"softshell_hood\":[{\"identifier\":\"softshell_red\",\"amount\":1}],\"softshell_zipper\":[{\"identifier\":\"softshell_red\",\"amount\":1}],\"softshell_application\":[{\"identifier\":\"softshell_redblack\",\"amount\":1}]}', NULL, NULL, NULL, '2021-07-02 10:38:43', '2021-07-02 10:38:43', '0001-01-01 00:00:00');


INSERT INTO `itemprice` (`id`, `channel_id`, `item_id`, `price`, `price_net`, `amountfrom`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3005, 1, 3003, 59.00000, 0.00000, 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3006, 1, 3003, 54.00000, 0.00000, 10, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');

INSERT INTO `optionclassification` (`id`, `identifier`, `sequencenumber`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3006, 'softshell_creatordesigner_body', 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3007, 'softshell_creatordesigner_hood', 2, '2021-06-30 11:05:30', '2021-06-30 11:05:30', '0001-01-01 00:00:00'),
(3008, 'softshell_creatordesigner_zipper', 3, '2021-06-30 11:05:33', '2021-06-30 11:05:33', '0001-01-01 00:00:00'),
(3009, 'softshell_creatordesigner_application', 4, '2021-06-30 11:05:35', '2021-06-30 11:05:35', '0001-01-01 00:00:00');

INSERT INTO `option` (`id`, `sequencenumber`, `identifier`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3016, 1, 'softshell_creatordesigner_red', '2021-06-30 11:05:27', '2021-06-30 11:05:33', '0001-01-01 00:00:00'),
(3017, 2, 'softshell_creatordesigner_avocado', '2021-06-30 11:05:27', '2021-06-30 11:05:34', '0001-01-01 00:00:00'),
(3022, 1, 'softshell_creatordesigner_redblack', '2021-06-30 11:05:35', '2021-06-30 11:05:35', '0001-01-01 00:00:00'),
(3023, 2, 'softshell_creatordesigner_whitegreen', '2021-06-30 11:05:35', '2021-06-30 11:05:35', '0001-01-01 00:00:00');

INSERT INTO `item_optionclassification` (`id`, `item_id`, `optionclassification_id`, `is_mandatory`, `date_created`, `date_updated`, `date_deleted`, `sequence_number`, `creator_view_id`)
VALUES
(3006, 3003, 3006, 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00', 1, 3002),
(3007, 3003, 3007, 1, '2021-06-30 11:05:30', '2021-06-30 11:05:30', '0001-01-01 00:00:00', 2, null),
(3008, 3003, 3008, 1, '2021-06-30 11:05:33', '2021-06-30 11:05:33', '0001-01-01 00:00:00', 3, null),
(3009, 3003, 3009, 1, '2021-06-30 11:05:35', '2021-06-30 11:05:35', '0001-01-01 00:00:00', 4, null);

INSERT INTO `item_optionclassification_option` (`id`, `item_optionclassification_id`, `option_id`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3016, 3006, 3016, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3017, 3006, 3017, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3018, 3007, 3016, '2021-06-30 11:05:30', '2021-06-30 11:05:30', '0001-01-01 00:00:00'),
(3019, 3007, 3017, '2021-06-30 11:05:31', '2021-06-30 11:05:31', '0001-01-01 00:00:00'),
(3020, 3008, 3016, '2021-06-30 11:05:33', '2021-06-30 11:05:33', '0001-01-01 00:00:00'),
(3021, 3008, 3017, '2021-06-30 11:05:34', '2021-06-30 11:05:34', '0001-01-01 00:00:00'),
(3022, 3009, 3022, '2021-06-30 11:05:35', '2021-06-30 11:05:35', '0001-01-01 00:00:00'),
(3023, 3009, 3023, '2021-06-30 11:05:35', '2021-06-30 11:05:35', '0001-01-01 00:00:00');

INSERT INTO `design_area` (`id`, `item_id`, `identifier`, `sequence_number`, `height`, `width`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3002, 3003, 'chest', 1, 100, 100, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3003, 3003, 'sleeve', 2, 100, 140, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3004, 3003, 'back_center', 3, 500, 400, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');

INSERT INTO `design_area_text` (`id`, `design_area_id`, `language_id`, `title`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3003, 3002, 1, 'Chest', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3004, 3002, 2, 'Brust', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3005, 3003, 1, 'Sleeve', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3006, 3003, 2, 'Ärmel', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3007, 3004, 1, 'Back center', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3008, 3004, 2, 'Rückseite Mitte', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');


INSERT INTO `creator_view` (`id`, `item_id`, `identifier`, `sequence_number`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3002, 3003, 'front_view', 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3003, 3003, 'side_view', 2, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3004, 3003, 'back_view', 3, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');

INSERT INTO `creator_view_design_area` (`id`, `design_area_id`, `creator_view_id`, `base_shape`, `position`, `custom_data`, `is_default`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3001, 3002, 3002, '{\"type\":\"Plane\",\"width\":144,\"height\":144,\"x\":-179.99999999999997,\"y\":-68.18181818181799,\"rotation\":{\"x\":0,\"y\":0,\"z\":0,\"order\":\"XYZ\"}}', '{\"x\":72,\"y\":72,\"width\":144,\"height\":144}', NULL, 0, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3002, 3003, 3002, '{\"type\":\"Cylinder\",\"width\":173,\"height\":124,\"x\":286,\"y\":-16,\"rotation\":{\"x\":0.1213,\"y\":0.6705,\"z\":0.17792256549810162,\"order\":\"XYZ\"},\"r\":64}', '{\"x\":86,\"y\":62,\"width\":172,\"height\":123}', NULL, 0, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3003, 3003, 3003, '{\"type\":\"Cylinder\",\"width\":101,\"height\":72,\"x\":330.0000000000003,\"y\":21.81818181818182,\"r\":128,\"rotation\":{\"x\":9.02129208547617e-17,\"y\":0.012721785742363836,\"z\":0.2679774739697903,\"order\":\"XYZ\"},\"inside\":false}', '{\"x\":50,\"y\":36,\"width\":71,\"height\":100}', NULL, 0, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3004, 3004, 3004, '{\"type\":\"Plane\",\"width\":396,\"height\":495,\"x\":-87,\"y\":-100,\"rotation\":{\"x\":0,\"y\":0,\"z\":0,\"order\":\"XYZ\"}}', '{\"x\":198,\"y\":248,\"width\":395,\"height\":494}', NULL, 0, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');

INSERT INTO `creator_view_text` (`id`, `language_id`, `creator_view_id`, `title`, `date_created`, `date_updated`, `date_deleted`)
VALUES
(3001, 1, 3002, 'Front view', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3002, 2, 3002, 'Vorderansicht', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3005, 1, 3003, 'Side view', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3006, 2, 3003, 'Seitenansicht', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3007, 1, 3004, 'Back view', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
(3008, 2, 3004, 'Rückansicht', '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');
