INSERT INTO `item` (`identifier`, `externalid`, `configurable`, `deactivated`, `minimum_order_amount`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `parent_id`, `configuration_mode`)
VALUES ('demo_pen', NULL, NULL, false, 500, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 'designer'),
('S-012192103920', NULL, NULL, false, 500, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 15, NULL),
('S-012192103924', NULL, NULL, false, 500, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 15, NULL),
('S-012192103927', NULL, NULL, false, 500, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 15, NULL),
('S-012192103934', NULL, NULL, false, 500, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, 15, NULL);

INSERT INTO `design_area` (`identifier`, `sequence_number`, `height`, `width`, `custom_data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `option_id`, `mask`)
VALUES ('a_1', 1, 45, 20, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `item` WHERE `identifier` = 'demo_pen'), NULL, NULL),
('a_2', 2, 50, 12, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `item` WHERE `identifier` = 'demo_pen'), NULL, NULL),
('c_1', 3, 35, 7, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `item` WHERE `identifier` = 'demo_pen'), NULL, NULL),
('c_2', 4, 40, 5, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `item` WHERE `identifier` = 'demo_pen'), NULL, NULL),
('d', 5, 60, 35, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `item` WHERE `identifier` = 'demo_pen'), NULL, NULL);

INSERT INTO `design_area_text` (`title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_area_id`, `language_id`)
VALUES ('Shaft 1', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_1'), 1),
('Shaft 1', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_1'), 2),
('Shaft 2', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_2'), 1),
('Shaft 2', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_2'), 2),
('Clip 1', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_1'), 1),
('Clip 1', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_1'), 2),
('Clip 2', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_2'), 1),
('Clip 2', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_2'), 2),
('360°', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'd'), 1),
('360°', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'd'), 2);

INSERT INTO `design_area_design_production_method` (`width`, `height`, `additional_data`, `custom_data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_area_id`, `design_production_method_id`, `allow_bulk_names`, `mask`, `is_default`, `default_colors`, `design_elements_locked`, `max_elements`, `max_images`, `max_texts` )
VALUES (NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_1'), 1, 0, NULL, 0, NULL, 1, 4, 1, 3),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_1'), 2, 0, NULL, 0, NULL, 0, 4, 2, 2),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_2'), 1, 0, NULL, 0, NULL, 1, 4, NULL, NULL),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_2'), 2, 0, NULL, 0, NULL, 0, 4, 1, 3),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_1'), 1, 0, NULL, 0, NULL, 1, 4, 1, 3),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_1'), 2, 0, NULL, 0, NULL, 0, 4, 2, 2),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_2'), 1, 0, NULL, 0, NULL, 1, 4, NULL, NULL),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_2'), 2, 0, NULL, 0, NULL, 0, 4, 1, 3),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'd'), 1, 0, NULL, 0, NULL, 1, 4, 1, 3),
(NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'd'), 2, 0, NULL, 0, NULL, 1, 4, 1, 3);

INSERT INTO `design_view` (`identifier`, `sequence_number`, `custom_data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `option_id`)
VALUES ('3d', 1, '{\"selectedCameraView\":\"A_1\"}', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `item` WHERE `identifier` = 'demo_pen'), NULL);

INSERT INTO `design_view_text` (`title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_view_id`, `language_id`)
VALUES ('3D', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_view` WHERE `identifier` = '3d'), 1),
('3D', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_view` WHERE `identifier` = '3d'), 2);

INSERT INTO `design_view_design_area` (`base_shape`, `position`, `custom_data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_view_id`, `design_area_id`, `is_default`)
VALUES ('{\"type\":\"Plane\"}', '{\"x\":990,\"y\":844,\"width\":876,\"height\":390}', '{\"selectedCameraView\":\"A_1\"}', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_view` WHERE `identifier` = '3d'), (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_1'), 0),
('{\"type\":\"Plane\"}', '{\"x\":536,\"y\":645,\"width\":759,\"height\":182}', '{\"0\":\"n\",\"1\":\"u\",\"2\":\"l\",\"3\":\"l\",\"selectedCameraView\":\"A_2\"}', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_view` WHERE `identifier` = '3d'), (SELECT `id` FROM `design_area` WHERE `identifier` = 'a_2'), 0),
('{\"type\":\"Plane\"}', '{\"x\":716,\"y\":291,\"width\":836,\"height\":168}', '{\"selectedCameraView\":\"A_1\"}', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_view` WHERE `identifier` = '3d'), (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_1'), 0),
('{\"type\":\"Plane\"}', '{\"x\":777,\"y\":276,\"width\":803,\"height\":100}', '{\"selectedCameraView\":\"A_1\"}', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_view` WHERE `identifier` = '3d'), (SELECT `id` FROM `design_area` WHERE `identifier` = 'c_2'), 0),
('{\"type\":\"Plane\"}', '{\"x\":937,\"y\":830,\"width\":1213,\"height\":707}', '{\"selectedCameraView\":\"A_1\"}', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_view` WHERE `identifier` = '3d'), (SELECT `id` FROM `design_area` WHERE `identifier` = 'd'), 0);

UPDATE color SET sequence_number = 2 WHERE id = 1;
UPDATE color SET sequence_number = 1 WHERE id = 2;
UPDATE color SET sequence_number = id WHERE id NOT IN(1, 2);
