INSERT INTO `item` (`identifier`, `externalid`, `configurable`, `deactivated`, `minimum_order_amount`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `parent_id`, `configuration_mode`)
VALUES ('demo_calendar', NULL, NULL, false, 1, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 'designer');

INSERT INTO `design_area` (`identifier`, `sequence_number`, `height`, `width`, `custom_data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `option_id`, `mask`)
VALUES ('front_complete', 1, 355, 265, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `item` WHERE `identifier` = 'demo_calendar'), NULL, NULL);

INSERT INTO `design_area_design_production_method` (`width`, `height`, `additional_data`, `custom_data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_area_id`, `design_production_method_id`, `allow_bulk_names`, `mask`, `is_default`, `default_colors`)
VALUES (NULL, NULL, NULL, NULL, NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'front_complete'), 1, 0, NULL, 0, NULL);

INSERT INTO `design_area_text` (`title`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `design_area_id`, `language_id`)
VALUES ('Front center', NOW(), NOW(), '0001-01-01 00:00:00', NULL, NULL, NULL, (SELECT `id` FROM `design_area` WHERE `identifier` = 'front_complete'), 1);
