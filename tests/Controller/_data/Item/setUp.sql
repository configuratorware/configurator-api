INSERT INTO itemgroup(id, identifier, sequencenumber, configurationvariant, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (5005, 'shape', 2, 0, '2019-01-30 15:30:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO itemgroupentry(id, identifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (1, 'red', 1, '2019-01-30 15:30:28', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(2, 'blue', 2, '2019-01-30 15:30:36', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(3, 's', 3, '2019-01-30 15:30:56', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(4, 'm', 4, '2019-01-30 15:31:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(5, 'l', 5, '2019-01-30 15:31:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6, 'round', 6, '2019-01-30 15:31:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO itemgroupentrytranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, itemgroupentry_id, language_id)
VALUES (1, 'Red', '2019-01-30 15:36:58', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
(2, 'Rot', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
(3, 'Blue', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
(4, 'Blau', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2),
(5, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 1),
(6, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 3, 2),
(7, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 4, 1),
(8, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 4, 2),
(9, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5, 1),
(10, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5, 2);

INSERT INTO item(id, identifier, accumulate_amounts, configurable, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id, visualization_mode_id, item_status_id)
VALUES (5005, 'hoodie', 1, NULL, false, '2019-02-01 11:00:38', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2, 2),
(5006, 'hoodie_red_s', 1, NULL, false, '2019-02-01 11:00:57', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL),
(5007, 'hoodie_red_m', 1, NULL, false, '2019-02-01 11:01:12', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL),
(5008, 'hoodie_red_l', 1, NULL, false, '2019-02-01 11:01:19', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL),
(5009, 'hoodie_blue_s', 1, NULL, false, '2019-02-01 11:01:32', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL),
(5010, 'hoodie_blue_m', 1, NULL, false, '2019-02-01 11:01:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL),
(5011, 'hoodie_blue_l', 1, NULL, false, '2019-02-01 11:01:45', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL);

INSERT INTO configuration(id, externaluser_id, code, name, selectedoptions, designdata, customdata, partslisthash, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, configurationtype_id, item_id)
VALUES (5005, NULL, 'default_hoodie', 'default_hoodie', '{}', NULL, NULL, NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5005),
(5006, NULL, 'default_hoodie_red_s', 'default_hoodie', '{}', NULL, NULL, NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5006),
(5007, NULL, 'default_hoodie_red_m', 'default_hoodie', '{}', NULL, NULL, NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5007),
(5008, NULL, 'default_hoodie_red_l', 'default_hoodie', '{}', NULL, NULL, NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5008),
(5009, NULL, 'default_hoodie_blue_s', 'default_hoodie', '{}', NULL, NULL, NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5009),
(5010, NULL, 'default_hoodie_blue_m', 'default_hoodie', '{}', NULL, NULL, NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5010),
(5011, NULL, 'default_hoodie_blue_l', 'default_hoodie', '{}', NULL, NULL, NULL, '2019-02-07 09:05:15', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 5011);

INSERT INTO item_itemgroup(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, itemgroup_id)
VALUES (5000, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 1),
(5001, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 2),
(5002, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 1),
(5003, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 2),
(5004, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 1),
(5005, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 2),
(5006, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 1),
(5007, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 2),
(5008, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 1),
(5009, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 2),
(5010, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 1),
(5011, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 2);

INSERT INTO item_itemgroupentry(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, itemgroupentry_id, itemgroup_id)
VALUES (5000, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 1, 1),
(5001, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5006, 3, 2),
(5002, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 1, 1),
(5003, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5007, 4, 2),
(5004, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 1, 1),
(5005, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5008, 5, 2),
(5006, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 2, 1),
(5007, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5009, 3, 2),
(5008, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 2, 1),
(5009, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5010, 4, 2),
(5010, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 2, 1),
(5011, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5011, 5, 2);
