INSERT INTO `configuration` (`id`, `externaluser_id`, `code`, `name`, `selectedoptions`, `designdata`, `customdata`, `partslisthash`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `configurationtype_id`, `item_id`)
VALUES (6005, NULL, 'default_hoodie', 'default_hoodie', '{}', '{\"front\":{\"canvasData\":{\"objects\":[{\"type\":\"Text\",\"x\":472.5,\"y\":472.5,\"rotation\":326.64767082111814,\"scaling\":4.723037981538016,\"content\":\"<div style=\\\"font-family: Arial, Helvetica, sans-serif;\\\"><span style=\\\"font-size: 18px; font-family: \\\"Comic Sans MS\\\", cursive, sans-serif; font-style: italic; font-weight: bold; color: rgb(0, 120, 184);\\\" data-color=\\\"blue\\\">My <span data-color=\\\"red\\\" style=\\\"color: rgb(247, 52, 41);\\\">Text</span></span></div>\",\"style\":{\"colors\":[{\"identifier\":\"blue\",\"value\":\"rgb(0, 120, 184)\"},{\"identifier\":\"red\",\"value\":\"rgb(247, 52, 41)\"}],\"fontSize\":\"18px\",\"fontFamily\":\"\\\"Comic Sans MS\\\", cursive, sans-serif\",\"fontWeight\":\"bold\",\"fontStyle\":\"italic\"},\"metric\":{\"x\":40,\"y\":40,\"width\":30,\"height\":8}},{\"type\":\"Text\",\"x\":100,\"y\":100,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"content\":\"<span style=\\\"font-size: 16px\\\">My Text</span>\"},{\"type\":\"Image\",\"x\":40,\"y\":80,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"content\":\"<span style=\\\"font-size: 16px\\\">My Text</span>\"}]},\"designProductionMethodIdentifier\":\"print\"},\"sleeve\":{\"canvasData\":{\"objects\":[{\"type\":\"Text\",\"x\":148,\"y\":118.5,\"rotation\":18.65126572445655,\"scaling\":6.4146347478286705,\"content\":\"<span style=\\\"font-size: 16px\\\">My Text</span>\"}]},\"designProductionMethodIdentifier\":\"print\", \"colorAmount\":5}}', '{\"selectedAmounts\":{\"hoodie_blue_s\":999,\"hoodie_red_m\":88},\"userSelectedCalculation\":{\"global_gift_packaging\":true}}', NULL, '2020-01-01 00:00:00', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 6005),
(6006, NULL, 'no_conf_hoodie', 'default_hoodie', '{}', '', '{\"selectedAmounts\":{\"hoodie\":2}}', NULL, '2020-01-01 00:00:00', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 6005);

INSERT INTO item(id, identifier, externalid, configurable, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id, item_status_id)
VALUES (6005, 'hoodie', NULL, NULL, 0, '2019-02-01 11:00:38', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2),
(6006, 'hoodie_red_s', NULL, NULL, 0, '2019-02-01 11:00:57', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, NULL),
(6007, 'hoodie_red_m', NULL, NULL, 0, '2019-02-01 11:01:12', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, NULL),
(6008, 'hoodie_red_l', NULL, NULL, 0, '2019-02-01 11:01:19', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, NULL),
(6009, 'hoodie_blue_s', NULL, NULL, 0, '2019-02-01 11:01:32', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, NULL),
(6010, 'hoodie_blue_m', NULL, NULL, 0, '2019-02-01 11:01:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, NULL),
(6011, 'hoodie_blue_l', NULL, NULL, 0, '2019-02-01 11:01:45', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, NULL);

INSERT INTO itemgroup(id, identifier, sequencenumber, configurationvariant, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (6001, '6000color', 1, 0, '2019-01-30 15:30:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6002, '6000size', 2, 0, '2019-01-30 15:30:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6003, '6000shape', 2, 0, '2019-01-30 15:30:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO itemgrouptranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, itemgroup_id, language_id)
VALUES (6001, 'Color', '2019-01-30 15:34:13', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6001, 1),
(6002, 'Farbe', '2019-01-30 15:34:20', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6001, 2),
(6003, 'Size', '2019-01-30 15:34:29', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6002, 1),
(6004, 'Größe', '2019-01-30 15:34:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6002, 2);

INSERT INTO itemgroupentry(id, identifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (6001, '6000red', 1, '2019-01-30 15:30:28', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6002, '6000blue', 2, '2019-01-30 15:30:36', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6003, '6000s', 3, '2019-01-30 15:30:56', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6004, '6000m', 4, '2019-01-30 15:31:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6005, '6000l', 5, '2019-01-30 15:31:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
(6006, '6000round', 6, '2019-01-30 15:31:07', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO itemgroupentrytranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, itemgroupentry_id, language_id)
VALUES (6001, 'Red', '2019-01-30 15:36:58', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6001, 1),
(6002, 'Rot', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6001, 2),
(6003, 'Blue', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6002, 1),
(6004, 'Blau', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6002, 2),
(6005, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6003, 1),
(6006, 'S', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6003, 2),
(6007, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6004, 1),
(6008, 'M', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6004, 2),
(6009, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, 1),
(6010, 'L', '2019-01-30 15:37:05', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6005, 2);

INSERT INTO item_itemgroup(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, itemgroup_id)
VALUES (6000, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6006, 1),
(6001, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6006, 2),
(6002, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6007, 1),
(6003, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6007, 2),
(6004, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6008, 1),
(6005, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6008, 2),
(6006, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6009, 1),
(6007, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6009, 2),
(6008, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6010, 1),
(6009, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6010, 2),
(6010, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6011, 1),
(6011, '2019-02-01 11:06:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6011, 2);

INSERT INTO item_itemgroupentry(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, itemgroupentry_id, itemgroup_id)
VALUES (6000, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6006, 6001, 6001),
(6001, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6006, 6003, 6002),
(6002, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6007, 6001, 6001),
(6003, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6007, 6004, 6002),
(6004, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6008, 6001, 6001),
(6005, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6008, 6005, 6002),
(6006, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6009, 6002, 6001),
(6007, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6009, 6003, 6002),
(6008, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6010, 6002, 6001),
(6009, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6010, 6004, 6002),
(6010, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6011, 6002, 6001),
(6011, '2019-02-01 11:10:41', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6011, 6005, 6002);

INSERT INTO `item_configuration_mode_item_status` (`id`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `item_status_id`, `configuration_mode_id`)
VALUES (6005, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 6005, 2, 2),
    (6006, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 6006, 2, 2),
    (6007, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 6007, 2, 2),
    (6008, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 6008, 2, 2),
    (6009, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 6009, 2, 2),
    (6010, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 6010, 2, 2),
    (6011, NOW(), NULL, '0001-01-01 00:00:00', 1, NULL, NULL, 6011, 2, 2);

INSERT INTO itemprice(price, price_net, amountfrom, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, channel_id, item_id)
VALUES (9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6005),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6005),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6006),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6006),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6007),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6007),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6008),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6008),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6009),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6009),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6010),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6010),
(9.99, 8.39, 1, '2019-02-04 15:04:22', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6011),
(8.99, 7.55, 100, '2019-02-04 15:06:55', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 6011);
