<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Controller;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * @MockPaths
 */
class FontsControllerTest extends ApiTestCase
{
    use VfsTestTrait;

    protected FontRepository $repository;

    protected string $fontDir;

    protected vfsStreamDirectory $root;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(Font::class);
        $this->root = vfsStream::setup('root', null, [
            'designer' => ['fonts' => []],
        ]);
        $this->fontDir = $this->root->url() . DIRECTORY_SEPARATOR . '/designer/fonts';

        $this->executeSql(file_get_contents(__DIR__ . '/_data/Font/setUp.sql'));
    }

    public function testFrontendDesignerList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/frontendapi/designer/fonts');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $expected = file_get_contents(__DIR__ . '/_data/Font/frontendDesignerList.json');
        $this->compareResponseContent($response, $expected);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/fonts');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $expected = file_get_contents(__DIR__ . '/_data/Font/adminFontList.json');
        $this->compareResponseContent($response, $expected);
    }

    public function testShouldListAllUnpaginatedFonts()
    {
        $sql = file_get_contents(__DIR__ . '/_data/Font/all_fonts.sql');
        $this->executeSql($sql);

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/fonts');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $content = json_decode($response->getContent(), true);

        self::assertEquals(15, $content['metadata']['totalcount']);
        self::assertCount(15, $content['data']);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/fonts/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $expected = file_get_contents(__DIR__ . '/_data/Font/detail.json');
        $this->compareResponseContent($response, $expected);
    }

    public function testCreate()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/Font/create.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/fonts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->repository->findOneBy(['name' => 'test_create']);

        self::assertInstanceOf(
            Font::class,
            $testInstance,
            'Font has not been created.'
        );
    }

    public function testUpdate()
    {
        $this->setUpFontFiles();
        $postdata = file_get_contents(__DIR__ . '/_data/Font/update.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/fonts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $testInstance = $this->repository->findOneBy(['id' => 1]);

        self::assertEquals(
            !false,
            strpos($testInstance->getName(), 'changed'),
            'Value has not been changed'
        );

        self::assertEquals(
            true,
            $testInstance->getIsDefault(),
            'is default was no saved'
        );

        $fontTimes = $this->repository->findOneBy(['id' => 2]);

        self::assertEquals(
            false,
            $fontTimes->getIsDefault(),
            'is default has not been reset for other font'
        );

        self::assertTrue($this->root->hasChild('designer/fonts/OpenSans_changed-Bold.ttf'));
        self::assertTrue($this->root->hasChild('designer/fonts/OpenSans_changed-Italic.ttf'));
        self::assertTrue($this->root->hasChild('designer/fonts/OpenSans_changed-Regular.ttf'));
    }

    public function testShouldUpdateFontSorting()
    {
        $postdata = file_get_contents(__DIR__ . '/_data/Font/updateSequence.json');

        $client = $this->createAuthenticatedClient();
        $uri = '/api/fonts/savesequencenumbers';
        $headers = ['CONTENT_TYPE' => 'application/json'];

        $client->request(Request::METHOD_POST, $uri, [], [], $headers, $postdata);
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $expected = [
            ['id' => '1', 'sequencenumber' => '5'],
            ['id' => '2', 'sequencenumber' => '4'],
            ['id' => '3', 'sequencenumber' => '3'],
            ['id' => '4', 'sequencenumber' => '2'],
            ['id' => '5', 'sequencenumber' => '1'],
        ];

        $actual = array_map(static function (Font $font) {
            return ['id' => $font->getId(), 'sequencenumber' => $font->getSequenceNumber()];
        }, $this->repository->findAll());

        self::assertEquals($expected, $actual);
    }

    public function testRemoveFile()
    {
        $sourceDir = __DIR__ . '/_data/Font/Open_Sans/';
        $fileName = 'OpenSans-Regular.ttf';

        copy($sourceDir . $fileName, $this->fontDir . DIRECTORY_SEPARATOR . $fileName);

        $postdata = file_get_contents(__DIR__ . '/_data/Font/removeFile.json');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/fonts',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $file = file_exists($this->fontDir . DIRECTORY_SEPARATOR . $fileName);

        self::assertFalse($file, 'File has not been removed');
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', 'api/fonts/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $deleted = $this->repository->findOneBy(['id' => 1]);

        self::assertNull($deleted, 'Not all Fonts have been marked as date_deleted.');
    }

    public function testFontFileUpload()
    {
        $fileName = 'OpenSans-Regular.ttf';
        $sourcePath = __DIR__ . '/_data/Font/Open_Sans/' . $fileName;
        $targetPath = $this->root->url() . DIRECTORY_SEPARATOR . $fileName;

        copy($sourcePath, $targetPath);

        $fontFile = new UploadedFile(
            $targetPath,
            $fileName,
            'font/ttf',
            null
        );

        $client = $this->createAuthenticatedClient();
        $client->insulate(false);

        $client->request(
            'POST',
            '/api/fonts/upload',
            [],
            ['font' => $fontFile]
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $file = file_exists($this->fontDir . DIRECTORY_SEPARATOR . $fileName);
        self::assertTrue($file, 'File has not been created.');
    }

    private function setUpFontFiles(): void
    {
        $workingDir = __DIR__ . '/_data/Font/Open_Sans/';
        $fileNameRegular = 'OpenSans-Regular.ttf';
        $fileNameBold = 'OpenSans-Bold.ttf';
        $fileNameBoldItalic = 'OpenSans-BoldItalic.ttf';
        $fileNameItalic = 'OpenSans-Italic.ttf';

        copy($workingDir . $fileNameRegular, $this->fontDir . DIRECTORY_SEPARATOR . $fileNameRegular);
        copy($workingDir . $fileNameBold, $this->fontDir . DIRECTORY_SEPARATOR . $fileNameBold);
        copy($workingDir . $fileNameBoldItalic, $this->fontDir . DIRECTORY_SEPARATOR . $fileNameBoldItalic);
        copy($workingDir . $fileNameItalic, $this->fontDir . DIRECTORY_SEPARATOR . $fileNameItalic);
    }
}
