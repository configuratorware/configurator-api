<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\Codesnippet;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CodesnippetsControllerTest extends ApiTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(Codesnippet::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Codesnippet/setUp.sql'));
    }

    public function testFrontendGetCurrent()
    {
        $client = self::createClient();

        $client->request('GET', '/frontendapi/codesnippets/getcurrent');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();

        $client->request('GET', '/api/codesnippets');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testListRestrictedToClientUser()
    {
        $client = $this->createAuthenticatedClient('rhm');

        $client->request('GET', '/api/codesnippets');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testDetailClientAllowed()
    {
        $client = $this->createAuthenticatedClient('rhm');

        $client->request('GET', '/api/codesnippets/2');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testDetailClientForbidden()
    {
        $client = $this->createAuthenticatedClient('rhm');

        $client->request('GET', '/api/codesnippets/1');

        self::assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode(), 'HTTP status code not ok');
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient('admin');
        $client->request('GET', '/api/codesnippets/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @param string $body
     * @dataProvider saveClientAllowedDataProvider
     */
    public function testSaveClientAllowed(string $body)
    {
        $client = $this->createAuthenticatedClient('rhm');

        $client->request(
            'POST',
            '/api/codesnippets',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $body
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $codeSnippet = $this->repository->findOneByIdentifier(json_decode($body)->identifier);

        self::assertInstanceOf(Codesnippet::class, $codeSnippet);
        self::assertInstanceOf(Client::class, $codeSnippet->getClient());
    }

    public function saveClientAllowedDataProvider()
    {
        return [
            // create new one without client (-> will get automatically added):
            ['{"identifier":"new_client_code_snippet","code":"<!-- This is a new code snippet updated -->"}'],
            // create new one without with  correct client:
            ['{"identifier":"new_client_code_snippet","code":"<!-- This is a new code snippet updated -->", "client": {"id":2}}'],
            // modify existing one with correct client
            ['{"id":2, "identifier":"test_client","code":"<!-- This is a new code snippet updated -->", "client": {"id":2}}'],
        ];
    }

    /**
     * @param string $body
     * @dataProvider saveClientForbiddenDataProvider
     */
    public function testSaveClientForbidden(string $body)
    {
        $client = $this->createAuthenticatedClient('rhm');

        $client->request(
            'POST',
            '/api/codesnippets',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $body
        );

        $response = $client->getResponse();
        self::assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode(), 'HTTP status code not ok');
    }

    public function saveClientForbiddenDataProvider()
    {
        return [
            // existing one without client:
            ['{"id":1, "identifier":"new_client_code_snippet","code":"<!-- This is a new code snippet updated -->"}'],
            // try adding to not owned client:
            ['{"identifier":"new_client_code_snippet","code":"<!-- This is a new code snippet updated -->", "client": {"id":1}}'],
        ];
    }

    public function testSave()
    {
        $postdata = '{"identifier":"new_code_snippet","code":"<!-- This is a new code snippet updated -->","channel":{"id":1}}';

        $client = $this->createAuthenticatedClient('admin');
        $client->request(
            'POST',
            '/api/codesnippets',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $codeSnippet = $this->repository->findOneByIdentifier('new_code_snippet');

        self::assertInstanceOf(Codesnippet::class, $codeSnippet);

        $expectedResult = json_decode($postdata);
        $receivedResult = json_decode($response->getContent());

        self::assertEquals($expectedResult->code, $receivedResult->code, 'content mismatch');
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/codesnippets/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $entity = $this->repository->findOneById(1);

        self::assertEquals(null, $entity);
    }
}
