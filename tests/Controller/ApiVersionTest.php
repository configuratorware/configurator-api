<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ApiVersionTest extends ApiTestCase
{
    /**
     * Testing backend route for ApiVersionController.
     */
    public function testFrontendApiGetVersion(): void
    {
        $client = static::createClient();

        $client->request('GET', '/frontendapi/version');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * Testing backend route for ApiVersionController.
     */
    public function testBackendApiGetVersion(): void
    {
        $client = $this->createAuthenticatedClient();

        $client->request('GET', '/api/version');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }
}
