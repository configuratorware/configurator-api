<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class AttributeValuesControllerTest extends ApiTestCase
{
    /**
     * @var AttributevalueRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Attributevalue::class);
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/attributevalues');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"metadata":{"totalcount":"2"},"data":[{"id":"1","value":"vegetarian","translated_value"' .
            ':"vegetarian","translations":[{"id":"1","language":"en_GB","translation":"vegetarian"},{"id":"2","language":"de_DE","translation":"vegetarier"}]},' .
            '{"id":2,"value":"edible","translated_value":"edible","translations":[{"id":"3","language":"en_GB","translation":"edible"},{"id":"4","language":"de_DE",' .
            '"translation":"essbar"}]}]}');
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/attributevalues/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '{"id":"1","value":"vegetarian","translated_value":"vegetarian","translations":' .
            '[{"id":"1","language":"en_GB","translation":"vegetarian"},{"id":"2","language":"de_DE","translation":"vegetarier"}]}');
    }

    public function testSave(): void
    {
        $postdata = '{"value":"testvalue","translations":[{"language":"en_GB","translation":"testvalue"},{"language":"de_DE","translation":"Testvalue"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/attributevalues',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $attributevalue = $this->repository->findOneByValue('testvalue');

        self::assertInstanceOf(Attributevalue::class, $attributevalue);

        $expectedResult = json_decode($postdata);
        $receivedResult = json_decode($response->getContent());

        self::assertEquals($expectedResult->value, $receivedResult->value, 'content mismatch');
    }

    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/attributevalues/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertNull($this->repository->find(1));
    }
}
