<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemPool;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ConfigurationVariantsControllerTest extends ApiTestCase
{
    /** @var Repository */
    protected $repository;

    /** @var EntityManager */
    protected $em;

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ConfigurationVariantsControllerTest_setUp.sql'));

        $this->repository = $this->em->getRepository(ItemPool::class);
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/configurationvariants?orderby=itempool.identifier&orderdir=ASC');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testFindItems()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/configurationvariants/finditems');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/configurationvariants/4001');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testSave()
    {
        $postdata = '{"identifier":"configurationvariant_test_2","items":[4003,4004]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/configurationvariants',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        $itemGroup = $this->repository->findOneBy(['identifier' => 'configurationvariant_test_2']);
        self::assertInstanceOf(ItemPool::class, $itemGroup);
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/configurationvariants/4001');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertEquals(null, $this->repository->find(1));
    }

    public function testFrontendConfigurationVariantsShouldReturnEmptyArray()
    {
        $client = self::createClient();
        $client->request('GET', '/frontendapi/items/getconfigurationvariants/sheep');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent($response, '[]');
    }

    public function testFrontendConfigurationVariantsShouldReturn404()
    {
        $client = self::createClient();
        $client->request('GET', '/frontendapi/items/getconfigurationvariants/cow');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode(), 'HTTP status code not 404');
    }

    public function testFrontendConfigurationVariantsShouldReturnVariants()
    {
        $client = self::createClient();
        $client->request('GET', '/frontendapi/items/getconfigurationvariants/ashley');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not found');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }
}
