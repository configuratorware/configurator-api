<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use org\bovigo\vfs\vfsStream;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * @MockPaths
 */
class ConfigurationsControllerTest extends ApiTestCase
{
    use VfsTestTrait;

    /**
     * @var ConfigurationRepository
     */
    protected $repository;

    /**
     * @var string
     */
    protected $projectDir;

    /**
     * @var Filesystem
     */
    protected $fileSystem;

    /**
     * @var \org\bovigo\vfs\vfsStreamDirectory
     */
    private $vfsRoot;

    protected function setUp(): void
    {
        parent::setUp();

        // create base data to be able to run the test
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ConfigurationsControllerTest_configurationData.sql'));

        $this->repository = $this->em->getRepository(Configuration::class);

        $_SERVER['HTTP_HOST'] = 'testshost.tld';

        $this->projectDir = static::$kernel->getProjectDir();

        $this->fileSystem = new Filesystem();

        $this->vfsRoot = vfsStream::setup();
    }

    public function testGeneratedPrintFilesList(): void
    {
        $sqlUp = file_get_contents(__DIR__ . '/_data/Configuration/generatedPrintFilesListSetUp.sql');
        $this->executeSql($sqlUp);

        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/configurations/printfiles/default_hoodie');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $sqlDown = file_get_contents(__DIR__ . '/_data/Configuration/generatedPrintFilesListTearDown.sql');
        $this->executeSql($sqlDown);

        $this->compareResponseContent(
            $response,
            '[{"designAreaIdentifier":"front","printFilePath":"' . $this->projectDir . '\/tests\/Controller\/_data\/Configuration\/print_file_default_hoodie\/front.png"},{"designAreaIdentifier":"sleeve","printFilePath":"' . $this->projectDir . '\/tests\/Controller\/_data\/Configuration\/print_file_default_hoodie\/sleeve.png"}]'
        );
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/configurations');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals(2, $result->metadata->totalcount, 'JSON content mismatch');
        self::assertEquals('My first Sheep', $result->data[0]->name, 'JSON content mismatch');
        self::assertEquals('sheep', $result->data[0]->item->identifier, 'JSON content mismatch');
        self::assertNotEmpty($result->data[0]->optionclassifications, 'JSON content mismatch');
    }

    public function testClientList(): void
    {
        $client = $this->createAuthenticatedClient('rhm');
        $client->request('GET', '/api/configurations');
        $response = $client->getResponse();
        $configurations = json_decode($response->getContent(), false);

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertEquals(1, $configurations->metadata->totalcount, 'JSON content mismatch');
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/configurations/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('defaultoptions_sheep', $result->name, 'JSON content mismatch');
        self::assertEquals('sheep', $result->item->identifier, 'JSON content mismatch');
        self::assertNotEmpty($result->optionclassifications, 'JSON content mismatch');
    }

    public function testDetailClient(): void
    {
        $client = $this->createAuthenticatedClient('rhm');
        $client->request('GET', '/api/configurations/556');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
    }

    public function testDelete(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/configurations/555');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');
        self::assertEquals(null, $this->repository->find(555));
    }

    /**
     * @param string $route
     *
     * @dataProvider providerShouldSave
     */
    public function testSave(string $route): void
    {
        $post = '{"configuration":{"name":"Admin Configuration","item":{"identifier":"sheep"},"optionclassifications":[]},"configurationtype":"user"}';

        $client = $this->createAuthenticatedClient();
        $client->request('POST', $route, [], [], ['CONTENT_TYPE' => 'application/json'], $post);

        $response = $client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        /* @var $configuration Configuration */
        $configuration = $this->repository->findOneByName('Admin Configuration');
        self::assertInstanceOf(Configuration::class, $configuration);
    }

    public function providerShouldSave(): \Generator
    {
        yield ['/api/configurations'];
        yield ['/api/configurations/frontendsave'];
    }

    public function testFrontendSave(): void
    {
        $post = '{"configuration":{"name":"My third configuration","item":{"identifier":"sheep"},"optionclassifications":[{"is_multiselect":false,"selectedoptions":null,"identifier":"coat","title":"coat","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature"}],"identifier":"face","title":"face","description":"{face}"},{"is_multiselect":false,"selectedoptions":null,"identifier":"legs","title":"legs","description":"{legs}"},{"is_multiselect":false,"selectedoptions":null,"identifier":"eyes","title":"eyes","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":null,"identifier":"ear_eyelid","title":"ear & eyelid","description":"{ear_eyelid}"}]},"configurationtype":"user"}';

        $client = self::createClient();
        $client->request('POST', '/frontendapi/configurations', [], [], ['CONTENT_TYPE' => 'application/json'], $post);

        $response = $client->getResponse();
        self::assertEquals(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        /* @var $configuration Configuration */
        $configuration = $this->repository->findOneByName('My third configuration');
        self::assertInstanceOf(Configuration::class, $configuration);
        self::assertEquals('9af56dd722fcc16bb51a1767981480674fb31c94', $configuration->getPartslisthash(), 'Partslist configuration hash wrong or missing');
        self::assertEquals('_default', $configuration->getClient()->getIdentifier());
    }

    public function testFrontendSwitchOptions(): void
    {
        $postData = file_get_contents(__DIR__ . '/_data/ConfigurationsController/ConfigurationsControllerTest_switch_options.json');
        $client = self::createClient();
        $client->request('POST', '/frontendapi/configuration/switchoption', [], [], ['CONTENT_TYPE' => 'application/json'], $postData);

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testCouldNotFrontendSaveDefaultConfiguration(): void
    {
        $post = '{"configuration":{"name":"Bad Request Configuration","item":{"identifier":"sheep"},"optionclassifications":[]},"configurationtype":"defaultoptions"}';

        $client = self::createClient();
        $client->request('POST', '/frontendapi/configurations', [], [], ['CONTENT_TYPE' => 'application/json'], $post);

        $response = $client->getResponse();
        self::assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode(), 'HTTP status code not ok');
    }

    public function testCouldNotFrontendSaveExistingDefaultConfiguration(): void
    {
        $post = '{"configuration":{"code":"defaultoptions_1", "name":"Bad Request Configuration","item":{"identifier":"sheep"},"optionclassifications":[]},"configurationtype":"defaultoptions"}';

        $client = self::createClient();
        $client->request('POST', '/frontendapi/configurations', [], [], ['CONTENT_TYPE' => 'application/json'], $post);

        $response = $client->getResponse();
        self::assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode(), 'HTTP status code not ok');
    }

    /**
     * @param string $name
     * @param string $itemIdentifier
     * @param array $images
     *
     * @dataProvider frontendSaveWithFilesDataProvider
     */
    public function testFrontendSaveFiles(string $name, string $itemIdentifier, array $images): void
    {
        $client = self::createClient();
        $client->request(
            'POST',
            'frontendapi/configurations',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $this->getConfigurationJson($name, $itemIdentifier, $images)
        );

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $content = json_decode($response->getContent());

        $files = glob(__DIR__ . '/_data/media/images/configurations/' . $content->code . '/*/*.png');

        if (empty($files)) {
            $files = glob(__DIR__ . '/_data/media/images/configurations/' . $content->code . '/*.png');
        }

        self::assertNotEmpty($files, 'Files not created.');

        $this->fileSystem->remove(__DIR__ . '/_data/media/images/configurations/');
    }

    public function frontendSaveWithFilesDataProvider(): array
    {
        return [
            [
                'My new Configuration',
                'sheep',
                [
                    'item_1' => [
                        'view_1' => __DIR__ . '/_data/Configuration/logo_redhotmagma.png',
                        'view_2' => __DIR__ . '/_data/Configuration/logo_redhotmagma.png',
                    ],
                ],
            ],
            [
                'My new Configuration',
                'sheep',
                [
                    'view_1' => __DIR__ . '/_data/Configuration/logo_redhotmagma.png',
                    'view_2' => __DIR__ . '/_data/Configuration/logo_redhotmagma.png',
                ],
            ],
        ];
    }

    public function testLoadByItemIdentifier(): void
    {
        $client = static::createClient();
        $client->insulate();

        $client->request('GET', '/frontendapi/configuration/loadbyitemidentifier/sheep?_language=de_DE');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('defaultoptions_sheep', $result->name, 'JSON content mismatch');
        self::assertEquals('coat', $result->optionclassifications[0]->identifier, 'JSON content mismatch');
        self::assertEquals('Fell', $result->optionclassifications[0]->title, 'JSON content mismatch');
        self::assertEquals('defaultoptions', $result->configurationType, 'JSON content mismatch');
        self::assertEquals('addToCart', $result->item->callToAction, 'JSON content mismatch');
    }

    public function testLoadByItemIdentifierSwitchOption(): void
    {
        $client = static::createClient();
        $client->insulate();

        $client->request('GET', '/frontendapi/configuration/loadbyitemidentifier/sheep?_language=de_DE&&_switch_options=%7B"coat"%3A%5B%7B"identifier"%3A"coat_red"%2C%20"amount"%3A%201%7D%5D%7D');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('coat_red', $result->optionclassifications[0]->selectedoptions[0]->identifier, 'JSON content mismatch');
    }

    public function testLoadByItemIdentifierGroupedItems(): void
    {
        $sql = file_get_contents(__DIR__ . '/_data/Item/setUp.sql');
        $this->executeSql($sql);

        $client = static::createClient();

        $client->request('GET', '/frontendapi/configuration/loadbyitemidentifier/hoodie?_language=de_DE');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('color', $result->item->itemGroup->identifier, 'JSON content mismatch');
        self::assertEquals(
            'size',
            $result->item->itemGroup->children[0]->itemGroup->identifier,
            'JSON content mismatch'
        );
    }

    public function testLoadbyconfigurationcode(): void
    {
        $client = static::createClient();
        $client->insulate();

        $client->request('GET', '/frontendapi/configuration/loadbyconfigurationcode/defaultoptions_1?_language=de_DE&_share_url=http%3A%2F%2Fwww.int.configuratorware.local%3Fcode%3D%7Bcode%7D%26param%3Dtest');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('defaultoptions_sheep', $result->name, 'JSON content mismatch');
        self::assertEquals('coat', $result->optionclassifications[0]->identifier, 'JSON content mismatch');
        self::assertEquals('Fell', $result->optionclassifications[0]->title, 'JSON content mismatch');
        self::assertEquals('defaultoptions', $result->configurationType, 'JSON content mismatch');
        self::assertEquals('http://www.int.configuratorware.local?code=defaultoptions_1&param=test', $result->shareUrl, 'JSON content mismatch');
    }

    public function testFrontendvalidateConfigurationShouldBeValid(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"1","language":"en_GB","title":"tags"},{"id":"2","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"face_nature","title":"{face_nature}","abstract":"{face_nature}","description":"{face_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"6","price":"15.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}},"switchedoptions":{"coat":{"identifier":"coat_nature","title":"{face_white}","abstract":"{face_white}","description":"{face_white}","attributes":[],"price":"17.45","stock":{},"amount":1,"amount_is_selectable":null}}}';

        $client = $this->createAuthenticatedClient('admin');
        $client->request(
            'POST',
            '/frontendapi/configuration/validate',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals(true, $receivedResult->success, 'content mismatch');
    }

    public function testFrontendvalidateConfigurationShouldBeInvalid(): void
    {
        $postdata = '{"configuration":{"code":"defaultoptions_1","name":"defaultoptions_sheep","item":{"identifier":"sheep","title":"Schaf","abstract":"Dein neuer wolliger Freund.","description":"Könnte ein schwarzes Schaf sein, deine Entscheidung!","attributes":[{"attribute_id":"1","identifier":"tags","translated_title":"Tags","texts":[{"id":"1","language":"en_GB","title":"tags"},{"id":"2","language":"de_DE","title":"Tags"}],"attributedatatype":"string","values":[{"id":null,"value":null,"translated_value":"vegetarier","translations":[{"language":"en_GB","translation":"vegetarian"},{"language":"de_DE","translation":"vegetarier"}]},{"id":null,"value":null,"translated_value":"essbar","translations":[{"language":"en_GB","translation":"edible"},{"language":"de_DE","translation":"essbar"}]}]}],"price":null,"stock":[{"id":"1","status":"available","deliverytime":"+2 weeks","channel":"1","amount":null}],"prices":[{"id":"1","price":"199.00","channel":"1","amountfrom":"1"},{"id":"2","price":"189.00","channel":"1","amountfrom":"10"}],"itemclassifications":[{"id":"1","parent_id":null,"identifier":"animal","sequencenumber":"1","translated_title":"Tier","translated_description":"alle möglichen Tiere","texts":[{"id":"1","language":"en_GB","title":"animal","description":"all kinds of animals"},{"id":"2","language":"de_DE","title":"Tier","description":"alle möglichen Tiere"}]}]},"optionclassifications":[{"is_multiselect":false,"selectedoptions":[{"identifier":"coat_nature","title":"{coat_nature}","abstract":"{coat_nature}","description":"{coat_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"3","price":"12.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"coat","title":"Fell","description":"{coat}"},{"is_multiselect":false,"is_mandatory":true,"selectedoptions":[],"identifier":"face","title":"Gesicht","description":"{face}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"legs_nature","title":"{legs_nature}","abstract":"{legs_nature}","description":"{legs_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"9","price":"18.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"legs","title":"Beine","description":"{legs}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"eyes_yellow","title":"{eyes_yellow}","abstract":"{eyes_yellow}","description":"{eyes_yellow}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"12","price":"22.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"eyes","title":"Augen","description":"{eyes}"},{"is_multiselect":false,"selectedoptions":[{"identifier":"ear_eyelid_nature","title":"{ear_eyelid_nature}","abstract":"{ear_eyelid_nature}","description":"{ear_eyelid_nature}","attributes":[],"price":null,"stock":[],"amount":1,"amount_is_selectable":null,"prices":[{"id":"15","price":"25.45","channel":"1","amountfrom":"1"}],"itemclassifications":[]}],"identifier":"ear_eyelid","title":"Ohr & Augenlid","description":"{ear_eyelid}"}],"visualizationData":null,"information":{"channelSettings":{"currencySymbol":"€"},"languageSettings":{"id":"2","iso":"de_DE","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":",","thousandsseparator":".","currencysymbolposition":"right","user_created_id":"0"}},"date_created":{"date":"2017-06-26 14:55:55.000000","timezone_type":3,"timezone":"Europe/Paris"}}}';

        $client = $this->createAuthenticatedClient('admin');
        $client->request(
            'POST',
            '/frontendapi/configuration/validate',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postdata
        );

        $response = $client->getResponse();

        $receivedResult = json_decode($response->getContent());

        self::assertEquals(false, $receivedResult->success, 'content mismatch, received result is not success: false');
    }

    public function testFrontendPdfUserDownload(): void
    {
        $client = static::createClient();
        $client->request('GET', '/frontendapi/configurations/pdf/user/defaultoptions_1');

        $response = $client->getResponse();

        $statusCode = $response->getStatusCode();
        self::assertSame(Response::HTTP_OK, $statusCode, 'HTTP status code not ok');

        $mimeType = mime_content_type($response->getFile()->getPathName());
        self::assertEquals('application/pdf', $mimeType, 'MimeType of downloaded File is not PDF');
    }

    public function testFrontendPdfProductionDownload(): void
    {
        $client = static::createClient();
        $client->request('GET', '/frontendapi/configurations/pdf/production/defaultoptions_1');

        $response = $client->getResponse();

        $statusCode = $response->getStatusCode();
        self::assertSame(Response::HTTP_OK, $statusCode, 'HTTP status code not ok');

        $mimeType = mime_content_type($response->getFile()->getPathName());
        self::assertEquals('application/pdf', $mimeType, 'MimeType of downloaded File is not PDF');
    }

    public function testFrontendPdfWrongDocumentType(): void
    {
        $client = static::createClient();
        $client->request('GET', '/frontendapi/configurations/pdf/thisissomerandomstring/defaultoptions_1');

        $response = $client->getResponse();

        $statusCode = $response->getStatusCode();
        self::assertSame(Response::HTTP_NOT_FOUND, $statusCode, 'HTTP status code not ok');
    }

    public function testFrontendPdfWrongConfigurationCode(): void
    {
        $client = static::createClient();
        $client->request('GET', '/frontendapi/configurations/pdf/production/thisissomerandomstring');

        $response = $client->getResponse();

        $statusCode = $response->getStatusCode();
        self::assertSame(Response::HTTP_NOT_FOUND, $statusCode, 'HTTP status code not ok');
    }

    public function testFrontendZipProductionDownload(): void
    {
        $client = static::createClient();
        $client->request('GET', '/frontendapi/configurations/zip/production/defaultoptions_1');

        $response = $client->getResponse();

        $statusCode = $response->getStatusCode();
        self::assertSame(Response::HTTP_OK, $statusCode, 'HTTP status code not ok');

        $mimeType = mime_content_type($response->getFile()->getPathName());
        self::assertEquals('application/zip', $mimeType, 'MimeType of downloaded File is not ZIP');
    }

    public function testFrontendZipProductionContent(): void
    {
        $client = static::createClient();

        $this->fileSystem->copy(
            __DIR__ . '/_data/Configuration/zip_base_svg/123456_chest.svg',
            $this->vfsRoot->url() . '/images/printfiles/123456_chest.svg'
        );

        $this->fileSystem->copy(
            __DIR__ . '/_data/Configuration/zip_base_svg/123456_fur.svg',
            $this->vfsRoot->url() . '/images/printfiles/123456_fur.svg'
        );

        $client->request('GET', '/frontendapi/configurations/zip/production/123456');
        $response = $client->getResponse();

        $statusCode = $response->getStatusCode();
        self::assertSame(Response::HTTP_OK, $statusCode, 'HTTP status code not ok');

        $archive = new \ZipArchive();
        $archive->open($response->getFile()->getPathName());

        $expectedFiles = [
            'Sheep_123456.pdf',
            'Sheep_123456.csv',
            'chest.pdf',
            'fur.pdf',
        ];

        foreach ($expectedFiles as $fileName) {
            self::assertNotFalse(
                $archive->statName($fileName),
                'The file "' . $fileName . '" is missing from the archive'
            );
        }
    }

    public function testFrontendZipWrongConfigurationCode(): void
    {
        $client = static::createClient();
        $client->request('GET', '/frontendapi/configurations/zip/production/thisissomerandomstring');

        $response = $client->getResponse();

        $statusCode = $response->getStatusCode();
        self::assertSame(Response::HTTP_NOT_FOUND, $statusCode, 'HTTP status code not ok');
    }

    private function getConfigurationJson(string $name, string $itemIdentifier, array $images): string
    {
        $configuration = new \stdClass();
        $configuration->configuration = new \stdClass();
        $configuration->configuration->name = $name;
        $configuration->configuration->optionclassifications = [];
        $configuration->configuration->item = new \stdClass();
        $configuration->configuration->item->identifier = $itemIdentifier;
        $configuration->configuration->screenshots = new \stdClass();
        $configuration->configurationtype = 'user';

        foreach ($images as $itemIdentifier => $item) {
            if (is_array($item)) {
                $configuration->configuration->screenshots->$itemIdentifier = new \stdClass();
                foreach ($item as $viewIdentifier => $view) {
                    $encoded = 'data:image/png;base64,' . base64_encode(file_get_contents($view));
                    $configuration->configuration->screenshots->$itemIdentifier->$viewIdentifier = $encoded;
                }
            } else {
                $encoded = 'data:image/png;base64,' . base64_encode(file_get_contents($item));
                $configuration->configuration->screenshots->$itemIdentifier = $encoded;
            }
        }

        return json_encode($configuration);
    }
}
