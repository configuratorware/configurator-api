<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CreatorViewControllerTest extends ApiTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/softshell_creatordesigner_2d.sql'));
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/creator_views');
        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/creator_views/3002');
        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testSave()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/creator_views', [], [], [], $this->saveJson);

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    private $saveJson = <<<JSON
{
        "id": 3002,
        "identifier": "front_view_changed",
        "sequenceNumber": "2",
        "itemId": 3003,
        "designAreas": [],
        "texts": []
}
JSON;
}
