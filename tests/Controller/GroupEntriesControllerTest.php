<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion\SkipDatabaseVersion;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion\SkipDatabaseVersionTrait;

class GroupEntriesControllerTest extends ApiTestCase
{
    use SkipDatabaseVersionTrait;

    /**
     * @var ItemgroupentryRepository
     */
    private $repository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Itemgroupentry::class);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/GroupEntry/setUp.sql'));
    }

    /**
     * @SkipDatabaseVersion("8.0.28")
     */
    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/groupentries?limit=10');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            '{"metadata":{"totalcount":"5"},"data":[{"id":1,"identifier":"red","translation":null,"translations":[{"id":1,"language":"en_GB","translation":"Red"},{"id":2,"language":"de_DE","translation":"Rot"}]},{"id":2,"identifier":"blue","translation":null,"translations":[{"id":3,"language":"en_GB","translation":"Blue"},{"id":4,"language":"de_DE","translation":"Blau"}]},{"id":3,"identifier":"s","translation":null,"translations":[{"id":5,"language":"en_GB","translation":"S"},{"id":6,"language":"de_DE","translation":"S"}]},{"id":4,"identifier":"m","translation":null,"translations":[{"id":7,"language":"en_GB","translation":"M"},{"id":8,"language":"de_DE","translation":"M"}]},{"id":5,"identifier":"l","translation":null,"translations":[{"id":9,"language":"en_GB","translation":"L"},{"id":10,"language":"de_DE","translation":"L"}]}]}'
        );
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/groupentries/1');
        $response = $client->getResponse();
        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $this->compareResponseContent(
            $response,
            '{"id":1,"identifier":"red","translation":null,"translations":[{"id":1,"language":"en_GB","translation":"Red"},{"id":2,"language":"de_DE","translation":"Rot"}]}'
        );
    }

    public function testSave()
    {
        $postData = '{"identifier":"green","translation":null,"translations":[{"language":"en_GB","translation":"Green"},{"language":"de_DE","translation":"Grün"}]}';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/groupentries',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $group = $this->repository->findOneBy(['identifier' => 'green']);

        self::assertInstanceOf(Itemgroupentry::class, $group);
    }

    public function testDelete()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/groupentries/1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $group = $this->repository->findOneBy(['identifier' => 'red']);

        self::assertEquals(null, $group);
    }

    public function testSaveSequenceNumbers()
    {
        $postData = '[{"id":1,"sequencenumber":100},{"id":2,"sequencenumber":101}]';

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/groupentries/savesequencenumbers',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $group = $this->repository->findOneBy(['id' => 1]);

        self::assertInstanceOf(Itemgroupentry::class, $group);
        self::assertEquals(100, $group->getSequencenumber());
    }
}
