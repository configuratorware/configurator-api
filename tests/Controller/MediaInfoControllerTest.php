<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class MediaInfoControllerTest extends ApiTestCase
{
    protected $repository;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    protected function setUp(): void
    {
        parent::setUp();

        $this->fileSystem = new Filesystem();
        $this->repository = $this->em->getRepository(Item::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/softshell_creator_2d.sql'));
    }

    public function testShouldReturnMediaInfoPerItem()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/mediainfo/2003');
        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }
}
