<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use org\bovigo\vfs\vfsStream;
use Redhotmagma\ConfiguratorApiBundle\Structure\LicenseFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * @MockPaths
 */
class LicenseControllerTest extends ApiTestCase
{
    use VfsTestTrait;

    /**
     * @var string
     */
    private $root;

    /**
     * @var Filesystem
     */
    private $filesystem;

    public function setUp(): void
    {
        parent::setUp();
        $this->root = vfsStream::setup()->url();
        $this->filesystem = new Filesystem();
    }

    /**
     * @dataProvider licenseFileDataProvider
     *
     * @param string $fileName
     * @param string $fileContent
     * @param string $mime
     * @param string $expectedStatusCode
     * @param string $expectedResponse
     */
    public function testLicenseFileUpload(
        string $fileName,
        string $fileContent,
        string $mime,
        int $expectedStatusCode,
        string $expectedResponse
    ): void {
        $filePath = $this->root . '/license_uploadfile/' . $fileName;
        $this->filesystem->dumpFile($filePath, $fileContent);
        $uploadedFile = new UploadedFile($filePath, $fileName, $mime);

        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/license/upload', [], ['license' => $uploadedFile]);
        $response = $client->getResponse();

        self::assertEquals($expectedStatusCode, $response->getStatusCode());
        $this->compareResponseContent($response, $expectedResponse);
    }

    public function licenseFileDataProvider(): array
    {
        return [
            [
                'license_creator.txt',
                'creator###meinshop;mein-shop;redhotmagma' . chr(10) .
                'ac39df4206108c00df3b4fb0a734ad215400ff82e8ae4e91ddc03e0f6707471e',
                'text/plain',
                200,
                $this->createExpectedResponseJson('creator'),
            ],
            [
                'license_designer.txt',
                'designer###meinshop;mein-shop;redhotmagma' . chr(10) .
                '056b92a53dc30ce5e4a98d4be65a90c154bf90980d2f195c29029c51e4905448',
                'text/plain',
                200,
                $this->createExpectedResponseJson('designer'),
            ],
            [
                'license_finder.txt',
                'finder###only-one-domain' . chr(10) .
                '151e631600289e520f77ddc11b1d33f8066870a1ea2d9f757cbb45dfa8765fd5',
                'text/plain',
                200,
                $this->createExpectedResponseJson('finder'),
            ],
            [
                'license_creator_designer.txt',
                'creator+designer###only-one-domain' . chr(10) .
                '63972c643c2a9abac40da2fd9f4a39d8e145a2667512e5a128b1f7d5332b9847',
                'text/plain',
                200,
                $this->createExpectedResponseJson('creator+designer'),
            ],
            [
                'license_creator_designer_finder.txt',
                'creator+designer+finder###only-one-domain' . chr(10) .
                '0b26396ce5998a55bd15765f8d736c372083cf749f7de3554e62e73550ae2e54',
                'text/plain',
                200,
                $this->createExpectedResponseJson('creator+designer+finder'),
            ],
            [
                'license_invalid_hash.txt',
                'creator+designer###only-one-domain' . chr(10) .
                'c666c55c99c5e77f6b714b648034fd8f674b036b53d77a59bb02d281a7f46765',
                'text/plain',
                200,
                $this->createExpectedResponseJson(null),
            ],
            [
                'license_invalid_product.txt',
                'Kreator###meinshop;mein-shop;redhotmagma.' . chr(10)
                . 'ac39df4206108c00df3b4fb0a734ad215400ff82e8ae4e91ddc03e0f6707471e',
                'text/plain',
                200,
                $this->createExpectedResponseJson(null),
            ],
            [
                'license_empty.txt',
                '',
                'text/plain',
                400,
                '{
                    "errors":[
                        {
                            "property":"uploaded_file",
                            "invalidvalue":"license_empty.txt",
                            "message":"error_uploading_file"
                        }
                    ],
                    "success":false,
                    "code":"validation_error"
                }',
            ],
            [
                'license_fake.png',
                '',
                'image/png,',
                400,
                '{
                    "errors":[
                        {
                            "property":"uploaded_file",
                            "invalidvalue":"license_fake.png",
                            "message":"error_uploading_file"
                        }
                    ],
                    "success":false,
                    "code":"validation_error"
                }',
            ],
        ];
    }

    /**
     * @param string|null $credentials
     *
     * @return string
     */
    private function createExpectedResponseJson(?string $credentials): string
    {
        $licenseFile = new LicenseFile();
        $licenseFile->license = $credentials;
        $licenseFile->credentials = [
            'assembly_points',
            'attribute_values',
            'attributes',
            'channels',
            'code_snippets',
            'color_palettes',
            'configuration_variants',
            'configurations',
            'credentials',
            'creator_views',
            'currencies',
            'data_transfer',
            'default_client',
            'design_areas',
            'design_production_methods',
            'design_templates',
            'design_views',
            'designer_global_calculation_types',
            'designer_global_item_prices',
            'fonts',
            'image_gallery',
            'inspirations',
            'item_classifications',
            'item_statuses',
            'items',
            'items_creator',
            'items_designer',
            'languages',
            'licenses',
            'media',
            'option_classifications',
            'option_pools',
            'options',
            'production_view',
            'roles',
            'settings',
            'software_updates',
            'tags',
            'translations',
            'users',
            'question_trees',
        ];

        return json_encode($licenseFile);
    }
}
