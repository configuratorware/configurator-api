<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

final class InspirationsControllerTest extends ApiTestCase
{
    private $repository;

    public function setUp(): void
    {
        parent::setUp();

        $this->repository = $this->em->getRepository(Inspiration::class);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_hoodie.sql'));
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/softshell_creator_2d.sql'));
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Inspiration/setUp.sql'));
    }

    public function testFrontendList(): void
    {
        $client = $this->createClient();
        $client->request('GET', '/frontendapi/inspirations/softshell_creator_2d');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testList(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/inspirations');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDetail(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/inspirations/1');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testDelete(): void
    {
        // sanity-check
        self::assertNotNull($this->repository->find(1));

        $client = $this->createAuthenticatedClient();
        $client->request('DELETE', '/api/inspirations/1');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);

        self::assertNull($this->repository->find(1));
    }

    public function testCreate(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/inspirations', [], [], [], $this->inspirationCreate);

        self::assertNotNull($this->repository->findOneBy(['configuration_code' => 'QQSSQQ']));
    }

    public function testUpdate(): void
    {
        $client = $this->createAuthenticatedClient();
        $client->request('POST', '/api/inspirations', [], [], [], $this->inspirationUpdate);
        $client->request('GET', '/api/inspirations/1');
        $response = $client->getResponse();

        self::assertJsonStringEqualsJsonString($this->inspirationUpdate, $response->getContent());
    }

    private $inspirationCreate = <<<JSON
{
  "active": false,
  "configurationCode": "QQSSQQ",
  "texts": [
	{
      "title": "test_en",
      "description": null,
      "language": "en_GB"
    },
    {
      "title": "test_de",
      "description": null,
      "language": "de_DE"
    }
  ],
  "thumbnail": null,
  "itemId": 2
}
JSON;

    private $inspirationUpdate = <<<JSON
{
  "id": 1,
  "active": true,
  "configurationCode": "KQWUTQ",
  "texts": [
    {
      "id": 1,
      "title": "test_en",
      "description": null,
      "language": "en_GB"
    },
	{
	  "id": 2,
      "title": "test_de",
      "description": null,
      "language": "de_DE"
    }
  ],
  "thumbnail": "/images/inspirations/thumbnail/1.png",
  "itemId": 2003
}
JSON;
}
