<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion\SkipDatabaseVersion;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\SkipDatabaseVersion\SkipDatabaseVersionTrait;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\MockPaths;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\vfsTest\VfsTestTrait;

/**
 * Tests for {@link ConfigurationModesController}.
 *
 * @MockPaths
 */
class ConfigurationModesControllerTest extends ApiTestCase
{
    use SkipDatabaseVersionTrait;
    use VfsTestTrait;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var vfsStreamDirectory
     */
    private $root;

    /**
     * @var string
     */
    private $designViewImagePath;

    /**
     * @var string
     */
    private $designViewThumbnailPath;

    /**
     * @var ItemRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->fileSystem = new Filesystem();
        $this->root = vfsStream::setup();

        $this->repository = $this->em->getRepository(Item::class);
        $this->designViewImagePath = static::$kernel->getContainer()->getParameter('design_view_image_path');
        $this->designViewThumbnailPath = static::$kernel->getContainer()->getParameter('design_view_thumbnail_path');
    }

    public function testGetData3d()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_pen_3d.sql'));

        $uri = '/frontendapi/configurationmodes/designer/getdata';
        $header = ['CONTENT_TYPE' => 'application/json'];
        $content = $this->createDataContent('pen', 'demo_pen');

        $client = static::createClient();
        $client->request('POST', $uri, [], [], $header, $content);

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @SkipDatabaseVersion("8.0.28")
     */
    public function testGetData2dDesigner()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_hoodie.sql'));
        $this->createMockFiles2d();

        $uri = '/frontendapi/configurationmodes/designer/getdata';
        $header = ['CONTENT_TYPE' => 'application/json'];
        $content = $this->createDataContent('hoodie', 'demo_hoodie');

        $client = static::createClient();
        $client->request('POST', $uri, [], [], $header, $content);

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @SkipDatabaseVersion("8.0.28")
     */
    public function testGetData2dOverlayDesigner()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_hoodie_2doverlay_visualization.sql'));
        $this->createMockFiles2d();

        $uri = '/frontendapi/configurationmodes/designer/getdata';
        $header = ['CONTENT_TYPE' => 'application/json'];
        $content = $this->createDataContent('hoodie', 'demo_hoodie');

        $client = static::createClient();
        $client->request('POST', $uri, [], [], $header, $content);

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testGetData2dCreatorDesigner()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/softshell_creatordesigner_2d.sql'));
        $this->createMockFiles2d();

        $uri = '/frontendapi/configurationmodes/designer/getdata';
        $header = ['CONTENT_TYPE' => 'application/json'];
        $content = $this->createDataContent('hoodie', 'softshell_creatordesigner_2d');

        $client = static::createClient();
        $client->request('POST', $uri, [], [], $header, $content);

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    /**
     * @SkipDatabaseVersion("8.0.28")
     */
    public function testGetDefaultView()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/Item/demo_calendar_2d.sql'));

        $uri = '/frontendapi/configurationmodes/designer/getdata';
        $header = ['CONTENT_TYPE' => 'application/json'];
        $content = $this->createDataContent('calendar', 'demo_calendar');

        $client = static::createClient();
        $client->request('POST', $uri, [], [], $header, $content);

        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    private function createMockFiles2d(): void
    {
        $items = $this->repository->getItemsByParent('demo_hoodie');

        foreach ($items as $item) {
            $this->fileSystem->dumpFile(
                $this->root->url() . '/' . $this->designViewThumbnailPath . '/demo_hoodie/' . $item->getIdentifier() . '/front_view.jpg',
                'This is a mock file:' . $item->getIdentifier()
            );

            $this->fileSystem->dumpFile(
                $this->root->url() . '/' . $this->designViewImagePath . '/demo_hoodie/front_view/' . $item->getIdentifier() . '.jpg',
                'This is a mock file:' . $item->getIdentifier()
            );
        }
    }

    private function createDataContent(string $name, string $itemIdentifier)
    {
        $data = [
            'configuration' => [
                'shareUrl' => 'https://www.redhotmagma.de',
                'name' => $name,
                'item' => [
                    'identifier' => $itemIdentifier,
                ],
            ],
        ];

        return json_encode($data);
    }
}
