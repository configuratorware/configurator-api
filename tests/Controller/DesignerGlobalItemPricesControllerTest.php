<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use redhotmagma\SymfonyTestUtils\Fixture\Fixtures;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DesignerGlobalItemPricesControllerTest extends ApiTestCase
{
    /**
     * @var ConfigurationRepository
     */
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignerGlobalItemPrices/setUp.sql'));
    }

    public function testList()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', 'api/designerglobalitemprices');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testDetail()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/designerglobalitemprices/1');
        $response = $client->getResponse();
        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testCreate()
    {
        $postData = file_get_contents(__DIR__ . '/_data/DesignerGlobalItemPrices/createJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designerglobalitemprices',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );

        $response = $client->getResponse();

        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }

    public function testDeletePrice()
    {
        $postData = file_get_contents(__DIR__ . '/_data/DesignerGlobalItemPrices/deleteJson.txt');

        $client = $this->createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/designerglobalitemprices',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            $postData
        );
        $response = $client->getResponse();

        $expected = $this->loadResponseFixture();

        Fixtures::assertResponseEquals($expected, $response);
    }
}
