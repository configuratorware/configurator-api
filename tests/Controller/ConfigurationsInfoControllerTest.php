<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Symfony\Component\HttpFoundation\Response;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\Fixtures;

class ConfigurationsInfoControllerTest extends ApiTestCase
{
    protected $repository;

    protected function setUp(): void
    {
        parent::setUp();
        // create data to be able to run the test
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ConfigurationsInfoControllerTest_data.sql'));
        $this->repository = $this->em->getRepository(Configuration::class);
    }

    public function testLoadConfigurationinfo()
    {
        $client = static::createClient();
        $client->insulate();

        $client->request('GET', '/frontendapi/configuration/loadconfigurationinfo/defaultoptions_1');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('defaultoptions_sheep', $result->name, 'JSON content mismatch');
    }

    public function testLoadConfigurationInfoExtendedData()
    {
        $client = static::createClient();
        $client->insulate();

        $client->request('GET', '/frontendapi/configuration/loadconfigurationinfo/defaultoptions_1?_extended_data=1');
        $response = $client->getResponse();

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $response);
    }

    public function testLoadconfigurationinfoWithParentCode()
    {
        $client = static::createClient();

        $client->request('GET', '/frontendapi/configuration/loadconfigurationinfo/luke_skywalker');

        Fixtures::assertResponseEquals($this->loadResponseFixture(), $client->getResponse());
    }

    public function testShouldAddDesignerInfo()
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignerCalculation/setUp.sql'));

        $client = static::createClient();

        $client->request('GET', '/frontendapi/configuration/loadconfigurationinfo/my_design');
        $response = $client->getResponse();

        self::assertSame(Response::HTTP_OK, $response->getStatusCode(), 'HTTP status code not ok');

        $result = json_decode($response->getContent());

        self::assertEquals('[[{"designAreaIdentifier":"front","designProductionMethodIdentifier":"print","colorAmount":null,"images":[{"image":"http:\/\/localhost:10030\/images\/imagegallery\/c4ca4238a0b923820dcc509a6f75849b.jpeg"}],"texts":[{"text":"My Text","fontSize":"16px","fontFamily":"\"Comic Sans MS\", cursive, sans-serif","customFont":false,"isBulkName":false,"fontWeight":"normal","fontStyle":"italic","colors":[{"identifier":"red","value":"rgb(247, 52, 41)"}]}]}]]',
            json_encode($result->designData), 'JSON content mismatch');

        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignerCalculation/tearDown.sql'));
    }
}
