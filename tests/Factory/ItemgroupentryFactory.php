<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Itemgroupentry>
 *
 * @method static Itemgroupentry|Proxy createOne(array $attributes = [])
 * @method static Itemgroupentry[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Itemgroupentry[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Itemgroupentry|Proxy find(object|array|mixed $criteria)
 * @method static Itemgroupentry|Proxy findOrCreate(array $attributes)
 * @method static Itemgroupentry|Proxy first(string $sortedField = 'id')
 * @method static Itemgroupentry|Proxy last(string $sortedField = 'id')
 * @method static Itemgroupentry|Proxy random(array $attributes = [])
 * @method static Itemgroupentry|Proxy randomOrCreate(array $attributes = [])
 * @method static Itemgroupentry[]|Proxy[] all()
 * @method static Itemgroupentry[]|Proxy[] findBy(array $attributes)
 * @method static Itemgroupentry[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Itemgroupentry[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemgroupentryRepository|RepositoryProxy repository()
 * @method Itemgroupentry|Proxy create(array|callable $attributes = [])
 */
final class ItemgroupentryFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Itemgroupentry::class;
    }
}
