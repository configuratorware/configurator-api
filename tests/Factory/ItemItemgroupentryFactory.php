<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<ItemItemgroupentry>
 *
 * @method static ItemItemgroupentry|Proxy createOne(array $attributes = [])
 * @method static ItemItemgroupentry[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ItemItemgroupentry[]|Proxy[] createSequence(array|callable $sequence)
 * @method static ItemItemgroupentry|Proxy find(object|array|mixed $criteria)
 * @method static ItemItemgroupentry|Proxy findOrCreate(array $attributes)
 * @method static ItemItemgroupentry|Proxy first(string $sortedField = 'id')
 * @method static ItemItemgroupentry|Proxy last(string $sortedField = 'id')
 * @method static ItemItemgroupentry|Proxy random(array $attributes = [])
 * @method static ItemItemgroupentry|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemItemgroupentry[]|Proxy[] all()
 * @method static ItemItemgroupentry[]|Proxy[] findBy(array $attributes)
 * @method static ItemItemgroupentry[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ItemItemgroupentry[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method ItemItemgroupentry|Proxy create(array|callable $attributes = [])
 */
final class ItemItemgroupentryFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return ItemItemgroupentry::class;
    }
}
