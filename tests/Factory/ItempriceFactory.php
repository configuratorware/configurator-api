<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItempriceRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Itemprice>
 *
 * @method static Itemprice|Proxy createOne(array $attributes = [])
 * @method static Itemprice[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Itemprice[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Itemprice|Proxy find(object|array|mixed $criteria)
 * @method static Itemprice|Proxy findOrCreate(array $attributes)
 * @method static Itemprice|Proxy first(string $sortedField = 'id')
 * @method static Itemprice|Proxy last(string $sortedField = 'id')
 * @method static Itemprice|Proxy random(array $attributes = [])
 * @method static Itemprice|Proxy randomOrCreate(array $attributes = [])
 * @method static Itemprice[]|Proxy[] all()
 * @method static Itemprice[]|Proxy[] findBy(array $attributes)
 * @method static Itemprice[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Itemprice[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItempriceRepository|RepositoryProxy repository()
 * @method Itemprice|Proxy create(array|callable $attributes = [])
 */
final class ItempriceFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Itemprice::class;
    }
}
