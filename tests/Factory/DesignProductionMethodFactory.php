<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<DesignProductionMethod>
 *
 * @method static DesignProductionMethod|Proxy createOne(array $attributes = [])
 * @method static DesignProductionMethod[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static DesignProductionMethod[]|Proxy[] createSequence(array|callable $sequence)
 * @method static DesignProductionMethod|Proxy find(object|array|mixed $criteria)
 * @method static DesignProductionMethod|Proxy findOrCreate(array $attributes)
 * @method static DesignProductionMethod|Proxy first(string $sortedField = 'id')
 * @method static DesignProductionMethod|Proxy last(string $sortedField = 'id')
 * @method static DesignProductionMethod|Proxy random(array $attributes = [])
 * @method static DesignProductionMethod|Proxy randomOrCreate(array $attributes = [])
 * @method static DesignProductionMethod[]|Proxy[] all()
 * @method static DesignProductionMethod[]|Proxy[] findBy(array $attributes)
 * @method static DesignProductionMethod[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static DesignProductionMethod[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static DesignProductionMethodRepository|RepositoryProxy repository()
 * @method DesignProductionMethod|Proxy create(array|callable $attributes = [])
 */
final class DesignProductionMethodFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return DesignProductionMethod::class;
    }
}
