<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Itemclassification>
 *
 * @method static Itemclassification|Proxy createOne(array $attributes = [])
 * @method static Itemclassification[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Itemclassification[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Itemclassification|Proxy find(object|array|mixed $criteria)
 * @method static Itemclassification|Proxy findOrCreate(array $attributes)
 * @method static Itemclassification|Proxy first(string $sortedField = 'id')
 * @method static Itemclassification|Proxy last(string $sortedField = 'id')
 * @method static Itemclassification|Proxy random(array $attributes = [])
 * @method static Itemclassification|Proxy randomOrCreate(array $attributes = [])
 * @method static Itemclassification[]|Proxy[] all()
 * @method static Itemclassification[]|Proxy[] findBy(array $attributes)
 * @method static Itemclassification[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Itemclassification[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemclassificationRepository|RepositoryProxy repository()
 * @method Itemclassification|Proxy create(array|callable $attributes = [])
 */
final class ItemclassificationFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Itemclassification::class;
    }
}
