<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<DesignAreaText>
 *
 * @method static DesignAreaText|Proxy createOne(array $attributes = [])
 * @method static DesignAreaText[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static DesignAreaText[]|Proxy[] createSequence(array|callable $sequence)
 * @method static DesignAreaText|Proxy find(object|array|mixed $criteria)
 * @method static DesignAreaText|Proxy findOrCreate(array $attributes)
 * @method static DesignAreaText|Proxy first(string $sortedField = 'id')
 * @method static DesignAreaText|Proxy last(string $sortedField = 'id')
 * @method static DesignAreaText|Proxy random(array $attributes = [])
 * @method static DesignAreaText|Proxy randomOrCreate(array $attributes = [])
 * @method static DesignAreaText[]|Proxy[] all()
 * @method static DesignAreaText[]|Proxy[] findBy(array $attributes)
 * @method static DesignAreaText[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static DesignAreaText[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method DesignAreaText|Proxy create(array|callable $attributes = [])
 */
final class DesignAreaTextFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return DesignAreaText::class;
    }
}
