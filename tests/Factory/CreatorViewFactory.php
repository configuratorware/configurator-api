<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<CreatorView>
 *
 * @method static CreatorView|Proxy createOne(array $attributes = [])
 * @method static CreatorView[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CreatorView[]|Proxy[] createSequence(array|callable $sequence)
 * @method static CreatorView|Proxy find(object|array|mixed $criteria)
 * @method static CreatorView|Proxy findOrCreate(array $attributes)
 * @method static CreatorView|Proxy first(string $sortedField = 'id')
 * @method static CreatorView|Proxy last(string $sortedField = 'id')
 * @method static CreatorView|Proxy random(array $attributes = [])
 * @method static CreatorView|Proxy randomOrCreate(array $attributes = [])
 * @method static CreatorView[]|Proxy[] all()
 * @method static CreatorView[]|Proxy[] findBy(array $attributes)
 * @method static CreatorView[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static CreatorView[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static CreatorViewRepository|RepositoryProxy repository()
 * @method CreatorView|Proxy create(array|callable $attributes = [])
 */
final class CreatorViewFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return CreatorView::class;
    }
}
