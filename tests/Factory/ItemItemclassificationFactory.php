<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemItemclassificationRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ItemItemclassification>
 *
 * @method static ItemItemclassification|Proxy createOne(array $attributes = [])
 * @method static ItemItemclassification[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ItemItemclassification[]|Proxy[] createSequence(array|callable $sequence)
 * @method static ItemItemclassification|Proxy find(object|array|mixed $criteria)
 * @method static ItemItemclassification|Proxy findOrCreate(array $attributes)
 * @method static ItemItemclassification|Proxy first(string $sortedField = 'id')
 * @method static ItemItemclassification|Proxy last(string $sortedField = 'id')
 * @method static ItemItemclassification|Proxy random(array $attributes = [])
 * @method static ItemItemclassification|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemItemclassification[]|Proxy[] all()
 * @method static ItemItemclassification[]|Proxy[] findBy(array $attributes)
 * @method static ItemItemclassification[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ItemItemclassification[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemItemclassificationRepository|RepositoryProxy repository()
 * @method ItemItemclassification|Proxy create(array|callable $attributes = [])
 */
final class ItemItemclassificationFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return ItemItemclassification::class;
    }
}
