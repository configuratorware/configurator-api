<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<ItemItemgroup>
 *
 * @method static ItemItemgroup|Proxy createOne(array $attributes = [])
 * @method static ItemItemgroup[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ItemItemgroup[]|Proxy[] createSequence(array|callable $sequence)
 * @method static ItemItemgroup|Proxy find(object|array|mixed $criteria)
 * @method static ItemItemgroup|Proxy findOrCreate(array $attributes)
 * @method static ItemItemgroup|Proxy first(string $sortedField = 'id')
 * @method static ItemItemgroup|Proxy last(string $sortedField = 'id')
 * @method static ItemItemgroup|Proxy random(array $attributes = [])
 * @method static ItemItemgroup|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemItemgroup[]|Proxy[] all()
 * @method static ItemItemgroup[]|Proxy[] findBy(array $attributes)
 * @method static ItemItemgroup[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ItemItemgroup[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method ItemItemgroup|Proxy create(array|callable $attributes = [])
 */
final class ItemItemgroupFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return ItemItemgroup::class;
    }
}
