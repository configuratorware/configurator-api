<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Factory\DompdfFactory;

class DompdfFactoryTest extends TestCase
{
    private DompdfFactory $factory;

    private string $webroot;

    public function setUp(): void
    {
        $this->webroot = __DIR__ . '/_fixtures/';

        $this->factory = new DompdfFactory($this->webroot);
    }

    public function tearDown(): void
    {
        if (file_exists($this->webroot . DIRECTORY_SEPARATOR . 'client')) {
            unlink($this->webroot . DIRECTORY_SEPARATOR . 'client');
        }
    }

    public function testShouldCreateDompdfWithDataFolder()
    {
        symlink('/tmp', $this->webroot . DIRECTORY_SEPARATOR . 'client');

        $dompdf = $this->factory->create();

        $expected = [__DIR__ . '/_fixtures/', '/tmp'];

        $this->assertEquals($expected, $dompdf->getOptions()->getChroot());
    }

    public function testShouldCreateDompdfWithoutDataFolder()
    {
        $dompdf = $this->factory->create();

        $expected = [__DIR__ . '/_fixtures/'];

        $this->assertEquals($expected, $dompdf->getOptions()->getChroot());
    }
}
