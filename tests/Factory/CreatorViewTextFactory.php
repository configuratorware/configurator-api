<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewText;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<CreatorViewText>
 *
 * @method static CreatorViewText|Proxy createOne(array $attributes = [])
 * @method static CreatorViewText[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static CreatorViewText[]|Proxy[] createSequence(array|callable $sequence)
 * @method static CreatorViewText|Proxy find(object|array|mixed $criteria)
 * @method static CreatorViewText|Proxy findOrCreate(array $attributes)
 * @method static CreatorViewText|Proxy first(string $sortedField = 'id')
 * @method static CreatorViewText|Proxy last(string $sortedField = 'id')
 * @method static CreatorViewText|Proxy random(array $attributes = [])
 * @method static CreatorViewText|Proxy randomOrCreate(array $attributes = [])
 * @method static CreatorViewText[]|Proxy[] all()
 * @method static CreatorViewText[]|Proxy[] findBy(array $attributes)
 * @method static CreatorViewText[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static CreatorViewText[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method CreatorViewText|Proxy create(array|callable $attributes = [])
 */
final class CreatorViewTextFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return CreatorViewText::class;
    }
}
