<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\RuleText;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;

/**
 * @extends ModelFactory<RuleText>
 *
 * @method static RuleText|Proxy createOne(array $attributes = [])
 * @method static RuleText[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static RuleText[]|Proxy[] createSequence(array|callable $sequence)
 * @method static RuleText|Proxy find(object|array|mixed $criteria)
 * @method static RuleText|Proxy findOrCreate(array $attributes)
 * @method static RuleText|Proxy first(string $sortedField = 'id')
 * @method static RuleText|Proxy last(string $sortedField = 'id')
 * @method static RuleText|Proxy random(array $attributes = [])
 * @method static RuleText|Proxy randomOrCreate(array $attributes = [])
 * @method static RuleText[]|Proxy[] all()
 * @method static RuleText[]|Proxy[] findBy(array $attributes)
 * @method static RuleText[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static RuleText[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method RuleText|Proxy create(array|callable $attributes = [])
 */
final class RuleTextFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return RuleText::class;
    }
}
