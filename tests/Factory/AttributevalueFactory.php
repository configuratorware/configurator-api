<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Attributevalue>
 *
 * @method static Attributevalue|Proxy createOne(array $attributes = [])
 * @method static Attributevalue[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Attributevalue[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Attributevalue|Proxy find(object|array|mixed $criteria)
 * @method static Attributevalue|Proxy findOrCreate(array $attributes)
 * @method static Attributevalue|Proxy first(string $sortedField = 'id')
 * @method static Attributevalue|Proxy last(string $sortedField = 'id')
 * @method static Attributevalue|Proxy random(array $attributes = [])
 * @method static Attributevalue|Proxy randomOrCreate(array $attributes = [])
 * @method static Attributevalue[]|Proxy[] all()
 * @method static Attributevalue[]|Proxy[] findBy(array $attributes)
 * @method static Attributevalue[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Attributevalue[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static AttributevalueRepository|RepositoryProxy repository()
 * @method Attributevalue|Proxy create(array|callable $attributes = [])
 */
final class AttributevalueFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'value' => self::faker()->word(),
        ];
    }

    protected static function getClass(): string
    {
        return Attributevalue::class;
    }
}
