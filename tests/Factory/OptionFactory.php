<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Option>
 *
 * @method static Option|Proxy createOne(array $attributes = [])
 * @method static Option[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Option[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Option|Proxy find(object|array|mixed $criteria)
 * @method static Option|Proxy findOrCreate(array $attributes)
 * @method static Option|Proxy first(string $sortedField = 'id')
 * @method static Option|Proxy last(string $sortedField = 'id')
 * @method static Option|Proxy random(array $attributes = [])
 * @method static Option|Proxy randomOrCreate(array $attributes = [])
 * @method static Option[]|Proxy[] all()
 * @method static Option[]|Proxy[] findBy(array $attributes)
 * @method static Option[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Option[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static OptionRepository|RepositoryProxy repository()
 * @method Option|Proxy create(array|callable $attributes = [])
 */
final class OptionFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Option::class;
    }
}
