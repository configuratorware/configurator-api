<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ItemOptionclassificationOption>
 *
 * @method static ItemOptionclassificationOption|Proxy createOne(array $attributes = [])
 * @method static ItemOptionclassificationOption[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ItemOptionclassificationOption[]|Proxy[] createSequence(array|callable $sequence)
 * @method static ItemOptionclassificationOption|Proxy find(object|array|mixed $criteria)
 * @method static ItemOptionclassificationOption|Proxy findOrCreate(array $attributes)
 * @method static ItemOptionclassificationOption|Proxy first(string $sortedField = 'id')
 * @method static ItemOptionclassificationOption|Proxy last(string $sortedField = 'id')
 * @method static ItemOptionclassificationOption|Proxy random(array $attributes = [])
 * @method static ItemOptionclassificationOption|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemOptionclassificationOption[]|Proxy[] all()
 * @method static ItemOptionclassificationOption[]|Proxy[] findBy(array $attributes)
 * @method static ItemOptionclassificationOption[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ItemOptionclassificationOption[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemOptionclassificationOptionRepository|RepositoryProxy repository()
 * @method ItemOptionclassificationOption|Proxy create(array|callable $attributes = [])
 */
final class ItemOptionclassificationOptionFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return ItemOptionclassificationOption::class;
    }
}
