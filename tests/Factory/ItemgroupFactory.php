<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Itemgroup>
 *
 * @method static Itemgroup|Proxy createOne(array $attributes = [])
 * @method static Itemgroup[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Itemgroup[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Itemgroup|Proxy find(object|array|mixed $criteria)
 * @method static Itemgroup|Proxy findOrCreate(array $attributes)
 * @method static Itemgroup|Proxy first(string $sortedField = 'id')
 * @method static Itemgroup|Proxy last(string $sortedField = 'id')
 * @method static Itemgroup|Proxy random(array $attributes = [])
 * @method static Itemgroup|Proxy randomOrCreate(array $attributes = [])
 * @method static Itemgroup[]|Proxy[] all()
 * @method static Itemgroup[]|Proxy[] findBy(array $attributes)
 * @method static Itemgroup[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Itemgroup[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemgroupRepository|RepositoryProxy repository()
 * @method Itemgroup|Proxy create(array|callable $attributes = [])
 */
final class ItemgroupFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Itemgroup::class;
    }
}
