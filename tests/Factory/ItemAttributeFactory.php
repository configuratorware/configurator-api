<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemattributeRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ItemAttribute>
 *
 * @method static ItemAttribute|Proxy createOne(array $attributes = [])
 * @method static ItemAttribute[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ItemAttribute[]|Proxy[] createSequence(array|callable $sequence)
 * @method static ItemAttribute|Proxy find(object|array|mixed $criteria)
 * @method static ItemAttribute|Proxy findOrCreate(array $attributes)
 * @method static ItemAttribute|Proxy first(string $sortedField = 'id')
 * @method static ItemAttribute|Proxy last(string $sortedField = 'id')
 * @method static ItemAttribute|Proxy random(array $attributes = [])
 * @method static ItemAttribute|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemAttribute[]|Proxy[] all()
 * @method static ItemAttribute[]|Proxy[] findBy(array $attributes)
 * @method static ItemAttribute[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ItemAttribute[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemattributeRepository|RepositoryProxy repository()
 * @method ItemAttribute|Proxy create(array|callable $attributes = [])
 */
final class ItemAttributeFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return ItemAttribute::class;
    }
}
