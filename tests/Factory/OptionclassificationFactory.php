<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Optionclassification>
 *
 * @method static Optionclassification|Proxy createOne(array $attributes = [])
 * @method static Optionclassification[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Optionclassification[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Optionclassification|Proxy find(object|array|mixed $criteria)
 * @method static Optionclassification|Proxy findOrCreate(array $attributes)
 * @method static Optionclassification|Proxy first(string $sortedField = 'id')
 * @method static Optionclassification|Proxy last(string $sortedField = 'id')
 * @method static Optionclassification|Proxy random(array $attributes = [])
 * @method static Optionclassification|Proxy randomOrCreate(array $attributes = [])
 * @method static Optionclassification[]|Proxy[] all()
 * @method static Optionclassification[]|Proxy[] findBy(array $attributes)
 * @method static Optionclassification[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Optionclassification[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static OptionclassificationRepository|RepositoryProxy repository()
 * @method Optionclassification|Proxy create(array|callable $attributes = [])
 */
final class OptionclassificationFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Optionclassification::class;
    }
}
