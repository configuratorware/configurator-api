<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<DesignAreaDesignProductionMethod>
 *
 * @method static DesignAreaDesignProductionMethod|Proxy createOne(array $attributes = [])
 * @method static DesignAreaDesignProductionMethod[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static DesignAreaDesignProductionMethod[]|Proxy[] createSequence(array|callable $sequence)
 * @method static DesignAreaDesignProductionMethod|Proxy find(object|array|mixed $criteria)
 * @method static DesignAreaDesignProductionMethod|Proxy findOrCreate(array $attributes)
 * @method static DesignAreaDesignProductionMethod|Proxy first(string $sortedField = 'id')
 * @method static DesignAreaDesignProductionMethod|Proxy last(string $sortedField = 'id')
 * @method static DesignAreaDesignProductionMethod|Proxy random(array $attributes = [])
 * @method static DesignAreaDesignProductionMethod|Proxy randomOrCreate(array $attributes = [])
 * @method static DesignAreaDesignProductionMethod[]|Proxy[] all()
 * @method static DesignAreaDesignProductionMethod[]|Proxy[] findBy(array $attributes)
 * @method static DesignAreaDesignProductionMethod[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static DesignAreaDesignProductionMethod[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static DesignAreaDesignProductionMethodRepository|RepositoryProxy repository()
 * @method DesignAreaDesignProductionMethod|Proxy create(array|callable $attributes = [])
 */
final class DesignAreaDesignProductionMethodFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return DesignAreaDesignProductionMethod::class;
    }
}
