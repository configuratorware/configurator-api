<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Configurationtype>
 *
 * @method static Configurationtype|Proxy createOne(array $attributes = [])
 * @method static Configurationtype[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Configurationtype[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Configurationtype|Proxy find(object|array|mixed $criteria)
 * @method static Configurationtype|Proxy findOrCreate(array $attributes)
 * @method static Configurationtype|Proxy first(string $sortedField = 'id')
 * @method static Configurationtype|Proxy last(string $sortedField = 'id')
 * @method static Configurationtype|Proxy random(array $attributes = [])
 * @method static Configurationtype|Proxy randomOrCreate(array $attributes = [])
 * @method static Configurationtype[]|Proxy[] all()
 * @method static Configurationtype[]|Proxy[] findBy(array $attributes)
 * @method static Configurationtype[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Configurationtype[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ConfigurationtypeRepository|RepositoryProxy repository()
 * @method Configurationtype|Proxy create(array|callable $attributes = [])
 */
final class ConfigurationtypeFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'identifier' => self::faker()->text(),
        ];
    }

    protected function initialize(): self
    {
        return $this;
    }

    protected static function getClass(): string
    {
        return Configurationtype::class;
    }
}
