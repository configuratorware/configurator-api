<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Attribute>
 *
 * @method static Attribute|Proxy createOne(array $attributes = [])
 * @method static Attribute[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Attribute[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Attribute|Proxy find(object|array|mixed $criteria)
 * @method static Attribute|Proxy findOrCreate(array $attributes)
 * @method static Attribute|Proxy first(string $sortedField = 'id')
 * @method static Attribute|Proxy last(string $sortedField = 'id')
 * @method static Attribute|Proxy random(array $attributes = [])
 * @method static Attribute|Proxy randomOrCreate(array $attributes = [])
 * @method static Attribute[]|Proxy[] all()
 * @method static Attribute[]|Proxy[] findBy(array $attributes)
 * @method static Attribute[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Attribute[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static AttributeRepository|RepositoryProxy repository()
 * @method Attribute|Proxy create(array|callable $attributes = [])
 */
final class AttributeFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
            'identifier' => self::faker()->word(),
            'attributedatatype' => self::faker()->randomElement(['text', 'number', 'integer', 'boolean']),
        ];
    }

    protected static function getClass(): string
    {
        return Attribute::class;
    }
}
