<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<ItemOptionclassification>
 *
 * @method static ItemOptionclassification|Proxy createOne(array $attributes = [])
 * @method static ItemOptionclassification[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static ItemOptionclassification[]|Proxy[] createSequence(array|callable $sequence)
 * @method static ItemOptionclassification|Proxy find(object|array|mixed $criteria)
 * @method static ItemOptionclassification|Proxy findOrCreate(array $attributes)
 * @method static ItemOptionclassification|Proxy first(string $sortedField = 'id')
 * @method static ItemOptionclassification|Proxy last(string $sortedField = 'id')
 * @method static ItemOptionclassification|Proxy random(array $attributes = [])
 * @method static ItemOptionclassification|Proxy randomOrCreate(array $attributes = [])
 * @method static ItemOptionclassification[]|Proxy[] all()
 * @method static ItemOptionclassification[]|Proxy[] findBy(array $attributes)
 * @method static ItemOptionclassification[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static ItemOptionclassification[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemOptionclassificationRepository|RepositoryProxy repository()
 * @method ItemOptionclassification|Proxy create(array|callable $attributes = [])
 */
final class ItemOptionclassificationFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return ItemOptionclassification::class;
    }
}
