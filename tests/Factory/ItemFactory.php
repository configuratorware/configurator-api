<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Item>
 *
 * @method static Item|Proxy createOne(array $attributes = [])
 * @method static Item[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Item[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Item|Proxy find(object|array|mixed $criteria)
 * @method static Item|Proxy findOrCreate(array $attributes)
 * @method static Item|Proxy first(string $sortedField = 'id')
 * @method static Item|Proxy last(string $sortedField = 'id')
 * @method static Item|Proxy random(array $attributes = [])
 * @method static Item|Proxy randomOrCreate(array $attributes = [])
 * @method static Item[]|Proxy[] all()
 * @method static Item[]|Proxy[] findBy(array $attributes)
 * @method static Item[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Item[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemRepository|RepositoryProxy repository()
 * @method Item|Proxy create(array|callable $attributes = [])
 */
final class ItemFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Item::class;
    }
}
