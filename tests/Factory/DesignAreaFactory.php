<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<DesignArea>
 *
 * @method static DesignArea|Proxy createOne(array $attributes = [])
 * @method static DesignArea[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static DesignArea[]|Proxy[] createSequence(array|callable $sequence)
 * @method static DesignArea|Proxy find(object|array|mixed $criteria)
 * @method static DesignArea|Proxy findOrCreate(array $attributes)
 * @method static DesignArea|Proxy first(string $sortedField = 'id')
 * @method static DesignArea|Proxy last(string $sortedField = 'id')
 * @method static DesignArea|Proxy random(array $attributes = [])
 * @method static DesignArea|Proxy randomOrCreate(array $attributes = [])
 * @method static DesignArea[]|Proxy[] all()
 * @method static DesignArea[]|Proxy[] findBy(array $attributes)
 * @method static DesignArea[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static DesignArea[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static DesignAreaRepository|RepositoryProxy repository()
 * @method DesignArea|Proxy create(array|callable $attributes = [])
 */
final class DesignAreaFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return DesignArea::class;
    }
}
