<?php

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Factory;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemtextRepository;
use Zenstruck\Foundry\ModelFactory;
use Zenstruck\Foundry\Proxy;
use Zenstruck\Foundry\RepositoryProxy;

/**
 * @extends ModelFactory<Itemtext>
 *
 * @method static Itemtext|Proxy createOne(array $attributes = [])
 * @method static Itemtext[]|Proxy[] createMany(int $number, array|callable $attributes = [])
 * @method static Itemtext[]|Proxy[] createSequence(array|callable $sequence)
 * @method static Itemtext|Proxy find(object|array|mixed $criteria)
 * @method static Itemtext|Proxy findOrCreate(array $attributes)
 * @method static Itemtext|Proxy first(string $sortedField = 'id')
 * @method static Itemtext|Proxy last(string $sortedField = 'id')
 * @method static Itemtext|Proxy random(array $attributes = [])
 * @method static Itemtext|Proxy randomOrCreate(array $attributes = [])
 * @method static Itemtext[]|Proxy[] all()
 * @method static Itemtext[]|Proxy[] findBy(array $attributes)
 * @method static Itemtext[]|Proxy[] randomSet(int $number, array $attributes = [])
 * @method static Itemtext[]|Proxy[] randomRange(int $min, int $max, array $attributes = [])
 * @method static ItemtextRepository|RepositoryProxy repository()
 * @method Itemtext|Proxy create(array|callable $attributes = [])
 */
final class ItemtextFactory extends ModelFactory
{
    protected function getDefaults(): array
    {
        return [
        ];
    }

    protected static function getClass(): string
    {
        return Itemtext::class;
    }
}
