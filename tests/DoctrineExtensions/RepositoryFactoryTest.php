<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\DoctrineExtensions;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\DoctrineExtensions\RepositoryFactory;

class RepositoryFactoryTest extends TestCase
{
    /**
     * @var EntityManagerInterface|\Phake_IMock
     * @Mock EntityManagerInterface
     */
    private $entityManager;

    protected function setUp(): void
    {
        \Phake::initAnnotations($this);
    }

    public function testShouldCreateRepository()
    {
        $repositoryFactory = new RepositoryFactory($this->entityManager);

        \Phake::when($this->entityManager)->getClassMetadata->thenReturn(new ClassMetadata(TestEntity::class));

        $repository = $repositoryFactory->createRepository(TestEntity::class, TestRepository::class);

        self::assertInstanceOf(TestRepository::class, $repository);
    }
}

final class TestRepository extends Repository
{
}

final class TestEntity
{
}
