<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class DesignViewRepositoryTest extends ApiTestCase
{
    /**
     * @var DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->designViewRepository = $this->em->getRepository(DesignView::class);
        $this->itemRepository = $this->em->getRepository(Item::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_data/DesignViewRepositoryTest/softshell_designer_2d.sql'));
    }

    public function testShouldReturnViewsWithoutDesignArea()
    {
        /** @var Item $item */
        $item = $this->itemRepository->findOneForConfigurationMode('softshell_designer_2d_copy');

        $designViews = $this->designViewRepository->findByItemAndHasDesignAreaRelation($item);
        $expectedIds = [3002, 3004, 3003];

        foreach ($designViews as $designView) {
            self::assertEquals(array_shift($expectedIds), $designView->getId());
        }
    }
}
