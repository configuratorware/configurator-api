INSERT IGNORE INTO item (id, identifier,  minimum_order_amount, accumulate_amounts, date_created, date_updated, date_deleted, visualization_mode_id, item_status_id, configuration_mode, overwrite_component_order)
VALUES (4003,  'softshell_creator2_2d', 1, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1, 2, 'creator', 0);

INSERT IGNORE INTO optionclassification (id, identifier, sequencenumber, date_created, date_updated, date_deleted, hidden_in_frontend)
VALUES (4001,  'softshell_body', 1, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (4002, 'softshell_zipper', 3, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (4003, 'softshell_application', 4, NOW(), NOW(), '0001-01-01 00:00:00', 0);

INSERT IGNORE INTO `option` (id, parent_id, sequencenumber, identifier, date_created, date_updated, date_deleted)
VALUES (4001, null, 1, 'softshell_red', NOW(), NOW(), '0001-01-01 00:00:00'),
       (4002, null, 2, 'softshell_avocado', NOW(), NOW(), '0001-01-01 00:00:00'),
       (4007, null, 1, 'softshell_redblack', NOW(), NOW(), '0001-01-01 00:00:00'),
       (4008, null, 2, 'softshell_whitegreen', NOW(), NOW(), '0001-01-01 00:00:00');

INSERT IGNORE INTO item_optionclassification (id, item_id, optionclassification_id, is_multiselect, minamount, maxamount, is_mandatory, date_created, date_updated, date_deleted, sequence_number)
VALUES (4001, 4003, 4001, 0, null, null, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1),
       (4002, 4003, 4002, 0, null, null, 1, NOW(), NOW(), '0001-01-01 00:00:00', 2),
       (4003, 4003, 4003, 0, null, null, 1, NOW(), NOW(), '0001-01-01 00:00:00', 3);

INSERT IGNORE INTO item_optionclassification_option (id, item_optionclassification_id, option_id, amountisselectable, date_created, date_updated, date_deleted)
VALUES (4001, 4001, 4001, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (4002, 4001, 4002, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (4003, 4002, 4001, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (4004, 4002, 4002, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (4005, 4003, 4001, 0, NOW(), NOW(), '0001-01-01 00:00:00'),
       (4006, 4003, 4007, 0, NOW(), NOW(), '0001-01-01 00:00:00');

INSERT INTO `option_text` (`option_id`, `language_id`, `title`, `abstract`, `description`, `searchtext`, `data`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`)
VALUES
(4001, 1, 'Red', NULL, NULL, NULL, NULL, '2021-09-06 14:44:11', '2021-09-06 14:44:18', '0001-01-01 00:00:00', NULL, NULL, NULL),
(4001, 2, 'Rot', NULL, NULL, NULL, NULL, '2021-09-06 14:44:11', '2021-09-06 14:44:18', '0001-01-01 00:00:00', NULL, NULL, NULL),
(4002, 1, 'Avocado', NULL, NULL, NULL, NULL, '2021-09-06 14:44:13', '2021-09-06 14:44:19', '0001-01-01 00:00:00', NULL, NULL, NULL),
(4002, 2, 'Avocado', NULL, NULL, NULL, NULL, '2021-09-06 14:44:13', '2021-09-06 14:44:19', '0001-01-01 00:00:00', NULL, NULL, NULL),
(4007, 1, 'Red/Black', NULL, NULL, NULL, NULL, '2021-09-06 14:44:23', '2021-09-06 14:44:23', '0001-01-01 00:00:00', NULL, NULL, NULL),
(4007, 2, 'Rot/Schwarz', NULL, NULL, NULL, NULL, '2021-09-06 14:44:23', '2021-09-06 14:44:23', '0001-01-01 00:00:00', NULL, NULL, NULL),
(4008, 1, 'Green/White', NULL, NULL, NULL, NULL, '2021-09-06 14:44:25', '2021-09-06 14:44:25', '0001-01-01 00:00:00', NULL, NULL, NULL),
(4008, 2, 'Grün/Weiß', NULL, NULL, NULL, NULL, '2021-09-06 14:44:25', '2021-09-06 14:44:25', '0001-01-01 00:00:00', NULL, NULL, NULL);
