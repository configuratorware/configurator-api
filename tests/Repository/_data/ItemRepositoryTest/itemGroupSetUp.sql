
INSERT INTO item(id, identifier, accumulate_amounts, configurable, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id, visualization_mode_id, item_status_id)
VALUES (5005, 'hoodie', 1, NULL, false, '2019-02-01 11:00:38', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2, 2),
       (5006, 'hoodie_red_s', 1, NULL, false, '2019-02-01 11:00:57', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, 2),
       (5007, 'hoodie_red_m', 1, NULL, false, '2019-02-01 11:01:12', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, 2),
       (5008, 'hoodie_red_l', 1, NULL, false, '2019-02-01 11:01:19', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, 1),
       (5009, 'hoodie_blue_s', 1, NULL, false, '2019-02-01 11:01:32', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, 1),
       (5010, 'hoodie_blue_m', 1, NULL, false, '2019-02-01 11:01:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL),
       (5011, 'hoodie_blue_l', 1, NULL, false, '2019-02-01 11:01:45', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 5005, NULL, NULL);
