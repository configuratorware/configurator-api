INSERT INTO itemclassification(id, identifier, sequencenumber, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, minimum_order_amount)
VALUES (1000, 'vehicle', 2, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 10),
       (1001, 'plants', 3, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 10),
       (1002, 'clothes', 4, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 100);

INSERT INTO item(id, identifier, externalid, configurable, deactivated, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, parent_id, item_status_id, override_option_order)
VALUES (6000, 'hoodie', NULL, NULL, 0, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2, 0),
       (6001, 'car', NULL, NULL, 0, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2, 1),
       (6002, 'bike', NULL, NULL, 0, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2, 0),
       (6003, 'boat', NULL, NULL, 0, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 2, 0),
       (6004, 'hoodie_xl', NULL, NULL, 0, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 6000, 2, 0);

INSERT INTO item_itemclassification(id, itemclassification_id, item_id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES (1000, 1002, 6000, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
       (1001, 1000, 6001, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
       (1002, 1000, 6002, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
       (1003, 1000, 6003, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
       (1004, 1002, 6004, NOW(), NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);
