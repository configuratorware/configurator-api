INSERT INTO `item` (`id`, `identifier`, `minimum_order_amount`, `accumulate_amounts`, `date_created`, `date_updated`, `date_deleted`, `visualization_mode_id`, `item_status_id`, `configuration_mode`)
VALUES
    (3053, 'softshell_designer_2d_copy', 1, 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00', 1, 2, 'designer');

INSERT INTO `configuration` (`id`, `configurationtype_id`, `item_id`, `client_id`, `externaluser_id`, `code`, `name`, `selectedoptions`, `designdata`, `customdata`, `partslisthash`, `date_created`, `date_updated`, `date_deleted`)
VALUES
    (3002, 2, 3053, NULL, NULL, '960XCH2HN', NULL, '{\"softshell_body\":[{\"identifier\":\"softshell_red\",\"amount\":1}],\"softshell_hood\":[{\"identifier\":\"softshell_red\",\"amount\":1}],\"softshell_zipper\":[{\"identifier\":\"softshell_red\",\"amount\":1}],\"softshell_application\":[{\"identifier\":\"softshell_redblack\",\"amount\":1}]}', NULL, NULL, NULL, '2021-07-02 10:38:43', '2021-07-02 10:38:43', '0001-01-01 00:00:00');

INSERT INTO `design_area` (`id`, `item_id`, `identifier`, `sequence_number`, `height`, `width`, `date_created`, `date_updated`, `date_deleted`)
VALUES
    (3002, 3053, 'chest', 1, 100, 100, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
    (3003, 3053, 'sleeve', 2, 100, 140, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
    (3004, 3053, 'back_center', 3, 500, 400, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');

INSERT INTO `design_view` (`id`, `item_id`, `identifier`, `sequence_number`, `date_created`, `date_updated`, `date_deleted`)
VALUES
    (3002, 3053, 'front_view', 1, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
    (3003, 3053, 'side_view', 6, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
    (3004, 3053, 'back_view', 3, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');

INSERT INTO `design_view_design_area` (`id`, `design_area_id`, `design_view_id`, `base_shape`, `position`, `custom_data`, `is_default`, `date_created`, `date_updated`, `date_deleted`)
VALUES
    (3001, 3002, 3002, '{\"type\":\"Plane\",\"width\":144,\"height\":144,\"x\":-179.99999999999997,\"y\":-68.18181818181799,\"rotation\":{\"x\":0,\"y\":0,\"z\":0,\"order\":\"XYZ\"}}', '{\"x\":72,\"y\":72,\"width\":144,\"height\":144}', NULL, 0, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
    (3002, 3003, 3002, '{\"type\":\"Cylinder\",\"width\":173,\"height\":124,\"x\":286,\"y\":-16,\"rotation\":{\"x\":0.1213,\"y\":0.6705,\"z\":0.17792256549810162,\"order\":\"XYZ\"},\"r\":64}', '{\"x\":86,\"y\":62,\"width\":172,\"height\":123}', NULL, 0, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00'),
    (3003, 3003, 3003, '{\"type\":\"Cylinder\",\"width\":101,\"height\":72,\"x\":330.0000000000003,\"y\":21.81818181818182,\"r\":128,\"rotation\":{\"x\":9.02129208547617e-17,\"y\":0.012721785742363836,\"z\":0.2679774739697903,\"order\":\"XYZ\"},\"inside\":false}', '{\"x\":50,\"y\":36,\"width\":71,\"height\":100}', NULL, 0, '2021-06-30 11:05:27', '2021-06-30 11:05:27', '0001-01-01 00:00:00');

