<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback;
use Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype;
use Redhotmagma\ConfiguratorApiBundle\Repository\RulefeedbackRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuletypeRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\ItemFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\OptionFactory;
use Tests\Redhotmagma\ConfiguratorApiBundle\Factory\RuleFactory;

class RuleRepositoryTest extends KernelTestCase
{
    private RuletypeRepository $ruletypeRepository;
    private RulefeedbackRepository $rulefeedbackRepository;
    private RuleRepository $ruleRepository;

    public function setUp(): void
    {
        $kernel = self::bootKernel();

        $em = $kernel->getContainer()->get('doctrine')->getManager();
        $this->ruletypeRepository = $em->getRepository(Ruletype::class);
        $this->rulefeedbackRepository = $em->getRepository(Rulefeedback::class);
        $this->ruleRepository = $em->getRepository(Rule::class);
    }

    public function testShouldGetRulesCountForItem(): void
    {
        $item = ItemFactory::createOne([
            'identifier' => 'test-rule-item',
        ])->disableAutoRefresh();
        $item2 = ItemFactory::createOne([
            'identifier' => 'test-rule-item-other',
        ])->disableAutoRefresh();

        $option = OptionFactory::createOne([
            'identifier' => 'test-rule-option',
        ])->disableAutoRefresh();

        RuleFactory::createOne([
            'data' => 'test-rule-data',
            'option' => $option,
            'item' => $item,
        ]);
        RuleFactory::createOne([
            'data' => 'test-rule-data',
            'option' => $option,
            'item' => null,
        ]);
        RuleFactory::createOne([
            'data' => 'test-rule-data',
            'option' => null,
            'item' => $item,
        ]);
        RuleFactory::createOne([
            'data' => 'test-rule-data',
            'option' => $option,
            'item' => $item2,
        ]);

        $actual = $this->ruleRepository->getCountForItem('test-rule-item');

        self::assertEquals(3, $actual);
    }
}
