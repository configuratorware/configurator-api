<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class OptionRepositoryTest extends ApiTestCase
{
    /**
     * @var OptionRepository
     */
    private $repository;

    public function setUp(): void
    {
        parent::setUp();
        if (!defined('C_LANGUAGE_SETTINGS')) {
            define(
                'C_LANGUAGE_SETTINGS',
                json_decode(
                    '{"id":"1","iso":"en_GB","dateformat":"d.m.Y","pricedecimals":2,"decimalpoint":".","thousandsseparator":",","currencysymbolposition":"left"}',
                    true
                )
            );
        }
        $this->repository = $this->em->getRepository(Option::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_data/OptionRepositoryTest/softshell_creator_2d.sql'));
    }

    public function testShouldFetchOptions(): void
    {
        $expected = ['softshell_red', 'softshell_avocado'];
        $options = $this->repository->fetchOptionitemsForFrontendlist('softshell_creator2_2d', 'softshell_body', false, []);
        $actual = [];
        foreach ($options as $option) {
            $actual[] = $option->getIdentifier();
        }
        self::assertSame($expected, $actual);
    }

    public function testShouldFetchOptionWithQuery(): void
    {
        $expected = ['softshell_red'];
        $options = $this->repository->fetchOptionitemsForFrontendlist('softshell_creator2_2d', 'softshell_body', false, [], 're%');
        $actual = [];
        foreach ($options as $option) {
            $actual[] = $option->getIdentifier();
        }
        self::assertSame($expected, $actual);
    }

    public function testShouldFetchOptionWithOverrideOrder(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/OptionRepositoryTest/softshell_creator_2d_override_option_order.sql'));

        $actual = $this->repository->fetchOptionitemsForFrontendlist('softshell_creator2_2d', 'softshell_body');
        $actual = array_map(static fn (Option $option) => $option->getIdentifier(), $actual);

        self::assertEquals(['softshell_avocado', 'softshell_red'], $actual);
    }
}
