<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ItemClassificationFilter;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ItemRepositoryTest extends ApiTestCase
{
    private ItemRepository $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(Item::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_fixtures/ItemRepositoryTest/setUp.sql'));
        if (!defined('C_LANGUAGE_SETTINGS')) {
            define(
                'C_LANGUAGE_SETTINGS',
                [
                    'id' => 1,
                    'iso' => 'en_GB',
                    'dateformat' => 'm/d/Y',
                    'pricedecimals' => 2,
                    'decimalpoint' => '.',
                    'thousandsseparator' => ',',
                    'currencysymbolposition' => 'left',
                ]
            );
        }
        if (!defined('C_CHANNEL')) {
            define('C_CHANNEL', '_default');
        }
    }

    public function testShouldGetItemByIdWithOrderedComponents(): void
    {
        $expected = ['softshell_application', 'softshell_body', 'softshell_zipper'];

        $item = $this->repository->findItemByIdWithOptionClassifications(3003);

        $actual = [];
        foreach ($item->getItemOptionclassification() as $itemOptionClassification) {
            $actual[] = $itemOptionClassification->getOptionClassification()->getIdentifier();
        }

        self::assertSame($expected, $actual);
    }

    public function testShouldGetItemByIdentifierWithOrderedComponents(): void
    {
        $expected = ['coat', 'face', 'legs', 'eyes', 'ear_eyelid'];

        $item = $this->repository->findItemByIdentifierWithOptionClassificationsAndOptions('sheep');

        $actual = [];
        foreach ($item->getItemOptionclassification() as $itemOptionClassification) {
            $actual[] = $itemOptionClassification->getOptionClassification()->getIdentifier();
        }

        self::assertSame($expected, $actual);
    }

    public function testShouldGetItemsFilteredByItemClassifications(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/frontendListSetUp.sql'));

        $expectedItemIdentifiers = ['sheep', 'hoodie'];

        $filter = new ItemClassificationFilter();
        $filter->setItemClassificationsIds([1, 1002]);
        $actual = $this->repository->getFrontendList($filter, new ListRequestNoDefaultArguments());
        $actualIds = array_map(static fn (Item $item) => $item->getIdentifier(), $actual);

        self::assertEquals($expectedItemIdentifiers, $actualIds);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/frontendListTearDown.sql'));
    }

    public function testShouldGetItemsPaginated(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/frontendListSetUp.sql'));

        $expectedItemIdentifiers = ['hoodie', 'car', 'bike'];

        $arguments = new ListRequestNoDefaultArguments();
        $arguments->offset = 2;
        $arguments->limit = 3;
        $actual = $this->repository->getFrontendList(new ItemClassificationFilter(), $arguments);
        $actualIds = array_map(static fn (Item $item) => $item->getIdentifier(), $actual);

        self::assertEquals($expectedItemIdentifiers, $actualIds);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/frontendListTearDown.sql'));
    }

    public function testShouldGetOverrideOptionOrderForItem(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/frontendListSetUp.sql'));

        $actual = $this->repository->getOverrideOptionOrderForItem('car');

        self::assertTrue($actual);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/frontendListTearDown.sql'));
    }

    public function testShoulNotFindDeactivatedChildItems(): void
    {
        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/itemGroupSetUp.sql'));
        $expected = [
            'hoodie_red_s',
            'hoodie_red_m',
            'hoodie_blue_m',
            'hoodie_blue_l',
        ];

        $actual = $this->repository->findActiveByParent('hoodie');
        $identifiers = array_map(static function (Item $item) {
            return $item->getIdentifier();
        }, $actual);

        $this->assertEquals($expected, $identifiers);

        $this->executeSql(file_get_contents(__DIR__ . '/_data/ItemRepositoryTest/itemGroupTearDown.sql'));
    }
}
