<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class ItempriceRepositoryTest extends ApiTestCase
{
    /**
     * @var ItempriceRepository
     */
    private $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(Itemprice::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_fixtures/ItempriceRepositoryTest/setUp.sql'));
    }

    public function testShouldGetItemPriceByIdentifier(): void
    {
        $expected = ['price' => 199.00, 'netPrice' => 167.22];

        $price = $this->repository->getItemPriceByIdentifier('sheep', '_default');

        self::assertSame($expected, $price);
    }

    public function testShouldGetItemPriceByIdentifierAndAmountFrom(): void
    {
        $expected = ['price' => 189.00, 'netPrice' => 158.82];

        $price = $this->repository->getItemPriceByIdentifier('sheep', '_default', 10);

        self::assertSame($expected, $price);
    }

    public function testShouldGetNullPriceIfNoValidPriceWasFound(): void
    {
        $expected = ['price' => 0.0, 'netPrice' => 0.0];

        $price = $this->repository->getItemPriceByIdentifier('grond', '_default', 10);

        self::assertSame($expected, $price);
    }

    /**
     * @param array $expected
     * @param array $identifiers
     * @param string $channel
     * @dataProvider providerShouldGetBulkPricesMappedByIdentifierAndAmountFrom
     */
    public function testShouldGetBulkPricesMappedByIdentifierAndAmountFrom(
        array $expected,
        array $identifiers,
        string $channel
    ): void {
        $prices = $this->repository->getBulkPricesMappedByIdentifierAndAmountFrom($identifiers, $channel);

        foreach ($expected as $key => $expectedItem) {
            self::assertSame($expectedItem, $prices[$key]);
        }
    }

    public function providerShouldGetBulkPricesMappedByIdentifierAndAmountFrom(): array
    {
        return [
            [
                [
                    'ItempriceRepositoryTest_3' => [
                        15 => ['price' => '4.00000', 'price_net' => '3.00000'],
                        10 => ['price' => '6.00000', 'price_net' => '5.00000'],
                        5 => ['price' => '8.00000', 'price_net' => '7.00000'],
                        1 => ['price' => '10.00000', 'price_net' => '9.00000'],
                    ],
                    'ItempriceRepositoryTest_2' => [
                        15 => ['price' => '4.00000', 'price_net' => '3.00000'],
                        10 => ['price' => '6.00000', 'price_net' => '5.00000'],
                    ],
                    'ItempriceRepositoryTest_1' => [
                        5 => ['price' => '8.00000', 'price_net' => '7.00000'],
                        1 => ['price' => '10.00000', 'price_net' => '9.00000'],
                    ],
                ],
                [
                    'ItempriceRepositoryTest_1',
                    'ItempriceRepositoryTest_2',
                    'ItempriceRepositoryTest_3',
                ],
                'ItempriceRepositoryTest',
            ],
            [
                [
                    'ItempriceRepositoryTest_3' => [
                        15 => ['price' => '4.00000', 'price_net' => '3.00000'],
                        10 => ['price' => '6.00000', 'price_net' => '5.00000'],
                        5 => ['price' => '8.00000', 'price_net' => '7.00000'],
                        1 => ['price' => '10.00000', 'price_net' => '9.00000'],
                    ],
                    'ItempriceRepositoryTest_2' => [
                        15 => ['price' => '4.00000', 'price_net' => '3.00000'],
                        10 => ['price' => '6.00000', 'price_net' => '5.00000'],
                    ],
                ],
                [
                    'ItempriceRepositoryTest_2',
                    'ItempriceRepositoryTest_3',
                ],
                'ItempriceRepositoryTest',
            ],
        ];
    }
}
