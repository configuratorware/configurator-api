DELETE FROM `itemprice` WHERE `channel_id` = (SELECT id FROM channel WHERE identifier = 'ItempriceRepositoryTest');

DELETE
FROM `item`
WHERE identifier IN ('ItempriceRepositoryTest_1', 'ItempriceRepositoryTest_2', 'ItempriceRepositoryTest_3');

DELETE FROM `channel` WHERE identifier IN ('ItempriceRepositoryTest');


