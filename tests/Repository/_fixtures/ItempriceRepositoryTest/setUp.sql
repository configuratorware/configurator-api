INSERT IGNORE INTO `item` (`id`, `identifier`, `externalid`, `configurable`, `deactivated`, `minimum_order_amount`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `parent_id`, `accumulate_amounts`)
VALUES (2, 'ItempriceRepositoryTest_1', NULL, NULL, NULL, 1, '2020-07-03 16:32:01', '2020-07-26 05:22:07', '0001-01-01 00:00:00', NULL, NULL, NULL, NULL, 1),
(3, 'ItempriceRepositoryTest_2', NULL, NULL, NULL, 1, '2020-07-03 16:32:01', '2020-07-26 05:22:07', '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
(4, 'ItempriceRepositoryTest_3', NULL, NULL, NULL, 1, '2020-07-03 16:32:01', '2020-07-26 05:22:07', '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1);

INSERT INTO `channel` (`identifier`, `settings`, `is_default`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `currency_id`)
VALUES ('ItempriceRepositoryTest', NULL, 1, '2020-07-03 14:49:50', NULL, '0001-01-01 00:00:00', 0, NULL, NULL, 1);

INSERT INTO `itemprice` (`price`, `price_net`, `amountfrom`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `channel_id`, `item_id`)
VALUES (10.0000, 9.00000, 1, '2020-07-03 16:32:01', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
        (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
        (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_1')),
(8.0000, 7.00000, 5, '2020-07-03 16:32:01', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
 (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_1')),
(6.0000, 5.00000, 10, '2020-07-03 16:32:01', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
 (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_2')),
(4.0000, 3.00000, 15, '2020-07-03 16:32:01', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
 (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_2')),
(10.00000, 9.0000, 1, '2020-07-03 16:32:01', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
 (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_3')),
(8.0000, 7.0000, 5, '2020-07-03 16:32:01', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
 (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_3')),
(6.0000, 5.0000, 10, '2020-07-03 16:32:02', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
 (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_3')),
(4.0000, 3.00000, 15, '2020-07-03 16:32:02', '2020-07-26 05:21:06', '0001-01-01 00:00:00', NULL, NULL, NULL,
 (SELECT id FROM `channel` WHERE `identifier` = 'ItempriceRepositoryTest'),
 (SELECT id FROM `item` WHERE `identifier` = 'ItempriceRepositoryTest_3'));