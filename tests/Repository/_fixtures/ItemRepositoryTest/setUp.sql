INSERT IGNORE INTO item (id, identifier,  minimum_order_amount, accumulate_amounts, date_created, date_updated, date_deleted, visualization_mode_id, item_status_id, configuration_mode, overwrite_component_order)
VALUES (3003,  'softshell_creator_component_order', 1, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1, 2, 'creator', 0);

INSERT IGNORE INTO optionclassification (id, identifier, sequencenumber, date_created, date_updated, date_deleted, hidden_in_frontend)
VALUES (3001,  'softshell_body', 4, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (3002, 'softshell_zipper', 5, NOW(), NOW(), '0001-01-01 00:00:00', 0),
       (3003, 'softshell_application', 2, NOW(), NOW(), '0001-01-01 00:00:00', 0);

INSERT IGNORE INTO `option` (id, parent_id, sequencenumber, identifier, date_created, date_updated, date_deleted)
VALUES (3001, null, 1, 'softshell_red', NOW(), NOW(), '0001-01-01 00:00:00'),
       (3002, null, 2, 'softshell_avocado', NOW(), NOW(), '0001-01-01 00:00:00');

INSERT IGNORE INTO item_optionclassification (id, item_id, optionclassification_id, is_multiselect, minamount, maxamount, is_mandatory, date_created, date_updated, date_deleted, sequence_number)
VALUES (3001, 3003, 3001, 0, null, null, 1, NOW(), NOW(), '0001-01-01 00:00:00', 1),
       (3002, 3003, 3002, 0, null, null, 1, NOW(), NOW(), '0001-01-01 00:00:00', 2),
       (3003, 3003, 3003, 0, null, null, 1, NOW(), NOW(), '0001-01-01 00:00:00', 3);
