<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\KernelAwareTestTrait;

class ItemclassificationRepositoryTest extends TestCase
{
    use KernelAwareTestTrait;

    private ItemclassificationRepository $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->getRepository(Itemclassification::class);
    }

    public function testShouldFindByItemIdentifier()
    {
        $expectedSequenceNumber = 2;

        $actual = $this->repository->getNextSequenceNumber();

        self::assertEquals($expectedSequenceNumber, $actual);
    }
}
