<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOptionDeltaprice;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\KernelAwareTestTrait;

class ItemOptionclassificationOptionDeltapriceRepositoryTest extends TestCase
{
    use KernelAwareTestTrait;

    /**
     * @var ItemOptionclassificationOptionDeltapriceRepository
     */
    private $repository;

    public function setUp(): void
    {
        $this->repository = $this->getRepository(ItemOptionclassificationOptionDeltaprice::class);
    }

    public function testShouldGetDeltaPriceByIdentifiers()
    {
        $expected = ['price' => 9.76, 'netPrice' => 0.0];

        $price = $this->repository->getDeltaPriceByIdentifiers('sheep', 'coat', 'coat_red', '_default');

        self::assertSame($expected, $price);
    }

    public function testShouldGetNegativeDeltaPriceByIdentifiers()
    {
        $expected = ['price' => -7.32, 'netPrice' => 0.0];

        $price = $this->repository->getDeltaPriceByIdentifiers('sheep', 'coat', 'coat_white', '_default');

        self::assertSame($expected, $price);
    }

    public function testShouldGetNullPriceIfNoValidPriceWasFound()
    {
        $expected = ['price' => 0.0, 'netPrice' => 0.0];

        $price = $this->repository->getDeltaPriceByIdentifiers('I', 'am', 'groot', '_default');

        self::assertSame($expected, $price);
    }
}
