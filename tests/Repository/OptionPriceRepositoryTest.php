<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPrice;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class OptionPriceRepositoryTest extends ApiTestCase
{
    /**
     * @var OptionPriceRepository
     */
    private $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(OptionPrice::class);
        $this->executeSql(file_get_contents(__DIR__ . '/_fixtures/OptionPriceRepositoryTest/setUp.sql'));
    }

    public function testShouldGetNullPrice()
    {
        $expected = ['price' => 0.00, 'netPrice' => 10.0];

        $actual = $this->repository->getOptionPriceByIdentifier('option_null_price', '_default');

        self::assertSame($expected, $actual);
    }
}
