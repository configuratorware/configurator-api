<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\KernelAwareTestTrait;

class SettingRepositoryTest extends TestCase
{
    use KernelAwareTestTrait;

    /**
     * @var SettingRepository
     */
    private $repository;

    public function setUp(): void
    {
        $this->repository = $this->getRepository(Setting::class);
    }

    public function testShouldGetCalculationMethod()
    {
        $calculationMethod = $this->repository->getCalculationMethod();

        self::assertSame(SettingRepository::SUM_OF_ALL, $calculationMethod);
    }
}
