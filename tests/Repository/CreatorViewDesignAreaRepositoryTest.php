<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\ApiTestCase;

class CreatorViewDesignAreaRepositoryTest extends ApiTestCase
{
    /**
     * @var CreatorViewDesignAreaRepository
     */
    private $repository;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = $this->em->getRepository(CreatorViewDesignArea::class);
        $this->executeSql(file_get_contents(__DIR__ . '/../Controller/_data/Item/softshell_creatordesigner_2d.sql'));
    }

    public function testShouldFindByItemIdentifier()
    {
        $expectedCount = 4;

        $actual = $this->repository->findByItemIdentifier('softshell_creatordesigner_2d');

        self::assertCount($expectedCount, $actual);
    }
}
