<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Repository;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Exception\ExternalResourceException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ReleasedApiVersionPackagistApiRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\ReleasedApiVersion;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ReleasedApiVersionPackagistApiRepositoryTest extends TestCase
{
    /**
     * @var HttpClientInterface|\Phake_IMock
     * @Mock HttpClientInterface
     */
    private $client;

    /**
     * @var ResponseInterface|\Phake_IMock
     * @Mock ResponseInterface
     */
    private $response;

    /**
     * @var ReleasedApiVersionPackagistApiRepository
     */
    private $repository;

    public function setUp(): void
    {
        \Phake::initAnnotations($this);

        $this->repository = new ReleasedApiVersionPackagistApiRepository($this->client);

        \Phake::when($this->client)->request(
            'GET',
            'https://packagist.org/packages/configuratorware/configurator-api.json'
        )->thenReturn($this->response);
    }

    public function testShouldReturnApiVersions(): void
    {
        \Phake::when($this->response)->toArray()->thenReturn(
            [
                'package' => [
                    'versions' => [
                        'dev-master' => [],
                    ],
                ],
            ]
        );

        self::assertInstanceOf(ReleasedApiVersion::class, $this->repository->findVersions()[0]);
    }

    public function testShouldThrowExternalResourceExceptionWhenHttpFails(): void
    {
        $this->expectException(ExternalResourceException::class);

        \Phake::when($this->client)->request(
            'GET',
            'https://packagist.org/packages/configuratorware/configurator-api.json'
        )->thenThrow(new TransportException());

        $this->repository->findVersions();
    }

    public function testShouldThrowExternalResourceExceptionWhenSchemaIsInvalid(): void
    {
        $this->expectException(ExternalResourceException::class);

        \Phake::when($this->response)->toArray()->thenReturn([]);

        $this->repository->findVersions();
    }
}
