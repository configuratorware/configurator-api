<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\VisualizationResourcesRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\ComponentCombination;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\OptionCombination;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;
use Symfony\Component\Finder\SplFileInfo;

class VisualizationResourcesRepositoryTest extends TestCase
{
    private VisualizationResourcesRepository $repository;

    private string $itemWithComponentsIdentifier = 'visualization_test_item';

    private string $itemWithoutComponentsIdentifier = 'other_visualization_test_item';

    public function setUp(): void
    {
        $baseDir = vfsStream::setup();
        vfsStream::create(
            [
                'visualization_component' => [
                    'creator' => [
                        $this->itemWithComponentsIdentifier => [
                            'basemodel' => [
                                'base.gltf' => 'test123',
                                'base.png' => 'test123',
                                'base.json' => '{"a1": "b2"}',
                                'another-base.gltf' => 'test123',
                            ],
                            'components' => [
                                'component1' => [
                                    'component.gltf' => 'test12345',
                                    'component.jpg' => 'jpg',
                                    'options' => [
                                        'option1' => [
                                            'fileoption2.png' => 'test',
                                        ],
                                    ],
                                ],
                                'component1+component2' => [
                                    'component1_component2.gltf' => 'test1234456',
                                    'options' => [
                                        'option1+option2' => [
                                            'file.png' => 'test',
                                            'file.jpg' => 'jpg-content',
                                            'file1.gltf' => 'gltf file',
                                            'fil1231.json' => '{"a":"b"}',
                                        ],
                                    ],
                                ],
                                'component3' => [
                                    'component3.gltf' => 'test1234567',
                                    'component3.jpg' => 'jpg',
                                    'options' => [
                                        'option1' => [
                                            'fileoption1.png' => 'test',
                                        ],
                                        'option2' => [
                                            'fileoption2.png' => 'test',
                                        ],
                                        'option3' => [
                                            'fileoption3.png' => 'test',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        $this->itemWithoutComponentsIdentifier => [
                            'basemodel' => [
                                'base.gltf' => 'test123',
                                'base.png' => 'test123',
                                'base.json' => '{"a1": "b2"}',
                            ],
                        ],
                    ],
                ],
            ],
            $baseDir
        );
        $pattern = $baseDir->url() . '/visualization_component/{configurationMode}/{itemIdentifier}';
        $this->repository = new VisualizationResourcesRepository($pattern);
    }

    public function testShouldFindComponentCombinationsForIdentifier()
    {
        $expected =
            [
                new ComponentCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1')),
                new ComponentCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1+component2')),
                new ComponentCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component3')),
            ];

        $actual = $this->repository->findComponentCombinationsFor($this->itemWithComponentsIdentifier);

        self::assertEquals($expected, $actual);
        self::assertEquals(['component1'], $actual[0]->components);
        self::assertEquals(['component1', 'component2'], $actual[1]->components);
        self::assertEquals(['component3'], $actual[2]->components);
    }

    public function testShouldFindOptionCombinationsForComponentCombinationWithSimpleComponents()
    {
        $configuration = $this->generateDefaultConfiguration();
        $componentCombination = new ComponentCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1'));

        $expected = new OptionCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1/options/option1'));

        $actual = $this->repository->findOptionsFor($componentCombination, $configuration);

        self::assertEquals([$expected], $actual);
    }

    public function testShouldFindOptionCombinationsForComponentCombinationWithCombinedComponents()
    {
        $configuration = $this->generateDefaultConfiguration();
        $configuration->optionclassifications[] = $this->appendOptionTo($this->generateOptionClassifications('component2'), 'option2');
        $componentCombination = new ComponentCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1+component2'));

        $expected = new OptionCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1+component2/options/option1+option2'));

        $actual = $this->repository->findOptionsFor($componentCombination, $configuration);

        self::assertEquals([$expected], $actual);
    }

    public function testShouldFindOptionCombinationsForComponentCombinationWithSimpleComponentsAndMultipleSelectedOptions()
    {
        $configuration = $this->generateDefaultConfiguration();
        $configuration->optionclassifications = [];
        $component3 = $this->generateOptionClassifications('component3');
        $this->appendOptionTo($component3, 'option1');
        $this->appendOptionTo($component3, 'option3');
        $configuration->optionclassifications[] = $component3;
        $componentCombination = new ComponentCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component3'));

        $expected = [
            new OptionCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component3/options/option1')),
            new OptionCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component3/options/option3')),
        ];

        $actual = $this->repository->findOptionsFor($componentCombination, $configuration);

        self::assertEquals($expected, $actual);
    }

    public function testShouldFindVisualizationDataForSimpleOptionCombination()
    {
        $optionCombination = new OptionCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1/options/option1'));
        $expectedFilePath = 'vfs://root/visualization_component/creator/visualization_test_item/components/component1/options/option1/fileoption2.png';
        $actual = $this->repository->findVisualizationFilesForOption($optionCombination);

        self::assertCount(1, $actual);

        $actualFile = $actual[$expectedFilePath];
        self::assertInstanceOf(SplFileInfo::class, $actualFile);
        self::assertEquals($expectedFilePath, $actualFile->getPathname());
    }

    public function testShouldFindVisualizationDataForCombinedOptionCombination()
    {
        $optionCombination = new OptionCombination(new \SplFileInfo('vfs://root/visualization_component/creator/visualization_test_item/components/component1+component2/options/option1+option2'));

        $expectedPath = 'vfs://root/visualization_component/creator/visualization_test_item/components/component1+component2/options/option1+option2/';
        $expectedFiles = ['file.png', 'file.jpg', 'file1.gltf', 'fil1231.json'];
        $expectedFilePaths = [
            $expectedPath . $expectedFiles[0],
            $expectedPath . $expectedFiles[1],
            $expectedPath . $expectedFiles[2],
            $expectedPath . $expectedFiles[3],
        ];

        $actual = $this->repository->findVisualizationFilesForOption($optionCombination);

        self::assertCount(4, $actual);

        foreach ($expectedFilePaths as $expectedFilePath) {
            $actualFile = $actual[$expectedFilePath];
            self::assertInstanceOf(SplFileInfo::class, $actualFile);
            self::assertEquals($expectedFilePath, $actualFile->getPathname());
        }
    }

    public function testShouldFindVisualizationDataForBaseModel()
    {
        $expectedPath = 'vfs://root/visualization_component/creator/visualization_test_item/basemodel/';
        $expectedFiles = ['base.gltf', 'base.png', 'base.json', 'another-base.gltf'];
        $expectedFilePaths = [
            $expectedPath . $expectedFiles[0],
            $expectedPath . $expectedFiles[1],
            $expectedPath . $expectedFiles[2],
            $expectedPath . $expectedFiles[3],
        ];

        $actual = $this->repository->findBaseModelFor($this->itemWithComponentsIdentifier);

        self::assertCount(4, $actual);

        foreach ($expectedFilePaths as $expectedFilePath) {
            $actualFile = $actual[$expectedFilePath];
            self::assertInstanceOf(SplFileInfo::class, $actualFile);
            self::assertEquals($expectedFilePath, $actualFile->getPathname());
        }
    }

    public function testNoErrorWhenComponentFolderMissing()
    {
        $expected = [];

        $actual = $this->repository->findComponentCombinationsFor($this->itemWithoutComponentsIdentifier);

        self::assertEquals($expected, $actual);
    }

    private function generateDefaultConfiguration()
    {
        $configuration = new Configuration();
        $configuration->itemIdentifier = $this->itemWithComponentsIdentifier;
        $configuration->optionclassifications[] = $this->appendOptionTo($this->generateOptionClassifications('component1'), 'option1');

        return $configuration;
    }

    private function generateOptionClassifications(string $identifier): OptionClassification
    {
        $component2 = new OptionClassification();
        $component2->identifier = $identifier;

        return $component2;
    }

    private function appendOptionTo(OptionClassification $optionClassification, string $optionIdentifier): OptionClassification
    {
        $option = new Option();
        $option->identifier = $optionIdentifier;
        $optionClassification->selectedoptions[] = $option;

        return $optionClassification;
    }
}
