<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDirectory;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\DesignViewImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\DesignViewDetailImage;
use Symfony\Component\Filesystem\Filesystem;

class DesignViewImageRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        vfsStream::setup();
    }

    /**
     * @param array $expectedImageState
     * @param array $givenFiles
     * @param Item $item
     * @param DesignView $designView
     *
     * @dataProvider providerShouldFindImage
     */
    public function testShouldFindImage(array $expectedImageState, array $givenFiles, Item $item, DesignView $designView): void
    {
        $root = vfsStream::create($givenFiles);
        $expectedImageState['realPath'] = $root->url() . $expectedImageState['url'];
        $expectedImage = DesignViewDetailImage::from(...array_values($expectedImageState));

        $repository = new DesignViewImageRepository('/view_images/', new Filesystem(), $root->url());

        $resultImage = $repository->findDetailImage($item, $designView);

        self::assertEquals($expectedImage, $resultImage);
    }

    public function providerShouldFindImage(): \Generator
    {
        yield [
            [
                'url' => '/view_images/parent_item_1/view_1/parent_item_1.jpg',
                'designViewIdentifier' => 'view_1',
                'parentIdentifier' => 'parent_item_1',
                'childIdentifier' => 'parent_item_1',
                'realPath' => '',
                'filename' => 'parent_item_1.jpg',
            ],
            [
                'view_images' => [
                    'parent_item_1' => [
                        'view_1' => [
                            'parent_item_1.jpg' => 'parent item image',
                        ],
                    ],
                ],
            ],
            $this->generateParentItem('parent_item_1'),
            $this->generateDesignView('view_1'),
        ];
        yield [
            [
                'url' => '/view_images/parent_item_1/view_1/child_item_1.png',
                'designViewIdentifier' => 'view_1',
                'parentIdentifier' => 'parent_item_1',
                'childIdentifier' => 'child_item_1',
                'realPath' => '',
                'filename' => 'child_item_1.png',
            ],
            [
                'view_images' => [
                    'parent_item_1' => [
                        'view_1' => [
                            'child_item_1.png' => 'parent item image',
                        ],
                    ],
                ],
            ],
            $this->generateChildItem('parent_item_1', 'child_item_1'),
            $this->generateDesignView('view_1'),
        ];
    }

    public function testShouldThrowInvalidDirectory(): void
    {
        $this->expectException(InvalidDirectory::class);

        $root = vfsStream::create([]);

        $repository = new DesignViewImageRepository('/view_images/', new Filesystem(), $root->url());

        $repository->findDetailImage($this->generateParentItem('test'), $this->generateDesignView('test'));
    }

    public function testShouldThrowFileException(): void
    {
        $this->expectException(FileException::class);

        $root = vfsStream::create([
            'view_images' => [
                'parent' => [
                    'view' => [],
                ],
            ],
        ]);

        $repository = new DesignViewImageRepository('/view_images/', new Filesystem(), $root->url());

        $repository->findDetailImage($this->generateParentItem('parent'), $this->generateDesignView('view'));
    }

    public function testShouldCopyImage(): void
    {
        $root = vfsStream::create([
            'tmp' => ['WQQWPN' => 'test image'],
        ]);

        // sanity check
        self::assertFileExists($root->url() . '/tmp/WQQWPN');
        self::assertFileNotExists($root->url() . '/view_images/parent/view/child.png');

        $repository = new DesignViewImageRepository('/view_images/', new Filesystem(), $root->url());

        $givenImage = DesignViewDetailImage::from(null, 'view', 'parent', 'child', $root->url() . '/tmp/WQQWPN', 'test.png');

        $repository->saveDetailImage($givenImage);

        self::assertFileExists($root->url() . '/tmp/WQQWPN');
        self::assertFileExists($root->url() . '/view_images/parent/view/child.png');
    }

    public function testShouldCopyImageAndDeletePreviousUpload(): void
    {
        $root = vfsStream::create([
            'tmp' => ['WQQWPN' => 'test image'],
            'view_images' => [
                'parent' => [
                    'view' => [
                        'child.jpg' => 'parent item image',
                    ],
                ],
            ],
        ]);

        // sanity check
        self::assertFileExists($root->url() . '/tmp/WQQWPN');
        self::assertFileExists($root->url() . '/view_images/parent/view/child.jpg');
        self::assertFileNotExists($root->url() . '/view_images/parent/view/child.png');

        $repository = new DesignViewImageRepository('/view_images/', new Filesystem(), $root->url());

        $givenImage = DesignViewDetailImage::from(null, 'view', 'parent', 'child', $root->url() . '/tmp/WQQWPN', 'test.png');

        $repository->saveDetailImage($givenImage);

        self::assertFileExists($root->url() . '/tmp/WQQWPN');
        self::assertFileNotExists($root->url() . '/view_images/parent/view/child.jpg');
        self::assertFileExists($root->url() . '/view_images/parent/view/child.png');
    }

    public function testShouldDeleteImage(): void
    {
        $root = vfsStream::create([
            'view_images' => [
                'parent' => [
                    'view' => [
                        'child.jpg' => 'parent item image',
                        'keep_child.jpg' => 'parent item image',
                    ],
                ],
            ],
        ]);

        // sanity check
        self::assertFileExists($root->url() . '/view_images/parent/view/child.jpg');
        self::assertFileExists($root->url() . '/view_images/parent/view/keep_child.jpg');

        $repository = new DesignViewImageRepository('/view_images/', new Filesystem(), $root->url());

        $givenImage = DesignViewDetailImage::from(null, 'view', 'parent', 'child', $root->url() . '/view_images/parent/view/child.jpg', 'child.png');

        $repository->deleteDetailImage($givenImage);

        self::assertFileNotExists($root->url() . '/view_images/parent/view/child.jpg');
        self::assertFileExists($root->url() . '/view_images/parent/view/keep_child.jpg');
    }

    private function generateChildItem(string $parentIdentifier, string $childIdentifier): Item
    {
        $parent = (new Item())->setIdentifier($parentIdentifier);
        $child = (new Item())->setIdentifier($childIdentifier);

        $child->setParent($parent);

        return $child;
    }

    private function generateParentItem(string $identifier): Item
    {
        return (new Item())->setIdentifier($identifier);
    }

    private function generateDesignView(string $identifier): DesignView
    {
        return (new DesignView())->setIdentifier($identifier);
    }
}
