<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\InspirationImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\InspirationThumbnail;
use Symfony\Component\Filesystem\Filesystem;
use Tests\Redhotmagma\ConfiguratorApiBundle\Test\EntityTestTrait;

final class InspirationImageRepositoryTest extends TestCase
{
    use EntityTestTrait;

    public function setUp(): void
    {
        vfsStream::setup();
    }

    public function testShouldFindThumbnail(): void
    {
        $root = vfsStream::create([
            'images' => [
                'thumbnails' => [
                    'item_identifier_1' => [
                        '5.jpg' => 'mock file content',
                    ],
                ],
            ],
        ]);

        $repository = new InspirationImageRepository(new Filesystem(), $root->url(), 'images/thumbnails');

        $inspiration = $this->generateInspiration(5, '/images/thumbnails/item_identifier_1/5.jpg', $this->generateItem('item_identifier_1'));
        $thumbnail = $repository->findThumbnail($inspiration);

        self::assertEquals('/images/thumbnails/item_identifier_1/5.jpg', $thumbnail->getUrl());
    }

    public function testShouldSaveThumbnail(): void
    {
        $root = vfsStream::create([
            'tmp' => [
                'QWLEQIT' => 'I am a testfile',
            ],
        ]);

        $repository = new InspirationImageRepository(new Filesystem(), $root->url(), 'images/thumbnails');

        $thumbnail = InspirationThumbnail::from(null, 18, 'item_identifier_save', $root->url() . '/tmp/QWLEQIT', 'test.jpg');

        $inspiration = $this->generateInspiration(18, null, $this->generateItem('item_identifier_save'));
        $thumbnail = $repository->saveThumbnail($thumbnail, $inspiration);

        $expectedUrl = '/images/thumbnails/item_identifier_save/18.jpg';
        $expectedFile = $root->url() . $expectedUrl;
        self::assertFileExists($expectedFile);
        self::assertEquals($thumbnail->getRealPath(), $expectedFile);
        self::assertEquals($thumbnail->getUrl(), $expectedUrl);
    }

    public function testShouldChangeThumbnail(): void
    {
        $root = vfsStream::create([
            'tmp' => [
                'QWLEQIT' => 'I am a testfile',
            ],
            'images' => [
                'thumbnails' => [
                    'item_identifier_change' => [
                        '23.png' => 'mock file content',
                    ],
                ],
            ],
        ]);

        // sanity check
        $existingUrl = '/images/thumbnails/item_identifier_change/23.png';
        $existingFile = $root->url() . $existingUrl;
        self::assertFileExists($existingFile);

        $repository = new InspirationImageRepository(new Filesystem(), $root->url(), 'images/thumbnails');

        $thumbnail = InspirationThumbnail::from(null, 18, 'item_identifier_change', $root->url() . '/tmp/QWLEQIT', 'test.jpg');

        $inspiration = $this->generateInspiration(18, $existingUrl, $this->generateItem('item_identifier_change'));
        $thumbnail = $repository->saveThumbnail($thumbnail, $inspiration);

        $expectedFile = $root->url() . '/images/thumbnails/item_identifier_change/18.jpg';
        self::assertFileExists($expectedFile);
        self::assertEquals($thumbnail->getRealPath(), $expectedFile);

        self::assertFileNotExists($existingFile);
    }

    public function testShouldDeleteThumbnail(): void
    {
        $root = vfsStream::create([
            'images' => [
                'thumbnails' => [
                    'item_identifier_delete' => [
                        '23.png' => 'mock file content',
                    ],
                ],
            ],
        ]);

        $thumbnailPath = $root->url() . '/images/thumbnails/item_identifier_delete/23.png';

        // sanity-check
        self::assertFileExists($thumbnailPath);

        $repository = new InspirationImageRepository(new Filesystem(), $root->url(), 'images/thumbnails');

        $thumbnail = InspirationThumbnail::from(null, 23, 'item_identifier_delete', $thumbnailPath, '23.png');

        $repository->deleteThumbnail($thumbnail);

        self::assertFileNotExists($root->url() . '/images/thumbnails/item_identifier_delete/23.png');
    }

    private function generateInspiration(int $id, ?string $thumbnailUrl, Item $item): Inspiration
    {
        $inspiration = Inspiration::forItem($item);
        $inspiration->setThumbnail($thumbnailUrl);

        $this->setPrivateId($inspiration, $id);

        return $inspiration;
    }

    private function generateItem(string $parentIdentifier): Item
    {
        return (new Item())->setIdentifier($parentIdentifier);
    }
}
