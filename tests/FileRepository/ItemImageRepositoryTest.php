<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ItemImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemThumbnail;
use Symfony\Component\Filesystem\Filesystem;

class ItemImageRepositoryTest extends TestCase
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $itemDetailImageBasePath;

    /**
     * @var string
     */
    private $itemThumbnailBasePath;

    /**
     * @var ItemImageRepository
     */
    private $itemImageRepository;

    public function setUp(): void
    {
        $this->filesystem = new Filesystem();

        $vfsRoot = vfsStream::setup();

        $basePath = $vfsRoot->url();
        $this->itemDetailImageBasePath = $basePath . $detailImagePath = '/images/item/image';
        $this->itemThumbnailBasePath = $basePath . $thumbnailPath = '/images/item/thumb';
        $this->itemImageRepository = new ItemImageRepository($basePath, $detailImagePath, $thumbnailPath);
    }

    /**
     * @param string $parentIdentifier
     * @param array $childIdentifiers
     * @param string $ext
     *
     * @dataProvider providerShouldReturnItemIdentifiers
     */
    public function testShouldReturnDetailImages(string $parentIdentifier, array $childIdentifiers, string $ext): void
    {
        $childIdentifiers[] = $parentIdentifier;
        $expectedImages[$parentIdentifier] = "/images/item/image/$parentIdentifier.$ext";
        foreach ($childIdentifiers as $identifier) {
            $this->filesystem->dumpFile($this->itemDetailImageBasePath . '/' . $identifier . '.' . $ext, 'This is a test file!');
            $expectedImages[$identifier] = "/images/item/image/$identifier.$ext";
        }

        $detailImages = $this->itemImageRepository->findDetailImages($this->generateItem($parentIdentifier, $childIdentifiers));

        foreach ($expectedImages as $identifier => $expectedImage) {
            $detailImage = current(array_filter($detailImages, static function (ItemDetailImage $thumbnail) use ($identifier) {
                return array_key_first($thumbnail->toMap()) === $identifier;
            }));

            self::assertEquals($expectedImage, $detailImage->getUrl());
            self::assertEquals([$identifier => $expectedImage], $detailImage->toMap());
        }
    }

    /**
     * @param string $parentIdentifier
     * @param array $childIdentifiers
     * @param string $ext
     *
     * @dataProvider providerShouldReturnItemIdentifiers
     */
    public function testShouldReturnThumbnails(string $parentIdentifier, array $childIdentifiers, string $ext): void
    {
        $childIdentifiers[] = $parentIdentifier;
        $expectedImages[$parentIdentifier] = "/images/item/thumb/$parentIdentifier.$ext";
        foreach ($childIdentifiers as $identifier) {
            $this->filesystem->dumpFile($this->itemThumbnailBasePath . '/' . $identifier . '.' . $ext, 'This is a test file!');
            $expectedImages[$identifier] = "/images/item/thumb/$identifier.$ext";
        }

        $detailImages = $this->itemImageRepository->findThumbnails($this->generateItem($parentIdentifier, $childIdentifiers));

        foreach ($expectedImages as $identifier => $expectedImage) {
            $thumbnail = current(array_filter($detailImages, static function (ItemThumbnail $thumbnail) use ($identifier) {
                return array_key_first($thumbnail->toMap()) === $identifier;
            }));

            self::assertEquals($expectedImage, $thumbnail->getUrl());
            self::assertEquals([$identifier => $expectedImage], $thumbnail->toMap());
        }
    }

    public function providerShouldReturnItemIdentifiers(): array
    {
        return [
            [
                'parent',
                ['child1', 'child2', 'child3'],
                'jpg',
            ],
            [
                'parent',
                [],
                'jpeg',
            ],
            [
                'parent',
                ['child1'],
                'png',
            ],
        ];
    }

    public function testShouldReturnNoDetailImages(): void
    {
        self::assertEmpty($this->itemImageRepository->findDetailImages($this->generateItem('parent', ['child1', 'child2', 'child3'])));
    }

    public function testShouldReturnNoThumbnail(): void
    {
        self::assertEmpty($this->itemImageRepository->findThumbnails($this->generateItem('parent', ['child1', 'child2', 'child3'])));
    }

    private function generateItem(string $parentIdentifier, array $childIdentifiers): Item
    {
        $item = (new Item())->setIdentifier($parentIdentifier);

        foreach ($childIdentifiers as $childIdentifier) {
            $item->addItem((new Item())->setIdentifier($childIdentifier));
        }

        return $item;
    }
}
