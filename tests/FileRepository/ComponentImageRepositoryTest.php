<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ComponentImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\ComponentThumbnail;
use Symfony\Component\Filesystem\Filesystem;

class ComponentImageRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        vfsStream::setup();
    }

    public function testShouldFindThumbnails(): void
    {
        $files = [
            'components' => [
                'component1.jpg' => 'I am a testfile',
                'component2.jpeg' => 'I am a testfile',
                'component3.png' => 'I am a testfile',
            ],
        ];

        $root = vfsStream::create($files);

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $result = $repository->findThumbnails($this->generateComponents());

        foreach ($files['components'] as $filename => $component) {
            $thumbnail = current(array_filter($result, static function (ComponentThumbnail $thumbnail) use ($filename) {
                return array_key_first($thumbnail->toMap()) === explode('.', $filename)[0];
            }));

            self::assertEquals('/components/' . $filename, $thumbnail->getUrl());
        }
    }

    public function testShouldFindNoThumbnails(): void
    {
        $files = [
            'components' => [
                'component1.svg' => 'I am a testfile',
                'component2.ttf' => 'I am a testfile',
                'component3.xlc' => 'I am a testfile',
            ],
        ];

        $root = vfsStream::create($files);

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $result = $repository->findThumbnails($this->generateComponents());

        self::assertEquals([], $result);
    }

    public function testShouldCreateBaseDirectory(): void
    {
        $root = vfsStream::create([]);

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $repository->findThumbnails($this->generateComponents());

        self::assertDirectoryExists($root->url() . '/components');
    }

    public function testShouldFindThumbnail(): void
    {
        $files = [
            'components' => [
                'component1.jpg' => 'I am a testfile',
            ],
        ];

        $root = vfsStream::create($files);

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $thumbnail = $repository->findThumbnail($this->generateComponent('component1'));

        self::assertEquals($thumbnail->getUrl(), '/components/component1.jpg');
    }

    public function testCantFindThumbnail(): void
    {
        $root = vfsStream::create(['components' => []]);

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $this->expectException(FileException::class);
        $repository->findThumbnail($this->generateComponent('component1'));
    }

    public function testShouldSaveThumbnail(): void
    {
        $files = [
            'tmp' => [
                'QWLEQIT' => 'I am a testfile',
            ],
        ];

        $root = vfsStream::create($files);

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $thumbnail = ComponentThumbnail::from(null, 'component_for_upload', $root->url() . '/tmp/QWLEQIT', 'test.jpg');

        $repository->saveThumbnail($thumbnail, $this->generateComponent('component_for_upload'));

        self::assertFileExists($root->url() . '/components/component_for_upload.jpg');
    }

    public function testShouldRemoveOtherMimeType(): void
    {
        $files = [
            'tmp' => [
                'QWLEQIT' => 'I am a testfile',
            ],
            'components' => [
                'component_for_upload.png' => 'I am a png testfile',
            ],
        ];

        $root = vfsStream::create($files);

        // sanity-checks
        self::assertFileExists($root->url() . '/components/component_for_upload.png');
        self::assertFileNotExists($root->url() . '/components/component_for_upload.jpg');

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $thumbnail = ComponentThumbnail::from(null, 'component_for_upload', $root->url() . '/tmp/QWLEQIT', 'test.jpg');

        $repository->saveThumbnail($thumbnail, $this->generateComponent('component_for_upload'));

        self::assertFileNotExists($root->url() . '/components/component_for_upload.png');
        self::assertFileExists($root->url() . '/components/component_for_upload.jpg');
    }

    public function testShouldDeleteThumbnail(): void
    {
        $files = [
            'components' => [
                'component_for_delete.jpg' => 'I am a testfile',
            ],
        ];

        $root = vfsStream::create($files);

        $repository = new ComponentImageRepository('/components/', new Filesystem(), $root->url());

        $thumbnail = ComponentThumbnail::from(null, 'component_for_delete', $root->url() . '/components/component_for_delete.jpg', 'component_for_delete.jpg');

        $repository->deleteThumbnail($thumbnail);

        self::assertFileNotExists($root->url() . '/components/component_for_upload.jpg');
    }

    private function generateComponents(): array
    {
        $components = [];
        foreach (['component1', 'component2', 'component3'] as $identifier) {
            $components[] = $this->generateComponent($identifier);
        }

        return $components;
    }

    private function generateComponent($identifier): Optionclassification
    {
        return (new Optionclassification())->setIdentifier($identifier);
    }
}
