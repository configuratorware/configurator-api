<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewThumbnailRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;
use Symfony\Component\Filesystem\Filesystem;

class CreatorViewThumbnailRepositoryTest extends TestCase
{
    /**
     * @var CreatorViewThumbnailRepository
     */
    private $repository;

    public function setUp(): void
    {
        $root = vfsStream::setup();
        $this->repository = new CreatorViewThumbnailRepository('/images/item/view_thumb/', new Filesystem(), $root->url());
    }

    /**
     * @param string $parentItemIdentifier
     * @param string $childItemIdentifier
     * @param array  $fileStructure
     * @param array  $expectedResult
     *
     * @dataProvider provideMockDataProducingResult
     */
    public function testViewThumbnailRetrievalWithResult(string $parentItemIdentifier, string $childItemIdentifier, array $fileStructure, array $expectedResult): void
    {
        vfsStream::create($fileStructure);

        $mockItem = $this->createMockItemEntity($parentItemIdentifier, $childItemIdentifier);

        $result = $this->repository->findThumbnails($mockItem);

        self::assertSame($expectedResult['url'], $result[0]->getUrl());
    }

    public function provideMockDataProducingResult(): array
    {
        return [
            [
                'mock_parent_item_identifier',
                'mock_child_item_identifier',
                [
                    'images' => [
                        'item' => [
                            'view_thumb' => [
                                'mock_parent_item_identifier' => [
                                    'mock_child_item_identifier' => [
                                        'mock_view1_identifier.png' => 'mock file content',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'url' => '/images/item/view_thumb/mock_parent_item_identifier/mock_child_item_identifier/mock_view1_identifier.png',
                    'identifier' => 'mock_view1_identifier',
                ],
            ],
            [
                'mock_parent_item_identifier',
                'mock_child_item_identifier',
                [
                    'images' => [
                        'item' => [
                            'view_thumb' => [
                                'mock_parent_item_identifier' => [
                                    'mock_child_item_identifier' => [
                                        'mock_view2_identifier.jpg' => 'mock file content',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                [
                    'url' => '/images/item/view_thumb/mock_parent_item_identifier/mock_child_item_identifier/mock_view2_identifier.jpg',
                    'identifier' => 'mock_view2_identifier',
                ],
            ],
        ];
    }

    /**
     * @param string $parentItemIdentifier
     * @param string $childItemIdentifier
     * @param array  $fileStructure
     *
     * @dataProvider provideMockDataNotProducingResult
     */
    public function testViewThumbnailRetrievalWithoutResult(string $parentItemIdentifier, string $childItemIdentifier, array $fileStructure): void
    {
        vfsStream::create($fileStructure);
        $mockItem = $this->createMockItemEntity($parentItemIdentifier, $childItemIdentifier);

        $result = $this->repository->findThumbnails($mockItem);

        self::assertEmpty($result);
    }

    public function provideMockDataNotProducingResult(): array
    {
        return [
            [
                'mock_parent_item_identifier',
                'mock_child_item_identifier',
                [
                    'mock_parent_item_identifier' => [
                        'mock_child_item_identifier' => [
                            'mock_view1_identifier.bmp' => 'mock file content',
                        ],
                    ],
                ],
            ],
            [
                'mock_parent_item_identifier',
                'mock_child_item_identifier',
                [
                    'mock_parent_item_identifier' => [
                        'mock_parent_item_identifier' => [
                            'mock_child_item_identifier' => [
                                'mock_view1_identifier.png' => 'mock file content',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'mock_parent_item_identifier',
                'mock_child_item_identifier',
                [
                    'mock_child_item_identifier' => [
                        'mock_parent_item_identifier' => [
                            'mock_parent_item_identifier' => [
                                'mock_view1_identifier.png' => 'mock file content',
                            ],
                        ],
                    ],
                ],
            ],
            [
                'mock_parent_item_identifier',
                'mock_child_item_identifier',
                [
                    'mock_parent_item_identifier' => [
                        'mock_view1_identifier.png' => 'mock file content',
                    ],
                    'mock_child_item_identifier' => [
                        'mock_view1_identifier.png' => 'mock file content',
                    ],
                ],
            ],
        ];
    }

    public function testShouldCreateThumbnail(): void
    {
        $root = vfsStream::create([
                '/tmp' => ['PWQJWQ' => 'I am a test file'],
                'images' => [
                    'item' => [
                        'view_thumb' => [],
                    ],
                ],
        ]);

        $repository = new CreatorViewThumbnailRepository('/images/item/view_thumb/', new Filesystem(), $root->url());

        $thumbnail = CreatorViewThumbnail::from(null, 'view_1', 'item_1', $root->url() . '/tmp/PWQJWQ', 'test.png');

        $creatorView = CreatorView::from('item_1/02_view_1_identifier', 'view_1', $this->createMockItemEntity('item_1', 'child_1'), '02');

        $repository->saveThumbnail($thumbnail, $this->createMockItemEntity('item_1', 'child_1'), $creatorView);

        self::assertFileExists($root->url() . '/images/item/view_thumb/item_1/item_1/view_1.png');
    }

    public function testShouldUpdateThumbnailWithoutChange(): void
    {
        $root = vfsStream::create([
            'images' => [
                'item' => [
                    'view_thumb' => [
                        'item_1' => [
                            'item_1' => [
                                'view_1_identifier.png' => 'mock file content',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $repository = new CreatorViewThumbnailRepository('/images/item/view_thumb/', new Filesystem(), $root->url());

        $thumbnail = CreatorViewThumbnail::from(null, 'view_1_identifier', 'item_1', $root->url() . '/images/item/view_thumb/item_1/item_1/view_1_identifier.png', 'view_1_identifier.png');

        $creatorView = CreatorView::from('item_1/02_view_1_identifier', 'view_1_identifier', $this->createMockItemEntity('item_1', 'child_1'), '02');

        $repository->saveThumbnail($thumbnail, $this->createMockItemEntity('item_1', 'child_1'), $creatorView);

        // check if file still exists
        self::assertFileExists($root->url() . '/images/item/view_thumb/item_1/item_1/view_1_identifier.png');
    }

    public function testShouldRemoveThumbnailIfOtherMimeType(): void
    {
        $root = vfsStream::create([
            '/tmp' => ['PWQJWQ' => 'I am a test file'],
            'images' => [
                'item' => [
                    'view_thumb' => [
                        'item_1' => [
                            'item_1' => [
                                'view_1_other_mime_type.jpg' => 'I am a  jpg mock file',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        // sanity checks
        self::assertFileNotExists($root->url() . '/images/item/view_thumb/item_1/item_1/view_1_other_mime_type.png');
        self::assertFileExists($root->url() . '/images/item/view_thumb/item_1/item_1/view_1_other_mime_type.jpg');

        $repository = new CreatorViewThumbnailRepository('/images/item/view_thumb/', new Filesystem(), $root->url());

        $thumbnail = CreatorViewThumbnail::from(null, 'view_1_other_mime_type', 'item_1', $root->url() . '/tmp/PWQJWQ', 'test.png');

        $creatorView = CreatorView::from('item_1/view_1_other_mime_type', 'view_1_other_mime_type', $this->createMockItemEntity('item_1', 'child_1'), '02');

        $repository->saveThumbnail($thumbnail, $this->createMockItemEntity('item_1', 'child_1'), $creatorView);

        // check if file still exists
        self::assertFileExists($root->url() . '/images/item/view_thumb/item_1/item_1/view_1_other_mime_type.png');
        self::assertFileNotExists($root->url() . '/images/item/view_thumb/item_1/item_1/view_1_other_mime_type.jpg');
    }

    public function testShouldDeleteViewThumbnail(): void
    {
        $root = vfsStream::create([
            'images' => [
                'item' => [
                    'view_thumb' => [
                        'item_1' => [
                            'item_1' => [
                                'view_1_identifier.png' => 'mock file content',
                                'view_2_identifier.png' => 'mock file content',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $repository = new CreatorViewThumbnailRepository('/images/item/view_thumb/', new Filesystem(), $root->url());

        $thumbnail = CreatorViewThumbnail::from(null, 'view_1_identifier', 'item_1', $root->url() . '/images/item/view_thumb/item_1/item_1/view_1_identifier.png', 'view_1_identifier.png');

        $repository->deleteThumbnail($thumbnail);

        self::assertFileNotExists($root->url() . '/images/item/view_thumb/item_1/item_1/view_1_identifier.png');
    }

    /**
     * @param string $parentItemIdentifier
     * @param string $childItemIdentifier
     *
     * @return Item
     */
    private function createMockItemEntity(string $parentItemIdentifier, string $childItemIdentifier): Item
    {
        $parentItem = new Item();
        $parentItem->setIdentifier($parentItemIdentifier);

        $childItem = new Item();
        $childItem->setIdentifier($childItemIdentifier);
        $childItem->setParent($parentItem);

        return $parentItem;
    }
}
