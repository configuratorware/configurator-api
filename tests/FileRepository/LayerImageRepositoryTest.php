<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\LayerImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\LayerImage;
use Symfony\Component\Filesystem\Filesystem;

class LayerImageRepositoryTest extends TestCase
{
    private $fileSystem;

    public function setUp(): void
    {
        vfsStream::setup();

        $this->fileSystem = new Filesystem();
    }

    /**
     * @param array $expectedLayerImages
     * @param array $givenDirStructure
     *
     * @dataProvider providerShouldFindLayerImages
     */
    public function testShouldFindLayerImages(array $expectedLayerImages, array $givenDirStructure): void
    {
        $root = vfsStream::create($givenDirStructure);
        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/item/configuratorimages');
        $item = $this->generateItem();

        $result = $repository->findLayerImages($item);

        self::assertCount(2, $result);

        foreach ($expectedLayerImages as $key => $expectedLayerImage) {
            self::assertSame($expectedLayerImage['url'], $result[$key]->getUrl());
            self::assertSame($expectedLayerImage['component'], $result[$key]->getComponentIdentifier());
            self::assertSame($expectedLayerImage['option'], $result[$key]->getOptionIdentifier());
            self::assertSame($expectedLayerImage['view'], $result[$key]->getViewIdentifier());
        }
    }

    public function providerShouldFindLayerImages(): \Generator
    {
        yield [
            [
                [
                    'url' => '/images/item/configuratorimages/mock_item/01_mock_view/01_mock_component_1/mock_option_1.png',
                    'component' => 'mock_component_1',
                    'option' => 'mock_option_1',
                    'view' => 'mock_view',
                ],
                [
                    'url' => '/images/item/configuratorimages/mock_item/01_mock_view/01_mock_component_1/mock_option_2.jpg',
                    'component' => 'mock_component_1',
                    'option' => 'mock_option_2',
                    'view' => 'mock_view',
                ],
            ],
            [
                'images' => [
                    'item' => [
                        'configuratorimages' => [
                            'mock_item' => [
                                '01_mock_view' => [
                                    '01_mock_component_1' => [
                                        'mock_option_1.png' => 'mock file content',
                                        'mock_option_2.jpg' => 'mock file content',
                                        'mock_option_3.bmp' => 'mock file content',
                                        'fake_dir' => [
                                            'mock_option_4.png' => 'mock file content',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param array $expectedLayerImages
     * @param array $givenFileStructure
     *
     * @dataProvider providerShouldFindLayerImagesForComponent
     */
    public function testShouldFindLayerImagesForComponent(array $expectedLayerImages, array $givenFileStructure): void
    {
        $root = vfsStream::create($givenFileStructure);
        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/item/configuratorimages');
        $item = $this->generateItem();
        $component = $item->getItemOptionclassification()->first()->getOptionclassification();

        $result = $repository->findLayerImagesForComponent($item, $component);

        self::assertCount(3, $result);

        foreach ($expectedLayerImages as $key => $expectedLayerImage) {
            self::assertSame($expectedLayerImage['url'], $result[$key]->getUrl());
            self::assertSame($expectedLayerImage['component'], $result[$key]->getComponentIdentifier());
            self::assertSame($expectedLayerImage['option'], $result[$key]->getOptionIdentifier());
            self::assertSame($expectedLayerImage['view'], $result[$key]->getViewIdentifier());
        }
    }

    public function providerShouldFindLayerImagesForComponent(): \Generator
    {
        yield [
            [
                [
                    'url' => '/images/item/configuratorimages/mock_item/01_mock_view_1/01_mock_component_1/mock_option_1.png',
                    'component' => 'mock_component_1',
                    'option' => 'mock_option_1',
                    'view' => 'mock_view_1',
                ],
                [
                    'url' => '/images/item/configuratorimages/mock_item/02_mock_view_2/01_mock_component_1/mock_option_2.jpg',
                    'component' => 'mock_component_1',
                    'option' => 'mock_option_2',
                    'view' => 'mock_view_2',
                ],
                [
                    'url' => '/images/item/configuratorimages/mock_item/02_mock_view_2/01_mock_component_1/mock_option_3.jpg',
                    'component' => 'mock_component_1',
                    'option' => 'mock_option_3',
                    'view' => 'mock_view_2',
                ],
            ],
            [
                'images' => [
                    'item' => [
                        'configuratorimages' => [
                            'mock_item' => [
                                '01_mock_view_1' => [
                                    '01_mock_component_1' => [
                                        'mock_option_1.png' => 'mock file content',
                                        'fake_dir' => [
                                            'mock_option_4.png' => 'mock file content',
                                        ],
                                    ],
                                ],
                                '02_mock_view_2' => [
                                    '01_mock_component_1' => [
                                        'mock_option_2.jpg' => 'mock file content',
                                        'mock_option_3.jpg' => 'mock file content',
                                    ],
                                    '02_mock_component_3' => [
                                        'mock_option_1.png' => 'mock file content',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function testShouldFindLayerImage(): void
    {
        $root = vfsStream::create([
            'images' => [
                'layer' => [
                    'mock_item' => [
                        '01_view_1' => [
                            '01_mock_component_1' => [
                                'mock_option_0_wrong.jpg' => 'I am a mock file!',
                                'mock_option_1.jpg' => 'I am a mock file!',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/layer');

        $item = $this->generateItem();
        /** @var ItemOptionclassification $itemOptionClassification1 */
        $itemOptionClassification1 = $item->getItemOptionclassification()->first();
        $optionClassification = $itemOptionClassification1->getOptionclassification();
        $option = $itemOptionClassification1->getItemOptionclassificationOption()->first()->getOption();
        $view = CreatorView::from('mock_item/01_view_1', 'view_1', $item, '01');

        $layerImage = $repository->findLayerImage($item, $optionClassification, $option, $view);

        self::assertEquals('/images/layer/mock_item/01_view_1/01_mock_component_1/mock_option_1.jpg', $layerImage->getUrl());
    }

    public function testShouldCreateLayerImage(): void
    {
        $root = vfsStream::create([
            '/tmp' => [
                'PWQJWQ' => 'I am a test file',
            ],
            'images' => [
                'configuratorimages' => [
                    'mock_item' => [
                        '01_view_1' => [
                            '01_mock_component_1' => [],
                        ],
                    ],
                ],
            ],
        ]);

        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/configuratorimages');

        $layerImage = LayerImage::from(null, 'mock_component_1', 'mock_option_1', 'view_1', $root->url() . '/tmp/PWQJWQ', 'test.png', 1, '01');

        $repository->saveLayerImage($layerImage, $this->generateItem());

        self::assertFileExists($root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.png');
    }

    public function testShouldChangeLayerImage(): void
    {
        $root = vfsStream::create([
            '/tmp' => [
                'PWQJWQ' => 'changed option',
            ],
            'images' => [
                'configuratorimages' => [
                    'mock_item' => [
                        '01_view_1' => [
                            '01_mock_component_1' => [
                                'mock_option_1.png' => 'existing option',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/configuratorimages');

        $layerImage = LayerImage::from(null, 'mock_component_1', 'mock_option_1', 'view_1', $root->url() . '/tmp/PWQJWQ', 'test.png', 1, '01');

        $repository->saveLayerImage($layerImage, $this->generateItem());

        self::assertEquals('changed option', file_get_contents($root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.png'));
    }

    public function testShouldRemoveLayerImageWithOtherMimeType(): void
    {
        $root = vfsStream::create([
            '/tmp' => [
                'PWQJWQ' => 'I am a test file',
            ],
            'images' => [
                'configuratorimages' => [
                    'mock_item' => [
                        '01_view_1' => [
                            '01_mock_component_1' => [
                                'mock_option_1.jpg' => 'existing jpg',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        // sanity checks
        self::assertFileExists($root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.jpg');
        self::assertFileNotExists($root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.png');

        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/configuratorimages');

        $layerImage = LayerImage::from(null, 'mock_component_1', 'mock_option_1', 'view_1', $root->url() . '/tmp/PWQJWQ', 'test.png', 1, '01');

        $repository->saveLayerImage($layerImage, $this->generateItem());

        self::assertFileNotExists($root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.jpg');
        self::assertFileExists($root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.png');
    }

    public function testShouldDeleteLayerImage(): void
    {
        $root = vfsStream::create([
            'images' => [
                'configuratorimages' => [
                    'mock_item' => [
                        '01_view_1' => [
                            '01_mock_component_1' => [
                                'mock_option_1.png' => 'I am a mock file',
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/configuratorimages');

        $layerImage = LayerImage::from(null, '', '', '', $root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.png', '', 1, '01');

        $repository->deleteLayerImage($layerImage, $this->generateItem());

        self::assertFileNotExists($root->url() . '/images/configuratorimages/mock_item/01_view_1/01_mock_component_1/mock_option_1.png');
    }

    public function testShouldCreateStores(): void
    {
        $root = vfsStream::create([]);
        $repository = new LayerImageRepository($this->fileSystem, $root->url(), $root->url() . '/images/item/configuratorimages');
        $item = $this->generateItem();

        $creatorView = CreatorView::from(null, 'mock_view_1_view', $item, '01');

        $repository->createStores($creatorView, $item);

        self::assertDirectoryExists($root->url() . '/images/item/configuratorimages/mock_item/01_mock_view_1_view/0_mock_component_1');
        self::assertDirectoryExists($root->url() . '/images/item/configuratorimages/mock_item/01_mock_view_1_view/1_mock_component_2');
    }

    /**
     * @return Item
     */
    private function generateItem(): Item
    {
        $itemOptionClassificationOption1 = new ItemOptionclassificationOption();
        $itemOptionClassificationOption1->setOption((new Option())->setIdentifier('mock_option_1'));
        $itemOptionClassificationOption2 = new ItemOptionclassificationOption();
        $itemOptionClassificationOption2->setOption((new Option())->setIdentifier('mock_option_2'));

        $itemOptionClassification1 = new ItemOptionclassification();
        $itemOptionClassification1->setOptionclassification((new Optionclassification())->setIdentifier('mock_component_1'));
        $itemOptionClassification2 = new ItemOptionclassification();
        $itemOptionClassification2->setOptionclassification((new Optionclassification())->setIdentifier('mock_component_2'));

        $itemOptionClassification1->addItemOptionclassificationOption($itemOptionClassificationOption1);
        $itemOptionClassification1->addItemOptionclassificationOption($itemOptionClassificationOption2);

        $item = new Item();
        $item->setIdentifier('mock_item');
        $itemOptionClassification1->setItem($item);
        $itemOptionClassification2->setItem($item);
        $item->addItemOptionclassification($itemOptionClassification1);
        $item->addItemOptionclassification($itemOptionClassification2);

        return $item;
    }
}
