<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewFileRepository;
use Symfony\Component\Filesystem\Filesystem;

class CreatorViewFileRepositoryTest extends TestCase
{
    private $fileSystem;

    public function setUp(): void
    {
        vfsStream::setup();

        $this->fileSystem = new Filesystem();
    }

    /**
     * @param array $dirStructure
     *
     * @dataProvider provideChangeDirStructure
     */
    public function testShouldUpdateView(array $dirStructure): void
    {
        $root = vfsStream::create($dirStructure);

        $repository = new CreatorViewFileRepository($root->url(), $this->fileSystem);

        $creatorView = CreatorView::from('mock_item_identifier/01_mock_view_1_view', 'changed', $this->generateMockItem(), '01');

        $repository->save($creatorView);

        self::assertDirectoryNotExists($root->url() . '/mock_item_identifier/01_mock_view_1_view');
        self::assertFileExists($root->url() . '/mock_item_identifier/01_changed/mockfile.jpg');
    }

    /**
     * @param array $dirStructure
     *
     * @dataProvider provideChangeDirStructure
     */
    public function testShouldNotUpdateView(array $dirStructure): void
    {
        $root = vfsStream::create($dirStructure);

        $repository = new CreatorViewFileRepository($root->url(), $this->fileSystem);

        $creatorView = CreatorView::from('mock_item_identifier/01_mock_view_1_view', 'mock_view_1_view', $this->generateMockItem(), '01');

        $repository->save($creatorView);

        self::assertFileExists($root->url() . '/mock_item_identifier/01_mock_view_1_view/mockfile.jpg');
    }

    /**
     * @param array $dirStructure
     *
     * @dataProvider provideChangeDirStructure
     */
    public function testShouldDeleteView(array $dirStructure): void
    {
        $root = vfsStream::create($dirStructure);

        $repository = new CreatorViewFileRepository($root->url(), $this->fileSystem);

        $creatorView = CreatorView::from('mock_item_identifier/01_mock_view_1_view', 'mock_view_1_view', $this->generateMockItem(), '01');
        $repository->delete($creatorView);

        self::assertDirectoryNotExists($root->url() . '/mock_item_identifier/01_mock_view_1_view/');
    }

    public function testShouldNotCreateDirectory(): void
    {
        $root = vfsStream::create([]);

        $repository = new CreatorViewFileRepository($root->url(), $this->fileSystem);

        $creatorView = CreatorView::from('mock_item_identifier/01_mock_view_1_view', 'mock_view_1_view', $this->generateMockItem(), '01');

        $createdDirectory = $repository->save($creatorView);

        self::assertSame('mock_item_identifier/01_mock_view_1_view', $createdDirectory);
        self::assertDirectoryNotExists($root->url() . "/$createdDirectory");
    }

    public function testShouldSilentlyCreateDirectoryWhenDirNotSet(): void
    {
        $root = vfsStream::create([]);

        $repository = new CreatorViewFileRepository($root->url(), $this->fileSystem);

        $creatorView = CreatorView::from(null, 'mock_view_2_view', $this->generateMockItem(), '1');

        $createdDirectory = $repository->save($creatorView);

        self::assertSame('mock_item_identifier/1_mock_view_2_view', $createdDirectory);
        self::assertDirectoryExists($root->url() . "/$createdDirectory");
    }

    public function provideChangeDirStructure(): \Generator
    {
        yield [['mock_item_identifier' => [
                '01_mock_view_1_view' => [
                    'mockfile.jpg' => 'This is a testfile',
                ],
                '02_mock_view_2' => [],
            ]],
        ];
    }

    /**
     * @param array $expectedViews
     * @param array $givenFileStructure
     *
     * @dataProvider providerShouldFindViews
     */
    public function testShouldFindViews(array $expectedViews, array $givenFileStructure): void
    {
        $root = vfsStream::create($givenFileStructure);
        $repository = new CreatorViewFileRepository($root->url(), $this->fileSystem);

        $item = new Item();
        $item->setIdentifier('mock_item_identifier');

        $result = $repository->findCreatorViews($item);

        foreach ($expectedViews as $key => $view) {
            self::assertSame($view['identifier'], $result[$key]->getIdentifier(), $key . 'identifier');
            self::assertSame($view['itemIdentifier'], $result[$key]->getItemIdentifier(), $key . 'itemIdentifier');
            self::assertSame($view['sequenceNumber'], $result[$key]->getSequenceNumber(), $key . 'sequenceNumber');
        }
    }

    public function providerShouldFindViews(): \Generator
    {
        yield [
            [
                [
                    'identifier' => 'mock_view_1_view',
                    'itemIdentifier' => 'mock_item_identifier',
                    'sequenceNumber' => '1',
                ],
                [
                    'identifier' => 'mock_view_2',
                    'itemIdentifier' => 'mock_item_identifier',
                    'sequenceNumber' => '2',
                ],
                [
                    'identifier' => 'mock_view_3',
                    'itemIdentifier' => 'mock_item_identifier',
                    'sequenceNumber' => '',
                ],
            ],
            [
                'mock_item_identifier' => [
                    '1_mock_view_1_view' => [],
                    '2_mock_view_2' => [],
                    'mock_view_3' => [],
                    'nested_view_1' => [
                        'fake_view_1' => [],
                    ],
                    'fake_file.jpg' => 'Mock file content',
                ],
            ],
        ];
    }

    private function generateMockItem(): Item
    {
        return (new Item())->setIdentifier('mock_item_identifier');
    }
}
