<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\FileRepository;

use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionThumbnail;
use Symfony\Component\Filesystem\Filesystem;

class OptionImageRepositoryTest extends TestCase
{
    public function setUp(): void
    {
        vfsStream::setup();
    }

    public function testShouldFindDetailImage(): void
    {
        $slug = 'option';
        $filename = 'test_option_fullsize.png';
        $expectedImageUrl = "/$slug/$filename";

        $vfsRoot = vfsStream::create([
            $slug => [$filename => 'I am a test file'],
        ]);

        $testOption = (new Option())->setIdentifier('test_option');

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), "/$slug/");
        $detailImage = $repository->findDetailImage($testOption);

        self::assertEquals($expectedImageUrl, $detailImage->getUrl());
        self::assertEquals(['test_option' => $expectedImageUrl], $detailImage->toMap());
    }

    public function testShouldFindDetailImages(): void
    {
        $slug = 'option';
        $expectedImageUrls = [
            'test_option_1' => 'test_option_1_fullsize.png',
            'test_option_2' => 'test_option_2_fullsize.png',
            'test_option_3' => 'test_option_3_fullsize.png',
        ];

        $testFiles = [];
        $testOptions = [];
        foreach ($expectedImageUrls as $identifier => $filename) {
            $testFiles[$slug][$filename] = 'I am a test file';
            $testOptions[] = (new Option())->setIdentifier($identifier);
        }

        $vfsRoot = vfsStream::create($testFiles);

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), "/$slug/");
        $detailImages = $repository->findDetailImages($testOptions);

        foreach ($expectedImageUrls as $identifier => $filename) {
            $detailImage = current(array_filter($detailImages, static function (OptionDetailImage $item) use ($identifier) {
                return array_key_first($item->toMap()) === $identifier;
            }));
            self::assertEquals('/option/' . $filename, $detailImage->getUrl());
            self::assertEquals([$identifier => '/option/' . $filename], $detailImage->toMap());
        }
    }

    public function testShouldThrowOnFindDetailImage(): void
    {
        $slug = 'option';
        $filename = 'other_option_fullsize.png';

        $vfsRoot = vfsStream::create([
            $slug => [$filename => 'I am a test file'],
        ]);

        $this->expectException(FileException::class);

        $testOption = (new Option())->setIdentifier('test_option');

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), "/$slug/");
        $repository->findDetailImage($testOption);
    }

    public function testShouldCreateBaseDirectory(): void
    {
        $vfsRoot = vfsStream::create([]);

        $testOption = (new Option())->setIdentifier('test_option');

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), '/option/');
        $repository->findDetailImages([$testOption]);

        self::assertDirectoryExists($vfsRoot->url() . '/option/');
    }

    public function testShouldCreateDetailImage(): void
    {
        $root = vfsStream::create([
            '/tmp' => [
                'PWQJWQ' => 'I am a test file',
                'ERWZED' => 'I am a test file',
            ],
            'images' => [
                'option' => [],
            ],
        ]);

        $repository = new OptionImageRepository(new Filesystem(), $root->url(), '/option/');

        $detailImage = OptionDetailImage::from(null, 'option_identifier_1', $root->url() . '/tmp/PWQJWQ', 'test.png');

        $repository->saveDetailImage($detailImage, (new Option())->setIdentifier('option_identifier_1'));

        self::assertFileExists($root->url() . '/option/option_identifier_1_fullsize.png');
    }

    public function testShouldRemoveDetailImageWithOtherMimeType(): void
    {
        $root = vfsStream::create([
            'tmp' => ['PWQJWQ' => 'I am a test file'],
            'option' => ['option_identifier_1_fullsize.jpeg' => 'I am a jpeg test file'],
        ]);

        // sanity checks
        self::assertFileNotExists($root->url() . '/option/option_identifier_1_fullsize.png');
        self::assertFileExists($root->url() . '/option/option_identifier_1_fullsize.jpeg');

        $repository = new OptionImageRepository(new Filesystem(), $root->url(), '/option/');

        $detailImage = OptionDetailImage::from(null, 'option_identifier_1', $root->url() . '/tmp/PWQJWQ', 'test.png');

        $repository->saveDetailImage($detailImage, (new Option())->setIdentifier('option_identifier_1'));

        self::assertFileExists($root->url() . '/option/option_identifier_1_fullsize.png');
        self::assertFileNotExists($root->url() . '/option/option_identifier_1_fullsize.jpeg');
    }

    public function testShouldDeleteDetailImage(): void
    {
        $files = [
            'option' => [
                'option_identifier_1_fullsize.jpg' => 'I am a testfile',
            ],
        ];

        $root = vfsStream::create($files);

        $repository = new OptionImageRepository(new Filesystem(), $root->url(), '/option/');

        $detailImage = OptionDetailImage::from('/option/option_identifier_1_fullsize.jpg', 'option_identifier_1', $root->url() . '/option/option_identifier_1_fullsize.jpg', 'option_identifier_1_fullsize.jpg');

        $repository->deleteDetailImage($detailImage);

        self::assertFileNotExists($root->url() . '/option/option_identifier_1_fullsize.jpg');
    }

    public function testShouldFindThumbnail(): void
    {
        $slug = 'option';
        $filename = 'test_option.jpg';
        $expectedImageUrl = "/$slug/$filename";

        $vfsRoot = vfsStream::create([
            $slug => [$filename => 'I am a test file'],
        ]);

        $testOption = (new Option())->setIdentifier('test_option');

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), "/$slug/");
        $detailImage = $repository->findThumbnail($testOption);

        self::assertEquals($expectedImageUrl, $detailImage->getUrl());
        self::assertEquals(['test_option' => $expectedImageUrl], $detailImage->toMap());
    }

    public function testShouldFindThumbnailDotIdentifier(): void
    {
        $slug = 'option';
        $filename = 'test.option.jpg';
        $expectedImageUrl = "/$slug/$filename";

        $vfsRoot = vfsStream::create([
            $slug => [$filename => 'I am a test file'],
        ]);

        $testOption = (new Option())->setIdentifier('test.option');

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), "/$slug/");
        $detailImage = $repository->findThumbnail($testOption);

        self::assertEquals($expectedImageUrl, $detailImage->getUrl());
        self::assertEquals(['test.option' => $expectedImageUrl], $detailImage->toMap());
    }

    public function testShouldThrowOnFindThumbnail(): void
    {
        $slug = 'option';
        $filename = 'other_option.jpg';

        $vfsRoot = vfsStream::create([
            $slug => [$filename => 'I am a test file'],
        ]);

        $this->expectException(FileException::class);

        $testOption = (new Option())->setIdentifier('test_option');

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), "/$slug/");
        $repository->findThumbnail($testOption);
    }

    public function testShouldFindThumbnails(): void
    {
        $slug = 'option';
        $expectedImageUrls = [
            'test_option_1' => 'test_option_1.jpg',
            'test_option_2' => 'test_option_2.png',
            'test_option_3' => 'test_option_3.jpeg',
        ];

        $testFiles = [];
        $testOptions = [];
        foreach ($expectedImageUrls as $identifier => $filename) {
            $testFiles[$slug][$filename] = 'I am a test file';
            $testOptions[] = (new Option())->setIdentifier($identifier);
        }

        $vfsRoot = vfsStream::create($testFiles);

        $repository = new OptionImageRepository(new Filesystem(), $vfsRoot->url(), "/$slug/");
        $detailImages = $repository->findThumbnails($testOptions);

        foreach ($expectedImageUrls as $identifier => $filename) {
            $detailImage = current(array_filter($detailImages, static function (OptionThumbnail $item) use ($identifier) {
                return array_key_first($item->toMap()) === $identifier;
            }));
            self::assertEquals('/option/' . $filename, $detailImage->getUrl());
            self::assertEquals([$identifier => '/option/' . $filename], $detailImage->toMap());
        }
    }

    public function testShouldCreateThumbnail(): void
    {
        $root = vfsStream::create(['/tmp' => ['PWQJWQ' => 'I am a test file']]);

        $repository = new OptionImageRepository(new Filesystem(), $root->url(), '/option/');

        $thumbnail = OptionThumbnail::from(null, 'option_identifier_1', $root->url() . '/tmp/PWQJWQ', 'test.png');

        $repository->saveThumbnail($thumbnail, (new Option())->setIdentifier('option_identifier_1'));

        self::assertFileExists($root->url() . '/option/option_identifier_1.png');
    }

    public function testShouldRemoveThumbnailWithOtherMimeType(): void
    {
        $root = vfsStream::create([
            '/tmp' => [
                'PWQJWQ' => 'I am a test file',
            ],
            'option' => ['option_identifier_1.jpg' => 'I am a jpg test file'],
        ]);

        // sanity checks
        self::assertFileExists($root->url() . '/option/option_identifier_1.jpg');
        self::assertFileNotExists($root->url() . '/option/option_identifier_1.png');

        $repository = new OptionImageRepository(new Filesystem(), $root->url(), '/option/');

        $thumbnail = OptionThumbnail::from(null, 'option_identifier_1', $root->url() . '/tmp/PWQJWQ', 'test.png');

        $repository->saveThumbnail($thumbnail, (new Option())->setIdentifier('option_identifier_1'));

        self::assertFileExists($root->url() . '/option/option_identifier_1.png');
        self::assertFileNotExists($root->url() . '/option/option_identifier_1.jpg');
    }

    public function testShouldDeleteThumbnail(): void
    {
        $files = [
            'option' => [
                'option_identifier_1.jpg' => 'I am a testfile',
            ],
        ];

        $root = vfsStream::create($files);

        $repository = new OptionImageRepository(new Filesystem(), $root->url(), '/option/');

        $thumbnail = OptionThumbnail::from('/option/option_identifier_1.jpg', 'option_identifier_1', $root->url() . '/option/option_identifier_1.jpg', 'option_identifier_1.jpg');

        $repository->deleteThumbnail($thumbnail);

        self::assertFileNotExists($root->url() . '/option/option_identifier_1.jpg');
    }
}
