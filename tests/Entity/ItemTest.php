<?php

declare(strict_types=1);

namespace Tests\Redhotmagma\ConfiguratorApiBundle\Entity;

use PHPUnit\Framework\TestCase;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification;

class ItemTest extends TestCase
{
    /**
     * @dataProvider itemEntities
     */
    public function testShouldReturnMinimumOrderAmount(Item $item, ?int $expectedAmount): void
    {
        $actual = $item->getMinimumOrderAmount();

        TestCase::assertEquals($expectedAmount, $actual);
    }

    public function itemEntities(): array
    {
        $itemClassification1 = new Itemclassification();
        $itemClassification1->setMinimumOrderAmount(25);
        $itemItemClassification1 = new ItemItemclassification();
        $itemItemClassification1->setItemclassification($itemClassification1);

        $itemClassification2 = new Itemclassification();
        $itemClassification2->setMinimumOrderAmount(10);
        $itemItemClassification2 = new ItemItemclassification();
        $itemItemClassification2->setItemclassification($itemClassification2);

        $item1 = new Item();
        $item1->setMinimumOrderAmount(50);

        $item2 = new Item();
        $item2->setMinimumOrderAmount(50);
        $item2->addItemItemclassification($itemItemClassification1);
        $item2->addItemItemclassification($itemItemClassification2);

        $item3 = new Item();
        $item3->setMinimumOrderAmount(15);
        $item3->addItemItemclassification($itemItemClassification1);
        $item3->addItemItemclassification($itemItemClassification2);

        return [
            'Item minimum amount with no item classification' => [$item1, 50],
            'Item minimum amount with item classifications' => [$item2, 50],
            'Item classification minimum amount' => [$item3, 25],
        ];
    }
}
