# UPGRADE FROM 1.33 to 1.35
If you want to update from a lower version than 1.33 to 1.35, please read previous UPGRADE notes.

## WeasyPrint removed from Docker Image

Since WeasyPrint was replaced with dompdf in configuratorware version 1.25, it has now been removed from the configuratorware docker image that is provided with the setup package.
If you want to continue using WeasyPrint in your project you can add it in your dockerfile like this:

        RUN pip3 install cairocffi==1.4
        RUN pip3 install weasyprint==52.5
