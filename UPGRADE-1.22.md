# UPGRADE FROM 1.21 to 1.22
If you want to update from a lower version than 1.21 to 1.22, please read previous UPGRADE notes.

## Change in internal classes
* The method addPreviewImageURLToStructure of class Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationImages was removed

## Inspirations
A new inspiration feature was introduced and therefore the support for the previous inspirations + designtemplates functionality dropped. If you rely on:
`/frontendapi/inspirations/{itemIdentifier}` or `/frontendapi/designtemplates` you have these possibilities:
* migrate your data to the new implementation
* reimplement the functionality in the project
