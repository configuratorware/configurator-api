# UPGRADE FROM 1.38 to 1.42
If you want to update from a lower version than 1.38 to 1.42, please read previous UPGRADE notes.

## Translations
Translations management has been updated to now use symfony/translation bundle.

If you're using a custom template for `@RedhotmagmaConfiguratorApi/configuration/main.html.twig` you should remove the use of `translations` property and replace it with Twig translation filter.
(e.g. `{{ translations.page }}` is replaced by `{{ 'page'|trans }}`)

The `default_path` in the `app/config/packages/translation.yaml` file needs to be adjusted like this:
```yaml
framework:
   default_locale: en
   translator:
      default_path: '%translations.cached_path%'
      fallbacks:
         - en
```

### Deprecations
* `Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationDocument\DataCollectionEvent::dataSet['translations']` key is deprecated and will be removed.


## Breaking changes for frontend dependencies (custom-builds)

###configurator-frontendgui:

`react` and `react-dom` are updated to the `18.2.0` (currently latest) version

If it is listed in the `package.json` file of your custom frontend build, please update it:

```json
"react": "^18.2.0",
"react-dom": "^18.2.0",
```

Important: Using the latest react version leads to "unmet peer dependencies" reported after `npm install` regarding `material-ui@4.x`,
which will be also updated in the future. As a workaround 
 - add the following `overrides` to your `package.json` file **(recommended)**:
    ```json
    "overrides": {
        "react": "^18.2.0",
        "react-dom": "^18.2.0"
    },
    ```
  - OR use the `--legacy-peer-deps` flag: `npm install --legacy-peer-deps`


Many of the `devDependencies` are updated in this version, we strongly recommend synchronizing your `package.json` file.

```json
"@babel/core": "^7.14.8",
"@babel/plugin-proposal-class-properties": "^7.14.5",
"@babel/plugin-proposal-decorators": "^7.14.5",
"@babel/plugin-syntax-dynamic-import": "^7.8.3",
"@babel/plugin-transform-modules-commonjs": "^7.14.5",
"@babel/preset-env": "^7.14.8",
"@babel/preset-react": "^7.14.5",
"@babel/preset-typescript": "^7.14.5",
"@pmmmwh/react-refresh-webpack-plugin": "^0.5.11",
"babel-loader": "^8.2.2",
"babel-plugin-module-resolver": "^5.0.0",
"babel-plugin-transform-react-remove-prop-types": "^0.4.24",
"css-loader": "^5.2.7",
"file-loader": "^6.2.0",
"mini-css-extract-plugin": "^2.7.6",
"node-sass": "^9.0.0",
"resolve-url-loader": "^5.0.0",
"react": "^18.2.0",
"react-dom": "^18.2.0",
"react-refresh": "^0.14.0",
"sass-loader": "^13.3.2",
"style-loader": "^3.3.3",
"terser-webpack-plugin": "^5.3.9",
"url-loader": "^4.1.1",
"webpack": "^5.88.2",
"webpack-cli": "^5.1.4",
"webpack-dev-server": "^4.15.1"
```

Important notes for projects using **custom webpack or babel configuration**:
 - due to `webpack` and `webpack-dev-server` updates the webpack configuration has changed
 - hot module replacement is now done by the `@pmmmwh/react-refresh-webpack-plugin` and the `react-refresh/babel` plugins


###configurator-admingui:

`react` and `react-dom` are updated to the `16.14.0` version

If it is listed in the `package.json` file of your custom frontend build, please update it:

```json
"react": "^16.14.0",
"react-dom": "^16.14.0",
```

Note: There are some "conflicting peer dependencies" reported after `npm install`, 
this will be fixed in the future (by updating older packages).

Many of the `devDependencies` are updated in this version, we strongly recommend synchronizing your `package.json` file.

```json
"@babel/cli": "^7.14.8",
"@babel/core": "^7.14.8",
"@babel/plugin-proposal-class-properties": "^7.14.5",
"@babel/plugin-syntax-dynamic-import": "^7.8.3",
"@babel/preset-env": "^7.14.8",
"@babel/preset-react": "^7.14.5",
"@pmmmwh/react-refresh-webpack-plugin": "^0.5.11",
"babel-loader": "^8.2.2",
"babel-plugin-module-resolver": "^5.0.0",
"css-loader": "^5.2.7",
"file-loader": "^6.2.0",
"mini-css-extract-plugin": "^2.7.6",
"node-sass": "^9.0.0",
"react-refresh": "^0.14.0",
"sass-loader": "^13.3.2",
"style-loader": "^3.3.3",
"terser-webpack-plugin": "^5.3.9",
"url-loader": "^4.1.1",
"webpack": "^5.88.2",
"webpack-cli": "^5.1.4",
"webpack-dev-server": "^4.15.1"
```

Important notes for projects using **custom webpack or babel configuration**:
- due to `webpack` and `webpack-dev-server` updates the webpack configuration has changed
- hot module replacement is now done by the `@pmmmwh/react-refresh-webpack-plugin` and the `react-refresh/babel` plugins


