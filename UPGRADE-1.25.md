# UPGRADE FROM 1.24 to 1.25
If you want to update from a lower version than 1.24 to 1.25, please read previous UPGRADE notes.

## Removed
* Redhotmagma\ConfiguratorApiBundle\Service\Base\PdfConverter
* Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationApi::generatePdf
* Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationApi::generateHtml

## Changes in PDF
Previous versions of configuratorware depended on weasyprint as PDF generator. From 1.25 on the default pdf generator is Dompdf which is installed via composer automatically without any further infrastructure requirement.
If you did not customize your template, there is nothing to do, otherwise you have 2 options:
1. adapt your template to work with DomPDF
1. adapt your service config as follows to continue to use weasyprint:

```yaml
    Redhotmagma\ConfiguratorApiBundle\Vector\HtmlPdfConverterInterface:
        alias: Redhotmagma\ConfiguratorApiBundle\Vector\WeasyprintHtmlPdfConverter
```