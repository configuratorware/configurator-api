# UPGRADE FROM 1.28 to 1.29
If you want to update from a lower version than 1.28 to 1.29, please read previous UPGRADE notes.

## Deprecations
* Redhotmagma\ConfiguratorApiBundle\Command\JWTKeyGeneratorCommand

## DomPdf
If your project has a custom template and is using DomPDF, please test your PDF after update. Configuratorware now requires ^1.2.1, which had changes in the style interpreter.

## Frontend dependencies
To build your custom frontend and adminarea, you need to update your package.json file. The minimum working exmaples are

configurator-frontendgui:
```json
    "dependencies": {
        "@configuratorware/configurator-frontendgui": "^1.29.0",
        "fontfaceobserver": "^2.1.0",
        "react-custom-scrollbars": "^4.2.1",
        "react-file-drop": "^0.2.8"
    },
    "devDependencies": {
        "@babel/core": "^7.16.0",
        "@babel/plugin-proposal-class-properties": "^7.13.0",
        "@babel/plugin-proposal-decorators": "^7.13.5",
        "@babel/plugin-syntax-dynamic-import": "^7.8.3",
        "@babel/preset-env": "^7.13.12",
        "@babel/preset-react": "^7.13.13",
        "@babel/preset-typescript": "^7.13.0",
        "babel-loader": "^8.2.2",
        "babel-plugin-module-resolver": "^4.1.0",
        "css-loader": "^5.2.0",
        "file-loader": "^6.2.0",
        "mini-css-extract-plugin": "^1.4.0",
        "node-sass": "^5.0.0",
        "optimize-css-assets-webpack-plugin": "^5.0.4",
        "resolve-url-loader": "^3.1.2",
        "sass-loader": "^11.0.1",
        "style-loader": "^2.0.0",
        "url-loader": "^4.1.1",
        "webpack": "^5.28.0",
        "webpack-cli": "^4.6.0",
        "webpack-dev-server": "^3.11.2"
    },
    "peerDependencies": {
        "@material-ui/core": "*",
        "@material-ui/icons": "*",
        "lodash": "*",
        "prop-types": "*",
        "react": "*",
        "react-dom": "*",
        "react-redux": "*"
    },
    "scripts": {
        "start": "webpack serve --port 3001 --host 0.0.0.0",
        "build": "webpack --config webpack.production.config.js"
    }
```

configurator-admingui:
```json
    "dependencies": {
        "@configuratorware/configurator-admingui": "^1.29.0",
        "prop-types": "^15.6.2",
        "react": "^16.7.0"
    },
    "devDependencies": {
        "@babel/cli": "^7.2.3",
        "@babel/core": "^7.2.2",
        "@babel/plugin-proposal-class-properties": "^7.2.3",
        "@babel/plugin-syntax-dynamic-import": "^7.2.0",
        "@babel/preset-env": "^7.2.3",
        "@babel/preset-react": "^7.0.0",
        "babel-loader": "^8.0.5",
        "babel-plugin-module-resolver": "^4.1.0",
        "css-loader": "^2.1.0",
        "file-loader": "^3.0.1",
        "mini-css-extract-plugin": "^0.5.0",
        "optimize-css-assets-webpack-plugin": "^5.0.1",
        "sass-loader": "^7.1.0",
        "style-loader": "^0.23.1",
        "node-sass": "^4.11.0",
        "terser-webpack-plugin": "4.2.*",
        "url-loader": "^1.1.2",
        "webpack": "^4.44.1",
        "webpack-cli": "^3.3.12",
        "webpack-dev-server": "^3.11.0"
    },
    "scripts": {
        "start": "webpack-dev-server --port 3000",
        "build": "webpack --config webpack.production.config.js"
    }
```

## admingui import paths
In your adminguis project sources, you need to remove the `/lib` part from the imports. So instead of `import '@configuratorware/configurator-admingui/lib';` you have to use `import '@configuratorware/configurator-admingui';`.
