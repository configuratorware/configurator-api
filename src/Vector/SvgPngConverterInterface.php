<?php

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

interface SvgPngConverterInterface
{
    public function svgToPng(Svg $svg, int $dpi = null): Png;
}
