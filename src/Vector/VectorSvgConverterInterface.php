<?php

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

interface VectorSvgConverterInterface
{
    public function vectorToSvg(Vector $vector): Svg;
}
