<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

use Symfony\Component\Filesystem\Filesystem;

final class Pdf
{
    public const EXTENSION = 'pdf';

    /**
     * @var string
     */
    private $path;

    /**
     * @param string $path
     */
    private function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @psalm-suppress TooManyArguments
     *
     * @return string
     */
    public static function generateTempPath(): string
    {
        $fs = new Filesystem();

        return $fs->tempnam(sys_get_temp_dir(), 'conf_doc_pdf_', '.pdf');
    }

    /**
     * @param string $path
     *
     * @return static
     */
    public static function withPath(string $path): self
    {
        return new self($path);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
