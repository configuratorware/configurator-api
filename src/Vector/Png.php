<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

final class Png
{
    public const EXTENSION = 'png';

    /**
     * @var string
     */
    private $path;

    /**
     * @param string $path
     */
    private function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @param string $path
     *
     * @return static
     */
    public static function withPath(string $path): self
    {
        return new self($path);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
