<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

final class Html
{
    /**
     * @var string
     */
    private $content;

    /**
     * @param string $content
     */
    private function __construct(string $content)
    {
        $this->content = $content;
    }

    public static function withContent(string $content): self
    {
        return new self($content);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        $tempName = sys_get_temp_dir() . '/' . uniqid('html_file', false);
        file_put_contents($tempName, $this->content);

        return $tempName;
    }

    public function getContent(): string
    {
        return $this->content;
    }
}
