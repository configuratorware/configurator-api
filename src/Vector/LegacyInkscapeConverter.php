<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * @internal
 */
final class LegacyInkscapeConverter implements InkscapeConverterInterface
{
    /**
     * @var Filesystem
     */
    private $fs;

    public function __construct(Filesystem $fs)
    {
        $this->fs = $fs;
    }

    /**
     * {@inheritDoc}
     *
     * @psalm-suppress TooManyArguments
     */
    public function vectorToSvg(Vector $vector): Svg
    {
        $sourceFilePath = $vector->getPath();
        $extension = $vector->getExtension();
        if (!$this->fs->exists($sourceFilePath)) {
            throw new \InvalidArgumentException('Source file doesn\'t exist');
        }

        // create a temp symlink with extension - Inkscape uses extension for file type guessing
        $tempSourceFilePath = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid('tempSourceFilePath') . '.' . $extension;
        $this->fs->symlink($sourceFilePath, $tempSourceFilePath); // symlink for avoiding file copy

        // create temporary SVG file to read the vector file contents
        $tempSvgFilePath = $this->fs->tempnam(sys_get_temp_dir(), 'tempSvgFilePath', '.svg');
        $command = 'inkscape --without-gui --file="' . $tempSourceFilePath . '" --export-plain-svg="' . $tempSvgFilePath . '"';
        $process = Process::fromShellCommandline($command);
        $process->mustRun();

        // get SVG content
        $svg = Svg::withContent(file_get_contents($tempSvgFilePath));

        // delete temporary files
        $this->fs->remove($tempSvgFilePath);
        $this->fs->remove($tempSourceFilePath);

        return $svg;
    }

    /**
     * {@inheritDoc}
     *
     * @psalm-suppress TooManyArguments
     */
    public function vectorToPng(Vector $vector, int $dpi = null): Png
    {
        $tempPath = $this->fs->tempnam(sys_get_temp_dir(), 'vector_image_pre_convert', '.png');
        $command = 'inkscape --without-gui --file="' . $vector->getPath() . '" --export-png="' . $tempPath . '"';
        if (null !== $dpi) {
            $command .= ' --export-dpi=' . $dpi;
        }

        $process = Process::fromShellCommandline($command);
        $process->mustRun();

        return Png::withPath($tempPath);
    }

    /**
     * {@inheritDoc}
     *
     * @psalm-suppress TooManyArguments
     */
    public function svgToPng(Svg $svg, int $dpi = null): Png
    {
        $sourcePath = $this->fs->tempnam(sys_get_temp_dir(), 'svgToPng', '.svg');
        $targetPath = $this->fs->tempnam(sys_get_temp_dir(), 'svgToPng', '.png');

        $this->fs->appendToFile($sourcePath, $svg->getContent());

        $command = 'inkscape --without-gui --file="' . $sourcePath . '" --export-png="' . $targetPath . '"';
        if (null !== $dpi) {
            $command .= ' --export-dpi=' . $dpi;
        }

        $process = Process::fromShellCommandline($command);
        $process->mustRun();

        $this->fs->remove($sourcePath);

        return Png::withPath($targetPath);
    }

    /**
     * @param Svg $svg
     *
     * @return Pdf
     *
     * @psalm-suppress TooManyArguments
     */
    public function svgToPdf(Svg $svg): Pdf
    {
        $sourcePath = $this->fs->tempnam(sys_get_temp_dir(), 'svgToPdf', '.svg');
        $targetPath = $this->fs->tempnam(sys_get_temp_dir(), 'svgToPdf', '.pdf');

        $this->fs->appendToFile($sourcePath, $svg->getContent());

        $command = 'inkscape --export-pdf="' . $targetPath . '" ' . $sourcePath;

        $process = Process::fromShellCommandline($command);
        $process->mustRun();

        $this->fs->remove($sourcePath);

        return Pdf::withPath($targetPath);
    }
}
