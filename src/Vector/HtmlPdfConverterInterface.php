<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

interface HtmlPdfConverterInterface
{
    public function htmlToPdf(Html $html): Pdf;
}
