<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

use Redhotmagma\ConfiguratorApiBundle\Exception\PdfConversionFailed;
use Redhotmagma\ConfiguratorApiBundle\Factory\DompdfFactory;
use Symfony\Component\Filesystem\Filesystem;

final class DompdfHtmlPdfConverter implements HtmlPdfConverterInterface
{
    private const TOOL = 'Dompdf';

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var DompdfFactory
     */
    private $dompdfFactory;

    /**
     * @param DompdfFactory $dompdfFactory
     * @param Filesystem $fs
     */
    public function __construct(DompdfFactory $dompdfFactory, Filesystem $fs)
    {
        $this->dompdfFactory = $dompdfFactory;
        $this->fs = $fs;
    }

    /**
     * @param Html $html
     *
     * @return Pdf
     */
    public function htmlToPdf(Html $html): Pdf
    {
        $pdfPath = Pdf::generateTempPath();

        $pdfContent = $this->createPdfContent($html);

        $this->fs->dumpFile($pdfPath, $pdfContent);

        return Pdf::withPath($pdfPath);
    }

    /**
     * @param Html $html
     *
     * @return string
     */
    private function createPdfContent(Html $html): string
    {
        $dompdf = $this->dompdfFactory->create();
        $dompdf->getOptions()->setDpi(300);
        $dompdf->setPaper('A4');

        $htmlContent = $html->getContent();
        $dompdf->loadHtml($htmlContent);
        $dompdf->render();

        $pdfContent = $dompdf->output();
        if (null === $pdfContent) {
            throw PdfConversionFailed::withTool(self::TOOL, $htmlContent);
        }

        return $pdfContent;
    }
}
