<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

final class Svg
{
    public const EXTENSION = 'svg';

    /**
     * @var string
     */
    private $content;

    /**
     * @param string $content
     */
    private function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @param string $content
     *
     * @return static
     */
    public static function withContent(string $content): self
    {
        return new self($content);
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
