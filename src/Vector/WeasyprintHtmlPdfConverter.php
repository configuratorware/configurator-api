<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Exception\PdfConversionFailed;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;

final class WeasyprintHtmlPdfConverter implements HtmlPdfConverterInterface
{
    private const TOOL = 'Weasyprint';

    /**
     * @var ProcessFactory
     */
    private $processFactory;

    public function __construct(ProcessFactory $processFactory)
    {
        $this->processFactory = $processFactory;
    }

    /**
     * @param Html $html
     *
     * @return Pdf
     */
    public function htmlToPdf(Html $html): Pdf
    {
        $pdfPath = Pdf::generateTempPath();

        $this->runWeasyprint($html->getPath(), $pdfPath);

        return Pdf::withPath($pdfPath);
    }

    /**
     * @param string $htmlSourcePath
     * @param string $pdfTargetPath
     *
     * @throws FileException
     */
    private function runWeasyprint(string $htmlSourcePath, string $pdfTargetPath): void
    {
        $process = $this->processFactory->create(['weasyprint', $htmlSourcePath, $pdfTargetPath]);
        $process->run();

        if (!$process->isSuccessful()) {
            throw PdfConversionFailed::withTool(self::TOOL, file_get_contents($htmlSourcePath));
        }
    }
}
