<?php

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

interface InkscapeConverterInterface extends SvgPngConverterInterface, SvgPdfConverterInterface, VectorPngConverterInterface, VectorSvgConverterInterface
{
}
