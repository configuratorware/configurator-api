<?php

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

interface VectorPngConverterInterface
{
    public function vectorToPng(Vector $vector, int $dpi = null): Png;
}
