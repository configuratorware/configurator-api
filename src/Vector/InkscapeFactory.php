<?php

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class InkscapeFactory
{
    public static function create(): InkscapeConverterInterface
    {
        if (self::isNewInkscapeVersion()) {
            return new InkscapeConverter(new Filesystem());
        }

        return new LegacyInkscapeConverter(new Filesystem());
    }

    public static function isNewInkscapeVersion(): bool
    {
        $command = 'inkscape --version';

        try {
            $process = Process::fromShellCommandline($command);
            $process->mustRun();
            $version = $process->getOutput();
            preg_match('/\d+\.\d+\.\d+/', $version, $match);

            if (1 === count($match)) {
                return version_compare($match[0], '1.2.2') >= 0;
            }
        } catch (\Throwable $t) {
            return false;
        }

        return false;
    }
}
