<?php

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

interface SvgPdfConverterInterface
{
    public function svgToPdf(Svg $svg): Pdf;
}
