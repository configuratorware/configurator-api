<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Vector;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;

final class Vector
{
    /**
     * @var string
     */
    private $extension;

    /**
     * @var string
     */
    private $path;

    /**
     * @param string $extension
     * @param string $path
     */
    private function __construct(string $extension, string $path)
    {
        $this->extension = $extension;
        $this->path = $path;
    }

    /**
     * @param string $path
     *
     * @return static
     */
    public static function withPath(string $path): self
    {
        $extension = (new \SplFileInfo($path))->getExtension();
        if ('' === $extension) {
            throw FileException::cannotGuessExtension($path);
        }

        return new self(strtolower($extension), $path);
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
