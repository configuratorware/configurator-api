<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalItemPrice\DesignerGlobalItemPriceApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPriceItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class DesignerGlobalItemPricesController
{
    /**
     * @var DesignerGlobalItemPriceApi
     */
    private $designerGlobalItemPricesApi;

    /**
     * DesignerGlobalItemPricesController constructor.
     *
     * @param DesignerGlobalItemPriceApi $designerGlobalItemPricesApi
     */
    public function __construct(DesignerGlobalItemPriceApi $designerGlobalItemPricesApi)
    {
        $this->designerGlobalItemPricesApi = $designerGlobalItemPricesApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->designerGlobalItemPricesApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return DesignerGlobalItemPriceItem
     */
    public function detailAction(int $id): DesignerGlobalItemPriceItem
    {
        $structure = $this->designerGlobalItemPricesApi->getOne($id);

        return $structure;
    }

    /**
     * @param DesignerGlobalItemPriceItem $structure
     *
     * @return DesignerGlobalItemPriceItem
     */
    public function saveAction(
        DesignerGlobalItemPriceItem $structure
    ): DesignerGlobalItemPriceItem {
        $structure = $this->designerGlobalItemPricesApi->save($structure);

        return $structure;
    }
}
