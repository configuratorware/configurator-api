<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\CreatorItem\CreatorItemApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class CreatorItemsController
{
    /**
     * @var CreatorItemApi
     */
    private $creatorItemApi;

    /**
     * CreatorItemsController constructor.
     *
     * @param CreatorItemApi $creatorItemApi
     */
    public function __construct(CreatorItemApi $creatorItemApi)
    {
        $this->creatorItemApi = $creatorItemApi;
    }

    /**
     * returns a list of item objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->creatorItemApi->getListResult($arguments);

        return $result;
    }
}
