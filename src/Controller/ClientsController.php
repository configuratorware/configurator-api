<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientUserCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Client;
use Redhotmagma\ConfiguratorApiBundle\Structure\CurrentClient;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ClientLogoUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FontUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FrontendClientArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;

final class ClientsController
{
    /**
     * @var ClientApi
     */
    private $clientApi;

    /**
     * @var ClientUserCheck
     */
    private $clientUserCheck;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var Security
     */
    private $security;

    /**
     * ClientsController constructor.
     *
     * @param ClientApi $clientApi
     * @param ClientRepository $clientRepository
     * @param ClientUserCheck $clientUserCheck
     * @param Security $security
     */
    public function __construct(
        ClientApi $clientApi,
        ClientRepository $clientRepository,
        ClientUserCheck $clientUserCheck,
        Security $security
    ) {
        $this->clientApi = $clientApi;
        $this->clientUserCheck = $clientUserCheck;
        $this->clientRepository = $clientRepository;
        $this->security = $security;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        return $this->clientApi->getListResult($arguments);
    }

    /**
     * @param int $id
     *
     * @return Client
     */
    public function detailAction(int $id): Client
    {
        return $this->clientApi->getOne($id);
    }

    /**
     * @param Client $structure
     *
     * @return Client
     */
    public function saveAction(Client $structure): Client
    {
        return $this->clientApi->save($structure);
    }

    /**
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id): bool
    {
        $this->clientApi->delete($id);

        return true;
    }

    /**
     * @param FrontendClientArguments $frontendClientArguments
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Client
     */
    public function frontendClientAction(
        FrontendClientArguments $frontendClientArguments
    ): \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Client {
        $client = $this->clientApi->frontendClient($frontendClientArguments->client, $frontendClientArguments->channel);

        return $client;
    }

    /**
     * @param $id
     * @param FontUploadArguments $font
     *
     * @return Response
     */
    public function uploadFontAction($id, FontUploadArguments $font): Response
    {
        $this->clientApi->uploadFont((int)$id, $font);

        return new Response();
    }

    /**
     * @param $id
     * @param ClientLogoUploadArguments $font
     *
     * @return Response
     */
    public function uploadLogoAction($id, ClientLogoUploadArguments $font): Response
    {
        $this->clientApi->uploadLogo((int)$id, $font);

        return new Response();
    }

    /**
     * @return Client
     */
    public function getDefaultAction(): Client
    {
        return $this->clientApi->getDefault();
    }

    /**
     * @return CurrentClient
     */
    public function getCurrentAction(): CurrentClient
    {
        $client = $this->security->getUser()->getMainClient();

        if (null === $client || !$this->clientUserCheck->currentUserHasClientRole()) {
            throw new AccessDeniedException();
        }

        return $this->clientApi->getCurrent($client);
    }

    /**
     * @param CurrentClient $structure
     *
     * @return CurrentClient
     */
    public function saveCurrentAction(CurrentClient $structure): CurrentClient
    {
        if (!isset($structure->id)) {
            throw new AccessDeniedException();
        }

        $client = $this->clientRepository->find($structure->id);

        if (null === $client) {
            throw new NotFoundException();
        }

        $this->clientUserCheck->checkClient($client);

        $user = $this->security->getUser();

        if (!$user->hasClient($client)) {
            throw new AccessDeniedException();
        }

        return $this->clientApi->saveCurrent($structure);
    }
}
