<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\ApiVersionApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\ApiVersion;

final class ApiVersionController
{
    /**
     * @var ApiVersionApi
     */
    private $apiVersionApi;

    public function __construct(ApiVersionApi $apiVersionApi)
    {
        $this->apiVersionApi = $apiVersionApi;
    }

    /**
     * Get the current API version.
     *
     * @return ApiVersion
     */
    public function versionAction(): ApiVersion
    {
        return $this->apiVersionApi->getVersionInfo();
    }
}
