<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Attribute\AttributeApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class AttributesController
{
    /**
     * @var AttributeApi
     */
    private $attributeApi;

    /**
     * AttributesController constructor.
     *
     * @param AttributeApi $attributeApi
     */
    public function __construct(AttributeApi $attributeApi)
    {
        $this->attributeApi = $attributeApi;
    }

    /**
     * returns a list of attribute objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->attributeApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one attribute object by its id.
     *
     * @param int $id
     *
     * @return Attribute
     */
    public function detailAction(int $id)
    {
        $structure = $this->attributeApi->getOne($id);

        return $structure;
    }

    /**
     * save an attribute.
     *
     * @param Attribute $attribute
     *
     * @return Attribute
     */
    public function saveAction(Attribute $attribute)
    {
        $attribute = $this->attributeApi->save($attribute);

        return $attribute;
    }

    /**
     * Deletes attributes by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->attributeApi->delete($id);

        return true;
    }
}
