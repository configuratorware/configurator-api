<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Group\GroupApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Group;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

final class GroupsController
{
    /**
     * @var GroupApi
     */
    private $groupApi;

    /**
     * GroupsController constructor.
     *
     * @param GroupApi $groupApi
     */
    public function __construct(GroupApi $groupApi)
    {
        $this->groupApi = $groupApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->groupApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return Group
     */
    public function detailAction(int $id)
    {
        $structure = $this->groupApi->getOne($id);

        return $structure;
    }

    /**
     * @param Group $group
     *
     * @return Group
     */
    public function saveAction(Group $group)
    {
        $group = $this->groupApi->save($group);

        return $group;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->groupApi->delete($id);

        return true;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     *
     * @return bool
     */
    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments)
    {
        $this->groupApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }
}
