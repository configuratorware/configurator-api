<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationInfo\ConfigurationInfoApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationInfo;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConfigurationInfoArguments;

final class ConfigurationsInfoController
{
    private ConfigurationInfoApi $configurationInfoApi;

    public function __construct(ConfigurationInfoApi $configurationInfoApi)
    {
        $this->configurationInfoApi = $configurationInfoApi;
    }

    public function loadConfigurationInfoAction(string $code, ConfigurationInfoArguments $configurationInfoArguments): ConfigurationInfo
    {
        $configurationInfo = $this->configurationInfoApi->getConfigurationInfo($code, $configurationInfoArguments->extendedData);

        return $configurationInfo;
    }

    public function loadProductionInfoAction(string $code): ConfigurationInfo
    {
        $productionInfo = $this->configurationInfoApi->getProductionInfo($code);

        return $productionInfo;
    }
}
