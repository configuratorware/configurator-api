<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\DesignProductionMethodApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod as DesignProductionMethodStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class DesignProductionMethodController
{
    /**
     * @var DesignProductionMethodApi
     */
    private $designProductionMethodApi;

    /**
     * @param DesignProductionMethodApi $designProductionMethodApi
     */
    public function __construct(DesignProductionMethodApi $designProductionMethodApi)
    {
        $this->designProductionMethodApi = $designProductionMethodApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->designProductionMethodApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return DesignProductionMethodStructure
     */
    public function detailAction(int $id)
    {
        $structure = $this->designProductionMethodApi->getOne($id);

        return $structure;
    }

    /**
     * @param DesignProductionMethodStructure $structure
     *
     * @return DesignProductionMethodStructure
     */
    public function saveAction(DesignProductionMethodStructure $structure)
    {
        $structure = $this->designProductionMethodApi->save($structure);

        return $structure;
    }

    /**
     * Deletes Design Production Methods by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->designProductionMethodApi->delete($id);

        return true;
    }
}
