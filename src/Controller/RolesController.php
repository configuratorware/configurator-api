<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Role\RoleApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\Role;

final class RolesController
{
    /**
     * @var RoleApi
     */
    private $roleApi;

    /**
     * RolesController constructor.
     *
     * @param RoleApi $roleApi
     */
    public function __construct(RoleApi $roleApi)
    {
        $this->roleApi = $roleApi;
    }

    /**
     * returns a list of role objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->roleApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one role object by its id.
     *
     * @param int $id
     *
     * @return Role
     */
    public function detailAction(int $id)
    {
        $structure = $this->roleApi->getOne($id);

        return $structure;
    }

    /**
     * save an role.
     *
     * @param Role $role
     *
     * @return Role
     */
    public function saveAction(Role $role)
    {
        $role = $this->roleApi->save($role);

        return $role;
    }

    /**
     * Deletes roles by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->roleApi->delete($id);

        return true;
    }
}
