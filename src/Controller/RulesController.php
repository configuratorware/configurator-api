<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Rule\RuleApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rule;

final class RulesController
{
    /**
     * @var RuleApi
     */
    private $ruleApi;

    /**
     * RulesController constructor.
     *
     * @param RuleApi $ruleApi
     */
    public function __construct(RuleApi $ruleApi)
    {
        $this->ruleApi = $ruleApi;
    }

    /**
     * returns a list of rule objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->ruleApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one rule object by its id.
     *
     * @param int $id
     *
     * @return Rule
     */
    public function detailAction(int $id)
    {
        $structure = $this->ruleApi->getOne($id);

        return $structure;
    }

    /**
     * save an rule.
     *
     * @param Rule $rule
     *
     * @return Rule
     */
    public function saveAction(Rule $rule)
    {
        $rule = $this->ruleApi->save($rule);

        return $rule;
    }

    /**
     * Deletes rules by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->ruleApi->delete($id);

        return true;
    }
}
