<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette\ColorPaletteApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPalette as ColorPaletteStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

final class ColorPaletteController
{
    /**
     * @var ColorPaletteApi
     */
    private $colorPaletteApi;

    /**
     * @param ColorPaletteApi $colorPaletteApi
     */
    public function __construct(ColorPaletteApi $colorPaletteApi)
    {
        $this->colorPaletteApi = $colorPaletteApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->colorPaletteApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return ColorPaletteStructure
     */
    public function detailAction(int $id): ColorPaletteStructure
    {
        $structure = $this->colorPaletteApi->getOne($id);

        return $structure;
    }

    /**
     * @param ColorPaletteStructure $colorPalette
     *
     * @return ColorPaletteStructure
     */
    public function saveAction(ColorPaletteStructure $colorPalette): ColorPaletteStructure
    {
        $colorPalette = $this->colorPaletteApi->save($colorPalette);

        return $colorPalette;
    }

    /**
     * Deletes color palettes by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id): bool
    {
        $this->colorPaletteApi->delete($id);

        return true;
    }

    public function saveColorSequenceNumberAction($id, SequenceNumberArguments $sequenceNumbers): bool
    {
        $this->colorPaletteApi->saveColorSequenceNumbers($id, $sequenceNumbers);

        return true;
    }
}
