<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\ConfigurationModeApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as ConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerData;

final class ConfigurationModesController
{
    /**
     * @var ConfigurationModeApi
     */
    private $configurationModeApi;

    /**
     * @param ConfigurationModeApi $configurationModeApi
     */
    public function __construct(ConfigurationModeApi $configurationModeApi)
    {
        $this->configurationModeApi = $configurationModeApi;
    }

    /**
     * @param ConfigurationStructure $configuration
     *
     * @return DesignerData
     *
     * @throws \Exception
     */
    public function getDesignerDataAction(ConfigurationStructure $configuration): DesignerData
    {
        return $this->configurationModeApi->getData($configuration);
    }
}
