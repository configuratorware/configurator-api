<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Language\LanguageApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Language as LanguageStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class LanguagesController
{
    /**
     * @var LanguageApi
     */
    private $languageApi;

    /**
     * CodeSnippetsController constructor.
     *
     * @param LanguageApi $languageApi
     */
    public function __construct(LanguageApi $languageApi)
    {
        $this->languageApi = $languageApi;
    }

    /**
     * @return array
     */
    public function frontendListAction(): array
    {
        $result = $this->languageApi->getFrontendList();

        return $result;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->languageApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return LanguageStructure
     */
    public function detailAction(int $id): LanguageStructure
    {
        $structure = $this->languageApi->getOne($id);

        return $structure;
    }
}
