<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\LicenseFile;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\LicenseFileUploadArguments;
use Symfony\Bundle\SecurityBundle\Security;

final class LicenseController
{
    /**
     * @var LicenseApi
     */
    private $licenseApi;

    /**
     * @var Security
     */
    private $security;

    /**
     * @param LicenseApi $licenseApi
     * @param Security $security
     */
    public function __construct(LicenseApi $licenseApi, Security $security)
    {
        $this->licenseApi = $licenseApi;
        $this->security = $security;
    }

    /**
     * @param LicenseFileUploadArguments $arguments
     *
     * @return LicenseFile
     */
    public function uploadLicenseFileAction(LicenseFileUploadArguments $arguments): LicenseFile
    {
        /** @var User $user */
        $user = $this->security->getUser();

        return $this->licenseApi->uploadLicenseFile($arguments, $user);
    }
}
