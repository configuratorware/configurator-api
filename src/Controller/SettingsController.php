<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Setting\SettingApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Setting as SettingStructure;

final class SettingsController
{
    /**
     * @var SettingApi
     */
    private $settingApi;

    /**
     * @param SettingApi $settingApi
     */
    public function __construct(SettingApi $settingApi)
    {
        $this->settingApi = $settingApi;
    }

    /**
     * returns one tag object by its id.
     *
     * @param int $id
     *
     * @return SettingStructure
     */
    public function detailAction(int $id): SettingStructure
    {
        return $this->settingApi->getOne($id);
    }

    /**
     * @param SettingStructure $structure
     *
     * @return bool
     */
    public function saveAction(SettingStructure $structure): bool
    {
        return $this->settingApi->save($structure);
    }
}
