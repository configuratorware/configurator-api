<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ItemItemStatus\ItemItemStatusApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Connector\ItemStatus as ConnectorItemStatus;

final class ItemsItemStatusController
{
    /**
     * @var ItemItemStatusApi
     */
    private $itemItemStatusApi;

    /**
     * @param ItemItemStatusApi $itemItemStatusApi
     */
    public function __construct(ItemItemStatusApi $itemItemStatusApi)
    {
        $this->itemItemStatusApi = $itemItemStatusApi;
    }

    /**
     * @param string $itemIdentifiers
     *
     * @return ConnectorItemStatus[]
     */
    public function connectorListAction(string $itemIdentifiers): array
    {
        return $this->itemItemStatusApi->getConnectorListResult($itemIdentifiers);
    }
}
