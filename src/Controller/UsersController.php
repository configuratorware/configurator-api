<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\User\UserApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\ApiUser;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChangeUserPassword;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;
use Symfony\Bundle\SecurityBundle\Security;

final class UsersController
{
    /**
     * @var UserApi
     */
    private $userApi;

    /**
     * @var Security
     */
    private $security;

    /**
     * UsersController constructor.
     *
     * @param UserApi $userApi
     * @param Security $security
     */
    public function __construct(UserApi $userApi, Security $security)
    {
        $this->userApi = $userApi;
        $this->security = $security;
    }

    /**
     * returns a list of User objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        return $this->userApi->getListResult($arguments);
    }

    /**
     * returns one User object by its id.
     *
     * @param int $id
     *
     * @return User
     */
    public function detailAction(int $id): User
    {
        return $this->userApi->getOne($id);
    }

    /**
     * save an User.
     *
     * @param User $user
     *
     * @return User
     */
    public function saveAction(User $user): User
    {
        $user = $this->userApi->save($user);

        return $user;
    }

    /**
     * Deletes users by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id): bool
    {
        $this->userApi->delete($id);

        return true;
    }

    /**
     * @param ChangeUserPassword $userPassword
     *
     * @return bool
     */
    public function changePasswordAction(ChangeUserPassword $userPassword): bool
    {
        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\User $user */
        $user = $this->security->getUser();
        $this->userApi->changePassword($userPassword, $user);

        return true;
    }

    /**
     * returns a list of User objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listApiUserAction(ListRequestArguments $arguments): PaginationResult
    {
        return $this->userApi->getApiUserListResult($arguments);
    }

    /**
     * returns one User object by its id.
     *
     * @param int $id
     *
     * @return ApiUser
     */
    public function detailApiUserAction(int $id): ApiUser
    {
        return $this->userApi->getApiUser($id);
    }

    /**
     * @param ApiUser $user
     *
     * @return ApiUser
     *
     * @throws \Exception
     */
    public function saveApiUserAction(ApiUser $user): ApiUser
    {
        return $this->userApi->saveApiUser($user);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteApiUserAction($id): bool
    {
        $this->userApi->delete($id);

        return true;
    }

    /**
     * returns one User object by its id.
     *
     * @param int $id
     *
     * @return bool
     */
    public function refreshKeyAction(int $id): bool
    {
        $this->userApi->refreshKey($id);

        return true;
    }
}
