<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Service\FrontendTranslationFile\FrontendTranslationFileApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FrontendTranslationFileUploadArguments;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

final class FrontendTranslationFileController
{
    /**
     * @var FrontendTranslationFileApi
     */
    private FrontendTranslationFileApi $translationsApi;

    public function __construct(FrontendTranslationFileApi $translationsApi)
    {
        $this->translationsApi = $translationsApi;
    }

    public function downloadAction(string $iso): FileResult
    {
        try {
            return $this->translationsApi->download($iso);
        } catch (FileNotFoundException $exception) {
            throw new NotFoundException();
        }
    }

    public function saveAction(string $iso, FrontendTranslationFileUploadArguments $translationUploadArguments): bool
    {
        $this->translationsApi->save($iso, $translationUploadArguments);

        return true;
    }
}
