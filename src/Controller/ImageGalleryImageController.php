<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage\ImageGalleryImageApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageFrontendList;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageUploadData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Symfony\Component\HttpFoundation\JsonResponse;

final class ImageGalleryImageController
{
    /**
     * @var ImageGalleryImageApi
     */
    private $imageGalleryImageApi;

    /**
     * ImageGalleryImageController constructor.
     *
     * @param ImageGalleryImageApi $imageGalleryImageApi
     */
    public function __construct(ImageGalleryImageApi $imageGalleryImageApi)
    {
        $this->imageGalleryImageApi = $imageGalleryImageApi;
    }

    /**
     * @param ImageGalleryImageFrontendList $imageGalleryImageFrontendList
     *
     * @return JsonResponse
     */
    public function frontendListAction(ImageGalleryImageFrontendList $imageGalleryImageFrontendList)
    {
        $response = $this->imageGalleryImageApi->frontendList($imageGalleryImageFrontendList);

        return new JsonResponse($response);
    }

    /**
     * @return JsonResponse
     */
    public function frontendListTagsAction()
    {
        $response = $this->imageGalleryImageApi->frontendListTags();

        return new JsonResponse($response);
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->imageGalleryImageApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return ImageGalleryImage
     */
    public function detailAction(int $id)
    {
        $structure = $this->imageGalleryImageApi->getOne($id);

        return $structure;
    }

    /**
     * @param ImageGalleryImage $attribute
     *
     * @return ImageGalleryImage
     */
    public function saveAction(ImageGalleryImage $attribute)
    {
        $attribute = $this->imageGalleryImageApi->save($attribute);

        return $attribute;
    }

    /**
     * @param ImageGalleryImageUploadData $imageGalleryImageUploadData
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage
     */
    public function uploadAction(ImageGalleryImageUploadData $imageGalleryImageUploadData)
    {
        $response = $this->imageGalleryImageApi->upload($imageGalleryImageUploadData);

        return $response;
    }

    /**
     * Deletes attributes by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->imageGalleryImageApi->delete($id);

        return true;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     *
     * @return bool
     */
    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments)
    {
        $this->imageGalleryImageApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }
}
