<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification\ItemClassificationApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Component\HttpFoundation\JsonResponse;

final class ItemClassificationsController
{
    /**
     * @var ItemClassificationApi
     */
    private $itemClassificationApi;

    /**
     * ItemClassificationsController constructor.
     *
     * @param ItemClassificationApi $itemClassificationApi
     */
    public function __construct(ItemClassificationApi $itemClassificationApi)
    {
        $this->itemClassificationApi = $itemClassificationApi;
    }

    /**
     * returns a list of itemClassification objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->itemClassificationApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one itemClassification object by its id.
     *
     * @param int $id
     *
     * @return ItemClassification
     */
    public function detailAction(int $id)
    {
        $structure = $this->itemClassificationApi->getOne($id);

        return $structure;
    }

    /**
     * save an itemClassification.
     *
     * @param ItemClassification $itemClassification
     *
     * @return ItemClassification
     */
    public function saveAction(ItemClassification $itemClassification)
    {
        $itemClassification = $this->itemClassificationApi->save($itemClassification);

        return $itemClassification;
    }

    /**
     * Deletes itemClassifications by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->itemClassificationApi->delete($id);

        return true;
    }

    /**
     * Returns the list of itemClassifications that have at least one Item.
     *
     * @return JsonResponse
     */
    public function frontendListAction(): JsonResponse
    {
        return new JsonResponse($this->itemClassificationApi->frontendList());
    }
}
