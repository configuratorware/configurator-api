<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationMode\VisualizationModeApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class VisualizationModesController
{
    /**
     * @var VisualizationModeApi
     */
    private $visualizationModeApi;

    /**
     * @param VisualizationModeApi $visualizationModeApi
     */
    public function __construct(VisualizationModeApi $visualizationModeApi)
    {
        $this->visualizationModeApi = $visualizationModeApi;
    }

    /**
     * @param ListRequestArguments $listRequestArguments
     * @param string $configurationMode
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $listRequestArguments, string $configurationMode): PaginationResult
    {
        return $this->visualizationModeApi->getListResult($listRequestArguments, $configurationMode);
    }
}
