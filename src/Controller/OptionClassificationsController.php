<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification\FrontendOptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification\OptionClassificationApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification as OptionClassificationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadComponentThumbnail;
use Symfony\Component\HttpFoundation\Response;

final class OptionClassificationsController
{
    private OptionClassificationApi $optionClassificationApi;
    private FrontendOptionClassification $frontendOptionClassification;

    public function __construct(OptionClassificationApi $optionClassificationApi, FrontendOptionClassification $frontendOptionClassification)
    {
        $this->optionClassificationApi = $optionClassificationApi;
        $this->frontendOptionClassification = $frontendOptionClassification;
    }

    public function listAction(ListRequestNoDefaultArguments $arguments): PaginationResult
    {
        $result = $this->optionClassificationApi->getListResult($arguments);

        return $result;
    }

    /**
     * @return OptionClassificationStructure[]
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function listWithOptionsForItemAction(string $itemIdentifier): array
    {
        $result = $this->frontendOptionClassification->getByItemIdentifier($itemIdentifier);

        return $result;
    }

    public function detailAction(int $id): OptionClassification
    {
        $structure = $this->optionClassificationApi->getOne($id);

        return $structure;
    }

    public function saveAction(OptionClassification $optionClassification): OptionClassification
    {
        $optionClassification = $this->optionClassificationApi->save($optionClassification);

        return $optionClassification;
    }

    /**
     * Deletes optionClassifications by id(s)
     * multiple ids can be used comma separated.
     */
    public function deleteAction(string $id): bool
    {
        $this->optionClassificationApi->delete($id);

        return true;
    }

    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments)
    {
        $this->optionClassificationApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }

    public function uploadThumbnail(UploadComponentThumbnail $uploadComponentThumbnail): Response
    {
        try {
            $this->optionClassificationApi->uploadThumbnail($uploadComponentThumbnail);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    public function deleteThumbnail(int $id): Response
    {
        try {
            $this->optionClassificationApi->deleteThumbnail($id);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }
}
