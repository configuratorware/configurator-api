<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree\QuestionTreeApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\QuestionTree as FrontendQuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\CommaSeparatedList;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\QuestionTreeUploadData;

final class QuestionTreesController
{
    private QuestionTreeApi $questionTreeApi;

    public function __construct(QuestionTreeApi $questionTreeApi)
    {
        $this->questionTreeApi = $questionTreeApi;
    }

    public function frontendDetailAction(string $identifier, ControlParameters $parameters): FrontendQuestionTree
    {
        return $this->questionTreeApi->getFrontendDetail($identifier, $parameters);
    }

    public function listAction(ListRequestArguments $arguments, ControlParameters $parameters): PaginationResult
    {
        return $this->questionTreeApi->getListResult($arguments, $parameters);
    }

    public function detailAction(int $id, ControlParameters $parameters): QuestionTree
    {
        return $this->questionTreeApi->getOne($id, $parameters);
    }

    public function saveAction(QuestionTree $unsavedQuestionTree, ControlParameters $parameters): QuestionTree
    {
        return $this->questionTreeApi->save($unsavedQuestionTree, $parameters);
    }

    public function uploadAction(QuestionTreeUploadData $questionTreeUploadData): bool
    {
        return $this->questionTreeApi->upload($questionTreeUploadData);
    }

    /**
     * Deletes attributes by id(s)
     * multiple ids can be used comma separated.
     *
     * @param CommaSeparatedList $idList
     *
     * @return bool
     */
    public function deleteAction(CommaSeparatedList $idList): bool
    {
        $this->questionTreeApi->delete($idList->elements);

        return true;
    }
}
