<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\DataCheck\DataCheckApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;

final class DataCheckController
{
    /**
     * @var DataCheckApi
     */
    private $dataCheckApi;

    /**
     * DataCheckController constructor.
     *
     * @param DataCheckApi $dataCheckApi
     */
    public function __construct(DataCheckApi $dataCheckApi)
    {
        $this->dataCheckApi = $dataCheckApi;
    }

    /**
     * @param string $itemIdentifier
     * @param ControlParameters $controlParameters
     *
     * @return DataCheck[]
     */
    public function listAction(string $itemIdentifier, ControlParameters $controlParameters): array
    {
        return $this->dataCheckApi->getList($itemIdentifier, $controlParameters);
    }
}
