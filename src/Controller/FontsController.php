<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Font\FontApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FontUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Symfony\Component\HttpFoundation\JsonResponse;

final class FontsController
{
    /**
     * @var FontApi
     */
    private $fontApi;

    /**
     * @param FontApi $fontApi
     */
    public function __construct(FontApi $fontApi)
    {
        $this->fontApi = $fontApi;
    }

    /**
     * @return JsonResponse
     */
    public function frontendDesignerListAction(): JsonResponse
    {
        $result = $this->fontApi->getFrontendDesignerList();

        return new JsonResponse($result);
    }

    /**
     * @param ListRequestNoDefaultArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestNoDefaultArguments $arguments): PaginationResult
    {
        $result = $this->fontApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return FontStructure
     */
    public function detailAction(int $id): FontStructure
    {
        $structure = $this->fontApi->getOne($id);

        return $structure;
    }

    /**
     * @param FontStructure $structure
     *
     * @return FontStructure
     *
     * @throws \Exception
     */
    public function saveAction(FontStructure $structure): FontStructure
    {
        $structure = $this->fontApi->save($structure);

        return $structure;
    }

    public function saveSequenceNumberAction(SequenceNumberArguments $arguments)
    {
        $this->fontApi->updateSequence($arguments);

        return true;
    }

    /**
     * @param FontUploadArguments $fontUpload
     *
     * @return bool
     */
    public function uploadAction(FontUploadArguments $fontUpload)
    {
        $this->fontApi->upload($fontUpload);

        return true;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id): bool
    {
        $this->fontApi->delete($id);

        return true;
    }
}
