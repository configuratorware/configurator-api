<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Currency\CurrencyApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Currency;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class CurrenciesController
{
    /**
     * @var CurrencyApi
     */
    private $currencyApi;

    /**
     * CurrencysController constructor.
     *
     * @param CurrencyApi $currencyApi
     */
    public function __construct(CurrencyApi $currencyApi)
    {
        $this->currencyApi = $currencyApi;
    }

    /**
     * returns a list of currency objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->currencyApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one currency object by its id.
     *
     * @param int $id
     *
     * @return Currency
     */
    public function detailAction(int $id)
    {
        $structure = $this->currencyApi->getOne($id);

        return $structure;
    }

    /**
     * save an currency.
     *
     * @param Currency $currency
     *
     * @return Currency
     */
    public function saveAction(Currency $currency)
    {
        $currency = $this->currencyApi->save($currency);

        return $currency;
    }

    /**
     * Deletes currencys by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->currencyApi->delete($id);

        return true;
    }
}
