<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadCreatorViewThumbnail;
use Symfony\Component\HttpFoundation\Response;

final class CreatorViewsController
{
    /**
     * @var CreatorViewApi
     */
    private $creatorViewApi;

    /**
     * @param CreatorViewApi $creatorViewApi
     */
    public function __construct(CreatorViewApi $creatorViewApi)
    {
        $this->creatorViewApi = $creatorViewApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function list(ListRequestArguments $arguments): PaginationResult
    {
        return $this->creatorViewApi->getListResult($arguments);
    }

    /**
     * @param int $id
     *
     * @return CreatorView
     */
    public function detail(int $id): CreatorView
    {
        return $this->creatorViewApi->getOne($id);
    }

    /**
     * @param CreatorView $creatorView
     *
     * @return CreatorView
     */
    public function save(CreatorView $creatorView): CreatorView
    {
        return $this->creatorViewApi->save($creatorView);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function delete(int $id): Response
    {
        $this->creatorViewApi->delete($id);

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param UploadCreatorViewThumbnail $uploadViewThumbnail
     *
     * @return Response
     */
    public function uploadThumbnail(UploadCreatorViewThumbnail $uploadViewThumbnail): Response
    {
        $this->creatorViewApi->uploadThumbnail($uploadViewThumbnail);

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function deleteThumbnail(int $id): Response
    {
        $this->creatorViewApi->deleteThumbnail($id);

        return new Response(null, Response::HTTP_OK);
    }
}
