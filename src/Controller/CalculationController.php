<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\CalculationApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

final class CalculationController
{
    /**
     * @var CalculationApi
     */
    private $calculationApi;

    /**
     * CalculationController constructor.
     *
     * @param CalculationApi $calculationApi
     */
    public function __construct(CalculationApi $calculationApi)
    {
        $this->calculationApi = $calculationApi;
    }

    /**
     * Calculates the current price for the configuration.
     * Expects a configuration object as payload.
     *
     * @param Configuration $configuration
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult
     */
    public function calculateAction(Configuration $configuration)
    {
        $result = $this->calculationApi->calculate($configuration);

        return $result;
    }
}
