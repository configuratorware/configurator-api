<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant\ConfigurationVariantApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationVariant;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

final class ConfigurationVariantsController
{
    /**
     * @var ConfigurationVariantApi
     */
    private $configurationVariantApi;

    /**
     * @param ConfigurationVariantApi $configurationVariantApi
     */
    public function __construct(ConfigurationVariantApi $configurationVariantApi)
    {
        $this->configurationVariantApi = $configurationVariantApi;
    }

    /**
     * returns a list of ConfigurationVariant objects.
     *
     * @Route(
     *     "/api/configurationvariants{trailingSlash}",
     *     name="configuratorware.api.configuration_variants_list",
     *     defaults={"trailingSlash": "/"},
     *     methods={"GET"},
     *     requirements={
     *         "trailingSlash" : "[/]{0,1}"
     *     }
     * )
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->configurationVariantApi->getListResult($arguments);

        return $result;
    }

    /**
     * find items to add to an item group
     * already linked items are marked, the gui can then display a warning.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function findItemsAction(ListRequestArguments $arguments): PaginationResult
    {
        $items = $this->configurationVariantApi->findItems($arguments);

        return $items;
    }

    /**
     * returns one ConfigurationVariant object by its id.
     *
     * @param int $id
     *
     * @return ConfigurationVariant
     */
    public function detailAction(int $id)
    {
        $structure = $this->configurationVariantApi->getOne($id);

        return $structure;
    }

    /**
     * save an configuration variants.
     *
     * @param ConfigurationVariant $configurationVariant
     *
     * @return ConfigurationVariant
     */
    public function saveAction(ConfigurationVariant $configurationVariant)
    {
        $configurationVariant = $this->configurationVariantApi->save($configurationVariant);

        return $configurationVariant;
    }

    /**
     * Deletes configuration variants by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->configurationVariantApi->delete($id);

        return true;
    }

    /**
     * gets configuration variants for an item (identifier) for the frontend.
     *
     * @param string $itemIdentifier
     *
     * @return JsonResponse
     */
    public function getConfigurationVariantsAction($itemIdentifier)
    {
        $response = $this->configurationVariantApi->getFrontendConfigurationVariants($itemIdentifier);

        // return a JsonResponse to prevent 404 if the array is empty
        return new JsonResponse($response);
    }
}
