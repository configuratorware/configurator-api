<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo\MediaInfoApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\MediaInfo;

final class MediaInfoController
{
    /**
     * @var MediaInfoApi
     */
    private $mediaInfoApi;

    /**
     * @param MediaInfoApi $mediaInfoApi
     */
    public function __construct(MediaInfoApi $mediaInfoApi)
    {
        $this->mediaInfoApi = $mediaInfoApi;
    }

    /**
     * @param int $id
     *
     * @return MediaInfo
     */
    public function findMediaInfo(int $id): MediaInfo
    {
        return $this->mediaInfoApi->findMediaInfo($id);
    }
}
