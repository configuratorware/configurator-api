<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Channel\ChannelApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Channel;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class ChannelsController
{
    /**
     * @var ChannelApi
     */
    private $channelApi;

    /**
     * AttributesController constructor.
     *
     * @param ChannelApi $channelApi
     */
    public function __construct(ChannelApi $channelApi)
    {
        $this->channelApi = $channelApi;
    }

    /**
     * returns a list of channel objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->channelApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one channel object by its id.
     *
     * @param int $id
     *
     * @return Channel
     */
    public function detailAction(int $id)
    {
        $structure = $this->channelApi->getOne($id);

        return $structure;
    }

    /**
     * save an channel.
     *
     * @param Channel $channel
     *
     * @return Channel
     */
    public function saveAction(Channel $channel)
    {
        $channel = $this->channelApi->save($channel);

        return $channel;
    }

    /**
     * Deletes channels by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->channelApi->delete($id);

        return true;
    }
}
