<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

final class AdminModeController
{
    /**
     * @var string
     */
    private $adminModeHash;

    /**
     * @param string $adminModeHash
     */
    public function __construct(string $adminModeHash)
    {
        $this->adminModeHash = $adminModeHash;
    }

    /**
     * returns the admin mode hash, configured in parameters.yml.
     *
     * @return array
     */
    public function getHashAction()
    {
        $response = ['hash' => $this->adminModeHash];

        return $response;
    }
}
