<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalCalculationType\DesignerGlobalCalculationTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class DesignerGlobalCalculationTypesController
{
    /**
     * @var DesignerGlobalCalculationTypeApi
     */
    private $designerGlobalCalculationTypeApi;

    /**
     * @param DesignerGlobalCalculationTypeApi $designerGlobalCalculationTypeApi
     */
    public function __construct(DesignerGlobalCalculationTypeApi $designerGlobalCalculationTypeApi)
    {
        $this->designerGlobalCalculationTypeApi = $designerGlobalCalculationTypeApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->designerGlobalCalculationTypeApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return DesignerGlobalCalculationTypeStructure
     */
    public function detailAction(int $id): DesignerGlobalCalculationTypeStructure
    {
        $structure = $this->designerGlobalCalculationTypeApi->getOne($id);

        return $structure;
    }

    /**
     * @param DesignerGlobalCalculationTypeStructure $structure
     *
     * @return DesignerGlobalCalculationTypeStructure
     */
    public function saveAction(
        DesignerGlobalCalculationTypeStructure $structure
    ): DesignerGlobalCalculationTypeStructure {
        $structure = $this->designerGlobalCalculationTypeApi->save($structure);

        return $structure;
    }

    /**
     * Deletes Design erGlobalCalculationTypes by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $ids
     *
     * @return bool
     */
    public function deleteAction($ids): bool
    {
        $this->designerGlobalCalculationTypeApi->delete($ids);

        return true;
    }
}
