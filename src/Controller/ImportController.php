<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConnectorImportArguments;
use Symfony\Component\HttpFoundation\Response;

final class ImportController
{
    /**
     * @var ImportApi
     */
    private $importApi;

    /**
     * ImportController constructor.
     *
     * @param ImportApi $importApi
     */
    public function __construct(
        ImportApi $importApi
    ) {
        $this->importApi = $importApi;
    }

    /**
     * @param ConnectorImportArguments $connectorImportArguments
     *
     * @return Response
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    public function invokeAction(ConnectorImportArguments $connectorImportArguments)
    {
        $responseCode = Response::HTTP_BAD_REQUEST;

        try {
            $importArray = $this->importApi->prepareImportArray($connectorImportArguments->importBody);
            $isImported = $this->importApi->executeImport(
                $connectorImportArguments->importOptions,
                $importArray
            );
        } catch (\InvalidArgumentException $exception) {
            $isImported = false;
        }

        if ($isImported) {
            $responseCode = Response::HTTP_OK;
        }

        return new Response(null, $responseCode);
    }
}
