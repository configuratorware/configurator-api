<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\CustomCss\CustomCssApi;
use Symfony\Component\HttpFoundation\Response;

final class CustomCssController
{
    /**
     * @var CustomCssApi
     */
    private $customCssApi;

    /**
     * @param CustomCssApi $customCssApi
     */
    public function __construct(CustomCssApi $customCssApi)
    {
        $this->customCssApi = $customCssApi;
    }

    /**
     * @param string|null $clientIdentifier
     *
     * @return Response
     */
    public function loadByClientAction(?string $clientIdentifier = null): Response
    {
        $cssContents = $this->customCssApi->loadByClient($clientIdentifier);

        $headers = ['Content-Type' => 'text/css'];

        return new Response($cssContents, 200, $headers);
    }
}
