<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\OptionFilterArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadOptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadOptionThumbnail;
use Symfony\Component\HttpFoundation\Response;

final class OptionsController
{
    /**
     * @var OptionApi
     */
    private $optionApi;

    /**
     * @var FrontendOptionApi
     */
    private $frontendOptionApi;

    /**
     * OptionsController constructor.
     *
     * @param OptionApi $optionApi
     * @param FrontendOptionApi $frontendOptionApi
     */
    public function __construct(OptionApi $optionApi, FrontendOptionApi $frontendOptionApi)
    {
        $this->optionApi = $optionApi;
        $this->frontendOptionApi = $frontendOptionApi;
    }

    /**
     * get a list of options for an option classification at an item
     * filtered by the given filter attributes.
     *
     * execute checks on the given configuration for each found option
     *
     * @param Configuration $configuration
     * @param string $itemIdentifier
     * @param string $optionClassificationIdentifier
     * @param OptionFilterArguments $optionFilters
     *
     * @return array
     */
    public function frontendListAction(
        Configuration $configuration,
        $itemIdentifier,
        $optionClassificationIdentifier,
        OptionFilterArguments $optionFilters
    ) {
        $options = $this->frontendOptionApi->getList($configuration, $itemIdentifier, $optionClassificationIdentifier, $optionFilters);

        return $options;
    }

    public function frontendMatchingOptionsAction(Configuration $configuration, $optionClassificationIdentifier)
    {
        $matchingOptions = $this->frontendOptionApi->getMatchingOptions($configuration, $optionClassificationIdentifier);

        return $matchingOptions;
    }

    /**
     * @param Configuration $configuration
     * @param $optionIdentifier
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option
     */
    public function frontendDetailAction(Configuration $configuration, $optionIdentifier)
    {
        $option = $this->frontendOptionApi->getDetail($configuration, $optionIdentifier);

        return $option;
    }

    /**
     * returns a list of option objects.
     *
     * @param ListRequestNoDefaultArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestNoDefaultArguments $arguments): PaginationResult
    {
        $result = $this->optionApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one option object by its id.
     *
     * @param int $id
     *
     * @return Option
     */
    public function detailAction(int $id)
    {
        $structure = $this->optionApi->getOne($id);

        return $structure;
    }

    /**
     * save an option.
     *
     * @param Option $option
     *
     * @return Option
     */
    public function saveAction(Option $option)
    {
        $option = $this->optionApi->save($option);

        return $option;
    }

    /**
     * Deletes options by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->optionApi->delete($id);

        return true;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     *
     * @return bool
     */
    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments)
    {
        $this->optionApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }

    /**
     * @param UploadOptionThumbnail $uploadOptionThumbnail
     *
     * @return Response
     */
    public function uploadThumbnail(UploadOptionThumbnail $uploadOptionThumbnail): Response
    {
        try {
            $this->optionApi->uploadThumbnail($uploadOptionThumbnail);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function deleteThumbnail(int $id): Response
    {
        try {
            $this->optionApi->deleteThumbnail($id);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param UploadOptionDetailImage $uploadOptionDetailImage
     *
     * @return Response
     */
    public function uploadDetailImage(UploadOptionDetailImage $uploadOptionDetailImage): Response
    {
        try {
            $this->optionApi->uploadDetailImage($uploadOptionDetailImage);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function deleteDetailImage(int $id): Response
    {
        try {
            $this->optionApi->deleteDetailImage($id);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @return PaginationResult
     */
    public function inputValidationTypeListAction(): PaginationResult
    {
        return $this->optionApi->getOptionInputValidationTypes();
    }
}
