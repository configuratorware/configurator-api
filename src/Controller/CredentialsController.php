<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Credential\CredentialApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class CredentialsController
{
    /**
     * @var CredentialApi
     */
    private $credentialApi;

    /**
     * CredentialsController constructor.
     *
     * @param CredentialApi $credentialApi
     */
    public function __construct(CredentialApi $credentialApi)
    {
        $this->credentialApi = $credentialApi;
    }

    /**
     * returns a list of credential objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->credentialApi->getListResult($arguments);

        return $result;
    }
}
