<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Export\ExportService;
use Redhotmagma\ConfiguratorApiBundle\Structure\ExportRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ExportController extends AbstractController
{
    private ExportService $exportService;

    public function __construct(ExportService $exportService)
    {
        $this->exportService = $exportService;
    }

    public function export(ExportRequest $exportRequest): Response
    {
        return $this->exportService->export($exportRequest->getItemIdentifiers(), $exportRequest->isCombine());
    }
}
