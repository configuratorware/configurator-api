<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Tag\TagApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\Tag;

final class TagsController
{
    /**
     * @var TagApi
     */
    private $tagApi;

    /**
     * TagsController constructor.
     *
     * @param TagApi $tagApi
     */
    public function __construct(TagApi $tagApi)
    {
        $this->tagApi = $tagApi;
    }

    /**
     * returns a list of tag objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->tagApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one tag object by its id.
     *
     * @param int $id
     *
     * @return Tag
     */
    public function detailAction(int $id)
    {
        $structure = $this->tagApi->getOne($id);

        return $structure;
    }

    /**
     * save an tag.
     *
     * @param Tag $tag
     *
     * @return Tag
     */
    public function saveAction(Tag $tag)
    {
        $tag = $this->tagApi->save($tag);

        return $tag;
    }

    /**
     * Deletes tags by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->tagApi->delete($id);

        return true;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     *
     * @return bool
     */
    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments)
    {
        $this->tagApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }
}
