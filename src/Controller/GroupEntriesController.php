<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\GroupEntry\GroupEntryApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

final class GroupEntriesController
{
    /**
     * @var GroupEntryApi
     */
    private $groupEntryApi;

    /**
     * GroupEntriesController constructor.
     *
     * @param GroupEntryApi $groupEntryEntryApi
     */
    public function __construct(GroupEntryApi $groupEntryEntryApi)
    {
        $this->groupEntryApi = $groupEntryEntryApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->groupEntryApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return GroupEntry
     */
    public function detailAction(int $id)
    {
        $structure = $this->groupEntryApi->getOne($id);

        return $structure;
    }

    /**
     * @param GroupEntry $groupEntry
     *
     * @return GroupEntry
     */
    public function saveAction(GroupEntry $groupEntry)
    {
        $groupEntry = $this->groupEntryApi->save($groupEntry);

        return $groupEntry;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->groupEntryApi->delete($id);

        return true;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     *
     * @return bool
     */
    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments)
    {
        $this->groupEntryApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }
}
