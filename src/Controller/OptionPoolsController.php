<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\OptionPool\OptionPoolApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class OptionPoolsController
{
    /**
     * @var OptionPoolApi
     */
    private $optionPoolApi;

    /**
     * @param OptionPoolApi $optionPoolApi
     */
    public function __construct(OptionPoolApi $optionPoolApi)
    {
        $this->optionPoolApi = $optionPoolApi;
    }

    /**
     * returns a list of OptionPool objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->optionPoolApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one OptionPool object by its id.
     *
     * @param int $id
     *
     * @return OptionPool
     */
    public function detailAction(int $id)
    {
        $structure = $this->optionPoolApi->getOne($id);

        return $structure;
    }

    /**
     * save an option pool.
     *
     * @param OptionPool $optionPool
     *
     * @return OptionPool
     */
    public function saveAction(OptionPool $optionPool)
    {
        $optionPool = $this->optionPoolApi->save($optionPool);

        return $optionPool;
    }

    /**
     * Deletes option pools by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->optionPoolApi->delete($id);

        return true;
    }
}
