<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern\ConstructionPatternApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConstructionPattern;

class ConstructionPatternsController
{
    /**
     * @var ConstructionPatternApi
     */
    private $constructionPatternApi;

    /**
     * @param ConstructionPatternApi $constructionPatternApi
     */
    public function __construct(ConstructionPatternApi $constructionPatternApi)
    {
        $this->constructionPatternApi = $constructionPatternApi;
    }

    /**
     * @param int $id
     *
     * @return ConstructionPattern
     */
    public function detailAction(int $id): ConstructionPattern
    {
        return $this->constructionPatternApi->getOne($id);
    }

    /**
     * @param ConstructionPattern $constructionPattern
     *
     * @return ConstructionPattern
     */
    public function saveAction(Constructionpattern $constructionPattern): ConstructionPattern
    {
        return $this->constructionPatternApi->save($constructionPattern);
    }
}
