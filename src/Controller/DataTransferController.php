<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\DataTransfer\DesignDataTransferApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignDataTransfer as DesignDataTransferStructure;
use Symfony\Component\HttpFoundation\JsonResponse;

final class DataTransferController
{
    /**
     * @var DesignDataTransferApi
     */
    private $designDataTransferApi;

    /**
     * DataTransferController constructor.
     *
     * @param DesignDataTransferApi $designDataTransferApi
     */
    public function __construct(DesignDataTransferApi $designDataTransferApi)
    {
        $this->designDataTransferApi = $designDataTransferApi;
    }

    /**
     * transfers design data from one item to another.
     *
     * @param DesignDataTransferStructure $designDataTransfer
     *
     * @return JsonResponse
     *
     * @throws \Exception
     */
    public function designDataAction(DesignDataTransferStructure $designDataTransfer)
    {
        $this->designDataTransferApi->transfer($designDataTransfer);

        return new JsonResponse(true);
    }
}
