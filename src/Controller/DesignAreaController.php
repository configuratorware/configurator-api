<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\DesignAreaApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DesignAreaMaskArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

final class DesignAreaController
{
    /**
     * @var DesignAreaApi
     */
    private $designAreaApi;

    /**
     * @param DesignAreaApi $designAreaApi
     */
    public function __construct(DesignAreaApi $designAreaApi)
    {
        $this->designAreaApi = $designAreaApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->designAreaApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return DesignAreaStructure
     */
    public function detailAction(int $id): DesignAreaStructure
    {
        $structure = $this->designAreaApi->getOne($id);

        return $structure;
    }

    /**
     * @param DesignAreaStructure $structure
     *
     * @return DesignAreaStructure
     */
    public function saveAction(DesignAreaStructure $structure): DesignAreaStructure
    {
        $structure = $this->designAreaApi->save($structure);

        return $structure;
    }

    /**
     * Deletes Design Areas by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id): bool
    {
        $this->designAreaApi->delete($id);

        return true;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     *
     * @return bool
     */
    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments)
    {
        $this->designAreaApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }

    /**
     * @param DesignAreaMaskArguments $arguments
     *
     * @return bool
     */
    public function uploadMaskAction(DesignAreaMaskArguments $arguments): bool
    {
        return $this->designAreaApi->uploadMask($arguments);
    }

    /**
     * @param DesignAreaMaskArguments $arguments
     *
     * @return bool
     */
    public function uploadDesignProductionMethodMaskAction(DesignAreaMaskArguments $arguments): bool
    {
        return $this->designAreaApi->uploadDesignProductionMethodMask($arguments);
    }
}
