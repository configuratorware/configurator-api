<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadDesignViewDetailImageForGroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadDesignViewDetailImageForItem;
use Symfony\Component\HttpFoundation\Response;

final class DesignViewController
{
    /**
     * @var DesignViewApi
     */
    private $designViewApi;

    /**
     * @param DesignViewApi $designViewApi
     */
    public function __construct(DesignViewApi $designViewApi)
    {
        $this->designViewApi = $designViewApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->designViewApi->getListResult($arguments);

        return $result;
    }

    /**
     * @param int $id
     *
     * @return DesignViewStructure
     */
    public function detailAction(int $id): DesignViewStructure
    {
        $structure = $this->designViewApi->getOne($id);

        return $structure;
    }

    /**
     * @param DesignViewStructure $structure
     *
     * @return DesignViewStructure
     */
    public function saveAction(DesignViewStructure $structure): DesignViewStructure
    {
        $structure = $this->designViewApi->save($structure);

        return $structure;
    }

    /**
     * Deletes Design Views by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id): bool
    {
        $this->designViewApi->delete($id);

        return true;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     *
     * @return bool
     */
    public function saveSequenceNumbersAction(SequenceNumberArguments $sequenceNumberArguments): bool
    {
        $this->designViewApi->saveSequenceNumbers($sequenceNumberArguments);

        return true;
    }

    /**
     * @param UploadDesignViewDetailImageForItem $uploadDetailImage
     *
     * @return Response
     */
    public function uploadDetailImageForItem(UploadDesignViewDetailImageForItem $uploadDetailImage): Response
    {
        try {
            $this->designViewApi->uploadDetailImageForItem($uploadDetailImage);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @param int $itemId
     *
     * @return Response
     */
    public function deleteDetailImageForItem(int $id, int $itemId): Response
    {
        try {
            $this->designViewApi->deleteDetailImageForItem($id, $itemId);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param UploadDesignViewDetailImageForGroupEntry $uploadDetailImage
     *
     * @return Response
     */
    public function uploadDetailImageForGroupEntry(UploadDesignViewDetailImageForGroupEntry $uploadDetailImage): Response
    {
        try {
            $this->designViewApi->uploadDetailImageForGroupEntry($uploadDetailImage);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param int $id
     * @param int $itemGroupEntryId
     *
     * @return Response
     */
    public function deleteDetailImageForGroupEntry(int $id, int $itemGroupEntryId): Response
    {
        try {
            $this->designViewApi->deleteDetailImageForGroupEntry($id, $itemGroupEntryId);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }
}
