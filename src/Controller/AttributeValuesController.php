<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue\AttributeValueApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class AttributeValuesController
{
    /**
     * @var AttributeValueApi
     */
    private $attributeValueApi;

    /**
     * AttributeValuesController constructor.
     *
     * @param AttributeValueApi $attributeValueApi
     */
    public function __construct(AttributeValueApi $attributeValueApi)
    {
        $this->attributeValueApi = $attributeValueApi;
    }

    /**
     * returns a list of attribute value objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->attributeValueApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one attribute value object by its id.
     *
     * @param int $id
     *
     * @return Attributevalue
     */
    public function detailAction(int $id)
    {
        $structure = $this->attributeValueApi->getOne($id);

        return $structure;
    }

    /**
     * save an attribute value.
     *
     * @param Attributevalue $attribute
     *
     * @return Attributevalue
     */
    public function saveAction(Attributevalue $attribute)
    {
        $attribute = $this->attributeValueApi->save($attribute);

        return $attribute;
    }

    /**
     * Deletes attribute values by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->attributeValueApi->delete($id);

        return true;
    }
}
