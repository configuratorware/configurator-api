<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationApi;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\ConfigurationDocumentApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationPrintFileSaveData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSaveData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSwitchOptionData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\PrintFile;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConfigurationLoadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

final class ConfigurationsController
{
    /**
     * @var ConfigurationApi
     */
    private $configurationApi;

    /**
     * @var ConfigurationDocumentApi
     */
    private $configurationDocumentApi;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @param ConfigurationApi $configurationApi
     * @param ConfigurationDocumentApi $configurationDocumentApi
     * @param ConfigurationRepository $configurationRepository
     */
    public function __construct(ConfigurationApi $configurationApi, ConfigurationDocumentApi $configurationDocumentApi, ConfigurationRepository $configurationRepository)
    {
        $this->configurationApi = $configurationApi;
        $this->configurationDocumentApi = $configurationDocumentApi;
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * returns a list of configuration objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        return $this->configurationApi->getListResult($arguments);
    }

    /**
     * returns one configuration object by its id.
     *
     * @param int $id
     *
     * @return Configuration
     */
    public function detailAction(int $id): Configuration
    {
        $configuration = $this->configurationRepository->find($id);
        if (null === $configuration) {
            throw NotFoundException::entityWithId(Configuration::class, $id);
        }

        return $this->configurationApi->getOne($id);
    }

    /**
     * Deletes configurations by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->configurationApi->delete($id);

        return true;
    }

    public function saveAction(ConfigurationSaveData $configurationSaveData, ControlParameters $controlParameters): FrontendConfigurationStructure
    {
        return $this->configurationApi->frontendSave($configurationSaveData, $controlParameters);
    }

    public function frontendSaveAction(ConfigurationSaveData $configurationSaveData, ControlParameters $controlParameters): FrontendConfigurationStructure
    {
        $configurationType = $configurationSaveData->configurationType ?? ConfigurationtypeRepository::USER;
        $validationException = new ValidationException([['property' => 'configurationType', 'invalidvalue' => ConfigurationtypeRepository::DEFAULT_OPTIONS, 'message' => 'default_options']]);

        if (ConfigurationtypeRepository::DEFAULT_OPTIONS === $configurationType) {
            throw $validationException;
        }

        $code = $configurationSaveData->configuration->code;
        if (null !== $code) {
            $configuration = $this->configurationRepository->findOneByCode($code);
            if (ConfigurationtypeRepository::DEFAULT_OPTIONS === $configuration->getConfigurationtype()->getIdentifier()) {
                throw $validationException;
            }
        }

        return $this->configurationApi->frontendSave($configurationSaveData, $controlParameters);
    }

    /**
     * @param string $configurationCode
     *
     * @return PrintFile[]
     */
    public function printFileListAction(string $configurationCode): array
    {
        return $this->configurationApi->getPrintFileListResults($configurationCode);
    }

    /**
     * @param ConfigurationPrintFileSaveData $configurationPrintFileSaveData
     *
     * @return string[]
     */
    public function uploadPrintDataAction(ConfigurationPrintFileSaveData $configurationPrintFileSaveData): array
    {
        $result = $this->configurationApi->uploadPrintData($configurationPrintFileSaveData);

        return $result;
    }

    public function loadByItemIdentifierAction(string $identifier, ControlParameters $controlParameters, ConfigurationLoadArguments $configurationLoadArguments): ?FrontendConfigurationStructure
    {
        $result = $this->configurationApi->loadByItemIdentifier($identifier, $controlParameters, $configurationLoadArguments);

        return $result;
    }

    public function loadByConfigurationCodeAction(string $code, ControlParameters $controlParameters): ?FrontendConfigurationStructure
    {
        $result = $this->configurationApi->loadByConfigurationCode($code, $controlParameters);

        return $result;
    }

    public function switchOptionAction(ConfigurationSwitchOptionData $configurationSwitchOptionData): ?FrontendConfigurationStructure
    {
        return $this->configurationApi->switchOption($configurationSwitchOptionData);
    }

    public function validateAction(FrontendConfigurationStructure $configuration): array
    {
        return $this->configurationApi->validateConfiguration($configuration);
    }

    /**
     * @param string $documentType
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     *
     * @return FileResult
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function pdfAction(string $documentType, string $configurationCode, ControlParameters $controlParameters): FileResult
    {
        return $this->configurationDocumentApi->generatePdf($documentType, $configurationCode, $controlParameters);
    }

    /**
     * @param string $documentType
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     *
     * @return Response
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function htmlAction(string $documentType, string $configurationCode, ControlParameters $controlParameters): Response
    {
        $html = $this->configurationDocumentApi->generateHtml($documentType, $configurationCode, $controlParameters);

        return new Response($html);
    }

    public function zipAction(string $configurationCode, ControlParameters $controlParameters): FileResult
    {
        return $this->configurationApi->generateZip($configurationCode, $controlParameters);
    }
}
