<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus\ItemStatusApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class ItemStatusController
{
    /**
     * @var ItemStatusApi
     */
    private $itemStatusApi;

    /**
     * @param ItemStatusApi $itemStatusApi
     */
    public function __construct(ItemStatusApi $itemStatusApi)
    {
        $this->itemStatusApi = $itemStatusApi;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->itemStatusApi->getListResult($arguments);

        return $result;
    }
}
