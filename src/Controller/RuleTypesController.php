<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\RuleType\RuleTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

final class RuleTypesController
{
    /**
     * @var RuleTypeApi
     */
    private $ruleTypeApi;

    /**
     * RuleTypesController constructor.
     *
     * @param RuleTypeApi $ruleTypeApi
     */
    public function __construct(RuleTypeApi $ruleTypeApi)
    {
        $this->ruleTypeApi = $ruleTypeApi;
    }

    /**
     * returns a list of ruleType objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->ruleTypeApi->getListResult($arguments);

        return $result;
    }
}
