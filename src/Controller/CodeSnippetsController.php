<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\CodeSnippet\CodeSnippetApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Component\HttpFoundation\JsonResponse;

final class CodeSnippetsController
{
    /**
     * @var CodeSnippetApi
     */
    private $codeSnippetApi;

    /**
     * CodeSnippetsController constructor.
     *
     * @param CodeSnippetApi $codeSnippetApi
     */
    public function __construct(CodeSnippetApi $codeSnippetApi)
    {
        $this->codeSnippetApi = $codeSnippetApi;
    }

    /**
     * returns a list of codeSnippet objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        $result = $this->codeSnippetApi->getListResult($arguments);

        return $result;
    }

    /**
     * returns one codeSnippet object by its id.
     *
     * @param int $id
     *
     * @return CodeSnippet
     */
    public function detailAction(int $id)
    {
        $structure = $this->codeSnippetApi->getOne($id);

        return $structure;
    }

    /**
     * save an codeSnippet.
     *
     * @param CodeSnippet $codeSnippet
     *
     * @return CodeSnippet
     */
    public function saveAction(CodeSnippet $codeSnippet)
    {
        $codeSnippet = $this->codeSnippetApi->save($codeSnippet);

        return $codeSnippet;
    }

    /**
     * Deletes codeSnippets by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id)
    {
        $this->codeSnippetApi->delete($id);

        return true;
    }

    /**
     * get code snippets for the current language channel combination.
     *
     * @return array
     */
    public function getCurrentAction()
    {
        $codeSnippets = $this->codeSnippetApi->findCurrent();

        // return a JsonResponse to prevent 404 if the array is empty
        return new JsonResponse($codeSnippets);
    }
}
