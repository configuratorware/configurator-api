<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint\AssemblyPointApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SampleOptionListRequestArguments;

final class AssemblyPointsController
{
    /**
     * @var AssemblyPointApi
     */
    private $assemblyPointApi;

    /**
     * AssemblyPointsController constructor.
     *
     * @param AssemblyPointApi $assemblyPointApi
     */
    public function __construct(AssemblyPointApi $assemblyPointApi)
    {
        $this->assemblyPointApi = $assemblyPointApi;
    }

    /**
     * @param Option $option
     *
     * @return Option
     */
    public function saveByOptionAction(Option $option)
    {
        $option = $this->assemblyPointApi->save($option);

        return $option;
    }

    /**
     * @param int $optionId
     *
     * @return Option
     */
    public function getByOptionAction($optionId)
    {
        $options = $this->assemblyPointApi->getByOption($optionId);

        return $options;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listOptionsAction(ListRequestArguments $arguments): PaginationResult
    {
        $response = $this->assemblyPointApi->getOptionsList($arguments);

        return $response;
    }

    /**
     * @param SampleOptionListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listSampleOptionsAction(SampleOptionListRequestArguments $arguments): PaginationResult
    {
        $response = $this->assemblyPointApi->getSampleOptions($arguments);

        return $response;
    }
}
