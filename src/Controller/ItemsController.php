<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemDuplicate;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DeleteLayerImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ItemClassificationFilter;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadLayerImage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

final class ItemsController
{
    private ItemApi $itemApi;

    /**
     * ItemsController constructor.
     *
     * @param ItemApi $itemApi
     */
    public function __construct(ItemApi $itemApi)
    {
        $this->itemApi = $itemApi;
    }

    /**
     * returns a list of item objects.
     *
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        return $this->itemApi->getListResult($arguments);
    }

    /**
     * @param string $configurationMode
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listByConfigurationModeAction(string $configurationMode, ListRequestArguments $arguments): PaginationResult
    {
        return $this->itemApi->getListResultBy($configurationMode, $arguments);
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listInspirationAction(ListRequestArguments $arguments): PaginationResult
    {
        return $this->itemApi->getInspirationListResult($arguments);
    }

    /**
     * returns one item object by its id.
     *
     * @param int $id
     *
     * @return Item
     */
    public function detailAction(int $id): Item
    {
        return $this->itemApi->getOne($id);
    }

    /**
     * save an item.
     *
     * @param Item $item
     *
     * @return Item
     */
    public function saveAction(Item $item): Item
    {
        return $this->itemApi->save($item);
    }

    /**
     * Deletes items by id(s)
     * multiple ids can be used comma separated.
     *
     * @param $id
     *
     * @return bool
     */
    public function deleteAction($id): bool
    {
        $this->itemApi->delete($id);

        return true;
    }

    /**
     * Duplicate an item.
     */
    public function duplicateAction(int $id, ItemDuplicate $itemDuplicate): JsonResponse
    {
        $duplicatedItem = $this->itemApi->duplicate($id, $itemDuplicate);

        return new JsonResponse($duplicatedItem);
    }

    /**
     * @param $parentIdentifier
     *
     * @return array
     */
    public function getConfigurableItemsByParentAction($parentIdentifier = ''): array
    {
        return $this->itemApi->getConfigurableItemsByParent($parentIdentifier);
    }

    /**
     * @param ItemStatus $itemStatus
     * @param Item       ...$items
     *
     * @return Response
     */
    public function updateStatus(ItemStatus $itemStatus, Item ...$items): Response
    {
        foreach ($items as $item) {
            $this->itemApi->updateItemStatus($item, $itemStatus);
        }

        return new Response(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param UploadLayerImage $uploadLayerImage
     *
     * @return Response
     */
    public function uploadLayerImage(UploadLayerImage $uploadLayerImage): Response
    {
        try {
            $this->itemApi->uploadLayerImage($uploadLayerImage);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param DeleteLayerImage $deleteLayerImage
     *
     * @return Response
     */
    public function deleteLayerImage(DeleteLayerImage $deleteLayerImage): Response
    {
        try {
            $this->itemApi->deleteLayerImage($deleteLayerImage);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * Returns a list of item objects.
     *
     * @return JsonResponse
     */
    public function frontendListAction(
        ItemClassificationFilter $itemClassificationFilter,
        ListRequestNoDefaultArguments $arguments
    ): JsonResponse {
        return new JsonResponse($this->itemApi->getFrontendList($itemClassificationFilter, $arguments));
    }
}
