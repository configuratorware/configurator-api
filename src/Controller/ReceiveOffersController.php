<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\ReceiveOfferApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\HttpFoundation\JsonResponse;

final class ReceiveOffersController
{
    /**
     * @var ReceiveOfferApi
     */
    private $receiveOfferApi;

    /**
     * ReceiveOffersController constructor.
     *
     * @param ReceiveOfferApi $receiveOfferApi
     */
    public function __construct(ReceiveOfferApi $receiveOfferApi)
    {
        $this->receiveOfferApi = $receiveOfferApi;
    }

    /**
     * @param ReceiveOfferRequestStructure $structure
     * @param ControlParameters $controlParameters
     *
     * @return JsonResponse
     */
    public function requestAction(
        ReceiveOfferRequestStructure $structure,
        ControlParameters $controlParameters
    ): JsonResponse {
        $result = $this->receiveOfferApi->request($structure, $controlParameters);

        $responseCode = JsonResponse::HTTP_BAD_REQUEST;
        if (true === $result) {
            $responseCode = JsonResponse::HTTP_OK;
        }

        return new JsonResponse($result, $responseCode);
    }
}
