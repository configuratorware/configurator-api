<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\MediaArguments;

final class MediaController
{
    /**
     * @var MediaApi
     */
    private $mediaApi;

    /**
     * MediaController constructor.
     *
     * @param MediaApi $mediaApi
     */
    public function __construct(MediaApi $mediaApi)
    {
        $this->mediaApi = $mediaApi;
    }

    /**
     * @param MediaArguments $mediaArguments
     *
     * @return mixed
     */
    public function getAction(MediaArguments $mediaArguments)
    {
        $response = $this->mediaApi->getMedia($mediaArguments);

        return $response;
    }
}
