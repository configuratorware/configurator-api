<?php

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Service\UserImage\DesignerUserImageApi;
use Redhotmagma\ConfiguratorApiBundle\Service\UserImage\UserImageApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequest as DesignerUserImageRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageResponse;
use Redhotmagma\ConfiguratorApiBundle\Validator\UserImageValidator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

final class UserImagesController
{
    /**
     * @var DesignerUserImageApi
     */
    private $designerUserImageApi;

    /**
     * @var UserImageApi
     */
    private $userImageApi;

    /**
     * @var UserImageValidator
     */
    private $userImageValidator;

    /**
     * UserImagesController constructor.
     *
     * @param DesignerUserImageApi $designerUserImageApi
     * @param UserImageApi $userImageApi
     * @param UserImageValidator $userImageValidator
     */
    public function __construct(
        DesignerUserImageApi $designerUserImageApi,
        UserImageApi $userImageApi,
        UserImageValidator $userImageValidator
    ) {
        $this->designerUserImageApi = $designerUserImageApi;
        $this->userImageApi = $userImageApi;
        $this->userImageValidator = $userImageValidator;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function uploadAction(Request $request)
    {
        /** @var UploadedFile $imageFormData */
        $imageFormData = $request->files->get('userImage');

        $this->userImageValidator->validate($imageFormData);

        $response = $this->userImageApi->upload($imageFormData);

        return $response;
    }

    /**
     * @param DesignerUserImageRequestStructure $designerUserImageRequestStructure
     *
     * @return DesignerUserImageResponse
     *
     * @throws \Exception
     */
    public function designerEditAction(
        DesignerUserImageRequestStructure $designerUserImageRequestStructure
    ): DesignerUserImageResponse {
        $response = $this->designerUserImageApi->edit(
            $designerUserImageRequestStructure
        );

        return $response;
    }
}
