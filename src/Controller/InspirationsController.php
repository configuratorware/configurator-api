<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Controller;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\Inspiration\InspirationApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Inspiration as FrontendInspirationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Inspiration as InspirationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadInspirationThumbnail;
use Symfony\Component\HttpFoundation\Response;

final class InspirationsController
{
    /**
     * @var InspirationApi
     */
    private $inspirationApi;
    /**
     * @var MediaHelper
     */
    private $mediaHelper;

    /**
     * @param InspirationApi $inspirationApi
     * @param MediaHelper $mediaHelper
     */
    public function __construct(InspirationApi $inspirationApi, MediaHelper $mediaHelper)
    {
        $this->inspirationApi = $inspirationApi;
        $this->mediaHelper = $mediaHelper;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return FrontendInspirationStructure[]
     */
    public function frontendListAction(string $itemIdentifier): array
    {
        $languageIso = '';
        if (defined('C_LANGUAGE_ISO')) {
            $languageIso = C_LANGUAGE_ISO;
        }

        $inspirations = $this->inspirationApi->frontendList($itemIdentifier, $languageIso);

        $host = $this->mediaHelper->getImageBaseUrl();
        foreach ($inspirations as $inspiration) {
            $thumbnail = $inspiration->thumbnail;
            if (null !== $thumbnail) {
                $inspiration->thumbnail = $host . $thumbnail;
            }

            $previewImage = $inspiration->previewImage;
            if (null !== $previewImage) {
                $inspiration->previewImage = $host . $previewImage;
            }
        }

        return $inspirations;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function listAction(ListRequestArguments $arguments): PaginationResult
    {
        return $this->inspirationApi->getListResult($arguments);
    }

    /**
     * @param int $id
     *
     * @return InspirationStructure
     */
    public function detailAction(int $id): InspirationStructure
    {
        return $this->inspirationApi->getOne($id);
    }

    /**
     * @param InspirationStructure $inspiration
     *
     * @return InspirationStructure
     */
    public function saveAction(InspirationStructure $inspiration): InspirationStructure
    {
        return $this->inspirationApi->save($inspiration);
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function deleteAction(string $id): bool
    {
        $this->inspirationApi->delete($id);

        return true;
    }

    /**
     * @param UploadInspirationThumbnail $uploadInspirationThumbnail
     *
     * @return Response
     */
    public function uploadThumbnail(UploadInspirationThumbnail $uploadInspirationThumbnail): Response
    {
        try {
            $this->inspirationApi->uploadThumbnail($uploadInspirationThumbnail);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }

    /**
     * @param int $id
     *
     * @return Response
     */
    public function deleteThumbnail(int $id): Response
    {
        try {
            $this->inspirationApi->deleteThumbnail($id);
        } catch (FileException $e) {
            return new Response(null, Response::HTTP_BAD_REQUEST);
        }

        return new Response(null, Response::HTTP_OK);
    }
}
