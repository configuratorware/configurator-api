<?php

namespace Redhotmagma\ConfiguratorApiBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public const DEFAULT_SECURITY = 'default_security';

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('redhotmagma_configurator_api');

        $treeBuilder->getRootNode()
            ->children()
                ->booleanNode(self::DEFAULT_SECURITY)
                ->defaultValue(true)
            ->end();

        return $treeBuilder;
    }
}
