<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\DependencyInjection\Compiler;

use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputValidation;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class OptionTextValidatorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if (!$container->has(OptionInputValidation::class)) {
            return;
        }

        $definition = $container->findDefinition(OptionInputValidation::class);
        $taggedServices = $container->findTaggedServiceIds('app.option_input_validation');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addValidator', [new Reference($id)]);
        }
    }
}
