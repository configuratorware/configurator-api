<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

class RedhotmagmaConfiguratorApiExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container): void
    {
        $processedConfigs = $this->processConfiguration(new Configuration(), $configs);

        $this->loadSecurity($processedConfigs, $container);
        $this->loadConfig($container);
    }

    public function prepend(ContainerBuilder $container): void
    {
        $container->setParameter('redhotmagma_configurator_api.project_dir', realpath(__DIR__ . '/../../.'));

        try {
            $configs = Yaml::parseFile(__DIR__ . '/../Resources/config/override.yml');
        } catch (ParseException $exception) {
            throw new InvalidArgumentException(sprintf('The file override.yml of this Bundle does not contain valid YAML: %s', $exception->getMessage()), 0, $exception);
        }

        foreach ($configs as $configKey => $config) {
            $container->prependExtensionConfig($configKey, $config);
        }
    }

    /**
     * @param array $processedConfigs
     * @param ContainerBuilder $container
     */
    private function loadSecurity(array $processedConfigs, ContainerBuilder $container): void
    {
        if ($processedConfigs[Configuration::DEFAULT_SECURITY]) {
            try {
                $configs = Yaml::parseFile(__DIR__ . '/../Resources/config/security.yml');
            } catch (ParseException $exception) {
                throw new InvalidArgumentException(sprintf('The file security.yml of this Bundle does not contain valid YAML: %s', $exception->getMessage()), 0, $exception);
            }

            foreach ($configs as $configKey => $config) {
                $container->prependExtensionConfig($configKey, $config);
            }
        }
    }

    /**
     * @param ContainerBuilder $container
     */
    private function loadConfig(ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('controller.yml');
        $loader->load('service.yml');
        $loader->load('command.yml');
        $loader->load('config.yml');
        $loader->load('file_repository.yaml');
        $loader->load('import.yml');
        $loader->load('listener.yml');
        $loader->load('resolver.yml');
        $loader->load('repository.yml');
        $loader->load('twig_extension.yml');
        $loader->load('validator.yml');
        $loader->load('vector.yml');
        $loader->load('exporter.yml');
    }
}
