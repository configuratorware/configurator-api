<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Logger;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\SQLParserUtils;

/**
 * Log SQL queries (statements) to output.
 *
 * @internal
 */
class SqlConsoleLogger implements \Doctrine\DBAL\Logging\SQLLogger
{
    /**
     * @var Connection
     */
    protected $conn;

    public static $counter = 0;

    public function __construct(Connection $connection)
    {
        $this->conn = $connection;
    }

    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.StaticAccess)
     */
    public function startQuery($sql, array $params = null, array $types = null)
    {
        $query = $sql;
        if ($params) {
            $expanded = SQLParserUtils::expandListParameters($sql, $params, $types);
            $query = vsprintf(str_replace('?', '%s', $expanded[0]), $this->quoteParams($expanded[1], $expanded[2]));
        }
        ++self::$counter;
        echo trim(preg_replace('/\s+/', ' ', $query), ' ') . ';' . chr(10);
    }

    protected function quoteParams($params, $types)
    {
        $quotedParams = [];
        foreach ($params as $key => $value) {
            $quotedParams[] = $this->conn->quote($value, $types[$key]);
        }

        return $quotedParams;
    }

    /**
     * {@inheritdoc}
     */
    public function stopQuery()
    {
        // implementing interface requirements
    }

    public function printSummary()
    {
        echo self::$counter . ' queries executed';
    }
}
