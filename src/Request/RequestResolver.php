<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Request;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Exception\ClassDoesNotExist;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 */
final class RequestResolver
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(StructureFromDataConverter $structureFromDataConverter)
    {
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * @param Request $request
     * @param string $structureName
     *
     * @return object
     */
    public function resolveToStructure(Request $request, string $structureName): object
    {
        if (!class_exists($structureName)) {
            throw ClassDoesNotExist::withFQN($structureName);
        }

        $content = (string)$request->getContent();
        $decodedContent = json_decode($content, false);

        if (empty($decodedContent)) {
            throw InvalidRequest::withContent($content);
        }

        return $this->structureFromDataConverter->convert($decodedContent, $structureName);
    }

    /**
     * @param Request $request
     * @param string $key
     *
     * @return string
     */
    public function resolveAttributeValue(Request $request, string $key): string
    {
        $value = $request->attributes->get($key);

        if (null === $value) {
            throw InvalidRequest::urlParamExpected($key);
        }

        return $value;
    }

    /**
     * @param Request $request
     * @param string $key
     *
     * @return UploadedFile
     */
    public function resolveUploadedFile(Request $request, string $key): UploadedFile
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get($key);
        if (null === $uploadedFile) {
            throw InvalidRequest::fileExpected($key);
        }

        return $uploadedFile;
    }
}
