<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Models;

/**
 * @deprecated Use \Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions instead of this
 */
final class ImportOptions extends \Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions
{
}
