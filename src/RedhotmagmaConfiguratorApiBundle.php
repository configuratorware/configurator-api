<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle;

use Redhotmagma\ConfiguratorApiBundle\DependencyInjection\Compiler\OptionTextValidatorPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class RedhotmagmaConfiguratorApiBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new OptionTextValidatorPass());
    }
}
