<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\AttributeValueCollector;
use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\TextCollector;

class ComponentDTO
{
    public string $componentIdentifier;
    public int $hiddenInFrontend = 0;
    /** @var TextDTO[] */
    public array $texts = [];
    public int $sequencenumber = 0;
    /** @var AttributeDTO[] */
    public array $attributes = [];
    public bool $isMultiselect = false;
    /** @var OptionDTO[] */
    public array $options = [];
    public ?ImagesDTO $images = null;
    public int $minamount;
    public int $maxamount;
    public bool $isMandatory;
    /** @var DefaultSelection[] */
    public array $defaultSelections = [];

    public function __construct(
        string $identifier,
        int $sequencenumber,
        bool $isMultiselect,
        int $minamount,
        int $maxamount,
        bool $isMandatory
    ) {
        $this->componentIdentifier = $identifier;
        $this->sequencenumber = $sequencenumber;
        $this->isMultiselect = $isMultiselect;
        $this->minamount = $minamount;
        $this->maxamount = $maxamount;
        $this->isMandatory = $isMandatory;
    }

    /** @param array<string, mixed> $data */
    public static function from(array $data): self
    {
        $componentData = $data['optionclassification'];
        $dto = new self(
            $componentData['identifier'],
            (int)$componentData['sequencenumber'],
            $data['is_multiselect'],
            (int)$data['minamount'],
            (int)$data['maxamount'],
            $data['is_mandatory']
        );

        $dto->texts = TextCollector::collectFrom($componentData['optionClassificationText']);

        $dto->attributes = AttributeValueCollector::collectFrom($componentData['optionClassificationAttribute']);
        $dto->options = array_map(
            static fn (array $data) => OptionDTO::from($data),
            $data['itemOptionclassificationOption']
        );

        return $dto;
    }

    public function updatePricesWith(PriceCollection $priceCollection): void
    {
        foreach ($this->options as $option) {
            $option->deltaPrices = $priceCollection->findDeltaPricesFor($this->componentIdentifier, $option->optionIdentifier);
            $option->prices = $priceCollection->findPrices($this->componentIdentifier, $option->optionIdentifier);
        }
    }
}
