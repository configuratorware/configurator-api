<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class PriceDTO
{
    public int $itemAmountFrom;
    public float $price;
    public float $priceNet;

    public function __construct(
        int $itemAmountFrom,
        float $price,
        float $priceNet
    ) {
        $this->itemAmountFrom = $itemAmountFrom;
        $this->price = $price;
        $this->priceNet = $priceNet;
    }

    /** @param array<string, mixed> $priceData */
    public static function from(array $priceData): self
    {
        return new self(
            (int)$priceData['amountfrom'],
            (float)$priceData['price'],
            (float)$priceData['price_net']
        );
    }
}
