<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\Iso;

class TextDTO
{
    public string $iso;
    public ?string $title = '';
    public ?string $description = '';
    public ?string $abstract = '';

    public function __construct(
        string $iso,
        ?string $title = '',
        ?string $description = '',
        ?string $abstract = ''
    ) {
        $this->iso = $iso;
        $this->title = $title;
        $this->description = $description;
        $this->abstract = $abstract;
    }

    /** @param array<string, mixed> $data */
    public static function from(array $data): self
    {
        return new self(
            Iso::fromLanguageProperty($data),
            $data['title'],
            $data['description'] ?? '',
            $data['abstract'] ?? ''
        );
    }

    public function hasContent(): bool
    {
        return $this->title || $this->description || $this->abstract;
    }
}
