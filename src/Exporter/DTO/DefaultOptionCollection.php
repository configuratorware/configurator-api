<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class DefaultOptionCollection
{
    /** @var array<string, array<int, DefaultSelection>> */
    public array $componentSelectedOptionsMap = [];

    /** @param array<int, mixed> $data */
    public static function from(array $data): self
    {
        $collection = new self();
        if (!empty($data)) {
            foreach ($data[0]['selectedoptions'] as $componentIdentifier => $selectedOptions) {
                $selectedOptionDTOS = array_map(
                    static fn (array $selectedOptionData) => DefaultSelection::from($selectedOptionData),
                    $selectedOptions
                );
                $collection->componentSelectedOptionsMap[(string)$componentIdentifier] = $selectedOptionDTOS;
            }
        }

        return $collection;
    }

    /** @return DefaultSelection[] */
    public function findDefaultSelectionFor(string $componentIdentifier): array
    {
        return $this->componentSelectedOptionsMap[$componentIdentifier] ?? [];
    }
}
