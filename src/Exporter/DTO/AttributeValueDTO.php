<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class AttributeValueDTO
{
    public string $value;
    /** @var TranslationDTO[] */
    public array $translations = [];

    public function __construct(
        string $value
    ) {
        $this->value = $value;
    }

    /** @param array<string, mixed> $value */
    public static function from(array $value): self
    {
        $dto = new self($value['value']);
        $dto->translations = array_map(
            static fn (array $translationData) => TranslationDTO::from($translationData),
            $value['attributevaluetranslation']
        );
        usort(
            $dto->translations,
            static fn (TranslationDTO $trans1, TranslationDTO $trans2) => $trans1->iso <=> $trans2->iso
        );

        return $dto;
    }
}
