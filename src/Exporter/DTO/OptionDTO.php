<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\AttributeValueCollector;
use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\TextCollector;

class OptionDTO
{
    public string $optionIdentifier;
    public bool $amountIsSelectable = false;
    public int $sequencenumber = 1;
    public ?ImagesDTO $images = null;
    /** @var TextDTO[] */
    public array $texts = [];
    /** @var mixed[] */
    public array $prices = [];
    /** @var mixed[] */
    public array $deltaPrices = [];
    /** @var AttributeDTO[] */
    public array $attributes = [];
    public bool $hasTextinput = false;
    public ?string $inputValidationType = null;

    public function __construct(
        string $optionIdentifier,
        bool $amountIsSelectable,
        int $sequencenumber,
        bool $hasTextinput,
        ?string $inputValidationType
    ) {
        $this->optionIdentifier = $optionIdentifier;
        $this->amountIsSelectable = $amountIsSelectable;
        $this->sequencenumber = $sequencenumber;
        $this->hasTextinput = $hasTextinput;
        $this->inputValidationType = $inputValidationType;
    }

    /** @param array<string, mixed> $data */
    public static function from(array $data): self
    {
        $optionData = $data['option'];
        $dto = new self(
            $optionData['identifier'],
            $data['amountisselectable'],
            (int)$optionData['sequencenumber'],
            (bool)$optionData['hasTextinput'],
            (string)$optionData['inputValidationType']
        );

        $dto->texts = TextCollector::collectFrom($optionData['optionText']);
        $dto->attributes = AttributeValueCollector::collectFrom($optionData['optionAttribute']);

        return $dto;
    }
}
