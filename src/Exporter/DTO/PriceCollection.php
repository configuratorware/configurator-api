<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\ChannelPriceCollector;
use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\ComponentOptionKey;

class PriceCollection
{
    /**
     * @var array<string,DeltaPriceDTO[]>
     */
    private array $deltaPrices = [];

    /**
     * @var array<string,ChannelPriceDTO[]>
     */
    private array $optionPrices = [];

    /** @param array<string, mixed> $itemComponentOptions */
    private function __construct(array $itemComponentOptions)
    {
        foreach ($itemComponentOptions as $itemComponentOption) {
            $data = $itemComponentOption[0];
            $componentIdentifier = $itemComponentOption['componentIdentifier'];
            $optionIdentifier = $itemComponentOption['optionIdentifier'];
            $key = ComponentOptionKey::for($componentIdentifier, $optionIdentifier);

            if (isset($data['itemOptionclassificationOptionDeltaprice'])) {
                $this->deltaPrices[$key] = DeltaPriceDTO::fromList($data['itemOptionclassificationOptionDeltaprice']);
            }
            $this->optionPrices[$key] = ChannelPriceCollector::collectFrom($data['option']['optionPrice']);
        }
    }

    /** @param array<string, mixed> $priceData */
    public static function from(array $priceData): self
    {
        return new self($priceData);
    }

    /** @return DeltaPriceDTO[] */
    public function findDeltaPricesFor(string $componentIdentifier, string $optionIdentifier): array
    {
        return $this->deltaPrices[ComponentOptionKey::for($componentIdentifier, $optionIdentifier)] ?? [];
    }

    /** @return ChannelPriceDTO[] */
    public function findPrices(string $componentIdentifier, string $optionIdentifier): array
    {
        return $this->optionPrices[ComponentOptionKey::for($componentIdentifier, $optionIdentifier)] ?? [];
    }
}
