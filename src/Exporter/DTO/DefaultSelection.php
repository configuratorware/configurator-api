<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class DefaultSelection
{
    public string $optionIdentifier;

    public int $amount;

    public function __construct(string $optionIdentifier, int $amount)
    {
        $this->optionIdentifier = $optionIdentifier;
        $this->amount = $amount;
    }

    /** @param array<string, mixed> $data */
    public static function from(array $data): self
    {
        return new self($data['identifier'], $data['amount'] ?? 1);
    }
}
