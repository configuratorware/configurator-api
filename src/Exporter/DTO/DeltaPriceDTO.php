<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class DeltaPriceDTO
{
    public string $channelIdentifier;
    public \stdClass $prices;

    public function __construct(
        string $channelIdentifier,
        float $price,
        float $priceNet
    ) {
        $this->channelIdentifier = $channelIdentifier;
        $this->prices = new \stdClass();
        $this->prices->price = new \stdClass();
        $this->prices->price->price = $price;
        $this->prices->price->priceNet = $priceNet;
    }

    /** @param array<string, mixed> $priceData */
    public static function from(array $priceData): self
    {
        return new self(
            (string)$priceData['channel']['identifier'],
            (float)$priceData['price'],
            (float)$priceData['price_net']
        );
    }

    /**
     * @param array<string, mixed> $listOfPriceData
     *
     * @return self[]
     */
    public static function fromList(array $listOfPriceData): array
    {
        return array_map(
            static fn (array $priceData) => self::from($priceData),
            $listOfPriceData
        );
    }
}
