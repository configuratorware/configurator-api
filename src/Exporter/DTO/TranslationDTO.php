<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\Iso;

class TranslationDTO
{
    public string $translation = '';
    public string $iso;

    public function __construct(
        string $translation,
        string $iso
    ) {
        $this->translation = $translation;
        $this->iso = $iso;
    }

    /** @param array<string, mixed> $translationData */
    public static function from(array $translationData): self
    {
        return new self(
            $translationData['translation'],
            Iso::fromLanguageProperty($translationData)
        );
    }
}
