<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class ChannelPriceDTO
{
    public string $channelIdentifier;

    /** @var PriceDTO[] */
    public array $bulkPrices = [];

    public function __construct(string $channelIdentifier)
    {
        $this->channelIdentifier = $channelIdentifier;
    }

    /** @param array<string, mixed> $priceData */
    public static function from(array $priceData): self
    {
        return new self($priceData['channel']['identifier']);
    }

    /** @param array<string, mixed> $bulkPriceData */
    public function append(array $bulkPriceData): void
    {
        $this->bulkPrices[] = PriceDTO::from($bulkPriceData);
    }
}
