<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class AttributeDTO
{
    public string $attributeIdentifier;
    public string $type = 'string';
    /** @var TextDTO[] */
    public array $texts = [];
    /** @var AttributeValueDTO[] */
    public array $attributeValues = [];

    public function __construct(
        string $attributeIdentifier,
        string $type
    ) {
        $this->attributeIdentifier = $attributeIdentifier;
        $this->type = $type;
    }

    /** @param array<string, mixed> $attributeData */
    public static function from(array $attributeData): self
    {
        $dto = new self($attributeData['identifier'], $attributeData['attributedatatype']);
        $dto->texts = array_map(
            static fn (array $textData) => TextDTO::from($textData),
            $attributeData['attributetext'] ?? []
        );

        usort(
            $dto->texts,
            static fn (TextDTO $text1, TextDTO $text2) => $text1->iso <=> $text2->iso
        );

        return $dto;
    }

    /** @param array<string, mixed> $attributeValueData */
    public function append(array $attributeValueData): void
    {
        $this->attributeValues[] = AttributeValueDTO::from($attributeValueData);
    }
}
