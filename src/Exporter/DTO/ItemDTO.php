<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\AttributeValueCollector;
use Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject\ChannelPriceCollector;

class ItemDTO
{
    public string $itemIdentifier;
    public string $visualizationModeIdentifier;
    public string $configurationMode;
    public string $itemStatusIdentifier;
    public int $minimumOrderAmount;
    public bool $accumulateAmounts;
    /** @var TextDTO[] */
    public array $texts = [];
    /** @var ChannelPriceDTO[] */
    public array $prices = [];
    /** @var AttributeDTO[] */
    public array $attributes = [];
    /** @var ItemClassificationDTO[] */
    public array $itemClassifications = [];
    /** @var ComponentDTO[] */
    public array $components = [];

    public function __construct(
        string $itemIdentifier,
        string $visualizationModeIdentifier,
        string $configurationMode,
        string $itemStatusIdentifier,
        int $minimumOrderAmount,
        bool $accumulateAmounts
    ) {
        $this->itemIdentifier = $itemIdentifier;
        $this->visualizationModeIdentifier = $visualizationModeIdentifier;
        $this->configurationMode = $configurationMode;
        $this->itemStatusIdentifier = $itemStatusIdentifier;
        $this->minimumOrderAmount = $minimumOrderAmount;
        $this->accumulateAmounts = $accumulateAmounts;
    }

    /** @param array<string, mixed> $data */
    public static function from(array $data): self
    {
        $dto = new self(
            $data['identifier'],
            $data['visualizationMode']['identifier'] ?? '',
            $data['configuration_mode'] ?? '',
            $data['itemStatus']['identifier'] ?? '',
            (int)($data['minimum_order_amount'] ?? 0),
            (bool)$data['accumulate_amounts']
        );

        $dto->texts = array_filter(array_map(
            static fn (array $data) => TextDTO::from($data),
            $data['itemtext']
        ),
            static fn (TextDTO $textDTO) => $textDTO->hasContent()
        );
        $dto->attributes = AttributeValueCollector::collectFrom($data['itemAttribute']);
        $dto->prices = ChannelPriceCollector::collectFrom($data['itemprice']);
        $dto->itemClassifications = array_map(
            static fn (array $data) => ItemClassificationDTO::from($data['itemclassification']),
            $data['item_itemclassification']
        );

        return $dto;
    }
}
