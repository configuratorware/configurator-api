<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\DTO;

class ItemClassificationDTO
{
    public string $identifier;

    /** @var TextDTO[] */
    public array $texts = [];

    public function __construct(string $identifier)
    {
        $this->identifier = $identifier;
    }

    /** @param mixed[] $data */
    public static function from(array $data): self
    {
        $dto = new self($data['identifier']);

        $dto->texts = array_filter(array_map(
            static fn (array $data) => TextDTO::from($data),
            $data['itemclassificationtext']
        ),
            static fn (TextDTO $textDTO) => $textDTO->hasContent()
        );

        return $dto;
    }
}
