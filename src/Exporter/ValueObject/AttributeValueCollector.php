<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject;

use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\AttributeDTO;

class AttributeValueCollector
{
    /** @var AttributeDTO[] */
    public array $attributeMap = [];

    /**
     * @param array<string, mixed> $data
     *
     * @return AttributeDTO[]
     */
    public static function collectFrom(array $data): array
    {
        $collector = new self();

        foreach ($data as $attributeData) {
            $identifier = $collector->findIdentifier($attributeData);

            if (!isset($collector->attributeMap[$identifier])) {
                $collector->attributeMap[$identifier] = AttributeDTO::from($attributeData['attribute']);
            }

            $collector->attributeMap[$identifier]->append($attributeData['attributevalue']);
        }

        return array_values($collector->attributeMap);
    }

    /** @param array<string, mixed> $data */
    private function findIdentifier(array $data): string
    {
        return $data['attribute']['identifier'];
    }
}
