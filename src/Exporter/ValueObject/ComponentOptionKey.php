<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject;

class ComponentOptionKey
{
    public static function for(string $componentIdentifier, string $optionIdentifier): string
    {
        return $componentIdentifier . $optionIdentifier;
    }
}
