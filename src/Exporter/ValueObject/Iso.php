<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject;

class Iso
{
    public const DEFAULT = 'de_DE';

    /** @param array<string, mixed> $data */
    public static function fromLanguageProperty(array $data): string
    {
        return $data['language']['iso'];
    }
}
