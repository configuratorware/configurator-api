<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject;

use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\ChannelPriceDTO;

class ChannelPriceCollector
{
    /**
     * @param array<string, mixed> $data
     *
     * @return ChannelPriceDTO[]
     */
    public static function collectFrom(array $data): array
    {
        $channelMap = [];
        $collector = new self();

        foreach ($data as $priceData) {
            $identifier = $collector->findIdentifier($priceData);

            if (!isset($channelMap[$identifier])) {
                $channelMap[$identifier] = ChannelPriceDTO::from($priceData);
            }

            $channelMap[$identifier]->append($priceData);
        }

        return array_values($channelMap);
    }

    /** @param array<string, mixed> $data */
    private function findIdentifier(array $data): string
    {
        return $data['channel']['identifier'];
    }
}
