<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\ValueObject;

use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\TextDTO;

class TextCollector
{
    /**
     * @param mixed[] $data
     *
     * @return TextDTO[]
     */
    public static function collectFrom(array $data): array
    {
        return array_filter(array_map(
            static fn (array $data) => TextDTO::from($data),
            $data
        ),
            static fn (TextDTO $textDTO) => $textDTO->hasContent()
        );
    }
}
