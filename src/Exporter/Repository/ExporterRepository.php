<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exporter\Repository;

use Doctrine\Common\Collections\Criteria;
use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\ComponentDTO;
use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\DefaultOptionCollection;
use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\ItemDTO;
use Redhotmagma\ConfiguratorApiBundle\Exporter\DTO\PriceCollection;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;

class ExporterRepository
{
    private ItemRepository $itemRepository;

    private ItemOptionclassificationRepository $componentRepository;
    private ConfigurationRepository $configurationRepository;
    private ItemOptionclassificationOptionRepository $itemOptionclassificationOptionRepository;

    public function __construct(
        ItemRepository $itemRepository,
        ItemOptionclassificationRepository $componentRepository,
        ConfigurationRepository $configurationRepository,
        ItemOptionclassificationOptionRepository $itemOptionclassificationOptionRepository
    ) {
        $this->itemRepository = $itemRepository;
        $this->componentRepository = $componentRepository;
        $this->configurationRepository = $configurationRepository;
        $this->itemOptionclassificationOptionRepository = $itemOptionclassificationOptionRepository;
    }

    /**
     * @param string[] $itemIdentifiers
     *
     * @return ItemDTO[]
     */
    public function loadItemdata(array $itemIdentifiers): array
    {
        $qb = $this->itemRepository->createQueryBuilder('Item');

        $qb->select([
            'Item',
            'VisualizationMode',
            'ItemStatus',
            'ItemText',
            'Language',
            'ItemAttribute',
            'Attribute',
            'AttributeText',
            'Language2',
            'AttributeValue',
            'AttributeValueTranslation',
            'Language3',
            'ItemPrice',
            'Channel',
            'ItemItemClassification',
            'ItemClassification',
            'ItemClassificationText',
            'Language4',
        ]);

        $qb->leftJoin('Item.visualizationMode', 'VisualizationMode')
            ->leftJoin('Item.itemStatus', 'ItemStatus')
            ->leftJoin('Item.itemtext', 'ItemText')
            ->leftJoin('ItemText.language', 'Language')
            ->leftJoin('Item.itemAttribute', 'ItemAttribute')
            ->leftJoin('ItemAttribute.attribute', 'Attribute')
            ->leftJoin('Attribute.attributetext', 'AttributeText')
            ->leftJoin('AttributeText.language', 'Language2')
            ->leftJoin('ItemAttribute.attributevalue', 'AttributeValue')
            ->leftJoin('AttributeValue.attributevaluetranslation', 'AttributeValueTranslation')
            ->leftJoin('AttributeValueTranslation.language', 'Language3')
            ->leftJoin('Item.itemprice', 'ItemPrice')
            ->leftJoin('ItemPrice.channel', 'Channel')
            ->leftJoin('Item.item_itemclassification', 'ItemItemClassification')
            ->leftJoin('ItemItemClassification.itemclassification', 'ItemClassification')
            ->leftJoin('ItemClassification.itemclassificationtext', 'ItemClassificationText')
            ->leftJoin('ItemClassificationText.language', 'Language4')
            ->addOrderBy('ItemText.id', Criteria::DESC)
            ->addOrderBy('AttributeValueTranslation.id', Criteria::DESC)
            ->addOrderBy('ItemClassificationText.id', Criteria::DESC);

        if (!empty($itemIdentifiers)) {
            $expr = $qb->expr();
            $qb->where($expr->in('Item.identifier', ':itemIdentifiers'))
                ->setParameter('itemIdentifiers', $itemIdentifiers);
        }

        $result = $qb->getQuery()->getArrayResult();

        return array_map(static fn (array $data) => ItemDTO::from($data), $result);
    }

    /** @return ComponentDTO[] */
    public function loadComponentsFor(ItemDTO $item): array
    {
        $qb = $this->componentRepository->createQueryBuilder('ItemComponent');
        $qb->select([
                'ItemComponent',
                'Component',
                'ComponentText',
                'Language',
                'ComponentAttribute',
                'Attribute',
                'AttributeText',
                'Language2',
                'AttributeValue',
                'AttributeValueTranslation',
                'Language3',
                'ItemComponentOption',
                'Option',
                'OptionAttribute',
                'OptionText',
                'Language4',
                'Attribute2',
                'AttributeText2',
                'Language5',
                'AttributeValue2',
                'AttributeValueTranslation2',
                'Language6',
            ]
        );
        $qb->join('ItemComponent.item', 'Item')
            ->join('ItemComponent.optionclassification', 'Component')
            ->leftJoin('Component.optionClassificationText', 'ComponentText')
            ->leftJoin('ComponentText.language', 'Language')
            ->leftJoin('Component.optionClassificationAttribute', 'ComponentAttribute')
            ->leftJoin('ComponentAttribute.attribute', 'Attribute')
            ->leftJoin('Attribute.attributetext', 'AttributeText')
            ->leftJoin('AttributeText.language', 'Language2')
            ->leftJoin('ComponentAttribute.attributevalue', 'AttributeValue')
            ->leftJoin('AttributeValue.attributevaluetranslation', 'AttributeValueTranslation')
            ->leftJoin('AttributeValueTranslation.language', 'Language3')
            ->leftJoin('ItemComponent.itemOptionclassificationOption', 'ItemComponentOption')
            ->leftJoin('ItemComponentOption.option', 'Option')
            ->leftJoin('Option.optionAttribute', 'OptionAttribute')
            ->leftJoin('OptionAttribute.attribute', 'Attribute2')
            ->leftJoin('Option.optionText', 'OptionText')
            ->leftJoin('OptionText.language', 'Language4')
            ->leftJoin('Attribute2.attributetext', 'AttributeText2')
            ->leftJoin('AttributeText2.language', 'Language5')
            ->leftJoin('OptionAttribute.attributevalue', 'AttributeValue2')
            ->leftJoin('AttributeValue2.attributevaluetranslation', 'AttributeValueTranslation2')
            ->leftJoin('AttributeValueTranslation2.language', 'Language6')
            ->where($qb->expr()->eq('Item.identifier', ':itemIdentifier'))
            ->setParameter('itemIdentifier', $item->itemIdentifier);
        $result = $qb->getQuery()->getArrayResult();

        return array_map(static fn (array $data) => ComponentDTO::from($data), $result);
    }

    public function findDefaultOptionsFor(ItemDTO $itemDTO): DefaultOptionCollection
    {
        $qb = $this->configurationRepository->createQueryBuilder('Configuration');
        $expr = $qb->expr();
        $qb->join('Configuration.item', 'Item')
            ->join('Configuration.configurationtype', 'ConfigurationType')
            ->where($expr->eq('Item.identifier', ':itemIdentifier'))
            ->andWhere($expr->eq('ConfigurationType.identifier', ':defaultOptions'))
            ->setParameter('itemIdentifier', $itemDTO->itemIdentifier)
            ->setParameter('defaultOptions', 'defaultOptions');

        $result = $qb->getQuery()->getArrayResult();

        return DefaultOptionCollection::from($result);
    }

    public function findPricesByComponentAndOption(ItemDTO $itemDTO): PriceCollection
    {
        $qb = $this->itemOptionclassificationOptionRepository->createQueryBuilder('ItemComponentOption');
        $expr = $qb->expr();
        $qb->select([
            'ItemComponentOption',
            'Option',
            'OptionPrice',
            'Channel',
            'DeltaPrice',
            'Channel2',
            'Optionclassification.identifier as componentIdentifier',
            'Option.identifier as optionIdentifier',
        ]);
        $qb->join('ItemComponentOption.itemOptionclassification', 'ItemOptionclassification')
            ->join('ItemOptionclassification.item', 'Item')
            ->join('ItemOptionclassification.optionclassification', 'Optionclassification')
            ->join('ItemComponentOption.option', 'Option')
            ->leftJoin('Option.optionPrice', 'OptionPrice')
            ->join('OptionPrice.channel', 'Channel')
            ->leftJoin('ItemComponentOption.itemOptionclassificationOptionDeltaprice', 'DeltaPrice')
            ->leftjoin('DeltaPrice.channel', 'Channel2')
            ->where($expr->eq('Item.identifier', ':itemIdentifier'))
            ->setParameter('itemIdentifier', $itemDTO->itemIdentifier);

        $result = $qb->getQuery()->getArrayResult();

        return PriceCollection::from($result);
    }
}
