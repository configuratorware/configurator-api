<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_sheep;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadConfigurationData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $clientRepository = $this->container->get('doctrine.orm.entity_manager')->getRepository(Client::class);

        $configuration = new Configuration();
        $configuration->setCode('defaultoptions_1');
        $configuration->setName('defaultoptions_sheep');
        $configuration->setItem($this->getReference('item-sheep', Item::class));
        $configuration->setSelectedoptions(json_decode('{"coat":[{"identifier":"coat_nature"}],"face":[{"identifier":"face_nature"}],"legs":[{"identifier":"legs_nature"}],"eyes":[{"identifier":"eyes_yellow"}],"ear_eyelid":[{"identifier":"ear_eyelid_nature"}]}'));
        $configuration->setConfigurationtype($manager->getRepository(Configurationtype::class)->findOneBy(['identifier' => 'defaultoptions']));
        $configuration->setClient($clientRepository->findOneBy(['identifier' => '_default']));

        $manager->persist($configuration);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1050;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_sheep'];
    }
}
