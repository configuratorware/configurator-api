<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_sheep;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationText;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadOptionclassificationData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    private ContainerInterface $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        // COAT
        $optionclassification = new Optionclassification();
        $optionclassification->setIdentifier('coat');
        $optionclassification->setSequencenumber(1);
        $manager->persist($optionclassification);

        $this->addReference('optinclassification-coat', $optionclassification);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $optionClassificationText->setTitle('coat');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $optionClassificationText->setTitle('Fell');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        // FACE
        $optionclassification = new Optionclassification();
        $optionclassification->setIdentifier('face');
        $optionclassification->setSequencenumber(2);
        $manager->persist($optionclassification);
        $this->addReference('optinclassification-face', $optionclassification);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $optionClassificationText->setTitle('face');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $optionClassificationText->setTitle('Gesicht');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        // LEGS
        $optionclassification = new Optionclassification();
        $optionclassification->setIdentifier('legs');
        $optionclassification->setSequencenumber(3);
        $manager->persist($optionclassification);
        $this->addReference('optinclassification-legs', $optionclassification);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $optionClassificationText->setTitle('legs');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $optionClassificationText->setTitle('Beine');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        // EYES
        $optionclassification = new Optionclassification();
        $optionclassification->setIdentifier('eyes');
        $optionclassification->setSequencenumber(4);
        $manager->persist($optionclassification);
        $this->addReference('optinclassification-eyes', $optionclassification);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $optionClassificationText->setTitle('eyes');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $optionClassificationText->setTitle('Augen');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        // EAR & EYELID
        $optionclassification = new Optionclassification();
        $optionclassification->setIdentifier('ear_eyelid');
        $optionclassification->setSequencenumber(5);
        $manager->persist($optionclassification);
        $this->addReference('optinclassification-ear_eyelid', $optionclassification);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $optionClassificationText->setTitle('ear & eyelid');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        $optionClassificationText = new OptionClassificationText();
        $optionClassificationText->setOptionClassification($optionclassification);
        $optionClassificationText->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $optionClassificationText->setTitle('Ohr & Augenlid');
        $optionClassificationText->setDescription('');
        $manager->persist($optionClassificationText);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1020;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_sheep'];
    }
}
