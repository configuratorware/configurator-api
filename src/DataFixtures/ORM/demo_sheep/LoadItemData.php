<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_sheep;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassificationtext;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\Stock;
use Redhotmagma\ConfiguratorApiBundle\Entity\Stockstatus;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadItemData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        // Sheep
        $item = new Item();
        $item->setConfigurable(true);
        $item->setDeactivated(false);
        $item->setIdentifier('sheep');
        $item->setConfigurationMode('creator');
        $item->setDateCreated(new \DateTime('2018-01-01'));
        $item->setDateUpdated(new \DateTime('2018-05-05'));
        $manager->persist($item);
        $this->addReference('item-sheep', $item);

        // Item Status
        $statusRepository = $this->container->get('doctrine.orm.entity_manager')->getRepository(ItemStatus::class);
        $item->setItemStatus($statusRepository->findOneBy(['identifier' => 'available']));

        // Visualization Settings
        $visualizationRepository = $this->container->get('doctrine.orm.entity_manager')->getRepository(VisualizationMode::class);
        $item->setVisualizationMode($visualizationRepository->findOneByIdentifier(VisualizationMode::IDENTIFIER_3D_SCENE));

        // Attributes
        $itemattribute = new ItemAttribute();
        $itemattribute->setItem($item);
        $itemattribute->setAttribute($this->getReference('attribute-tags', Attribute::class));
        $itemattribute->setAttributevalue($this->getReference('attributevalue-vegetarian', Attributevalue::class));
        $manager->persist($itemattribute);

        $itemattribute = new ItemAttribute();
        $itemattribute->setItem($item);
        $itemattribute->setAttribute($this->getReference('attribute-tags', Attribute::class));
        $itemattribute->setAttributevalue($this->getReference('attributevalue-edible', Attributevalue::class));
        $manager->persist($itemattribute);

        // Prices
        $itemprice = new Itemprice();
        $itemprice->setChannel($manager->getRepository(Channel::class)->findOneBy(['identifier' => '_default']));
        $itemprice->setItem($item);
        $itemprice->setAmountfrom(1);
        $itemprice->setPrice(199);
        $itemprice->setPriceNet(167.22);
        $manager->persist($itemprice);

        $itemprice = new Itemprice();
        $itemprice->setChannel($manager->getRepository(Channel::class)->findOneBy(['identifier' => '_default']));
        $itemprice->setItem($item);
        $itemprice->setAmountfrom(10);
        $itemprice->setPrice(189);
        $itemprice->setPriceNet(158.82);
        $manager->persist($itemprice);

        // Stock
        $stock = new Stock();
        $stock->setChannel($manager->getRepository(Channel::class)->findOneBy(['identifier' => '_default']));
        $stock->setItem($item);
        $stock->setStockstatus($manager->getRepository(Stockstatus::class)->findOneBy(['identifier' => 'available']));
        $stock->setDeliverytime('');
        $manager->persist($stock);

        // Itemclassification
        $itemclassification = new Itemclassification();
        $itemclassification->setIdentifier('animal');
        $itemclassification->setSequencenumber(1);
        $manager->persist($itemclassification);

        $itemclassificationtext = new Itemclassificationtext();
        $itemclassificationtext->setItemclassification($itemclassification);
        $itemclassificationtext->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $itemclassificationtext->setTitle('animal');
        $itemclassificationtext->setDescription('all kinds of animals');
        $manager->persist($itemclassificationtext);

        $itemclassificationtext = new Itemclassificationtext();
        $itemclassificationtext->setItemclassification($itemclassification);
        $itemclassificationtext->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $itemclassificationtext->setTitle('Tier');
        $itemclassificationtext->setDescription('alle möglichen Tiere');
        $manager->persist($itemclassificationtext);

        $itemitemclassification = new ItemItemclassification();
        $itemitemclassification->setItemclassification($itemclassification);
        $itemitemclassification->setItem($item);
        $manager->persist($itemitemclassification);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1010;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_sheep'];
    }
}
