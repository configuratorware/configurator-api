<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_sheep;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPrice;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadOptionData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    private ContainerInterface $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $optionsData = $this->getOptionsData();

        foreach ($optionsData as $data) {
            $option = new Option();
            $option->setSequencenumber($data['sequenceNumber']);
            $option->setDeactivated($data['deactivated']);
            $option->setIdentifier($data['identifier']);
            $option->setHasTextinput(isset($data['hasTextinput']) && $data['hasTextinput']);
            $manager->persist($option);
            $this->addReference($data['reference'], $option);
            $this->addOptionPrices($manager, $data['prices'], $option);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1030;
    }

    /**
     * @param ObjectManager $manager
     * @param array $prices
     * @param Option $option
     */
    private function addOptionPrices(
        ObjectManager $manager,
        array $prices,
        Option $option
    ): void {
        foreach ($prices as $price) {
            $optionPrice = new OptionPrice();
            $optionPrice->setChannel($manager->getRepository(Channel::class)->findOneBy(['identifier' => '_default']));
            $optionPrice->setOption($option);
            $optionPrice->setAmountfrom($price['amountFrom']);
            $optionPrice->setPrice($price['grossPrice']);
            $optionPrice->setPriceNet(round($price['grossPrice'] / 1.19, 2));
            $manager->persist($optionPrice);
        }
    }

    private function getOptionsData(): array
    {
        return [
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'coat_nature',
                'reference' => 'option-coat_nature',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 12.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'coat_white',
                'hasTextinput' => true,
                'reference' => 'option-coat_white',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 13.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'coat_red',
                'reference' => 'option-coat_red',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 14.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'face_nature',
                'reference' => 'option-face_nature',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 15.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'face_white',
                'reference' => 'option-face_white',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 16.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'face_brown',
                'reference' => 'option-face_brown',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 17.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'legs_nature',
                'reference' => 'option-legs_nature',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 18.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'legs_white',
                'reference' => 'option-legs_white',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 19.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'legs_brown',
                'reference' => 'option-legs_brown',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 21.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'eyes_yellow',
                'reference' => 'option-eyes_yellow',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 22.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'eyes_blue',
                'reference' => 'option-eyes_blue',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 23.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'eyes_red',
                'reference' => 'option-eyes_red',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 24.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'ear_eyelid_nature',
                'reference' => 'option-ear_eyelid_nature',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 25.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'ear_eyelid_white',
                'reference' => 'option-ear_eyelid_white',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 26.45,
                    ],
                ],
            ],
            [
                'sequenceNumber' => 0,
                'deactivated' => 0,
                'identifier' => 'ear_eyelid_brown',
                'reference' => 'option-ear_eyelid_brown',
                'prices' => [
                    [
                        'amountFrom' => 1,
                        'grossPrice' => 27.45,
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_sheep'];
    }
}
