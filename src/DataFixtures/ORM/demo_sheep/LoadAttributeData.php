<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_sheep;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attributetext;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevaluetranslation;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadAttributeData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        // create tags attribute with translations
        $attribute = new Attribute();
        $attribute->setIdentifier('tags');
        $attribute->setAttributedatatype('string');
        $manager->persist($attribute);
        $this->addReference('attribute-tags', $attribute);

        $attributetext = new Attributetext();
        $attributetext->setAttribute($attribute);
        $attributetext->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $attributetext->setTitle('tags');
        $manager->persist($attributetext);

        $attributetext = new Attributetext();
        $attributetext->setAttribute($attribute);
        $attributetext->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $attributetext->setTitle('Tags');
        $manager->persist($attributetext);

        // create attributevalues with translations
        $attributevalue = new Attributevalue();
        $attributevalue->setValue('vegetarian');
        $manager->persist($attributevalue);
        $this->addReference('attributevalue-vegetarian', $attributevalue);

        $attributevaluetranslation = new Attributevaluetranslation();
        $attributevaluetranslation->setAttributevalue($attributevalue);
        $attributevaluetranslation->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $attributevaluetranslation->setTranslation('vegetarian');
        $manager->persist($attributevaluetranslation);

        $attributevaluetranslation = new Attributevaluetranslation();
        $attributevaluetranslation->setAttributevalue($attributevalue);
        $attributevaluetranslation->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $attributevaluetranslation->setTranslation('vegetarier');
        $manager->persist($attributevaluetranslation);

        $attributevalue = new Attributevalue();
        $attributevalue->setValue('edible');
        $manager->persist($attributevalue);
        $this->addReference('attributevalue-edible', $attributevalue);

        $attributevaluetranslation = new Attributevaluetranslation();
        $attributevaluetranslation->setAttributevalue($attributevalue);
        $attributevaluetranslation->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $attributevaluetranslation->setTranslation('edible');
        $manager->persist($attributevaluetranslation);

        $attributevaluetranslation = new Attributevaluetranslation();
        $attributevaluetranslation->setAttributevalue($attributevalue);
        $attributevaluetranslation->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $attributevaluetranslation->setTranslation('essbar');
        $manager->persist($attributevaluetranslation);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1000;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_sheep'];
    }
}
