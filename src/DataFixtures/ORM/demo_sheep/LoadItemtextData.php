<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_sheep;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadItemtextData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    private ContainerInterface $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $itemtext = new Itemtext();
        $itemtext->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'en_GB']));
        $itemtext->setItem($this->getReference('item-sheep', Item::class));
        $itemtext->setTitle('Sheep');
        $itemtext->setAbstract('Your wooly new friend.');
        $itemtext->setDescription('May be a black sheep. It´s up to you!');

        $manager->persist($itemtext);

        $itemtext = new Itemtext();
        $itemtext->setLanguage($manager->getRepository(Language::class)->findOneBy(['iso' => 'de_DE']));
        $itemtext->setItem($this->getReference('item-sheep', Item::class));
        $itemtext->setTitle('Schaf');
        $itemtext->setAbstract('Dein neuer wolliger Freund.');
        $itemtext->setDescription('Könnte ein schwarzes Schaf sein, deine Entscheidung!');

        $manager->persist($itemtext);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1070;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_sheep'];
    }
}
