<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_sheep;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOptionDeltaprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadConstructionPatternData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    private float $deltaPrice = 2.44;

    private ContainerInterface $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        // COAT
        $itemOptionClassification = new ItemOptionclassification();
        $itemOptionClassification->setItem($this->getReference('item-sheep', Item::class));
        $itemOptionClassification->setOptionclassification($this->getReference('optinclassification-coat', Optionclassification::class));
        $itemOptionClassification->setIsMultiselect(false);
        $itemOptionClassification->setIsMandatory(true);
        $itemOptionClassification->setSequenceNumber(3);
        $manager->persist($itemOptionClassification);

        $reference = 'option-coat_nature';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        $reference = 'option-coat_white';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, false);

        $reference = 'option-coat_red';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        // FACE
        $itemOptionClassification = new ItemOptionclassification();
        $itemOptionClassification->setItem($this->getReference('item-sheep', Item::class));
        $itemOptionClassification->setOptionclassification($this->getReference('optinclassification-face', Optionclassification::class));
        $itemOptionClassification->setIsMultiselect(false);
        $itemOptionClassification->setIsMandatory(true);
        $itemOptionClassification->setSequenceNumber(5);
        $manager->persist($itemOptionClassification);

        $reference = 'option-face_nature';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        $reference = 'option-face_white';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, false);

        $reference = 'option-face_brown';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        // LEGS
        $itemOptionClassification = new ItemOptionclassification();
        $itemOptionClassification->setItem($this->getReference('item-sheep', Item::class));
        $itemOptionClassification->setOptionclassification($this->getReference('optinclassification-legs', Optionclassification::class));
        $itemOptionClassification->setIsMultiselect(false);
        $itemOptionClassification->setIsMandatory(true);
        $manager->persist($itemOptionClassification);

        $reference = 'option-legs_nature';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        $reference = 'option-legs_white';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, false);

        $reference = 'option-legs_brown';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        // EYES
        $itemOptionClassification = new ItemOptionclassification();
        $itemOptionClassification->setItem($this->getReference('item-sheep', Item::class));
        $itemOptionClassification->setOptionclassification($this->getReference('optinclassification-eyes', Optionclassification::class));
        $itemOptionClassification->setIsMultiselect(false);
        $itemOptionClassification->setIsMandatory(true);
        $manager->persist($itemOptionClassification);

        $reference = 'option-eyes_yellow';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        $reference = 'option-eyes_blue';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, false);

        $reference = 'option-eyes_red';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        // EAR & EYELID
        $itemOptionClassification = new ItemOptionclassification();
        $itemOptionClassification->setItem($this->getReference('item-sheep', Item::class));
        $itemOptionClassification->setOptionclassification($this->getReference('optinclassification-ear_eyelid', Optionclassification::class));
        $itemOptionClassification->setIsMultiselect(false);
        $itemOptionClassification->setIsMandatory(true);
        $manager->persist($itemOptionClassification);

        $reference = 'option-ear_eyelid_nature';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        $reference = 'option-ear_eyelid_white';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, false);

        $reference = 'option-ear_eyelid_brown';
        $itemOptionClassificationOption = $this->createItemOptionClassificationOption(
            $manager,
            $itemOptionClassification,
            $reference
        );
        $this->createItemOptionClassificationOptionDeltaPrice($manager, $itemOptionClassificationOption, true);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1040;
    }

    public function getDeltaPrice($isPositive): float
    {
        if ($isPositive) {
            return $this->deltaPrice += 2.44;
        }

        return -($this->deltaPrice += 2.44);
    }

    private function createItemOptionClassificationOption(
        ObjectManager $manager,
        ItemOptionclassification $itemOptionClassification,
        string $reference
    ): ItemOptionclassificationOption {
        $itemOptionClassificationOption = new ItemOptionclassificationOption();
        $itemOptionClassificationOption->setItemOptionclassification($itemOptionClassification);
        $itemOptionClassificationOption->setOption($this->getReference($reference, Option::class));
        $itemOptionClassificationOption->setAmountisselectable(false);
        $manager->persist($itemOptionClassificationOption);

        return $itemOptionClassificationOption;
    }

    private function createItemOptionClassificationOptionDeltaPrice(
        ObjectManager $manager,
        ItemOptionclassificationOption $itemOptionClassificationOption,
        bool $isPositive
    ): void {
        $itemOptionClassificationOptionDeltaPrice = new ItemOptionclassificationOptionDeltaprice();
        $itemOptionClassificationOptionDeltaPrice->setItemOptionclassificationOption($itemOptionClassificationOption);
        $itemOptionClassificationOptionDeltaPrice->setPrice($this->getDeltaPrice($isPositive));

        $channel = $manager->getRepository(Channel::class)->findOneBy(['identifier' => '_default']);
        if (null !== $channel) {
            $itemOptionClassificationOptionDeltaPrice->setChannel($channel);
        }

        $manager->persist($itemOptionClassificationOptionDeltaPrice);
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_sheep'];
    }
}
