<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_designer_base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDesignProductionMethodData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $designProductionMethods = $this->getDesignProductionMethods();

        foreach ($designProductionMethods as $designProductionMethod) {
            $method = new DesignProductionMethod();
            $method->setIdentifier($designProductionMethod['identifier']);
            $method->setOptions($designProductionMethod['options']);
            $this->addReference($designProductionMethod['identifier'], $method);
            foreach ($designProductionMethod['texts'] as $language => $text) {
                $methodtext = new DesignProductionMethodText();
                $methodtext->setLanguage(
                    $manager->getRepository(Language::class)->findOneBy(['iso' => $language])
                );
                $methodtext->setTitle($text);
                $methodtext->setDesignProductionMethod($this->getReference($designProductionMethod['identifier'], DesignProductionMethod::class));
                $method->addDesignProductionMethodText($methodtext);
            }

            $methodColorRelation = new DesignProductionMethodColorPalette();
            $methodColorRelation->setDesignProductionMethod($this->getReference($designProductionMethod['identifier'], DesignProductionMethod::class));
            $methodColorRelation->setColorPalette(
                $manager->getRepository(ColorPalette::class)->findOneBy(['identifier' => 'default'])
            );

            $method->addDesignProductionMethodColorPalette($methodColorRelation);
            $manager->persist($method);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 1000;
    }

    private function getDesignProductionMethods(): array
    {
        return [
            [
                'identifier' => 'screen_printing',
                'options' => '{"minFontSize":4,"maxColorAmount":4,"visualizationEffect":null,"hasEngravingBackgroundColors":false, "vectorsRequired":true}',
                'texts' => [
                    'en_GB' => 'Screen Printing',
                    'de_DE' => 'Siebdruck',
                ],
            ],
            [
                'identifier' => 'pad_printing',
                'options' => '{"minFontSize":4,"maxColorAmount":4,"visualizationEffect":null,"hasEngravingBackgroundColors":false, "vectorsRequired":true}',
                'texts' => [
                    'en_GB' => 'Pad Printing',
                    'de_DE' => 'Tampondruck',
                ],
            ],
            [
                'identifier' => 'transfer_printing',
                'options' => '{"minFontSize":4,"maxColorAmount":4,"visualizationEffect":null,"hasEngravingBackgroundColors":false, "vectorsRequired":true}',
                'texts' => [
                    'en_GB' => 'Transfer Printing',
                    'de_DE' => 'Transferdruck',
                ],
            ],
            [
                'identifier' => 'engraving',
                'options' => '{"minFontSize":6,"maxColorAmount":1,"visualizationEffect":"engraving","hasEngravingBackgroundColors":true, "vectorsRequired":null}',
                'texts' => [
                    'en_GB' => 'Engraving',
                    'de_DE' => 'Gravur',
                ],
            ],
            [
                'identifier' => 'embroidery',
                'options' => '{"minFontSize":6,"maxColorAmount":8,"visualizationEffect":"embroidery","hasEngravingBackgroundColors":false, "vectorsRequired":true}',
                'texts' => [
                    'en_GB' => 'Embroidery',
                    'de_DE' => 'Stick',
                ],
            ],
            [
                'identifier' => 'digital_printing',
                'options' => '{"minFontSize":4,"maxColorAmount":null,"visualizationEffect":null,"hasEngravingBackgroundColors":false, "vectorsRequired":false}',
                'texts' => [
                    'en_GB' => 'Digital Printing',
                    'de_DE' => 'Digitaldruck',
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_designer_base'];
    }
}
