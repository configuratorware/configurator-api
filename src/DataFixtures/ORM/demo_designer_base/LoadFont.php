<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_designer_base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadFont extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    private ContainerInterface $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $fonts = $this->getFonts();

        foreach ($fonts as $font) {
            $entity = new Font();
            $entity->setName($font['name']);
            $entity->setActive(true);
            $entity->setFileNameRegular($font['file_name_regular'] ?? null);
            $entity->setFileNameBold($font['file_name_bold'] ?? null);
            $entity->setFileNameItalic($font['file_name_italic'] ?? null);
            $entity->setFileNameBoldItalic($font['file_name_bold_italic'] ?? null);
            $manager->persist($entity);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 1000;
    }

    private function getFonts(): array
    {
        return [
            [
                'name' => 'Amatic',
                'file_name_regular' => 'Amatic_SC-regular.ttf',
                'file_name_bold' => 'Amatic_SC-bold.ttf',
            ],
            [
                'name' => 'Bangers',
                'file_name_regular' => 'Bangers-regular.ttf',
            ],
            [
                'name' => 'Bree',
                'file_name_regular' => 'Bree_Serif-regular.ttf',
            ],
            [
                'name' => 'Crismon',
                'file_name_regular' => 'Crismon_Text-regular.ttf',
                'file_name_bold' => 'Crismon_Text-bold.ttf',
                'file_name_italic' => 'Crismon_Text-italic.ttf',
                'file_name_bold_italic' => 'Crismon_Text-bolditalic.ttf',
            ],
            [
                'name' => 'Lobster',
                'file_name_regular' => 'Lobster-regular.ttf',
            ],
            [
                'name' => 'Luckiest',
                'file_name_regular' => 'Luckiest_Guy-regular.ttf',
            ],
            [
                'name' => 'Merriweather',
                'file_name_regular' => 'Merriweather-regular.ttf',
                'file_name_bold' => 'Merriweather-bold.ttf',
                'file_name_bold_italic' => 'Merriweather-bolditalic.ttf',
                'file_name_italic' => 'Merriweather-italic.ttf',
            ],
            [
                'name' => 'Montsserrat',
                'file_name_regular' => 'Montserrat-regular.ttf',
                'file_name_bold' => 'Montserrat-bold.ttf',
                'file_name_bold_italic' => 'Montserrat-bolditalic.ttf',
                'file_name_italic' => 'Montserrat-italic.ttf',
            ],
            [
                'name' => 'OpenSans',
                'file_name_regular' => 'Open_Sans-regular.ttf',
                'file_name_bold' => 'Open_Sans-bold.ttf',
                'file_name_bold_italic' => 'Open_Sans-bolditalic.ttf',
                'file_name_italic' => 'Open_Sans-italic.ttf',
            ],
            [
                'name' => 'OpenSansCondensed',
                'file_name_regular' => 'Open_Sans_Condensed-regular.ttf',
                'file_name_bold' => 'Open_Sans_Condensed-bold.ttf',
                'file_name_bold_italic' => 'Open_Sans_Condensed-bolditalic.ttf',
                'file_name_italic' => 'Open_Sans_Condensed-italic.ttf',
            ],
            [
                'name' => 'PoiretOne-Regular',
                'file_name_regular' => 'Poiret_One-regular.ttf',
            ],
            [
                'name' => 'Roboto',
                'file_name_regular' => 'Roboto-regular.ttf',
                'file_name_bold' => 'Roboto-bold.ttf',
                'file_name_bold_italic' => 'Roboto-bolditalic.ttf',
                'file_name_italic' => 'Roboto-italic.ttf',
            ],
            [
                'name' => 'RobotoCondensed',
                'file_name_regular' => 'Roboto_Condensed-regular.ttf',
                'file_name_bold' => 'Roboto_Condensed-bold.ttf',
                'file_name_bold_italic' => 'Roboto_Condensed-bolditalic.ttf',
                'file_name_italic' => 'Roboto_Condensed-italic.ttf',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_designer_base'];
    }
}
