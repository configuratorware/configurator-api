<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_designer_base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Color;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPaletteText;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDefaultColors extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $languageRepo = $manager->getRepository(Language::class);
        $langEn = $languageRepo->findOneBy(['iso' => 'en_GB']);
        $langDe = $languageRepo->findOneBy(['iso' => 'de_DE']);

        $palette = new ColorPalette();
        $palette->setIdentifier('default');

        $paletteTextEn = new ColorPaletteText();
        $paletteTextEn->setLanguage($langEn);
        $paletteTextEn->setTitle('default colors');
        $palette->addColorPaletteText($paletteTextEn);
        $paletteTextEn->setColorPalette($palette);

        $paletteTextDe = new ColorPaletteText();
        $paletteTextDe->setLanguage($langDe);
        $paletteTextDe->setTitle('Standardfarben');
        $palette->addColorPaletteText($paletteTextDe);
        $paletteTextDe->setColorPalette($palette);

        $manager->persist($palette);

        $colorsArray = $this->getDefaultColors();
        foreach ($colorsArray as $colorArray) {
            $color = new Color();
            $color->setIdentifier($colorArray['title_en']);
            $color->setHexValue($colorArray['hex']);

            $colorTextEn = new ColorText();
            $colorTextEn->setLanguage($langEn);
            $colorTextEn->setTitle($colorArray['title_en']);
            $color->addColorText($colorTextEn);
            $colorTextEn->setColor($color);

            $colorTextDe = new ColorText();
            $colorTextDe->setLanguage($langDe);
            $colorTextDe->setTitle($colorArray['title_de']);
            $color->addColorText($colorTextDe);
            $colorTextDe->setColor($color);

            $palette->addColor($color);
            $color->setColorPalette($palette);
        }

        $manager->persist($palette);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 950;
    }

    /**
     * Get default colors array.
     *
     * @return array
     */
    private function getDefaultColors(): array
    {
        return [
            [
                'hex' => '000000',
                'title_de' => 'schwarz',
                'title_en' => 'black',
            ],
            [
                'hex' => 'ffffff',
                'title_de' => 'weiß',
                'title_en' => 'white',
            ],
            [
                'hex' => '2eb24c',
                'title_de' => 'grün',
                'title_en' => 'green',
            ],
            [
                'hex' => '85c342',
                'title_de' => 'grüngelb',
                'title_en' => 'green-yellow',
            ],
            [
                'hex' => 'f2ed22',
                'title_de' => 'gelb',
                'title_en' => 'yellow',
            ],
            [
                'hex' => 'fcd107',
                'title_de' => 'gelborange',
                'title_en' => 'yellow-orange',
            ],
            [
                'hex' => 'f9a61a',
                'title_de' => 'orange',
                'title_en' => 'orange',
            ],
            [
                'hex' => 'f57d1f',
                'title_de' => 'orangerot',
                'title_en' => 'orange-red',
            ],
            [
                'hex' => 'ec2b26',
                'title_de' => 'rot',
                'title_en' => 'red',
            ],
            [
                'hex' => 'eb1a91',
                'title_de' => 'pink',
                'title_en' => 'pink',
            ],
            [
                'hex' => '804299',
                'title_de' => 'violett',
                'title_en' => 'purple',
            ],
            [
                'hex' => '29449b',
                'title_de' => 'dunkelblau',
                'title_en' => 'dark blue',
            ],
            [
                'hex' => '0e7bbe',
                'title_de' => 'blau',
                'title_en' => 'blue',
            ],
            [
                'hex' => '0bb3d8',
                'title_de' => 'blaugrün',
                'title_en' => 'blue-green',
            ],
            [
                'hex' => '333333',
                'title_de' => 'dunkelgrau',
                'title_en' => 'dark grey',
            ],
            [
                'hex' => '777777',
                'title_de' => 'mittelgrau',
                'title_en' => 'grey',
            ],
            [
                'hex' => 'bbbbbb',
                'title_de' => 'hellgrau',
                'title_en' => 'light grey',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_designer_base'];
    }
}
