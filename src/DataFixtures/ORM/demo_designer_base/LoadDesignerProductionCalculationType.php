<?php
/**
 * Created by PhpStorm.
 * User: simon
 * Date: 2019-04-24
 * Time: 16:10.
 */

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_designer_base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationTypeText;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDesignerProductionCalculationType extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $designerProductionCalculationTypes = $this->getDesignerProductionCalculationTypes();

        foreach ($designerProductionCalculationTypes as $designerProductionCalculationType) {
            $calculationType = new DesignerProductionCalculationType();
            $calculationType->setIdentifier($designerProductionCalculationType['identifier']);
            $calculationType->setColorAmountDependent($designerProductionCalculationType['color_amount_dependent']);
            $calculationType->setItemAmountDependent($designerProductionCalculationType['item_amount_dependent']);
            $calculationType->setSelectableByUser($designerProductionCalculationType['selectable_by_user']);
            $calculationType->setDeclareSeparately($designerProductionCalculationType['declare_separately']);
            $calculationType->setDesignProductionMethod(
                $this->getReference($designerProductionCalculationType['designProductionMethodIdentifier'], DesignProductionMethod::class)
            );
            $this->addReference($designerProductionCalculationType['identifier'], $calculationType);

            foreach ($designerProductionCalculationType['texts'] as $language => $text) {
                $calculationTypeText = new DesignerProductionCalculationTypeText();
                $calculationTypeText->setLanguage(
                    $manager->getRepository(Language::class)->findOneBy(['iso' => $language])
                );
                $calculationTypeText->setTitle($text);
                $calculationTypeText->setDesignerProductionCalculationType(
                    $this->getReference($designerProductionCalculationType['identifier'], DesignerProductionCalculationType::class)
                );
                $calculationType->addDesignerProductionCalculationTypeText($calculationTypeText);
            }

            if (false === $designerProductionCalculationType['item_amount_dependent']) {
                foreach ($designerProductionCalculationType['prices'] as $price) {
                    $calculationTypePrice = new DesignProductionMethodPrice();
                    $calculationTypePrice->setColorAmountFrom($price['color_amount_from']);
                    $calculationTypePrice->setPrice($price['price']);
                    $calculationTypePrice->setPriceNet($price['priceNet']);
                    $calculationTypePrice->setChannel(
                        $manager->getRepository(Channel::class)->findOneBy(['identifier' => '_default'])
                    );
                    $calculationTypePrice->setDesignerProductionCalculationType(
                        $this->getReference($designerProductionCalculationType['identifier'], DesignerProductionCalculationType::class)
                    );
                    $calculationType->addDesignProductionMethodPrice($calculationTypePrice);
                }
            }

            $manager->persist($calculationType);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 1100;
    }

    private function getDesignerProductionCalculationTypes(): array
    {
        return [
            [
                'identifier' => 'printing_costs_digital_printing',
                'color_amount_dependent' => false,
                'item_amount_dependent' => true,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'digital_printing',
                'prices' => [],
                'texts' => [
                    'en_GB' => 'Printing costs',
                    'de_DE' => 'Druckkosten',
                ],
            ],
            [
                'identifier' => 'basic_cost_pad_printing',
                'color_amount_dependent' => false,
                'item_amount_dependent' => false,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'pad_printing',
                'prices' => [
                    [
                        'color_amount_from' => 49.00,
                        'item_amount_from' => null,
                        'price' => 49.00,
                        'priceNet' => 41.17,
                    ],
                ],
                'texts' => [
                    'en_GB' => 'Basic costs',
                    'de_DE' => 'Grundkosten',
                ],
            ],
            [
                'identifier' => 'printing_costs_pad_printing',
                'color_amount_dependent' => false,
                'item_amount_dependent' => true,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'pad_printing',
                'prices' => [],
                'texts' => [
                    'en_GB' => 'Printing costs',
                    'de_DE' => 'Druckkosten',
                ],
            ],
            [
                'identifier' => 'basic_cost_screen_printing',
                'color_amount_dependent' => false,
                'item_amount_dependent' => false,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'screen_printing',
                'prices' => [
                    [
                        'color_amount_from' => 49.00,
                        'item_amount_from' => null,
                        'price' => 49.00,
                        'priceNet' => 41.17,
                    ],
                ],
                'texts' => [
                    'en_GB' => 'Basic costs',
                    'de_DE' => 'Grundkosten',
                ],
            ],
            [
                'identifier' => 'printing_costs_screen_printing',
                'color_amount_dependent' => false,
                'item_amount_dependent' => true,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'screen_printing',
                'prices' => [],
                'texts' => [
                    'en_GB' => 'Printing costs',
                    'de_DE' => 'Druckkosten',
                ],
            ],
            [
                'identifier' => 'basic_cost_transfer_printing',
                'color_amount_dependent' => false,
                'item_amount_dependent' => false,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'transfer_printing',
                'prices' => [
                    [
                        'color_amount_from' => 49.00,
                        'item_amount_from' => null,
                        'price' => 49.00,
                        'priceNet' => 41.17,
                    ],
                ],
                'texts' => [
                    'en_GB' => 'Basic costs',
                    'de_DE' => 'Grundkosten',
                ],
            ],
            [
                'identifier' => 'printing_costs_transfer_printing',
                'color_amount_dependent' => false,
                'item_amount_dependent' => true,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'transfer_printing',
                'prices' => [],
                'texts' => [
                    'en_GB' => 'Printing costs',
                    'de_DE' => 'Druckkosten',
                ],
            ],
            [
                'identifier' => 'basic_cost_embroidery',
                'color_amount_dependent' => false,
                'item_amount_dependent' => false,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'embroidery',
                'prices' => [
                    [
                        'color_amount_from' => 49.00,
                        'item_amount_from' => null,
                        'price' => 49.00,
                        'priceNet' => 41.17,
                    ],
                ],
                'texts' => [
                    'en_GB' => 'Basic costs',
                    'de_DE' => 'Grundkosten',
                ],
            ],
            [
                'identifier' => 'printing_costs_embroidery',
                'color_amount_dependent' => false,
                'item_amount_dependent' => true,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'embroidery',
                'prices' => [],
                'texts' => [
                    'en_GB' => 'Printing costs',
                    'de_DE' => 'Druckkosten',
                ],
            ],
            [
                'identifier' => 'basic_cost_engraving',
                'color_amount_dependent' => false,
                'item_amount_dependent' => false,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'engraving',
                'prices' => [
                    [
                        'color_amount_from' => 49.00,
                        'item_amount_from' => null,
                        'price' => 49.00,
                        'priceNet' => 41.17,
                    ],
                ],
                'texts' => [
                    'en_GB' => 'Basic costs',
                    'de_DE' => 'Grundkosten',
                ],
            ],
            [
                'identifier' => 'printing_costs_engraving',
                'color_amount_dependent' => false,
                'item_amount_dependent' => true,
                'selectable_by_user' => false,
                'declare_separately' => false,
                'designProductionMethodIdentifier' => 'engraving',
                'prices' => [],
                'texts' => [
                    'en_GB' => 'Printing costs',
                    'de_DE' => 'Druckkosten',
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_designer_base'];
    }
}
