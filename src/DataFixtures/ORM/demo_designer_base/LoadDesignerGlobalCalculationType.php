<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\demo_designer_base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypePrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypeText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadDesignerGlobalCalculationType extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * Load data fixtures with the passed EntityManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $designerGlobalCalculationTypes = $this->getDesignerGlobalCalculationTypes();

        foreach ($designerGlobalCalculationTypes as $designerGlobalCalculationType) {
            $calculationType = new DesignerGlobalCalculationType();
            $calculationType->setIdentifier($designerGlobalCalculationType['identifier']);
            $calculationType->setItemAmountDependent($designerGlobalCalculationType['item_amount_dependent']);
            $calculationType->setSelectableByUser($designerGlobalCalculationType['selectable_by_user']);
            $calculationType->setImageDataDependent($designerGlobalCalculationType['image_data_dependent']);
            $this->addReference($designerGlobalCalculationType['identifier'], $calculationType);

            foreach ($designerGlobalCalculationType['texts'] as $language => $text) {
                $calculationTypeText = new DesignerGlobalCalculationTypeText();
                $calculationTypeText->setLanguage(
                    $manager->getRepository(Language::class)->findOneBy(['iso' => $language])
                );
                $calculationTypeText->setTitle($text);
                $calculationTypeText->setDesignerGlobalCalculationType($this->getReference($designerGlobalCalculationType['identifier'], DesignerGlobalCalculationType::class));
                $calculationType->addDesignerGlobalCalculationTypeText($calculationTypeText);
            }

            if (false === $designerGlobalCalculationType['item_amount_dependent']) {
                $calculationTypePrice = new DesignerGlobalCalculationTypePrice();
                $calculationTypePrice->setPrice($designerGlobalCalculationType['price']);
                $calculationTypePrice->setPriceNet($designerGlobalCalculationType['priceNet']);
                $calculationTypePrice->setChannel($manager->getRepository(Channel::class)->findOneBy(['identifier' => '_default']));
                $calculationTypePrice->setDesignerGlobalCalculationType($this->getReference($designerGlobalCalculationType['identifier'], DesignerGlobalCalculationType::class));
                $calculationType->addDesignerGlobalCalculationTypePrice($calculationTypePrice);
            }

            $manager->persist($calculationType);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture.
     *
     * @return int
     */
    public function getOrder(): int
    {
        return 1100;
    }

    private function getDesignerGlobalCalculationTypes(): array
    {
        return [
            [
                'identifier' => 'professional_filecheck',
                'item_amount_dependent' => false,
                'selectable_by_user' => true,
                'image_data_dependent' => false,
                'price' => 49.00,
                'priceNet' => 41.17,
                'texts' => [
                    'en_GB' => 'Professional filecheck',
                    'de_DE' => 'Proficheck',
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['demo_designer_base'];
    }
}
