<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\user_admin;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadClientData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $defaultClient = $manager->getRepository(Client::class)->findOneBy(['identifier' => '_default']);

        if ($defaultClient instanceof Client) {
            $defaultClient->setFromEmailAddress('info@redhotmagma.de')->setToEmailAddresses('info@redhotmagma.de');
            $manager->persist($defaultClient);
            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 70;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['user_admin'];
    }
}
