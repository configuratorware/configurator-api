<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\user_admin;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Entity\UserRole;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadUserRoleData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $repository = $this->container->get('doctrine.orm.entity_manager')->getRepository(Role::class);

        $objUserRole = new UserRole();
        $objUserRole->setUser($this->getReference('user-admin', User::class));
        $objUserRole->setRole($repository->findOneBy(['role' => 'ROLE_ADMIN']));
        $manager->persist($objUserRole);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 80;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['user_admin'];
    }
}
