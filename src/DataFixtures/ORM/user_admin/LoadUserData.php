<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\user_admin;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @internal
 */
class LoadUserData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        // Admin User
        $adminUser = new User();
        $adminUser->setUsername('admin');
        $adminUser->setEmail('admin@redhotmagma.de');
        $adminUser->setPassword('admin', $this->passwordHasher);
        $adminUser->setIsActive(1);
        $manager->persist($adminUser);
        $this->addReference('user-admin', $adminUser);

        // Client User
        $clientUser = new User();
        $clientUser->setUsername('rhm');
        $clientUser->setEmail('rhm@redhotmagma.de');
        $clientUser->setPassword('admin', $this->passwordHasher);
        $clientUser->setIsActive(1);
        $manager->persist($clientUser);
        $this->addReference('user-rhm', $clientUser);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 70;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['user_admin'];
    }
}
