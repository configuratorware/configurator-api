<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Stockstatus;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadStockstatusData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $stockstatus = new Stockstatus();
        $stockstatus->setIdentifier('available');
        $manager->persist($stockstatus);
        $this->addReference('stockstatus-available', $stockstatus);

        $stockstatus = new Stockstatus();
        $stockstatus->setIdentifier('limited_availability');
        $manager->persist($stockstatus);
        $this->addReference('stockstatus-limited_availability', $stockstatus);

        $stockstatus = new Stockstatus();
        $stockstatus->setIdentifier('not_available');
        $manager->persist($stockstatus);
        $this->addReference('stockstatus-not_available', $stockstatus);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 120;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['base'];
    }
}
