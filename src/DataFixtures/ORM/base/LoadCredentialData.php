<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Credential;
use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadCredentialData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        foreach ($this->getCredentialArray() as $strCredential) {
            $objCredential = new Credential();
            $objCredential->setArea($strCredential);
            $objCredential->setIdentifier('access');
            $manager->persist($objCredential);

            $objRoleCredential = new RoleCredential();
            $objRoleCredential->setRole($this->getReference('role-admin', Role::class));
            $objRoleCredential->setCredential($objCredential);
            $manager->persist($objRoleCredential);
        }

        $manager->flush();
    }

    /**
     * returns credentials array.
     *
     * @return string[]
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    protected function getCredentialArray(): array
    {
        return [
            'users',
            'credentials',
            'configurations',
            'production_view',
            'tags',
            'image_gallery',
            'items',
            'options',
            'option_classifications',
            'attributes',
            'attribute_values',
            'inspirations',
            'design_templates',
            'option_pools',
            'item_classifications',
            'configuration_variants',
            'code_snippets',
            'assembly_points',
            'channels',
            'roles',
            'currencies',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 50;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['base'];
    }
}
