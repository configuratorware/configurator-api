<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktexttranslation;
use Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadRulefeedbacktextData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-itemdependency', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_itemdependency');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('[optionidentifier_needed] is needed to select [optionidentifier_selected].');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('[optionidentifier_needed] muss gewählt sein, um [optionidentifier_selected] auswählen zu können.');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-itemexclusion', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_itemexclusion');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('[optionidentifier_excluded] must not be selected to select [optionidentifier_selected].');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('[optionidentifier_excluded] darf nicht gewählt sein, um [optionidentifier_selected] auswählen zu können.');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-optionclassificationminamount', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_minamount');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('The minimum amount for options in [optionclassificationidentifier] is [minamount].');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Die Mindestmenge für Optionen in [optionclassificationidentifier] ist [minamount].');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-optionclassificationmaxamount', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_maxamount');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('The maximal amount for options in [optionclassificationidentifier] is [maxamount].');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Die Maximalmenge für Optionen in [optionclassificationidentifier] ist [maxamount].');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-itemattributematch', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_itemattributematch');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Incompatible values for "[attribute]".');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Werte für "[attribute]" stimmen nicht überein.');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-itemattributemaxsum', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_itemattributemaxsum');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Max amount for "[attribute]" exceeded.');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Maximalwert für "[attribute]" überschritten.');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-optionmaxamount', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_optionmaxamount');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Max amount for "[option]" exceeded.');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('Maximale Menge für "[option]" überschritten.');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-defaultoption', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_defaultoption');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-additionaloption', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_additionaloption');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-attribute_value_group_auto_switch', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('error_attribute_value_group_auto_switch');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackText = new Rulefeedbacktext();
        $ruleFeedbackText->setRuletype($this->getReference('ruletype-attribute_value_group_auto_switch', Ruletype::class));
        $ruleFeedbackText->setRulefeedback($this->getReference('rulefeedback-silent', Rulefeedback::class));
        $ruleFeedbackText->setIdentifier('silent_attribute_value_group_auto_switch');
        $manager->persist($ruleFeedbackText);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-en', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('');
        $manager->persist($ruleFeedbackTexttranslation);

        $ruleFeedbackTexttranslation = new Rulefeedbacktexttranslation();
        $ruleFeedbackTexttranslation->setRulefeedbacktext($ruleFeedbackText);
        $ruleFeedbackTexttranslation->setLanguage($this->getReference('language-de', Language::class));
        $ruleFeedbackTexttranslation->setTranslation('');
        $manager->persist($ruleFeedbackTexttranslation);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 110;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['base'];
    }
}
