<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadLanguageData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $objLanguage = new Language();
        $objLanguage->setIso('en_GB');
        $objLanguage->setDateformat('m/d/Y');
        $objLanguage->setDecimalpoint('.');
        $objLanguage->setThousandsseparator(',');
        $objLanguage->setPricedecimals(2);
        $objLanguage->setCurrencysymbolposition('left');
        $objLanguage->setCurrencysymbolposition('left');
        $objLanguage->setIsDefault(true);

        $manager->persist($objLanguage);

        $this->addReference('language-en', $objLanguage);

        $objLanguage = new Language();
        $objLanguage->setIso('de_DE');
        $objLanguage->setDateformat('d.m.Y');
        $objLanguage->setDecimalpoint(',');
        $objLanguage->setThousandsseparator('.');
        $objLanguage->setPricedecimals(2);
        $objLanguage->setCurrencysymbolposition('right');
        $objLanguage->setIsDefault(false);

        $manager->persist($objLanguage);

        $this->addReference('language-de', $objLanguage);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 10;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['base'];
    }
}
