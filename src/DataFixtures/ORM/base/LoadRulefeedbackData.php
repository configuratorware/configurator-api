<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback;
use Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype;
use Redhotmagma\ConfiguratorApiBundle\Entity\RuletypeRulefeedback;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadRulefeedbackData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $rulefeedback = new Rulefeedback();
        $rulefeedback->setIdentifier('error');

        $manager->persist($rulefeedback);
        $this->addReference('rulefeedback-error', $rulefeedback);

        $rulefeedback = new Rulefeedback();
        $rulefeedback->setIdentifier('silent');

        $manager->persist($rulefeedback);
        $this->addReference('rulefeedback-silent', $rulefeedback);

        $rulefeedback = new Rulefeedback();
        $rulefeedback->setIdentifier('notice');

        $manager->persist($rulefeedback);
        $this->addReference('rulefeedback-notice', $rulefeedback);

        // add RuletypeRulefeedback relations
        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-itemdependency', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-itemexclusion', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-optionclassificationminamount', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-optionclassificationmaxamount', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-itemattributematch', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-itemattributemaxsum', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-optionmaxamount', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-error', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-defaultoption', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-silent', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-additionaloption', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-silent', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $ruletypeRulefeedback = new RuletypeRulefeedback();
        $ruletypeRulefeedback->setRuletype($this->getReference('ruletype-attribute_value_group_auto_switch', Ruletype::class));
        $ruletypeRulefeedback->setRulefeedback($this->getReference('rulefeedback-silent', Rulefeedback::class));
        $manager->persist($ruletypeRulefeedback);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 100;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['base'];
    }
}
