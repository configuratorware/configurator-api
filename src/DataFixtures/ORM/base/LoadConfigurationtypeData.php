<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadConfigurationtypeData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('user');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('defaultoptions');
        $manager->persist($configurationtype);

        $this->addReference('configurationtype-defaultoptions', $configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('inspiration');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('cart');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('mail');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('share');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('languageswitch');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('print');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('designtemplate');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('partslist');
        $manager->persist($configurationtype);

        $configurationtype = new Configurationtype();
        $configurationtype->setIdentifier('channelswitch');
        $manager->persist($configurationtype);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['base'];
    }
}
