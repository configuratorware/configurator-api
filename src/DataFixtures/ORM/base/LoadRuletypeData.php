<?php

namespace Redhotmagma\ConfiguratorApiBundle\DataFixtures\ORM\base;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @internal
 */
class LoadRuletypeData extends Fixture implements OrderedFixtureInterface, ContainerAwareInterface, FixtureGroupInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager): void
    {
        $ruletype = new Ruletype();
        $ruletype->setIdentifier('optiondependency');
        $ruletype->setSequencenumber(1);
        $ruletype->setItemrule(true);
        $ruletype->setOptionrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['option_identifier']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-itemdependency', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('optionexclusion');
        $ruletype->setSequencenumber(2);
        $ruletype->setItemrule(true);
        $ruletype->setOptionrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['option_identifier']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-itemexclusion', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('optionclassificationminamount');
        $ruletype->setSequencenumber(3);
        $ruletype->setItemrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['minamount', 'optionclassification_identifier']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-optionclassificationminamount', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('optionclassificationmaxamount');
        $ruletype->setSequencenumber(4);
        $ruletype->setItemrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['maxamount', 'optionclassification_identifier']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-optionclassificationmaxamount', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('itemattributematch');
        $ruletype->setSequencenumber(5);
        $ruletype->setItemrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['attribute_identifier']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-itemattributematch', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('itemattributemaxsum');
        $ruletype->setSequencenumber(6);
        $ruletype->setItemrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['attribute_identifier', 'maxamount']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-itemattributemaxsum', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('optionmaxamount');
        $ruletype->setSequencenumber(7);
        $ruletype->setItemrule(true);
        $ruletype->setOptionrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['maxamount']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-optionmaxamount', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('defaultoption');
        $ruletype->setSequencenumber(99);
        $ruletype->setItemrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['option_identifier']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-defaultoption', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('additionaloption');
        $ruletype->setSequencenumber(100);
        $ruletype->setItemrule(true);
        $ruletype->setOptionrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['option_identifier']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-additionaloption', $ruletype);

        $ruletype = new Ruletype();
        $ruletype->setIdentifier('attribute_value_group_auto_switch');
        $ruletype->setSequencenumber(8);
        $ruletype->setItemrule(true);
        $ruletype->setGlobalrule(0);
        $ruletype->setAvailablerulefields(['attribute_identifier', 'apply_per_option_classification']);

        $manager->persist($ruletype);
        $this->addReference('ruletype-attribute_value_group_auto_switch', $ruletype);

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder(): int
    {
        return 90;
    }

    /**
     * {@inheritDoc}
     */
    public static function getGroups(): array
    {
        return ['base'];
    }
}
