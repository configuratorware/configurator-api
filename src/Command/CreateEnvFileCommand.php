<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class CreateEnvFileCommand extends Command
{
    private const URL_PREFIX = 'DATABASE_URL=';
    private const ENV_PREFIX = 'APP_ENV=';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $projectDir;

    /**
     * @param Filesystem $filesystem
     * @param string $projectDir
     */
    public function __construct(Filesystem $filesystem, string $projectDir)
    {
        $this->filesystem = $filesystem;
        $this->projectDir = $projectDir;

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('configuratorware:create-env-file')
            ->setDescription('Creates the .env.local with the credentials for the MySQL connection')
            ->addArgument('username', InputArgument::REQUIRED, 'The username used for the connection')
            ->addArgument('password', InputArgument::REQUIRED, 'The password used for the connection')
            ->addArgument('host', InputArgument::REQUIRED, 'The MySQL host')
            ->addArgument('port', InputArgument::REQUIRED, 'The port used for the connection')
            ->addArgument('db_name', InputArgument::REQUIRED, 'The database\'s name')
            ->addArgument('env', InputArgument::OPTIONAL, 'If dev or prod. Default: prod', 'prod');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->checkForExistingEnvLocal();
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());

            return 0;
        }

        $connectionURL = sprintf(
            'mysql://%s:%s@%s:%s/%s',
            $input->getArgument('username'),
            $input->getArgument('password'),
            $input->getArgument('host'),
            $input->getArgument('port'),
            $input->getArgument('db_name')
        );

        $env = self::ENV_PREFIX . $input->getArgument('env');

        $db = self::URL_PREFIX . $connectionURL;

        $this->filesystem->dumpFile($this->projectDir . '/.env.local', $db . PHP_EOL . $env);

        return 0;
    }

    /**
     * @throws \Exception
     */
    private function checkForExistingEnvLocal(): void
    {
        if ($this->filesystem->exists($this->projectDir . '/.env.local')) {
            throw new \Exception('A .env.local file already exists and won\'t be overwritten');
        }
    }
}
