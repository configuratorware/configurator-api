<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @deprecated use lexik:jwt:generate-keypair --skip-if-exists
 */
class JWTKeyGeneratorCommand extends Command
{
    /**
     * @var string
     */
    private $jwtPrivateKeyPath;

    /**
     * @var string
     */
    private $jwtPublicKeyPath;

    /**
     * @var string
     */
    private $jwtKeyPassPhrase;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @param $jwtPrivateKeyPath
     * @param $jwtPublicKeyPath
     * @param $jwtKeyPassPhrase
     */
    public function __construct($jwtPrivateKeyPath, $jwtPublicKeyPath, $jwtKeyPassPhrase)
    {
        $this->jwtPrivateKeyPath = $jwtPrivateKeyPath;
        $this->jwtPublicKeyPath = $jwtPublicKeyPath;
        $this->jwtKeyPassPhrase = $jwtKeyPassPhrase;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName('redhotmagma:generate-jwt-keys')
            ->setDescription('@deprecated - Generate the JWT Key files for Authentication.');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->fileSystem = new Filesystem();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);
        $style->warning('This command is deprecated, use `lexik:jwt:generate-keypair --skip-if-exists` instead.');

        $returnCode = $this->generateKey($output);
        if (0 !== $returnCode) {
            return $returnCode;
        }
        $returnCode = $this->validateKeyGeneration($output);

        return $returnCode;
    }

    protected function generateKey(OutputInterface $output): int
    {
        if (empty($this->jwtKeyPassPhrase)) {
            $output->writeln('You have to set parameter "jwt_key_pass_phrase"');

            return 1;
        }

        // exit if the keys already exist
        if (is_file($this->jwtPrivateKeyPath) && is_file($this->jwtPublicKeyPath)) {
            $output->writeln('Keys already exist');

            return 0;
        }

        $output->writeln('Generate JWT Authentication key for Configurator');
        $destination = dirname($this->jwtPrivateKeyPath) . DIRECTORY_SEPARATOR;

        $this->fileSystem->mkdir($destination);

        $tmpPassPhraseFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'passphrase';

        $this->fileSystem->dumpFile($tmpPassPhraseFile, $this->jwtKeyPassPhrase);

        exec(sprintf(
            'openssl genrsa -passout file:%s -out %s -aes256 4096',
            $tmpPassPhraseFile,
            $this->jwtPrivateKeyPath
        ));

        $this->fileSystem->chmod($this->jwtPrivateKeyPath, 0640);

        exec(sprintf(
            'openssl rsa -passin file:%s  -pubout -in %s -out %s',
            $tmpPassPhraseFile,
            $this->jwtPrivateKeyPath,
            $this->jwtPublicKeyPath
        ));

        $this->fileSystem->chmod($this->jwtPublicKeyPath, 0640);

        $this->fileSystem->remove($tmpPassPhraseFile);
        $output->writeln('JWT Key Generation Successful');

        return 0;
    }

    private function validateKeyGeneration(OutputInterface $output): int
    {
        $command = $this->getApplication()->find('lexik:jwt:check-config');

        $arguments = [
            'command' => 'lexik:jwt:check-config',
        ];

        $greetInput = new ArrayInput($arguments);
        $returnCode = $command->run($greetInput, $output);

        return $returnCode;
    }
}
