<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Doctrine\DBAL\Exception\TableNotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Service\FrontendTranslationFile\CachedFrontendTranslationFileCreator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateCachedTranslationCommand extends Command
{
    use LockableTrait;

    /**
     * @var CachedFrontendTranslationFileCreator
     */
    private $cachedTranslationCreator;

    /**
     * CreateCachedTranslationCommand constructor.
     *
     * @param CachedFrontendTranslationFileCreator $cachedTranslationCreator
     */
    public function __construct(
        CachedFrontendTranslationFileCreator $cachedTranslationCreator
    ) {
        parent::__construct();
        $this->cachedTranslationCreator = $cachedTranslationCreator;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('configuratorware:create-cached-translation')
            ->setDescription('Combine available translations to a cached translation, per language');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);

        if (!$this->lock()) {
            $output->writeln('The command is already running!');

            return 0;
        }

        $style->writeln('Creating cached translation files');

        try {
            $this->cachedTranslationCreator->createCachedTranslationFiles();
        } catch (TableNotFoundException $exception) {
            $style->note('Language table is not present, can\'t create merged translation files');
        }

        return 0;
    }
}
