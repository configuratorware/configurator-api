<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FileHandler;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\FontPathsInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DownloadFontsCommand extends Command
{
    private const DOWNLOAD_SLUG = 'fonts';
    private const FONT_FILENAME = 'fonts.tar.gz';
    private const FONT_TYPES = ['-regular.ttf', '-italic.ttf', '-bold.ttf', '-bolditalic.ttf'];

    /**
     * @var FileHandler
     */
    private $fileHandler;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var FontPathsInterface
     */
    private $fontPaths;

    /**
     * @var FontRepository
     */
    private $fontRepository;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var string
     */
    private $server;

    /**
     * @param FileHandler $fileHandler
     * @param Filesystem $filesystem
     * @param FontPathsInterface $fontPaths
     * @param FontRepository $fontRepository
     * @param string $mediaBasePath
     * @param string $server
     */
    public function __construct(
        FileHandler $fileHandler,
        Filesystem $filesystem,
        FontPathsInterface $fontPaths,
        FontRepository $fontRepository,
        string $mediaBasePath,
        string $server
    ) {
        parent::__construct();
        $this->fileHandler = $fileHandler;
        $this->filesystem = $filesystem;
        $this->fontPaths = $fontPaths;
        $this->mediaBasePath = $mediaBasePath;
        $this->server = rtrim($server, '/');
        $this->fontRepository = $fontRepository;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this->setName('configuratorware:download-fonts')
            ->setDescription('Downloads compressed font files');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $fontUploadPath = $this->fontPaths->getFontPathRelative();
        $designerPath = str_replace('/' . self::DOWNLOAD_SLUG, '', $fontUploadPath);

        $this->downloadFiles($this->mediaBasePath . '/' . $designerPath);
        $this->updateDatabase($this->mediaBasePath . '/' . $fontUploadPath);

        return 0;
    }

    /**
     * @param string $destinationFolder
     */
    private function downloadFiles(string $destinationFolder): void
    {
        $downloadUrl = $this->server . '/' . self::DOWNLOAD_SLUG . '/' . self::FONT_FILENAME;

        $file = $this->fileHandler->downloadFile($downloadUrl);

        if ($file) {
            $archiveFullFileName = $destinationFolder . DIRECTORY_SEPARATOR . self::FONT_FILENAME;

            $this->filesystem->dumpFile($archiveFullFileName, $file);

            $this->fileHandler->uncompress($destinationFolder, self::FONT_FILENAME, true);

            $this->filesystem->remove($archiveFullFileName);
        }
    }

    /**
     * @param string $fontFolder
     */
    private function updateDatabase(string $fontFolder): void
    {
        $finder = new Finder();
        $finder->in($fontFolder);

        $mappedFonts = $this->mapFonts($finder);
        $existingFontNames = $this->existingFontNames($this->fontRepository->findByNames(array_keys($mappedFonts)));

        foreach ($mappedFonts as $name => $font) {
            if (!in_array($name, $existingFontNames, true)) {
                $newFont = new Font();
                $newFont->setName($name);
                $newFont->setActive(true);
                $newFont->setFileNameRegular($font['-regular.ttf'] ?? null);
                $newFont->setFileNameItalic($font['-italic.ttf'] ?? null);
                $newFont->setFileNameBold($font['-bold.ttf'] ?? null);
                $newFont->setFileNameBoldItalic($font['-bolditalic.ttf'] ?? null);
                $this->fontRepository->save($newFont);
            }
        }
    }

    /**
     * @param Finder $finder
     *
     * @return array
     */
    private function mapFonts(Finder $finder): array
    {
        $fonts = [];

        foreach ($finder as $item) {
            $name = $item->getRelativePathname();
            foreach (self::FONT_TYPES as $type) {
                if (false !== strpos($name, $type)) {
                    $fonts[str_replace($type, '', $name)][$type] = $name;
                }
            }
        }

        return $fonts;
    }

    /**
     * @param Font[] $existingFonts
     *
     * @return array
     */
    private function existingFontNames(array $existingFonts): array
    {
        $keys = [];

        foreach ($existingFonts as $font) {
            $keys[] = $font->getName();
        }

        return $keys;
    }
}
