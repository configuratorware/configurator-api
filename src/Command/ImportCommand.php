<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportCommand extends Command
{
    use LockableTrait;

    private ImportApi $importApi;

    public function __construct(
        ImportApi $importApi
    ) {
        parent::__construct();
        $this->importApi = $importApi;
    }

    protected static $optionNames = [
        'deleteNonExistingItems',
        'deleteNonExistingAttributeRelations',
        'deleteWriteProtectedAttributes',
        'deleteNonExistingItemClassificationRelations',
        'deleteNonExistingItemPrices',
        'deleteNonExistingDesignAreas',
        'deleteNonExistingDesignAreaVisualData',
        'deleteNonExistingDesignAreaDesignProductionMethod',
        'deleteNonExistingDesignAreaDesignProductionMethodPrices',
        'deleteNonExistingDesignViewThumbnails',
        'deleteNonExistingItemImages',
        'deleteNonExistingComponentThumbnails',
        'deleteNonExistingOptionThumbnails',
        'deleteNonExistingComponentRelations',
        'overwriteItemImages',
        'priceChannel',
    ];

    protected function configure(): void
    {
        $this
            ->setName('configuratorware:import')
            ->setDescription('Import XML and JSON files from the file system or URL')
            ->addOption(
                'deleteNonExistingItems',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent items',
                false
            )
            ->addOption(
                'deleteNonExistingAttributeRelations',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent attribute relations',
                true
            )
            ->addOption(
                'deleteWriteProtectedAttributes',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent attributes even if marked as "writeProtected"',
                false
            )
            ->addOption(
                'deleteNonExistingItemClassificationRelations',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent item classification relations',
                true
            )
            ->addOption(
                'deleteNonExistingItemPrices',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent item prices',
                true
            )
            ->addOption(
                'deleteNonExistingDesignAreas',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent design areas',
                true
            )
            ->addOption(
                'deleteNonExistingDesignAreaVisualData',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent design area visual data',
                false
            )
            ->addOption(
                'deleteNonExistingDesignAreaDesignProductionMethodPrices',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent design area design production method prices',
                true
            )
            ->addOption(
                'deleteNonExistingDesignAreaDesignProductionMethod',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent design area design production methods',
                true
            )
            ->addOption(
                'deleteNonExistingDesignViewThumbnails',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent design view thumbnails',
                false
            )->addOption(
                'deleteNonExistingItemImages',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete item images and thumbnails for imported components without images',
                false
            )->addOption(
                'deleteNonExistingComponentThumbnails',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete component thumbnails for imported components without images',
                false
            )->addOption(
                'deleteNonExistingOptionThumbnails',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete option thumbnails and detail images for imported options without images',
                false
            )->addOption(
                'deleteNonExistingComponentRelations',
                '',
                InputOption::VALUE_REQUIRED,
                'Delete nonexistent component relations',
                false
            )
            ->addOption(
                'dry-run',
                '',
                InputOption::VALUE_NONE,
                'Dry run the import, log queries'
            )
            ->addOption(
                'log-sql',
                '',
                InputOption::VALUE_NONE,
                'Dry run the import, log queries'
            )
            ->addOption(
                'move-processed-file',
                '',
                InputOption::VALUE_REQUIRED,
                'Move a processed file to log dir',
                true
            )
            ->addOption(
                'source',
                '',
                InputOption::VALUE_OPTIONAL,
                'Path to source file or directory, or URL',
                false
            )
            ->addOption(
                'overwriteItemImages',
                '',
                InputOption::VALUE_REQUIRED,
                'Overwrite already present item images',
                false
            )->addOption(
                'priceChannel',
                '',
                InputOption::VALUE_REQUIRED,
                'Manipulate prices only in this specified channel, e.g. "_default"',
                ''
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int Error code
     *
     * @throws \Doctrine\DBAL\ConnectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $style = new SymfonyStyle($input, $output);

        if (!$this->lock()) {
            $output->writeln('An import is already running!');

            return 0;
        }

        $importOptions = $this->getImportOptions($input);

        // print argument info if verbose
        if ($output->isVerbose()) {
            $this->printArgumentInfo($style, $importOptions);
        }

        // error if source not specified
        if (!$input->getOption('source')) {
            $style->warning('Source file not specified!');

            return 0;
        }

        $content = $this->importApi->readFromSource($input->getOption('source'), $importOptions);

        if (empty($content)) {
            $style->writeln('Empty content.');

            return 0;
        }

        // convert data and print exception messages
        try {
            $importArray = $this->importApi->prepareImportArray($content);
        } catch (\InvalidArgumentException $exception) {
            $style->warning($exception->getMessage());

            return 1;
        }

        $isImported = $this->importApi->executeImport(
            $importOptions,
            $importArray
        );

        if ($isImported) {
            $style->success('Import successfully finished.');
        } else {
            $style->warning('Import failed.');
        }

        $this->importApi->removeOldImportFiles();

        return 0;
    }

    /**
     * @param OutputInterface $output
     * @param ImportOptions $options
     */
    public function printArgumentInfo(OutputInterface $output, ImportOptions $options): void
    {
        $output->writeln('<options=underscore>Import options are:</>');
        foreach (self::$optionNames as $optionName) {
            $output->writeln('    ' . $optionName . ': ' . ($options->$optionName ? 'yes' : 'no'));
        }
    }

    /**
     * @param InputInterface $input
     *
     * @return ImportOptions
     */
    public function getImportOptions(InputInterface $input): ImportOptions
    {
        $importOptions = new ImportOptions();
        foreach (self::$optionNames as $optionName) {
            if (null !== $input->getOption($optionName)) {
                $importOptions->$optionName = filter_var($input->getOption($optionName), FILTER_VALIDATE_BOOLEAN);
            }
        }

        $importOptions->priceChannel = $input->getOption('priceChannel');
        $importOptions->dryRun = $input->getOption('dry-run');
        $importOptions->logSql = $input->getOption('log-sql');
        $importOptions->moveProcessedFile = $input->getOption('move-processed-file');

        return $importOptions;
    }
}
