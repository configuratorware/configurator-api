<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Entity\RuleText;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Rule\RuleTitleGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateRuleTitlesCommand extends Command
{
    /**
     * @var RuleRepository
     */
    private $ruleRepository;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var RuleTitleGenerator
     */
    private $ruleTitleGenerator;

    /**
     * UpdateRuleTitlesCommand constructor.
     *
     * @param RuleRepository $ruleRepository
     * @param LanguageRepository $languageRepository
     * @param RuleTitleGenerator $ruleTitleGenerator
     */
    public function __construct(
        RuleRepository $ruleRepository,
        LanguageRepository $languageRepository,
        RuleTitleGenerator $ruleTitleGenerator
    ) {
        $this->ruleRepository = $ruleRepository;
        $this->languageRepository = $languageRepository;
        $this->ruleTitleGenerator = $ruleTitleGenerator;

        parent::__construct('configuratorware:update-rule-titles');
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this->setName('configuratorware:update-rule-titles')
            ->setDescription('Generate rule titles for all rules in all languages');
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        if (!defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', 'de_DE');
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $rules = $this->ruleRepository->findAll();
        $languages = $this->languageRepository->findAll();
        $this->updateRuleTitles($rules, $languages);

        return 0;
    }

    /**
     * @param array $rules
     * @param array $languages
     */
    private function updateRuleTitles($rules, $languages): void
    {
        /** @var Rule $rule */
        foreach ($rules as $rule) {
            /** @var Language $language */
            foreach ($languages as $language) {
                $found = false;
                /** @var RuleText $ruleText */
                foreach ($rule->getRuleText() as $ruleText) {
                    if ($ruleText->getLanguage()->getId() == $language->getId()) {
                        $found = true;
                        $ruleTitle = $this->ruleTitleGenerator->getRuleTitle($rule, $language->getIso());
                        $ruleText->setTitle($ruleTitle);
                    }
                }

                if (!$found) {
                    $ruleText = new RuleText();
                    $ruleText->setRule($rule);
                    $ruleText->setLanguage($language);

                    $ruleTitle = $this->ruleTitleGenerator->getRuleTitle($rule, $language->getIso());
                    $ruleText->setTitle($ruleTitle);

                    $rule->addRuleText($ruleText);
                }
            }

            $this->ruleRepository->save($rule);
        }
    }
}
