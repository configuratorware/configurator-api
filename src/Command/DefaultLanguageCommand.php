<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Language\LanguageApi;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DefaultLanguageCommand extends Command
{
    private LanguageRepository $languageRepository;

    private LanguageApi $languageApi;

    /**
     * @param LanguageRepository $languageRepository
     * @param LanguageApi $languageApi
     */
    public function __construct(LanguageRepository $languageRepository, LanguageApi $languageApi)
    {
        parent::__construct();
        $this->languageRepository = $languageRepository;
        $this->languageApi = $languageApi;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this->setName('configuratorware:default-language')->setDescription('Change default language')
            ->addArgument(
                'iso',
                InputArgument::REQUIRED,
                'The iso code for the language that should be default'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $iso = (string)$input->getArgument('iso');

        $entity = $this->languageRepository->findOneBy(['iso' => $iso]);

        if (null === $entity) {
            $output->writeln(sprintf('This language does not exist %s', $iso));

            return 1;
        }

        $structure = $this->languageApi->getOne($entity->getId());

        $structure->isDefault = true;

        $this->languageApi->save($structure);

        return 0;
    }
}
