<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Service\Export\ExportItemService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConfiguratorwareExportItemCommand extends Command
{
    protected static $defaultName = 'configuratorware:export-item';

    private ExportItemService $exportItemService;

    public function __construct(ExportItemService $exportItemService)
    {
        $this->exportItemService = $exportItemService;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setName(self::$defaultName);
        $this
            ->setDescription('Export Excel file to importable json file')
            ->addOption(
                'item-filter',
                'i',
                InputOption::VALUE_OPTIONAL,
                'Items to export. When empty, all items are exported. Can be a comma-separated list of item identifiers.',
                false
            )
            ->addOption(
                'output-folder',
                'o',
                InputOption::VALUE_REQUIRED,
                'Exported json file for importer'
            )
            ->addOption(
                'combine',
                'c',
                InputOption::VALUE_NONE,
                'Combine all items into one export file'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$outputFolder = $input->getOption('output-folder')) {
            $output->writeln('<error>Output folder is required.</error>');

            return self::FAILURE;
        }

        $combineOutput = $input->getOption('combine');
        $items = [];
        if ($itemsAsString = (string)$input->getOption('item-filter')) {
            $items = explode(',', $itemsAsString);
        }

        try {
            $this->exportItemService->exportItems($items, $outputFolder, $combineOutput);
        } catch (\InvalidArgumentException $e) {
            $output->writeln('<error>' . $e->getMessage() . '</error>');

            return self::FAILURE;
        }

        return self::SUCCESS;
    }
}
