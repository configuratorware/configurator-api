<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\FileHandler;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DownloadFrontendAppsCommand extends Command
{
    private const VALID_PROJECTS = ['frontendgui', 'admingui', 'frontendgui+admingui'];

    private const DOWNLOADABLE_CONTENT = [
        'frontendgui' => [
            'base_url' => '{{server}}/configurator-frontendgui/',
            'destination_folder' => '/',
        ],
        'admingui' => [
            'base_url' => '{{server}}/configurator-admingui/',
            'destination_folder' => '/admin/',
        ],
    ];

    private const TO_BACKUP = [
        'frontendgui' => [
            'bundle*',
            'favicon.ico',
            'fonts' => ['MaterialIcons-Regular*'],
            'images' => [
                'designer_placeholder_logo.svg',
                'embroidery_multiply*',
                'embroidery_screen*',
                'error_gears*',
            ],
            'translations' => ['*.json'],
            'translations_custom' => ['*.json'],
        ],
        'admingui' => [
            'admin' => [
                'bundle*',
                'favicon.ico',
            ],
        ],
    ];

    private const FILE_EXTENSION = 'tar.gz';

    private const UNWRAP_PATH = '/public';

    /**
     * @var FileHandler
     */
    private $fileHandler;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $webDocumentRoot;

    /**
     * @var string
     */
    private $server;

    /**
     * @param FileHandler $fileHandler
     * @param Filesystem $filesystem
     * @param string $webDocumentRoot
     * @param string $server
     */
    public function __construct(
        FileHandler $fileHandler,
        Filesystem $filesystem,
        string $webDocumentRoot,
        string $server
    ) {
        $this->fileHandler = $fileHandler;
        $this->filesystem = $filesystem;
        $this->webDocumentRoot = $webDocumentRoot;
        $this->server = rtrim($server, '/');

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('configuratorware:download-frontend-apps')
            ->setDescription('Downloads the compressed frontend files')
            ->addArgument(
                'projects',
                InputArgument::REQUIRED,
                sprintf('The projects that should be downloaded. Possible values: %s %s or %s', ...self::VALID_PROJECTS)
            )
            ->addArgument(
                'version',
                InputArgument::OPTIONAL,
                'The version to be downloaded. Defaults to latest.',
                'latest'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $projectsArg = (string)$input->getArgument('projects');
        if (!in_array($projectsArg, self::VALID_PROJECTS, true)) {
            $output->writeln(sprintf('Wrong input. Possible values: %s %s or %s', ...self::VALID_PROJECTS));

            return 0;
        }

        $requestedProjects = explode('+', $projectsArg);
        $fileName = $input->getArgument('version') . '.' . self::FILE_EXTENSION;

        foreach (self::DOWNLOADABLE_CONTENT as $project => $content) {
            if (!in_array($project, $requestedProjects, true)) {
                continue;
            }

            $this->fileHandler->backupFiles(
                $this->webDocumentRoot,
                $this->findFilesToBackup($this->webDocumentRoot, self::TO_BACKUP[$project] ?? []),
                $this->webDocumentRoot . DIRECTORY_SEPARATOR . $project . '.zip'
            );

            $destinationFolder = $this->webDocumentRoot . $content['destination_folder'];
            $serverUrl = str_replace('{{server}}', $this->server, $content['base_url']);

            $file = $this->fileHandler->downloadFile($serverUrl . $fileName);

            if ($file) {
                $archiveFullFileName = $destinationFolder . $fileName;

                $this->filesystem->dumpFile($archiveFullFileName, $file);

                $this->fileHandler->uncompress($destinationFolder, $fileName, true, self::UNWRAP_PATH);

                $this->filesystem->remove($archiveFullFileName);
            }
        }

        return 0;
    }

    /**
     * @param string $basePath
     * @param array $toBackup
     *
     * @return array
     */
    private function findFilesToBackup(string $basePath, array $toBackup): array
    {
        $subDirResults = [];
        $rootDirFiles = [];

        foreach ($toBackup as $relPath => $item) {
            if (is_array($item)) {
                $subDirResults[] = $this->searchFilesInFolder($basePath, $relPath, $item);
            } else {
                $rootDirFiles[] = $item;
            }
        }

        $rootDirResults = [];
        if ([] !== $rootDirFiles) {
            $rootDirResults = $this->searchFilesInFolder($basePath, '', $rootDirFiles);
        }

        return array_merge($rootDirResults, ...$subDirResults);
    }

    /**
     * @param string $basePath
     * @param string $relPath
     * @param array $toBackup
     *
     * @return array
     */
    private function searchFilesInFolder(string $basePath, string $relPath, array $toBackup): array
    {
        $patterns = [];
        $files = [];

        $relPath .= '' !== $relPath ? DIRECTORY_SEPARATOR : '';

        foreach ($toBackup as $item) {
            if (false !== strpos($item, '*')) {
                $patterns[] = $item;
            } else {
                $files[] = $relPath . $item;
            }
        }

        $dir = $basePath . DIRECTORY_SEPARATOR . $relPath;

        if (!is_dir($dir)) {
            return $files;
        }

        $finder = new Finder();
        $finder->in($dir)->name($patterns)->depth('== 0')->files();
        foreach ($finder as $item) {
            $files[] = str_replace($basePath . DIRECTORY_SEPARATOR, '', $item->getPathname());
        }

        return $files;
    }
}
