<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Service\RefreshBrowsercache\RefreshBrowsercacheHash;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class RefreshBrowsercacheHashCommand extends Command
{
    private const FRONTENDGUI_INDEX_PATH = '/index.html';
    private const ADMINGUI_INDEX_PATH = '/admin/index.html';

    protected static $defaultName = 'configuratorware:refresh-browsercache-hash';

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var RefreshBrowsercacheHash
     */
    private $refreshBrowsercacheHash;

    /**
     * @var string
     */
    private $webDocumentRoot;

    /**
     * @param Filesystem $fs
     * @param string $webDocumentRoot
     */
    public function __construct(Filesystem $fs, RefreshBrowsercacheHash $refreshBrowsercacheHash, string $webDocumentRoot)
    {
        parent::__construct();
        $this->fs = $fs;
        $this->refreshBrowsercacheHash = $refreshBrowsercacheHash;
        $this->webDocumentRoot = $webDocumentRoot;
    }

    protected function configure(): void
    {
        $this->setDescription('Refresh the version hashes in index.html for frontendgui and admingui.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $frontendIndexPath = $this->webDocumentRoot . self::FRONTENDGUI_INDEX_PATH;
        $adminIndexPath = $this->webDocumentRoot . self::ADMINGUI_INDEX_PATH;
        $hash = uniqid();

        foreach ([$frontendIndexPath, $adminIndexPath] as $indexPath) {
            if (!file_exists($indexPath)) {
                continue;
            }

            $indexHtmlContent = file_get_contents($indexPath);

            $this->fs->dumpFile($indexPath, $this->refreshBrowsercacheHash->refresh($indexHtmlContent, $hash));
        }

        $io->success('Hash was successfully updated.');

        return 0;
    }
}
