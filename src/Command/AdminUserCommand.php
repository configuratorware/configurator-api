<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Service\User\UserApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AdminUserCommand extends Command
{
    private UserApi $userApi;

    /**
     * @param UserApi $userApi
     */
    public function __construct(UserApi $userApi)
    {
        $this->userApi = $userApi;

        parent::__construct();
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this->setName('configuratorware:admin-user')
            ->setDescription('Creates an admin user or resets password.')
            ->addArgument('username', InputArgument::REQUIRED, 'The admin user\'s username')
            ->addArgument('password', InputArgument::OPTIONAL, 'The admin user\'s password');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $user = new User();

        $user->username = $input->getArgument('username');
        $user->password = $input->getArgument('password');

        $output->writeln($this->userApi->createAdminUser($user));

        return 0;
    }
}
