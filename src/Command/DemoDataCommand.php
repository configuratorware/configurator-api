<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FileHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\DesignProductionMethodApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class DemoDataCommand extends Command
{
    private const DOWNLOAD_SLUG = 'demo-items';

    private const LAYER_ARCHIVE_FILE_NAME = 'configuratorimages.tar.gz';

    private const LAYER_ARCHIVE_UNWRAP_PATH = 'configuratorimages';

    private const VALID_LICENSES = [
        'designer' => 'designer',
        'creator' => 'creator',
        'creator+designer' => 'creatordesigner',
    ];

    private const CALCULATION_TYPES = [
        'screen_printing' => [
            'printing_cost' => 'printing_costs_screen_printing',
            'setup_cost' => [
                'setup_costs_screen_printing' => [
                    ['price' => 49.00],
                ],
            ],
        ],
        'embroidery' => [
            'printing_cost' => 'printing_costs_embroidery',
            'setup_cost' => [
                'setup_costs_embroidery' => [
                    ['price' => 49.00],
                ],
            ],
        ],
        'digital_printing' => [
            'printing_cost' => 'printing_costs_digital_printing',
            'setup_cost' => [
                'setup_costs_digital_printing' => [
                    ['price' => 49.00],
                ],
            ],
        ],
    ];

    /**
     * @var string
     */
    private $configuratorImagesPath;

    /**
     * @var string
     */
    private $demoResourcePath;

    /**
     * @var DesignProductionMethodApi
     */
    private $designProductionMethodApi;

    /**
     * @var FileHandler
     */
    private $fileHandler;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var ImportApi
     */
    private $importApi;

    /**
     * @var string
     */
    private $server;
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @param ChannelRepository $channelRepository
     * @param string $configuratorImagesPath
     * @param string $demoResourcePath
     * @param DesignProductionMethodApi $designProductionMethodApi
     * @param FileHandler $fileHandler
     * @param Filesystem $fs
     * @param ImportApi $importApi
     * @param string $server
     */
    public function __construct(
        ChannelRepository $channelRepository,
        string $configuratorImagesPath,
        string $demoResourcePath,
        DesignProductionMethodApi $designProductionMethodApi,
        FileHandler $fileHandler,
        Filesystem $fs,
        ImportApi $importApi,
        string $server
    ) {
        parent::__construct('configuratorware:demo-data');
        $this->channelRepository = $channelRepository;
        $this->configuratorImagesPath = rtrim($configuratorImagesPath, DIRECTORY_SEPARATOR);
        $this->demoResourcePath = rtrim($demoResourcePath, DIRECTORY_SEPARATOR);
        $this->designProductionMethodApi = $designProductionMethodApi;
        $this->fileHandler = $fileHandler;
        $this->fs = $fs;
        $this->importApi = $importApi;
        $this->server = rtrim($server, DIRECTORY_SEPARATOR);
    }

    protected function configure(): void
    {
        $this->setDescription('Add demo data to the project.')
            ->addArgument(
                'license',
                InputArgument::REQUIRED,
                sprintf('The license should be one of the following: %s %s or %s', ...array_keys(self::VALID_LICENSES))
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', 'en_GB');
        }

        $io = new SymfonyStyle($input, $output);
        $licenseArg = $input->getArgument('license');

        if (!isset(self::VALID_LICENSES[$licenseArg])) {
            $io->error(sprintf('The license should be one of the following: %s %s or %s', ...array_keys(self::VALID_LICENSES)));

            return 1;
        }

        $license = self::VALID_LICENSES[$licenseArg];

        if (in_array($license, ['creatordesigner', 'designer'])) {
            $this->addCalculationTypes();
        }

        $this->importJson($license);

        if (in_array($license, ['creator', 'creatordesigner'])) {
            $this->downloadLayerImages($license);
        }

        $io->success('Demo items were created.');

        return 0;
    }

    /**
     * @param string $licence
     *
     * @throws \Doctrine\DBAL\ConnectionException
     * @throws \Doctrine\DBAL\Exception
     * @throws FileException
     */
    private function importJson(string $licence): void
    {
        $path = $this->demoResourcePath . DIRECTORY_SEPARATOR . $licence . '.json';

        if (!file_exists($path)) {
            throw FileException::notFound($path);
        }

        $this->importApi->executeImport(new ImportOptions(), $this->importApi->prepareImportArray(file_get_contents($path)));
    }

    private function addCalculationTypes(): void
    {
        $defaultChannel = $this->channelRepository->findOneByIdentifier(Channel::DEFAULT_IDENTIFIER);

        if (null === $defaultChannel) {
            return;
        }

        $designProductionMethods = $this->designProductionMethodApi->getListResult(new ListRequestArguments());

        foreach ($designProductionMethods->data as $designProductionMethod) {
            if (!isset(self::CALCULATION_TYPES[$designProductionMethod->identifier])) {
                continue;
            }

            $calculationTypeData = self::CALCULATION_TYPES[$designProductionMethod->identifier];

            // check if any calculation type for this production method was already created with a same identifier
            foreach ($designProductionMethod->calculationTypes as $existingType) {
                if ($existingType->identifier === $calculationTypeData['printing_cost']
                    || array_key_exists($existingType->identifier, $calculationTypeData['setup_cost'])) {
                    continue 2;
                }
            }

            $productionCalculationType = new DesignerProductionCalculationType();
            $productionCalculationType->identifier = $calculationTypeData['printing_cost'];
            $productionCalculationType->itemAmountDependent = true;
            $productionCalculationType->pricePerItem = true;
            $designProductionMethod->calculationTypes[] = $productionCalculationType;

            foreach ($calculationTypeData['setup_cost'] as $setupCostIdentifier => $setupCost) {
                $setupCalculationType = new DesignerProductionCalculationType();
                $setupCalculationType->identifier = $setupCostIdentifier;

                foreach ($setupCost as $priceData) {
                    $price = new DesignProductionMethodPrice();
                    $price->price = $priceData['price'];
                    $price->channel = $defaultChannel->getId();
                    $setupCalculationType->prices[] = $price;
                }

                $designProductionMethod->calculationTypes[] = $setupCalculationType;
            }

            $this->designProductionMethodApi->save($designProductionMethod);
        }
    }

    /**
     * @param string $licence
     */
    private function downloadLayerImages(string $licence): void
    {
        $destinationPath = $this->configuratorImagesPath . DIRECTORY_SEPARATOR . self::LAYER_ARCHIVE_FILE_NAME;
        $downloadUrl = $this->server . DIRECTORY_SEPARATOR . self::DOWNLOAD_SLUG . DIRECTORY_SEPARATOR . $licence . DIRECTORY_SEPARATOR . self::LAYER_ARCHIVE_FILE_NAME;

        $file = $this->fileHandler->downloadFile($downloadUrl);

        if ($file) {
            $this->fs->dumpFile($destinationPath, $file);

            $this->fileHandler->uncompress(
                $this->configuratorImagesPath,
                self::LAYER_ARCHIVE_FILE_NAME,
                false,
                self::LAYER_ARCHIVE_UNWRAP_PATH
            );

            $this->fs->remove($destinationPath);
        }
    }
}
