<?php

namespace Redhotmagma\ConfiguratorApiBundle\Command;

use Redhotmagma\ConfiguratorApiBundle\Entity\Credential;
use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential;
use Redhotmagma\ConfiguratorApiBundle\Repository\CredentialRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseApi;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseChecker;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddCredentialsCommand extends Command
{
    private CredentialRepository $credentialRepository;

    private RoleRepository $roleRepository;

    private array $credentials;

    private LicenseApi $licenseApi;

    private LicenseChecker $licenseChecker;

    public function __construct(
        CredentialRepository $credentialRepository,
        LicenseApi $licenseApi,
        RoleRepository $roleRepository,
        LicenseChecker $licenseChecker,
        array $credentials
    ) {
        parent::__construct();
        $this->credentialRepository = $credentialRepository;
        $this->licenseApi = $licenseApi;
        $this->roleRepository = $roleRepository;
        $this->licenseChecker = $licenseChecker;
        $this->credentials = $credentials;
    }

    protected function configure(): void
    {
        $this
            ->setName('configuratorware:add-credentials')
            ->setDescription('Reads a YML to create new credentials')
            ->addArgument('license', InputArgument::OPTIONAL, 'The license used for the configurator');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $licenseProduct = $input->getArgument('license');

        if (null === $licenseProduct) {
            $licenseProduct = $this->licenseApi->getValidProduct();
        }

        try {
            $license = $this->checkValidLicenses((string)$licenseProduct);
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());

            return 0;
        }

        $roles = $this->getRoles();

        foreach ($this->credentials as $credential => $details) {
            if (null !== $this->credentialRepository->findOneBy(['area' => $credential]) ||
                !$this->isLicenseValidForCredential($license, $details['configuration_mode'])) {
                continue;
            }

            $credentialEntity = (new Credential())->setArea($credential);

            foreach ($details['role'] as $role) {
                $roleCredential = new RoleCredential();

                $roleCredential->setCredential($credentialEntity);
                $roleCredential->setRole($roles[$role]);

                $credentialEntity->addRoleCredential($roleCredential);
            }

            $this->credentialRepository->save($credentialEntity);
        }

        return 0;
    }

    /**
     * @throws \Exception
     */
    private function checkValidLicenses(string $license): array
    {
        if (!$this->licenseChecker->isValidProductCombination($license)) {
            throw new \Exception('License is not valid. Expected combination of following parts, separated by \'+\': \'designer\', \'creator\', \'finder\'');
        }

        return explode('+', $license);
    }

    /**
     * @return array
     */
    private function getRoles(): array
    {
        return [
            Role::ADMIN_ROLE => $this->roleRepository->findOneBy(['role' => Role::ADMIN_ROLE]),
            Role::CLIENT_ROLE => $this->roleRepository->findOneBy(['role' => Role::CLIENT_ROLE]),
            Role::CONNECTOR_ROLE => $this->roleRepository->findOneBy(['role' => Role::CONNECTOR_ROLE]),
        ];
    }

    private function isLicenseValidForCredential(array $license, array $configuration_mode): bool
    {
        return count(array_intersect($license, $configuration_mode)) > 0;
    }
}
