<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\Query;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * Class DesignProductionMethodRepository.
 *
 * @method Item findOneByIdentifier(string $identifier)
 */
class DesignProductionMethodRepository extends Repository
{
    /**
     * @param array $itemIds
     *
     * @return array
     */
    public function getMappedColorPaletteIdsByItems(array $itemIds): array
    {
        return $this->createQueryBuilder('a')
            ->select('DISTINCT a.id mappingId, IDENTITY(b.colorPalette) structureId')
            ->join(DesignProductionMethodColorPalette::class, 'b', 'WITH', 'b.designProductionMethod = a.id')
            ->join(
                DesignAreaDesignProductionMethod::class,
                'c',
                'WITH',
                'c.designProductionMethod = b.designProductionMethod'
            )
            ->join(
                DesignArea::class,
                'd',
                'WITH',
                'd.id = c.designArea'
            )
            ->andWhere('d.item IN (:ids)')
            ->setParameter('ids', $itemIds)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }
}
