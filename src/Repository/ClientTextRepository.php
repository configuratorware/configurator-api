<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientText;

/**
 * @method ClientText findOneBy(array $criteria, array $orderBy = null)
 */
class ClientTextRepository extends Repository
{
}
