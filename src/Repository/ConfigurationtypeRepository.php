<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype;

/**
 * ConfigurationtypeRepository.
 *
 * @method Configurationtype findOneByIdentifier(string $identifier)
 */
class ConfigurationtypeRepository extends Repository
{
    public const DEFAULT_OPTIONS = 'defaultoptions';
    public const PARTSLIST = 'partslist';
    public const CART = 'cart';
    public const USER = 'user';

    /**
     * @return Configurationtype
     */
    public function getDefaultType()
    {
        $configurationType = $this->findOneByIdentifier(self::DEFAULT_OPTIONS);

        return $configurationType;
    }
}
