<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Redhotmagma\ApiBundle\Repository\Repository;

/**
 * ItempriceRepository.
 */
class OptionPriceRepository extends Repository
{
    /**
     * get the item price for an option by identifier.
     *
     * @param string $identifier
     * @param string $channel
     * @param int $amountFrom
     *
     * @return array
     */
    public function getOptionPriceByIdentifier($identifier, $channel, $amountFrom = 1): array
    {
        $return = [];
        $return['price'] = 0.0;
        $return['netPrice'] = 0.0;

        $queryBuilder = $this->createQueryBuilder('OptionPrice');
        $queryBuilder->select('OptionPrice.price, OptionPrice.price_net')
            ->join(
                'OptionPrice.option',
                'Option',
                'WITH',
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('Option.identifier', ':identifier'),
                    $queryBuilder->expr()->eq('OptionPrice.amountfrom', ':amountFrom')
                )
            )
            ->join(
                'OptionPrice.channel',
                'Channel',
                'WITH',
                $queryBuilder->expr()->eq('Channel.identifier', ':channelIdentifier')
            )
            ->setParameters([
                'identifier' => $identifier,
                'amountFrom' => $amountFrom,
                'channelIdentifier' => $channel,
            ]);

        try {
            $result = $queryBuilder->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $result = null;
        }

        if (!is_null($result)) {
            $return['price'] = (float)$result['price'];
            $return['netPrice'] = (float)$result['price_net'];
        }

        return $return;
    }
}
