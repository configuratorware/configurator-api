<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;

/**
 * @method ItemOptionclassificationOption findOneBy(array $criteria, ?array $orderBy = null)
 */
class ItemOptionclassificationOptionRepository extends Repository
{
}
