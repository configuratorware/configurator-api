<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette;

class ColorPaletteRepository extends Repository
{
    public function findOneById(int $id): ?ColorPalette
    {
        $qb = $this->createQueryBuilder('colorPalette');
        $expr = $qb->expr();

        $query = $qb->addSelect('color')
            ->leftJoin('colorPalette.color', 'color')
            ->orderBy('color.sequence_number')
            ->where($expr->eq('colorPalette.id', ':id'))
            ->setParameter('id', $id)
            ->getQuery();

        try {
            return $query->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // There shouldn't be a non-unique result when searching by id.
            return null;
        }
    }

    /**
     * @param array|null $filter
     *
     * @return array
     */
    public function getByDesignProductionMethods(
        array $filter = null
    ): array {
        $query = $this->createQueryBuilder('a', 'a.id')
            ->select('a')
            ->join(DesignProductionMethodColorPalette::class, 'b', 'WITH', 'b.colorPalette = a.id');

        if ($filter) {
            $query = $query->andWhere('b.designProductionMethod IN (:filter)')
                ->setParameter('filter', $filter);
        }

        $query = $query->getQuery()
            ->getResult();

        return $query;
    }
}
