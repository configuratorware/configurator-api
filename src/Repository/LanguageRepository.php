<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;

/**
 * LanguageRepository.
 *
 * @method Language findOneByIso(string $iso)
 * @method Language findOneById(int $id);
 * @method Language findOneBy([] $array);
 * @method ArrayCollection|Language[] findAll()
 */
class LanguageRepository extends Repository
{
    /**
     * returns a language as an array based on its iso (e.g. en_GB).
     *
     * @param string $iso
     *
     * @return array|null
     *
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     */
    public function findOneByIsoAsArray($iso)
    {
        $languageArray = null;

        $queryBuilder = $this->createQueryBuilder('language');

        $queryBuilder->where(
            $queryBuilder->expr()->eq('language.iso', ':iso')
        );

        $queryBuilder->setParameter('iso', $iso);

        $query = $queryBuilder->getQuery();

        $result = $query->getResult(Query::HYDRATE_ARRAY);

        if (!empty($result)) {
            $languageArray = $result[0];

            // only allow scalars because we want to set this in a global constant
            foreach ($languageArray as $i => $value) {
                if (!is_scalar($value)) {
                    unset($languageArray[$i]);
                }
            }
        }

        return $languageArray;
    }

    public function findAllIsoMapped(): array
    {
        $all = $this->findAll();

        $map = [];
        foreach ($all as $language) {
            $map[$language->getIso()] = $language;
        }

        return $map;
    }

    public function findDefault(): Language
    {
        return $this->findOneBy(['is_default' => true]);
    }
}
