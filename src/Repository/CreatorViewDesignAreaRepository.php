<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;

class CreatorViewDesignAreaRepository extends Repository
{
    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    public function findByItemIdentifier(string $itemIdentifier): array
    {
        $queryBuilder = $this->createQueryBuilder($this->getNormalizedEntityName());
        $queryBuilder
            ->join('creatorviewdesignarea.creatorView', 'creatorView')
            ->leftJoin('creatorView.item', 'creatorViewItem')
            ->join('creatorviewdesignarea.designArea', 'designArea')
            ->leftJoin('designArea.item', 'designAreaItem')
            ->where($queryBuilder->expr()->eq('creatorViewItem.identifier', ':itemIdentifier'))
            ->where($queryBuilder->expr()->eq('designAreaItem.identifier', ':itemIdentifier'));

        return $queryBuilder->setParameter('itemIdentifier', $itemIdentifier)
            ->getQuery()
            ->getResult();
    }
}
