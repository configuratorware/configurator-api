<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ConfigurationMode;

/**
 * @method ConfigurationMode findOneBy(array $criteria, array $orderBy = null)
 * @method ConfigurationMode findOneByIdentifier(string $identifier)
 * @method ConfigurationMode[] findAll()
 */
class ConfigurationModeRepository extends Repository
{
}
