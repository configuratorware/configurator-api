<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;

/**
 * Class ClientRepository.
 *
 * @method Client findOneByIdentifier(string $identifier)
 * @method Client findOneBy(array $criteria, array $orderBy = null)
 * @method Client findOneById(int $id)
 */
class ClientRepository extends Repository
{
    public const DEFAULT_CLIENT_IDENTIFIER = '_default';

    protected function addFiltersToQuery($queryBuilder, $queryParts, $parameters)
    {
        // _no_default_filter, do not select the _default client
        if (!empty($parameters['filters'])
            && !empty($parameters['filters']['_no_default_filter'])
            && true === $parameters['filters']['_no_default_filter']
        ) {
            $queryParts[] = $queryBuilder->expr()->neq('client.identifier', ':defaultClientIdentifier');
            $queryBuilder->setParameter('defaultClientIdentifier', self::DEFAULT_CLIENT_IDENTIFIER);

            unset($parameters['filters']['_no_default_filter']);
        }

        $queryParts = parent::addFiltersToQuery($queryBuilder, $queryParts, $parameters);

        return $queryParts;
    }
}
