<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;

/**
 * AssemblypointRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AssemblypointRepository extends Repository
{
    /**
     * returns a list of assemblypoints for a specific option.
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param $optionId
     *
     * @return array
     */
    public function getByOptionId($optionId)
    {
        $entityname = $this->getNormalizedEntityName();
        $querybuilder = $this->createQueryBuilder($entityname);
        $querybuilder->leftJoin('assemblypoint.option', 'option');
        $querybuilder->leftJoin('assemblypoint.assemblypointimageelement', 'assemblypointimageelement');
        $querybuilder->leftJoin('assemblypointimageelement.optionclassification', 'optionclassification');

        $querybuilder->where($querybuilder->expr()->eq('option', ':optionId'));

        $querybuilder->setParameter('optionId', $optionId);

        $query = $querybuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }
}
