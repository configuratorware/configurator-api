<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemPool;

class ItemPoolRepository extends Repository
{
    /**
     * get a list of item groups related to the given list of item ids
     * indexed by the ite is.
     *
     * @param array $itemIds
     *
     * @return array
     */
    public function getRelatedConfigurationVariantItemGroupsForItemIds($itemIds)
    {
        $result = [];

        $itemIds = array_filter($itemIds);

        if (!empty($itemIds)) {
            $entityName = $this->getNormalizedEntityName();
            $queryBuilder = $this->createQueryBuilder($entityName);

            $queryBuilder->select('item.id AS item_id');
            $queryBuilder->from(ItemItemPool::class, 'ItemItemPool');
            $queryBuilder->addSelect('item.identifier AS item_identifier');
            $queryBuilder->addSelect('itemPool.identifier AS item_pool_identifier');

            $queryBuilder->leftJoin('ItemItemPool.item', 'item');
            $queryBuilder->leftJoin('ItemItemPool.itemPool', 'itemPool');

            $queryBuilder->where($queryBuilder->expr()->in('item.id', ':item_ids'));

            $queryBuilder->setParameter('item_ids', $itemIds);

            $query = $queryBuilder->getQuery();
            $resultRows = $query->getResult();

            foreach ($resultRows as $row) {
                $result[$row['item_id']] = $row;
            }
        }

        return $result;
    }
}
