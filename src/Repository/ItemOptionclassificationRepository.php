<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;

/**
 * ItemOptionclassificationRepository.
 *
 * @method ItemOptionclassification findOneByOptionclassification(Optionclassification $optionClassification)
 */
class ItemOptionclassificationRepository extends Repository
{
}
