<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;

/**
 * @method VisualizationMode findOneById(int $id)
 */
class VisualizationModeRepository extends Repository
{
}
