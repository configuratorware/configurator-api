<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\AbstractQuery;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;

/**
 * @method Inspiration find($id, $lockMode = null, $lockVersion = null)
 */
class InspirationRepository extends Repository
{
    /**
     * @param int[] $itemIds
     *
     * @return array<int, int>
     */
    public function countPerItem(array $itemIds): array
    {
        $queryBuilder = $this->createQueryBuilder('Inspiration');

        $assocs = $queryBuilder->select('IDENTITY(Inspiration.item) as itemId, count(Inspiration.id) as count')
            ->where($queryBuilder->expr()->in('Inspiration.item', ':itemIds'))
            ->setParameter('itemIds', $itemIds)
            ->groupBy('Inspiration.item')
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);

        $map = [];
        foreach ($assocs as $assoc) {
            $map[$assoc['itemId']] = $assoc['count'];
        }

        return $map;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return Inspiration[]
     */
    public function fetchActiveByItemIdentifier(string $itemIdentifier): array
    {
        $queryBuilder = $this->createQueryBuilder('Inspiration');
        $expr = $queryBuilder->expr();

        return $queryBuilder
            ->join('Inspiration.item', 'Item')
            ->andWhere($expr->eq('Inspiration.active', true))
            ->andWhere($expr->eq('Item.identifier', ':itemIdentifier'))
            ->setParameter('itemIdentifier', $itemIdentifier)
            ->getQuery()
            ->getResult();
    }
}
