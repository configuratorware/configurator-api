<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Color;

/**
 * @method Color findOneBy(array $criteria, array $orderBy = null)
 * @method Color findOneById(int $id)
 */
class ColorRepository extends Repository
{
    /**
     * @param string $colorPaletteIdentifier
     * @param string $colorIdentifier
     *
     * @return Color|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @deprecated should not be used, use findColorsInAllPalettes and select the proper color from the result
     */
    public function findOneByColorPaletteAndColorIdentifier(
        string $colorPaletteIdentifier,
        string $colorIdentifier
    ): ?Color {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder->join(
            $entityName . '.colorPalette',
            'palette',
            'WITH',
            $queryBuilder->expr()->eq('palette.identifier', ':colorPaletteIdentifier')
        );

        $queryBuilder->andWhere($entityName . '.identifier = :colorIdentifier');

        $queryBuilder->setParameter('colorPaletteIdentifier', $colorPaletteIdentifier);
        $queryBuilder->setParameter('colorIdentifier', $colorIdentifier);

        $query = $queryBuilder->getQuery();

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param array<string> $colorIdentifiers
     *
     * @return array<string, array<string, Color>>
     */
    public function findColorsInAllPalettes(array $colorIdentifiers): array
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $result = $queryBuilder
            ->join($entityName . '.colorPalette', 'palette')
            ->andWhere($entityName . '.identifier IN (:colorIdentifiers)')
            ->setParameter('colorIdentifiers', $colorIdentifiers)
            ->getQuery()
            ->getResult();

        $map = [];
        foreach ($result as $item) {
            $map[$item->getColorPalette()->getIdentifier()][$item->getIdentifier()] = $item;
        }

        return $map;
    }
}
