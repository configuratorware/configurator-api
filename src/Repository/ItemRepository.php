<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ItemClassificationFilter;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;

/**
 * @method Item find(int $id)
 * @method Item findOneBy(array $array)
 * @method Item findOneById(int $id)
 * @method Item findOneByIdentifier(string $identifier)
 * @method Item findOneByExternalid(string $externalId)
 * @method list<Item> findAll()
 */
class ItemRepository extends Repository
{
    public function findOneForConfigurationMode(string $identifier)
    {
        $qb = $this->createQueryBuilder('item');
        $expr = $qb->expr();

        $query = $qb->addSelect(['designArea', 'designAreaDesignProductionMethod', 'designProductionMethod'])
            ->addSelect(['designProductionMethodColorPalette', 'colorPalette', 'color'])
            ->leftJoin('item.designArea', 'designArea')
            ->leftJoin('designArea.designAreaDesignProductionMethod', 'designAreaDesignProductionMethod')
            ->leftJoin('designAreaDesignProductionMethod.designProductionMethod', 'designProductionMethod')
            ->leftJoin('designProductionMethod.designProductionMethodColorPalette', 'designProductionMethodColorPalette')
            ->leftJoin('designProductionMethodColorPalette.colorPalette', 'colorPalette')
            ->leftJoin('colorPalette.color', 'color')
            ->where($expr->eq('item.identifier', ':identifier'))
            ->setParameter('identifier', $identifier)
            ->orderBy('color.sequence_number')
            ->getQuery();

        try {
            return $query->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            // identifier is unique, so it shouldn't throw this exception
            return null;
        }
    }

    /**
     * fetch a list of base items to which the given option is connected via construction pattern.
     *
     * @param int $optionId
     *
     * @return  array
     */
    public function fetchBaseItemsByOptionId($optionId)
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->leftJoin('item.itemOptionclassification', 'itemOptionclassification');
        $queryBuilder->leftJoin('itemOptionclassification.itemOptionclassificationOption',
            'itemOptionclassificationOption');

        $queryBuilder->where(
            $queryBuilder->expr()->eq('itemOptionclassificationOption.option', ':option')
        );

        $queryBuilder->setParameter('option', $optionId);

        $query = $queryBuilder->getQuery();

        $baseItems = $query->getResult();

        return $baseItems;
    }

    /**
     * Returns a list of items based on the parent identifier
     * the parent identifier can be null for the top level.
     *
     * @param string $parentIdentifier
     *
     * @return Item[]
     */
    public function getItemsByParent($parentIdentifier = '')
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->leftJoin('item.parent', 'parent');

        if (empty($parentIdentifier)) {
            $conditions = $queryBuilder->expr()->andX(
                $queryBuilder->expr()->isNull('parent.identifier')
            );
        } else {
            $conditions = $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('parent.identifier', ':parentidentifier')
            );

            $queryBuilder->setParameter('parentidentifier', $parentIdentifier);
        }

        $queryBuilder->where(
            $conditions
        );

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    public function getOverrideOptionOrderForItem(string $itemIdentifier): bool
    {
        $entityName = $this->getNormalizedEntityName();
        $qb = $this->createQueryBuilder($entityName);

        $qb->select('item.overrideOptionOrder')
            ->where($qb->expr()->eq('item.identifier', ':itemIdentifier'))
            ->setParameter('itemIdentifier', $itemIdentifier);

        $result = $qb->getQuery()->getArrayResult();

        return !empty($result) && $result[0]['overrideOptionOrder'];
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $querybuilder
     * @param array $queryparts
     * @param array $searchablefields
     * @param array $parameters
     *
     * @return array
     */
    protected function addSearchToQuery($querybuilder, $queryparts, $searchablefields, $parameters)
    {
        $queryParts = parent::addSearchToQuery(
            $querybuilder, $queryparts, $searchablefields, $parameters
        );

        // since we cannot auto join the children we manually join and add the search conditions
        $childQueryParts = [];
        if (!empty($parameters['query'])) {
            $splitQuery = explode(',', $parameters['query']);

            $querybuilder->leftJoin(Item::class, 'child', 'WITH', 'child.parent = item.id');
            $querybuilder->leftJoin('child.itemtext', 'childItemText');

            foreach ($splitQuery as $key => $value) {
                $queryBindParam = ':_query' . ($key > 0 ? '_' . $key : '');
                $childQueryParts[] = $querybuilder->expr()->like('child.identifier', $queryBindParam);
                $childQueryParts[] = $querybuilder->expr()->like('childItemText.title', $queryBindParam);
            }

            $childQuery = call_user_func_array([$querybuilder->expr(), 'orX'], $childQueryParts);

            // take the last element from the $queryParts since this is the search queries
            // created from the searchable fields and link them with an or condition to our child query parts
            $searchQuery = array_pop($queryParts);
            $queryParts[] = $querybuilder->expr()->orX($searchQuery, $childQuery);
        }

        return $queryParts;
    }

    /**
     * Only get options or items depending on filtering.
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     * @param array $queryParts
     * @param array $parameters
     *
     * @return  array
     */
    protected function addFiltersToQuery($queryBuilder, $queryParts, $parameters)
    {
        // parent filter, top level only -> only select items have no parents
        if (!empty($parameters['filters'])
            && !empty($parameters['filters']['_parent_filter'])
            && 'top_level_only' == $parameters['filters']['_parent_filter']
        ) {
            $queryParts[] = $queryBuilder->expr()->isNull('item.parent');

            unset($parameters['filters']['_parent_filter']);
        }

        // parent filter, sub_level_only -> only select items that have a parent or have no children
        if (!empty($parameters['filters'])
            && !empty($parameters['filters']['_parent_filter'])
            && 'sub_level_only' == $parameters['filters']['_parent_filter']
        ) {
            // only join the child if it was not already joined from somewhere else
            if (!in_array('child', $queryBuilder->getAllAliases())) {
                $queryBuilder->leftJoin(Item::class, 'child', 'WITH', 'child.parent = item.id');
            }

            $queryParts[] = $queryBuilder->expr()->orX(
                $queryBuilder->expr()->isNotNull('item.parent'),
                $queryBuilder->expr()->isNull('child.id')
            );

            unset($parameters['filters']['_parent_filter']);
        }

        $queryParts = parent::addFiltersToQuery($queryBuilder, $queryParts, $parameters);

        return $queryParts;
    }

    /**
     * @return Item[]
     */
    public function findChildrenByGroupEntry(int $parentItemId, int $groupEntryId): array
    {
        $queryBuilder = $this->createQueryBuilder('item');
        $expr = $queryBuilder->expr();

        return $queryBuilder->select('item')
            ->addSelect('itemItemgroupentry')
            ->leftJoin('item.parent', 'parent')
            ->leftJoin('item.itemItemgroupentry', 'itemItemgroupentry')
            ->andWhere($expr->eq('parent.id', ':itemId'))
            ->andWhere($expr->eq('itemItemgroupentry.itemgroupentry', ':groupEntryId'))
            ->setParameter('itemId', $parentItemId)
            ->setParameter('groupEntryId', $groupEntryId)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string[] $itemIdentifiers
     *
     * @return Item[]
     */
    public function findByIdentifiersIndexed(array $itemIdentifiers, int $limit = 10000): array
    {
        return $this->createQueryBuilder('item', 'item.identifier')
            ->andWhere('item.identifier IN (:items)')
            ->setParameter('items', $itemIdentifiers)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findItemByIdWithOptionClassifications(int $id): ?Item
    {
        $queryBuilder = $this->createQueryBuilder('item');
        $expr = $queryBuilder->expr();

        return $queryBuilder->select('item')
            ->addSelect('itemOptionclassification')
            ->addSelect('optionclassification')
            ->leftJoin('item.itemOptionclassification', 'itemOptionclassification')
            ->leftJoin('itemOptionclassification.optionclassification', 'optionclassification')
            ->leftJoin('itemOptionclassification.itemOptionclassificationOption', 'itemOptionclassificationOption')
            ->leftJoin('itemOptionclassificationOption.option', 'option')
            ->andWhere($expr->eq('item.id', ':itemId'))
            ->addOrderBy('optionclassification.sequencenumber', Criteria::ASC)
            ->addOrderBy('optionclassification.identifier', Criteria::ASC)
            ->addOrderBy('optionclassification.id', Criteria::ASC)
            ->setParameter('itemId', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function findItemByIdentifierWithOptionClassificationsAndOptions(string $itemIdentifier): ?Item
    {
        $queryBuilder = $this->createQueryBuilder('item');
        $expr = $queryBuilder->expr();

        $queryBuilder->select('item')
            ->addSelect('itemOptionclassification')
            ->addSelect('optionclassification')
            ->addSelect('itemOptionclassificationOption')
            ->addSelect('option')
            ->leftJoin('item.itemOptionclassification', 'itemOptionclassification')
            ->leftJoin('itemOptionclassification.optionclassification', 'optionclassification')
            ->leftJoin('itemOptionclassification.itemOptionclassificationOption', 'itemOptionclassificationOption')
            ->leftJoin('itemOptionclassificationOption.option', 'option')
            ->andWhere($expr->orX(
                $expr->eq('option.deactivated', '0'),
                $expr->isNull('option.deactivated')
            ))
            ->andWhere($expr->eq('item.identifier', ':itemIdentifier'))
            ->addOrderBy('optionclassification.sequencenumber', Criteria::ASC)
            ->addOrderBy('optionclassification.identifier', Criteria::ASC)
            ->addOrderBy('optionclassification.id', Criteria::ASC)
            ->setParameter('itemIdentifier', $itemIdentifier)
            ->addSelect('optionClassificationAttribute')
            ->addSelect('attribute')
            ->addSelect('attributevalue')
            ->addSelect('attributetext')
            ->addSelect('attributevaluetranslation')
            ->leftJoin('optionclassification.optionClassificationAttribute', 'optionClassificationAttribute')
            ->leftJoin('optionClassificationAttribute.attribute', 'attribute')
            ->leftJoin('optionClassificationAttribute.attributevalue', 'attributevalue')
            ->leftJoin(
                'attribute.attributetext',
                'attributetext',
                Join::WITH,
                $expr->eq('attributetext.language', ':language'))
            ->leftJoin(
                'attributevalue.attributevaluetranslation',
                'attributevaluetranslation',
                Join::WITH,
                $expr->eq('attributevaluetranslation.language', ':language'))
            ->addSelect('optionAttribute')
            ->addSelect('attribute2')
            ->addSelect('attributevalue2')
            ->addSelect('attributetext2')
            ->addSelect('attributevaluetranslation2')
            ->leftJoin('option.optionAttribute', 'optionAttribute')
            ->leftJoin('optionAttribute.attribute', 'attribute2')
            ->leftJoin('optionAttribute.attributevalue', 'attributevalue2')
            ->leftJoin(
                'attribute2.attributetext',
                'attributetext2',
                Join::WITH,
                $expr->eq('attributetext2.language', ':language'))
            ->leftJoin(
                'attributevalue2.attributevaluetranslation',
                'attributevaluetranslation2',
                Join::WITH,
                $expr->eq('attributevaluetranslation2.language', ':language'))
            ->addSelect('optionPrice')
            ->leftJoin('option.optionPrice', 'optionPrice')
            ->leftJoin('optionPrice.channel', 'channel', Join::WITH, $expr->eq('channel.identifier', ':channel'));

        if (true === $this->getOverrideOptionOrderForItem($itemIdentifier)) {
            $queryBuilder->addOrderBy('itemOptionclassificationOption.sequenceNumber', Criteria::ASC);
        } else {
            $queryBuilder->addOrderBy('option.sequencenumber', Criteria::ASC);
        }
        $languageSettings = C_LANGUAGE_SETTINGS;
        $queryBuilder->setParameter('language', $languageSettings['id']);
        $queryBuilder->setParameter('channel', C_CHANNEL);

        return $queryBuilder->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get list of active Items.
     *
     * @return Item[]
     */
    public function getFrontendList(
        ItemClassificationFilter $itemClassificationFilter,
        ListRequestNoDefaultArguments $arguments
    ): array {
        $qb = $this->getFrontendListQuery($itemClassificationFilter);

        $this->applyPagination($qb, $arguments);

        return $qb->getQuery()->getResult();
    }

    /**
     * Get count of active Items.
     */
    public function fetchFrontendListCount(ItemClassificationFilter $itemClassificationFilter): int
    {
        $qb = $this->getFrontendListQuery($itemClassificationFilter);
        $qb->select($qb->expr()->count('item'));

        return (int)$qb->getQuery()->getSingleScalarResult();
    }

    public function findActiveByParent(string $parentIdentifier): array
    {
        $qb = $this->createQueryBuilder('Item');
        $expr = $qb->expr();
        /** @psalm-suppress TooManyArguments */
        $qb->leftJoin('Item.itemStatus', 'ItemStatus')
            ->join('Item.parent', 'ParentItem')
            ->where($expr->orX($expr->eq('ItemStatus.item_available', ':true'), $expr->isNull('ItemStatus.id')))
            ->andWhere($expr->eq('ParentItem.identifier', ':parentIdentifier'))
            ->setParameter('true', true)
            ->setParameter('parentIdentifier', $parentIdentifier);

        return $qb->getQuery()->getResult();
    }

    private function getFrontendListQuery(ItemClassificationFilter $itemClassificationFilter): QueryBuilder
    {
        $entityName = $this->getNormalizedEntityName();
        $qb = $this->createQueryBuilder($entityName);
        $qb->leftJoin('item.parent', 'parent')
            ->where($qb->expr()->eq('item.deactivated', ':deactivated'))
            ->andWhere($qb->expr()->isNull('item.parent'))
            ->setParameter('deactivated', false);

        if ($itemClassificationFilter->containFilterValues()) {
            $itemClassificationFilter->applyTo($qb);
        }

        return $qb;
    }

    private function applyPagination(QueryBuilder $qb, ListRequestNoDefaultArguments $arguments): void
    {
        if ($arguments->limit) {
            $qb->setMaxResults($arguments->limit)
                ->setFirstResult($arguments->offset);
        }
    }
}
