<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\Query;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;

/**
 * SettingRepository.
 *
 * @method Setting findOneBy(array $criteria)
 * @method Setting[] findAll()
 */
class SettingRepository extends Repository
{
    public const DELTA_PRICES = 'deltaprices';
    public const SUM_OF_ALL = 'sumofall';

    /**
     * get the settings for the configurator.
     *
     * @return Setting|null
     */
    public function getSetting(): ?Setting
    {
        return $this->find(1);
    }

    /**
     * get the calculation method.
     *
     * @return string|null
     */
    public function getCalculationMethod(): ?string
    {
        $result = null;
        $setting = $this->getSetting();
        if (null !== $setting) {
            $result = $setting->getCalculationmethod();
        }

        return $result;
    }

    /**
     * Returns the settings as an array.
     *
     * @return array
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     */
    public function getSettingAsArray(): array
    {
        $return = [];

        $entityname = $this->getNormalizedEntityName();

        $querybuilder = $this->createQueryBuilder($entityname);

        $query = $querybuilder->getQuery();

        $result = $query->getResult(Query::HYDRATE_ARRAY);

        // remove default fields
        if (!empty($result)) {
            $defaultFields = [
                'id',
                'date_created',
                'date_updated',
                'date_deleted',
                'user_created_id',
                'user_updated_id',
                'user_deleted_id',
            ];

            foreach ($result[0] as $field => $value) {
                if (in_array($field, $defaultFields)) {
                    unset($result[0][$field]);
                }
            }

            $return = $result[0];
        }

        return $return;
    }
}
