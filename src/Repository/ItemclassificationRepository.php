<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;

/**
 * ItemclassificationRepository.
 *
 * @method Itemclassification findOneByIdentifier(string $identifier)
 */
class ItemclassificationRepository extends Repository
{
    /**
     * fetch item classifications by an attribute + value.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.81
     *
     * @version 1.0.81
     *
     * @param   string $attributeIdentifier
     * @param   string $attributeValue
     *
     * @return  array
     */
    public function getByAttribute($attributeIdentifier, $attributeValue)
    {
        $entityname = $this->getNormalizedEntityName();
        $querybuilder = $this->createQueryBuilder($entityname);
        $querybuilder->leftJoin('itemclassification.itemclassificationAttribute', 'itemclassificationAttribute');
        $querybuilder->leftJoin('itemclassificationAttribute.attribute', 'attribute');
        $querybuilder->leftJoin('itemclassificationAttribute.attributevalue', 'attributevalue');

        $querybuilder->where($querybuilder->expr()->andX(
            $querybuilder->expr()->eq('attribute.identifier', ':attributeIdentifier'),
            $querybuilder->expr()->eq('attributevalue.value', ':attributeValue')
        ));

        $querybuilder->setParameter('attributeIdentifier', $attributeIdentifier);
        $querybuilder->setParameter('attributeValue', $attributeValue);

        $query = $querybuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * Fetch Itemclassification entities having at least one active item associated to it.
     *
     * @return Itemclassification[]
     */
    public function findAllHavingActiveItems(): array
    {
        $entityname = $this->getNormalizedEntityName();
        $qb = $this->createQueryBuilder($entityname);
        $qb->join('itemclassification.item_itemclassification', 'item_itemclassification')
            ->leftJoin('item_itemclassification.item', 'item')
            ->where($qb->expr()->eq('item.deactivated', ':deactivated'))
            ->setParameter('deactivated', false)
            ->orderBy('itemclassification.sequencenumber', Criteria::ASC);

        return $qb->getQuery()->getResult();
    }

    /**
     * add custom filter to seperate itemclassifications and optionclassification.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   \Doctrine\ORM\QueryBuilder $querybuilder
     * @param   array $queryparts
     * @param   array $parameters
     *
     * @return  array
     */
    protected function addFiltersToQuery($querybuilder, $queryparts, $parameters)
    {
        if (!empty($parameters['filters'])) {
            // handle parent_id
            if (array_key_exists('_parent_id', $parameters['filters'])) {
                if (null === $parameters['filters']['_parent_id']) {
                    $queryparts[] = $querybuilder->expr()->orX(
                        $querybuilder->expr()->isNull('itemclassification.parent_id'),
                        $querybuilder->expr()->eq('itemclassification.parent_id', 0)
                    );

                    unset($parameters['filters']['_parent_id']);
                } elseif ($parameters['filters']['_parent_id'] > 0) {
                    $queryparts[] = $querybuilder->expr()
                        ->eq('itemclassification.parent_id', ':parent_id');

                    $querybuilder->setParameter('parent_id', $parameters['filters']['_parent_id']);

                    unset($parameters['filters']['_parent_id']);
                }
            }
        }

        $parameters['filters'] = array_filter($parameters['filters']);

        $queryparts = parent::addFiltersToQuery($querybuilder, $queryparts, $parameters);

        return $queryparts;
    }

    public function getNextSequenceNumber(): int
    {
        $entityName = $this->getNormalizedEntityName();
        $qb = $this->createQueryBuilder($entityName);
        $sequenceNumber = $qb
            ->select($qb->expr()->max('itemclassification.sequencenumber'))
            ->getQuery()
            ->getSingleScalarResult();

        return (int)$sequenceNumber + 1;
    }
}
