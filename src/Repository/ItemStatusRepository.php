<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;

/**
 * @method ItemStatus findOneById(int $id)
 * @method ItemStatus findOneByIdentifier(string $identifier)
 * @method ItemStatus[] findBy(array $array)
 */
class ItemStatusRepository extends Repository
{
}
