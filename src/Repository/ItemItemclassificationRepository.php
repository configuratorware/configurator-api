<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification;

/**
 * ItemItemclassificationRepository.
 *
 * @method ItemItemclassification findOneByItemclassification(Itemclassification $itemClassification)
 */
class ItemItemclassificationRepository extends Repository
{
}
