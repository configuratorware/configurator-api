<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @method DesignView find($id, $lockMode = null, $lockVersion = null)
 */
class DesignViewRepository extends Repository
{
    /**
     * @param Item $item
     *
     * @return DesignView[]
     *
     * @throws \Exception
     */
    public function findByItemAndHasDesignAreaRelation(Item $item): array
    {
        $base = $this->getNormalizedEntityName();

        $designViews = $this->createQueryBuilder($base)
            ->leftJoin(
                DesignViewDesignArea::class,
                'DesignViewDesignArea',
                'WITH',
                "DesignViewDesignArea.designView = $base"
            )
            ->andWhere("$base.item = :item")
            ->orderBy("$base.sequence_number")
            ->setParameter('item', $item)
            ->getQuery()
            ->getResult();

        if ([] === $designViews) {
            throw new \Exception('No designViews found with relations');
        }

        return $designViews;
    }
}
