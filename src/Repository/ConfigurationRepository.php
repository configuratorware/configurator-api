<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @method Configuration findOneBy(array $array)
 * @method Configuration findOneByCode(string $code)
 */
class ConfigurationRepository extends Repository
{
    /**
     * returns the base configuration as entity via the base item's identifier.
     *
     * @param $identifier
     *
     * @return Configuration|null
     *
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     */
    public function getBaseconfigurationByItemIdentifier($identifier)
    {
        $return = null;

        $entityname = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityname);
        $queryBuilder->leftJoin('configuration.item', 'item');
        $queryBuilder->leftJoin('configuration.configurationtype', 'configurationtype');

        $queryBuilder->where($queryBuilder->expr()
            ->andX(
                $queryBuilder->expr()->eq('item.identifier', ':itemidentifier'),
                $queryBuilder->expr()->eq('configurationtype.identifier', ':configurationtypeidentifier')
            ));

        $queryBuilder->setParameter('itemidentifier', $identifier);
        $queryBuilder->setParameter('configurationtypeidentifier', ConfigurationtypeRepository::DEFAULT_OPTIONS);

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        if (!empty($result)) {
            $return = $result[0];
        }

        return $return;
    }

    /**
     * @param Item $item
     *
     * @return Configuration|null
     */
    public function findDefaultConfigurationForItem(Item $item): ?Configuration
    {
        $configuration = null;

        $queryBuilder = $this->createQueryBuilder('Configuration');

        $queryBuilder->join('Configuration.configurationtype', 'ConfigurationType')
            ->where($queryBuilder->expr()->eq('Configuration.item', ':item'))
            ->andWhere($queryBuilder->expr()->eq('ConfigurationType.identifier', ':type'))
            ->setParameters([
                'item' => $item,
                'type' => ConfigurationtypeRepository::DEFAULT_OPTIONS,
            ]);

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        if (!empty($result)) {
            $configuration = $result[0];
        }

        return $configuration;
    }

    /**
     * custom filter to only get configurations with the type user or cart.
     *
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     * @param array $queryParts
     * @param array $parameters
     *
     * @return  array
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    protected function addFiltersToQuery($queryBuilder, $queryParts, $parameters)
    {
        if (empty($parameters['filters'])
            && empty($parameters['filters']['configuration.configurationtype.identifier'])) {
            $queryBuilder->join('configuration.configurationtype', 'configurationtype');
            $queryParts[] = $queryBuilder->expr()->in('configurationtype.identifier', ['user', 'cart']);
        }

        // filter by client
        if (isset($parameters['filters']['_client_config_filter'])) {
            $queryParts[] = $queryBuilder->expr()->eq('configuration.client', ':configurationClient');
            $queryBuilder->setParameter('configurationClient', $parameters['filters']['_client_config_filter']);
            unset($parameters['filters']['_client_config_filter']);
        }

        // do not get configurations for deleted items
        $queryBuilder->join('configuration.item', 'item');
        $queryParts[] = $queryBuilder->expr()->isNotNull('item.id');

        $queryParts = parent::addFiltersToQuery($queryBuilder, $queryParts, $parameters);

        return $queryParts;
    }
}
