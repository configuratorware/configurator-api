<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\Query;
use Redhotmagma\ApiBundle\Repository\Repository;

/**
 * Class DesignerProductionCalculationTypeRepository.
 */
class DesignerProductionCalculationTypeRepository extends Repository
{
    public function findByDesignProductionMethodIdentifier($identifier)
    {
        $query = $this->createQueryBuilder('a')
            ->join('a.designProductionMethod', 'b')
            ->andWhere('b.identifier = :identifier')
            ->setParameter('identifier', $identifier)
            ->getQuery()
            ->getResult();

        return $query;
    }

    public function getByDesignProductionMethod(int $method)
    {
        $result = $this->createQueryBuilder('a', 'a.id')
            ->andWhere('a.designProductionMethod = :method')
            ->setParameter('method', $method)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        return $result;
    }
}
