<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ConfiguratorApiBundle\Exception\ExternalResourceException;
use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\ReleasedApiVersion;
use Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion\ReleasedApiVersionRepositoryInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ReleasedApiVersionPackagistApiRepository implements ReleasedApiVersionRepositoryInterface
{
    private const METHOD_GET = 'GET';
    private const URL = 'https://packagist.org/packages/configuratorware/configurator-api.json';

    /**
     * @var HttpClientInterface HttpClientInterface
     */
    private $client;

    /**
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return ReleasedApiVersion[]
     *
     * @throws ExternalResourceException when http request fails or the response does not match the expected result
     */
    public function findVersions(): array
    {
        try {
            $responseAsArray = $this->client->request(self::METHOD_GET, self::URL)->toArray();
        } catch (\Throwable $e) {
            throw ExternalResourceException::httpFailed(self::URL);
        }

        if (!isset($responseAsArray['package']['versions'])) {
            throw ExternalResourceException::invalidSchema('$responseAsArray[\'package\'][\'versions\']');
        }

        $releasedVersions = [];
        foreach (array_keys($responseAsArray['package']['versions']) as $tag) {
            $releasedVersions[] = ReleasedApiVersion::fromState($tag);
        }

        return $releasedVersions;
    }
}
