<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext;

/**
 * ItemtextRepository.
 *
 * @method Itemtext findOneByItem(Item $item)
 */
class ItemtextRepository extends Repository
{
}
