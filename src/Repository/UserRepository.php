<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method User find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, ?array $orderBy = null)
 * @method User findOneById(int $id)
 * @method User findOneByEmail(string $email)
 */
class UserRepository extends Repository implements UserLoaderInterface
{
    public function findUserByUsername(string $username): ?User
    {
        return $this->findOneBy([
            'username' => $username,
        ]);
    }

    public function findUserByEmail(string $email): ?User
    {
        return $this->findOneBy([
            'email' => $email,
        ]);
    }

    /**
     * @return User
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findAny()
    {
        return $this->createQueryBuilder('u')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws UserNotFoundException
     */
    public function loadUserByUsername(string $username): User
    {
        $user = $this->findUserByUsername($username);

        // allow login by email too
        if (!$user) {
            $user = $this->findUserByEmail($username);
        }

        if (!$user || !$user->getIsActive()) {
            throw new UserNotFoundException(sprintf('Email "%s" does not exist.', $username));
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    protected function addFiltersToQuery($queryBuilder, $queryParts, $parameters): array
    {
        // update query to filter apiusers or normal users based on filter parameter
        if (isset($parameters['filters']['_apiuser']) && true === $parameters['filters']['_apiuser']) {
            $queryParts[] = $queryBuilder->expr()->isNotNull('user.api_key');
            unset($parameters['filters']['_apiuser']);
        } else {
            $queryParts[] = $queryBuilder->expr()->isNull('user.api_key');
        }

        return parent::addFiltersToQuery($queryBuilder, $queryParts, $parameters);
    }

    public function loadUserByIdentifier(string $identifier): ?UserInterface
    {
        return $this->loadUserByUsername($identifier);
    }
}
