<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser;

/**
 * ChannelRepository.
 *
 * @method Channel findOneByIdentifier(string $identifier)
 * @method Channel findOneBy(array $criteria)
 * @method list<Channel> findAll()
 */
class ChannelRepository extends Repository
{
    public const DEFAULT_CHANNEL_IDENTIFIER = '_default';

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $queryParts
     * @param array $parameters
     *
     * @return array
     */
    protected function addFiltersToQuery($queryBuilder, $queryParts, $parameters): array
    {
        // _client_user_filter, limit to current users client(s) if the current user has the role client
        if (isset($parameters['filters']['_client_user_filter'])) {
            $queryBuilder->join(
                ClientChannel::class,
                'clientChannel',
                'WITH',
                'clientChannel.channel = channel.id'
            );

            $queryBuilder->join(
                Client::class,
                'client',
                'WITH',
                'client.id = clientChannel.client'
            );

            $queryBuilder->join(
                ClientUser::class,
                'clientUser',
                'WITH',
                'clientUser.client = client.id'
            );

            $queryParts[] = $queryBuilder->expr()->eq('clientUser.user', ':currentUser');
            $queryBuilder->setParameter('currentUser', $parameters['filters']['_client_user_filter']);

            unset($parameters['filters']['_client_user_filter']);
        }

        $queryParts = parent::addFiltersToQuery($queryBuilder, $queryParts, $parameters);

        return $queryParts;
    }
}
