<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\DBAL\Exception as DBALException;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * ItemgroupRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ItemgroupRepository extends Repository
{
    /**
     * Returns the configurationvariant children of a parent item.
     *
     * @param Item $parent
     *
     * @return array
     *
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     */
    public function getConfigurationvariantsForParentItem($parent)
    {
        $itemRepository = $this->getEntityManager()->getRepository(Item::class);

        $queryBuilder = $itemRepository->createQueryBuilder('item');

        $queryBuilder->getEntityManager()->createQueryBuilder();

        $queryBuilder->leftJoin('item.itemItemgroup', 'itemItemgroup');
        $queryBuilder->join(
            'itemItemgroup.itemgroup',
            'itemgroup',
            'WITH',
            $queryBuilder->expr()->eq('itemgroup.configurationvariant', true)
        );

        $queryBuilder->where(
            $queryBuilder->expr()->eq('item.parent', ':parent')
        );

        $queryBuilder->setParameter('parent', $parent->getId());

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * get a mapping of parents, itemgroups and itemgroupentries by a list of item identifiers.
     *
     * @param array $itemIdentifiers
     *
     * @return  array
     */
    public function getItemGroupMapping($itemIdentifiers)
    {
        $result = [];

        $itemIdentifiers = array_filter($itemIdentifiers);

        if (!empty($itemIdentifiers)) {
            $con = $this->getEntityManager()->getConnection();

            $sql = 'SELECT 
                      parentItem.identifier AS parent_identifier,
                      i.identifier AS group_identifier,
                      i2.identifier AS group_entry_identifier,
                      childItem.identifier AS item_identifier,
                      childItem.id
                    FROM
                      item_itemgroupentry ii
                    LEFT JOIN itemgroup i ON ii.itemgroup_id = i.id
                    LEFT JOIN item childItem ON ii.item_id = childItem.id
                    LEFT JOIN itemgroupentry i2 on ii.itemgroupentry_id = i2.id
                    LEFT JOIN item parentItem ON childItem.parent_id = parentItem.id
                    LEFT JOIN itemtext ON itemtext.item_id = childItem.id
                    WHERE ii.date_deleted = "0001-01-01 00:00:00"
                      AND parentItem.id IS NOT NULL
                      AND childItem.identifier IN ("' . implode('","', $itemIdentifiers) . '")
                      AND childItem.date_deleted = "0001-01-01 00:00:00"
                    ORDER BY itemtext.title, childItem.id, parentItem.id, i.sequencenumber, i2.sequencenumber';

            try {
                $stmt = $con->prepare($sql);
                $result = $stmt->executeQuery();

                $result = $result->fetchAllAssociative();
            } catch (DBALException $e) {
                $result = [];
            }
        }

        return $result;
    }

    /**
     * get a mapping of parents, itemgroups and itemgroupentries by a list of option identifiers.
     *
     * @param array $optionIdentifiers
     *
     * @return  array
     */
    public function getOptionGroupMapping($optionIdentifiers)
    {
        $result = [];

        $optionIdentifiers = array_filter($optionIdentifiers);

        if (!empty($optionIdentifiers)) {
            $con = $this->getEntityManager()->getConnection();

            $sql = 'SELECT 
                      o1.identifier AS parent_identifier,
                      i.identifier AS group_identifier,
                      i1.identifier AS group_entry_identifier,
                      o.identifier AS option_identifier,
                      o.id
                    FROM
                      option_group_entry oge
                    LEFT JOIN itemgroup i ON oge.group_id = i.id
                    LEFT JOIN `option` o ON oge.option_id = o.id
                    LEFT JOIN itemgroupentry i1 ON oge.group_entry_id = i1.id
                    LEFT JOIN `option` o1 ON o.parent_id = o1.id
                    WHERE 
                      o.identifier IN ("' . implode('","', $optionIdentifiers) . '")
                      AND o.date_deleted = "0001-01-01 00:00:00"
                    ORDER BY o.id, o1.id, i.sequencenumber, i1.sequencenumber';

            try {
                $stmt = $con->prepare($sql);
                $result = $stmt->executeQuery();

                $result = $result->fetchAllAssociative();
            } catch (DBALException $e) {
                $result = [];
            }
        }

        return $result;
    }

    public function getNextSequenceNumber(): int
    {
        $entityName = $this->getNormalizedEntityName();
        $qb = $this->createQueryBuilder($entityName);
        $sequenceNumber = $qb
            ->select($qb->expr()->max('itemgroup.sequencenumber'))
            ->getQuery()
            ->getSingleScalarResult();

        return (int)$sequenceNumber + 1;
    }
}
