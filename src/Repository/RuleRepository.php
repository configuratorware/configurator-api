<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;

/**
 * @method Rule find($id, $lockMode = null, $lockVersion = null)
 */
class RuleRepository extends Repository
{
    /**
     * get all global rules (no item_id set) ordered by the sequence number of the rules rule type.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  array
     */
    public function getSortedGlobalRules()
    {
        $entityname = $this->getNormalizedEntityName();
        $querybuilder = $this->createQueryBuilder($entityname);
        $querybuilder->leftJoin('rule.ruletype', 'ruletype');
        $querybuilder->where($querybuilder->expr()->isNull('rule.item'));
        $querybuilder->orderBy('ruletype.sequencenumber', 'asc');

        $query = $querybuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * find rules for reverse option dependeny checks.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.27
     *
     * @version 1.0.27
     *
     * @param   string $itemIdentifier
     * @param   array $selectedOptionIdentifiers
     *
     * @return  array
     */
    public function getForReverseOptionDependencyCheck($itemIdentifier, $selectedOptionIdentifiers)
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->leftJoin('rule.ruletype', 'ruletype');
        $queryBuilder->leftJoin('rule.option', 'option');
        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('ruletype.identifier', ':optiondependency'),
                $queryBuilder->expr()->like('rule.data', ':itemIdentifier'),
                $queryBuilder->expr()->in('option.identifier', ':selectedOptionIdentifiers')
            ));

        $queryBuilder->setParameter('optiondependency', 'optiondependency');
        $queryBuilder->setParameter('itemIdentifier', '%"' . $itemIdentifier . '"%');
        $queryBuilder->setParameter('selectedOptionIdentifiers', $selectedOptionIdentifiers);

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * find rules for reverse additional options
     * it finds teh rules for a set of removed options
     * to check if he additional option also has to be removed.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.63
     *
     * @version 1.0.63
     *
     * @param   string $itemIdentifier
     * @param   array $optionIdentifiers
     *
     * @return  array
     */
    public function getAdditionalOptionsRules($itemIdentifier, $optionIdentifiers)
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->leftJoin('rule.ruletype', 'ruletype');
        $queryBuilder->leftJoin('rule.option', 'option');
        $queryBuilder->leftJoin('rule.item', 'baseItem');
        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('ruletype.identifier', ':additionaloption'),
                $queryBuilder->expr()->eq('baseItem.identifier', ':itemIdentifier'),
                $queryBuilder->expr()->in('option.identifier', ':selectedOptionIdentifiers')
            ));

        $queryBuilder->setParameter('additionaloption', 'additionaloption');
        $queryBuilder->setParameter('itemIdentifier', $itemIdentifier);
        $queryBuilder->setParameter('selectedOptionIdentifiers', $optionIdentifiers);

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param $itemIdentifier
     * @param $optionClassificationIdentifier
     *
     * @return mixed
     */
    public function getOptionClassificationMinAmountRules($itemIdentifier, $optionClassificationIdentifier)
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->leftJoin('rule.ruletype', 'ruletype');
        $queryBuilder->leftJoin('rule.option', 'option');
        $queryBuilder->leftJoin('rule.item', 'baseItem');
        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('ruletype.identifier', ':optionclassificationminamount'),
                $queryBuilder->expr()->eq('baseItem.identifier', ':itemIdentifier'),
                $queryBuilder->expr()->like('rule.data', ':optionClassificationIdentifier')
            ));

        $queryBuilder->setParameter('optionclassificationminamount', 'optionclassificationminamount');
        $queryBuilder->setParameter('itemIdentifier', $itemIdentifier);
        $queryBuilder->setParameter('optionClassificationIdentifier', '%"' . $optionClassificationIdentifier . '"%');

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $id
     *
     * @return Rule
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOptionExclusionReverseRule(int $id): Rule
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        return $queryBuilder
            ->where($queryBuilder->expr()->eq('rule.reverseRule', ':reverseRuleId'))
            ->setParameter('reverseRuleId', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getCountForItem(string $itemIdentifier): int
    {
        $entityName = $this->getNormalizedEntityName();
        $qb = $this->createQueryBuilder($entityName);
        $expr = $qb->expr();
        $qb->leftJoin('rule.item', 'item')
            ->where(
                $expr->orX()->addMultiple([
                    $expr->isNull('item'),
                    $expr->eq('item.identifier', ':itemIdentifier'),
                ])
            )
            ->setParameter('itemIdentifier', $itemIdentifier);

        return $this->getCountForQueryBuilder($qb);
    }

    /**
     * override search query building to join the current language for title search.
     *
     * @param \Doctrine\ORM\QueryBuilder $querybuilder
     * @param array $queryparts
     * @param array $searchablefields
     * @param array $parameters
     *
     * @return array
     */
    protected function addSearchToQuery($querybuilder, $queryparts, $searchablefields, $parameters)
    {
        $languageSettings = C_LANGUAGE_SETTINGS;

        $querybuilder->leftJoin(
            'rule.ruleText',
            'ruleText',
            'WITH',
            $querybuilder->expr()->eq('ruleText.language', $languageSettings['id'])
        );

        $serachExp[] = $querybuilder->expr()->like('ruleText.title', ':_query');
        $queryparts[] = call_user_func_array([$querybuilder->expr(), 'orX'], $serachExp);

        $querybuilder->setParameter('_query', '%' . $parameters['query'] . '%');

        return $queryparts;
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     * @param array $queryParts
     * @param array $parameters
     *
     * @psalm-suppress TooManyArguments
     *
     * @return array
     */
    protected function addFiltersToQuery($queryBuilder, $queryParts, $parameters)
    {
        if (!empty($parameters['filters'])
            && !empty($parameters['filters']['rule.item.identifier'])) {
            // get one side of exclusion rules to not select them for the list
            // the other direction will still be selected
            $entityName = $this->getNormalizedEntityName();
            $exclusionQueryBuilder = $this->createQueryBuilder($entityName);
            $exclusionQueryBuilder->leftJoin('rule.ruletype', 'ruletype');
            $exclusionQueryBuilder->leftJoin('rule.item', 'baseItem');
            $exclusionQueryBuilder->where(
                $exclusionQueryBuilder->expr()->andX(
                    $exclusionQueryBuilder->expr()->eq('ruletype.identifier', ':option_exclusion_type'),
                    $exclusionQueryBuilder->expr()->eq('baseItem.identifier', ':item_identifier')
                )
            );

            $exclusionQueryBuilder->setParameter('option_exclusion_type', 'optionexclusion');
            $exclusionQueryBuilder->setParameter('item_identifier', $parameters['filters']['rule.item.identifier']);

            $query = $exclusionQueryBuilder->getQuery();

            $result = $query->getResult();

            // match rules to get the pairs
            /** @var Rule $rule */
            $ruleExcludes = [];
            foreach ($result as $rule) {
                $ruleData = json_decode($rule->getData());
                $option_identifier = $ruleData->option_identifier;

                $optionIdentifiers = [$option_identifier, $rule->getOption()->getIdentifier()];
                natsort($optionIdentifiers);
                $excludeIdentifier = implode('---', $optionIdentifiers);
                $ruleExcludes[$excludeIdentifier] = $rule->getId();
            }

            // exclude one side of each pair from the next select
            if (!empty($ruleExcludes)) {
                $queryParts[] = $queryBuilder->expr()->notIn('rule.id', ':rule_excludes');
                $queryBuilder->setParameter('rule_excludes', $ruleExcludes);
            }
        }

        $queryParts = parent::addFiltersToQuery($queryBuilder, $queryParts, $parameters);

        return $queryParts;
    }
}
