<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassificationtext;

/**
 * ItemclassificationtextRepository.
 *
 * @method Itemclassificationtext findOneByItemclassification(Itemclassification $itemClassification)
 */
class ItemclassificationtextRepository extends Repository
{
}
