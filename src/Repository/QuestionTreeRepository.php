<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

/**
 * @method QuestionTree|null find($id, $lockMode = null, $lockVersion = null)
 * @method QuestionTree|null findOneBy(array $criteria, array $orderBy = null)
 * @method QuestionTree[]    findAll()
 * @method QuestionTree[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionTreeRepository extends Repository
{
    public function findOneWithTexts(int $id): ?QuestionTree
    {
        $queryBuilder = $this->createQueryBuilder('questionTree');
        $expr = $queryBuilder->expr();

        $query = $queryBuilder->addSelect('texts')
            ->leftJoin('questionTree.texts', 'texts')
            ->andWhere($expr->eq('questionTree.id', ':id'))
            ->addOrderBy('texts.id', 'ASC')
            ->setParameter('id', $id)
            ->getQuery();

        try {
            return $query->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByIdentifierAndLanguageISO(string $identifier, string $languageISO): ?QuestionTree
    {
        $queryBuilder = $this->createQueryBuilder('questionTree');
        $expr = $queryBuilder->expr();

        $query = $queryBuilder->addSelect('data')
            ->leftJoin('questionTree.texts', 'texts')
            ->leftJoin('questionTree.data', 'data')
            ->leftJoin('data.language', 'language')
            ->andWhere($expr->eq('questionTree.identifier', ':identifier'))
            ->andWhere($expr->eq('language.iso', ':languageISO'))
            ->setParameter('identifier', $identifier)
            ->setParameter('languageISO', $languageISO)
            ->getQuery();

        try {
            return $query->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    /**
     * @param ListRequestArguments $arguments
     * @param string[]|array       $searchableFields
     *
     * @return QuestionTree[]|array
     */
    public function fetchListWithTexts(ListRequestArguments $arguments, array $searchableFields): array
    {
        $queryBuilder = $this->getQueryBuilderFromParameters($arguments->toArray(), $searchableFields);
        $query = $queryBuilder->addSelect('texts')
            ->leftJoin($this->getNormalizedEntityName() . '.texts', 'texts')
            ->getQuery();

        return $query->getResult();
    }
}
