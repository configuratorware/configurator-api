<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * ItempriceRepository.
 */
class ItempriceRepository extends Repository
{
    /**
     * get the item price for an item by identifier.
     *
     * @param string $identifier
     * @param string $channel
     * @param int $amountFrom
     *
     * @return array
     */
    public function getItemPriceByIdentifier($identifier, $channel, $amountFrom = 1): array
    {
        $return = [];
        $return['price'] = 0.0;
        $return['netPrice'] = 0.0;

        $queryBuilder = $this->createQueryBuilder('Itemprice');
        $queryBuilder->select('Itemprice.price, Itemprice.price_net');
        $queryBuilder->leftJoin('Itemprice.item', 'Item');
        $queryBuilder->leftJoin('Itemprice.channel', 'Channel');

        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->lte('Itemprice.amountfrom', ':amountFrom'),
                $queryBuilder->expr()->eq('Item.identifier', ':identifier'),
                $queryBuilder->expr()->eq('Channel.identifier', ':channelIdentifier')
            )
        );

        $queryBuilder->setParameters([
            'identifier' => $identifier,
            'amountFrom' => $amountFrom,
            'channelIdentifier' => $channel,
        ]);

        $queryBuilder->orderBy('Itemprice.amountfrom', 'DESC');

        $result = $queryBuilder->getQuery()->getResult();

        if (!empty($result)) {
            $return['price'] = (float)$result[0]['price'];
            $return['netPrice'] = (float)$result[0]['price_net'];
        }

        return $return;
    }

    /**
     * get the item price for an item by identifier.
     *
     * @param array $identifiers
     * @param string $channel
     *
     * @return array
     */
    public function getBulkPricesMappedByIdentifierAndAmountFrom(array $identifiers, string $channel): array
    {
        $queryBuilder = $this->createQueryBuilder('itemPrice')
            ->select('itemPrice.price, itemPrice.price_net, item.identifier, itemPrice.amountfrom')
            ->leftJoin('itemPrice.item', 'item')
            ->leftJoin('itemPrice.channel', 'channel');

        $queryBuilder->andWhere(
            $queryBuilder->expr()->in('item.identifier', ':identifiers')
        );

        $queryBuilder->andWhere(
            $queryBuilder->expr()->eq('channel.identifier', ':channelIdentifier')
        );

        $queryBuilder
            ->setParameter('identifiers', $identifiers)
            ->setParameter('channelIdentifier', $channel)
            ->orderBy('itemPrice.amountfrom', 'DESC');

        $queryResult = $queryBuilder->getQuery()->getResult();

        $result = [];
        foreach ($queryResult as $item) {
            $identifier = $item['identifier'];
            unset($item['identifier']);
            $amountFrom = $item['amountfrom'];
            unset($item['amountfrom']);
            $result[$identifier][$amountFrom] = $item;
        }

        return $result;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    public function getBulkSteps(string $itemIdentifier)
    {
        $queryBuilder = $this->createQueryBuilder('Itemprice');
        $queryBuilder->select('Itemprice.amountfrom');
        $queryBuilder->leftJoin('Itemprice.item', 'Item');
        $queryBuilder->leftJoin('Itemprice.channel', 'Channel');

        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('Item.identifier', ':identifier'),
                $queryBuilder->expr()->eq('Channel.identifier', ':channelIdentifier')
            )
        );

        $queryBuilder->setParameters([
            'identifier' => $itemIdentifier,
            'channelIdentifier' => C_CHANNEL,
        ]);

        $queryBuilder->orderBy('Itemprice.amountfrom');

        $result = $queryBuilder->getQuery()->getScalarResult();
        $result = array_map('current', $result);

        return $result;
    }
}
