<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;

class DesignAreaDesignProductionMethodRepository extends Repository
{
    /**
     * @param string $itemIdentifier
     * @param string $designAreaIdentifier
     * @param string $designProductionMethodIdentifier
     *
     * @return DesignAreaDesignProductionMethod|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByItemAndDesignAreaAreaAndProductionMethod(
        string $itemIdentifier,
        string $designAreaIdentifier,
        string $designProductionMethodIdentifier
    ): ?DesignAreaDesignProductionMethod {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder->join(
            $entityName . '.designArea',
            'area',
            'WITH',
            $queryBuilder->expr()->eq('area.identifier', ':designAreaIdentifier')
        );

        $queryBuilder->join(
            'area.item',
            'item',
            'WITH',
            $queryBuilder->expr()->eq('item.identifier', ':itemIdentifier')
        );

        $queryBuilder->join(
            $entityName . '.designProductionMethod',
            'method',
            'WITH',
            $queryBuilder->expr()->eq('method.identifier', ':designProductionMethodIdentifier')
        );

        $queryBuilder->setParameter('itemIdentifier', $itemIdentifier);
        $queryBuilder->setParameter('designAreaIdentifier', $designAreaIdentifier);
        $queryBuilder->setParameter('designProductionMethodIdentifier', $designProductionMethodIdentifier);

        $query = $queryBuilder->getQuery();

        $result = $query->getOneOrNullResult();

        return $result;
    }
}
