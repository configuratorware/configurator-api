<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;

/**
 * @method Optionclassification findOneByItemclassification(Itemclassification $itemClassification)
 * @method Optionclassification find($id, $lockMode = null, $lockVersion = null);
 */
class OptionclassificationRepository extends Repository
{
    /**
     * fetch option classifications by an attribute + value.
     *
     * @param string $attributeIdentifier
     * @param string $attributeValue
     *
     * @return  array
     */
    public function getByAttribute($attributeIdentifier, $attributeValue)
    {
        $entityname = $this->getNormalizedEntityName();
        $querybuilder = $this->createQueryBuilder($entityname);
        $querybuilder->leftJoin('optionclassification.optionClassificationAttribute', 'optionClassificationAttribute');
        $querybuilder->leftJoin('optionClassificationAttribute.attribute', 'attribute');
        $querybuilder->leftJoin('optionClassificationAttribute.attributevalue', 'attributevalue');

        $querybuilder->where($querybuilder->expr()->andX(
            $querybuilder->expr()->eq('attribute.identifier', ':attributeIdentifier'),
            $querybuilder->expr()->eq('attributevalue.value', ':attributeValue')
        ));

        $querybuilder->setParameter('attributeIdentifier', $attributeIdentifier);
        $querybuilder->setParameter('attributeValue', $attributeValue);

        $query = $querybuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $id
     *
     * @deprecated use ItemRepository::findItemByIdWithOptionClassifications instead
     */
    public function getByItemId($id)
    {
        $entityname = $this->getNormalizedEntityName();
        $querybuilder = $this->createQueryBuilder($entityname);
        $querybuilder->leftJoin('optionclassification.itemOptionclassification', 'itemOptionclassification');
        $querybuilder->leftJoin('itemOptionclassification.item', 'item');

        $querybuilder->where($querybuilder->expr()->eq('item.id', ':itemid'));

        $querybuilder->addOrderBy('optionclassification.sequencenumber', 'ASC');
        $querybuilder->addOrderBy('optionclassification.identifier', 'ASC');
        $querybuilder->addOrderBy('optionclassification.id', 'ASC');

        $querybuilder->setParameter('itemid', $id);

        $query = $querybuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }

    /**
     * Get next unused sequence number.
     *
     * @return int
     */
    public function getNextSequenceNumber(): int
    {
        $entityName = $this->getNormalizedEntityName();
        $sequenceNumber = $this->createQueryBuilder($entityName)
            ->select('MAX(optionclassification.sequencenumber)')
            ->getQuery()
            ->getSingleScalarResult();

        return (int)$sequenceNumber + 1;
    }

    /**
     * add custom filter to seperate itemclassifications and optionclassification.
     *
     * @param \Doctrine\ORM\QueryBuilder $querybuilder
     * @param array $queryparts
     * @param array $parameters
     *
     * @return  array
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    protected function addFiltersToQuery($querybuilder, $queryparts, $parameters)
    {
        if (!empty($parameters['filters'])) {
            // handle parent_id
            if (array_key_exists('_parent_id', $parameters['filters'])) {
                if (null === $parameters['filters']['_parent_id']) {
                    $queryparts[] = $querybuilder->expr()->orX(
                        $querybuilder->expr()->isNull('optionclassification.parent_id'),
                        $querybuilder->expr()->eq('optionclassification.parent_id', 0)
                    );

                    unset($parameters['filters']['_parent_id']);
                } elseif ($parameters['filters']['_parent_id'] > 0) {
                    $queryparts[] = $querybuilder->expr()
                        ->eq('optionclassification.parent_id', ':parent_id');

                    $querybuilder->setParameter('parent_id', $parameters['filters']['_parent_id']);

                    unset($parameters['filters']['_parent_id']);
                }
            }
        }

        $parameters['filters'] = array_filter($parameters['filters']);

        $queryparts = parent::addFiltersToQuery($querybuilder, $queryparts, $parameters);

        return $queryparts;
    }
}
