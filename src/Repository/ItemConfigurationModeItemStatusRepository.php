<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ConfigurationMode;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemConfigurationModeItemStatus;

class ItemConfigurationModeItemStatusRepository extends Repository
{
    /**
     * @param int $itemId
     * @param string $configurationModeIdentifier
     *
     * @return ItemConfigurationModeItemStatus|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getOneOrNullByItemAndConfigurationMode(
        int $itemId,
        string $configurationModeIdentifier
    ): ?ItemConfigurationModeItemStatus {
        $result = $this->createQueryBuilder('a')
            ->join(ConfigurationMode::class, 'b', 'WITH', 'b.id = a.configurationMode')
            ->andWhere('a.item = :item AND b.identifier = :configurationMode')
            ->setParameter('item', $itemId)
            ->setParameter('configurationMode', $configurationModeIdentifier)
            ->getQuery()
            ->getOneOrNullResult();

        return $result;
    }
}
