<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;

/**
 * Class FontRepository.
 *
 * @method Font findOneBy(array $properties)
 */
class FontRepository extends Repository
{
    /**
     * @param array $names
     *
     * @return Font[]
     */
    public function findByNames(array $names): array
    {
        $base = $this->getNormalizedEntityName();
        $qb = $this->createQueryBuilder($base);
        $expr = $qb->expr();

        return $qb->andWhere($expr->in($base . '.name', ':names'))
            ->andWhere($expr->eq($base . '.active', true))
            ->setParameter('names', $names)
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Font[]
     */
    public function frontendList(): array
    {
        $qb = $this->createQueryBuilder('font');
        $expr = $qb->expr();

        $query = $qb->andWhere($expr->eq('font.active', true))
            ->orderBy('font.sequence_number')
            ->addOrderBy('font.name')
            ->getQuery();

        return $query->getResult();
    }
}
