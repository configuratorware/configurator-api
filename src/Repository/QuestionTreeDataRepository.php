<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTreeData;

class QuestionTreeDataRepository extends Repository
{
    public function findOneByLanguageIdAndQuestionTreeId(int $languageId, int $questionTreeId): ?QuestionTreeData
    {
        $queryBuilder = $this->createQueryBuilder('questionTreeData');
        $expr = $queryBuilder->expr();
        $query = $queryBuilder
            ->join('questionTreeData.questionTree', 'questionTree')
            ->andWhere($expr->eq('questionTreeData.language', ':languageId'))
            ->andWhere($expr->eq('questionTree.id', ':questionTreeId'))
            ->setParameter('languageId', $languageId)
            ->setParameter('questionTreeId', $questionTreeId)
            ->getQuery();

        try {
            return $query->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
