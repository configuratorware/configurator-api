<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

class DesignAreaDesignProductionMethodPriceRepository extends Repository
{
    /**
     * @param DesignerProductionCalculationType $calculationType
     * @param Item $item
     * @param string $designProductionMethodIdentifier
     * @param string $designAreaIdentifier
     * @param int $itemAmount
     *
     * @return DesignAreaDesignProductionMethodPrice[]
     */
    public function getCalculationPrices(
        DesignerProductionCalculationType $calculationType,
        Item $item,
        string $designProductionMethodIdentifier,
        string $designAreaIdentifier,
        int $itemAmount = 1
    ): array {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder
            ->join(
                DesignAreaDesignProductionMethod::class,
                'DesignAreaDesignProductionMethod',
                'WITH',
                $entityName . '.designAreaDesignProductionMethod = DesignAreaDesignProductionMethod'
            )
            ->join(
                DesignArea::class,
                'DesignArea',
                'WITH',
                'DesignAreaDesignProductionMethod.designArea = DesignArea'
            )
            ->join(
                DesignProductionMethod::class,
                'DesignProductionMethod',
                'WITH',
                'DesignAreaDesignProductionMethod.designProductionMethod = DesignProductionMethod'
            )
            ->join(
                Channel::class,
                'Channel',
                'WITH',
                $entityName . '.channel = Channel'
            );

        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('DesignArea.item', ':item'),
                $queryBuilder->expr()->eq('DesignProductionMethod.identifier', ':designProductionMethodIdentifier'),
                $queryBuilder->expr()->eq('DesignArea.identifier', ':designAreaIdentifier'),
                $queryBuilder->expr()->eq('Channel.identifier', ' :channelIdentifier'),
                $queryBuilder->expr()->lte($entityName . '.amount_from', ':amountFrom'),
                $queryBuilder->expr()->eq($entityName . '.designerProductionCalculationType', ':calculationType')
            )
        );

        $queryBuilder->orderBy($entityName . '.amount_from', 'DESC');

        $queryBuilder->setParameter('item', $item)
            ->setParameter('designProductionMethodIdentifier', $designProductionMethodIdentifier)
            ->setParameter('designAreaIdentifier', $designAreaIdentifier)
            ->setParameter('channelIdentifier', C_CHANNEL)
            ->setParameter('amountFrom', $itemAmount)
            ->setParameter('calculationType', $calculationType);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string $itemIdentifier
     * @param string $designAreaIdentifier
     * @param string $designProductionMethodIdentifier
     *
     * @return array
     */
    public function getBulkSteps(
        string $itemIdentifier,
        string $designAreaIdentifier,
        string $designProductionMethodIdentifier
    ) {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->select($entityName . '.amount_from');

        $queryBuilder->leftJoin($entityName . '.designAreaDesignProductionMethod', 'designAreaDesignProductionMethod');
        $queryBuilder->leftJoin('designAreaDesignProductionMethod.designProductionMethod', 'designProductionMethod');
        $queryBuilder->join(
            $entityName . '.channel',
            'Channel',
            'WITH',
            $queryBuilder->expr()->eq('Channel.identifier', ':channelIdentifier')
        );
        $queryBuilder->leftJoin('designAreaDesignProductionMethod.designArea', 'designArea');
        $queryBuilder->leftJoin('designArea.item', 'item');

        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq('designArea.identifier', ':designAreaIdentifier'),
                $queryBuilder->expr()->eq('designProductionMethod.identifier', ':designProductionMethodIdentifier'),
                $queryBuilder->expr()->eq('item.identifier', ':itemIdentifier')
            )
        );

        $queryBuilder->setParameter('designAreaIdentifier', $designAreaIdentifier);
        $queryBuilder->setParameter('designProductionMethodIdentifier', $designProductionMethodIdentifier);
        $queryBuilder->setParameter('channelIdentifier', C_CHANNEL);
        $queryBuilder->setParameter('itemIdentifier', $itemIdentifier);

        $queryBuilder->orderBy($entityName . '.amount_from');
        $queryBuilder->groupBy($entityName . '.amount_from');

        $result = $queryBuilder->getQuery()->getScalarResult();
        $result = array_map('current', $result);

        return $result;
    }
}
