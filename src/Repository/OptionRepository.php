<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionOptionPool;

/**
 * OptionRepository.
 *
 * @method Option findOneByItem(Item $item)
 * @method Option find($id, $lockMode = null, $lockVersion = null)
 */
class OptionRepository extends Repository
{
    /**
     * get a list of option classification identifiers for which the given option is selectable on the given base item.
     *
     * @param string $optionIdentifier
     * @param string $baseItemIdentifier
     *
     * @return  array
     */
    public function getOptionClassificationIdentifiersByOptionIdentifierAndBaseItemIdentifier($optionIdentifier, $baseItemIdentifier)
    {
        $itemClassificationRepository = $this->getEntityManager()->getRepository(Optionclassification::class);
        $queryBuilder = $itemClassificationRepository->createQueryBuilder('optionclassification');
        $queryBuilder->join('optionclassification.itemOptionclassification', 'itemOptionclassification');
        $queryBuilder->join('itemOptionclassification.item', 'item');
        $queryBuilder->join('itemOptionclassification.itemOptionclassificationOption',
            'itemOptionclassificationOption');
        $queryBuilder->join('itemOptionclassificationOption.option', 'option');

        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('option.identifier', ':optionidentifier'),
            $queryBuilder->expr()->eq('item.identifier', ':itemidentifier')
        ));

        $queryBuilder->setParameter('optionidentifier', $optionIdentifier);
        $queryBuilder->setParameter('itemidentifier', $baseItemIdentifier);

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        $itemClassificationIdentifiers = [];
        foreach ($result as $optionClassification) {
            /* @var Optionclassification $optionClassification */
            $itemClassificationIdentifiers[] = $optionClassification->getIdentifier();
        }

        return $itemClassificationIdentifiers;
    }

    /**
     * get an option by its identifier and a base items identifier
     * can be used to check if the option is in the construction pattern for the given base item.
     *
     * @param string $optionIdentifier
     * @param string $baseItemIdentifier
     *
     * @return  Option
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     */
    public function getOptionByIdentifierAndBaseItemIdentifier($optionIdentifier, $baseItemIdentifier)
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->join('option.itemOptionclassificationOption', 'itemOptionclassificationOption');
        $queryBuilder->join('itemOptionclassificationOption.itemOptionclassification', 'itemOptionclassification');
        $queryBuilder->join('itemOptionclassification.item', 'item');

        $queryBuilder->where($queryBuilder->expr()->andX(
            $queryBuilder->expr()->eq('option.identifier', ':optionidentifier'),
            $queryBuilder->expr()->eq('item.identifier', ':itemidentifier')
        ));

        $queryBuilder->setParameter('optionidentifier', $optionIdentifier);
        $queryBuilder->setParameter('itemidentifier', $baseItemIdentifier);

        $query = $queryBuilder->getQuery();

        try {
            $result = $query->getOneOrNullResult();
        } catch (\Doctrine\ORM\NonUniqueResultException $e) {
            $result = null;
        }

        return $result;
    }

    /**
     * look for a grouped_image_identifier attribute and use it if set
     * if not return the identifier as image name's first part.
     *
     * @param Option $option
     *
     * @return string
     *
     * @deprecated the image identifier should not be used anymore as urls can be retrieved from backend and the functionality can be extended
     *
     * notice: cannot be removed until assembly points get refactored
     */
    public function getImageIdentifierForOption($option)
    {
        $identifier = $option->getIdentifier();

        $attributes = $option->getOptionAttribute();

        if (!empty($attributes)) {
            /** @var ItemAttribute $itemAttribute */
            foreach ($attributes as $itemAttribute) {
                if ('grouped_image_identifier' === $itemAttribute->getAttribute()->getIdentifier()) {
                    $identifier = $itemAttribute->getAttributevalue()->getValue();

                    break;
                }
            }
        }

        return $identifier;
    }

    /**
     * fetch a list of options based on optionclassification(identifier) and baseitem (identifier).
     *
     * @param string      $itemIdentifier
     * @param string      $optionClassificationIdentifier
     * @param bool        $returnCount
     * @param array       $filters
     * @param string|null $queryTitle
     *
     * @return  Option[]|int
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0.16
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function fetchOptionitemsForFrontendlist(
        string $itemIdentifier,
        string $optionClassificationIdentifier,
        bool $returnCount = false,
        array $filters = [],
        ?string $queryTitle = null
    ) {
        $entityName = $this->getNormalizedEntityName();

        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->select([
                'option',
                'itemOptionclassificationOption',
                'itemOptionclassification',
                'item2',
                'optionText',
                'optionAttribute',
                'attribute',
                'attributevalue',
                'optionPrice',
                'optionStock',
                'attributeText',
                'attributevalueTranslation',
            ]
        );
        $expr = $queryBuilder->expr();

        if (true === $returnCount) {
            $queryBuilder->select('COUNT( DISTINCT ' . $entityName . '.id)');
        }

        $queryBuilder->join('option.itemOptionclassificationOption', 'itemOptionclassificationOption');
        $queryBuilder->join('itemOptionclassificationOption.itemOptionclassification', 'itemOptionclassification');
        $queryBuilder->join('itemOptionclassification.item', 'item2');
        $queryBuilder->join(
            'itemOptionclassification.optionclassification',
            'optionclassification',
            JOIN::WITH,
            $expr->eq('optionclassification.identifier', ':optionclassificationidentifier')
        );

        $queryParts = [];
        $queryParts[] = $expr->andX(
            $expr->orX(
                $expr->eq('option.deactivated', '0'),
                $expr->isNull('option.deactivated')
            ),
            $expr->eq('item2.identifier', ':itemidentifier'),
            $expr->eq('optionclassification.identifier', ':optionclassificationidentifier')
        );

        $queryBuilder->leftJoin('option.optionAttribute', 'optionAttribute');
        $queryBuilder->leftJoin('optionAttribute.attribute', 'attribute');
        $queryBuilder->leftJoin('optionAttribute.attributevalue', 'attributevalue');
        $queryBuilder->leftJoin(
            'attribute.attributetext',
            'attributeText',
            Join::WITH,
            $expr->eq('attributeText.language', ':language'));
        $queryBuilder->leftJoin(
            'attributevalue.attributevaluetranslation',
            'attributevalueTranslation',
            Join::WITH,
            $expr->eq('attributevalueTranslation.language', ':language'));

        $queryBuilder->leftJoin('option.optionPrice', 'optionPrice');
        $queryBuilder->leftJoin('option.optionStock', 'optionStock');
        // add filters
        if (!empty($filters)) {
            foreach ($filters as $filterAttribute => $filterValue) {
                $queryParts[] = $expr->eq('attribute.identifier', ':attributeidentifier');
                $queryParts[] = $expr->eq('attributevalue.value', ':attributevalue');

                $queryBuilder->setParameter('attributeidentifier', $filterAttribute);
                $queryBuilder->setParameter('attributevalue', $filterValue);
            }
        }
        $languageSettings = C_LANGUAGE_SETTINGS;
        $queryBuilder->leftJoin(
            'option.optionText',
            'optionText',
            Join::WITH,
            $expr->eq('optionText.language', ':language'));
        $queryBuilder->setParameter('language', $languageSettings['id']);
        if ($queryTitle) {
            $queryParts[] = $expr->like('optionText.title', ':optiontitle');
            $queryBuilder->setParameter('optiontitle', $queryTitle);
        }

        $where = call_user_func_array([$expr, 'andX'], $queryParts);
        $queryBuilder->where($where);

        $queryBuilder->setParameter('itemidentifier', $itemIdentifier);
        $queryBuilder->setParameter('optionclassificationidentifier', $optionClassificationIdentifier);

        /** @var ItemRepository $itemRepository */
        $itemRepository = $this->getEntityManager()->getRepository(Item::class);
        if (true === $itemRepository->getOverrideOptionOrderForItem($itemIdentifier)) {
            $queryBuilder->addOrderBy('itemOptionclassificationOption.sequenceNumber', Criteria::ASC);
        } else {
            $queryBuilder->addOrderBy('option.sequencenumber', Criteria::ASC);
        }
        $queryBuilder->addOrderBy('option.id', Criteria::ASC);

        $query = $queryBuilder->getQuery();

        if (true === $returnCount) {
            /** @var int $result */
            $result = $query->getSingleScalarResult();
        } else {
            /** @var Option[] $result */
            $result = $query->getResult();
        }

        return $result;
    }

    /**
     * Get only Options that are assemblypoints parent.
     *
     * @param mixed $parameters
     * @param array $searchableFields
     *
     * @return array
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     */
    public function fetchListForAssemblyPointOptions($parameters, $searchableFields)
    {
        $queryBuilder = $this->getQueryBuilderFromParameters($parameters, $searchableFields);

        $query = $this->getQueryForListForAssemblypointOptions($queryBuilder);

        $result = $query->getResult();

        return $result;
    }

    /**
     * Get count only for Options that are assemblypoints parent.
     *
     * @param mixed $parameters
     * @param array $searchableFields
     *
     * @return int
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     */
    public function fetchListCountForAssemblyPointOptions($parameters, $searchableFields)
    {
        $queryBuilder = $this->getQueryBuilderFromParameters($parameters, $searchableFields, true);

        $query = $this->getQueryForListForAssemblypointOptions($queryBuilder, true);

        $result = $query->getSingleScalarResult();

        $result = (int)$result;

        return $result;
    }

    /**
     * for each of the given option identifiers (and their alternatives) by the given attribute identifier
     * give a list of alternative options per attribute value
     * this is used to build a list (tree) of options to resolve an AttributeValueGroupAutoSwitchCheck.
     *
     * @param array  $optionIdentifiers
     * @param string $attributeIdentifier
     *
     * @return array
     */
    public function getForAlternateOptionsMappingForAttributeValueGroup($optionIdentifiers, $attributeIdentifier)
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder->select('option.identifier AS optionIdentifier');
        $queryBuilder->addSelect('attribute.identifier AS attributeIdentifier');
        $queryBuilder->addSelect('attributevalue.value AS attributeValue');
        $queryBuilder->addSelect('option1.identifier AS alternateOptionIdentifier');

        $queryBuilder->leftJoin('option.optionAttribute', 'optionAttribute');
        $queryBuilder->leftJoin('optionAttribute.attribute', 'attribute');
        $queryBuilder->leftJoin('optionAttribute.attributevalue', 'attributevalue');
        $queryBuilder->leftJoin('option.optionOptionPool', 'optionOptionPool');
        $queryBuilder->leftjoin(OptionOptionPool::class, 'optionOptionPool1', 'WITH',
            'optionOptionPool.optionPool = optionOptionPool1.optionPool');
        $queryBuilder->leftJoin('optionOptionPool1.option', 'option1');

        $queryBuilder->where($queryBuilder->expr()->eq('attribute.identifier', ':attributeIdentifier'));
        $queryBuilder->andWhere($queryBuilder->expr()->orX(
            $queryBuilder->expr()->in('option.identifier', ':optionIdentifiers'),
            $queryBuilder->expr()->in('option1.identifier', ':alternateOptionIdentifiers')
        ));
        $queryBuilder->andWhere($queryBuilder->expr()->neq('option.identifier', 'option1.identifier'));

        $queryBuilder->setParameter('optionIdentifiers', $optionIdentifiers);
        $queryBuilder->setParameter('alternateOptionIdentifiers', $optionIdentifiers);
        $queryBuilder->setParameter('attributeIdentifier', $attributeIdentifier);

        $query = $queryBuilder->getQuery();
        $result = $query->getResult();

        return $result;
    }

    /**
     * Get query for list for assemblypoint-options.
     *
     * @param QueryBuilder $queryBuilder
     * @param bool         $isCountQuery
     *
     * @return Query
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     */
    protected function getQueryForListForAssemblypointOptions($queryBuilder, $isCountQuery = false)
    {
        $languageSettings = C_LANGUAGE_SETTINGS;

        $queryBuilder->leftJoin('option.itemOptionclassificationOption', 'itemOptionclassificationOption');
        $queryBuilder->leftJoin('itemOptionclassificationOption.itemOptionclassification', 'itemOptionclassification');
        $queryBuilder->leftJoin('itemOptionclassification.optionclassification', 'optionclassification');
        $queryBuilder->leftJoin('optionclassification.assemblypointimageelement', 'assemblypointimageelement');

        if (!in_array('itemtext', $queryBuilder->getAllAliases())) {
            $queryBuilder->leftJoin(
                'option.optionText',
                'optionText',
                'WITH',
                $queryBuilder->expr()->eq('optionText.language', $languageSettings['id']));
        }

        $queryBuilder->innerJoin(
            'assemblypointimageelement.parentAssemblypointimageelementRelation',
            'parentAssemblypointimageelementRelation',
            'WITH',
            $queryBuilder->expr()->isNotNull('parentAssemblypointimageelementRelation.id')
        );

        if (false === $isCountQuery) {
            $queryBuilder->addGroupBy('option');
        }

        $query = $queryBuilder->getQuery();

        return $query;
    }

    /**
     * List sample Options
     * used in the assemblypointeditor to add sample options to elements.
     *
     * @param array $parameters
     * @param array $searchableFields
     *
     * @return array
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0.10
     */
    public function fetchListForAssemblypointSampleOptions($parameters, $searchableFields)
    {
        $queryBuilder = $this->getQueryBuilderFromParameters($parameters, $searchableFields);

        $query = $this->getQueryForAssemblypointSampleOptions($queryBuilder, $parameters);

        $result = $query->getResult();

        return $result;
    }

    /**
     * List sample Options count
     * used in the assemblypointeditor to add sample options to elements.
     *
     * @param mixed $parameters
     * @param array $searchableFields
     *
     * @return int
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0.10
     */
    public function fetchListCountForAssemblypointSampleOptions($parameters, $searchableFields)
    {
        $queryBuilder = $this->getQueryBuilderFromParameters($parameters, $searchableFields, true);

        $query = $this->getQueryForAssemblypointSampleOptions($queryBuilder, $parameters);

        $result = $query->getSingleScalarResult();

        $result = (int)$result;

        return $result;
    }

    /**
     * Get query for listing sample options.
     *
     * @param QueryBuilder $queryBuilder
     * @param mixed        $parameters
     *
     * @return  Query
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0.10
     */
    protected function getQueryForAssemblypointSampleOptions($queryBuilder, $parameters)
    {
        $queryBuilder->leftJoin('option.itemOptionclassificationOption', 'itemOptionclassificationOption');
        $queryBuilder->leftJoin('itemOptionclassificationOption.itemOptionclassification', 'itemOptionclassification');
        $queryBuilder->leftJoin('itemOptionclassification.optionclassification', 'optionclassification');
        $queryBuilder->leftJoin('optionclassification.assemblypointimageelement', 'assemblypointimageelement');

        if (!empty($parameters->baseItemIds)) {
            $queryBuilder->andWhere($queryBuilder->expr()->in('itemOptionclassification.item', ':baseItemIds'));
            $queryBuilder->setParameter('baseItemIds', $parameters->baseItemIds);
        }

        $query = $queryBuilder->distinct()->getQuery();

        return $query;
    }

    /**
     * @param array<string> $identifiers
     *
     * @return Option[]
     */
    public function findOptionsByIdentifiers(array $identifiers): array
    {
        $entityName = $this->getNormalizedEntityName();

        $queryBuilder = $this->createQueryBuilder($entityName, $entityName . '.identifier');

        return $queryBuilder->where($queryBuilder->expr()->in($entityName . '.identifier', ':identifiers'))
            ->setParameter('identifiers', $identifiers)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array<string> $identifiers
     *
     * @return array
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function findTextInputOptionsByIdentifiers(array $identifiers): array
    {
        if (empty($identifiers)) {
            return [];
        }
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $expr = $queryBuilder->expr();
        $queryBuilder->select('option.identifier, option.hasTextinput, option.inputValidationType')
                 ->where($expr->in('option.identifier', ':identifiers'))
                 ->andWhere($expr->eq('option.hasTextinput', 1))
                 ->andWhere($expr->isNotNull('option.inputValidationType'))
                 ->setParameter('identifiers', $identifiers);

        return $queryBuilder->getQuery()->getArrayResult();
    }
}
