<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;

/**
 * @method DesignViewDesignArea[] findByDesignArea(DesignArea $designArea)
 */
class DesignViewDesignAreaRepository extends Repository
{
    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    public function getByItemIdentifier(string $itemIdentifier): array
    {
        $base = $this->getNormalizedEntityName();
        $designView = 'designView';
        $designArea = 'designArea';
        $designViewItem = 'designViewItem';
        $designAreaItem = 'designAreaItem';

        $queryBuilder = $this->createQueryBuilder($base)
            ->join("$base.$designView", $designView)
            ->leftJoin("$designView.item", $designViewItem)
            ->join("$base.$designArea", $designArea)
            ->leftJoin("$designArea.item", $designAreaItem);

        $queryBuilder->where(
            $queryBuilder->expr()->eq("$designViewItem.identifier", ':itemIdentifier')
        );

        $queryBuilder->where(
            $queryBuilder->expr()->eq("$designAreaItem.identifier", ':itemIdentifier')
        );

        $result = $queryBuilder->setParameter('itemIdentifier', $itemIdentifier)
            ->getQuery()
            ->getResult();

        return $result;
    }
}
