<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

class DesignerGlobalItemPriceRepository extends Repository
{
    /**
     * @param Item $item
     * @param DesignerGlobalCalculationType $calculationType
     * @param int $itemAmount
     *
     * @return array
     */
    public function getCalculationPrice(
        Item $item,
        DesignerGlobalCalculationType $calculationType,
        int $itemAmount = 1
    ) {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder->join(
            $entityName . '.channel',
            'Channel',
            'WITH',
            $queryBuilder->expr()->eq('Channel.identifier', ':channelIdentifier')
        );

        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->eq($entityName . '.item', ':item'),
                $queryBuilder->expr()->eq($entityName . '.designerGlobalCalculationType',
                    ':designerGlobalCalculationType'),
                $queryBuilder->expr()->lte($entityName . '.amount_from', ':amountFrom')
            )
        );

        $queryBuilder->setParameter('item', $item);
        $queryBuilder->setParameter('designerGlobalCalculationType', $calculationType);
        $queryBuilder->setParameter('channelIdentifier', C_CHANNEL);
        $queryBuilder->setParameter('amountFrom', $itemAmount);

        $queryBuilder->orderBy($entityName . '.amount_from', 'DESC');

        $query = $queryBuilder->getQuery();

        $prices = $query->getResult();

        $price = null;
        if (!empty($prices)) {
            $price = $prices[0];
        }

        return $price;
    }
}
