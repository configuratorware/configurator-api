<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser;

class CodeSnippetRepository extends Repository
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param array $queryParts
     * @param array $parameters
     *
     * @return array
     */
    protected function addFiltersToQuery($queryBuilder, $queryParts, $parameters): array
    {
        // _client_user_filter, limit to current users client(s) if the current user has the role client
        if (isset($parameters['filters']['_client_user_filter'])) {
            $queryBuilder->join(
                ClientUser::class,
                'clientUser',
                'WITH',
                'clientUser.client = codesnippet.client'
            );

            $queryParts[] = $queryBuilder->expr()->eq('clientUser.user', ':currentUser');
            $queryBuilder->setParameter('currentUser', $parameters['filters']['_client_user_filter']);

            unset($parameters['filters']['_client_user_filter']);
        }

        $queryParts = parent::addFiltersToQuery($queryBuilder, $queryParts, $parameters);

        return $queryParts;
    }

    /**
     * @return mixed
     */
    public function findCurrent()
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);
        $queryBuilder->leftJoin('codesnippet.channel', 'channel');
        $queryBuilder->leftJoin('codesnippet.language', 'language');

        $queryBuilder->where(
            $queryBuilder->expr()->andX(
                $queryBuilder->expr()->orX($queryBuilder->expr()->eq('channel.identifier', ':channel'),
                    $queryBuilder->expr()->isNull('codesnippet.channel')),
                $queryBuilder->expr()->orX($queryBuilder->expr()->eq('language.iso', ':language'),
                    $queryBuilder->expr()->isNull('codesnippet.language'))
            )
        );

        $queryBuilder->setParameter('channel', C_CHANNEL);
        $queryBuilder->setParameter('language', C_LANGUAGE_ISO);

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        return $result;
    }
}
