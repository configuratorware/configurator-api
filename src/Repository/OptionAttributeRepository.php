<?php

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\AttributeValueCheckResult;

/**
 * OptionAttributeRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OptionAttributeRepository extends Repository
{
    /**
     * fetch a list of OptionAttribute entities by a list of option identifiers and an attribute identifier.
     *
     * @param   string[] $optionIdentifiers
     *
     * @return  OptionAttribute[]
     */
    public function fetchByOptionIdentifiersAndAttributeIdentifier(array $optionIdentifiers, string $attributeIdentifier): array
    {
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder->leftJoin('optionattribute.attribute', 'attribute');
        $queryBuilder->leftJoin('optionattribute.option', 'option');

        $queryBuilder->where($queryBuilder->expr()->in('option.identifier', ':option_identifiers'));
        $queryBuilder->andWhere($queryBuilder->expr()->eq('attribute.identifier', ':attribute_identifier'));

        $queryBuilder->setParameter('option_identifiers', $optionIdentifiers);
        $queryBuilder->setParameter('attribute_identifier', $attributeIdentifier);

        $query = $queryBuilder->getQuery();

        return $query->getResult();
    }

    /**
     * checks if the given options have one common attribute value for the given attribute.
     *
     * @author  Michael Aichele <aichele@redhotmamga.de>
     *
     * @since   1.0.18
     *
     * @version 1.0.18
     *
     * @param   string[] $optionIdentifiers
     * @param   string[] $attributeValues
     */
    public function checkCommonAttributeValues(
        array $optionIdentifiers,
        string $attributeIdentifier,
        array $attributeValues = []
    ): AttributeValueCheckResult {
        $checkResult = new AttributeValueCheckResult();
        $checkResult->attributeIdentifier = $attributeIdentifier;
        $checkResult->attributeTitle = $attributeIdentifier;

        $languageSettings = C_LANGUAGE_SETTINGS;
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder->select('option.id AS option_id, option.identifier as option_identifier');
        $queryBuilder->addSelect('attribute.identifier AS attribute_identifier');
        $queryBuilder->addSelect('GROUP_CONCAT(attributevalue.value) AS attribute_values');
        $queryBuilder->addSelect('attributetext.title AS attribute_title');
        $queryBuilder->leftJoin('optionattribute.attribute', 'attribute');
        $queryBuilder->leftJoin('attribute.attributetext', 'attributetext', 'WITH',
            $queryBuilder->expr()->eq('attributetext.language', $languageSettings['id']));
        $queryBuilder->leftJoin('optionattribute.option', 'option');
        $queryBuilder->leftJoin('optionattribute.attributevalue', 'attributevalue');

        $queryBuilder->where($queryBuilder->expr()->in('option.identifier', ':option_identifiers'));
        $queryBuilder->andWhere($queryBuilder->expr()->eq('attribute.identifier', ':attribute_identifier'));

        $queryBuilder->setParameter('option_identifiers', $optionIdentifiers);
        $queryBuilder->setParameter('attribute_identifier', $attributeIdentifier);

        $queryBuilder->groupBy('option');

        $query = $queryBuilder->getQuery();

        $result = $query->getResult();

        if (!empty($result)) {
            array_walk($result, static function (array &$row) {
                $row['attribute_values'] = explode(',', $row['attribute_values']);
            });

            $commonValues = array_intersect(...array_column($result, 'attribute_values'));
            $checkResult->hasCommonValues = !empty($commonValues);

            if (isset($result[0]['attribute_title'])) {
                $checkResult->attributeTitle = $result[0]['attribute_title'];
            }

            if (!empty($attributeValues)) {
                $conflictingOptionIdentifiers = array_map(static function ($row) use ($attributeValues) {
                    if (empty(array_intersect($attributeValues, $row['attribute_values']))) {
                        return $row['option_identifier'];
                    }

                    return null;
                }, $result);
                $checkResult->conflictingOptionIdentifiers = array_filter($conflictingOptionIdentifiers);
            } else {
                $checkResult->conflictingOptionIdentifiers = array_column($result, 'option_identifier');
            }
        }

        return $checkResult;
    }

    /**
     * search for attribute usage in the construction pattern of a given item.
     *
     * @param   string $itemIdentifier
     * @param   string $attributeSearchQuery
     *
     * @return  array
     */
    public function findAttributesInConstructionPattern($itemIdentifier, $attributeSearchQuery)
    {
        $languageSettings = C_LANGUAGE_SETTINGS;
        $entityName = $this->getNormalizedEntityName();
        $queryBuilder = $this->createQueryBuilder($entityName);

        $queryBuilder->addSelect('attribute');
        $queryBuilder->addSelect('attributevalue');
        $queryBuilder->addSelect('option');
        $queryBuilder->addSelect('itemoptionclassificationoption');
        $queryBuilder->addSelect('itemoptionclassification');
        $queryBuilder->addSelect('optionclassification');
        $queryBuilder->addSelect('attributetext');
        $queryBuilder->addSelect('attributevaluetranslation');

        $queryBuilder->leftJoin('optionattribute.option', 'option');
        $queryBuilder->leftJoin('option.itemOptionclassificationOption', 'itemoptionclassificationoption');
        $queryBuilder->leftJoin('itemoptionclassificationoption.itemOptionclassification', 'itemoptionclassification');
        $queryBuilder->leftJoin('itemoptionclassification.item', 'item');
        $queryBuilder->leftJoin('optionattribute.attribute', 'attribute');
        $queryBuilder->leftJoin('itemoptionclassification.optionclassification', 'optionclassification');
        $queryBuilder->leftJoin('attribute.attributetext', 'attributetext', 'WITH',
            $queryBuilder->expr()->eq('attributetext.language', $languageSettings['id']));
        $queryBuilder->leftJoin('optionattribute.attributevalue', 'attributevalue');
        $queryBuilder->leftJoin('attributevalue.attributevaluetranslation', 'attributevaluetranslation', 'WITH',
            $queryBuilder->expr()->eq('attributevaluetranslation.language', $languageSettings['id']));

        $queryBuilder->where($queryBuilder->expr()->eq('item.identifier', ':item_identifier'));
        $queryBuilder->andWhere($queryBuilder->expr()->like('attribute.identifier', ':attribute_identifier'));

        $queryBuilder->setParameter('item_identifier', $itemIdentifier);
        $queryBuilder->setParameter('attribute_identifier', '%' . $attributeSearchQuery . '%');

        $queryBuilder->addGroupBy('optionclassification.id');
        $queryBuilder->addGroupBy('optionattribute.attribute');
        $queryBuilder->addGroupBy('optionattribute.attributevalue');

        $query = $queryBuilder->getQuery();
        $result = $query->getResult();

        return $result;
    }
}
