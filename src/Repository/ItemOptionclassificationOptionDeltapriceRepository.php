<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Doctrine\ORM\NonUniqueResultException;
use Redhotmagma\ApiBundle\Repository\Repository;

/**
 * ItemOptionclassificationOptionDeltapriceRepository.
 */
class ItemOptionclassificationOptionDeltapriceRepository extends Repository
{
    /**
     * get the delta price for an selected option.
     *
     * @author  Andreas Nölke <noelke@redhotmagma.de>
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param string $itemIdentifier
     * @param string $optionClassificationIdentifier
     * @param string $optionIdentifier
     * @param string $channel
     *
     * @return array
     */
    public function getDeltaPriceByIdentifiers($itemIdentifier, $optionClassificationIdentifier, $optionIdentifier, $channel): array
    {
        $return = [];
        $return['price'] = 0.0;
        $return['netPrice'] = 0.0;

        $queryBuilder = $this->createQueryBuilder('DeltaPrice');
        $queryBuilder->select('DeltaPrice.price, DeltaPrice.price_net')
            ->join(
                'DeltaPrice.itemOptionclassificationOption',
                'ItemOptionClassificationOption'
            )
            ->join(
                'ItemOptionClassificationOption.option',
                'Option',
                'WITH',
                $queryBuilder->expr()->eq('Option.identifier', ':optionIdentifier'))
            ->join('ItemOptionClassificationOption.itemOptionclassification', 'ItemOptionClassification')
            ->join(
                'ItemOptionClassification.optionclassification',
                'OptionClassification',
                'WITH',
                $queryBuilder->expr()->eq('OptionClassification.identifier', ':optionClassificationIdentifier'))
            ->join(
                'ItemOptionClassification.item',
                'Item',
                'WITH',
                $queryBuilder->expr()->eq('Item.identifier', ':itemIdentifier')
            )
            ->join(
                'DeltaPrice.channel',
                'Channel',
                'WITH',
                $queryBuilder->expr()->eq('Channel.identifier', ':channelIdentifier')
            )
            ->setParameters([
                'optionIdentifier' => $optionIdentifier,
                'optionClassificationIdentifier' => $optionClassificationIdentifier,
                'itemIdentifier' => $itemIdentifier,
                'channelIdentifier' => $channel,
            ]);

        try {
            $result = $queryBuilder->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $result = null;
        }

        if (!is_null($result)) {
            $return['price'] = (float)$result['price'];
            $return['netPrice'] = (float)$result['price_net'];
        }

        return $return;
    }
}
