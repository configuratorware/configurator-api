<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Repository;

use Redhotmagma\ApiBundle\Repository\Repository;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;

/**
 * @method CreatorView find($id, $lockMode = null, $lockVersion = null)
 */
class CreatorViewRepository extends Repository
{
    /**
     * @param int $itemId
     *
     * @return CreatorView[]
     */
    public function findByItemId(int $itemId): array
    {
        $queryBuilder = $this->createQueryBuilder('creatorView');
        $expr = $queryBuilder->expr();

        return $queryBuilder->select('creatorView')
            ->andWhere($expr->eq('creatorView.item', ':itemId'))
            ->setParameter('itemId', $itemId)
            ->getQuery()
            ->getResult();
    }
}
