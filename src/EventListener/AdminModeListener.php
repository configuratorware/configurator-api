<?php

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class AdminModeListener implements EventSubscriberInterface
{
    /**
     * @var string
     */
    private $adminModeHash;

    /**
     * @return string
     */
    public function getAdminModeHash(): string
    {
        return $this->adminModeHash;
    }

    /**
     * @param string $adminModeHash
     */
    public function setAdminModeHash(string $adminModeHash)
    {
        $this->adminModeHash = $adminModeHash;
    }

    /**
     * Sets admin mode global.
     *
     * @param   RequestEvent $event
     *
     * @throws \Exception
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        $adminModeHash = $request->get(ControlParameters::ADMIN_MODE);

        $adminMode = false;
        if ($adminModeHash === $this->getAdminModeHash()) {
            $adminMode = true;
        }

        if (false === defined('C_ADMIN_MODE')) {
            define('C_ADMIN_MODE', $adminMode);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 17]],
        ];
    }
}
