<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ConfigurationInfo;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationInfo\GetProductionInfoEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationInfo\ConfigurationStructureToConfigurationInfoConverter;

class GetProductionInfoListener
{
    private ConfigurationLoad $configurationLoad;

    private ConfigurationStructureToConfigurationInfoConverter $configurationStructureToConfigurationInfoConverter;

    public function __construct(
        ConfigurationLoad $configurationLoad,
        ConfigurationStructureToConfigurationInfoConverter $configurationStructureToConfigurationInfoConverter
    ) {
        $this->configurationLoad = $configurationLoad;
        $this->configurationStructureToConfigurationInfoConverter = $configurationStructureToConfigurationInfoConverter;
    }

    public function onGetProductionInfo(GetProductionInfoEvent $event)
    {
        $configurationCode = $event->getConfigurationCode();
        $configuration = $this->configurationLoad->loadByCode($configurationCode, $event->getControlParameters());
        $configurationInfo = $this->configurationStructureToConfigurationInfoConverter->convert($configuration, false);

        $event->setConfigurationInfo($configurationInfo);
    }
}
