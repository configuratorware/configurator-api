<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ConfigurationInfo;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationInfo\GetConfigurationInfoEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationInfo\ConfigurationStructureToConfigurationInfoConverter;

class GetConfigurationInfoListener
{
    /**
     * @var ConfigurationLoad
     */
    private $configurationLoad;

    /**
     * @var ConfigurationStructureToConfigurationInfoConverter
     */
    private $configurationStructureToConfigurationInfoConverter;

    /**
     * GetConfigurationInfoListener constructor.
     *
     * @param ConfigurationLoad $configurationLoad
     * @param ConfigurationStructureToConfigurationInfoConverter $configurationStructureToConfigurationInfoConverter
     */
    public function __construct(
        ConfigurationLoad $configurationLoad,
        ConfigurationStructureToConfigurationInfoConverter $configurationStructureToConfigurationInfoConverter
    ) {
        $this->configurationLoad = $configurationLoad;
        $this->configurationStructureToConfigurationInfoConverter = $configurationStructureToConfigurationInfoConverter;
    }

    /**
     * @param GetConfigurationInfoEvent $event
     */
    public function onGetConfigurationInfo(GetConfigurationInfoEvent $event)
    {
        $configurationCode = $event->getConfigurationCode();
        $configuration = $this->configurationLoad->loadByCode($configurationCode);

        if (null === $configuration) {
            throw new NotFoundException();
        }

        $configurationInfo = $this->configurationStructureToConfigurationInfoConverter->convert($configuration, $event->getExtendedData());

        $event->setConfigurationInfo($configurationInfo);
    }
}
