<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ReceiveOffer;

use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\CustomerEmailDataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\OfferRequestCustomerEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\EmailNotSendException;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\CustomerOfferEmail;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\ReceiveOfferSender;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class OfferRequestCustomerEventListener
{
    private ReceiveOfferSender $receiveOfferSender;

    private LoggerInterface $logger;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(ReceiveOfferSender $receiveOfferSender, LoggerInterface $logger, EventDispatcherInterface $eventDispatcher)
    {
        $this->receiveOfferSender = $receiveOfferSender;
        $this->logger = $logger;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function onRequest(OfferRequestCustomerEvent $event): void
    {
        $customerEmail = CustomerOfferEmail::from($event->getClient(), $event->getStructure(), $event->getMailData());

        $mailDataEvent = new CustomerEmailDataCollectionEvent($customerEmail);
        $this->eventDispatcher->dispatch($mailDataEvent, CustomerEmailDataCollectionEvent::NAME);

        if (true === $event->getSendOfferRequestToCustomer()) {
            try {
                $this->receiveOfferSender->sendCustomerEmail($mailDataEvent->getCustomerEmail(), $mailDataEvent->getHtmlTemplate(), $mailDataEvent->getTextTemplate(), $mailDataEvent->getMailAttachments());
            } catch (EmailNotSendException $exception) {
                $this->logger->notice($exception->getMessage(), $exception->getStackTrace());
            }
        }
    }
}
