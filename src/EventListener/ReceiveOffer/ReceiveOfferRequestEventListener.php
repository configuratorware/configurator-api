<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ReceiveOffer;

use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\EmailDataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\ReceiveOfferRequestEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\EmailNotSendException;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\ReceiveOfferEmail;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\ReceiveOfferSender;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ReceiveOfferRequestEventListener
{
    private ReceiveOfferSender $receiveOfferSender;

    private LoggerInterface $logger;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(ReceiveOfferSender $receiveOfferSender, LoggerInterface $logger, EventDispatcherInterface $eventDispatcher)
    {
        $this->receiveOfferSender = $receiveOfferSender;
        $this->logger = $logger;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function onRequest(ReceiveOfferRequestEvent $event): void
    {
        $clientEmail = ReceiveOfferEmail::from($event->getClient(), $event->getReceiveOfferRequestStructure(), $event->getMailData());

        $mailDataEvent = new EmailDataCollectionEvent($clientEmail);
        $this->eventDispatcher->dispatch($mailDataEvent, EmailDataCollectionEvent::NAME);

        try {
            $this->receiveOfferSender->send($mailDataEvent->getReceiveOfferEmail(), $mailDataEvent->getHtmlTemplate(), $mailDataEvent->getTextTemplate(), $mailDataEvent->getMailAttachments());

            $mailSent = true;
        } catch (EmailNotSendException $exception) {
            $this->logger->notice($exception->getMessage(), $exception->getStackTrace());

            $mailSent = false;
        }

        $event->setMailSent($mailSent);
    }
}
