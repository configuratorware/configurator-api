<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Option;

use Redhotmagma\ConfiguratorApiBundle\Events\Option\OptionAddTrafficTrafficLightToStockEvent;

class OptionAddTrafficLightToStockListener
{
    /**
     * @param OptionAddTrafficTrafficLightToStockEvent $event
     */
    public function onAddTrafficLight(OptionAddTrafficTrafficLightToStockEvent $event)
    {
        $stockStructure = $event->getStockStructure();

        if ('not_available' == $stockStructure->status) {
            $stockStructure->traffic_light = 'red';
        } elseif ('limited_availability' == $stockStructure->status) {
            $stockStructure->traffic_light = 'yellow';
        } else {
            $stockStructure->traffic_light = 'green';
        }

        $event->setStockStructure($stockStructure);
    }
}
