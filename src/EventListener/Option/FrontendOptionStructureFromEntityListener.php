<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Option;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Events\Option\FrontendOptionStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Option\OptionAddTrafficTrafficLightToStockEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionDeltapriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\VatCalculationService;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\AttributeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Stock;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class FrontendOptionStructureFromEntityListener
{
    private ItemOptionclassificationOptionDeltapriceRepository $deltaPriceRepository;

    private SettingRepository $settingRepository;

    private StructureFromEntityConverter $structureFromEntityConverter;

    private AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter;

    private VatCalculationService $vatCalculationService;

    private EventDispatcherInterface $eventDispatcher;

    private OptionDetailImageRepositoryInterface $optionDetailImageRepository;

    private OptionThumbnailRepositoryInterface $optionThumbnailRepository;

    public function __construct(
        ItemOptionclassificationOptionDeltapriceRepository $deltaPriceRepository,
        SettingRepository $settingRepository,
        StructureFromEntityConverter $structureFromEntityConverter,
        AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter,
        VatCalculationService $vatCalculationService,
        EventDispatcherInterface $eventDispatcher,
        OptionDetailImageRepositoryInterface $optionDetailImageRepository,
        OptionThumbnailRepositoryInterface $optionThumbnailRepository
    ) {
        $this->deltaPriceRepository = $deltaPriceRepository;
        $this->settingRepository = $settingRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
        $this->vatCalculationService = $vatCalculationService;
        $this->eventDispatcher = $eventDispatcher;
        $this->optionDetailImageRepository = $optionDetailImageRepository;
        $this->optionThumbnailRepository = $optionThumbnailRepository;
    }

    /**
     * @param FrontendOptionStructureFromEntityEvent $event
     */
    public function onFrontendStructureFromEntity(FrontendOptionStructureFromEntityEvent $event)
    {
        $entity = $event->getOptionEntity();
        $structureClassName = $event->getStructureClassName();
        $baseItemIdentifier = $event->getBaseItemIdentifier();
        $optionClassificationIdentifier = $event->getOptionClassificationIdentifier();

        /** @var Option $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure->title = $entity->getTranslatedTitle();
        $structure->abstract = $entity->getTranslatedAbstract();
        $structure->description = $entity->getTranslatedDescription();

        try {
            $structure->detailImage = $this->optionDetailImageRepository->findDetailImage($entity)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }

        try {
            $structure->thumbnail = $this->optionThumbnailRepository->findThumbnail($entity)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }

        $this->addPriceToStructure($structure, $entity, $baseItemIdentifier, $optionClassificationIdentifier);
        $this->addOptionAttributesToStructure($structure, $entity);

        // add stock
        $stockEntity = $entity->getCurrentChannelStock();
        if (!empty($stockEntity)) {
            $stockStructure = $this->structureFromEntityConverter->convertOne($stockEntity, Stock::class);
            $stockStructure->status = $stockEntity->getStockstatus()->getIdentifier();

            // set traffic light
            $event = new OptionAddTrafficTrafficLightToStockEvent($structure, $stockEntity, $stockStructure);
            $this->eventDispatcher->dispatch($event, OptionAddTrafficTrafficLightToStockEvent::NAME);
            $stockStructure = $event->getStockStructure();

            $structure->stock = $stockStructure;
        }

        $itemOptionClassificationOptions = $entity->getItemOptionclassificationOption();
        if (!$itemOptionClassificationOptions->isEmpty()) {
            /** @var ItemOptionclassificationOption $itemOptionClassificationOption */
            foreach ($itemOptionClassificationOptions as $itemOptionClassificationOption) {
                $itemOptionClassification = $itemOptionClassificationOption->getItemOptionclassification();
                if (
                    $itemOptionClassification->getItem()->getIdentifier() === $baseItemIdentifier &&
                    $itemOptionClassification->getOptionclassification()->getIdentifier() === $event->getOptionClassificationIdentifier()
                ) {
                    $structure->amount_is_selectable = $itemOptionClassificationOption->getAmountisselectable();

                    break;
                }
            }
        }

        $event->setOptionStructure($structure);
    }

    /**
     * @param Option $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     * @param string $baseItemIdentifier
     * @param string $optionClassificationIdentifier
     *
     * @return mixed
     */
    protected function addPriceToStructure($structure, $entity, $baseItemIdentifier, $optionClassificationIdentifier = null)
    {
        $calculationMethod = $this->settingRepository->getCalculationMethod();

        $price = ['price' => 0, 'netPrice' => 0];
        if (SettingRepository::DELTA_PRICES === $calculationMethod) {
            $price = $this->deltaPriceRepository->getDeltaPriceByIdentifiers($baseItemIdentifier,
                $optionClassificationIdentifier, $entity->getIdentifier(), C_CHANNEL);
        } elseif (SettingRepository::SUM_OF_ALL === $calculationMethod) {
            if ($entity->getCurrentChannelPrice()) {
                $price['price'] = $entity->getCurrentChannelPrice()->getPrice();
                $price['netPrice'] = $entity->getCurrentChannelPrice()->getPriceNet();
            }
        }
        $this->vatCalculationService->addPricesToStructure($structure, $price['price'], $price['netPrice']);

        return $structure;
    }

    /**
     * @param Option $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     *
     * @return Option
     */
    protected function addOptionAttributesToStructure($structure, $entity)
    {
        $itemAttributes = $entity->getOptionAttribute()->toArray();
        usort($itemAttributes, static function (OptionAttribute $a, OptionAttribute $b) {
            return $a->getSequencenumber() > $b->getSequencenumber() ? 1 : -1;
        });

        $attributesRelationStructures = $this->attributeRelationStructureFromEntityConverter->convertMany($itemAttributes, AttributeRelation::class);

        $structure->attributes = $attributesRelationStructures;

        return $structure;
    }
}
