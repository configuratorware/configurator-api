<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Rules;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\ValueObject\RulesExistCacheKey;
use Symfony\Contracts\Cache\CacheInterface;

class DeleteRulesCacheSubscriber implements EventSubscriber
{
    private ItemRepository $itemRepository;

    private CacheInterface $cachePool;

    public function __construct(ItemRepository $itemRepository, CacheInterface $cachePool)
    {
        $this->itemRepository = $itemRepository;

        $this->cachePool = $cachePool;
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove,
        ];
    }

    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->deleteCachePostUpdate($args);
    }

    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->deleteCachePostUpdate($args);
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $this->deleteCachePostUpdate($args);
    }

    private function deleteCachePostUpdate(LifecycleEventArgs $args): void
    {
        if (!$args->getObject() instanceof Rule) {
            return;
        }

        /** @var Item|null $item */
        $item = $args->getObject()->getItem();

        if (null !== $item) {
            $this->cachePool->delete(RulesExistCacheKey::for($item->getIdentifier()));
        } else {
            $items = $this->itemRepository->findAll();

            foreach ($items as $item) {
                $this->cachePool->delete(RulesExistCacheKey::for($item->getIdentifier()));
            }
        }
    }
}
