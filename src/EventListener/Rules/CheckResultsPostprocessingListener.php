<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Rules;

use Redhotmagma\ConfiguratorApiBundle\Events\Rules\CheckResultsPostprocessingEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckResultsPostprocessing;

class CheckResultsPostprocessingListener
{
    /**
     * @var CheckResultsPostprocessing
     */
    private $checkResultsPostprocessing;

    /**
     * CheckResultsPostprocessingListener constructor.
     *
     * @param CheckResultsPostprocessing $checkResultsPostprocessing
     */
    public function __construct(CheckResultsPostprocessing $checkResultsPostprocessing)
    {
        $this->checkResultsPostprocessing = $checkResultsPostprocessing;
    }

    /**
     * @param CheckResultsPostprocessingEvent $event
     */
    public function onCheckResultsPostprocessing(CheckResultsPostprocessingEvent $event)
    {
        $configuration = $event->getConfiguration();

        $configuration = $this->checkResultsPostprocessing->checkResultsPostprocessing($configuration);

        $event->setConfiguration($configuration);
    }
}
