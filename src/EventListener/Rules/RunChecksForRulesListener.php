<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Rules;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Events\Rules\RunChecksForRulesEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\ActionResolver;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckFactory;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResults;

class RunChecksForRulesListener
{
    /**
     * @var CheckFactory
     */
    private $checkFactory;

    /**
     * @var ActionResolver
     */
    private $actionResolver;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var array|Configuration[]
     */
    private $defaultConfigurations = [];

    /**
     * RunChecksForRulesListener constructor.
     *
     * @param CheckFactory            $checkFactory
     * @param ActionResolver          $actionResolver
     * @param ConfigurationRepository $configurationRepository
     */
    public function __construct(
        CheckFactory $checkFactory,
        ActionResolver $actionResolver,
        ConfigurationRepository $configurationRepository
    ) {
        $this->checkFactory = $checkFactory;
        $this->actionResolver = $actionResolver;
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * @param RunChecksForRulesEvent $event
     */
    public function onRunChecksForRules(RunChecksForRulesEvent $event)
    {
        $configuration = $event->getConfiguration();
        $rules = $event->getRules();
        $reverse = $event->isReverse();

        $checkResultObjects = [];
        $checksStatus = true;

        /** @var Rule $rule */
        foreach ($rules as $rule) {
            $check = $this->checkFactory->create($rule, $reverse);
            $defaultConfiguration = $this->findDefaultConfigurationFor($configuration->item->identifier);

            if (null !== $check && $defaultConfiguration) {
                $checkResult = $check->execute($rule, $configuration, $defaultConfiguration);

                // execute action based on the check result
                // the configuration could have been changed by executing the action
                // use updated configuration for further checks
                $configuration = $this->actionResolver->executeRuleActions($rule, $checkResult, $configuration);

                $checkResultObjects[] = $checkResult;

                if (false === $checkResult->status) {
                    $checksStatus = false;
                }
            }
        }

        $checkResults = new CheckResults();
        $checkResults->status = $checksStatus;
        $checkResults->check_results = $checkResultObjects;
        $checkResults->configuration = $configuration;

        $event->setCheckResults($checkResults);
    }

    /**
     * @param string $itemIdentifier
     *
     * @return Configuration|null
     */
    private function findDefaultConfigurationFor(string $itemIdentifier): ?Configuration
    {
        if (!array_key_exists($itemIdentifier, $this->defaultConfigurations)) {
            $this->defaultConfigurations[$itemIdentifier] = $this->configurationRepository->getBaseconfigurationByItemIdentifier($itemIdentifier);
        }

        return $this->defaultConfigurations[$itemIdentifier];
    }
}
