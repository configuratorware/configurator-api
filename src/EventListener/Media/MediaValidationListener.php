<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Media;

use Redhotmagma\ConfiguratorApiBundle\Events\Media\MediaValidationEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Media\ConstantTrueAuthentication;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class MediaValidationListener
{
    /**
     * @var ConstantTrueAuthentication
     */
    private $mediaAuthentication;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * MediaValidationListener constructor.
     *
     * @param ConstantTrueAuthentication $mediaAuthentication
     * @param RequestStack $requestStack
     */
    public function __construct(ConstantTrueAuthentication $mediaAuthentication, RequestStack $requestStack)
    {
        $this->mediaAuthentication = $mediaAuthentication;
        $this->requestStack = $requestStack;
    }

    /**
     * @param MediaValidationEvent $event
     */
    public function onMediaValidate(MediaValidationEvent $event)
    {
        $auth = $this->mediaAuthentication;
        $request = $this->requestStack->getCurrentRequest();

        if ($auth && !$auth->isAuthenticated($request)) {
            throw new AccessDeniedHttpException();
        }
    }
}
