<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ConfigurationDocument;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationDocument\DataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\DataCollector;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\EditedDesignAreaModifier;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;

class DataCollectionListener
{
    /**
     * @var ConfigurationLoad
     */
    private $configurationLoad;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var DataCollector
     */
    private $dataCollector;

    /**
     * @var EditedDesignAreaModifier
     */
    private $editedDesignAreaModifier;

    /**
     * DataCollectionListener constructor.
     *
     * @param ConfigurationLoad $configurationLoad
     * @param ConfigurationRepository $configurationRepository
     * @param DataCollector $dataCollector
     * @param EditedDesignAreaModifier $editedDesignAreaModifier
     */
    public function __construct(
        ConfigurationLoad $configurationLoad,
        ConfigurationRepository $configurationRepository,
        DataCollector $dataCollector,
        EditedDesignAreaModifier $editedDesignAreaModifier
    ) {
        $this->configurationLoad = $configurationLoad;
        $this->configurationRepository = $configurationRepository;
        $this->dataCollector = $dataCollector;
        $this->editedDesignAreaModifier = $editedDesignAreaModifier;
    }

    /**
     * @param DataCollectionEvent $event
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function onCollection(DataCollectionEvent $event): void
    {
        $fakeAdminModeControlParameters = new ControlParameters([], true);

        $frontendConfiguration = $this->configurationLoad->loadByCode($event->getConfigurationCode(), $fakeAdminModeControlParameters);
        $configurationEntity = $this->configurationRepository->findOneBy(['code' => $event->getConfigurationCode()]);

        if (!$frontendConfiguration || !$configurationEntity) {
            throw new NotFoundException();
        }

        $event->setFrontendConfigurationStructure($frontendConfiguration);
        $event->setConfigurationEntity($configurationEntity);

        $dataSet = $this->dataCollector->create(
            $frontendConfiguration,
            $configurationEntity,
            $event->getDocumentType(),
            $event->getControlParameters()
        );

        $dataSet['editedDesignAreas'] = $this->editedDesignAreaModifier->change($dataSet['editedDesignAreas']);

        $event->setDataSet($dataSet);
    }
}
