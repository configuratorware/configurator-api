<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ConfigurationDocument;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationDocument\DataCollectionEvent;

/**
 * @deprecated Custom translations are now integrated with the Symfony translation component, use the trans() function instead
 */
class CustomTranslationsListener
{
    /**
     * @var string
     */
    private $customTranslationsBasePath;

    /**
     * @param string $customTranslationsBasePath
     */
    public function __construct(string $customTranslationsBasePath)
    {
        $this->customTranslationsBasePath = rtrim($customTranslationsBasePath, '/');
    }

    /**
     * @param DataCollectionEvent $event
     */
    public function onCollection(DataCollectionEvent $event): void
    {
        $dataSet = $event->getDataSet();

        $translations = $this->getLanguageFileContents();

        if (array_key_exists('translations', $dataSet)) {
            $translations = array_replace(
                $dataSet['translations'],
                $translations
            );
        }

        $dataSet['translations'] = $translations;

        $event->setDataSet($dataSet);
    }

    /**
     * @return array
     */
    private function getLanguageFileContents(): array
    {
        $languageFile = $this->customTranslationsBasePath . '/' . C_LANGUAGE_ISO . '.json';

        if (file_exists($languageFile)) {
            return json_decode(file_get_contents($languageFile), true);
        }

        return [];
    }
}
