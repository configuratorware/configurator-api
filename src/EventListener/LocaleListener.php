<?php

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleListener implements EventSubscriberInterface
{
    private LanguageRepository $languageRepository;

    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    /**
     * Sets current Language.
     *
     * There are three ways to do it
     *
     * Use the URL-Param _language (e.g. ?_language=en_GB)
     *
     * @param RequestEvent $event
     *
     * @throws \Exception
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        $request = $event->getRequest();

        $strLocaleParam = $request->get(ControlParameters::LANGUAGE);
        if (null === $strLocaleParam && defined('C_LANGUAGE_ISO')) {
            $strLocaleParam = C_LANGUAGE_ISO;
        }

        /** @var Language|null $language */
        $language = null === $strLocaleParam ? null : $this->languageRepository->findOneByIso($strLocaleParam);

        if (!$language) {
            /** @var Language|null $language */
            $language = $this->languageRepository->findDefault();
        }

        if (!$language) {
            throw new \Exception('Default Language missing');
        }

        if (false === defined('C_LANGUAGE_ISO')) {
            define('C_LANGUAGE_ISO', $language->getIso());
        }

        if (false === defined('C_LANGUAGE_SETTINGS')) {
            define('C_LANGUAGE_SETTINGS', $this->createSettings($language));
        }

        $request->setLocale($language->getIso());
    }

    /**
     * @return array<string, string|array{0: string, 1: int}|list<array{0: string, 1?: int}>>
     */
    public static function getSubscribedEvents(): array
    {
        return [
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => [['onKernelRequest', 17]],
        ];
    }

    private function createSettings(Language $language): array
    {
        return [
            'id' => (string)$language->getId(),
            'iso' => $language->getIso(),
            'dateformat' => $language->getDateformat(),
            'pricedecimals' => $language->getPricedecimals(),
            'decimalpoint' => $language->getDecimalpoint(),
            'thousandsseparator' => $language->getThousandsseparator(),
            'currencysymbolposition' => $language->getCurrencysymbolposition(),
            'user_created_id' => $language->getUserCreatedId(),
            'numberdecimals' => $language->getNumberdecimals(),
        ];
    }
}
