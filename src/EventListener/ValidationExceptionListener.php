<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ValidationExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        /** @var ValidationException $exception */
        $exception = $event->getThrowable();

        if (isset($exception) && ValidationException::class === get_class($exception)) {
            $exceptionContent = new \stdClass();
            $exceptionContent->errors = $exception->getViolations();
            $exceptionContent->success = false;
            $exceptionContent->code = 'validation_error';

            $responseJson = json_encode($exceptionContent);

            $response = new Response($responseJson, 400, [
                'Content-Type' => 'application/json',
            ]);

            $event->setResponse($response);
        }
    }
}
