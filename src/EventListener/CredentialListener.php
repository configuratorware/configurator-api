<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Service\Credential\CredentialChecker;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class CredentialListener
{
    /**
     * @var CredentialChecker
     */
    private $credentialChecker;

    /**
     * @var Security
     */
    private $security;

    /**
     * KernelRequestListener constructor.
     *
     * @param CredentialChecker $credentialChecker
     * @param Security $security
     */
    public function __construct(CredentialChecker $credentialChecker, Security $security)
    {
        $this->credentialChecker = $credentialChecker;
        $this->security = $security;
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $routeCredentials = $event->getRequest()->get('__api_credential', '');

        /** @var User $user */
        $user = $this->security->getUser();

        $isUserAllowed = $this->credentialChecker->check($user, $routeCredentials);

        if (false === $isUserAllowed) {
            throw new AuthenticationException();
        }
    }
}
