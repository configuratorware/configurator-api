<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * @internal
 */
final class AuthenticationFailedListener
{
    public static function onKernelException(ExceptionEvent $exceptionEvent): void
    {
        $exception = $exceptionEvent->getThrowable();
        if ($exception instanceof AuthenticationException) {
            $exceptionEvent->setResponse(new JsonResponse(['code' => 401, 'message' => 'Authentication failed'], Response::HTTP_UNAUTHORIZED));
        }
    }
}
