<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ConfiguratorApiBundle\Exception\MinimumOrderAmountException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class MinimumOrderAmountExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        /** @var MinimumOrderAmountException $exception */
        $exception = $event->getThrowable();

        if (isset($exception) && MinimumOrderAmountException::class === get_class($exception)) {
            $exceptionContent = new \stdClass();
            $exceptionContent->errors = $exception->getViolations();
            $exceptionContent->success = false;
            $exceptionContent->code = 'minimum_order_amount_error';

            $responseJson = json_encode($exceptionContent);

            $response = new Response($responseJson, 400, ['Content-Type' => 'application/json']);

            $event->setResponse($response);
        }
    }
}
