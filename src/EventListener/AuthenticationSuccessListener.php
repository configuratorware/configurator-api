<?php

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserCredentials;

/**
 * Class AuthenticationSuccessListener.
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 *
 * @version 1.0
 */
class AuthenticationSuccessListener
{
    /**
     * @var UserCredentials
     */
    private $userCredentials;

    /**
     * @var LicenseFileValidator
     */
    private $licenseFileValidator;

    /**
     * AuthenticationSuccessListener constructor.
     *
     * @param UserCredentials $userCredentials
     */
    public function __construct(UserCredentials $userCredentials, LicenseFileValidator $licenseFileValidator)
    {
        $this->userCredentials = $userCredentials;
        $this->licenseFileValidator = $licenseFileValidator;
    }

    /**
     * add credential informations to login payload.
     *
     * @param AuthenticationSuccessEvent $event
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
        $data = $event->getData();
        /** @var User $user */
        $user = $event->getUser();

        $data['license'] = $this->licenseFileValidator->getValidProduct();
        $data['credentials'] = $this->userCredentials->getByUser($user);

        $event->setData($data);
    }
}
