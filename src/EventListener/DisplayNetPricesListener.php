<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Class DisplayNetPricesListener.
 */
class DisplayNetPricesListener
{
    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event): void
    {
        if (false === defined('CALCULATION_PRICES_NET')) {
            $request = $event->getRequest();
            $displayNetPrice = $request->get('_display_net_prices');

            if (in_array($displayNetPrice, ['1', 'true'])) {
                define('CALCULATION_PRICES_NET', true);
            } else {
                define('CALCULATION_PRICES_NET', false);
            }
        }
    }
}
