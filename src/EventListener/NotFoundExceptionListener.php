<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class NotFoundExceptionListener
{
    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        /** @var NotFoundException $exception */
        $exception = $event->getThrowable();

        if (isset($exception) && NotFoundException::class === get_class($exception)) {
            $exceptionContent = new \stdClass();
            $exceptionContent->errors = [];
            $exceptionContent->success = false;
            $exceptionContent->code = 'not_found';

            $responseJson = json_encode($exceptionContent);

            $response = new Response($responseJson, 404, [
                'Content-Type' => 'application/json',
            ]);

            $event->setResponse($response);
        }
    }
}
