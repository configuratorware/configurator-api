<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Exception\ImageConversionException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;

class ImageConversionExceptionListener
{
    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof ImageConversionException) {
            $previous = $exception->getPrevious() ?? $exception;

            $this->logger->error($exception->getMessage(), [
                'message' => $previous->getMessage(),
                'source' => sprintf('%s:%s', $previous->getFile(), $previous->getLine()),
            ]);

            $exceptionContent = new \stdClass();
            $exceptionContent->errors = ['image_conversion_error'];
            $exceptionContent->success = false;
            $exceptionContent->code = 'image_conversion_error';

            $response = new JsonResponse($exceptionContent, 400);

            $event->setResponse($response);
        }
    }
}
