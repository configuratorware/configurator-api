<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\CalculationAggregator;

class AggregateDesignerCalculationListener
{
    /**
     * @var CalculationAggregator
     */
    private $calculationAggrgator;

    /**
     * AggregateDesignerCalculationListener constructor.
     *
     * @param CalculationAggregator $calculationAggrgator
     */
    public function __construct(CalculationAggregator $calculationAggrgator)
    {
        $this->calculationAggrgator = $calculationAggrgator;
    }

    /**
     * @param CalculateEvent $event
     */
    public function onCalculate(CalculateEvent $event)
    {
        $calculationResult = $event->getCalculationResult();
        $calculationResult = $this->calculationAggrgator->aggregate($calculationResult);

        $event->setCalculationResult($calculationResult);
    }
}
