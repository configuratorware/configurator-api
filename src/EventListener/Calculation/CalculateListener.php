<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\PriceCalculation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;

class CalculateListener
{
    /**
     * @var PriceCalculation
     */
    private $calculation;

    /**
     * CalculateListener constructor.
     *
     * @param PriceCalculation $calculation
     */
    public function __construct(PriceCalculation $calculation)
    {
        $this->calculation = $calculation;
    }

    /**
     * @param CalculateEvent $event
     */
    public function onCalculate(CalculateEvent $event)
    {
        $calculationResult = new CalculationResult();
        $channelSettings = C_CHANNEL_SETTINGS;

        if (!isset($channelSettings['hidePrices']) || false === $channelSettings['hidePrices']) {
            $configuration = $event->getConfiguration();
            $calculationResult = $this->calculation->calculate($configuration);
        }

        $event->setCalculationResult($calculationResult);
    }
}
