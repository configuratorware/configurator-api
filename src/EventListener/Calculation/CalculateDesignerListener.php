<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\DesignerApi;

class CalculateDesignerListener
{
    private DesignerApi $designerCalculation;

    public function __construct(DesignerApi $designerCalculation)
    {
        $this->designerCalculation = $designerCalculation;
    }

    public function onCalculate(CalculateEvent $event): void
    {
        $channelSettings = C_CHANNEL_SETTINGS;
        if (!isset($channelSettings['hidePrices']) || false === $channelSettings['hidePrices']) {
            // Generate Fake Item From Total
            $designerResult = $this->designerCalculation->fakeItemPosition(
                $event->getCalculationResult(),
                $event->getConfiguration()
            );

            // Get & Add Data for Positions
            $designerResult = $this->designerCalculation->getAllCalculationTypePrices(
                $event->getConfiguration(),
                $designerResult
            );

            // Add Positions to Structure
            $calculationResultStructure = $this->designerCalculation->addPositionsToStructure(
                $event->getCalculationResult(),
                $designerResult
            );

            // Calculate Totals
            $calculationResultStructure = $this->designerCalculation->updateTotals(
                $calculationResultStructure,
                $designerResult
            );

            $event->setCalculationResult($calculationResultStructure);
        }
    }
}
