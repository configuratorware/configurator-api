<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\BulkSavings;

class BulkSavingsCalculationListener
{
    /**
     * @var BulkSavings
     */
    private $bulkSavings;

    /**
     * BulkSavingsCalculationListener constructor.
     *
     * @param BulkSavings $bulkSavings
     */
    public function __construct(BulkSavings $bulkSavings)
    {
        $this->bulkSavings = $bulkSavings;
    }

    /**
     * @param CalculateEvent $event
     */
    public function onCalculate(CalculateEvent $event)
    {
        // only execute bulk savings execution if needed
        // bulk savings service will trigger the calculate event with this flag set to false
        // to prevent recursive execution
        if (true === $event->getCalculateBulkSavings()) {
            $calculationResult = $event->getCalculationResult();
            $configuration = $event->getConfiguration();
            $calculationResult = $this->bulkSavings->addBulkBulkSavingsToCalculationResult(
                $calculationResult, $configuration
            );

            $event->setCalculationResult($calculationResult);
        }
    }
}
