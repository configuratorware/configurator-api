<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\BulkSavings;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\GlobalChannelDiscount;

class GlobalChannelDiscountListener
{
    /**
     * @var BulkSavings
     */
    private $globalChannelDiscount;

    /**
     * BulkSavingsCalculationListener constructor.
     *
     * @param BulkSavings $bulkSavings
     */
    public function __construct(GlobalChannelDiscount $globalChannelDiscount)
    {
        $this->globalChannelDiscount = $globalChannelDiscount;
    }

    /**
     * @param CalculateEvent $event
     */
    public function onCalculate(CalculateEvent $event)
    {
        $calculationResultStructure = $this->globalChannelDiscount->updatePositions(
            $event->getCalculationResult()
        );

        $calculationResultStructure = $this->globalChannelDiscount->updateTotals(
            $event->getCalculationResult()
        );

        $event->setCalculationResult($calculationResultStructure);
    }
}
