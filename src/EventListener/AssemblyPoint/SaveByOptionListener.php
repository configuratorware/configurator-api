<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Events\AssemblyPoint\SaveByOptionEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint\AssemblyPointSave;

class SaveByOptionListener
{
    /**
     * @var AssemblyPointSave
     */
    private $assemblyPointSave;

    /**
     * SaveByOptionListener constructor.
     *
     * @param AssemblyPointSave $assemblyPointSave
     */
    public function __construct(AssemblyPointSave $assemblyPointSave)
    {
        $this->assemblyPointSave = $assemblyPointSave;
    }

    /**
     * @param SaveByOptionEvent $event
     */
    public function onSave(SaveByOptionEvent $event)
    {
        $option = $event->getOption();

        $option = $this->assemblyPointSave->save($option);

        $event->setOption($option);
    }
}
