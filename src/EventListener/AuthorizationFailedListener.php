<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @internal
 */
final class AuthorizationFailedListener
{
    public static function onKernelException(ExceptionEvent $exceptionEvent): void
    {
        $exception = $exceptionEvent->getThrowable();
        if ($exception instanceof AccessDeniedException) {
            $exceptionEvent->setResponse(new JsonResponse(['code' => 403, 'message' => 'Authorization failed'], Response::HTTP_FORBIDDEN));
        }
    }
}
