<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Item;

use Redhotmagma\ConfiguratorApiBundle\Events\Item\GetConfigurableItemsByParentEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus\ItemStatusAvailability;

class GetConfigurableItemsByParentListener
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var FrontendItemStructureFromEntityConverter
     */
    private $frontendItemStructureFromEntityConverter;

    /**
     * @var ItemStatusAvailability
     */
    private $itemStatusAvailability;

    /**
     * GetConfigurableItemsByParentListener constructor.
     *
     * @param ItemRepository $itemRepository
     * @param FrontendItemStructureFromEntityConverter $frontendItemStructureFromEntityConverter
     * @param ItemStatusAvailability $itemStatusAvailability
     */
    public function __construct(
        ItemRepository $itemRepository,
        FrontendItemStructureFromEntityConverter $frontendItemStructureFromEntityConverter,
        ItemStatusAvailability $itemStatusAvailability
    ) {
        $this->itemRepository = $itemRepository;
        $this->frontendItemStructureFromEntityConverter = $frontendItemStructureFromEntityConverter;
        $this->itemStatusAvailability = $itemStatusAvailability;
    }

    /**
     * @param GetConfigurableItemsByParentEvent $event
     */
    public function onGetConfigurableItemsByParent(GetConfigurableItemsByParentEvent $event): void
    {
        $parentIdentifier = $event->getParentIdentifier();

        $items = $this->itemRepository->getItemsByParent($parentIdentifier);

        $structures = [];
        foreach ($items as $item) {
            if ($this->itemStatusAvailability->checkItemAvailability($item)) {
                $structures[] = $this->frontendItemStructureFromEntityConverter->convertOne($item);
            }
        }

        $event->setItemStructures($structures);
    }
}
