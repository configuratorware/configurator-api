<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Item;

use Redhotmagma\ConfiguratorApiBundle\Events\Item\CheckItemAvailabilityEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus\ItemStatusAvailability;

class CheckItemAvailabilityListener
{
    /**
     * @var ItemStatusAvailability
     */
    private $itemStatusAvailability;

    /**
     * CheckItemAvailabilityListener constructor.
     *
     * @param ItemStatusAvailability $itemStatusAvailability
     */
    public function __construct(ItemStatusAvailability $itemStatusAvailability)
    {
        $this->itemStatusAvailability = $itemStatusAvailability;
    }

    /**
     * @param CheckItemAvailabilityEvent $event
     */
    public function onCheckItemAvailability(CheckItemAvailabilityEvent $event): void
    {
        $event->setIsAvailable($this->check($event));
    }

    private function check(CheckItemAvailabilityEvent $event): bool
    {
        $item = $event->getItem();

        if ((!$item->getDeactivated()) && $this->itemStatusAvailability->checkItemAvailability($item)) {
            return true;
        }

        if (null !== $event->getControlParameters()) {
            return $event->getControlParameters()->isAdminMode();
        }

        // this stays for BC compatibility
        if (defined('C_ADMIN_MODE') && C_ADMIN_MODE === true) {
            return true;
        }

        return false;
    }
}
