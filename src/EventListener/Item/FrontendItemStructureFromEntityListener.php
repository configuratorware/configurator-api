<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Item;

use Redhotmagma\ConfiguratorApiBundle\Events\Item\FrontendItemStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback\DesignerFallbackApi;

class FrontendItemStructureFromEntityListener
{
    /**
     * @var DesignerFallbackApi
     */
    private $designerFallbackApi;

    /**
     * @param DesignerFallbackApi $designerFallbackApi
     */
    public function __construct(DesignerFallbackApi $designerFallbackApi)
    {
        $this->designerFallbackApi = $designerFallbackApi;
    }

    /**
     * @param FrontendItemStructureFromEntityEvent $event
     */
    public function onFrontendItemStructureFromEntityConversion(FrontendItemStructureFromEntityEvent $event)
    {
        $itemStructure = $event->getStructure();
        $itemStructure->settings = $this->designerFallbackApi->getCheckedItemSetting($itemStructure);
        $event->setStructure($itemStructure);
    }
}
