<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ChannelClientListener implements EventSubscriberInterface
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * ChannelClientListener constructor.
     *
     * @param ChannelRepository $channelRepository
     * @param ClientRepository $clientRepository
     */
    public function __construct(ChannelRepository $channelRepository, ClientRepository $clientRepository)
    {
        $this->channelRepository = $channelRepository;
        $this->clientRepository = $clientRepository;
    }

    /**
     * Sets current Channel and Client.
     *
     * Use the URL-Param _channel (e.g. ?_channel=_default)
     * if the param is not set get the default channel from the database
     *
     * @param RequestEvent $event
     *
     * @throws \Exception
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        $this->handleChannel($request);
        $this->handleClient($request);
    }

    /**
     * @param Request $request
     *
     * @throws \Exception
     */
    private function handleChannel(Request $request)
    {
        $channel = $request->get(ControlParameters::CHANNEL);

        if (empty($channel)) {
            $channelEntity = $this->channelRepository->findOneBy(['is_default' => 1]);

            if (!empty($channelEntity)) {
                $channel = $channelEntity->getIdentifier();
            }
        } else {
            $channelEntity = $this->channelRepository->findOneBy(['identifier' => $channel]);
        }

        if (empty($channelEntity)) {
            throw new \Exception('Channel not found');
        }

        if (false === defined('C_CHANNEL')) {
            define('C_CHANNEL', $channel);
        }

        if (false === defined('C_CHANNEL_SETTINGS')) {
            $settings = $channelEntity->getSettings();

            if (empty($settings)) {
                $settings = [];
            }

            $settings['id'] = $channelEntity->getId();
            $settings['currencySymbol'] = $channelEntity->getCurrency()->getSymbol();
            $settings['channel'] = $channelEntity->getIdentifier();
            $settings['globalDiscountPercentage'] = $channelEntity->getGlobalDiscountPercentage();
            $settings['hidePrices'] = (bool)$request->get(ControlParameters::HIDE_PRICES, $settings['hidePrices'] ?? null);

            define('C_CHANNEL_SETTINGS', $settings);
        }
    }

    /**
     * @param Request $request
     */
    private function handleClient(Request $request)
    {
        $client = $request->get(ControlParameters::CLIENT);

        if (empty($client)) {
            $client = ClientRepository::DEFAULT_CLIENT_IDENTIFIER;
        }
        $clientEntity = $this->clientRepository->findOneByIdentifier($client);

        if (false === defined('C_CLIENT')) {
            define('C_CLIENT', $client);
        }

        if (false === defined('C_CLIENT_SETTINGS')) {
            $clientSettings = [];
            $clientSettings['theme'] = json_decode(json_encode($clientEntity->getTheme()), true);
            $clientSettings['email'] = $clientEntity->getToEmailAddresses();
            $clientSettings['contactName'] = $clientEntity->getContactName();
            $clientSettings['contactStreet'] = $clientEntity->getContactStreet();
            $clientSettings['contactPostCode'] = $clientEntity->getContactPostCode();
            $clientSettings['contactCity'] = $clientEntity->getContactCity();
            $clientSettings['contactPhone'] = $clientEntity->getContactPhone();
            $clientSettings['contactEmail'] = $clientEntity->getContactEmail();
            $clientSettings['pdfHeader'] = $clientEntity->getPdfHeaderHtml();
            $clientSettings['pdfFooter'] = $clientEntity->getPdfFooterHtml();
            define('C_CLIENT_SETTINGS', $clientSettings);
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 17]],
        ];
    }
}
