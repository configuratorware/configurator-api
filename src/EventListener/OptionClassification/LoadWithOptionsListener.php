<?php

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\OptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Events\OptionClassification\FrontendOptionClassificationsWithOptionsEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification\FrontendOptionClassification;

class LoadWithOptionsListener
{
    private FrontendOptionClassification $frontendOptionClassification;

    public function __construct(FrontendOptionClassification $frontendOptionClassification)
    {
        $this->frontendOptionClassification = $frontendOptionClassification;
    }

    public function onLoadWithOptions(FrontendOptionClassificationsWithOptionsEvent $event): void
    {
        $optionClassifications = $this->frontendOptionClassification->getOptionClassificationStructureForItem($event->getItem(), true);
        $event->setOptionClassifications($optionClassifications);
    }
}
