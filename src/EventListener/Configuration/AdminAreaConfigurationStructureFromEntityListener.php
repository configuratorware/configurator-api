<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\AdminAreaConfigurationStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationInfo\DesignDataInformation;

class AdminAreaConfigurationStructureFromEntityListener
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var ConfigurationStructureFromEntityConverter
     */
    private $configurationStructureFromEntityConverter;

    /**
     * @var DesignDataInformation
     */
    private $designDataInformation;

    /**
     * AdminAreaConfigurationStructureFromEntityListener constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param ConfigurationStructureFromEntityConverter $configurationStructureFromEntityConverter
     * @param DesignDataInformation $designDataInformation
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        ConfigurationStructureFromEntityConverter $configurationStructureFromEntityConverter,
        DesignDataInformation $designDataInformation
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->configurationStructureFromEntityConverter = $configurationStructureFromEntityConverter;
        $this->designDataInformation = $designDataInformation;
    }

    /**
     * @param AdminAreaConfigurationStructureFromEntityEvent $event
     */
    public function onConvertOne(AdminAreaConfigurationStructureFromEntityEvent $event)
    {
        $entity = $event->getConfigurationEntity();
        $structureClassName = $event->getStructureClassName();

        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->configurationStructureFromEntityConverter->addItemToStructure($structure, $entity);
        $this->configurationStructureFromEntityConverter->addClientToStructure($structure, $entity);
        $structure = $this->configurationStructureFromEntityConverter->addOptionClassificationsToStructure($structure, $entity);
        $structure = $this->configurationStructureFromEntityConverter->addInformationToStructure($structure);
        $structure = $this->configurationStructureFromEntityConverter->addPreviewImageURLToStructure($structure);
        $structure = $this->configurationStructureFromEntityConverter->addImagesToStructure($structure);

        // depending on visualization method: add visualization data
        $structure = $this->configurationStructureFromEntityConverter->addVisualizationDataToStructure($structure);

        $structure->designdata = $this->designDataInformation->parseDesignData($entity->getDesigndata());

        $structure->configurationType = $entity->getConfigurationtype()->getIdentifier();

        $event->setConfigurationStructure($structure);
    }
}
