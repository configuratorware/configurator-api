<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\FrontendConfigurationSaveEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DTO\CustomerInfo;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DTO\CustomerInfoFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\FrontendConfigurationSave;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSaveData;

class FrontendConfigurationSaveListener
{
    /**
     * @var CustomerInfoFactory
     */
    private $customerInfoFactory;

    /**
     * @var FrontendConfigurationSave
     */
    private $frontendConfigurationSave;

    /**
     * @param CustomerInfoFactory $customerInfoFactory
     * @param FrontendConfigurationSave $frontendConfigurationSave
     */
    public function __construct(CustomerInfoFactory $customerInfoFactory, FrontendConfigurationSave $frontendConfigurationSave)
    {
        $this->customerInfoFactory = $customerInfoFactory;
        $this->frontendConfigurationSave = $frontendConfigurationSave;
    }

    /**
     * @param FrontendConfigurationSaveEvent $event
     */
    public function onSave(FrontendConfigurationSaveEvent $event): void
    {
        $configurationSaveData = $event->getConfigurationSaveData();
        $calculationResult = $event->getCalculationResult();
        $customerInfo = $this->customerInfoFactory->create($event->getControlParameters());

        $this->updateFromCustomerInfo($configurationSaveData, $customerInfo);
        $configuration = $this->frontendConfigurationSave->save($configurationSaveData, $calculationResult);

        $event->setConfiguration($configuration);
    }

    /**
     * @param ConfigurationSaveData $configurationSaveData
     * @param CustomerInfo $customerInfo
     */
    private function updateFromCustomerInfo(ConfigurationSaveData $configurationSaveData, CustomerInfo $customerInfo): void
    {
        if (!is_string($configurationSaveData->configuration->client)) {
            $configurationSaveData->configuration->client = $customerInfo->clientIdentifier;
        }

        if (!is_string($configurationSaveData->configuration->channel)) {
            $configurationSaveData->configuration->channel = $customerInfo->channelIdentifier;
        }

        if (!is_string($configurationSaveData->configuration->language)) {
            $configurationSaveData->configuration->language = $customerInfo->languageIso;
        }
    }
}
