<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\SwitchOptionEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\VisualizationData;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option as OptionFrontendStructure;

class SwitchOptionListener
{
    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var FrontendOptionStructureFromEntityConverter
     */
    private $frontendOptionStructureFromEntityConverter;

    /**
     * @var VisualizationData
     */
    private $visualizationData;

    /**
     * SwitchOptionListener constructor.
     *
     * @param OptionRepository $optionRepository
     * @param FrontendOptionStructureFromEntityConverter $frontendOptionStructureFromEntityConverter
     * @param VisualizationData $visualizationData
     */
    public function __construct(
        OptionRepository $optionRepository,
        FrontendOptionStructureFromEntityConverter $frontendOptionStructureFromEntityConverter,
        VisualizationData $visualizationData
    ) {
        $this->optionRepository = $optionRepository;
        $this->frontendOptionStructureFromEntityConverter = $frontendOptionStructureFromEntityConverter;
        $this->visualizationData = $visualizationData;
    }

    /**
     * @param SwitchOptionEvent $event
     */
    public function onSwitchOption(SwitchOptionEvent $event)
    {
        $configuration = $event->getConfiguration();
        $switchOptions = $event->getSwitchOptions();
        $addVisualizationDataAtSwitch = $event->getAddVisualizationDataAtSwitch();
        $optionStructureMap = $this->structuresMappedBySwitchOptions($switchOptions, $event->getConfiguration()->item->identifier);

        $removedOptions = [];
        // compare current options with switched options
        foreach ($switchOptions as $optionClassificationIdentifier => $switchedOption) {
            if ($switchedOption instanceof \stdClass) {
                $switchedOption = [$switchedOption];
            }

            foreach ($switchedOption as $switchOptionElement) {
                if (empty($configuration->optionclassifications)) {
                    continue;
                }

                $optionStructure = $optionStructureMap[$optionClassificationIdentifier][$switchOptionElement->identifier];
                $optionStructure->amount = $switchOptionElement->amount;

                if (property_exists($switchOptionElement, 'inputText')) {
                    $optionStructure->inputText = $switchOptionElement->inputText;
                }

                foreach ($configuration->optionclassifications as $optionclassification) {
                    if ($optionclassification->identifier == $optionClassificationIdentifier) {
                        // single select
                        if (false === $optionclassification->is_multiselect) {
                            if (isset($optionclassification->selectedoptions[0])) {
                                $removedOptions[] = $optionclassification->selectedoptions[0]->identifier;
                            }
                            $optionclassification->selectedoptions = [$optionStructure];
                        } // multi select
                        else {
                            if (empty($optionclassification->selectedoptions)) {
                                $optionclassification->selectedoptions = [];
                            }

                            // look for already selected option with the same identifier
                            // if its already there replace it with the given option for updated amount
                            // else just add it to the list
                            $foundAtPosition = false;
                            foreach ($optionclassification->selectedoptions as $i => $selectedoption) {
                                if ($selectedoption->identifier == $optionStructure->identifier) {
                                    $foundAtPosition = $i;

                                    break;
                                }
                            }

                            if (false !== $foundAtPosition) {
                                $optionclassification->selectedoptions[$foundAtPosition] = $optionStructure;
                            } else {
                                $optionclassification->selectedoptions[] = $optionStructure;
                            }
                        }

                        // unset options with amount 0
                        foreach ($optionclassification->selectedoptions as $i => $selectedOption) {
                            if (0 == $selectedOption->amount) {
                                if (isset($optionclassification->selectedoptions[0])) {
                                    $removedOptions[] = $optionclassification->selectedoptions[$i]->identifier;
                                }
                                unset($optionclassification->selectedoptions[$i]);
                            }
                        }

                        $optionclassification->selectedoptions = array_values($optionclassification->selectedoptions);
                    }
                }
            }
        }

        if (true === $addVisualizationDataAtSwitch) {
            $this->visualizationData->addToConfiguration($configuration);
        }

        $event->setRemovedOptions($removedOptions);
        $event->setConfiguration($configuration);
    }

    /**
     * @param array<string,\stdClass>|array<string,\stdClass[]> $switchOptions
     *
     * @return array<string, array<string, OptionFrontendStructure>>
     */
    private function structuresMappedBySwitchOptions(array $switchOptions, string $itemIdentifier): array
    {
        $identifiers = $this->collectOptionIdentifiers($switchOptions);
        $entityMap = $this->optionRepository->findOptionsByIdentifiers($identifiers);

        $map = [];
        foreach ($switchOptions as $optionClassificationIdentifier => $switchedOptions) {
            if ($switchedOptions instanceof \stdClass) {
                $switchedOptions = [$switchedOptions];
            }

            foreach ($switchedOptions as $switchedOption) {
                $map[$optionClassificationIdentifier][(string)$switchedOption->identifier] = $this->frontendOptionStructureFromEntityConverter->convertOne(
                    $entityMap[$switchedOption->identifier],
                    OptionFrontendStructure::class,
                    $itemIdentifier,
                    $optionClassificationIdentifier
                );
            }
        }

        return $map;
    }

    /**
     * Collects the "identifier" fields from the given $switchOptions.
     *
     * @param array<string,\stdClass>|array<string,\stdClass[]> $switchOptions
     *
     * @return string[]
     */
    private function collectOptionIdentifiers(array $switchOptions): array
    {
        $identifiers = [];
        foreach ($switchOptions as $switchedOptions) {
            if ($switchedOptions instanceof \stdClass) {
                $identifiers[] = $switchedOptions->identifier;
            } else {
                foreach ($switchedOptions as $switchOptionElement) {
                    $identifiers[] = $switchOptionElement->identifier;
                }
            }
        }

        return $identifiers;
    }
}
