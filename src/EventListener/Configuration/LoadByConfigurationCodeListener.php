<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\LoadByConfigurationCodeEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\ShareUrlResolver;

class LoadByConfigurationCodeListener
{
    /**
     * @var ConfigurationLoad
     */
    private $configurationLoad;

    /**
     * @var ShareUrlResolver
     */
    private $shareUrl;

    /**
     * LoadByItemIdentifierListener constructor.
     *
     * @param ConfigurationLoad $configurationLoad
     * @param ShareUrlResolver $shareUrl
     */
    public function __construct(ConfigurationLoad $configurationLoad, ShareUrlResolver $shareUrl)
    {
        $this->configurationLoad = $configurationLoad;
        $this->shareUrl = $shareUrl;
    }

    /**
     * @param LoadByConfigurationCodeEvent $event
     */
    public function onLoad(LoadByConfigurationCodeEvent $event): void
    {
        $configurationCode = $event->getConfigurationCode();
        $controlParameters = $event->getControlParameters();

        $configuration = $this->configurationLoad->loadByCode($configurationCode, $controlParameters);
        if (null === $configuration) {
            throw new NotFoundException();
        }

        $configuration->shareUrl = $this->shareUrl->getShareUrl($configuration->code, $controlParameters);

        $event->setConfiguration($configuration);
    }
}
