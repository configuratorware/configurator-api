<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\AddVisualizationDataToStructureEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\VisualizationData;

class AddVisualizationDataToStructureListener
{
    /**
     * @var VisualizationData
     */
    private $visualizationData;

    /**
     * @param VisualizationData $visualizationData
     */
    public function __construct(VisualizationData $visualizationData)
    {
        $this->visualizationData = $visualizationData;
    }

    /**
     * @param AddVisualizationDataToStructureEvent $event
     */
    public function onAddVisualizationData(AddVisualizationDataToStructureEvent $event): void
    {
        $configuration = $event->getConfiguration();
        $this->visualizationData->addToConfiguration($configuration);
        $event->setConfiguration($configuration);
    }
}
