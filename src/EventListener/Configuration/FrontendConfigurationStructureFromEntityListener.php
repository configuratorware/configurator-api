<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\FrontendConfigurationStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

class FrontendConfigurationStructureFromEntityListener
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var ConfigurationStructureFromEntityConverter
     */
    private $configurationStructureFromEntityConverter;

    /**
     * AdminAreaConfigurationStructureFromEntityListener constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param ConfigurationStructureFromEntityConverter $configurationStructureFromEntityConverter
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        ConfigurationStructureFromEntityConverter $configurationStructureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->configurationStructureFromEntityConverter = $configurationStructureFromEntityConverter;
    }

    /**
     * @param FrontendConfigurationStructureFromEntityEvent $event
     */
    public function onConvertOne(FrontendConfigurationStructureFromEntityEvent $event)
    {
        $entity = $event->getConfigurationEntity();
        $structureClassName = $event->getStructureClassName();

        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->configurationStructureFromEntityConverter->addItemToStructure($structure, $entity, $event->getControlParameters());
        $structure = $this->configurationStructureFromEntityConverter->addOptionClassificationsToStructure($structure, $entity);
        $structure = $this->configurationStructureFromEntityConverter->addInformationToStructure($structure);
        $structure = $this->configurationStructureFromEntityConverter->addPreviewImageURLToStructure($structure);
        $structure = $this->configurationStructureFromEntityConverter->addImagesToStructure($structure);

        // depending on visualization method: add visualization data
        /** @var Configuration $structure */
        $structure = $this->configurationStructureFromEntityConverter->addVisualizationDataToStructure($structure);

        $structure->configurationType = $entity->getConfigurationtype()->getIdentifier();

        if (null !== $channel = $entity->getChannel()) {
            $structure->channel = $channel->getIdentifier();
        }

        if (null !== $client = $entity->getClient()) {
            $structure->client = $client->getIdentifier();
        }

        if (null !== $language = $entity->getLanguage()) {
            $structure->language = $language->getIso();
        }

        $event->setConfigurationStructure($structure);
    }
}
