<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use League\Csv\CannotInsertRecord;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\ZipGenerationEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DesignAreaPdfGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ZipGenerator;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ZipGenerationListener
{
    /**
     * @var ZipGenerator
     */
    private $zipGenerator;

    /**
     * @var DesignAreaPdfGenerator
     */
    private $designAreaPdfGenerator;
    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * ZipGenerationListener constructor.
     *
     * @param ConfigurationRepository $configurationRepository
     * @param DesignAreaPdfGenerator $designAreaPdfGenerator
     * @param ZipGenerator $zipGenerator
     */
    public function __construct(
        ConfigurationRepository $configurationRepository,
        DesignAreaPdfGenerator $designAreaPdfGenerator,
        ZipGenerator $zipGenerator
    ) {
        $this->configurationRepository = $configurationRepository;
        $this->designAreaPdfGenerator = $designAreaPdfGenerator;
        $this->zipGenerator = $zipGenerator;
    }

    /**
     * @param ZipGenerationEvent $event
     *
     * @throws CannotInsertRecord
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function onGeneration(ZipGenerationEvent $event): void
    {
        $pdfs = $this->zipGenerator->getPdfs($event->getConfigurationCode(), $event->getControlParameters());
        foreach ($pdfs as $pdf) {
            $event->addFile($pdf);
        }

        $csvs = $this->zipGenerator->getCsv($event->getConfigurationCode());
        foreach ($csvs as $csv) {
            $event->addFile($csv);
        }

        $userImages = $this->zipGenerator->getUserImages($event->getConfigurationCode());
        foreach ($userImages as $image) {
            $event->addFile($image);
        }

        $configuration = $this->configurationRepository->findOneByCode($event->getConfigurationCode());

        $designAreaPdfFiles = $this->designAreaPdfGenerator->generateFiles($configuration);

        foreach ($designAreaPdfFiles as $designAreaPdfFileResult) {
            $event->addFile($designAreaPdfFileResult);
        }
    }
}
