<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\LoadByItemIdentifierEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\ShareUrlResolver;

class LoadByItemIdentifierListener
{
    /**
     * @var ConfigurationLoad
     */
    private $configurationLoad;

    /**
     * @var ShareUrlResolver
     */
    private $shareUrl;

    /**
     * LoadByItemIdentifierListener constructor.
     *
     * @param ConfigurationLoad $configurationLoad
     * @param ShareUrlResolver $shareUrl
     */
    public function __construct(ConfigurationLoad $configurationLoad, ShareUrlResolver $shareUrl)
    {
        $this->configurationLoad = $configurationLoad;
        $this->shareUrl = $shareUrl;
    }

    /**
     * @param LoadByItemIdentifierEvent $event
     */
    public function onLoad(LoadByItemIdentifierEvent $event)
    {
        $controlParameters = $event->getControlParameters();

        $itemIdentifier = $event->getItemIdentifier();
        $configuration = $this->configurationLoad->loadByItemIdentifier($itemIdentifier, $controlParameters);

        if (null !== $controlParameters) {
            $configuration->shareUrl = $this->shareUrl->getShareUrl($configuration->code, $controlParameters);
        }

        $event->setConfiguration($configuration);
    }
}
