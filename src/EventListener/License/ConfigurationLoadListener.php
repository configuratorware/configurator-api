<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\License;

use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\LoadByConfigurationCodeEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\LoadByItemIdentifierEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Symfony\Contracts\EventDispatcher\Event;

class ConfigurationLoadListener
{
    /**
     * @var LicenseFileValidator
     */
    private $licenseFileValidator;

    /**
     * AddLicenseInfoListener constructor.
     *
     * @param LicenseFileValidator $licenseFileValidator
     */
    public function __construct(LicenseFileValidator $licenseFileValidator)
    {
        $this->licenseFileValidator = $licenseFileValidator;
    }

    /**
     * @param LoadByItemIdentifierEvent|LoadByConfigurationCodeEvent|Event $event
     */
    public function addLicenseInfo(Event $event): void
    {
        if (!($event instanceof LoadByConfigurationCodeEvent) && !($event instanceof LoadByItemIdentifierEvent)) {
            return;
        }

        $configuration = $event->getConfiguration();
        if (null === $configuration) {
            return;
        }

        $configuration->license = $this->licenseFileValidator->getValidProduct();
        $event->setConfiguration($configuration);
    }
}
