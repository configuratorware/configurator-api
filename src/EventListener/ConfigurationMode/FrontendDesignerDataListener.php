<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ConfigurationMode;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationMode\FrontendDesignerDataEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\FrontendCreatorDesignerData;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\FrontendDesignerData;

class FrontendDesignerDataListener
{
    private ItemRepository $itemRepository;

    private FrontendCreatorDesignerData $frontendCreatorDesignerData;

    private FrontendDesignerData $frontendDesignerData;

    public function __construct(ItemRepository $itemRepository, FrontendCreatorDesignerData $frontendCreatorDesignerData, FrontendDesignerData $frontendDesignerData)
    {
        $this->itemRepository = $itemRepository;
        $this->frontendCreatorDesignerData = $frontendCreatorDesignerData;
        $this->frontendDesignerData = $frontendDesignerData;
    }

    public function onGetDesignerData(FrontendDesignerDataEvent $event): void
    {
        $configuration = $event->getConfiguration();

        $itemIdentifier = $configuration->item->identifier;

        $item = $this->itemRepository->findOneForConfigurationMode($itemIdentifier);

        if (null === $item) {
            throw new NotFoundException();
        }

        if (null !== $item->getParent()) {
            $item = $item->getParent();
        }

        $configurationMode = $item->getConfigurationMode();

        if (Item::CONFIGURATION_MODE_CREATOR_DESIGNER === $configurationMode) {
            $event->setDesignerData($this->frontendCreatorDesignerData->provideForItem($item));
        } else {
            $event->setDesignerData($this->frontendDesignerData->provideForItem($item));
        }
    }
}
