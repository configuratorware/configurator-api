<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener;

use Redhotmagma\ApiBundle\Service\DefaultApiServices\GetResponseService;
use Redhotmagma\ApiBundle\Structure\Listresult\ListResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Event\ViewEvent;

class KernelViewListener
{
    /**
     * @var GetResponseService
     */
    private $responseService;

    public function __construct(GetResponseService $responseService)
    {
        $this->responseService = $responseService;
    }

    /**
     * @param ViewEvent $event
     */
    public function onKernelView(ViewEvent $event)
    {
        $controllerResult = $event->getControllerResult();

        // transform a pagination result into a list result structure
        if ($controllerResult instanceof PaginationResult) {
            $response = $this->responseService->getListJsonResponse(
                $controllerResult->data,
                $controllerResult->count,
                ListResult::class
            );
        } elseif ($controllerResult instanceof FileResult) {
            $response = new BinaryFileResponse($controllerResult->filePath);

            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_ATTACHMENT,
                $controllerResult->fileName
            );
        } else {
            $response = $this->responseService->getJsonResponse($controllerResult);
        }

        $event->setResponse($response);
    }
}
