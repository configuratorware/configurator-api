<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\EventListener\ConfigurationVariant;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationVariant\GetFrontendConfigurationVariantsEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant\FrontendConfigurationVariants;

class GetFrontendConfigurationVariantsListener
{
    /**
     * @var FrontendConfigurationVariants
     */
    private $frontendConfigurationVariants;

    /**
     * GetFrontendConfigurationVariantsListener constructor.
     *
     * @param FrontendConfigurationVariants $frontendConfigurationVariants
     */
    public function __construct(FrontendConfigurationVariants $frontendConfigurationVariants)
    {
        $this->frontendConfigurationVariants = $frontendConfigurationVariants;
    }

    /**
     * @param GetFrontendConfigurationVariantsEvent $event
     */
    public function onGetFrontendConfigurationVariants(GetFrontendConfigurationVariantsEvent $event)
    {
        $itemIdentifier = $event->getItemIdentifier();
        $configurationVariants = $this->frontendConfigurationVariants->getConfigurationVariantsForItem($itemIdentifier);

        $event->setConfigurationVariants($configurationVariants);
    }
}
