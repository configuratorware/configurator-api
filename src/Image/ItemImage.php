<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

/**
 * @internal
 */
abstract class ItemImage
{
    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $itemIdentifier;

    /**
     * @param string $url
     * @param string $identifier
     */
    protected function __construct(string $url, string $identifier)
    {
        $this->url = $url;
        $this->itemIdentifier = $identifier;
    }

    /**
     * @return array<string, string>
     */
    public function toMap(): array
    {
        return [$this->itemIdentifier => $this->url];
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
