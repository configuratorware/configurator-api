<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

/**
 * @internal
 */
abstract class OptionImage
{
    public const PLACEHOLDER_OPTION_IDENTIFIER = '{optionIdentifier}';
    public const PLACEHOLDER_IMAGE_EXT = '{ext}';

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string
     */
    private $optionIdentifier;

    /**
     * @var string
     */
    private $realPath;

    /**
     * @var string
     */
    private $filename;

    /**
     * @param string|null $url
     * @param string $optionIdentifier
     * @param string $realPath
     * @param string $filename
     */
    protected function __construct(?string $url, string $optionIdentifier, string $realPath, string $filename)
    {
        $this->url = $url;
        $this->optionIdentifier = $optionIdentifier;
        $this->realPath = $realPath;
        $this->filename = $filename;
    }

    /**
     * @return array<string, string|null>
     */
    public function toMap(): array
    {
        return [$this->optionIdentifier => $this->url];
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getRealPath(): string
    {
        return $this->realPath;
    }

    /**
     * @param string $optionIdentifier
     *
     * @return bool
     */
    public function isForOption(string $optionIdentifier): bool
    {
        return $this->optionIdentifier === $optionIdentifier;
    }

    /**
     * @param $pattern
     *
     * @return string
     */
    public function createStorePath($pattern): string
    {
        $ext = pathinfo($this->filename, PATHINFO_EXTENSION);

        $patternMatchers = [
            self::PLACEHOLDER_OPTION_IDENTIFIER => $this->optionIdentifier,
            self::PLACEHOLDER_IMAGE_EXT => $ext,
        ];

        return strtr($pattern, $patternMatchers);
    }
}
