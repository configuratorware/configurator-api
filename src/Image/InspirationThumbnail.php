<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

final class InspirationThumbnail
{
    public const PLACEHOLDER_INSPIRATION_ID = '{inspirationId}';
    public const PLACEHOLDER_ITEM_IDENTIFIER = '{itemIdentifier}';
    public const PLACEHOLDER_IMAGE_EXT = '{ext}';

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var int
     */
    private $inspirationId;

    /**
     * @var string
     */
    private $itemIdentifier;

    /**
     * @var string
     */
    private $realPath;

    /**
     * @var string
     */
    private $filename;

    /**
     * InspirationThumbnail constructor.
     *
     * @param string|null $url
     * @param int $inspirationId
     * @param string $itemIdentifier
     * @param string $realPath
     * @param string $filename
     */
    public function __construct(?string $url, int $inspirationId, string $itemIdentifier, string $realPath, string $filename)
    {
        $this->url = $url;
        $this->inspirationId = $inspirationId;
        $this->itemIdentifier = $itemIdentifier;
        $this->realPath = $realPath;
        $this->filename = $filename;
    }

    /**
     * @param string|null $url
     * @param int $inspirationId
     * @param string $itemIdentifier
     * @param string $realPath
     * @param string $filename
     *
     * @return static
     */
    public static function from(?string $url, int $inspirationId, string $itemIdentifier, string $realPath, string $filename): self
    {
        return new self($url, $inspirationId, $itemIdentifier, $realPath, $filename);
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getRealPath(): string
    {
        return $this->realPath;
    }

    /**
     * @param $pattern
     *
     * @return string
     */
    public function createStorePath($pattern): string
    {
        $ext = pathinfo($this->filename, PATHINFO_EXTENSION);

        $patternMatchers = [
            self::PLACEHOLDER_INSPIRATION_ID => $this->inspirationId,
            self::PLACEHOLDER_ITEM_IDENTIFIER => $this->itemIdentifier,
            self::PLACEHOLDER_IMAGE_EXT => $ext,
        ];

        return strtr($pattern, $patternMatchers);
    }
}
