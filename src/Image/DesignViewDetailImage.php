<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

final class DesignViewDetailImage
{
    public const PLACEHOLDER_PARENT_ITEM_IDENTIFIER = '{parentItemIdentifier}';
    public const PLACEHOLDER_CHILD_ITEM_IDENTIFIER = '{childItemIdentifier}';
    public const PLACEHOLDER_VIEW_IDENTIFIER = '{viewIdentifier}';
    public const PLACEHOLDER_IMAGE_EXT = '{ext}';

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string
     */
    private $designViewIdentifier;

    /**
     * @var string
     */
    private $parentIdentifier;

    /**
     * @var string
     */
    private $childIdentifier;

    /**
     * @var string
     */
    private $realPath;

    /**
     * @var string
     */
    private $filename;

    /**
     * @param string|null $url
     * @param string $designViewIdentifier
     * @param string $parentIdentifier
     * @param string $childIdentifier
     * @param string $realPath
     * @param string $filename
     */
    public function __construct(?string $url, string $designViewIdentifier, string $parentIdentifier, string $childIdentifier, string $realPath, string $filename)
    {
        $this->url = $url;
        $this->designViewIdentifier = $designViewIdentifier;
        $this->parentIdentifier = $parentIdentifier;
        $this->childIdentifier = $childIdentifier;
        $this->realPath = $realPath;
        $this->filename = $filename;
    }

    /**
     * @param string|null $url
     * @param string $designViewIdentifier
     * @param string $parentIdentifier
     * @param string $childIdentifier
     * @param string $realPath
     * @param string $filename
     *
     * @return static
     */
    public static function from(?string $url, string $designViewIdentifier, string $parentIdentifier, string $childIdentifier, string $realPath, string $filename): self
    {
        return new self($url, $designViewIdentifier, $parentIdentifier, $childIdentifier, $realPath, $filename);
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getChildIdentifier(): string
    {
        return $this->childIdentifier;
    }

    /**
     * @return string
     */
    public function getRealPath(): string
    {
        return $this->realPath;
    }

    /**
     * @param string $pattern
     *
     * @return string
     */
    public function createStorePath(string $pattern): string
    {
        $ext = pathinfo($this->filename, PATHINFO_EXTENSION);

        $patternMatchers = [
            self::PLACEHOLDER_PARENT_ITEM_IDENTIFIER => $this->parentIdentifier,
            self::PLACEHOLDER_CHILD_ITEM_IDENTIFIER => $this->childIdentifier,
            self::PLACEHOLDER_VIEW_IDENTIFIER => $this->designViewIdentifier,
            self::PLACEHOLDER_IMAGE_EXT => $ext,
        ];

        return strtr($pattern, $patternMatchers);
    }
}
