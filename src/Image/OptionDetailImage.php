<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

final class OptionDetailImage extends OptionImage
{
    /**
     * @param string|null $url
     * @param string $optionIdentifier
     * @param string $realPath
     * @param string $filename
     *
     * @return static
     */
    public static function from(?string $url, string $optionIdentifier, string $realPath, string $filename): self
    {
        return new self($url, $optionIdentifier, $realPath, $filename);
    }
}
