<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

final class LayerImage
{
    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string
     */
    private $optionIdentifier;

    /**
     * @var string
     */
    private $componentIdentifier;

    /**
     * @var string
     */
    private $viewIdentifier;

    /**
     * @var string
     */
    private $realPath;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var int|null
     */
    private $componentSequenceNumber;

    /**
     * @var string
     */
    private $viewSequenceNumber;

    /**
     * @param string|null $url
     * @param string $componentIdentifier
     * @param string $optionIdentifier
     * @param string $viewIdentifier
     * @param string $realPath
     * @param string $filename
     * @param int|null $componentSequenceNumber
     * @param string $viewSequenceNumber
     */
    private function __construct(?string $url, string $componentIdentifier, string $optionIdentifier, string $viewIdentifier, string $realPath, string $filename, ?int $componentSequenceNumber, string $viewSequenceNumber)
    {
        $this->url = $url;
        $this->componentIdentifier = $componentIdentifier;
        $this->optionIdentifier = $optionIdentifier;
        $this->viewIdentifier = $viewIdentifier;
        $this->realPath = $realPath;
        $this->filename = $filename;
        $this->componentSequenceNumber = $componentSequenceNumber;
        $this->viewSequenceNumber = $viewSequenceNumber;
    }

    /**
     * @param string|null $url
     * @param string $componentIdentifier
     * @param string $optionIdentifier
     * @param string $viewIdentifier
     * @param string $realPath
     * @param string $filename
     * @param int|null $componentSequenceNumber
     * @param string $viewSequenceNumber
     *
     * @return self
     */
    public static function from(?string $url, string $componentIdentifier, string $optionIdentifier, string $viewIdentifier, string $realPath, string $filename, ?int $componentSequenceNumber, string $viewSequenceNumber): self
    {
        return new self($url, $componentIdentifier, $optionIdentifier, $viewIdentifier, $realPath, $filename, $componentSequenceNumber, $viewSequenceNumber);
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getComponentIdentifier(): string
    {
        return $this->componentIdentifier;
    }

    /**
     * @return int|null
     */
    public function getComponentSequenceNumber(): ?int
    {
        return $this->componentSequenceNumber;
    }

    /**
     * @return string
     */
    public function getOptionIdentifier(): string
    {
        return $this->optionIdentifier;
    }

    /**
     * @return string
     */
    public function getViewIdentifier(): string
    {
        return $this->viewIdentifier;
    }

    /**
     * @return string
     */
    public function getViewSequenceNumber(): string
    {
        return $this->viewSequenceNumber;
    }

    /**
     * @return string
     */
    public function getRealPath(): string
    {
        return $this->realPath;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }
}
