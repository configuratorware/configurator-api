<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

final class ItemThumbnail extends ItemImage
{
    /**
     * @param string $url
     * @param string $itemIdentifier
     *
     * @return static
     */
    public static function from(string $url, string $itemIdentifier): self
    {
        return new self($url, $itemIdentifier);
    }
}
