<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

final class ComponentThumbnail
{
    public const PLACEHOLDER_COMPONENT_IDENTIFIER = '{componentIdentifier}';
    public const PLACEHOLDER_IMAGE_EXT = '{ext}';

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string
     */
    private $componentIdentifier;

    /**
     * @var string
     */
    private $realPath;

    /**
     * @var string
     */
    private $filename;

    /**
     * @param string|null $url
     * @param string $componentIdentifier
     * @param string $realPath
     * @param string $filename
     */
    private function __construct(?string $url, string $componentIdentifier, string $realPath, string $filename)
    {
        $this->url = $url;
        $this->componentIdentifier = $componentIdentifier;
        $this->realPath = $realPath;
        $this->filename = $filename;
    }

    /**
     * @param string|null $url
     * @param string $componentIdentifier
     * @param string $realPath
     * @param string $fileName
     *
     * @return static
     */
    public static function from(?string $url, string $componentIdentifier, string $realPath, string $fileName): self
    {
        return new self($url, $componentIdentifier, $realPath, $fileName);
    }

    /**
     * @return array<string, string|null>
     */
    public function toMap(): array
    {
        return [$this->componentIdentifier => $this->url];
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getRealPath(): string
    {
        return $this->realPath;
    }

    /**
     * @param string $componentIdentifier
     *
     * @return bool
     */
    public function isForComponent(string $componentIdentifier): bool
    {
        return $this->componentIdentifier === $componentIdentifier;
    }

    /**
     * @param $pattern
     *
     * @return string
     */
    public function createStorePath($pattern): string
    {
        $ext = pathinfo($this->filename, PATHINFO_EXTENSION);

        $patternMatchers = [
            self::PLACEHOLDER_COMPONENT_IDENTIFIER => $this->componentIdentifier,
            self::PLACEHOLDER_IMAGE_EXT => $ext,
        ];

        return strtr($pattern, $patternMatchers);
    }
}
