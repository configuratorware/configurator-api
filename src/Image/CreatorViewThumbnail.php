<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Image;

final class CreatorViewThumbnail
{
    public const PLACEHOLDER_ITEM_IDENTIFIER = '{itemIdentifier}';
    public const PLACEHOLDER_VIEW_IDENTIFIER = '{viewIdentifier}';
    public const PLACEHOLDER_IMAGE_EXT = '{ext}';

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string
     */
    private $creatorViewIdentifier;

    /**
     * @var string
     */
    private $itemIdentifier;

    /**
     * @var string
     */
    private $realPath;

    /**
     * @var string
     */
    private $filename;

    /**
     * @param string|null $url
     * @param string $creatorViewIdentifier
     * @param string $itemIdentifier
     * @param string $realPath
     * @param string $filename
     */
    private function __construct(?string $url, string $creatorViewIdentifier, string $itemIdentifier, string $realPath, string $filename)
    {
        $this->url = $url;
        $this->creatorViewIdentifier = $creatorViewIdentifier;
        $this->itemIdentifier = $itemIdentifier;
        $this->realPath = $realPath;
        $this->filename = $filename;
    }

    /**
     * @param string|null $url
     * @param string $creatorViewIdentifier
     * @param string $itemIdentifier
     * @param string $realPath
     * @param string $filename
     *
     * @return static
     */
    public static function from(?string $url, string $creatorViewIdentifier, string $itemIdentifier, string $realPath, string $filename): self
    {
        return new self($url, $creatorViewIdentifier, $itemIdentifier, $realPath, $filename);
    }

    /**
     * @return array<string, self>
     */
    public function toCreatorViewIdentifierMap(): array
    {
        return [$this->creatorViewIdentifier => $this];
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getRealPath(): string
    {
        return $this->realPath;
    }

    /**
     * @param string $creatorViewIdentifier
     *
     * @return bool
     */
    public function isForCreatorView(string $creatorViewIdentifier): bool
    {
        return $this->creatorViewIdentifier === $creatorViewIdentifier;
    }

    /**
     * @param string $pattern
     *
     * @return string
     */
    public function createStorePath(string $pattern): string
    {
        $ext = pathinfo($this->filename, PATHINFO_EXTENSION);

        $patternMatchers = [
          self::PLACEHOLDER_ITEM_IDENTIFIER => $this->itemIdentifier,
          self::PLACEHOLDER_VIEW_IDENTIFIER => $this->creatorViewIdentifier,
          self::PLACEHOLDER_IMAGE_EXT => $ext,
        ];

        return strtr($pattern, $patternMatchers);
    }
}
