<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\TwigExtensions;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class ConfiguratorApiExtension extends AbstractExtension
{
    /**
     * @var Base64Encode
     */
    private $base64Encode;

    /**
     * @param Base64Encode $base64Encode
     */
    public function __construct(Base64Encode $base64Encode)
    {
        $this->base64Encode = $base64Encode;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('base64_encode', $this->base64Encode),
        ];
    }
}
