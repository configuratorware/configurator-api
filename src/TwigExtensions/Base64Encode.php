<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\TwigExtensions;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * @internal
 */
final class Base64Encode
{
    private const CHECK_MARK = 'data';
    private const PLACEHOLDER = '{mimeType}';
    private const META_DATA = 'data:{mimeType};base64,';

    /**
     * @var MimeTypeGuesserInterface
     */
    private $mimeTypeGuesser;

    /**
     * @param MimeTypeGuesserInterface $mimeTypeGuesser
     */
    public function __construct(MimeTypeGuesserInterface $mimeTypeGuesser)
    {
        $this->mimeTypeGuesser = $mimeTypeGuesser;
    }

    public function __invoke(string $fileName): string
    {
        if (0 === strpos($fileName, self::CHECK_MARK)) {
            // is already base64encoded
            return $fileName;
        }

        if (!file_exists($fileName)) {
            throw FileException::notFound($fileName);
        }

        $mimeType = $this->mimeTypeGuesser->guessMimeType($fileName);
        if (null === $mimeType) {
            throw FileException::cannotGuessMimeType($fileName);
        }

        $metaData = str_replace(self::PLACEHOLDER, $mimeType, self::META_DATA);

        return $metaData . base64_encode(file_get_contents($fileName));
    }
}
