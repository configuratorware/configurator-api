<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FrontendTranslationFileUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Validator\FrontendTranslationFileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class FrontendTranslationFileUploadResolver implements ArgumentValueResolverInterface
{
    /**
     * @var FrontendTranslationFileValidator
     */
    private $translationValidator;

    /**
     * TranslationUploadResolver constructor.
     *
     * @param FrontendTranslationFileValidator $translationValidator
     */
    public function __construct(
        FrontendTranslationFileValidator $translationValidator
    ) {
        $this->translationValidator = $translationValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return FrontendTranslationFileUploadArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $structure = new FrontendTranslationFileUploadArguments();
        $structure->uploadedFile = $request->files->get('translation');

        if (!$structure->uploadedFile) {
            throw new \InvalidArgumentException('"translation" file is missing');
        }

        if ('application/json' !== $structure->uploadedFile->getClientMimeType()) {
            throw new ValidationException(['Wrong file type, application/json expected']);
        }

        $translationContents = file_get_contents($structure->uploadedFile->getPathname());
        $this->translationValidator->validate($request->get('iso'), $translationContents);

        yield $structure;
    }
}
