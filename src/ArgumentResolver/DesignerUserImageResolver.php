<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequest as DesignerUserImageRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\UserImageEditDataValidator;
use Redhotmagma\ConfiguratorApiBundle\Validator\UserImageValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignerUserImageResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var UserImageEditDataValidator
     */
    private $userImageEditDataValidator;

    /**
     * @var UserImageValidator
     */
    private $userImageValidator;

    /**
     * DesignerUserImageResolver constructor.
     *
     * @param UserImageEditDataValidator $userImageEditDataValidator
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param UserImageValidator $userImageValidator
     */
    public function __construct(
        UserImageEditDataValidator $userImageEditDataValidator,
        StructureFromDataConverter $structureFromDataConverter,
        UserImageValidator $userImageValidator
    ) {
        $this->userImageEditDataValidator = $userImageEditDataValidator;
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->userImageValidator = $userImageValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return DesignerUserImageRequestStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        /** @var string $postData */
        $postData = (string)$request->request->get('userImageEditData');
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, DesignerUserImageRequestStructure::class);

        $structure->uploadedFile = $request->files->get('userImage');

        $this->userImageEditDataValidator->validate($structure);

        if ($structure->uploadedFile) {
            $this->userImageValidator->validate($structure->uploadedFile);
        }

        yield $structure;
    }
}
