<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ImageGalleryImageResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * ImageGalleryImageResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(StructureFromDataConverter $structureFromDataConverter)
    {
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ImageGalleryImage::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, ImageGalleryImage::class);

        yield $structure;
    }
}
