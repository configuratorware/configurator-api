<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\Inspiration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
final class InspirationResolver implements ArgumentValueResolverInterface
{
    /**
     * @var RequestResolver
     */
    private $requestResolver;

    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @param RequestResolver $requestResolver
     * @param StructureValidator $structureValidator
     */
    public function __construct(RequestResolver $requestResolver, StructureValidator $structureValidator)
    {
        $this->requestResolver = $requestResolver;
        $this->structureValidator = $structureValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return Inspiration::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $structure = $this->requestResolver->resolveToStructure($request, Inspiration::class);

        $violations = $this->structureValidator->validate($structure);

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }

        yield $structure;
    }
}
