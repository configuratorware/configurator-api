<?php

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ControlParametersResolver implements ArgumentValueResolverInterface
{
    /**
     * @var string
     */
    private $adminModeHash;

    /**
     * @param string $adminModeHash
     */
    public function __construct(string $adminModeHash)
    {
        $this->adminModeHash = $adminModeHash;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ControlParameters::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $parameters = $request->query->all();

        $filteredParameters = [];

        foreach ($parameters as $key => $parameter) {
            if (0 === mb_strpos($key, '_')) {
                $filteredParameters[$key] = $parameter;
            }
        }

        if (isset($parameters[ControlParameters::ADMIN_MODE])) {
            $isAdminMode = $parameters[ControlParameters::ADMIN_MODE] === $this->adminModeHash;
        } else {
            $isAdminMode = false !== strpos($request->getPathInfo(), '/api');
        }

        if (isset($filteredParameters[ControlParameters::SHARE_URL])) {
            $filteredParameters[ControlParameters::SHARE_URL] = urldecode($filteredParameters[ControlParameters::SHARE_URL]);
        }

        yield new ControlParameters($filteredParameters, $isAdminMode);
    }
}
