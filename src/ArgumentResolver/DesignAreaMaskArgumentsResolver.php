<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DesignAreaMaskArguments;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignAreaMaskArgumentsValidator;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignAreaMaskArgumentsResolver implements ArgumentValueResolverInterface
{
    /**
     * @var DesignAreaMaskArgumentsValidator
     */
    private $designAreaMaskArgumentsValidator;

    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * @param DesignAreaMaskArgumentsValidator $designAreaMaskArgumentsValidator
     * @param FileValidator $fileValidator
     */
    public function __construct(
        DesignAreaMaskArgumentsValidator $designAreaMaskArgumentsValidator,
        FileValidator $fileValidator
    ) {
        $this->designAreaMaskArgumentsValidator = $designAreaMaskArgumentsValidator;
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return DesignAreaMaskArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $structure = new DesignAreaMaskArguments();
        $structure->designAreaId = $request->get('designAreaId');
        $structure->designProductionMethodId = $request->get('designProductionMethodId');
        $structure->uploadedFile = $request->files->get('mask');

        $this->designAreaMaskArgumentsValidator->validate($structure);

        $allowedMimeTypes = ['image/svg+xml'];

        $this->fileValidator->validate($structure->uploadedFile, $allowedMimeTypes);

        yield $structure;
    }
}
