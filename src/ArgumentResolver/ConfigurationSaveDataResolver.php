<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\Service;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSaveData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ConfigurationSaveDataResolver implements ArgumentValueResolverInterface
{
    /**
     * @var Service
     */
    private $service;

    /**
     * ConfigurationResolver constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConfigurationSaveData::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();

        if (empty($postData)) {
            throw new \InvalidArgumentException('The post content should not be empty to resolve Configuration');
        }

        $data = json_decode($postData);

        $configurationData = $data;
        if (!empty($data->configuration)) {
            $configurationData = $data->configuration;
        }

        $configuration = $this->service->getStructureFromData($configurationData, Configuration::class);

        $configurationSaveData = new ConfigurationSaveData();
        $configurationSaveData->configuration = $configuration;
        $configurationSaveData->configurationType = $data->configurationtype;

        $thumbnail = null;
        if (!empty($data->thumbnail)) {
            $thumbnail = $data->thumbnail;
        }
        $configurationSaveData->thumbnail = $thumbnail;

        $screenshots = [];
        if (!empty($data->configuration->screenshots)) {
            $screenshots = $data->configuration->screenshots;
        }
        $configurationSaveData->screenshots = $screenshots;

        $designDataSnapshots = [];
        if (!empty($data->configuration->designDataSnapshots)) {
            $designDataSnapshots = $data->configuration->designDataSnapshots;
        }
        $configurationSaveData->designDataSnapshots = $designDataSnapshots;

        $printDesignData = [];
        if (!empty($data->configuration->printDesignData)) {
            $printDesignData = $data->configuration->printDesignData;
        }
        $configurationSaveData->printDesignData = $printDesignData;

        $printFiles = [];
        if (!empty($data->printfiles)) {
            $printFiles = $data->printfiles;
        }
        $configurationSaveData->printFiles = $printFiles;

        yield $configurationSaveData;
    }
}
