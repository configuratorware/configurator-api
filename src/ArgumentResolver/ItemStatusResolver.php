<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ItemStatusResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * ItemStatusResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(StructureFromDataConverter $structureFromDataConverter)
    {
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ItemStatus::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = (string)$request->getContent();
        $data = json_decode($postData);

        if (empty($data) || !isset($data->itemStatusId) || !is_numeric($data->itemStatusId)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $object = new \stdClass();
        $object->id = $data->itemStatusId;
        $structure = $this->structureFromDataConverter->convert($object, ItemStatus::class);

        yield $structure;
    }
}
