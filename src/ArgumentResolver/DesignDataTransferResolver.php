<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignDataTransfer;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignDataTransferValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignDataTransferResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var DesignDataTransferValidator
     */
    private $designDataTransferValidator;

    /**
     * DesignDataTransferResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param DesignDataTransferValidator $designDataTransferValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        DesignDataTransferValidator $designDataTransferValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->designDataTransferValidator = $designDataTransferValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return DesignDataTransfer::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \Exception
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, DesignDataTransfer::class);

        $this->designDataTransferValidator->validate($structure);

        yield $structure;
    }
}
