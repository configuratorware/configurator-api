<?php

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\ExportRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ExportRequestResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ExportRequest::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $data = json_decode($request->getContent(), true);

        if (!isset($data['item_identifiers']) || !is_array($data['item_identifiers'])) {
            throw new BadRequestHttpException('item_identifiers parameter is required and must be an array');
        }

        $itemIdentifiers = $data['item_identifiers'];
        $combine = $data['combine'] ?? false;

        yield new ExportRequest($itemIdentifiers, $combine);
    }
}
