<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Validator\QuestionTreeValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class QuestionTreeResolver implements ArgumentValueResolverInterface
{
    private QuestionTreeValidator $validator;

    private RequestResolver $requestResolver;

    public function __construct(QuestionTreeValidator $validator, RequestResolver $requestResolver)
    {
        $this->validator = $validator;
        $this->requestResolver = $requestResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return QuestionTree::class === $argument->getType();
    }

    /**
     * @param Request $request
     * @param ArgumentMetadata $argument
     *
     * @return \Generator
     *
     * @throws \Exception
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        /** @var QuestionTree $structure */
        $structure = $this->requestResolver->resolveToStructure($request, QuestionTree::class);

        $this->validator->validate($structure);

        yield $structure;
    }
}
