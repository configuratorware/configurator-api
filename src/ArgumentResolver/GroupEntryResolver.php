<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Validator\GroupEntryValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class GroupEntryResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var GroupEntryValidator
     */
    private $groupEntryValidator;

    /**
     * GroupEntryResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param GroupEntryValidator $groupEntryValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        GroupEntryValidator $groupEntryValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->groupEntryValidator = $groupEntryValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return GroupEntry::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, GroupEntry::class);

        $this->groupEntryValidator->validate($structure);

        yield $structure;
    }
}
