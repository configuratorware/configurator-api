<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConnectorImportArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ImportResolver implements ArgumentValueResolverInterface
{
    public const OPTION_NAMES = [
        'deleteNonExistingItems',
        'deleteNonExistingAttributeRelations',
        'deleteNonExistingItemPrices',
        'deleteNonExistingDesignAreas',
        'deleteNonExistingDesignAreaVisualData',
        'deleteNonExistingDesignAreaDesignProductionMethod',
        'deleteNonExistingDesignAreaDesignProductionMethodPrices',
        'deleteNonExistingComponentRelations',
        'overwriteItemImages',
    ];

    public const STRING_TO_BOOLEAN_TRANSFORMER = [
        'true' => true,
        '1' => true,
        'false' => false,
        '0' => false,
    ];

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return ConnectorImportArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $arguments = new ConnectorImportArguments();

        $arguments->importOptions = $this->getImportOptions($request);
        $arguments->importBody = $request->getContent();

        yield $arguments;
    }

    /**
     * @param Request $request
     *
     * @return ImportOptions
     */
    private function getImportOptions(Request $request): ImportOptions
    {
        $importOptions = new ImportOptions();

        // Set Default Options
        foreach (self::OPTION_NAMES as $property) {
            $importOptions->$property = true;
        }
        $importOptions->deleteNonExistingItems = false;

        // Overwrite Options from GET Parameters
        foreach (self::OPTION_NAMES as $property) {
            $value = (string)$request->query->get($property);
            if (array_key_exists($value, self::STRING_TO_BOOLEAN_TRANSFORMER)) {
                $importOptions->$property = self::STRING_TO_BOOLEAN_TRANSFORMER[$value];
            }
        }

        if ($request->query->has('priceChannel')) {
            $importOptions->priceChannel = (string)$request->query->get('priceChannel');
        }

        return $importOptions;
    }
}
