<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\CommaSeparatedList;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class CommaSeparatedListResolver implements ArgumentValueResolverInterface
{
    private const LIST_URL_PART = 'commaSeparatedList';

    private RequestResolver $requestResolver;

    public function __construct(RequestResolver $requestResolver)
    {
        $this->requestResolver = $requestResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return CommaSeparatedList::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $commaSeparatedElements = $this->requestResolver->resolveAttributeValue($request, self::LIST_URL_PART);

        $structure = new CommaSeparatedList();
        $structure->elements = explode(',', $commaSeparatedElements);

        yield $structure;
    }
}
