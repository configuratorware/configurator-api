<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPriceItem;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignerGlobalItemPriceItemValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignerGlobalItemPriceItemResolver implements ArgumentValueResolverInterface
{
    /**
     * @var DesignerGlobalItemPriceItemValidator
     */
    private $designerGlobalItemPriceItemValidator;

    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * DesignerGlobalItemPriceItemResolver constructor.
     *
     * @param DesignerGlobalItemPriceItemValidator $designerGlobalItemPriceItemValidator
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(
        DesignerGlobalItemPriceItemValidator $designerGlobalItemPriceItemValidator,
        StructureFromDataConverter $structureFromDataConverter
    ) {
        $this->designerGlobalItemPriceItemValidator = $designerGlobalItemPriceItemValidator;
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return DesignerGlobalItemPriceItem::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, DesignerGlobalItemPriceItem::class);

        $this->designerGlobalItemPriceItemValidator->validate($structure);

        yield $structure;
    }
}
