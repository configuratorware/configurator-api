<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\LicenseFileUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class LicenseFileUploadResolver implements ArgumentValueResolverInterface
{
    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * LicenseFileUploadResolver constructor.
     *
     * @param FileValidator $fileValidator
     */
    public function __construct(FileValidator $fileValidator)
    {
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return LicenseFileUploadArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $structure = new LicenseFileUploadArguments();
        $structure->uploadedFile = $request->files->get('license');

        if (!$structure->uploadedFile) {
            throw new \InvalidArgumentException('License file is missing');
        }

        $this->fileValidator->validate($structure->uploadedFile, ['text/plain']);

        yield $structure;
    }
}
