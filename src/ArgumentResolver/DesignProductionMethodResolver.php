<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod as DesignProductionMethodStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignProductionMethodValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignProductionMethodResolver implements ArgumentValueResolverInterface
{
    /**
     * @var DesignProductionMethodValidator
     */
    private $designProductionMethodValidator;

    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * DesignProductionMethodResolver constructor.
     *
     * @param DesignProductionMethodValidator $designProductionMethodValidator
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(
        DesignProductionMethodValidator $designProductionMethodValidator,
        StructureFromDataConverter $structureFromDataConverter
    ) {
        $this->designProductionMethodValidator = $designProductionMethodValidator;
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return DesignProductionMethodStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, DesignProductionMethodStructure::class);

        $this->designProductionMethodValidator->validate($structure);

        yield $structure;
    }
}
