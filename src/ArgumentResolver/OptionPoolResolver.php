<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool;
use Redhotmagma\ConfiguratorApiBundle\Validator\OptionPoolValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class OptionPoolResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var OptionPoolValidator
     */
    private $optionPoolValidator;

    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        OptionPoolValidator $optionPoolValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->optionPoolValidator = $optionPoolValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return OptionPool::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, OptionPool::class);

        $this->optionPoolValidator->validate($structure);

        yield $structure;
    }
}
