<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ItemIdsResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(StructureFromDataConverter $structureFromDataConverter)
    {
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return Item::class === $argument->getType() && true === $argument->isVariadic();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = (string)$request->getContent();
        $data = json_decode($postData);

        if (empty($data) || !isset($data->itemIds) || !is_array($data->itemIds)) {
            throw new \InvalidArgumentException('Invalid Data');
        }
        foreach ($data->itemIds as $itemId) {
            if (!is_numeric($itemId)) {
                throw new \InvalidArgumentException('Invalid Data');
            }
        }

        $items = [];
        foreach ($data->itemIds as $itemId) {
            $object = new \stdClass();
            $object->id = $itemId;
            $items[] = $this->structureFromDataConverter->convert($object, Item::class);
        }

        yield from $items;
    }
}
