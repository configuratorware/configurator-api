<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Channel;
use Redhotmagma\ConfiguratorApiBundle\Validator\ChannelValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ChannelResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var ChannelValidator
     */
    private $channelValidator;

    /**
     * ChannelResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param ChannelValidator $channelValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        ChannelValidator $channelValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->channelValidator = $channelValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return Channel::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, Channel::class);

        $this->channelValidator->validate($structure);

        yield $structure;
    }
}
