<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FontUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class FontUploadResolver implements ArgumentValueResolverInterface
{
    private const ALLOWED_MIME_TYPES = [
        'font/ttf',
        'font/sfnt',
        'application/x-font-ttf',
        'application/x-font-truetype',
        'application/x-font-opentype',
        'application/font-woff',
        'application/font-woff2',
        'application/font-sfnt',
    ];

    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * FontUploadResolver constructor.
     *
     * @param FileValidator $fileValidator
     */
    public function __construct(FileValidator $fileValidator)
    {
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return FontUploadArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $structure = new FontUploadArguments();
        $structure->uploadedFile = $request->files->get('font');

        $this->fileValidator->validate($structure->uploadedFile, self::ALLOWED_MIME_TYPES, '/\s+/');

        yield $structure;
    }
}
