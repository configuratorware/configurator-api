<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConstructionPattern;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ConstructionPatternResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * ConstructionPatternResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param StructureValidator $structureValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        StructureValidator $structureValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->structureValidator = $structureValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConstructionPattern::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, ConstructionPattern::class);

        $this->structureValidator->validate($structure);

        yield $structure;
    }
}
