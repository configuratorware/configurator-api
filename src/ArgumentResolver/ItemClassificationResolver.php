<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Validator\ItemClassificationValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ItemClassificationResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var ItemClassificationValidator
     */
    private $itemClassificationValidator;

    /**
     * ItemClassificationResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param ItemClassificationValidator $itemClassificationValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        ItemClassificationValidator $itemClassificationValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->itemClassificationValidator = $itemClassificationValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ItemClassification::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, ItemClassification::class);

        $this->itemClassificationValidator->validate($structure);

        yield $structure;
    }
}
