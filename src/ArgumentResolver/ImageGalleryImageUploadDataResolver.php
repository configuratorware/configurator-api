<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageUploadData;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ImageGalleryImageUploadDataResolver implements ArgumentValueResolverInterface
{
    /**
     * @var array
     */
    private $allowedMimeTypes;

    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * ImageGalleryImageUploadDataResolver constructor.
     *
     * @param array $allowedMimeTypes
     * @param FileValidator $fileValidator
     */
    public function __construct(
        array $allowedMimeTypes,
        FileValidator $fileValidator
    ) {
        $this->allowedMimeTypes = $allowedMimeTypes;
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ImageGalleryImageUploadData::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $imageUploads = [];

        $imageGalleryImageUploadData = new ImageGalleryImageUploadData();

        $imageGalleryImageUploadData->imageGalleryImageId = $request->get('id');

        $galleryImage = $request->files->get('galleryImage');
        if (!empty($galleryImage)) {
            $imageGalleryImageUploadData->galleryImageFormData = $galleryImage;
            $imageUploads[] = $galleryImage;
        }

        $printImage = $request->files->get('printImage');
        if (!empty($printImage)) {
            $imageGalleryImageUploadData->printImageFormData = $printImage;
            $imageUploads[] = $printImage;
        }

        $this->fileValidator->validateMany(
            $imageUploads,
            $this->allowedMimeTypes
        );

        yield $imageGalleryImageUploadData;
    }
}
