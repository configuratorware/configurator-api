<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SampleOptionListRequestArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class SampleOptionsListRequestArgumentsResolver implements ArgumentValueResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return SampleOptionListRequestArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $listRequestArguments = new SampleOptionListRequestArguments();

        $listRequestArguments->query = $request->get('query');
        $listRequestArguments->filters = $request->get('filters', SampleOptionListRequestArguments::DEFAULT_FILTERS);
        $listRequestArguments->offset = $request->get('offset', SampleOptionListRequestArguments::DEFAULT_OFFSET);
        $listRequestArguments->limit = $request->get('limit', SampleOptionListRequestArguments::DEFAULT_LIMIT);
        $listRequestArguments->orderBy = $request->get('orderBy');
        $listRequestArguments->orderDir = $request->get('orderDir', SampleOptionListRequestArguments::DEFAULT_ORDERDIR);
        $listRequestArguments->optionId = $request->get('option_id');
        $listRequestArguments->optionClassificationIdentifier = $request->get('optionclassification');
        $listRequestArguments->imageElementIdentifier = $request->get('imageelement');

        $listRequestArguments->filters['optionclassification.identifier'] = $listRequestArguments->optionClassificationIdentifier;

        yield $listRequestArguments;
    }
}
