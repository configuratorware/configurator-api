<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\QuestionTreeUploadData;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class QuestionTreeUploadResolver implements ArgumentValueResolverInterface
{
    private const QUESTION_TREE_ID_URL_PART = 'id';
    private const QUESTION_TREE_LANGUAGE_ID_URL_PART = 'languageId';
    private const MULTIPART_FORM_KEY = 'file';
    private const ALLOWED_MIME_TYPES = [
        'application/json',
        'text/plain',
    ];

    private FileValidator $fileValidator;

    private RequestResolver $requestResolver;

    public function __construct(FileValidator $fileValidator, RequestResolver $requestResolver)
    {
        $this->fileValidator = $fileValidator;
        $this->requestResolver = $requestResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return QuestionTreeUploadData::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $questionTreeId = $this->requestResolver->resolveAttributeValue($request, self::QUESTION_TREE_ID_URL_PART);
        $languageId = $this->requestResolver->resolveAttributeValue($request, self::QUESTION_TREE_LANGUAGE_ID_URL_PART);
        $uploadedFile = $this->requestResolver->resolveUploadedFile($request, self::MULTIPART_FORM_KEY);

        $this->fileValidator->validate($uploadedFile, self::ALLOWED_MIME_TYPES);

        $structure = new QuestionTreeUploadData();
        $structure->questionTreeId = $questionTreeId;
        $structure->languageId = $languageId;
        $structure->dataFileFormData = $uploadedFile;

        yield $structure;
    }
}
