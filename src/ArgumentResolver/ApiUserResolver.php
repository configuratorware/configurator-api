<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\ApiUser;
use Redhotmagma\ConfiguratorApiBundle\Validator\ApiUserValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ApiUserResolver implements ArgumentValueResolverInterface
{
    /**
     * @var ApiUserValidator
     */
    private $apiUserValidator;

    /**
     * @var RequestResolver
     */
    private $requestResolver;

    public function __construct(ApiUserValidator $apiUserValidator, RequestResolver $requestResolver)
    {
        $this->apiUserValidator = $apiUserValidator;
        $this->requestResolver = $requestResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ApiUser::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        /** @var ApiUser $structure */
        $structure = $this->requestResolver->resolveToStructure($request, ApiUser::class);

        $this->apiUserValidator->validate($structure);

        yield $structure;
    }
}
