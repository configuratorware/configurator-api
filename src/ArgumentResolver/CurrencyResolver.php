<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Currency;
use Redhotmagma\ConfiguratorApiBundle\Validator\CurrencyValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class CurrencyResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var CurrencyValidator
     */
    private $currencyValidator;

    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        CurrencyValidator $currencyValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->currencyValidator = $currencyValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return Currency::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, Currency::class);

        $this->currencyValidator->validate($structure);

        yield $structure;
    }
}
