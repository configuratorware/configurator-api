<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignViewValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignViewResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var DesignViewValidator
     */
    private $designViewValidator;

    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        DesignViewValidator $designViewValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->designViewValidator = $designViewValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return DesignViewStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, DesignViewStructure::class);

        $this->designViewValidator->validate($structure);

        yield $structure;
    }
}
