<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChangeUserPassword;
use Redhotmagma\ConfiguratorApiBundle\Validator\ChangeUserPasswordValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ChangeUserPasswordResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var ChangeUserPasswordValidator
     */
    private $changeUserPasswordValidator;

    /**
     * ChangeUserPasswordResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param ChangeUserPasswordValidator $changeUserPasswordValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        ChangeUserPasswordValidator $changeUserPasswordValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->changeUserPasswordValidator = $changeUserPasswordValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ChangeUserPassword::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, ChangeUserPassword::class);

        $this->changeUserPasswordValidator->validate($structure);

        yield $structure;
    }
}
