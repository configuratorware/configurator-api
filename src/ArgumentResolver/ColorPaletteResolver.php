<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Validator\ColorPaletteValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ColorPaletteResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var ColorPaletteValidator
     */
    private $colorPaletteValidator;

    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        ColorPaletteValidator $colorPaletteValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->colorPaletteValidator = $colorPaletteValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return ColorPalette::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, ColorPalette::class);

        $this->colorPaletteValidator->validate($structure);

        yield $structure;
    }
}
