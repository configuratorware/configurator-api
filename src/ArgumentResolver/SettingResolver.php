<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\Setting as SettingStructure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class SettingResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * SettingResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param StructureValidator $structureValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        StructureValidator $structureValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->structureValidator = $structureValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return SettingStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $body = $request->getContent();

        if (!is_string($body)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert(json_decode($body), SettingStructure::class);

        $violations = $this->structureValidator->validate($structure);

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }

        yield $structure;
    }
}
