<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ClientLogoUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ClientLogoUploadResolver implements ArgumentValueResolverInterface
{
    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * @param FileValidator $fileValidator
     */
    public function __construct(
        FileValidator $fileValidator
    ) {
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ClientLogoUploadArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $clientLogoUploadData = new ClientLogoUploadArguments();
        $clientLogoUploadData->uploadedFile = $request->files->get('logo');

        $this->fileValidator->validate(
            $clientLogoUploadData->uploadedFile,
            [
                'image/bmp', // BMP
                'image/x-bmp',
                'image/x-ms-bmp',
                'image/gif', // GIF
                'image/jpeg', // JPG
                'image/pjpeg',
                'image/png', // PNG
            ]
        );

        yield $clientLogoUploadData;
    }
}
