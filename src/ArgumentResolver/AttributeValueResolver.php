<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Validator\AttributeValueValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class AttributeValueResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var AttributeValueValidator
     */
    private $attributeValueValidator;

    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        AttributeValueValidator $attributeValueValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->attributeValueValidator = $attributeValueValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return Attributevalue::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, Attributevalue::class);

        $this->attributeValueValidator->validate($structure);

        yield $structure;
    }
}
