<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FrontendClientArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class FrontendClientArgumentsResolver implements ArgumentValueResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return FrontendClientArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $frontendClientArgument = new FrontendClientArguments();
        $frontendClientArgument->client = $request->get(ControlParameters::CLIENT, ClientRepository::DEFAULT_CLIENT_IDENTIFIER);
        $frontendClientArgument->channel = $request->get(ControlParameters::CHANNEL, ChannelRepository::DEFAULT_CHANNEL_IDENTIFIER);

        yield $frontendClientArgument;
    }
}
