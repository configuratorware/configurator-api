<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidRequest;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadInspirationThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
final class UploadInspirationThumbnailResolver implements ArgumentValueResolverInterface
{
    private const THUMBNAIL_ID_URL_PARAM = 'id';
    private const MULTIPART_FORM_KEY = 'file';

    private const ALLOWED_MIME_TYPES = [
        'image/jpg',
        'image/jpeg',
        'image/pjpeg',
        'image/png',
    ];

    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * @param FileValidator $fileValidator
     */
    public function __construct(FileValidator $fileValidator)
    {
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritDoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return UploadInspirationThumbnail::class === $argument->getType();
    }

    /**
     * {@inheritDoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $thumbnailId = $request->attributes->get(self::THUMBNAIL_ID_URL_PARAM);

        if (null === $thumbnailId) {
            throw InvalidRequest::urlParamExpected(self::THUMBNAIL_ID_URL_PARAM);
        }

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get(self::MULTIPART_FORM_KEY);

        if (null === $uploadedFile) {
            throw InvalidRequest::fileExpected(self::MULTIPART_FORM_KEY);
        }

        $this->fileValidator->validate($uploadedFile, self::ALLOWED_MIME_TYPES, '/\s+/');

        yield UploadInspirationThumbnail::from((int)$thumbnailId, $uploadedFile->getClientOriginalName(), $uploadedFile->getRealPath());
    }
}
