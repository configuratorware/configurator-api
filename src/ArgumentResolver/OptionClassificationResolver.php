<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Validator\OptionClassificationValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class OptionClassificationResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var OptionClassificationValidator
     */
    private $optionClassificationValidator;

    /**
     * OptionClassificationResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param OptionClassificationValidator $optionClassificationValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        OptionClassificationValidator $optionClassificationValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->optionClassificationValidator = $optionClassificationValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return OptionClassification::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, OptionClassification::class);

        $this->optionClassificationValidator->validate($structure);

        yield $structure;
    }
}
