<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageFrontendList;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ImageGalleryImageFrontendListResolver implements ArgumentValueResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ImageGalleryImageFrontendList::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $imageGalleryImageFrontendList = new ImageGalleryImageFrontendList();

        $tags = $request->get('tags');
        if (!empty($tags)) {
            $tags = explode(',', $tags);
        }
        $imageGalleryImageFrontendList->tags = $tags;

        $imageGalleryImageFrontendList->title = $request->files->get('title');

        yield $imageGalleryImageFrontendList;
    }
}
