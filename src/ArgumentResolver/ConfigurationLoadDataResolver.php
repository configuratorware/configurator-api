<?php

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConfigurationLoadArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

final class ConfigurationLoadDataResolver implements ArgumentValueResolverInterface
{
    public const SWITCH_OPTIONS_URL_PARAM = '_switch_options';

    public const NO_CHECK_URL_PARAM = '_no_check';

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConfigurationLoadArguments::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $switchOptions = $request->query->get(self::SWITCH_OPTIONS_URL_PARAM, '');
        $switchOptions = (array)json_decode($switchOptions);
        $noCheck = $request->query->get(self::NO_CHECK_URL_PARAM);
        $noCheck = '1' === $noCheck || 'true' === $noCheck;

        yield ConfigurationLoadArguments::from($switchOptions, $noCheck);
    }
}
