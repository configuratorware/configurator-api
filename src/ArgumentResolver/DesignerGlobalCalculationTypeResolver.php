<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignerGlobalCalculationTypeValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignerGlobalCalculationTypeResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var DesignerGlobalCalculationTypeValidator
     */
    private $designerGlobalCalculationTypeValidator;

    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        DesignerGlobalCalculationTypeValidator $designerGlobalCalculationTypeValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->designerGlobalCalculationTypeValidator = $designerGlobalCalculationTypeValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return DesignerGlobalCalculationTypeStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, DesignerGlobalCalculationTypeStructure::class);
        $this->designerGlobalCalculationTypeValidator->validate($structure);

        yield $structure;
    }
}
