<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationVariant;
use Redhotmagma\ConfiguratorApiBundle\Validator\ConfigurationVariantValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ConfigurationVariantResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var ConfigurationVariantValidator
     */
    private $configurationVariantValidator;

    /**
     * ConfigurationVariantResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param ConfigurationVariantValidator $configurationVariantValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        ConfigurationVariantValidator $configurationVariantValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->configurationVariantValidator = $configurationVariantValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConfigurationVariant::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, ConfigurationVariant::class);

        $this->configurationVariantValidator->validate($structure);

        yield $structure;
    }
}
