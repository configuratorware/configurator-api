<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\MediaArguments;
use Redhotmagma\ConfiguratorApiBundle\Validator\MediaValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @internal
 */
class MediaArgumentsResolver implements ArgumentValueResolverInterface
{
    /**
     * @var MediaValidator
     */
    private $mediaValidator;

    /**
     * MediaArgumentsResolver constructor.
     *
     * @param MediaValidator $mediaValidator
     */
    public function __construct(MediaValidator $mediaValidator)
    {
        $this->mediaValidator = $mediaValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return MediaArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $mediaArguments = new MediaArguments();

        $mobile = (bool)$request->get('mobile', false);
        $scale = $request->get('scale', 100);

        if ($mobile) {
            $scale = 50;
        } elseif (!is_numeric($scale) || (int)$scale < 1 || (int)$scale > 100) {
            throw new BadRequestHttpException('Query param \'scale\' must be a whole number between 1 and 100');
        } else {
            $scale = (int)$scale;
        }

        $mediaArguments->path = $request->get('path');
        $mediaArguments->scale = $scale;
        $mediaArguments->mobile = $mobile;

        $this->mediaValidator->validate($mediaArguments);

        yield $mediaArguments;
    }
}
