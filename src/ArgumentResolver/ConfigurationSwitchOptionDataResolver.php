<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSwitchOptionData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ConfigurationSwitchOptionDataResolver implements ArgumentValueResolverInterface
{
    /**
     * @var RequestResolver
     */
    private $requestResolver;

    /**
     * @param RequestResolver $requestResolver
     */
    public function __construct(RequestResolver $requestResolver)
    {
        $this->requestResolver = $requestResolver;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConfigurationSwitchOptionData::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        /** @var ConfigurationSwitchOptionData $structure */
        $structure = $this->requestResolver->resolveToStructure($request, ConfigurationSwitchOptionData::class);
        // casting switchOptions to array seems to be necessary because @Serializer\Type(array<\stdClass> does not work properly
        $structure->switchedOptions = (array)$structure->switchedOptions;
        $structure->noCheck = $request->get('nocheck', false);

        yield $structure;
    }
}
