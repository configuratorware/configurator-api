<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\FontValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class FontResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var FontValidator
     */
    private $fontValidator;

    /**
     * FontResolver constructor.
     *
     * @param FontValidator $fontValidator
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(
        FontValidator $fontValidator,
        StructureFromDataConverter $structureFromDataConverter
    ) {
        $this->fontValidator = $fontValidator;
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return FontStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, FontStructure::class);

        $this->fontValidator->validate($structure);

        yield $structure;
    }
}
