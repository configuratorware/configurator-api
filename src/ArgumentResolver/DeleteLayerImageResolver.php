<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidRequest;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DeleteLayerImage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
final class DeleteLayerImageResolver implements ArgumentValueResolverInterface
{
    private const ITEM_ID_URL_PARAM = 'id';
    private const COMPONENT_ID_URL_PARAM = 'optionClassificationId';
    private const OPTION_ID_URL_PARAM = 'optionId';
    private const CREATOR_VIEW_ID_URL_PARAM = 'creatorViewId';

    /**
     * {@inheritDoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return DeleteLayerImage::class === $argument->getType();
    }

    /**
     * {@inheritDoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $itemId = $request->attributes->get(self::ITEM_ID_URL_PARAM);
        $componentId = $request->attributes->get(self::COMPONENT_ID_URL_PARAM);
        $optionId = $request->attributes->get(self::OPTION_ID_URL_PARAM);
        $viewId = $request->attributes->get(self::CREATOR_VIEW_ID_URL_PARAM);

        if (!isset($itemId, $componentId, $optionId, $viewId)) {
            throw InvalidRequest::urlParamsExpected([self::ITEM_ID_URL_PARAM, self::COMPONENT_ID_URL_PARAM, self::OPTION_ID_URL_PARAM, self::CREATOR_VIEW_ID_URL_PARAM]);
        }

        yield DeleteLayerImage::from((int)$itemId, (int)$componentId, (int)$optionId, (string)$viewId);
    }
}
