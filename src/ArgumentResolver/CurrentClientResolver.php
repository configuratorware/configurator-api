<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\CurrentClient;
use Redhotmagma\ConfiguratorApiBundle\Validator\ClientValidator;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class CurrentClientResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var ClientValidator
     */
    private $clientValidator;

    /**
     * @var Security
     */
    private $security;

    /**
     * CurrentClientResolver constructor.
     *
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param ClientValidator $clientValidator
     */
    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        ClientValidator $clientValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->clientValidator = $clientValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return CurrentClient::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        /* @var $structure CurrentClient */
        $structure = $this->structureFromDataConverter->convert($data, CurrentClient::class);

        $this->clientValidator->validate($structure);

        yield $structure;
    }
}
