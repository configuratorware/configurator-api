<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidRequest;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadComponentThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
final class UploadComponentThumbnailResolver implements ArgumentValueResolverInterface
{
    private const COMPONENT_ID_URL_PARAM = 'id';
    private const MULTIPART_FORM_KEY = 'file';

    private const ALLOWED_MIME_TYPES = [
        'image/jpg',
        'image/jpeg',
        'image/pjpeg',
        'image/png',
    ];

    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * @param FileValidator $fileValidator
     */
    public function __construct(FileValidator $fileValidator)
    {
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritDoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return UploadComponentThumbnail::class === $argument->getType();
    }

    /**
     * {@inheritDoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $componentId = $request->attributes->get(self::COMPONENT_ID_URL_PARAM);

        if (null === $componentId) {
            throw InvalidRequest::urlParamExpected(self::COMPONENT_ID_URL_PARAM);
        }

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get(self::MULTIPART_FORM_KEY);

        if (null === $uploadedFile) {
            throw InvalidRequest::fileExpected(self::MULTIPART_FORM_KEY);
        }

        $this->fileValidator->validate($uploadedFile, self::ALLOWED_MIME_TYPES, '/\s+/');

        yield UploadComponentThumbnail::from((int)$componentId, $uploadedFile->getClientOriginalName(), $uploadedFile->getRealPath());
    }
}
