<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ItemClassificationFilter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ItemClassificationFilterResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ItemClassificationFilter::class === $argument->getType();
    }

    /**
     * @return iterable<ItemClassificationFilter>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $filter = new ItemClassificationFilter();

        $itemClassifications = $request->query->get('item-classifications', '[]');
        if ($itemClassificationsIds = json_decode($itemClassifications)) {
            $filter->setItemClassificationsIds($itemClassificationsIds);
        }

        yield $filter;
    }
}
