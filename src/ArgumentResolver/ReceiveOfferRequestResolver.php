<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\ReceiveOfferRequestValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ReceiveOfferRequestResolver implements ArgumentValueResolverInterface
{
    /**
     * @var ReceiveOfferRequestValidator
     */
    private $receiveOfferRequestValidator;

    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * ReceiveOfferRequestResolver constructor.
     *
     * @param ReceiveOfferRequestValidator $receiveOfferRequestValidator
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(
        ReceiveOfferRequestValidator $receiveOfferRequestValidator,
        StructureFromDataConverter $structureFromDataConverter
    ) {
        $this->receiveOfferRequestValidator = $receiveOfferRequestValidator;
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return ReceiveOfferRequestStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, ReceiveOfferRequestStructure::class);

        $this->receiveOfferRequestValidator->validate($structure);

        yield $structure;
    }
}
