<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemDuplicate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ItemDuplicateResolver implements ArgumentValueResolverInterface
{
    private StructureFromDataConverter $structureFromDataConverter;

    public function __construct(StructureFromDataConverter $structureFromDataConverter)
    {
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ItemDuplicate::class === $argument->getType();
    }

    /**
     * @return iterable<ItemDuplicate>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $data = json_decode($request->getContent(), false, 512, JSON_THROW_ON_ERROR);
        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        yield $this->structureFromDataConverter->convert($data, ItemDuplicate::class);
    }
}
