<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ListRequestArgumentsResolver implements ArgumentValueResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ListRequestArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $listRequestArguments = new ListRequestArguments();

        $listRequestArguments->query = $request->get('query');
        $listRequestArguments->filters = $request->get('filters', ListRequestArguments::DEFAULT_FILTERS);
        $listRequestArguments->offset = $request->get('offset', ListRequestArguments::DEFAULT_OFFSET);
        $listRequestArguments->limit = $request->get('limit', ListRequestArguments::DEFAULT_LIMIT);
        $listRequestArguments->orderBy = $request->get('orderBy', $request->get('orderby'));
        $listRequestArguments->orderDir = $request->get('orderDir', $request->get('orderdir', ListRequestArguments::DEFAULT_ORDERDIR));

        yield $listRequestArguments;
    }
}
