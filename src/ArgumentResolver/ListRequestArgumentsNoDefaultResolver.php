<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ListRequestArgumentsNoDefaultResolver implements ArgumentValueResolverInterface
{
    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ListRequestNoDefaultArguments::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $listRequestArguments = new ListRequestNoDefaultArguments();

        $listRequestArguments->query = $request->get('query');
        $listRequestArguments->filters = $request->get('filters', ListRequestNoDefaultArguments::DEFAULT_FILTERS);
        $listRequestArguments->offset = $request->get('offset', ListRequestNoDefaultArguments::DEFAULT_OFFSET);
        $listRequestArguments->limit = $request->get('limit');
        $listRequestArguments->orderBy = $request->get('orderBy', $request->get('orderby'));
        $listRequestArguments->orderDir = $request->get('orderDir', $request->get('orderdir'));

        yield $listRequestArguments;
    }
}
