<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;
use Redhotmagma\ConfiguratorApiBundle\Validator\DesignAreaValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class DesignAreaResolver implements ArgumentValueResolverInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var DesignAreaValidator
     */
    private $designAreaValidator;

    public function __construct(
        StructureFromDataConverter $structureFromDataConverter,
        DesignAreaValidator $designAreaValidator
    ) {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->designAreaValidator = $designAreaValidator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(
        Request $request,
        ArgumentMetadata $argument
    ): bool {
        return DesignAreaStructure::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(
        Request $request,
        ArgumentMetadata $argument
    ): iterable {
        $postData = $request->getContent();
        $data = json_decode($postData);

        if (empty($data)) {
            throw new \InvalidArgumentException('Invalid Data');
        }

        $structure = $this->structureFromDataConverter->convert($data, DesignAreaStructure::class);

        $this->designAreaValidator->validate($structure);

        yield $structure;
    }
}
