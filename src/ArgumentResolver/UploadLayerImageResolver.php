<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidRequest;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadLayerImage;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
final class UploadLayerImageResolver implements ArgumentValueResolverInterface
{
    private const ITEM_ID_URL_PARAM = 'id';
    private const COMPONENT_ID_URL_PARAM = 'optionClassificationId';
    private const OPTION_ID_URL_PARAM = 'optionId';
    private const CREATOR_VIEW_ID_URL_PARAM = 'creatorViewId';
    private const MULTIPART_FORM_KEY = 'file';

    private const ALLOWED_MIME_TYPES = [
        'image/jpg',
        'image/jpeg',
        'image/pjpeg',
        'image/png',
    ];

    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * @param FileValidator $fileValidator
     */
    public function __construct(FileValidator $fileValidator)
    {
        $this->fileValidator = $fileValidator;
    }

    /**
     * {@inheritDoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return UploadLayerImage::class === $argument->getType();
    }

    /**
     * {@inheritDoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $itemId = $request->attributes->get(self::ITEM_ID_URL_PARAM);
        $componentId = $request->attributes->get(self::COMPONENT_ID_URL_PARAM);
        $optionId = $request->attributes->get(self::OPTION_ID_URL_PARAM);
        $viewId = $request->attributes->get(self::CREATOR_VIEW_ID_URL_PARAM);

        if (!isset($itemId, $componentId, $optionId, $viewId)) {
            throw InvalidRequest::urlParamsExpected([self::ITEM_ID_URL_PARAM, self::COMPONENT_ID_URL_PARAM, self::OPTION_ID_URL_PARAM, self::CREATOR_VIEW_ID_URL_PARAM]);
        }

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get(self::MULTIPART_FORM_KEY);

        if (null === $uploadedFile) {
            throw InvalidRequest::fileExpected(self::MULTIPART_FORM_KEY);
        }

        $this->fileValidator->validate($uploadedFile, self::ALLOWED_MIME_TYPES, '/\s+/');

        yield UploadLayerImage::from((int)$itemId, (int)$componentId, (int)$optionId, (string)$viewId, $uploadedFile->getClientOriginalName(), $uploadedFile->getRealPath());
    }
}
