<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\Service;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationPrintFileSaveData;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
class ConfigurationPrintFileSaveDataResolver implements ArgumentValueResolverInterface
{
    /**
     * @var Service
     */
    private $service;

    /**
     * ConfigurationResolver constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConfigurationPrintFileSaveData::class === $argument->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $postData = $request->getContent();

        if (empty($postData)) {
            throw new \InvalidArgumentException('The post content should not be empty to resolve Configuration');
        }

        $data = json_decode($postData);

        $configurationData = $data;
        if (!empty($data->configuration)) {
            $configurationData = $data->configuration;
        }

        $configurationPrintFileSaveData = $this->service->getStructureFromData($configurationData,
            ConfigurationPrintFileSaveData::class);

        yield $configurationPrintFileSaveData;
    }
}
