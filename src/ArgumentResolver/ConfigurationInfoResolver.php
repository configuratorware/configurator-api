<?php

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConfigurationInfoArguments;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

class ConfigurationInfoResolver implements ArgumentValueResolverInterface
{
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return ConfigurationInfoArguments::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $argument = new ConfigurationInfoArguments();
        $argument->extendedData = $request->query->get('_extended_data', false);

        yield $argument;
    }
}
