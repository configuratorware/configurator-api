<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\ArgumentResolver;

use Redhotmagma\ConfiguratorApiBundle\Request\RequestResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadDesignViewDetailImageForGroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Validator\FileValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;

/**
 * @internal
 */
final class UploadDesignViewDetailImageForGroupEntryResolver implements ArgumentValueResolverInterface
{
    private const DESIGN_VIEW_ID_URL_PARAM = 'id';
    private const GROUP_ENTRY_ID_URL_PARAM = 'itemGroupEntryId';
    private const MULTIPART_FORM_KEY = 'file';

    private const ALLOWED_MIME_TYPES = [
        'image/jpg',
        'image/jpeg',
        'image/pjpeg',
        'image/png',
    ];

    /**
     * @var FileValidator
     */
    private $fileValidator;

    /**
     * @var RequestResolver
     */
    private $requestResolver;

    /**
     * @param FileValidator $fileValidator
     * @param RequestResolver $requestResolver
     */
    public function __construct(FileValidator $fileValidator, RequestResolver $requestResolver)
    {
        $this->fileValidator = $fileValidator;
        $this->requestResolver = $requestResolver;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        return UploadDesignViewDetailImageForGroupEntry::class === $argument->getType();
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $designViewId = $this->requestResolver->resolveAttributeValue($request, self::DESIGN_VIEW_ID_URL_PARAM);

        $itemGroupEntryId = $this->requestResolver->resolveAttributeValue($request, self::GROUP_ENTRY_ID_URL_PARAM);

        $uploadedFile = $this->requestResolver->resolveUploadedFile($request, self::MULTIPART_FORM_KEY);

        $this->fileValidator->validate($uploadedFile, self::ALLOWED_MIME_TYPES, '/\s+/');

        yield new UploadDesignViewDetailImageForGroupEntry((int)$designViewId, (int)$itemGroupEntryId, $uploadedFile->getClientOriginalName(), $uploadedFile->getRealPath());
    }
}
