<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Symfony\Contracts\EventDispatcher\Event;

class AdminAreaConfigurationStructureFromEntityEvent extends Event
{
    public const NAME = 'configuratorware.configuration.admin_area_structure_from_entity';

    /**
     * @var Configuration
     */
    private $configurationStructure;

    /**
     * @var \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration
     */
    private $configurationEntity;

    /**
     * @var string
     */
    private $structureClassName;

    /**
     * AdminAreaConfigurationStructureFromEntityEvent constructor.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration $configurationEntity
     * @param string $structureClassName
     */
    public function __construct(
        \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration $configurationEntity,
        string $structureClassName
    ) {
        $this->configurationEntity = $configurationEntity;
        $this->structureClassName = $structureClassName;
    }

    /**
     * @return Configuration
     */
    public function getConfigurationStructure(): Configuration
    {
        return $this->configurationStructure;
    }

    /**
     * @param Configuration $configurationStructure
     */
    public function setConfigurationStructure(Configuration $configurationStructure)
    {
        $this->configurationStructure = $configurationStructure;
    }

    /**
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration
     */
    public function getConfigurationEntity(): \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration
    {
        return $this->configurationEntity;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration $configurationEntity
     */
    public function setConfigurationEntity(\Redhotmagma\ConfiguratorApiBundle\Entity\Configuration $configurationEntity)
    {
        $this->configurationEntity = $configurationEntity;
    }

    /**
     * @return string
     */
    public function getStructureClassName(): string
    {
        return $this->structureClassName;
    }

    /**
     * @param string $structureClassName
     */
    public function setStructureClassName(string $structureClassName)
    {
        $this->structureClassName = $structureClassName;
    }
}
