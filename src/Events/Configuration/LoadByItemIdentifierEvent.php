<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class LoadByItemIdentifierEvent extends Event
{
    public const NAME = 'configuratorware.configuration.load_by_item_identifier';

    /**
     * @var string
     */
    private $itemIdentifier;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var ControlParameters
     */
    private $controlParameters;

    /**
     * LoadByItemIdentifierEvent constructor.
     *
     * @param string $itemIdentifier
     */
    public function __construct(string $itemIdentifier)
    {
        $this->itemIdentifier = $itemIdentifier;
    }

    /**
     * @return string
     */
    public function getItemIdentifier(): string
    {
        return $this->itemIdentifier;
    }

    /**
     * @param string $itemIdentifier
     */
    public function setItemIdentifier(string $itemIdentifier): void
    {
        $this->itemIdentifier = $itemIdentifier;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): ?Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @return ControlParameters|null
     */
    public function getControlParameters(): ?ControlParameters
    {
        return $this->controlParameters;
    }

    /**
     * @param ControlParameters $controlParameters
     */
    public function setControlParameters(ControlParameters $controlParameters): void
    {
        $this->controlParameters = $controlParameters;
    }

    /**
     * @return ControlParameters|null
     *
     * @deprecated use getControlParameters instead
     */
    public function getGETParameterCollection(): ?ControlParameters
    {
        return $this->getControlParameters();
    }

    /**
     * @param ControlParameters $controlParameters
     *
     * @deprecated use setControlParameters instead
     */
    public function setGETParameterCollection(ControlParameters $controlParameters): void
    {
        $this->setControlParameters($controlParameters);
    }
}
