<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSaveData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class FrontendConfigurationSaveEvent extends Event
{
    public const NAME = 'configuratorware.configuration.frontend_save';

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var ConfigurationSaveData
     */
    private $configurationSaveData;

    /**
     * @var ControlParameters
     */
    private $controlParameters;

    /**
     * @var CalculationResult|null
     */
    private $calculationResult;

    /**
     * @param ConfigurationSaveData $configurationSaveData
     * @param ControlParameters $controlParameters
     * @param CalculationResult|null $calculationResult
     */
    public function __construct(ConfigurationSaveData $configurationSaveData, ControlParameters $controlParameters, CalculationResult $calculationResult = null)
    {
        $this->configurationSaveData = $configurationSaveData;
        $this->controlParameters = $controlParameters;
        $this->calculationResult = $calculationResult;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @return ConfigurationSaveData
     */
    public function getConfigurationSaveData(): ConfigurationSaveData
    {
        return $this->configurationSaveData;
    }

    /**
     * @param ConfigurationSaveData $configurationSaveData
     */
    public function setConfigurationSaveData(ConfigurationSaveData $configurationSaveData): void
    {
        $this->configurationSaveData = $configurationSaveData;
    }

    /**
     * @return ControlParameters
     */
    public function getControlParameters(): ControlParameters
    {
        return $this->controlParameters;
    }

    /**
     * @return CalculationResult|null
     */
    public function getCalculationResult(): ?CalculationResult
    {
        return $this->calculationResult;
    }
}
