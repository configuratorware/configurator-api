<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\EventDispatcher\Event;

class AddVisualizationDataToStructureEvent extends Event
{
    public const NAME = 'configuratorware.configuration.add_visualization_data_to_structure';

    /**
     * @var Configuration|\Redhotmagma\ConfiguratorApiBundle\Structure\Configuration
     */
    private $configuration;

    /**
     * AddVisualizationDataToStructureEvent constructor.
     *
     * @param Configuration|\Redhotmagma\ConfiguratorApiBundle\Structure\Configuration $configuration
     */
    public function __construct($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return Configuration|\Redhotmagma\ConfiguratorApiBundle\Structure\Configuration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param Configuration|\Redhotmagma\ConfiguratorApiBundle\Structure\Configuration $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }
}
