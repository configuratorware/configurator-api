<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\ZipModel;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class ZipGenerationEvent extends Event
{
    public const NAME = 'configuratorware.configuration.zip_generation';

    /**
     * @var string
     */
    private $configurationCode;

    /**
     * @var ControlParameters
     */
    private $controlParameters;

    /**
     * @var ZipModel[]
     */
    private $files = [];

    /**
     * ZipGenerationEvent constructor.
     *
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     */
    public function __construct(string $configurationCode, ControlParameters $controlParameters)
    {
        $this->configurationCode = $configurationCode;
        $this->controlParameters = $controlParameters;
    }

    /**
     * @return string
     */
    public function getConfigurationCode(): string
    {
        return $this->configurationCode;
    }

    /**
     * @return ControlParameters
     */
    public function getControlParameters(): ControlParameters
    {
        return $this->controlParameters;
    }

    /**
     * @return array
     */
    public function getFiles(): array
    {
        return $this->files;
    }

    /**
     * @param ZipModel[] $files
     */
    public function setFiles(array $files): void
    {
        $this->files = $files;
    }

    /**
     * @param ZipModel $file
     *
     * @return array
     */
    public function addFile(ZipModel $file): array
    {
        $this->files[] = $file;

        return $this->files;
    }
}
