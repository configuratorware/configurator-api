<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class LoadByConfigurationCodeEvent extends Event
{
    public const NAME = 'configuratorware.configuration.load_by_configuration_code';

    /**
     * @var string
     */
    private $configurationCode;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var ControlParameters
     */
    private $controlParameters;

    /**
     * LoadByConfigurationCodeEvent constructor.
     *
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     */
    public function __construct(string $configurationCode, ControlParameters $controlParameters)
    {
        $this->configurationCode = $configurationCode;
        $this->controlParameters = $controlParameters;
    }

    /**
     * @return string
     */
    public function getConfigurationCode(): string
    {
        return $this->configurationCode;
    }

    /**
     * @param string $configurationCode
     *
     * @deprecated risky operation
     */
    public function setConfigurationCode(string $configurationCode): void
    {
        $this->configurationCode = $configurationCode;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @return ControlParameters
     */
    public function getControlParameters(): ControlParameters
    {
        return $this->controlParameters;
    }
}
