<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationValidationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\EventDispatcher\Event;

class ConfigurationValidationEvent extends Event
{
    public const NAME = 'configuratorware.configuration.validation';

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var ConfigurationValidationResult
     */
    private $validationResult;

    /**
     * ConfigurationValidationEvent constructor.
     *
     * @param Configuration $configuration
     * @param ConfigurationValidationResult $validationResult
     */
    public function __construct(Configuration $configuration, ConfigurationValidationResult $validationResult)
    {
        $this->configuration = $configuration;
        $this->validationResult = $validationResult;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return ConfigurationValidationResult
     */
    public function getValidationResult(): ConfigurationValidationResult
    {
        return $this->validationResult;
    }

    /**
     * @param ConfigurationValidationResult $validationResult
     */
    public function setValidationResult(ConfigurationValidationResult $validationResult)
    {
        $this->validationResult = $validationResult;
    }
}
