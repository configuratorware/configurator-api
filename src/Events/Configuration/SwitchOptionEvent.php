<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use stdClass;
use Symfony\Contracts\EventDispatcher\Event;

class SwitchOptionEvent extends Event
{
    public const NAME = 'configuratorware.configuration.switch_option';

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var array<string,stdClass>|array<string,stdClass[]>
     */
    private $switchOptions;

    /**
     * @var array
     */
    private $removedOptions;

    /**
     * @var bool
     */
    private $addVisualizationDataAtSwitch;

    /**
     * SwitchOptionEvent constructor.
     *
     * @param Configuration $configuration
     * @param array<string,stdClass>|array<string,stdClass[]> $switchOptions
     * @param bool $addVisualizationDataAtSwitch
     */
    public function __construct(Configuration $configuration, $switchOptions, bool $addVisualizationDataAtSwitch = true)
    {
        $this->configuration = $configuration;
        $this->switchOptions = $switchOptions;
        $this->addVisualizationDataAtSwitch = $addVisualizationDataAtSwitch;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return array<string,stdClass>|array<string,stdClass[]>
     *
     *@deprecated use getSwitchOptions instead
     */
    public function getSwtichOptions()
    {
        return $this->switchOptions;
    }

    /**
     * for string array keys that are valid integers: be aware that php automatically casts those to int!
     *
     * @return array<string,stdClass>|array<string,stdClass[]>
     */
    public function getSwitchOptions()
    {
        return $this->switchOptions;
    }

    /**
     * @param array<string,stdClass>|array<string,stdClass[]> $switchOptions
     *
     *@deprecated use setSwitchOptions instead
     */
    public function setSwtichOptions($switchOptions)
    {
        $this->switchOptions = $switchOptions;
    }

    /**
     * @param array<string,stdClass>|array<string,stdClass[]> $switchOptions
     */
    public function setSwitchOptions($switchOptions)
    {
        $this->switchOptions = $switchOptions;
    }

    /**
     * @return array
     */
    public function getRemovedOptions(): array
    {
        return $this->removedOptions;
    }

    /**
     * @param array $removedOptions
     */
    public function setRemovedOptions(array $removedOptions)
    {
        $this->removedOptions = $removedOptions;
    }

    /**
     * @return bool
     */
    public function getAddVisualizationDataAtSwitch(): bool
    {
        return $this->addVisualizationDataAtSwitch;
    }

    /**
     * @param bool $addVisualizationDataAtSwitch
     */
    public function setAddVisualizationDataAtSwitch(bool $addVisualizationDataAtSwitch)
    {
        $this->addVisualizationDataAtSwitch = $addVisualizationDataAtSwitch;
    }
}
