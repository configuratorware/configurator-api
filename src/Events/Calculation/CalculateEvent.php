<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\EventDispatcher\Event;

class CalculateEvent extends Event
{
    public const NAME = 'configuratorware.calculation.calculate';

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var bool
     */
    private $calculateBulkSavings;

    /**
     * @var CalculationResult|null
     */
    private $calculationResult;

    /**
     * CalculateEvent constructor.
     *
     * @param Configuration $configuration
     * @param bool $calculateBulkSavings
     */
    public function __construct(Configuration $configuration, bool $calculateBulkSavings = true)
    {
        $this->configuration = $configuration;
        $this->calculateBulkSavings = $calculateBulkSavings;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }

    /**
     * @return bool
     */
    public function getCalculateBulkSavings(): bool
    {
        return $this->calculateBulkSavings;
    }

    /**
     * @param bool $calculateBulkSavings
     */
    public function setCalculateBulkSavings(bool $calculateBulkSavings): void
    {
        $this->calculateBulkSavings = $calculateBulkSavings;
    }

    /**
     * @return CalculationResult
     */
    public function getCalculationResult(): ?CalculationResult
    {
        return $this->calculationResult;
    }

    /**
     * @param CalculationResult $calculationResult
     */
    public function setCalculationResult(CalculationResult $calculationResult): void
    {
        $this->calculationResult = $calculationResult;
    }
}
