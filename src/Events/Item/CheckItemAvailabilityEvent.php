<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class CheckItemAvailabilityEvent extends Event
{
    public const NAME = 'configuratorware.item.check_item_availability';

    /**
     * @var Item
     */
    private $item;

    /**
     * @var bool
     */
    private $isAvailable;

    /**
     * @var ControlParameters|null
     */
    private $controlParameters;

    /**
     * CheckItemAvailabilityEvent constructor.
     *
     * @param Item $item
     * @param ControlParameters|null $controlParameters
     */
    public function __construct(Item $item, ControlParameters $controlParameters = null)
    {
        $this->item = $item;
        $this->controlParameters = $controlParameters;
    }

    /**
     * @return Item
     */
    public function getItem(): Item
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem(Item $item)
    {
        $this->item = $item;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->isAvailable;
    }

    /**
     * @param bool $isAvailable
     */
    public function setIsAvailable(bool $isAvailable)
    {
        $this->isAvailable = $isAvailable;
    }

    /**
     * @return ControlParameters
     */
    public function getControlParameters(): ?ControlParameters
    {
        return $this->controlParameters;
    }
}
