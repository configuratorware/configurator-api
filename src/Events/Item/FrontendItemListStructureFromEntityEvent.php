<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemList;
use Symfony\Contracts\EventDispatcher\Event;

class FrontendItemListStructureFromEntityEvent extends Event
{
    public const NAME = 'configuratorware.item.frontend_item_list_structure_from_entity_event';

    private Item $entity;

    private ItemList $structure;

    public function __construct(Item $entity, ItemList $structure)
    {
        $this->entity = $entity;
        $this->structure = $structure;
    }

    /**
     * @param Item $entity
     */
    public function setEntity(Item $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return Item
     */
    public function getEntity(): Item
    {
        return $this->entity;
    }

    /**
     * @param ItemList $structure
     */
    public function setStructure(ItemList $structure): void
    {
        $this->structure = $structure;
    }

    /**
     * @return ItemList
     */
    public function getStructure(): ItemList
    {
        return $this->structure;
    }
}
