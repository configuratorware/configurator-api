<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Item;

use Symfony\Contracts\EventDispatcher\Event;

class GetConfigurableItemsByParentEvent extends Event
{
    public const NAME = 'configuratorware.item.get_configurable_items_by_parent';

    /**
     * @var string
     */
    private $parentIdentifier;

    /**
     * @var array
     */
    private $itemStructures;

    /**
     * GetConfigurableItemsByParentEvent constructor.
     *
     * @param string $parentIdentifier
     */
    public function __construct(string $parentIdentifier)
    {
        $this->parentIdentifier = $parentIdentifier;
    }

    /**
     * @return string
     */
    public function getParentIdentifier(): string
    {
        return $this->parentIdentifier;
    }

    /**
     * @param string $parentIdentifier
     */
    public function setParentIdentifier(string $parentIdentifier)
    {
        $this->parentIdentifier = $parentIdentifier;
    }

    /**
     * @return array
     */
    public function getItemStructures(): array
    {
        return $this->itemStructures;
    }

    /**
     * @param array $itemStructures
     */
    public function setItemStructures(array $itemStructures)
    {
        $this->itemStructures = $itemStructures;
    }
}
