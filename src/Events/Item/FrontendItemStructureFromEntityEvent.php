<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item as FrontendItemStructure;
use Symfony\Contracts\EventDispatcher\Event;

class FrontendItemStructureFromEntityEvent extends Event
{
    public const NAME = 'configuratorware.item.frontend_item_structure_from_entity_event';

    /**
     * @var Item
     */
    private $entity;

    /**
     * @var FrontendItemStructure
     */
    private $structure;

    /**
     * @param FrontendItemStructure $structure
     * @param Item $entity
     */
    public function __construct(Item $entity, FrontendItemStructure $structure)
    {
        $this->entity = $entity;
        $this->structure = $structure;
    }

    /**
     * @return Item
     */
    public function getEntity(): Item
    {
        return $this->entity;
    }

    /**
     * @param Item $entity
     */
    public function setEntity(Item $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return FrontendItemStructure
     */
    public function getStructure(): FrontendItemStructure
    {
        return $this->structure;
    }

    /**
     * @param FrontendItemStructure $structure
     */
    public function setStructure(FrontendItemStructure $structure): void
    {
        $this->structure = $structure;
    }
}
