<?php

namespace Redhotmagma\ConfiguratorApiBundle\Events\OptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;

class FrontendOptionClassificationsWithOptionsEvent
{
    public const NAME = 'configuratorware.optionclassification.frontend_with_options';
    /**
     * @var OptionClassification[]
     */
    private array $optionClassifications = [];
    private Item $item;

    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    /**
     * @return OptionClassification[]
     */
    public function getOptionClassifications(): array
    {
        return $this->optionClassifications;
    }

    /**
     * @param array $optionClassifications
     *
     * @return void
     */
    public function setOptionClassifications(array $optionClassifications): void
    {
        $this->optionClassifications = $optionClassifications;
    }

    public function getItem(): Item
    {
        return $this->item;
    }

    public function setItem(Item $item): void
    {
        $this->item = $item;
    }
}
