<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Media;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\MediaArguments;
use Symfony\Contracts\EventDispatcher\Event;

class MediaValidationEvent extends Event
{
    public const NAME = 'configuratorware.media.validation';

    /**
     * @var MediaArguments
     */
    private $mediaArguments;

    /**
     * MediaValidationEvent constructor.
     *
     * @param MediaArguments $mediaArguments
     */
    public function __construct(MediaArguments $mediaArguments)
    {
        $this->mediaArguments = $mediaArguments;
    }

    /**
     * @return MediaArguments
     */
    public function getMediaArguments(): MediaArguments
    {
        return $this->mediaArguments;
    }

    /**
     * @param MediaArguments $mediaArguments
     */
    public function setMediaArguments(MediaArguments $mediaArguments)
    {
        $this->mediaArguments = $mediaArguments;
    }
}
