<?php

namespace Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationDocument;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class DataCollectionEvent extends Event
{
    public const NAME = 'configuratorware.configuration_document.data_collection';

    /**
     * @var string
     */
    private $configurationCode;

    /**
     * @var string
     */
    private $documentType;

    /**
     * @var FrontendConfigurationStructure
     */
    private $frontendConfigurationStructure;

    /**
     * @var ControlParameters
     */
    private $controlParameters;

    /**
     * @var Configuration
     */
    private $configurationEntity;

    /**
     * @var array
     */
    private $dataSet = [];

    /**
     * DataCollectionEvent constructor.
     *
     * @param string $configurationCode
     * @param string $documentType
     * @param ControlParameters $controlParameters
     */
    public function __construct(
        string $configurationCode,
        string $documentType,
        ControlParameters $controlParameters
    ) {
        $this->configurationCode = $configurationCode;
        $this->documentType = $documentType;
        $this->controlParameters = $controlParameters;
    }

    /**
     * @return string
     */
    public function getConfigurationCode(): string
    {
        return $this->configurationCode;
    }

    /**
     * @return string
     */
    public function getDocumentType(): string
    {
        return $this->documentType;
    }

    /**
     * @return ControlParameters
     */
    public function getControlParameters(): ControlParameters
    {
        return $this->controlParameters;
    }

    /**
     * @return FrontendConfigurationStructure
     */
    public function getFrontendConfigurationStructure(): FrontendConfigurationStructure
    {
        return $this->frontendConfigurationStructure;
    }

    /**
     * @param FrontendConfigurationStructure $frontendConfigurationStructure
     */
    public function setFrontendConfigurationStructure(
        FrontendConfigurationStructure $frontendConfigurationStructure
    ): void {
        $this->frontendConfigurationStructure = $frontendConfigurationStructure;
    }

    /**
     * @return Configuration
     */
    public function getConfigurationEntity(): Configuration
    {
        return $this->configurationEntity;
    }

    /**
     * @param Configuration $configurationEntity
     */
    public function setConfigurationEntity(Configuration $configurationEntity): void
    {
        $this->configurationEntity = $configurationEntity;
    }

    /**
     * @return array
     */
    public function getDataSet(): array
    {
        return $this->dataSet;
    }

    /**
     * @param array $dataSet
     */
    public function setDataSet(array $dataSet): void
    {
        $this->dataSet = $dataSet;
    }

    /**
     * @return ControlParameters
     *
     * @deprecated use getControlParameters instead
     */
    public function getGETParameterCollection(): ControlParameters
    {
        return $this->getControlParameters();
    }
}
