<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationVariant;

use Symfony\Contracts\EventDispatcher\Event;

class GetFrontendConfigurationVariantsEvent extends Event
{
    public const NAME = 'configuratorware.configuration_variant.get_frontend_configuration_variants';

    /**
     * @var array
     */
    private $configurationVariants;

    /**
     * @var string
     */
    private $itemIdentifier;

    /**
     * GetFrontendConfigurationVariantsEvent constructor.
     *
     * @param string $itemIdentifier
     */
    public function __construct(string $itemIdentifier)
    {
        $this->itemIdentifier = $itemIdentifier;
    }

    /**
     * @return array
     */
    public function getConfigurationVariants(): array
    {
        return $this->configurationVariants;
    }

    /**
     * @param array $configurationVariants
     */
    public function setConfigurationVariants(array $configurationVariants)
    {
        $this->configurationVariants = $configurationVariants;
    }

    /**
     * @return string
     */
    public function getItemIdentifier(): string
    {
        return $this->itemIdentifier;
    }

    /**
     * @param string $itemIdentifier
     */
    public function setItemIdentifier(string $itemIdentifier)
    {
        $this->itemIdentifier = $itemIdentifier;
    }
}
