<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Rules;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\EventDispatcher\Event;

class CheckResultsPostprocessingEvent extends Event
{
    public const NAME = 'configuratorware.rules.check_results_postprocessing';

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * CheckResultsPostprocessingEvent constructor.
     *
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }
}
