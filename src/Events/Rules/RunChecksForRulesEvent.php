<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Rules;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResults;
use Symfony\Contracts\EventDispatcher\Event;

class RunChecksForRulesEvent extends Event
{
    public const NAME = 'configuratorware.rules.run_checks_for_rules';

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var array
     */
    private $rules;

    /**
     * @var bool
     */
    private $reverse;

    /**
     * @var CheckResults
     */
    private $checkResults;

    /**
     * RunChecksForRulesEvent constructor.
     *
     * @param Configuration $configuration
     * @param array $rules
     * @param bool $reverse
     */
    public function __construct(Configuration $configuration, array $rules, bool $reverse)
    {
        $this->configuration = $configuration;
        $this->rules = $rules;
        $this->reverse = $reverse;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @param Configuration $configuration
     */
    public function setConfiguration(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return array
     */
    public function getRules(): array
    {
        return $this->rules;
    }

    /**
     * @param array $rules
     */
    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    /**
     * @return bool
     */
    public function isReverse(): bool
    {
        return $this->reverse;
    }

    /**
     * @param bool $reverse
     */
    public function setReverse(bool $reverse)
    {
        $this->reverse = $reverse;
    }

    /**
     * @return CheckResults
     */
    public function getCheckResults(): CheckResults
    {
        return $this->checkResults;
    }

    /**
     * @param CheckResults $checkResults
     */
    public function setCheckResults(CheckResults $checkResults)
    {
        $this->checkResults = $checkResults;
    }
}
