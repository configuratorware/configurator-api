<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationInfo;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationInfo;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class GetProductionInfoEvent extends Event
{
    public const NAME = 'configuratorware.configuration_info.get_production_info';

    /**
     * @var ConfigurationInfo
     */
    private $configurationInfo;

    /**
     * @var string
     */
    private $configurationCode;

    /**
     * @var ControlParameters|null
     */
    private $controlParameters;

    /**
     * GetConfigurationInfoEvent constructor.
     *
     * @param string $configurationCode
     * @param ControlParameters|null $controlParameters
     */
    public function __construct(string $configurationCode, ControlParameters $controlParameters = null)
    {
        $this->configurationCode = $configurationCode;
        $this->controlParameters = $controlParameters;
    }

    /**
     * @return ConfigurationInfo
     */
    public function getConfigurationInfo(): ConfigurationInfo
    {
        return $this->configurationInfo;
    }

    /**
     * @param ConfigurationInfo $configurationInfo
     */
    public function setConfigurationInfo(ConfigurationInfo $configurationInfo)
    {
        $this->configurationInfo = $configurationInfo;
    }

    /**
     * @return string
     */
    public function getConfigurationCode(): string
    {
        return $this->configurationCode;
    }

    /**
     * @param string $configurationCode
     */
    public function setConfigurationCode(string $configurationCode)
    {
        $this->configurationCode = $configurationCode;
    }

    /**
     * @return ControlParameters|null
     */
    public function getControlParameters(): ?ControlParameters
    {
        return $this->controlParameters;
    }
}
