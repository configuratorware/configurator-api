<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationInfo;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationInfo;
use Symfony\Contracts\EventDispatcher\Event;

class GetConfigurationInfoEvent extends Event
{
    public const NAME = 'configuratorware.configuration_info.get_configuration_info';

    private ConfigurationInfo $configurationInfo;

    private string $configurationCode;

    private bool $extendedData = false;

    public function __construct(string $configurationCode)
    {
        $this->configurationCode = $configurationCode;
    }

    public function getConfigurationInfo(): ConfigurationInfo
    {
        return $this->configurationInfo;
    }

    public function setConfigurationInfo(ConfigurationInfo $configurationInfo)
    {
        $this->configurationInfo = $configurationInfo;
    }

    public function getConfigurationCode(): string
    {
        return $this->configurationCode;
    }

    public function setConfigurationCode(string $configurationCode)
    {
        $this->configurationCode = $configurationCode;
    }

    public function getExtendedData(): bool
    {
        return $this->extendedData;
    }

    public function setExtendedData(bool $extendedData): void
    {
        $this->extendedData = $extendedData;
    }
}
