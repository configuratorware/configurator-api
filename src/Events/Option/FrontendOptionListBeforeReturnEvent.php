<?php

namespace Redhotmagma\ConfiguratorApiBundle\Events\Option;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\EventDispatcher\Event;

class FrontendOptionListBeforeReturnEvent extends Event
{
    public const NAME = 'configuratorware.option.frontend_list_before_return';

    /**
     * @array
     */
    private $options;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var string
     */
    private $itemIdentifier;

    /**
     * @var string
     */
    private $optionClassificationIdentifier;

    /**
     * @var array
     */
    private $filters;

    /**
     * @var string|null
     */
    private $query;

    /**
     * FrontendOptionStructureFromEntityEvent constructor.
     *
     * @param array $options
     * @param Configuration $configuration
     * @param string $itemIdentifier
     * @param string $optionClassificationIdentifier
     * @param array $filters
     */
    public function __construct(
        array $options,
        Configuration $configuration,
        string $itemIdentifier = '',
        string $optionClassificationIdentifier = '',
        array $filters = [],
        string $query = null
    ) {
        $this->options = $options;
        $this->configuration = $configuration;
        $this->itemIdentifier = $itemIdentifier;
        $this->optionClassificationIdentifier = $optionClassificationIdentifier;
        $this->filters = $filters;
        $this->query = $query;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
    }

    /**
     * @return Configuration
     */
    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    /**
     * @return string
     */
    public function getItemIdentifier(): string
    {
        return $this->itemIdentifier;
    }

    /**
     * @return string
     */
    public function getOptionClassificationIdentifier(): string
    {
        return $this->optionClassificationIdentifier;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return string|null
     */
    public function getQuery(): ?string
    {
        return $this->query;
    }
}
