<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Option;

use Redhotmagma\ConfiguratorApiBundle\Entity\OptionStock;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Symfony\Contracts\EventDispatcher\Event;

class OptionAddTrafficTrafficLightToStockEvent extends Event
{
    public const NAME = 'configuratorware.option.add_traffic_light_to_stock';

    /**
     * @var Option
     */
    private $optionStructure;

    /**
     * @var OptionStock
     */
    private $stockEntity;

    /**
     * @var \Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock
     */
    private $stockStructure;

    /**
     * OptionAddTrafficTrafficLightToStockEvent constructor.
     *
     * @param Option $optionStructure
     * @param OptionStock $stockEntity
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock $stockStructure
     */
    public function __construct(
        Option $optionStructure,
        OptionStock $stockEntity,
        \Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock $stockStructure
    ) {
        $this->optionStructure = $optionStructure;
        $this->stockEntity = $stockEntity;
        $this->stockStructure = $stockStructure;
    }

    /**
     * @return Option
     */
    public function getOptionStructure(): Option
    {
        return $this->optionStructure;
    }

    /**
     * @param Option $optionStructure
     */
    public function setOptionStructure(Option $optionStructure)
    {
        $this->optionStructure = $optionStructure;
    }

    /**
     * @return OptionStock
     */
    public function getStockEntity(): OptionStock
    {
        return $this->stockEntity;
    }

    /**
     * @param OptionStock $stockEntity
     */
    public function setStockEntity(OptionStock $stockEntity)
    {
        $this->stockEntity = $stockEntity;
    }

    /**
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock
     */
    public function getStockStructure(): \Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock
    {
        return $this->stockStructure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock $stockStructure
     */
    public function setStockStructure(\Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock $stockStructure)
    {
        $this->stockStructure = $stockStructure;
    }
}
