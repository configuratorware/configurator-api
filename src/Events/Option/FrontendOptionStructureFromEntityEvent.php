<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\Option;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Symfony\Contracts\EventDispatcher\Event;

class FrontendOptionStructureFromEntityEvent extends Event
{
    public const NAME = 'configuratorware.option.frontend_structure_from_entity';

    /**
     * @var Option
     */
    private $optionEntity;

    /**
     * @var \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option
     */
    private $optionStructure;

    /**
     * @var string
     */
    private $structureClassName;

    /**
     * @var string
     */
    private $baseItemIdentifier;

    /**
     * @var string
     */
    private $optionClassificationIdentifier;

    /**
     * FrontendOptionStructureFromEntityEvent constructor.
     *
     * @param Option $optionEntity
     * @param string $structureClassName
     * @param string $baseItemIdentifier
     * @param string $optionClassificationIdentifier
     */
    public function __construct(
        Option $optionEntity,
        string $structureClassName,
        string $baseItemIdentifier = '',
        string $optionClassificationIdentifier = ''
    ) {
        $this->optionEntity = $optionEntity;
        $this->structureClassName = $structureClassName;
        $this->baseItemIdentifier = $baseItemIdentifier;
        $this->optionClassificationIdentifier = $optionClassificationIdentifier;
    }

    /**
     * @return Option
     */
    public function getOptionEntity(): Option
    {
        return $this->optionEntity;
    }

    /**
     * @param Option $optionEntity
     */
    public function setOptionEntity(Option $optionEntity)
    {
        $this->optionEntity = $optionEntity;
    }

    /**
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option
     */
    public function getOptionStructure(): \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option
    {
        return $this->optionStructure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option $optionStructure
     */
    public function setOptionStructure(\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option $optionStructure)
    {
        $this->optionStructure = $optionStructure;
    }

    /**
     * @return string
     */
    public function getStructureClassName(): string
    {
        return $this->structureClassName;
    }

    /**
     * @param string $structureClassName
     */
    public function setStructureClassName(string $structureClassName)
    {
        $this->structureClassName = $structureClassName;
    }

    /**
     * @return string
     */
    public function getBaseItemIdentifier(): string
    {
        return $this->baseItemIdentifier;
    }

    /**
     * @param string $baseItemIdentifier
     */
    public function setBaseItemIdentifier(string $baseItemIdentifier)
    {
        $this->baseItemIdentifier = $baseItemIdentifier;
    }

    /**
     * @return string
     */
    public function getOptionClassificationIdentifier(): string
    {
        return $this->optionClassificationIdentifier;
    }

    /**
     * @param string $optionClassificationIdentifier
     */
    public function setOptionClassificationIdentifier(string $optionClassificationIdentifier)
    {
        $this->optionClassificationIdentifier = $optionClassificationIdentifier;
    }
}
