<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer;

use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\DTO\MailAttachment;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\CustomerOfferEmail;
use Symfony\Contracts\EventDispatcher\Event;

class CustomerEmailDataCollectionEvent extends Event
{
    public const NAME = 'configuratorware.receive_offer.customer_email_data_collection';

    public const DEFAULT_HTML_TEMPLATE = '@RedhotmagmaConfiguratorApi/receive-offer/customer.html.twig';

    public const DEFAULT_TEXT_TEMPLATE = '@RedhotmagmaConfiguratorApi/receive-offer/customer.txt.twig';

    private CustomerOfferEmail $offerRequestCustomerEmail;

    /**
     * @var MailAttachment[]
     */
    private array $mailAttachments = [];

    private string $htmlTemplate = self::DEFAULT_HTML_TEMPLATE;

    private string $textTemplate = self::DEFAULT_TEXT_TEMPLATE;

    public function __construct(CustomerOfferEmail $customerEmail)
    {
        $this->offerRequestCustomerEmail = $customerEmail;
    }

    public function setCustomerEmail(CustomerOfferEmail $customerEmail): void
    {
        $this->offerRequestCustomerEmail = $customerEmail;
    }

    public function getCustomerEmail(): CustomerOfferEmail
    {
        return $this->offerRequestCustomerEmail;
    }

    /**
     * @return MailAttachment[]
     */
    public function getMailAttachments(): array
    {
        return $this->mailAttachments;
    }

    /**
     * @param MailAttachment[] $mailAttachments
     */
    public function setMailAttachments(array $mailAttachments): void
    {
        $this->mailAttachments = $mailAttachments;
    }

    public function getHtmlTemplate(): string
    {
        return $this->htmlTemplate;
    }

    public function setHtmlTemplate(string $htmlTemplate): void
    {
        $this->htmlTemplate = $htmlTemplate;
    }

    public function getTextTemplate(): string
    {
        return $this->textTemplate;
    }

    public function setTextTemplate(string $textTemplate): void
    {
        $this->textTemplate = $textTemplate;
    }
}
