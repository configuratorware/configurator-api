<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer;

use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\OfferRequestMailData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class ReceiveOfferRequestEvent extends Event
{
    public const NAME = 'configuratorware.receive_offer.request';

    private bool $mailSent = false;

    private ReceiveOfferRequestStructure $receiveOfferRequestStructure;

    private ControlParameters $controlParameters;

    private Client $client;

    private OfferRequestMailData $mailData;

    public function __construct(
        ReceiveOfferRequestStructure $receiveOfferRequestStructure,
        ControlParameters $controlParameters,
        Client $client,
        OfferRequestMailData $mailData
    ) {
        $this->receiveOfferRequestStructure = $receiveOfferRequestStructure;
        $this->controlParameters = $controlParameters;
        $this->client = $client;
        $this->mailData = $mailData;
    }

    public function isMailSent(): bool
    {
        return $this->mailSent;
    }

    public function setMailSent(bool $mailSent): void
    {
        $this->mailSent = $mailSent;
    }

    public function getReceiveOfferRequestStructure(): ReceiveOfferRequestStructure
    {
        return $this->receiveOfferRequestStructure;
    }

    /**
     * @deprecated
     */
    public function setReceiveOfferRequestStructure(ReceiveOfferRequestStructure $receiveOfferRequestStructure): void
    {
        $this->receiveOfferRequestStructure = $receiveOfferRequestStructure;
    }

    public function getControlParameters(): ?ControlParameters
    {
        return $this->controlParameters;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getMailData(): OfferRequestMailData
    {
        return $this->mailData;
    }

    /**
     * @deprecated use getControlParameters instead
     */
    public function getGETParameterCollection(): ?ControlParameters
    {
        return $this->getControlParameters();
    }
}
