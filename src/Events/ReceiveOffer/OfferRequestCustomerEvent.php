<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer;

use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\OfferRequestMailData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\Event;

class OfferRequestCustomerEvent extends Event
{
    public const NAME = 'configuratorware.receive_offer.customer_event';

    private Client $client;

    private ReceiveOfferRequestStructure $structure;

    private ControlParameters  $controlParameters;

    private OfferRequestMailData $mailData;

    private bool $sendOfferRequestToCustomer;

    public function __construct(
        Client $client,
        ReceiveOfferRequestStructure $structure,
        ControlParameters $controlParameters,
        OfferRequestMailData $mailData
    ) {
        $this->client = $client;
        $this->structure = $structure;
        $this->controlParameters = $controlParameters;
        $this->mailData = $mailData;
        $this->sendOfferRequestToCustomer = $client->sendOfferRequestToCustomer;
    }

    public function setClient(Client $client): void
    {
        $this->client = $client;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getStructure(): ReceiveOfferRequestStructure
    {
        return $this->structure;
    }

    public function getControlParameters(): ControlParameters
    {
        return $this->controlParameters;
    }

    public function getMailData(): OfferRequestMailData
    {
        return $this->mailData;
    }

    public function setSendOfferRequestToCustomer(bool $sendOfferRequestToCustomer): void
    {
        $this->sendOfferRequestToCustomer = $sendOfferRequestToCustomer;
    }

    public function getSendOfferRequestToCustomer(): bool
    {
        return $this->sendOfferRequestToCustomer;
    }
}
