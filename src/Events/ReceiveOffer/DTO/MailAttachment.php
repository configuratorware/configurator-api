<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\DTO;

class MailAttachment
{
    private string $filePath;

    private ?string $fileName;

    private ?string $contentType;

    public function __construct(string $filePath, ?string $fileName = null, ?string $contentType = null)
    {
        $this->filePath = $filePath;
        $this->fileName = $fileName;
        $this->contentType = $contentType;
    }

    public function getFilePath(): string
    {
        return $this->filePath;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function getContentType(): ?string
    {
        return $this->contentType;
    }
}
