<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer;

use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\DTO\MailAttachment;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\ReceiveOfferEmail;
use Symfony\Contracts\EventDispatcher\Event;

class EmailDataCollectionEvent extends Event
{
    public const NAME = 'configuratorware.receive_offer.email_data_collection';

    public const DEFAULT_HTML_TEMPLATE = '@RedhotmagmaConfiguratorApi/receive-offer/client.html.twig';

    public const DEFAULT_TEXT_TEMPLATE = '@RedhotmagmaConfiguratorApi/receive-offer/client.txt.twig';

    private ReceiveOfferEmail $receiveOfferEmail;

    /**
     * @var MailAttachment[]
     */
    private array $mailAttachments = [];

    private string $htmlTemplate = self::DEFAULT_HTML_TEMPLATE;

    private string $textTemplate = self::DEFAULT_TEXT_TEMPLATE;

    public function __construct(ReceiveOfferEmail $receiveOfferEmail)
    {
        $this->receiveOfferEmail = $receiveOfferEmail;
    }

    public function setReceiveOfferEmail(ReceiveOfferEmail $receiveOfferEmail): void
    {
        $this->receiveOfferEmail = $receiveOfferEmail;
    }

    public function getReceiveOfferEmail(): ReceiveOfferEmail
    {
        return $this->receiveOfferEmail;
    }

    /**
     * @return MailAttachment[]
     */
    public function getMailAttachments(): array
    {
        return $this->mailAttachments;
    }

    /**
     * @param MailAttachment[] $mailAttachments
     */
    public function setMailAttachments(array $mailAttachments): void
    {
        $this->mailAttachments = $mailAttachments;
    }

    public function getHtmlTemplate(): string
    {
        return $this->htmlTemplate;
    }

    public function setHtmlTemplate(string $htmlTemplate): void
    {
        $this->htmlTemplate = $htmlTemplate;
    }

    public function getTextTemplate(): string
    {
        return $this->textTemplate;
    }

    public function setTextTemplate(string $textTemplate): void
    {
        $this->textTemplate = $textTemplate;
    }
}
