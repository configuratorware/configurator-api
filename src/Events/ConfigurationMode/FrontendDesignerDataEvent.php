<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationMode;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerData;

class FrontendDesignerDataEvent
{
    public const NAME = 'configuratorware.configuration_mode.frontend_designer_data';

    private Configuration $configuration;

    private DesignerData $designerData;

    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    public function getDesignerData(): DesignerData
    {
        return $this->designerData;
    }

    public function setDesignerData(DesignerData $designerData): FrontendDesignerDataEvent
    {
        $this->designerData = $designerData;

        return $this;
    }

    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }
}
