<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Events\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Structure\Option;
use Symfony\Contracts\EventDispatcher\Event;

class SaveByOptionEvent extends Event
{
    public const NAME = 'configuratorware.assembly_point.save_by_option';

    /**
     * @var Option
     */
    private $option;

    /**
     * SaveByOptionEvent constructor.
     *
     * @param Option $option
     */
    public function __construct(Option $option)
    {
        $this->option = $option;
    }

    /**
     * @return Option
     */
    public function getOption(): Option
    {
        return $this->option;
    }

    /**
     * @param Option $option
     */
    public function setOption(Option $option)
    {
        $this->option = $option;
    }
}
