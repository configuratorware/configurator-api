<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\DirectoryAlreadyExists;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Image\LayerImage;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * @internal
 */
final class LayerImageRepository implements LayerImageRepositoryInterface
{
    private const FIND_ALL_PATH_PATTERN = '!\w+/\w+!i';
    private const FIND_ALL_FILE_PATTERN = '!\w+\.(png|jpg|jpeg)$!i';
    private const SEQUENCE_NUMBER_SEPARATOR_PATTERN = '/([0-9_]*_)/';
    private const SEQUENCE_NUMBER_PATTERN = '/([\d]*)/';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $configuratorImagesPath;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * ViewLayerOptionImageRepository constructor.
     *
     * @param Filesystem $filesystem
     * @param string $mediaBasePath
     * @param string $configuratorImagesPath
     */
    public function __construct(Filesystem $filesystem, string $mediaBasePath, string $configuratorImagesPath)
    {
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
        $this->configuratorImagesPath = rtrim($configuratorImagesPath, DIRECTORY_SEPARATOR);
    }

    /**
     * {@inheritDoc}
     */
    public function findLayerImages(Item $item): array
    {
        if (!is_dir($this->createItemDirPath($item))) {
            return [];
        }

        $viewLayerOptionImages = [];

        $finder = new Finder();
        $finder->in($this->createItemDirPath($item))->path(self::FIND_ALL_PATH_PATTERN)->name(self::FIND_ALL_FILE_PATTERN)->depth('<=2')->files();

        foreach ($finder as $fileInfo) {
            $viewLayerOptionImages[] = $this->layerImageFromFileInfo($fileInfo);
        }

        return $viewLayerOptionImages;
    }

    /**
     * {@inheritDoc}
     */
    public function findLayerImagesForComponent(Item $item, Optionclassification $component): array
    {
        $basePath = $this->createItemDirPath($item);
        if (!is_dir($basePath)) {
            return [];
        }

        $pathPattern = sprintf('/.*\/[0-9]+_%s/', $component->getIdentifier());

        $finder = new Finder();
        $finder->in($basePath)->path($pathPattern)->depth('<=2')->files();

        $viewLayerOptionImages = [];
        foreach ($finder as $fileInfo) {
            $viewLayerOptionImages[] = $this->layerImageFromFileInfo($fileInfo);
        }

        return $viewLayerOptionImages;
    }

    private function layerImageFromFileInfo(SplFileInfo $fileInfo): LayerImage
    {
        return LayerImage::from(
            $this->createUrl($fileInfo),
            $this->retrieveComponentIdentifier($fileInfo),
            $this->retrieveOptionIdentifier($fileInfo),
            $this->retrieveViewIdentifier($fileInfo),
            $fileInfo->getPathname(),
            $fileInfo->getFilename(),
            $this->retrieveComponentSequenceNumber($fileInfo),
            $this->retrieveViewSequenceNumber($fileInfo)
        );
    }

    /**
     * {@inheritDoc}
     */
    public function findLayerImage(Item $item, Optionclassification $component, Option $option, CreatorView $creatorView): LayerImage
    {
        $basePath = $this->createItemDirPath($item) . DIRECTORY_SEPARATOR . $creatorView->getSequenceNumber() . '_' . $creatorView->getIdentifier();
        $pathPattern = sprintf('/[0-9]+_%s/', $component->getIdentifier());
        $filePattern = sprintf('/%s.(png|jpg|jpeg)/', $option->getIdentifier());

        $finder = new Finder();
        $finder->in($basePath)->path($pathPattern)->name($filePattern)->depth('<=2')->files();

        foreach ($finder->getIterator() as $fileInfo) {
            return $this->layerImageFromFileInfo($fileInfo);
        }

        throw FileException::notFound($basePath . $pathPattern . $filePattern);
    }

    /**
     * {@inheritDoc}
     */
    public function saveLayerImage(LayerImage $layerImage, Item $item): void
    {
        $basePath = $this->createItemDirPath($item);
        $pathPattern = sprintf('/[0-9]+_%s\/[0-9]+_%s/', $layerImage->getViewIdentifier(), $layerImage->getComponentIdentifier());

        $finder = new Finder();
        $finder->in($basePath)->path($pathPattern)->directories();

        $dir = null;
        foreach ($finder as $found) {
            $dir = $found->getPathname();

            break;
        }

        if (null === $dir) {
            $viewDirectory = $item->getIdentifier() . DIRECTORY_SEPARATOR . $layerImage->getViewSequenceNumber() . '_' . $layerImage->getViewIdentifier();
            $viewDirPath = $this->configuratorImagesPath . DIRECTORY_SEPARATOR . $viewDirectory;
            $componentSequenceNumber = $layerImage->getComponentSequenceNumber() ?? 0;
            $dir = $this->createComponentDirectory($viewDirPath, $componentSequenceNumber, $layerImage->getComponentIdentifier());
            $this->filesystem->mkdir($dir);
        }

        // find existing file
        $finder = new Finder();
        /** @var \Iterator<SplFileInfo> $foundFiles */
        $foundFiles = $finder->in($dir)->files()->name(sprintf('/%s.(png|jpg|jpeg)/', $layerImage->getOptionIdentifier()))->getIterator();

        // create or overwrite new layer image
        $layerImageName = $layerImage->getOptionIdentifier() . '.' . pathinfo($layerImage->getFilename(), PATHINFO_EXTENSION);
        $layerImagePath = $dir . DIRECTORY_SEPARATOR . $layerImageName;
        $this->filesystem->rename($layerImage->getRealPath(), $layerImagePath, true);
        $this->filesystem->chmod($layerImagePath, 0666);

        if (null === $foundFiles) {
            return;
        }

        // delete old if the file was of different mimetype
        $foundFiles->rewind();
        /** @var SplFileInfo $existingFile */
        $existingFile = $foundFiles->current();
        if (null !== $existingFile && $existingFile->getPathname() !== $layerImagePath) {
            $this->filesystem->remove($existingFile->getPathname());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function deleteLayerImage(LayerImage $layerImage, Item $item): void
    {
        $this->filesystem->remove($layerImage->getRealPath());
    }

    /**
     * {@inheritDoc}
     */
    public function createStores(CreatorView $view, Item $item): string
    {
        $paths = [];

        $viewDirectory = $this->createViewDirectory($view);
        $viewDirPath = $this->configuratorImagesPath . DIRECTORY_SEPARATOR . $viewDirectory;

        if (is_dir($viewDirPath)) {
            throw DirectoryAlreadyExists::withPath($viewDirPath);
        }

        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($item->getItemOptionclassification() as $i => $itemOptionClassification) {
            $componentSequenceNumber = $itemOptionClassification->getSequenceNumber() ?? $i;
            $componentIdentifier = $itemOptionClassification->getOptionclassification()->getIdentifier();

            $paths[] = $this->createComponentDirectory($viewDirPath, $componentSequenceNumber, $componentIdentifier);
        }

        $this->filesystem->mkdir($paths);

        return $viewDirectory;
    }

    /**
     * @param string $viewDirPath
     * @param int $componentSequenceNumber
     * @param string $componentIdentifier
     *
     * @return string
     */
    private function createComponentDirectory(string $viewDirPath, int $componentSequenceNumber, string $componentIdentifier): string
    {
        return $viewDirPath . DIRECTORY_SEPARATOR . $componentSequenceNumber . '_' . $componentIdentifier;
    }

    /**
     * @param CreatorView $view
     *
     * @return string
     */
    private function createViewDirectory(CreatorView $view): string
    {
        return $view->getItemIdentifier() . DIRECTORY_SEPARATOR . $view->getSequenceNumber() . '_' . $view->getIdentifier();
    }

    /**
     * @param Item $item
     *
     * @return string
     */
    private function createItemDirPath(Item $item): string
    {
        return $this->configuratorImagesPath . DIRECTORY_SEPARATOR . $item->getIdentifier();
    }

    /**
     * @param \SplFileInfo $fileInfo
     *
     * @return string
     */
    private function createUrl(\SplFileInfo $fileInfo): string
    {
        return str_replace($this->mediaBasePath, '', $fileInfo->getPathname());
    }

    /**
     * @param \SplFileInfo $fileInfo
     *
     * @return string
     */
    private function retrieveComponentIdentifier(\SplFileInfo $fileInfo): string
    {
        $parts = explode(DIRECTORY_SEPARATOR, $fileInfo->getPathname());
        end($parts);

        return preg_replace(self::SEQUENCE_NUMBER_SEPARATOR_PATTERN, '', prev($parts), 1);
    }

    /**
     * @param \SplFileInfo $fileInfo
     *
     * @return int
     */
    private function retrieveComponentSequenceNumber(\SplFileInfo $fileInfo): int
    {
        $parts = explode(DIRECTORY_SEPARATOR, $fileInfo->getPathname());
        end($parts);

        $matches = [];
        preg_match(self::SEQUENCE_NUMBER_PATTERN, prev($parts), $matches);

        return (int) ($matches[0] ?? 0);
    }

    /**
     * @param \SplFileInfo $fileInfo
     *
     * @return string
     */
    private function retrieveViewIdentifier(\SplFileInfo $fileInfo): string
    {
        $parts = explode(DIRECTORY_SEPARATOR, $fileInfo->getPathname());
        end($parts);
        prev($parts);

        return preg_replace(self::SEQUENCE_NUMBER_SEPARATOR_PATTERN, '', prev($parts), 1);
    }

    /**
     * @param \SplFileInfo $fileInfo
     *
     * @return string
     */
    private function retrieveViewSequenceNumber(\SplFileInfo $fileInfo): string
    {
        $parts = explode(DIRECTORY_SEPARATOR, $fileInfo->getPathname());
        end($parts);
        prev($parts);

        $matches = [];
        preg_match(self::SEQUENCE_NUMBER_PATTERN, prev($parts), $matches);

        return $matches[0] ?? '0';
    }

    /**
     * @param \SplFileInfo $fileInfo
     *
     * @return string
     */
    private function retrieveOptionIdentifier(\SplFileInfo $fileInfo): string
    {
        return str_replace('.' . $fileInfo->getExtension(), '', $fileInfo->getFilename());
    }
}
