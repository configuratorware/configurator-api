<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Image\ComponentThumbnail;

interface ComponentThumbnailRepositoryInterface
{
    /**
     * @param Optionclassification[] $components
     *
     * @return ComponentThumbnail[]
     */
    public function findThumbnails(array $components): array;

    /**
     * @param Optionclassification $component
     *
     * @return ComponentThumbnail
     */
    public function findThumbnail(Optionclassification $component): ComponentThumbnail;

    /**
     * @param ComponentThumbnail $thumbnail
     * @param Optionclassification $component
     */
    public function saveThumbnail(ComponentThumbnail $thumbnail, Optionclassification $component): void;

    /**
     * @param ComponentThumbnail $thumbnail
     */
    public function deleteThumbnail(ComponentThumbnail $thumbnail): void;
}
