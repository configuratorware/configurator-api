<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Image\InspirationThumbnail;

interface InspirationThumbnailRepositoryInterface
{
    /**
     * @param Inspiration $inspiration
     *
     * @return InspirationThumbnail
     */
    public function findThumbnail(Inspiration $inspiration): InspirationThumbnail;

    /**
     * @param InspirationThumbnail $thumbnail
     * @param Inspiration $inspiration
     *
     * @return InspirationThumbnail
     */
    public function saveThumbnail(InspirationThumbnail $thumbnail, Inspiration $inspiration): InspirationThumbnail;

    /**
     * @param InspirationThumbnail $thumbnail
     */
    public function deleteThumbnail(InspirationThumbnail $thumbnail): void;
}
