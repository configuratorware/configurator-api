<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;

interface CreatorViewThumbnailRepositoryInterface
{
    /**
     * @param Item $item
     *
     * @return CreatorViewThumbnail[]
     */
    public function findThumbnails(Item $item): array;

    /**
     * @param CreatorViewThumbnail $creatorViewThumbnail
     * @param Item $item
     * @param CreatorView $creatorView
     */
    public function saveThumbnail(CreatorViewThumbnail $creatorViewThumbnail, Item $item, CreatorView $creatorView): void;

    /**
     * @param CreatorViewThumbnail $creatorViewThumbnail
     */
    public function deleteThumbnail(CreatorViewThumbnail $creatorViewThumbnail): void;
}
