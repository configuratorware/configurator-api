<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Exception\ImageEntityMismatch;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDirectory;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject\ImageFinder;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
final class OptionImageRepository implements OptionDetailImageRepositoryInterface, OptionThumbnailRepositoryInterface
{
    private const DETAIL_IMAGE_SUFFIX = '_fullsize';

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var string
     */
    private $optionImagePath;

    /**
     * @param string $mediaBasePath
     * @param string $optionImagePath
     */
    public function __construct(Filesystem $filesystem, string $mediaBasePath, string $optionImagePath)
    {
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
        $this->optionImagePath = trim($optionImagePath, DIRECTORY_SEPARATOR);
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImage(Option $option): OptionDetailImage
    {
        return $this->findDetailImageByIdentifier($option->getIdentifier());
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImageByIdentifier(string $optionIdentifier): OptionDetailImage
    {
        $file = ImageFinder::for($this->getBasePath())->findImagePathBy($optionIdentifier . self::DETAIL_IMAGE_SUFFIX);

        if (null === $file) {
            throw FileException::noMatch($this->getBasePath() . DIRECTORY_SEPARATOR . $optionIdentifier . self::DETAIL_IMAGE_SUFFIX);
        }

        return OptionDetailImage::from(
            $this->createUrl($file->getFilename()),
            $this->trimIdentifierFromFileName($file->getFilename()),
            $file->getPathname(),
            $file->getFilename()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImages(array $options): array
    {
        $detailImages = [];
        foreach ($options as $option) {
            try {
                $detailImages[] = $this->findDetailImageByIdentifier($option->getIdentifier());
            } catch (FileException $exception) {
                // no useful exception handling possible
            }
        }

        return $detailImages;
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImagesByIdentifiers(array $optionIdentifiers): array
    {
        $detailImages = [];
        foreach ($optionIdentifiers as $optionIdentifier) {
            try {
                $detailImages[] = $this->findDetailImageByIdentifier($optionIdentifier);
            } catch (FileException $exception) {
                // no useful exception handling possible
            }
        }

        return $detailImages;
    }

    /**
     * {@inheritDoc}
     */
    public function saveDetailImage(OptionDetailImage $detailImage, Option $option): void
    {
        if (!$detailImage->isForOption($option->getIdentifier())) {
            throw ImageEntityMismatch::forOption($option->getIdentifier());
        }

        $detailImagePath = $this->createDetailImagePath($detailImage);
        $this->createDirIfNotExist($detailImagePath);

        $existingFile = ImageFinder::for($this->getBasePath())->findImagePathBy($option->getIdentifier() . self::DETAIL_IMAGE_SUFFIX);
        if ($existingFile) {
            $this->filesystem->remove($existingFile->getPathname());
        }
        $this->filesystem->rename($detailImage->getRealPath(), $detailImagePath, true);
        $this->filesystem->chmod($detailImagePath, 0666);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteDetailImage(OptionDetailImage $detailImage): void
    {
        $this->filesystem->remove($detailImage->getRealPath());
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnail(Option $option): OptionThumbnail
    {
        return $this->findThumbnailByIdentifier($option->getIdentifier());
    }

    public function findThumbnailByIdentifier(string $optionIdentifier): OptionThumbnail
    {
        $file = ImageFinder::for($this->getBasePath())->findImagePathBy($optionIdentifier);

        if (null === $file) {
            throw FileException::noMatch($this->getBasePath() . DIRECTORY_SEPARATOR . $optionIdentifier);
        }

        return OptionThumbnail::from(
            $this->createUrl($file->getFilename()),
            $this->trimIdentifierFromFileName($file->getFilename()),
            $file->getPathname(),
            $file->getFilename()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnails(array $options): array
    {
        $thumbnailImages = [];
        foreach ($options as $option) {
            try {
                $thumbnailImages[] = $this->findThumbnailByIdentifier($option->getIdentifier());
            } catch (FileException $exception) {
                // no useful exception handling possible
            }
        }

        return $thumbnailImages;
    }

    public function findThumbnailsByIdentifiers(array $optionIdentifiers): array
    {
        $thumbnailImages = [];
        foreach ($optionIdentifiers as $optionIdentifier) {
            try {
                $thumbnailImages[] = $this->findThumbnailByIdentifier($optionIdentifier);
            } catch (FileException $exception) {
                // no useful exception handling possible
            }
        }

        return $thumbnailImages;
    }

    /**
     * {@inheritDoc}
     */
    public function saveThumbnail(OptionThumbnail $optionThumbnail, Option $option): void
    {
        if (!$optionThumbnail->isForOption($option->getIdentifier())) {
            throw ImageEntityMismatch::forOption($option->getIdentifier());
        }

        $thumbnailPath = $this->createThumbnailPath($optionThumbnail);

        $this->createDirIfNotExist($thumbnailPath);

        $existingFile = ImageFinder::for($this->getBasePath())->findImagePathBy($option->getIdentifier());

        if ($existingFile) {
            $this->filesystem->remove($existingFile->getPathname());
        }

        $this->filesystem->rename($optionThumbnail->getRealPath(), $thumbnailPath, true);
        $this->filesystem->chmod($thumbnailPath, 0666);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteThumbnail(OptionThumbnail $optionThumbnail): void
    {
        $this->filesystem->remove($optionThumbnail->getRealPath());
    }

    private function getBasePath(): string
    {
        $basePath = $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->optionImagePath;

        if (!is_dir($basePath)) {
            $this->filesystem->mkdir($basePath);
        }

        return $basePath;
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function trimIdentifierFromFileName(string $fileName): string
    {
        $name = pathinfo($fileName, PATHINFO_FILENAME);

        return str_replace(self::DETAIL_IMAGE_SUFFIX, '', $name);
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function createUrl(string $fileName): string
    {
        return DIRECTORY_SEPARATOR . $this->optionImagePath . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * @param OptionDetailImage $optionDetailImage
     *
     * @return string
     */
    private function createDetailImagePath(OptionDetailImage $optionDetailImage): string
    {
        $pathPattern = OptionDetailImage::PLACEHOLDER_OPTION_IDENTIFIER . self::DETAIL_IMAGE_SUFFIX . '.' . OptionDetailImage::PLACEHOLDER_IMAGE_EXT;

        return $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->optionImagePath . DIRECTORY_SEPARATOR . $optionDetailImage->createStorePath($pathPattern);
    }

    /**
     * @param OptionThumbnail $optionThumbnail
     *
     * @return string
     */
    private function createThumbnailPath(OptionThumbnail $optionThumbnail): string
    {
        $pathPattern = OptionDetailImage::PLACEHOLDER_OPTION_IDENTIFIER . '.' . OptionDetailImage::PLACEHOLDER_IMAGE_EXT;

        return $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->optionImagePath . DIRECTORY_SEPARATOR . $optionThumbnail->createStorePath($pathPattern);
    }

    /**
     * @param string $imagePath
     */
    private function createDirIfNotExist(string $imagePath): void
    {
        $separatorPosition = strrpos($imagePath, DIRECTORY_SEPARATOR);
        if (false === $separatorPosition) {
            throw InvalidDirectory::noDirectorySeparator($imagePath);
        }

        $dir = substr($imagePath, 0, $separatorPosition);
        if (!is_dir($dir)) {
            $this->filesystem->mkdir($dir);
        }
    }
}
