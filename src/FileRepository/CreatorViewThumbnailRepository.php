<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\ImageEntityMismatch;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDirectory;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidItem;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * @internal
 */
final class CreatorViewThumbnailRepository implements CreatorViewThumbnailRepositoryInterface
{
    /**
     * @var string
     */
    private $creatorViewThumbnailPath;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param string $creatorViewThumbnailPath
     * @param Filesystem $filesystem
     * @param string $mediaBasePath
     */
    public function __construct(string $creatorViewThumbnailPath, Filesystem $filesystem, string $mediaBasePath)
    {
        $this->creatorViewThumbnailPath = trim($creatorViewThumbnailPath, DIRECTORY_SEPARATOR);
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnails(Item $item): array
    {
        if (null !== $item->getParent()) {
            throw InvalidItem::hasParent($item->getIdentifier());
        }

        $path = $this->getViewDirectoryPath($item);

        if (!is_dir($path)) {
            return [];
        }

        $finder = new Finder();
        $finder->in($path)->files()->name('/(.+)\.(png|jpg|jpeg)$/Ui');

        if (false === $finder->hasResults()) {
            return [];
        }

        $viewThumbnails = [];
        foreach ($finder as $fileInfo) {
            $url = $this->retrieveUrl($fileInfo);
            $viewIdentifier = $fileInfo->getFilenameWithoutExtension();
            $itemIdentifier = $fileInfo->getRelativePath();

            $viewThumbnails[] = CreatorViewThumbnail::from($url, $viewIdentifier, $itemIdentifier, $fileInfo->getPathName(), $fileInfo->getFilename());
        }

        return $viewThumbnails;
    }

    /**
     * {@inheritDoc}
     */
    public function saveThumbnail(CreatorViewThumbnail $creatorViewThumbnail, Item $item, CreatorView $creatorView): void
    {
        if (null !== $item->getParent()) {
            throw InvalidItem::hasParent($item->getIdentifier());
        }

        if (!$creatorViewThumbnail->isForCreatorView($creatorView->getIdentifier())) {
            throw ImageEntityMismatch::forCreatorView($creatorView->getIdentifier());
        }

        $thumbnailPath = $this->createPathForThumbnail($creatorViewThumbnail, $item);

        $separatorPosition = strrpos($thumbnailPath, DIRECTORY_SEPARATOR);
        if (false === $separatorPosition) {
            throw InvalidDirectory::noDirectorySeparator($thumbnailPath);
        }

        $dir = substr($thumbnailPath, 0, $separatorPosition);
        if (!is_dir($dir)) {
            $this->filesystem->mkdir($dir);
        }

        // find existing file
        $finder = new Finder();
        /** @var \Iterator<SplFileInfo> $foundFiles */
        $foundFiles = $finder->in($dir)->files()->name(sprintf('/%s.(png|jpg|jpeg)/', $creatorView->getIdentifier()))->getIterator();

        // create or overwrite file
        $this->filesystem->rename($creatorViewThumbnail->getRealPath(), $thumbnailPath, true);
        $this->filesystem->chmod($thumbnailPath, 0666);

        if (null === $foundFiles) {
            return;
        }

        // delete old if the file was of different mimetype
        $foundFiles->rewind();
        /** @var SplFileInfo $existingFile */
        $existingFile = $foundFiles->current();
        if (null !== $existingFile && $existingFile->getPathname() !== $thumbnailPath) {
            $this->filesystem->remove($existingFile->getPathname());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function deleteThumbnail(CreatorViewThumbnail $creatorViewThumbnail): void
    {
        $this->filesystem->remove($creatorViewThumbnail->getRealPath());
    }

    /**
     * @param CreatorViewThumbnail $thumbnail
     * @param Item $item
     *
     * @return string
     */
    private function createPathForThumbnail(CreatorViewThumbnail $thumbnail, Item $item): string
    {
        $pathPattern = CreatorViewThumbnail::PLACEHOLDER_ITEM_IDENTIFIER . DIRECTORY_SEPARATOR . CreatorViewThumbnail::PLACEHOLDER_VIEW_IDENTIFIER . '.' . CreatorViewThumbnail::PLACEHOLDER_IMAGE_EXT;

        return $this->getViewDirectoryPath($item) . DIRECTORY_SEPARATOR . $thumbnail->createStorePath($pathPattern);
    }

    /**
     * @param Item $item
     *
     * @return string
     */
    private function getViewDirectoryPath(Item $item): string
    {
        return $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->creatorViewThumbnailPath . DIRECTORY_SEPARATOR . $item->getIdentifier();
    }

    /**
     * @param SplFileInfo $fileInfo
     *
     * @return string
     */
    private function retrieveUrl(SplFileInfo $fileInfo): string
    {
        return str_replace($this->mediaBasePath, '', $fileInfo->getPathname());
    }
}
