<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Image\DesignViewDetailImage;

interface DesignViewDetailImageRepositoryInterface
{
    /**
     * finds the image for a certain item, parent or child.
     *
     * @param Item $item
     * @param DesignView $designView
     *
     * @return DesignViewDetailImage
     */
    public function findDetailImage(Item $item, DesignView $designView): DesignViewDetailImage;

    public function saveDetailImage(DesignViewDetailImage $designViewImage): void;

    public function deleteDetailImage(DesignViewDetailImage $designViewImage): void;
}
