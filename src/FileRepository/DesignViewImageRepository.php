<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDesignView;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDirectory;
use Redhotmagma\ConfiguratorApiBundle\Image\DesignViewDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject\ImageFinder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * @internal
 */
final class DesignViewImageRepository implements DesignViewDetailImageRepositoryInterface
{
    /**
     * @var string
     */
    private $designViewImagePath;

    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param string $designViewImagePath
     * @param Filesystem $filesystem
     * @param string $mediaBasePath
     */
    public function __construct(string $designViewImagePath, Filesystem $filesystem, string $mediaBasePath)
    {
        $this->designViewImagePath = trim($designViewImagePath, DIRECTORY_SEPARATOR);
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
    }

    /**
     * {@inheritDoc}
     */
    public function findDetailImage(Item $item, DesignView $designView): DesignViewDetailImage
    {
        $itemIdentifier = $item->getIdentifier();
        $designViewIdentifier = $designView->getIdentifier();
        if (null === $designViewIdentifier) {
            throw InvalidDesignView::missingIdentifier();
        }

        $parentIdentifier = $item->getIdentifier();
        if (null !== $item->getParent()) {
            $parentIdentifier = $item->getParent()->getIdentifier();
        }

        $dir = $this->createDesignViewImagePath() . DIRECTORY_SEPARATOR . $parentIdentifier . DIRECTORY_SEPARATOR . $designView->getIdentifier();
        if (!is_dir($dir)) {
            throw InvalidDirectory::notFound($dir);
        }

        $detailImage = ImageFinder::for($dir)->findImagePathBy($itemIdentifier);

        if (null === $detailImage) {
            throw FileException::notFound($dir . DIRECTORY_SEPARATOR . $itemIdentifier);
        }

        $imagePath = $detailImage->getPathname();

        return DesignViewDetailImage::from($this->creatUrl($imagePath), $designViewIdentifier, $parentIdentifier, $itemIdentifier, $imagePath, pathinfo($imagePath, PATHINFO_BASENAME));
    }

    /**
     * {@inheritDoc}
     */
    public function saveDetailImage(DesignViewDetailImage $designViewImage): void
    {
        $imagePath = $this->createPathForImage($designViewImage);

        $dir = pathinfo($imagePath, PATHINFO_DIRNAME);
        if (!is_dir($dir)) {
            $this->filesystem->mkdir($dir);
        }

        // find existing file
        $finder = new Finder();
        /** @var \Iterator<SplFileInfo> $foundFiles */
        $foundFiles = $finder->in($dir)->files()->name(sprintf('/%s.(png|jpg|jpeg)/', $designViewImage->getChildIdentifier()))->getIterator();

        // create or overwrite file
        $this->filesystem->copy($designViewImage->getRealPath(), $imagePath, true);
        $this->filesystem->chmod($imagePath, 0666);

        if (null === $foundFiles) {
            return;
        }

        // delete old if the file was of different mimetype
        $foundFiles->rewind();
        /** @var SplFileInfo $existingFile */
        $existingFile = $foundFiles->current();
        if (null !== $existingFile && $existingFile->getPathname() !== $imagePath) {
            $this->filesystem->remove($existingFile->getPathname());
        }
    }

    /**
     * @param DesignViewDetailImage $designViewImage
     */
    public function deleteDetailImage(DesignViewDetailImage $designViewImage): void
    {
        $this->filesystem->remove($designViewImage->getRealPath());
    }

    /**
     * @param string $path
     *
     * @return string
     */
    private function creatUrl(string $path): string
    {
        return str_replace($this->mediaBasePath, '', $path);
    }

    /**
     * @param DesignViewDetailImage $designViewImage
     *
     * @return string
     */
    private function createPathForImage(DesignViewDetailImage $designViewImage): string
    {
        $pattern = DesignViewDetailImage::PLACEHOLDER_PARENT_ITEM_IDENTIFIER . DIRECTORY_SEPARATOR . DesignViewDetailImage::PLACEHOLDER_VIEW_IDENTIFIER . DIRECTORY_SEPARATOR . DesignViewDetailImage::PLACEHOLDER_CHILD_ITEM_IDENTIFIER . '.' . DesignViewDetailImage::PLACEHOLDER_IMAGE_EXT;

        return $this->createDesignViewImagePath() . DIRECTORY_SEPARATOR . $designViewImage->createStorePath($pattern);
    }

    /**
     * @return string
     */
    private function createDesignViewImagePath(): string
    {
        return $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->designViewImagePath;
    }
}
