<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Exception\ImageEntityMismatch;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDirectory;
use Redhotmagma\ConfiguratorApiBundle\Image\ComponentThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject\ImageFinder;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
final class ComponentImageRepository implements ComponentThumbnailRepositoryInterface
{
    /**
     * @var string
     */
    private $componentImagePath;

    /**
     * @var string
     */
    private $mediaBasePath;
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param string $componentImagePath
     * @param Filesystem $filesystem
     * @param string $mediaBasePath
     */
    public function __construct(string $componentImagePath, Filesystem $filesystem, string $mediaBasePath)
    {
        $this->componentImagePath = trim($componentImagePath, DIRECTORY_SEPARATOR);
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnails(array $components): array
    {
        $thumbnails = [];
        foreach ($components as $component) {
            try {
                $thumbnails[] = $this->findThumbnail($component);
            } catch (FileException $exception) {
                // no useful exception handling possible
            }
        }

        return $thumbnails;
    }

    public function findThumbnail(Optionclassification $component): ComponentThumbnail
    {
        $image = ImageFinder::for($this->getBasePath())->findImagePathBy($component->getIdentifier());

        if (!$image) {
            throw FileException::notFound($this->getBasePath() . $component->getIdentifier());
        }

        return ComponentThumbnail::from(
            $this->createUrl($image->getFilename()),
            $this->trimIdentifierFromFileName($image->getFilename()),
            $image->getPath(),
            $image->getFilename()
        );
    }

    /**
     * {@inheritDoc}
     */
    public function saveThumbnail(ComponentThumbnail $thumbnail, Optionclassification $component): void
    {
        if (!$thumbnail->isForComponent($component->getIdentifier())) {
            throw ImageEntityMismatch::forComponent($component->getIdentifier());
        }

        $thumbnailPath = $this->createPathFromThumbnail($thumbnail);

        $separatorPosition = strrpos($thumbnailPath, DIRECTORY_SEPARATOR);
        if (false === $separatorPosition) {
            throw InvalidDirectory::noDirectorySeparator($thumbnailPath);
        }

        $dir = substr($thumbnailPath, 0, $separatorPosition);
        if (!is_dir($dir)) {
            $this->filesystem->mkdir($dir);
        }

        $existingFile = ImageFinder::for($dir)->findImagePathBy($component->getIdentifier());

        if ($existingFile) {
            $this->filesystem->remove($existingFile->getPathname());
        }
        // create or overwrite file
        $this->filesystem->rename($thumbnail->getRealPath(), $thumbnailPath, true);
        $this->filesystem->chmod($thumbnailPath, 0666);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteThumbnail(ComponentThumbnail $thumbnail): void
    {
        $this->filesystem->remove($thumbnail->getRealPath());
    }

    /**
     * @return string
     */
    private function getBasePath(): string
    {
        $basePath = $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->componentImagePath;

        if (!is_dir($basePath)) {
            $this->filesystem->mkdir($basePath);
        }

        return $basePath;
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function trimIdentifierFromFileName(string $fileName): string
    {
        return pathinfo($fileName, PATHINFO_FILENAME);
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function createUrl(string $fileName): string
    {
        return DIRECTORY_SEPARATOR . $this->componentImagePath . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * @param ComponentThumbnail $thumbnail
     *
     * @return string
     */
    private function createPathFromThumbnail(ComponentThumbnail $thumbnail): string
    {
        $pattern = ComponentThumbnail::PLACEHOLDER_COMPONENT_IDENTIFIER . '.' . ComponentThumbnail::PLACEHOLDER_IMAGE_EXT;

        return $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->componentImagePath . DIRECTORY_SEPARATOR . $thumbnail->createStorePath($pattern);
    }
}
