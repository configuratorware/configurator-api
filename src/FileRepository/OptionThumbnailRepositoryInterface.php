<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionThumbnail;

interface OptionThumbnailRepositoryInterface
{
    /**
     * @throws FileException when no image was found
     */
    public function findThumbnail(Option $option): OptionThumbnail;

    /**
     * @throws FileException when no image was found
     */
    public function findThumbnailByIdentifier(string $optionIdentifier): OptionThumbnail;

    /**
     * @param Option[] $options
     *
     * @return OptionThumbnail[]
     */
    public function findThumbnails(array $options): array;

    /**
     * @param string[] $optionIdentifiers
     *
     * @return OptionThumbnail[]
     */
    public function findThumbnailsByIdentifiers(array $optionIdentifiers): array;

    /**
     * @param OptionThumbnail $optionThumbnail
     * @param Option $option
     */
    public function saveThumbnail(OptionThumbnail $optionThumbnail, Option $option): void;

    /**
     * @param OptionThumbnail $optionThumbnail
     */
    public function deleteThumbnail(OptionThumbnail $optionThumbnail): void;
}
