<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\DTO\CreatorView as CreatorViewDTO;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
final class CreatorViewFileRepository implements CreatorViewFileRepositoryInterface
{
    /**
     * @var string
     */
    private $configuratorImagesPath;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @param string $configuratorImagesPath
     * @param Filesystem $filesystem
     */
    public function __construct(string $configuratorImagesPath, Filesystem $filesystem)
    {
        $this->configuratorImagesPath = rtrim($configuratorImagesPath, DIRECTORY_SEPARATOR);
        $this->filesystem = $filesystem;
    }

    public function save(CreatorView $view): string
    {
        $newDirectory = $this->createNewViewDirectory($view);
        $newPath = $this->configuratorImagesPath . DIRECTORY_SEPARATOR . $newDirectory;
        $directory = $view->getDirectory();
        if (empty($directory)) {
            if (!is_dir($newPath)) {
                $this->filesystem->mkdir($newPath);
            }

            return $newDirectory;
        }

        $existingPath = $this->createPathFrom($view);
        if ($existingPath === $newPath) {
            return $directory;
        }

        $this->filesystem->rename($existingPath, $newPath);

        return $newDirectory;
    }

    public function delete(CreatorView $view): void
    {
        $directory = $view->getDirectory();
        if (null === $directory || count(explode(DIRECTORY_SEPARATOR, $directory)) < 2) {
            return;
        }

        $dirName = $this->createPathFrom($view);
        if (is_dir($dirName)) {
            $this->filesystem->remove($dirName);
        }
    }

    /**
     * @param Item $item
     *
     * @return array|CreatorViewDTO[]
     */
    public function findCreatorViews(Item $item): array
    {
        $viewDirectory = $this->getViewsDirectory($item->getIdentifier());
        if (!is_dir($viewDirectory)) {
            return [];
        }

        $finder = new Finder();
        $finder->in($viewDirectory)->directories()->depth('<1');

        $creatorViews = [];
        foreach ($finder as $fileInfo) {
            $dirName = $fileInfo->getFilename();
            $viewIdentifier = $this->trimViewFolderName($dirName);
            $viewSequenceNumber = $this->trimViewOrderNumber($dirName);

            $creatorViews[] = CreatorViewDTO::from($viewIdentifier, $viewSequenceNumber, $item->getIdentifier());
        }

        return $creatorViews;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return string
     */
    private function getViewsDirectory(string $itemIdentifier): string
    {
        return $this->configuratorImagesPath . DIRECTORY_SEPARATOR . $itemIdentifier;
    }

    /**
     * @param string $viewDirectoryName
     *
     * @return string
     */
    private function trimViewFolderName(string $viewDirectoryName): string
    {
        return preg_replace('/^\d+_/', '', $viewDirectoryName);
    }

    /**
     * @param $dirName
     *
     * @return string
     */
    private function trimViewOrderNumber($dirName): string
    {
        preg_match('/^\d+_/', $dirName, $matchingString);

        return trim($matchingString[0] ?? '', '_');
    }

    /**
     * @param CreatorView $view
     *
     * @return string
     *
     * @psalm-suppress PossiblyNullArgument
     */
    private function createPathFrom(CreatorView $view): string
    {
        return $this->configuratorImagesPath . DIRECTORY_SEPARATOR . trim($view->getDirectory(), DIRECTORY_SEPARATOR);
    }

    /**
     * @param CreatorView $view
     *
     * @return string
     */
    private function createNewViewDirectory(CreatorView $view): string
    {
        return $view->getItemIdentifier() . DIRECTORY_SEPARATOR . $view->getSequenceNumber() . '_' . $view->getIdentifier();
    }
}
