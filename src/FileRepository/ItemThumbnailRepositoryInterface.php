<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemThumbnail;

interface ItemThumbnailRepositoryInterface
{
    /**
     * @param Item $item
     *
     * @return ItemThumbnail[]
     */
    public function findThumbnails(Item $item): array;
}
