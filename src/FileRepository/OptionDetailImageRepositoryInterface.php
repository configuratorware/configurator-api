<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionDetailImage;

interface OptionDetailImageRepositoryInterface
{
    /**
     * @throws FileException when no image was found
     */
    public function findDetailImage(Option $option): OptionDetailImage;

    /**
     * @throws FileException when no image was found
     */
    public function findDetailImageByIdentifier(string $optionIdentifier): OptionDetailImage;

    /**
     * @param Option[] $options
     *
     * @return OptionDetailImage[]
     */
    public function findDetailImages(array $options): array;

    /**
     * @param string[] $optionIdentifiers
     *
     * @return OptionDetailImage[]
     */
    public function findDetailImagesByIdentifiers(array $optionIdentifiers): array;

    /**
     * @param OptionDetailImage $detailImage
     * @param Option $option
     */
    public function saveDetailImage(OptionDetailImage $detailImage, Option $option): void;

    /**
     * @param OptionDetailImage $detailImage
     */
    public function deleteDetailImage(OptionDetailImage $detailImage): void;
}
