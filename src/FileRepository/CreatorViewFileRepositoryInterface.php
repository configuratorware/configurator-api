<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\DTO\CreatorView as CreatorViewDTO;

interface CreatorViewFileRepositoryInterface
{
    /**
     * @param CreatorView $view
     */
    public function save(CreatorView $view): string;

    /**
     * @param CreatorView $view
     */
    public function delete(CreatorView $view): void;

    /**
     * @param Item $item
     *
     * @return array|CreatorViewDTO[]
     */
    public function findCreatorViews(Item $item): array;
}
