<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\ComponentCombination;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\OptionCombination;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\SelectedOptions;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationResourcesRepositoryInterface;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
final class VisualizationResourcesRepository implements VisualizationResourcesRepositoryInterface
{
    public const COMPONENTS_FOLDER = '/components';
    public const BASE_MODEL_FOLDER = '/basemodel';
    public const CREATOR_IDENTIFIER = 'creator';

    /**
     * @var string
     */
    private $creatorDirPattern;

    public function __construct(string $visualizationComponentDataDir)
    {
        $this->creatorDirPattern = $visualizationComponentDataDir;
    }

    /** {@inheritDoc} */
    public function findBaseModelFor(string $itemIdentifier): array
    {
        $itemPath = $this->toItemPath($itemIdentifier);
        $baseModelPath = $itemPath . self::BASE_MODEL_FOLDER;
        if (!file_exists($baseModelPath)) {
            return [];
        }

        $finder = new Finder();
        $finder->files()->depth(0);

        return iterator_to_array($finder->in($baseModelPath));
    }

    /** {@inheritDoc} */
    public function findComponentCombinationsFor(string $itemIdentifier): array
    {
        $itemPath = $this->toItemPath($itemIdentifier);
        $componentsFolder = $itemPath . self::COMPONENTS_FOLDER;
        if (!file_exists($componentsFolder)) {
            return [];
        }
        $finder = new Finder();
        $finder->directories()->depth(0);

        $combinations = [];
        foreach ($finder->in($componentsFolder) as $file) {
            $combinations[] = new ComponentCombination($file);
        }

        return $combinations;
    }

    /** {@inheritDoc} */
    public function findOptionsFor(ComponentCombination $componentCombination, Configuration $configuration): array
    {
        $optionCombinations = [];
        if (!file_exists($componentCombination->optionFolder)) {
            return $optionCombinations;
        }

        $finder = new Finder();
        $finder->directories();

        foreach ($finder->in($componentCombination->optionFolder) as $file) {
            $optionCombination = new OptionCombination($file);
            if ($optionCombination->doesMatch(SelectedOptions::inOrderForComponents($configuration, $componentCombination->components))) {
                $optionCombinations[] = $optionCombination;
            }
        }

        return $optionCombinations;
    }

    /** {@inheritDoc} */
    public function findVisualizationFilesForComponent(ComponentCombination $componentCombination): array
    {
        $finder = new Finder();
        $finder->files()->depth(0);

        return iterator_to_array($finder->in($componentCombination->componentFolder));
    }

    /** {@inheritDoc} */
    public function findVisualizationFilesForOption(OptionCombination $optionCombination): array
    {
        $finder = new Finder();

        return iterator_to_array($finder->in($optionCombination->pathName));
    }

    private function toItemPath(string $itemIdentifier): string
    {
        return str_replace(['{configurationMode}', '{itemIdentifier}'], [self::CREATOR_IDENTIFIER, $itemIdentifier], $this->creatorDirPattern);
    }
}
