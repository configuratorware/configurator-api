<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemThumbnail;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
final class ItemImageRepository implements ItemDetailImageRepositoryInterface, ItemThumbnailRepositoryInterface
{
    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var string
     */
    private $itemDetailImagePath;

    /**
     * @var string
     */
    private $itemThumbnailPath;

    /**
     * @param string $mediaBasePath
     * @param string $itemDetailImagePath
     * @param string $itemThumbnailPath
     */
    public function __construct(string $mediaBasePath, string $itemDetailImagePath, string $itemThumbnailPath)
    {
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
        $this->itemDetailImagePath = trim($itemDetailImagePath, DIRECTORY_SEPARATOR);
        $this->itemThumbnailPath = trim($itemThumbnailPath, DIRECTORY_SEPARATOR);
    }

    /**
     * {@inheritdoc}
     */
    public function findDetailImages(Item $item): array
    {
        $basePath = $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->itemDetailImagePath;
        if (!is_dir($basePath)) {
            return [];
        }

        $finder = new Finder();
        $finder->in($basePath)->files()->name($this->createRegex($item));

        $detailImages = [];
        foreach ($finder as $fileInfo) {
            $fileName = $fileInfo->getFilename();
            $detailImages[] = ItemDetailImage::from($this->createUrl($this->itemDetailImagePath, $fileName), $this->trimIdentifierFromFileName($fileName));
        }

        return $detailImages;
    }

    /**
     * {@inheritdoc}
     */
    public function findThumbnails(Item $item): array
    {
        $basePath = $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->itemThumbnailPath;
        if (!is_dir($basePath)) {
            return [];
        }

        $finder = new Finder();
        $finder->in($basePath)->files()->name($this->createRegex($item));

        $thumbnails = [];
        foreach ($finder as $fileInfo) {
            $fileName = $fileInfo->getFilename();
            $thumbnails[] = ItemThumbnail::from($this->createUrl($this->itemThumbnailPath, $fileName), $this->trimIdentifierFromFileName($fileName));
        }

        return $thumbnails;
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function trimIdentifierFromFileName(string $fileName): string
    {
        return pathinfo($fileName, PATHINFO_FILENAME);
    }

    /**
     * @param Item $item
     *
     * @return string
     */
    private function createRegex(Item $item): string
    {
        $identifiers = [];
        $identifiers[] = $item->getIdentifier();
        foreach ($item->getItem() as $child) {
            $identifiers[] = $child->getIdentifier();
        }

        return '/(' . implode('|', $identifiers) . ').(png|jpg|jpeg)$/i';
    }

    /**
     * @param string $imagePath
     * @param string $fileName
     *
     * @return string
     */
    private function createUrl(string $imagePath, string $fileName): string
    {
        return DIRECTORY_SEPARATOR . $imagePath . DIRECTORY_SEPARATOR . $fileName;
    }
}
