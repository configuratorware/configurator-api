<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Image\LayerImage;

interface LayerImageRepositoryInterface
{
    /**
     * @param Item $item
     *
     * @return array|LayerImage[]
     */
    public function findLayerImages(Item $item): array;

    /**
     * @param Item $item
     * @param Optionclassification $component
     *
     * @return array|LayerImage[]
     */
    public function findLayerImagesForComponent(Item $item, Optionclassification $component): array;

    /**
     * @param Item $item
     * @param Optionclassification $component
     * @param Option $option
     * @param CreatorView $creatorView
     *
     * @return LayerImage
     */
    public function findLayerImage(Item $item, Optionclassification $component, Option $option, CreatorView $creatorView): LayerImage;

    /**
     * @param LayerImage $layerImage
     * @param Item $item
     */
    public function saveLayerImage(LayerImage $layerImage, Item $item): void;

    /**
     * @param LayerImage $layerImage
     * @param Item $item
     */
    public function deleteLayerImage(LayerImage $layerImage, Item $item): void;

    /**
     * @param CreatorView $view
     * @param Item $item
     */
    public function createStores(CreatorView $view, Item $item): string;
}
