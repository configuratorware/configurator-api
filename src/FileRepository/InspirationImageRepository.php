<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Image\InspirationThumbnail;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
final class InspirationImageRepository implements InspirationThumbnailRepositoryInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $mediaBasePath;
    /**
     * @var string
     */
    private $inspirationThumbnailPath;

    public function __construct(Filesystem $filesystem, string $mediaBasePath, string $inspirationThumbnailPath)
    {
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
        $this->inspirationThumbnailPath = trim($inspirationThumbnailPath, DIRECTORY_SEPARATOR);
    }

    /**
     * {@inheritDoc}
     */
    public function findThumbnail(Inspiration $inspiration): InspirationThumbnail
    {
        $thumbnailPath = $this->mediaBasePath . $inspiration->getThumbnail();

        if (!$this->filesystem->exists($thumbnailPath)) {
            throw FileException::notFound($thumbnailPath);
        }

        $item = $inspiration->getItem();
        $filename = pathinfo($thumbnailPath, PATHINFO_BASENAME);

        return InspirationThumbnail::from($this->createUrl($item, $filename), $this->trimIdFromFileName($filename), $item->getIdentifier(), $thumbnailPath, $filename);
    }

    /**
     * {@inheritDoc}
     */
    public function saveThumbnail(InspirationThumbnail $thumbnail, Inspiration $inspiration): InspirationThumbnail
    {
        $thumbnailToAdd = $this->getBasePath() . DIRECTORY_SEPARATOR . $thumbnail->createStorePath(InspirationThumbnail::PLACEHOLDER_ITEM_IDENTIFIER . DIRECTORY_SEPARATOR . InspirationThumbnail::PLACEHOLDER_INSPIRATION_ID . '.' . InspirationThumbnail::PLACEHOLDER_IMAGE_EXT);

        $dir = pathinfo($thumbnailToAdd, PATHINFO_DIRNAME);
        if (!is_dir($dir)) {
            $this->filesystem->mkdir($dir);
        }

        $this->filesystem->rename($thumbnail->getRealPath(), $thumbnailToAdd, true);
        $this->filesystem->chmod($thumbnailToAdd, 0666);

        $this->removePresentThumbnail($inspiration, $thumbnailToAdd);

        $item = $inspiration->getItem();
        $filename = pathinfo($thumbnailToAdd, PATHINFO_BASENAME);

        return InspirationThumbnail::from($this->createUrl($item, $filename), $this->trimIdFromFileName($filename), $item->getIdentifier(), $thumbnailToAdd, $filename);
    }

    /**
     * {@inheritDoc}
     */
    public function deleteThumbnail(InspirationThumbnail $thumbnail): void
    {
        $this->filesystem->remove($thumbnail->getRealPath());
    }

    /**
     * @return string
     */
    private function getBasePath(): string
    {
        return $this->mediaBasePath . DIRECTORY_SEPARATOR . $this->inspirationThumbnailPath;
    }

    /**
     * @param string $filename
     *
     * @return int
     */
    public function trimIdFromFileName(string $filename): int
    {
        return (int)pathinfo($filename, PATHINFO_FILENAME);
    }

    /**
     * @param Item $item
     * @param string $filename
     *
     * @return string
     */
    private function createUrl(Item $item, string $filename): string
    {
        return DIRECTORY_SEPARATOR . $this->inspirationThumbnailPath . DIRECTORY_SEPARATOR . $item->getIdentifier() . DIRECTORY_SEPARATOR . $filename;
    }

    /**
     * @param Inspiration $inspiration
     * @param string $thumbnailToAdd
     */
    private function removePresentThumbnail(Inspiration $inspiration, string $thumbnailToAdd): void
    {
        if (null === $inspiration->getThumbnail()) {
            return;
        }

        $presentThumbnail = $this->mediaBasePath . $inspiration->getThumbnail();
        if ($presentThumbnail === $thumbnailToAdd) {
            return;
        }

        $this->filesystem->remove($presentThumbnail);
    }
}
