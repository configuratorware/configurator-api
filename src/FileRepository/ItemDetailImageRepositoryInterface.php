<?php

namespace Redhotmagma\ConfiguratorApiBundle\FileRepository;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemDetailImage;

interface ItemDetailImageRepositoryInterface
{
    /**
     * @param Item $item
     *
     * @return ItemDetailImage[]
     */
    public function findDetailImages(Item $item): array;
}
