<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use RuntimeException;

/**
 * @internal
 */
final class ClassDoesNotExist extends RuntimeException
{
    /**
     * @param string $className
     *
     * @return static
     */
    public static function withFQN(string $className): self
    {
        return new self(sprintf('A Class %s already exists.', $className));
    }
}
