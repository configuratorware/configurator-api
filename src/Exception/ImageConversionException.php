<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class ImageConversionException extends \RuntimeException
{
    public static function cannotCreateImageFromPath(\Throwable $previous): self
    {
        return new self('Failed to create Imagick image from path', 0, $previous);
    }

    public static function cannotConvertToRGBColorspace(): self
    {
        return new self('Conversion to RGB color space returned without creating temp image');
    }

    public static function conversionFailed(\Throwable $previous): self
    {
        return new self('Failed to process image', 0, $previous);
    }
}
