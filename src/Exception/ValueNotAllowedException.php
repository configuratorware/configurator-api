<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use RuntimeException;

/**
 * @internal
 */
final class ValueNotAllowedException extends RuntimeException
{
    /**
     * @param string $value
     * @param array $expected
     *
     * @return static
     */
    public static function expected(string $value, array $expected): self
    {
        return new self(sprintf('The given value: %s is not allowed. Expected: %s.', $value, implode(',', $expected)));
    }
}
