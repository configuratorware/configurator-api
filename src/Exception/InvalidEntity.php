<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class InvalidEntity extends \DomainException
{
    public static function withProperty(string $className, string $propertyName): self
    {
        return new self("$className::$propertyName is invalid.");
    }
}
