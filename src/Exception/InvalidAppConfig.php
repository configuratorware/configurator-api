<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class InvalidAppConfig extends \Exception
{
    public static function missing(string $parameter): self
    {
        return new self("Config value for $parameter is missing.");
    }
}
