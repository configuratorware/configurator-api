<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class InvalidRequest extends \RuntimeException
{
    /**
     * @param string $content
     *
     * @return static
     */
    public static function withContent(string $content): self
    {
        return new self(sprintf('The request with the following content: %s could not be processed.', $content));
    }

    /**
     * @param string $urlParam
     *
     * @return static
     */
    public static function urlParamExpected(string $urlParam): self
    {
        return new self(sprintf('The request needs to be contain an url parameter: %s', $urlParam));
    }

    /**
     * @param array $urlParams
     *
     * @return static
     */
    public static function urlParamsExpected(array $urlParams): self
    {
        return new self(sprintf('The request requires multiple url parameters: %s', json_encode($urlParams)));
    }

    /**
     * @param string $multipartFormKey
     *
     * @return static
     */
    public static function fileExpected(string $multipartFormKey): self
    {
        return new self(sprintf('The request needs to be contain a file with multipart-form-key: %s', $multipartFormKey));
    }
}
