<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use DomainException;

/**
 * @internal
 */
final class InvalidCreatorView extends DomainException
{
    /**
     * @return static
     */
    public static function missingId(): self
    {
        return new self('The id cannot be null.');
    }
}
