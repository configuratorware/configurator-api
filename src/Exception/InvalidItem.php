<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use RuntimeException;

/**
 * @internal
 */
final class InvalidItem extends RuntimeException
{
    /**
     * @param string $itemIdentifier
     *
     * @return static
     */
    public static function hasParent(string $itemIdentifier): self
    {
        return new self(sprintf('The item with identifier: %s has parents and can therefore not be processed here.', $itemIdentifier));
    }
}
