<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use RuntimeException;

/**
 * @internal
 */
final class InvalidDirectory extends RuntimeException
{
    /**
     * @param string $path
     *
     * @return static
     */
    public static function notFound(string $path): self
    {
        return new self(sprintf('Directory %s could not be found.', $path));
    }

    /**
     * @param string $path
     *
     * @return static
     */
    public static function noDirectorySeparator(string $path): self
    {
        return new self(sprintf('Path %s has no directory separator, but it is expected.', $path));
    }
}
