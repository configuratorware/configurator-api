<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class ImageEntityMismatch extends \RuntimeException
{
    /**
     * @param string $identifier
     *
     * @return static
     */
    public static function forComponent(string $identifier): self
    {
        return new self("The provided Image and Component (= OptionClassification) do not match for identifier: $identifier");
    }

    /**
     * @param string $identifier
     *
     * @return static
     */
    public static function forOption(string $identifier): self
    {
        return new self("The provided Image and Option do not match for identifier: $identifier");
    }

    /**
     * @param string $identifier
     *
     * @return static
     */
    public static function forCreatorView(string $identifier): self
    {
        return new self("The provided Image and CreatorView do not match for identifier: $identifier");
    }
}
