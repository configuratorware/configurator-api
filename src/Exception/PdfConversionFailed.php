<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class PdfConversionFailed extends \RuntimeException
{
    public static function withTool(string $toolName, string $htmlContent): self
    {
        return new self("Pdf creation with $toolName failed for the following content: $htmlContent");
    }
}
