<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use RuntimeException;

/**
 * @internal
 */
final class FileException extends RuntimeException
{
    /**
     * @param string $path
     *
     * @return static
     */
    public static function alreadyExists(string $path): self
    {
        return new self(sprintf('File: %s already exists.', $path));
    }

    /**
     * @param string $path
     *
     * @return static
     */
    public static function notFound(string $path): self
    {
        return new self(sprintf('File: %s could not be found.', $path));
    }

    /**
     * @param string $pattern
     *
     * @return static
     */
    public static function noMatch(string $pattern): self
    {
        return new self(sprintf('File with pattern %s does not exist.', $pattern));
    }

    /**
     * @param string $path
     * @param string $expected
     *
     * @return static
     */
    public static function wrongExtension(string $path, string $expected): self
    {
        return new self(sprintf('File: %s has the wrong extension, expected %s.', $path, $expected));
    }

    /**
     * @param string $path
     *
     * @return static
     */
    public static function cannotGuessExtension(string $path): self
    {
        return new self(sprintf('Could not guess extension for File: %s.', $path));
    }

    /**
     * @param string $path
     * @param string $expected
     *
     * @return static
     */
    public static function wrongMimeType(string $path, string $expected): self
    {
        return new self(sprintf('File: %s has the wrong mimeType, expected %s.', $path, $expected));
    }

    /**
     * @param string $path
     *
     * @return static
     */
    public static function cannotGuessMimeType(string $path): self
    {
        return new self(sprintf('Could not guess mimetype for File: %s.', $path));
    }
}
