<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use DomainException;

/**
 * @internal
 */
final class InvalidDesignView extends DomainException
{
    /**
     * @return static
     */
    public static function missingIdentifier(): self
    {
        return new self('The identifier cannot be null.');
    }

    /**
     * @return static
     */
    public static function missingItem(): self
    {
        return new self('The item cannot be null.');
    }
}
