<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class NotFoundException extends \RuntimeException
{
    /**
     * @param string $className
     *
     * @return static
     */
    public static function entity(string $className): self
    {
        return new self(sprintf('Entity: %s not found.', $className));
    }

    /**
     * @param string $className
     * @param $id
     *
     * @return static
     */
    public static function entityWithId(string $className, $id): self
    {
        return new self("Entity $className with id $id not found");
    }
}
