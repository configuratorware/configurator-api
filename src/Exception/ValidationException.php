<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class ValidationException extends \RuntimeException
{
    /**
     * @var array
     */
    private $violations;

    public function __construct(array $violations = [])
    {
        $this->violations = $violations;
    }

    /**
     * @return array
     */
    public function getViolations(): array
    {
        return $this->violations;
    }
}
