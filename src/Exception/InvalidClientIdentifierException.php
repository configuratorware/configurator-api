<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class InvalidClientIdentifierException extends \DomainException
{
    /**
     * @return static
     */
    public static function mustBeDefault(): self
    {
        return new self('the default client identifier must not be changed');
    }
}
