<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class InvalidOptionExclusionRule extends \DomainException
{
    /**
     * @return static
     */
    public static function missingReverseRuleIds(): self
    {
        return new self('An id must be set for leading and reverse Rule.');
    }
}
