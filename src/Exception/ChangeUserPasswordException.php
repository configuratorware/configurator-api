<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class ChangeUserPasswordException extends \RuntimeException
{
    /**
     * @return static
     */
    public static function oldPasswordInvalid(): self
    {
        return new self('Old password invalid.');
    }

    /**
     * @return static
     */
    public static function newPasswordEmpty(): self
    {
        return new self('New password empty.');
    }
}
