<?php

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class MinimumOrderAmountException extends \RuntimeException
{
    /**
     * @var array
     */
    private $violations;

    public function __construct(array $violations = [], int $code = 0, \Throwable $previous = null)
    {
        $this->violations = $violations;
    }

    /**
     * @return array
     */
    public function getViolations(): array
    {
        return $this->violations;
    }
}
