<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class UserNotProvided extends \LogicException
{
    public static function bySecurity(): self
    {
        return new self('User was not provided by user provider');
    }
}
