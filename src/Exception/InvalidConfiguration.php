<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class InvalidConfiguration extends \DomainException
{
    public static function missingCode(): self
    {
        return new self('Configuration code missing.');
    }
}
