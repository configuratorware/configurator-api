<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class ExternalResourceException extends \RuntimeException
{
    /**
     * @param string $url
     *
     * @return static
     */
    public static function httpFailed(string $url): self
    {
        return new self(sprintf('The http request to %s failed', $url));
    }

    /**
     * @param string $expected
     *
     * @return static
     */
    public static function invalidSchema(string $expected): self
    {
        return new self(sprintf('The Schema provided is invalid, expected: %s', $expected));
    }
}
