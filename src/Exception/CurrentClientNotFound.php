<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class CurrentClientNotFound extends \DomainException
{
    /**
     * @param $id
     *
     * @return static
     */
    public static function withId($id): self
    {
        return new self("A client with id $id could not be found.");
    }
}
