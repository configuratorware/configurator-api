<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

/**
 * @internal
 */
final class EmailNotSendException extends \RuntimeException
{
    private array $stackTrace = [];

    public static function receiveOffer(array $stackTrace = []): self
    {
        $exception = new self('ReceiveOffer Email could not be sent.');
        $exception->stackTrace = $stackTrace;

        return $exception;
    }

    public static function customerOffer(array $stackTrace = []): self
    {
        $exception = new self('Customer Offer Email could not be sent.');
        $exception->stackTrace = $stackTrace;

        return $exception;
    }

    public static function missingRequiredFields(): self
    {
        return new self('Please specify in Clients at least: fromEmailAddress and toEmailAddress');
    }

    public function getStackTrace(): array
    {
        return $this->stackTrace;
    }
}
