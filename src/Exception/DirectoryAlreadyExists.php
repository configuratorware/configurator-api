<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use RuntimeException;

/**
 * @internal
 */
final class DirectoryAlreadyExists extends RuntimeException
{
    /**
     * @param string $path
     *
     * @return static
     */
    public static function withPath(string $path): self
    {
        return new self(sprintf('A Directory with the path: %s already exists', $path));
    }
}
