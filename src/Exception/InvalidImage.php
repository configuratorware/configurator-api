<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Exception;

use RuntimeException;

/**
 * @internal
 */
final class InvalidImage extends RuntimeException
{
    /**
     * @param string $imageType
     * @param $identifier
     *
     * @return static
     */
    public static function missingUrl(string $imageType, string $identifier): self
    {
        return new self(sprintf('The %s-image for %s needs to provide a url.', $imageType, $identifier));
    }
}
