<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignView
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var ItemRelation
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\ItemRelation")
     */
    public $item;

    /**
     * @var OptionRelation|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\OptionRelation")
     */
    public $option;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * @var array<DesignViewText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignViewText>")
     */
    public $texts;

    /**
     * @var array<DesignAreaRelation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaRelation>")
     */
    public $designAreas;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customData;

    /**
     * @var array<mixed>|null
     * @Serializer\Type("array")
     */
    public $images;
}
