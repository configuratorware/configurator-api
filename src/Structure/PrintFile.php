<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

class PrintFile
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $designAreaIdentifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $printFilePath;
}
