<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class QuestionTreeText
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Regex("/[a-z]{2}_[A-Z]{2}/")
     */
    public $language;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;
}
