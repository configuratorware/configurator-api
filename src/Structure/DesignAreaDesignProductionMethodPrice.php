<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignAreaDesignProductionMethodPrice
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $channel;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $colorAmountFrom;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $amountFrom;

    /**
     * @var float
     * @Serializer\Type("float")
     * @Serializer\Groups({"administrationarea"})
     */
    public $price;

    /**
     * @var float
     * @Serializer\Type("float")
     * @Serializer\Groups({"administrationarea"})
     */
    public $priceNet;
}
