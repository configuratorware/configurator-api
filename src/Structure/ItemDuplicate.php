<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ItemDuplicate
{
    /**
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public ?string $identifier;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemtext>")
     *
     * @var Itemtext[]
     */
    public array $itemTexts = [];
}
