<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class OptionPool.
 */
class OptionPool
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var array<Option>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Option>")
     */
    public $options;

    /**
     * @var array<OptionPoolText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\OptionPoolText>")
     */
    public $texts;
}
