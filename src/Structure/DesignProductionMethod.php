<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignProductionMethod
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $options;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $minimumOrderAmount;

    /**
     * @var array<DesignProductionMethodText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodText>")
     */
    public $texts;

    /**
     * @var array<ColorPaletteRelation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ColorPaletteRelation>")
     */
    public $colorPalettes;

    /**
     * @var array<DesignerProductionCalculationType>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationType>")
     */
    public $calculationTypes;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customData;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $translatedTitle;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $defaultColors;

    /**
     * @Serializer\Type("boolean")
     */
    public $vectorizedLogoMandatory;
}
