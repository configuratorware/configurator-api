<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class User.
 */
class Item
{
    /**
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $deactivated;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $accumulateAmounts;

    /**
     * title from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_title;

    /**
     * abstract from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_abstract;

    /**
     * description from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_description;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemtext>")
     */
    public $texts;

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemattribute>")
     * @Assert\Valid
     */
    public $attributes;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $price;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemprice>")
     * @Assert\Valid
     */
    public $prices;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Stock>")
     * @Assert\Valid
     */
    public $stock;

    /**
     * @var array<ItemClassification|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemclassification>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification>")
     */
    public $itemclassifications;

    /**
     * @var array<ChildItem>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ChildItem>")
     */
    public $children = [];

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $minimumOrderAmount;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $configurationMode;

    /**
     * @var ItemStatus
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus")
     */
    public $itemStatus;

    /**
     * @var VisualizationMode|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationMode")
     */
    public $visualizationMode;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $callToAction;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $overwriteComponentOrder;

    /**
     * @Serializer\Type("bool")
     */
    public $overrideOptionOrder;
}
