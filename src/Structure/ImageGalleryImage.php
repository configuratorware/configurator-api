<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ImageGalleryImage.
 */
class ImageGalleryImage
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $imageUrl;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $thumbUrl;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $printUrl;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * title from imagegalleryimagetext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $translated_title;

    /**
     * description from imagegalleryimagetext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $translated_description;

    /**
     * @var array<ImageGalleryImageText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImageText>")
     * @Serializer\Groups({"administrationarea"})
     */
    public $texts;

    /**
     * @var array<Tag>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Tag>")
     * @Serializer\Groups({"administrationarea"})
     */
    public $tags;

    /**
     * @var array<ImageGalleryImagePrice>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImagePrice>")
     * @Assert\Valid
     */
    public array $prices;
}
