<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidImage;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;

final class ViewMediaInfo
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $sequenceNumber;

    /**
     * @var MediaInfoImage
     */
    public $thumbnail;

    /**
     * @param int|null $id
     * @param string $viewIdentifier
     * @param string $sequenceNumber
     * @param MediaInfoImage $thumbnail
     */
    private function __construct(?int $id, string $viewIdentifier, string $sequenceNumber, MediaInfoImage $thumbnail)
    {
        $this->id = $id;
        $this->identifier = $viewIdentifier;
        $this->sequenceNumber = $sequenceNumber;
        $this->thumbnail = $thumbnail;
    }

    /**
     * @param CreatorView               $view
     * @param CreatorViewThumbnail $viewThumbnail
     *
     * @return ViewMediaInfo
     */
    public static function fromViewThumbnail(CreatorView $view, CreatorViewThumbnail $viewThumbnail): self
    {
        if (null === $viewThumbnail->getUrl()) {
            throw InvalidImage::missingUrl(get_class($viewThumbnail), $viewThumbnail->getRealPath());
        }

        return new self($view->getId(), $view->getIdentifier(), $view->getSequenceNumber(), MediaInfoImage::fromPath($viewThumbnail->getUrl()));
    }

    /**
     * @param CreatorView $view
     * @param string $expectedSource
     *
     * @return ViewMediaInfo
     */
    public static function forMissing(CreatorView $view, string $expectedSource): self
    {
        return new self($view->getId(), $view->getIdentifier(), $view->getSequenceNumber(), MediaInfoImage::forMissing($expectedSource));
    }
}
