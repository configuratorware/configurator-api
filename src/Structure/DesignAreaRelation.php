<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignAreaRelation
{
    // Columns which should be retrieved from DesignArea and not from DesignViewDesignArea
    public const LOCKED_ORIGIN_PROPERTIES = ['id'];

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $isDefault;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $baseShape;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $position;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $customData;

    /**
     * @param int|null $id
     * @param string|null $identifier
     * @param bool $isDefault
     * @param string|null $title
     * @param $baseShape
     * @param $position
     * @param $customData
     *
     * @psalm-suppress PossiblyNullPropertyAssignmentValue
     */
    public function __construct(int $id = null, string $identifier = null, bool $isDefault = false, string $title = null, $baseShape = null, $position = null, $customData = null)
    {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->isDefault = $isDefault;
        $this->title = $title;
        $this->baseShape = $baseShape;
        $this->position = $position;
        $this->customData = $customData;
    }
}
