<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Channel.
 */
class Channel
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Currency")
     */
    public $currency;

    /**
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $settings;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $globalDiscountPercentage;
}
