<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Tag.
 */
class Tag
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * title from tagtranslation for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $translated_title;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"administrationarea"})
     */
    public $is_multiselect;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"administrationarea"})
     */
    public $is_background;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * @var array<Tagtranslation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Tagtranslation>")
     * @Serializer\Groups({"administrationarea"})
     */
    public $translations;
}
