<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignProductionMethodRelation
{
    // Columns which should be retrieved from DesignProductionMethod and not from DesignAreaDesignProductionMethod
    public const LOCKED_ORIGIN_PROPERTIES = ['id'];

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @Serializer\Type("string")
     */
    public $mask;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $height;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $width;

    /**
     * @var bool|null
     * @Serializer\Type("bool")
     */
    public $isDefault;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $options;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $minimumOrderAmount;

    /**
     * @var array<DesignerProductionCalculationTypeDesignAreaRelation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationTypeDesignAreaRelation>")
     */
    public $calculationTypes;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customData;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $allowBulkNames;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $additionalData;

    /**
     * @Serializer\Type("string")
     */
    public $defaultColors;

    /**
     * @var bool|null
     * @Serializer\Type("boolean")
     */
    public $designElementsLocked;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $maxImages;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $maxTexts;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $maxElements;

    /**
     * @var bool|null
     * @Serializer\Type("boolean")
     */
    public $oneLineText;
}
