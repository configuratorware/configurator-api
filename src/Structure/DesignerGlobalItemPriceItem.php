<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignerGlobalItemPriceItem
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var array<DesignerGlobalCalculationTypeRelation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationTypeRelation>")
     */
    public $calculationTypes;
}
