<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class GroupEntry
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $translation;

    /**
     * @var array<GroupEntryTranslation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntryTranslation>")
     */
    public $translations;
}
