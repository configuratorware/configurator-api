<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ColorPalette.
 */
class ColorPalette
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var Color|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Color")
     */
    public $defaultColor;

    /**
     * @var array<ColorPaletteText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ColorPaletteText>")
     */
    public $texts;

    /**
     * @var array<Color>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Color>")
     */
    public $colors;
}
