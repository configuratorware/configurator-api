<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Setting
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var float
     * @Serializer\Type("float")
     * @Assert\NotBlank()
     */
    public $maxZoom2d;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customCss;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $defaultVisualizationModeLabel;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $showItemIdentifier = false;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $calculationMethod;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $shareurl;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $configurationsClientRestricted;
}
