<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class GroupRelation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * translation from Grouptranslation for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $translation;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry")
     */
    public $groupEntry;
}
