<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData;

class VisualizationData3dVariant
{
    /**
     * @var array
     */
    public $designViews = [];
}
