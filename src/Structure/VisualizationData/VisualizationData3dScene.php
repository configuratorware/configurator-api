<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData;

class VisualizationData3dScene
{
    /**
     * @var array<VisualizationObject>
     */
    public $sceneData = [];
}
