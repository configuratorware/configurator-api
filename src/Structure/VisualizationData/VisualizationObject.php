<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData;

class VisualizationObject
{
    public const MODEL_EXTENSION = '/(gltf|glb)/';
    public const PARAMS_EXTENSION = 'json';
    public const FILES_EXTENSION = '/(png|jpg|jpeg)/';

    /**
     * @var \stdClass|null
     */
    public $params;

    /**
     * @var string|null
     */
    public $model;

    /**
     * @var array
     */
    public $files = [];

    public function __construct(array $files, ?string $model, ?\stdClass $params)
    {
        $this->files = $files;
        $this->model = $model;
        $this->params = $params;
    }
}
