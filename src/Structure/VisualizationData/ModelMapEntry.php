<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData;

class ModelMapEntry
{
    /**
     * @var string
     */
    public $model;

    // TODO: Define array type

    /**
     * @var array
     */
    public $params = [];

    /**
     * @var string
     */
    public $texture;
}
