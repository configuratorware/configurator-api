<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData;

class SceneData
{
    /**
     * @var string
     */
    public $src;

    /**
     * @var Material
     */
    public $material;
}
