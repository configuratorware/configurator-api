<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData;

class ModelMap
{
    /**
     * @var array
     */
    public $itemModels = [];

    /**
     * @var array
     */
    public $optionModels = [];
}
