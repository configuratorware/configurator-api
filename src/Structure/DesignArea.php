<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignArea
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\ItemRelation")
     */
    public $item;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\OptionRelation")
     */
    public $option;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $mask;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * @var array<DesignAreaText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaText>")
     */
    public $texts;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $height;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $width;

    /**
     * @var array<DesignProductionMethodRelation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodRelation>")
     */
    public $designProductionMethods;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customData;
}
