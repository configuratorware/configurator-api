<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Internal;

class PaginationResult
{
    /**
     * @var array
     */
    public $data;

    /**
     * @var int
     */
    public $count;

    /**
     * @param array $data
     * @param int $count
     */
    public function __construct(array $data = [], int $count = 0)
    {
        $this->data = $data;
        $this->count = $count;
    }
}
