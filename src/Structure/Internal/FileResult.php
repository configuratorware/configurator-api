<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Internal;

class FileResult
{
    public const EXT_ZIP = '.zip';

    /**
     * @var string
     */
    public $filePath;

    /**
     * @var string
     */
    public $fileName;

    /**
     * @var string|null
     */
    public $mimeType;

    /**
     * @param string $filePath
     * @param string $fileName
     * @param string|null $mimeType
     */
    public function __construct(string $filePath, string $fileName, string $mimeType = null)
    {
        $fileName = str_replace(['\\', '/'], '', $fileName);

        $this->filePath = $filePath;
        $this->fileName = preg_replace('/ {1,}/', '_', $fileName);
        $this->mimeType = $mimeType;
    }
}
