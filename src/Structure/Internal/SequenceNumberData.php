<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Internal;

class SequenceNumberData
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $sequencenumber;
}
