<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignerProductionCalculationType
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $colorAmountDependent;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $itemAmountDependent;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $selectableByUser;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $declareSeparately;

    /**
     * @var array<DesignerProductionCalculationTypeText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationTypeText>")
     */
    public $texts;

    /**
     * @var array<DesignProductionMethodPrice>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodPrice>")
     */
    public $prices;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $bulkNameDependent;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $pricePerItem;
}
