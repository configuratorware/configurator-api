<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class SelectableOptionDeltaPrice
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public $price;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public $priceNet;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $channel;

    /**
     * SelectableOptionDeltaPrice constructor.
     *
     * @param int|null $id
     * @param float|null $price
     * @param float|null $priceNet
     * @param int|null $channel
     */
    public function __construct(?int $id = null, ?float $price = null, ?float $priceNet = null, ?int $channel = null)
    {
        $this->id = $id;
        $this->price = $price;
        $this->priceNet = $priceNet;
        $this->channel = $channel;
    }
}
