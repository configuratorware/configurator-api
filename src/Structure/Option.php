<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Option
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $configurable;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $deactivated;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_title;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_abstract;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_description;

    /**
     * @var array<OptionText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\OptionText>")
     * @Assert\Valid
     */
    public $texts;

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemattribute>")
     * @Assert\Valid
     */
    public $attributes;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\OptionPrice>")
     * @Assert\Valid
     */
    public $prices;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequencenumber;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\OptionStock>")
     * @Assert\Valid
     */
    public $stock;

    /**
     * @var array<mixed>
     * @Serializer\Type("object")
     */
    public $visualizationdata;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $thumbnail;

    /**
     * @var bool|null
     * @Serializer\Type("boolean")
     */
    public $hasTextinput;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $inputValidationType;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $inputText;
}
