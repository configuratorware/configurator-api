<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

class LicenseFile
{
    /**
     * @Serializer\Type("array")
     */
    public $license;

    /**
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $credentials;
}
