<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AttributeRelation.
 */
class AttributeRelation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $attribute_id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * title from Attributetext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_title;

    /**
     * @var array<Attributetext>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Attributetext>")
     */
    public $texts;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $attributedatatype;

    /**
     * @var array<Attributevalue>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue>")
     * @Assert\Valid
     */
    public $values;
}
