<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignerGlobalItemPrice
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $channel;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $amountFrom;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $price;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $priceNet;
}
