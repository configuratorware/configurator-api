<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option as OptionEntity;

final class MediaInfoOption
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $translated_title;

    /**
     * @var MediaInfoOptionImages
     */
    public $images;

    /**
     * @param int $id
     * @param string $identifier
     * @param string $translated_title
     * @param MediaInfoOptionImages $optionImage
     */
    private function __construct(int $id, string $identifier, string $translated_title, MediaInfoOptionImages $optionImage)
    {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->translated_title = $translated_title;
        $this->images = $optionImage;
    }

    /**
     * @param OptionEntity $option
     * @param MediaInfoOptionImages $optionImage
     *
     * @return static
     */
    public static function from(OptionEntity $option, MediaInfoOptionImages $optionImage): self
    {
        return new self((int)$option->getId(), $option->getIdentifier(), $option->getTranslatedTitle(), $optionImage);
    }
}
