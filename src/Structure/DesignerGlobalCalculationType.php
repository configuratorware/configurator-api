<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeEntity;
use Symfony\Component\Validator\Constraints as Assert;

class DesignerGlobalCalculationType
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $itemAmountDependent;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $pricePerItem;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $selectableByUser;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $imageDataDependent;

    /**
     * @Serializer\Type("string")
     *
     * @var string
     * @Assert\Choice(choices=DesignerGlobalCalculationTypeEntity::IMAGE__DEPENDENT_CONSTS)
     */
    public $imageDependent;

    /**
     * @var array<DesignerGlobalCalculationTypeText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationTypeText>")
     */
    public $texts;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationTypePrice>")
     */
    public $prices;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $bulkNameDependent;
}
