<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Attributevaluetranslation.
 */
class Attributevaluetranslation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Regex("/[a-z]{2}_[A-Z]{2}/")
     * @Serializer\Groups({"administrationarea"})
     */
    public $language;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $translation;
}
