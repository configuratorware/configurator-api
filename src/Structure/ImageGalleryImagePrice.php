<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ImageGalleryImagePrice
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public ?int $id;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public float $price;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public float $priceNet;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $channel;
}
