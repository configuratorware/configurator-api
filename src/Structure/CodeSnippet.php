<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Codesnippet.
 */
class CodeSnippet
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $code;

    /**
     * @var Channel|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Channel")
     */
    public $channel;

    /**
     * @var Client|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Client")
     */
    public $client;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Locale
     */
    public $language;
}
