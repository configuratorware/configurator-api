<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

class DesignDataTransfer
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sourceItemId;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $targetItemId;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $designAreas;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $visualizationData;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $itemStatus;
}
