<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ClientText
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $termsAndConditionsLink;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $dataPrivacyLink;

    /**
     * @var Channel
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Channel")
     */
    public $channel;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Regex("/[a-z]{2}_[A-Z]{2}/")
     * @Assert\NotBlank()
     */
    public $language;
}
