<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

class DataCheck
{
    public const TYPE_NEUTRAL = 'neutral';
    public const TYPE_NOTICE = 'notice';
    public const TYPE_ERROR = 'error';

    /**
     * @var string
     */
    public $element;

    /**
     * @var string
     */
    public $type;

    /**
     * @var array<DataCheckPlaceholder>
     */
    public $placeholders;

    /**
     * @var array<self>
     */
    public $children;

    /**
     * @param string $element
     * @param string $type
     * @param array $placeholders
     * @param array $children
     */
    public function __construct(string $element, string $type, array $placeholders = [], array $children = [])
    {
        $this->element = $element;
        $this->type = $type;
        $this->placeholders = $placeholders;
        $this->children = $children;
    }
}
