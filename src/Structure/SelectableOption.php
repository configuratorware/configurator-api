<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class SelectableOption
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $amountIsSelectable;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $thumbnail;

    /**
     * @var array<SelectableOptionDeltaPrice>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOptionDeltaPrice>")
     */
    public $deltaPrices;

    public function __construct(int $id = 0, string $identifier = null, int $sequenceNumber = null, ?string $title = null, bool $amountIsSelectable = false, ?string $thumbnail = null, array $deltaPrices = [])
    {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->sequenceNumber = $sequenceNumber;
        $this->title = $title;
        $this->amountIsSelectable = $amountIsSelectable;
        $this->thumbnail = $thumbnail;
        $this->deltaPrices = $deltaPrices;
    }
}
