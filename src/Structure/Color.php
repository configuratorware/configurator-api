<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Color.
 */
class Color
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $hex_value;

    /**
     * @var array<ColorText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ColorText>")
     */
    public $texts;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequence_number;
}
