<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Rules;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class CheckResult.
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 *
 * @version 1.0
 */
class CheckResultOptionInformation
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $optionclassification_identifiers;

    /**
     * thumbnail image.
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public $thumbnail;
}
