<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Rules;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class CheckResult.
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 *
 * @version 1.0
 */
class CheckResult
{
    /**
     * true for passed check, false for failed check.
     *
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $status;

    /**
     * identifier of the rule type that has been checked.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $rule_type;

    /**
     * option that was checked, may be empty on global checks.
     *
     * @var CheckResultOptionInformation|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CheckResultOptionInformation")
     */
    public $checked_option;

    /**
     * array of options that caused the check to fail.
     *
     * @var array<CheckResultOptionInformation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CheckResultOptionInformation>")
     */
    public $conflicting_options;

    /**
     * error message or notice in the current language
     * rulefeedbacktexttranslation with filled placeholders.
     *
     * may be empty if the check is passed
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $message;

    /**
     * additional information, e.g. list of alternative items, ...
     *
     * @var mixed
     */
    public $data;
}
