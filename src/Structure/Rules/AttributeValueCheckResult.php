<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Rules;

class AttributeValueCheckResult
{
    public string $attributeIdentifier = '';

    public string $attributeTitle = '';

    public bool $hasCommonValues = false;
    /**
     * @var array|string[]
     */
    public array $conflictingOptionIdentifiers = [];
}
