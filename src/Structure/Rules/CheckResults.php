<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Rules;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class CheckResult.
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 *
 * @version 1.0
 */
class CheckResults
{
    /**
     * true if all checks pass, false if one or more of the checks fail.
     *
     * @Serializer\Type("boolean")
     */
    public $status;

    /**
     * a list of CheckResult objects.
     *
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult>")
     */
    public $check_results;

    /**
     * updated Configuration object, after executing checks and their actions (e.g. options could have been changed).
     *
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration")
     */
    public $configuration;
}
