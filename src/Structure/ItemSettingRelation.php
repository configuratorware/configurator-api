<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

/**
 * @deprecated  use \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemSettingRelation instead
 */
class ItemSettingRelation extends \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemSettingRelation
{
}
