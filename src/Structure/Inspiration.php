<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Inspiration
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var bool
     */
    public $active;

    /**
     * @var string|null
     */
    public $configurationCode;

    /**
     * @var array<InspirationText>|null
     * @Assert\Valid
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\InspirationText>")
     */
    public $texts;

    /**
     * @var string|null
     */
    public $thumbnail;

    /**
     * @var int|null
     * @Assert\NotBlank
     */
    public $itemId;

    /**
     * @param int|null $id
     * @param bool $active
     * @param string|null $configurationCode
     * @param InspirationText[] $texts
     * @param string|null $thumbnail
     * @param int|null $itemId
     */
    public function __construct(int $id = null, bool $active = false, string $configurationCode = null, array $texts = null, string $thumbnail = null, int $itemId = null)
    {
        $this->id = $id;
        $this->active = $active;
        $this->configurationCode = $configurationCode;
        $this->texts = $texts;
        $this->thumbnail = $thumbnail;
        $this->itemId = $itemId;
    }
}
