<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Itemattribute.
 */
class Stock
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $status;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $deliverytime;

    /**
     * @var Channel
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $channel;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $amount;
}
