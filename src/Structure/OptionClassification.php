<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Optionclassification.
 */
class OptionClassification
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\Groups({"frontend"})
     */
    public $id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"frontend"})
     */
    public $parent_id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $sequencenumber;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $hiddenInFrontend;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $hiddenInPdf;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $translated_title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $translated_description;

    /**
     * @var array<OptionClassificationText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassificationText>")
     * @Serializer\Groups({"administrationarea"})
     * @Assert\Valid
     */
    public $texts;

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemattribute>")
     * @Serializer\Groups({"administrationarea"})
     * @Assert\Valid
     */
    public $attributes;

    /**
     * @var array<Option>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Option>")
     * @Serializer\Groups({"administrationarea"})
     */
    public $selectedoptions;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"administrationarea", "frontend"})
     */
    public $is_multiselect;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"administrationarea", "frontend"})
     */
    public $is_mandatory;
}
