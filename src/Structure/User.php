<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class User
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $username;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $firstname;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $lastname;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $email;

    /**
     * @Serializer\Type("string")
     */
    public $password;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $active;

    /**
     * @var array<Role>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Role>")
     */
    public $roles;
}
