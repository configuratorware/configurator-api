<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Currency.
 */
class Currency
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $iso;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $symbol;
}
