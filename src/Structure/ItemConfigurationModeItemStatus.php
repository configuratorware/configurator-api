<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Mode.
 */
class ItemConfigurationModeItemStatus
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var ItemRelation
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\ItemRelation")
     */
    public $item;

    /**
     * @var ItemStatus
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus")
     */
    public $itemStatus;
}
