<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class UploadDesignViewDetailImageForGroupEntry
{
    /**
     * @var int
     */
    public $designViewId;

    /**
     * @var int
     */
    public $groupEntryId;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $realPath;

    /**
     * @param int $designViewId
     * @param int $groupEntryId
     * @param string $filename
     * @param string $realPath
     */
    public function __construct(int $designViewId, int $groupEntryId, string $filename, string $realPath)
    {
        $this->designViewId = $designViewId;
        $this->groupEntryId = $groupEntryId;
        $this->filename = $filename;
        $this->realPath = $realPath;
    }
}
