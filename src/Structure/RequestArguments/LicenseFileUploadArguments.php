<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class LicenseFileUploadArguments
{
    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public $uploadedFile;
}
