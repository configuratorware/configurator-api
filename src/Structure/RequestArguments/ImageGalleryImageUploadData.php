<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageGalleryImageUploadData
{
    /**
     * @var int
     */
    public $imageGalleryImageId;

    /**
     * @var UploadedFile
     */
    public $galleryImageFormData;

    /**
     * @var UploadedFile
     */
    public $printImageFormData;
}
