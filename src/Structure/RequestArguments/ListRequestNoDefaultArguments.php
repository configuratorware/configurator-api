<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class ListRequestNoDefaultArguments extends ListRequestArguments
{
    public const DEFAULT_FILTERS = [];
    public const DEFAULT_OFFSET = 0;
    public const DEFAULT_LIMIT = null;
    public const DEFAULT_ORDERDIR = null;

    /**
     * @var int
     */
    public $limit = null;

    /**
     * @var string
     */
    public $orderDir = null;
}
