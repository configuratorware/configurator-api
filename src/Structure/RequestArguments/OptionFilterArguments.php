<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class OptionFilterArguments
{
    /**
     * @var array
     */
    public $filters;

    /**
     * @var string
     */
    public $queryTitle;
}
