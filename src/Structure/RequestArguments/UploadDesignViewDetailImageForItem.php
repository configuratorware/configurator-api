<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class UploadDesignViewDetailImageForItem
{
    /**
     * @var int
     */
    public $designViewId;

    /**
     * @var int
     */
    public $itemId;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $realPath;

    /**
     * UploadDesignViewImageForItem constructor.
     *
     * @param int $designViewId
     * @param int $itemId
     * @param string $filename
     * @param string $realPath
     */
    public function __construct(int $designViewId, int $itemId, string $filename, string $realPath)
    {
        $this->designViewId = $designViewId;
        $this->itemId = $itemId;
        $this->filename = $filename;
        $this->realPath = $realPath;
    }
}
