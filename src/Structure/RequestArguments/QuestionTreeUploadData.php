<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class QuestionTreeUploadData
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $questionTreeId;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $languageId;

    /**
     * @var UploadedFile
     */
    public $dataFileFormData;
}
