<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class ImageGalleryImageFrontendList
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $tags;
}
