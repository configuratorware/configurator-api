<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;

class SequenceNumberArguments
{
    /**
     * @var array<SequenceNumberData>
     */
    public $data;
}
