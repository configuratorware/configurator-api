<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class UploadLayerImage
{
    /**
     * @var int
     */
    public $itemId;

    /**
     * @var int
     */
    public $componentId;

    /**
     * @var int
     */
    public $optionId;

    /**
     * @var string
     */
    public $creatorViewId;

    /**
     * @var string
     */
    public $fileName;

    /**
     * @var string
     */
    public $realPath;

    /**
     * @param int $itemId
     * @param int $componentId
     * @param int $optionId
     * @param string $creatorViewId
     * @param string $fileName
     * @param string $realPath
     */
    private function __construct(int $itemId, int $componentId, int $optionId, string $creatorViewId, string $fileName, string $realPath)
    {
        $this->itemId = $itemId;
        $this->componentId = $componentId;
        $this->optionId = $optionId;
        $this->creatorViewId = $creatorViewId;
        $this->fileName = $fileName;
        $this->realPath = $realPath;
    }

    /**
     * @param int $itemId
     * @param int $componentId
     * @param int $optionId
     * @param string $creatorViewId
     * @param string $fileName
     * @param string $realPath
     *
     * @return static
     */
    public static function from(int $itemId, int $componentId, int $optionId, string $creatorViewId, string $fileName, string $realPath): self
    {
        return new self($itemId, $componentId, $optionId, $creatorViewId, $fileName, $realPath);
    }
}
