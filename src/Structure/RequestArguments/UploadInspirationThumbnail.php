<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class UploadInspirationThumbnail
{
    /**
     * @var int
     */
    public $inspirationId;

    /**
     * @var string
     */
    public $filename;

    /**
     * @var string
     */
    public $realPath;

    /**
     * @param int $inspirationId
     * @param string $filename
     * @param string $realPath
     */
    private function __construct(int $inspirationId, string $filename, string $realPath)
    {
        $this->inspirationId = $inspirationId;
        $this->filename = $filename;
        $this->realPath = $realPath;
    }

    /**
     * @param int $inspirationId
     * @param string $filename
     * @param string $realPath
     *
     * @return static
     */
    public static function from(int $inspirationId, string $filename, string $realPath): self
    {
        return new self($inspirationId, $filename, $realPath);
    }
}
