<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class ConnectorImportArguments
{
    /**
     * @var \Redhotmagma\ConfiguratorApiBundle\Models\ImportOptions
     */
    public $importOptions;

    /**
     * @var string
     */
    public $importBody;
}
