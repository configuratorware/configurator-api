<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class MediaArguments
{
    /**
     * @var string
     */
    public $path;

    /**
     * @var int
     */
    public $scale;

    /**
     * @var bool
     */
    public $mobile;
}
