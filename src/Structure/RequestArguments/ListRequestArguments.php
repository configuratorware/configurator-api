<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class ListRequestArguments
{
    public const DEFAULT_FILTERS = [];
    public const DEFAULT_OFFSET = 0;
    public const DEFAULT_LIMIT = 10;
    public const DEFAULT_ORDERDIR = 'ASC';

    /**
     * @var string
     */
    public $query;

    /**
     * @var array
     */
    public $filters = self::DEFAULT_FILTERS;

    /**
     * @var int
     */
    public $offset = self::DEFAULT_OFFSET;

    /**
     * @var int
     */
    public $limit = self::DEFAULT_LIMIT;

    /**
     * @var string
     */
    public $orderBy;

    /**
     * @var string
     */
    public $orderDir = self::DEFAULT_ORDERDIR;

    public function toArray()
    {
        $arguments = [];

        $arguments['query'] = $this->query;
        $arguments['filters'] = $this->filters;
        $arguments['offset'] = $this->offset;
        $arguments['limit'] = $this->limit;
        $arguments['orderby'] = $this->orderBy;
        $arguments['orderdir'] = $this->orderDir;

        return $arguments;
    }
}
