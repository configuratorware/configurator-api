<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class UploadComponentThumbnail
{
    /**
     * @var int
     */
    public $componentId;

    /**
     * @var string
     */
    public $fileName;

    /**
     * @var string
     */
    public $realPath;

    /**
     * @param int $componentId
     * @param string $fileName
     * @param string $realPath
     */
    private function __construct(int $componentId, string $fileName, string $realPath)
    {
        $this->componentId = $componentId;
        $this->fileName = $fileName;
        $this->realPath = $realPath;
    }

    /**
     * @param int $componentId
     * @param string $fileName
     * @param string $realPath
     *
     * @return static
     */
    public static function from(int $componentId, string $fileName, string $realPath): self
    {
        return new self($componentId, $fileName, $realPath);
    }
}
