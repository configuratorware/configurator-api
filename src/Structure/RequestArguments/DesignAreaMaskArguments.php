<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

class DesignAreaMaskArguments
{
    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $designAreaId;

    /**
     * @var string|null
     */
    public $designProductionMethodId;

    /**
     * @var UploadedFile
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="Symfony\Component\HttpFoundation\File\UploadedFile",
     *     message="The upload does not contain any file."
     * )
     */
    public $uploadedFile;
}
