<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class ClientLogoUploadArguments
{
    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public $uploadedFile;
}
