<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use Closure;

class ControlParameters
{
    public const ADMIN_MODE = '_admin_mode';
    public const CHANNEL = '_channel';
    public const CLIENT = '_client';
    public const HIDE_PRICES = '_hide_prices';
    public const LANGUAGE = '_language';
    public const SHARE_URL = '_share_url';

    /**
     * @var array
     */
    private $getParameterBag;

    /**
     * @var bool
     */
    private $isAdminMode;

    /**
     * @param array|null $getParameterBag
     * @param bool $isAdminMode
     */
    public function __construct(array $getParameterBag = [], bool $isAdminMode = false)
    {
        $this->getParameterBag = $getParameterBag;
        $this->isAdminMode = $isAdminMode;
    }

    /**
     * @param string $parameterName
     * @param string|null $default
     *
     * @return string|null
     * @psalm-return ($default is string ? string : string|null)
     */
    public function getParameter(string $parameterName, string $default = null): ?string
    {
        if (isset($this->getParameterBag[$parameterName])) {
            return (string)$this->getParameterBag[$parameterName];
        }

        return $default;
    }

    /**
     * @return bool
     */
    public function isAdminMode(): bool
    {
        return $this->isAdminMode;
    }

    /**
     * @param string $baseUrl
     *
     * @return string
     */
    public function mergeGetParametersToUrl(string $baseUrl): string
    {
        if ([] === $this->getParameterBag) {
            return $baseUrl;
        }

        if (false === strpos($baseUrl = rtrim($baseUrl, '?'), '?')) {
            return $baseUrl . '?' . $this->toUrlPart();
        }

        // only not existent parameters will be added
        foreach ($this->getParameterBag as $key => $param) {
            if (false === strpos($baseUrl, $key) && 0 === strpos($key, '_')) {
                $baseUrl .= '&' . $key . '=' . $param;
            }
        }

        return $baseUrl;
    }

    /**
     * @return string
     */
    private function toUrlPart(): string
    {
        $urlPart = '';

        foreach ($this->getParameterBag as $key => $param) {
            if (0 === strpos($key, '_')) {
                $urlPart .= $key . '=' . $param . '&';
            }
        }

        return rtrim($urlPart, '&');
    }

    /**
     * @param string $baseUrl
     *
     * @return string
     *
     * @deprecated use mergeToUrl instead
     */
    public function merge(string $baseUrl): string
    {
        return $this->mergeGetParametersToUrl($baseUrl);
    }

    /**
     * Creates a new ControlParameters object with filtered parameters, according to
     * sent closure. Original object won't be changed.
     *
     * The Closure function should return a boolean:
     * * true - when the parameter needs to be kept
     * * false - when the parameter needs to be removed
     *
     * Also, the Closure should consist of one string parameter, which is the control parameter name. Example:
     * ```php
     * $controlParameters = new ControlParameters([
     *    '_keep_this' => 'will be kept',
     *    '_remove_this' => 'will be removed',
     * ]);
     * $fn = function (string $parameterName) {
     *     return $parameterName === '_keep_this';
     * }
     * $clonedParameters = $controlParameters->filter($fn);
     * ```
     *
     * @param Closure $filterFunction
     *
     * @return ControlParameters
     */
    public function filter(Closure $filterFunction): ControlParameters
    {
        $bag = [];

        foreach ($this->getParameterBag as $key => $param) {
            if ($filterFunction($key)) {
                $bag[$key] = $param;
            }
        }

        return new ControlParameters($bag, $this->isAdminMode);
    }

    /**
     * @return array
     */
    public function getAllParameters(): array
    {
        return $this->getParameterBag;
    }
}
