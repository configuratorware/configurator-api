<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

final class ConfigurationLoadArguments
{
    public array $switchedOptions;

    public bool $noCheck;

    private function __construct(array $switchedOptions = [], bool $noCheck = false)
    {
        $this->switchedOptions = $switchedOptions;
        $this->noCheck = $noCheck;
    }

    public static function from(array $switchedOptions = [], bool $noCheck = false): self
    {
        return new self($switchedOptions, $noCheck);
    }
}
