<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class UploadCreatorViewThumbnail
{
    /**
     * @var int
     */
    public $viewId;

    /**
     * @var string
     */
    public $fileName;

    /**
     * @var string
     */
    public $realPath;

    /**
     * @param int $viewId
     * @param string $fileName
     * @param string $realPath
     */
    private function __construct(int $viewId, string $fileName, string $realPath)
    {
        $this->viewId = $viewId;
        $this->fileName = $fileName;
        $this->realPath = $realPath;
    }

    /**
     * @param int $viewId
     * @param string $fileName
     * @param string $realPath
     *
     * @return static
     */
    public static function from(int $viewId, string $fileName, string $realPath): self
    {
        return new self($viewId, $fileName, $realPath);
    }
}
