<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use Doctrine\ORM\QueryBuilder;

class ItemClassificationFilter
{
    /**
     * @var int[]
     */
    private array $itemClassificationsIds = [];

    /**
     * @param int[] $itemClassificationsIds
     */
    public function setItemClassificationsIds(array $itemClassificationsIds): self
    {
        $this->itemClassificationsIds = $itemClassificationsIds;

        return $this;
    }

    public function containFilterValues(): bool
    {
        return count($this->itemClassificationsIds) > 0;
    }

    public function applyTo(QueryBuilder $qb): void
    {
        $qb->leftJoin('item.item_itemclassification', 'item_itemclassification')
            ->leftJoin('item_itemclassification.itemclassification', 'itemclassification')
            ->andWhere($qb->expr()->in('itemclassification.id', ':itemClassificationsIds'))
            ->setParameter('itemClassificationsIds', $this->itemClassificationsIds);
    }
}
