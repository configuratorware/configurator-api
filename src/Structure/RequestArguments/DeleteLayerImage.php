<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class DeleteLayerImage
{
    /**
     * @var int
     */
    public $itemId;

    /**
     * @var int
     */
    public $componentId;

    /**
     * @var int
     */
    public $optionId;

    /**
     * @var string
     */
    public $creatorViewId;

    /**
     * @param int $itemId
     * @param int $componentId
     * @param int $optionId
     * @param string $creatorViewId
     */
    private function __construct(int $itemId, int $componentId, int $optionId, string $creatorViewId)
    {
        $this->itemId = $itemId;
        $this->componentId = $componentId;
        $this->optionId = $optionId;
        $this->creatorViewId = $creatorViewId;
    }

    /**
     * @param int $itemId
     * @param int $componentId
     * @param int $optionId
     * @param string $creatorViewId
     *
     * @return static
     */
    public static function from(int $itemId, int $componentId, int $optionId, string $creatorViewId): self
    {
        return new self($itemId, $componentId, $optionId, $creatorViewId);
    }
}
