<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class FrontendTranslationFileUploadArguments
{
    /**
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    public $uploadedFile;
}
