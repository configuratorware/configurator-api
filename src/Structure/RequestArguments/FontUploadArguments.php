<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FontUploadArguments
{
    /**
     * @var UploadedFile
     */
    public $uploadedFile;
}
