<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class FrontendClientArguments
{
    /**
     * @var string
     */
    public $client;

    /**
     * @var string
     */
    public $channel;
}
