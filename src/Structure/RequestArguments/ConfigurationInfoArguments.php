<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class ConfigurationInfoArguments
{
    public bool $extendedData = false;
}
