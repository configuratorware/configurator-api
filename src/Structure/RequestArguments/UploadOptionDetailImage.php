<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @internal
 */
final class UploadOptionDetailImage
{
    /**
     * @var int
     */
    public $optionId;

    /**
     * @var string
     */
    public $fileName;

    /**
     * @var string
     */
    public $realPath;

    /**
     * @param int $optionId
     * @param string $fileName
     * @param string $realPath
     */
    private function __construct(int $optionId, string $fileName, string $realPath)
    {
        $this->optionId = $optionId;
        $this->fileName = $fileName;
        $this->realPath = $realPath;
    }

    /**
     * @param int $optionId
     * @param string $fileName
     * @param string $realPath
     *
     * @return static
     */
    public static function from(int $optionId, string $fileName, string $realPath): self
    {
        return new self($optionId, $fileName, $realPath);
    }
}
