<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

class CommaSeparatedList
{
    /**
     * @var array<string>
     */
    public $elements;
}
