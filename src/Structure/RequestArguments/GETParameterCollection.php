<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments;

/**
 * @deprecated use ControlParameters instead
 */
class GETParameterCollection extends ControlParameters
{
}
