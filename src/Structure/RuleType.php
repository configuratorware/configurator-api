<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role.
 */
class RuleType
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_title;

    /**
     * @var object
     * @Serializer\Type("object")
     * @Assert\NotBlank()
     */
    public $availablerulefields;
}
