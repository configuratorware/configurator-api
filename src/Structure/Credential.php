<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Credential.
 */
class Credential
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $area;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $identifier;
}
