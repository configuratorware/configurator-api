<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Tagtranslation.
 */
class Tagtranslation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $translation;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Regex("/[a-z]{2}_[A-Z]{2}/")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $language;
}
