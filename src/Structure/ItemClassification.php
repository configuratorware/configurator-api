<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ItemClassification
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"frontend"})
     */
    public $id;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\Groups({"frontend"})
     */
    public $parent_id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $sequencenumber;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $translated_title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $translated_description;

    /**
     * @var array<Itemclassificationtext>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemclassificationtext>")
     * @Serializer\Groups({"administrationarea"})
     * @Assert\Valid
     */
    public $texts;

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Itemattribute>")
     * @Serializer\Groups({"administrationarea"})
     * @Assert\Valid
     */
    public $attributes;

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public $minimumOrderAmount;
}
