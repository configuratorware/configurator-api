<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

class ConfigurationSaveData
{
    /**
     * @var Configuration
     */
    public $configuration;

    /**
     * @var string
     */
    public $configurationType;

    /**
     * @var array
     */
    public $screenshots;

    /**
     * @var array
     */
    public $designDataSnapshots;

    /**
     * @var array
     */
    public $printDesignData;

    /**
     * @var string
     */
    public $thumbnail;

    /**
     * @var array
     */
    public $printFiles;
}
