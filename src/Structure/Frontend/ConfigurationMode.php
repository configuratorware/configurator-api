<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Mode.
 */
class ConfigurationMode
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var ItemStatus
     * @Serializer\Type("ItemStatus")
     */
    public $itemStatus;
}
