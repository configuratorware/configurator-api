<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

/**
 * Class MediaItem.
 *
 * @author Daniel Klier <klier@redhotmagma.de>
 */
class MediaItem
{
    /**
     * @var string
     */
    public $relativePathname;

    /**
     * @var string
     */
    public $contentType;

    /**
     * @var string
     */
    public $eTag;

    /**
     * @var resource
     */
    public $fileStream;
}
