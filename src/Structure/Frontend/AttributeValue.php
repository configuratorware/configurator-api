<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Attributevalue.
 */
class AttributeValue
{
    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $value;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $translated_value;
}
