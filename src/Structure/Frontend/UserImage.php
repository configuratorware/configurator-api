<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class UserImage.
 */
class UserImage
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $original;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $preview;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $thumb;
}
