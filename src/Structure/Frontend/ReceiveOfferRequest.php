<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ReceiveOfferRequest
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $configurationCode = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public string $emailAddress = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $name = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $company = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $phoneNumber = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $zip = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $city = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $street = '';

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public string $country = '';

    /**
     * @var \stdClass
     * @Serializer\Type("stdClass")
     */
    public \stdClass $additional;

    public function __construct()
    {
        $this->additional = new \stdClass();
    }
}
