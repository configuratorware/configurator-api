<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class DesignerData.
 */
class DesignerData
{
    /**
     * @var array<DesignView>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignView>")
     */
    public $designViews;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $uploadMaxSize;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $maxZoom2d;

    /**
     * @param DesignView[] $designViews
     * @param int $uploadMaxSize
     * @param float $maxZoom2d
     */
    public function __construct(array $designViews, int $uploadMaxSize, float $maxZoom2d)
    {
        $this->designViews = $designViews;
        $this->uploadMaxSize = $uploadMaxSize;
        $this->maxZoom2d = $maxZoom2d;
    }
}
