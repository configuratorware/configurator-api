<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class UserImage.
 */
class DesignerUserImageResponseImage
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $url;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $format;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $mimeType;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"frontend"})
     */
    public $width;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"frontend"})
     */
    public $height;

    /**
     * @var \StdClass
     */
    public $additionalData;
}
