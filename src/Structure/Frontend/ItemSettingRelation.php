<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class ItemSettingRelation
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $visualizationCreator;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $visualizationDesigner;
}
