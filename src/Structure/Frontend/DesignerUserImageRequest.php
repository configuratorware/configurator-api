<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignerUserImageRequest
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Type(
     *     type="string",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    public $fileName;

    /**
     * @var array<DesignerUserImageRequestOperation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequestOperation>")
     */
    public $operations;

    /**
     * @Serializer\Type("Symfony\Component\HttpFoundation\File\UploadedFile")
     */
    public $uploadedFile;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $createCopy;
}
