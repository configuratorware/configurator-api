<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class Itemclassification
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $description;

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemattribute>")
     * @Serializer\Groups({"frontend"})
     */
    public $attributes;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $thumbnail;

    /**
     * @var int|null
     * @Serializer\Type("int")
     */
    public $minimumOrderAmount;
}
