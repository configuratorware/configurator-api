<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Configuration.
 */
class ConfigurationInfo
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $code;

    /**
     * Reference to the parent code, provided by the frontend on explicit occasions
     * (?_addparentcodeonsave=true).
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $parentCode;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $name;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $item;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $itemIdentifier;

    /**
     * @var CalculationResult|null
     * @Serializer\Type("float")
     */
    public $calculation;

    /**
     * Below are selected options ->selectedoptions.
     *
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $optionclassifications;

    /**
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $images;

    /**
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $printImages;

    /**
     * @var object
     * @Serializer\Type("object")
     */
    public $customdata;

    /**
     * @var array<DesignDataInfo|mixed>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignDataInfo>")
     */
    public $designData;

    /**
     * @var DateTime
     * @Serializer\Type("DateTime")
     */
    public $date_created;

    /**
     * @Serializer\Type("object")
     */
    public $finderResult;
}
