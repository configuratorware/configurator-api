<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class QuestionTree
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * Title from QuestionTreeText for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * Freemind Data converted to JSON, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $data;
}
