<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class DesignView.
 */
class DesignView
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * @var array<mixed>|null
     * @Serializer\Type("array")
     */
    public $images;

    /**
     * @var array<mixed>|null
     * @Serializer\Type("array")
     */
    public $thumbnails;

    /**
     * @var array<DesignArea>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignArea")
     */
    public $designAreas;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customData;
}
