<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Language.
 */
class Language
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $iso;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $dateFormat;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $dateTimeFormat;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $decimalpoint;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $priceDecimals;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $numberDecimals;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $thousandsSeparator;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $currencySymbolPosition;
}
