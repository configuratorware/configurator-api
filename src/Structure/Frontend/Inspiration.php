<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

class Inspiration
{
    /**
     * @var string|null
     */
    public $configurationCode;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var string|null
     */
    public $thumbnail;

    /**
     * @var string|null
     */
    public $previewImage;

    /**
     * @param string|null $configurationCode
     * @param string|null $title
     * @param string|null $description
     * @param string|null $thumbnail
     * @param string|null $previewImage
     */
    public function __construct(?string $configurationCode = null, ?string $title = null, ?string $description = null, ?string $thumbnail = null, ?string $previewImage = null)
    {
        $this->configurationCode = $configurationCode;
        $this->title = $title;
        $this->description = $description;
        $this->thumbnail = $thumbnail;
        $this->previewImage = $previewImage;
    }
}
