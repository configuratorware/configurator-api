<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class Option
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * title from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * abstract from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $abstract;

    /**
     * description from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $description;

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemattribute>")
     */
    public $attributes;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public $price;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public $netPrice;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $priceFormatted;

    /**
     * @var Stock
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Stock")
     */
    public $stock;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $amount;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $amount_is_selectable;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $detailImage;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $thumbnail;

    /**
     * @var bool|null
     * @Serializer\Type("boolean")
     */
    public $hasTextinput;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $inputValidationType;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $inputText;
}
