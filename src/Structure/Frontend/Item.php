<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class Item
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $parentItemIdentifier;

    /**
     * @var ItemSettingRelation
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\ItemSetting")
     */
    public $settings;

    /**
     * @var array<ConfigurationMode>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationMode>")
     */
    public $configurationModes;

    /**
     * title from Itemtext for current language, read-only.
     *
     * @var string|null
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * abstract from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $abstract;

    /**
     * description from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $description;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $detailImage;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $thumbnail;

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemattribute")
     */
    public $attributes;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public $price;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public $netPrice;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Stock")
     */
    public $stock;

    /**
     * @var int
     * @Serializer\Type("int")
     */
    public $minimumOrderAmount;

    /**
     * @Serializer\Type("stdClass")
     */
    public $itemGroup;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $callToAction;

    /**
     * @var array<Itemclassification>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemclassification>")
     */
    public array $itemclassifications;
}
