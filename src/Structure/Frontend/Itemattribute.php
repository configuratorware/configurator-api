<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class Itemattribute
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_title;

    /**
     * @var array<Attributetext>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Attributetext>")
     */
    public $texts;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $attributedatatype;

    /**
     * @var array<AttributeValue>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\AttributeValue>")
     */
    public $values;
}
