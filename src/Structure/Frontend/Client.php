<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class Client
{
    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $channel;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $hidePrices;

    /**
     * @var ClientText|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ClientText")
     */
    public $texts;

    /**
     * @var object
     * @Serializer\Type("object")
     */
    public $theme;
}
