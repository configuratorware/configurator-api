<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class CalculationResult.
 */
class CalculationResult
{
    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $total = 0;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $totalFormatted = '';

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $netTotal = 0;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $netTotalFormatted = '';

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $netTotalFromGrossTotal = 0;
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $netTotalFromGrossTotalFormatted = '';

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $vatRate = 0;

    /**
     * @var array<CalculationResultPosition>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResultPosition")
     */
    public $positions = [];

    /**
     * @var array<BulkSavingsMessage>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\BulkSavingsMessage>")
     */
    public $bulkSavings = [];
}
