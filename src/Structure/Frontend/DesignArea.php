<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class DesignArea.
 */
class DesignArea
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $defaultViewIdentifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $mask;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $baseShape;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $position;

    /**
     * @var array<DesignProductionMethod>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod>")
     */
    public $designProductionMethods;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customData;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $height;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $width;
}
