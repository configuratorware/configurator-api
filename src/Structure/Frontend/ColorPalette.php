<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ColorPalette.
 */
class ColorPalette
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $title;

    /**
     * @var Color
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Color")
     */
    public $defaultColor;

    /**
     * @var array<Color>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Color")
     * @Serializer\Groups({"frontend"})
     */
    public $colors;
}
