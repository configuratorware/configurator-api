<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

class DesignDataInfo
{
    /**
     * @var string
     */
    public $designAreaIdentifier;

    /**
     * @var string
     */
    public $designProductionMethodIdentifier;

    /**
     * @var int
     */
    public $colorAmount;

    /**
     * @var array
     */
    public $images = [];

    /**
     * @var array
     */
    public $texts = [];
}
