<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Codesnippet.
 */
class Codesnippet
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $code;
}
