<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class ConfigurationSwitchOptionData
{
    /**
     * @var Configuration
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration")
     */
    public $configuration;

    /**
     * @var array
     */
    public $switchedOptions;

    /**
     * @var bool
     */
    public $noCheck;

    public static function from(Configuration $configuration, array $switchedOptions, bool $noCheck): self
    {
        $switchOptionData = new self();
        $switchOptionData->configuration = $configuration;
        $switchOptionData->switchedOptions = $switchedOptions;
        $switchOptionData->noCheck = $noCheck;

        return $switchOptionData;
    }
}
