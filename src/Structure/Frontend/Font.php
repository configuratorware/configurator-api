<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class Font
{
    public const FILENAME_PROPERTIES = ['fileNameRegular', 'fileNameBold', 'fileNameItalic', 'fileNameBoldItalic'];

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $name;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $isDefault;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameRegular;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameBold;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameItalic;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameBoldItalic;
}
