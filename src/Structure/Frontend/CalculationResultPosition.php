<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class CalculationResultPosition
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $calculationTypeIdentifier;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $itemAmountDependent;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $declareSeparately;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $selectableByUser;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $selected;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $price;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $priceFormatted;

    /**
     * @var float
     * @Serializer\Type("float")
     */
    public $priceNet;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $priceNetFormatted;
}
