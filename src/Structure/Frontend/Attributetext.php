<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class Attributetext
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $language;

    /**
     * @var string
     */
    public $title;
}
