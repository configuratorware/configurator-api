<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class ClientText
{
    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $termsAndConditionsLink;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $dataPrivacyLink;
}
