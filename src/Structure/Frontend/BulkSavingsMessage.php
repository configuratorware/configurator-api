<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

class BulkSavingsMessage
{
    /**
     * @var string
     */
    public $itemIdentifier;

    /**
     * @var string
     */
    public $itemTitle;

    /**
     * @var int
     */
    public $savingsBulkAmount;

    /**
     * @var int
     */
    public $savingAmount;
}
