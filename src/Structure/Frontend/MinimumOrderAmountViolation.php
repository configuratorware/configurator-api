<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

class MinimumOrderAmountViolation
{
    /**
     * @var string
     */
    public $itemIdentifier;

    /**
     * @var string
     */
    public $itemTitle;

    /**
     * @var int
     */
    public $selectedAmount;

    /**
     * @var int
     */
    public $minimumAmount;
}
