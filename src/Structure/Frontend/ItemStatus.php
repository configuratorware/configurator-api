<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ItemStatus.
 */
class ItemStatus
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $identifier;

    /**
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"frontend"})
     */
    public $item_available;
}
