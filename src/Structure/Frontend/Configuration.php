<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Configuration.
 */
class Configuration
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $code;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $shareUrl;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $name;

    /**
     * @var Item
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item")
     */
    public $item;

    /**
     * @var OptionClassification[]
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification>")
     */
    public $optionclassifications;

    /**
     * @Serializer\Type("object")
     */
    public $visualizationData;

    /**
     * @Serializer\Type("object")
     */
    public $designdata;

    /**
     * @Serializer\Type("object")
     */
    public $information;

    /**
     * @Serializer\Type("object")
     */
    public $customdata;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $partslisthash;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $previewImageURL;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $configurationType;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $client;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $channel;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $language;

    /**
     * @var CalculationResult|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult>")
     */
    public $calculationResult;

    /**
     * @var DateTime
     * @Serializer\Type("DateTime")
     */
    public $date_created;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $license;

    /**
     * @var string|null
     * @Serializer\Type("object")
     */
    public $finderResult;
}
