<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class DesignProductionMethod
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $mask;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $height;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $width;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $isDefault;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $options;

    /**
     * @var array<ColorPalette>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ColorPalette")
     */
    public $colorPalettes;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customData;

    /**
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $engravingBackgroundColors;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $allowBulkNames;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $defaultColors;

    /**
     * @var bool|null
     * @Serializer\Type("boolean")
     */
    public $designElementsLocked;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $maxImages;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $maxTexts;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $maxElements;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public bool $oneLineText;

    /**
     * @Serializer\Type("boolean")
     */
    public $vectorizedLogoMandatory;
}
