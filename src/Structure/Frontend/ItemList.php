<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class ItemList
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public string $identifier = '';

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public string $title = '';

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public string $abstract = '';

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public string $description = '';

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public string $detailImage = '';

    /**
     * @Serializer\Type("string")
     */
    public string $thumbnail = '';

    /**
     * @var array<Itemattribute>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemattribute>")
     */
    public array $attributes;

    /**
     * @var array<Itemclassification>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemclassification>")
     */
    public array $itemClassifications;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public ?float $price;

    /**
     * @var float|null
     * @Serializer\Type("float")
     */
    public ?float $netPrice;

    /**
     * @var string|null
     * @Serializer\Type("float")
     */
    public string $priceFormatted;

    /**
     * @var string|null
     * @Serializer\Type("float")
     */
    public string $netPriceFormatted;
}
