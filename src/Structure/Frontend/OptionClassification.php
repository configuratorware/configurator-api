<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class OptionClassification extends Itemclassification
{
    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"frontend"})
     */
    public $is_multiselect;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"frontend"})
     */
    public $is_mandatory;

    /**
     * @var int
     */
    public $sequenceNumber;

    /**
     * @var array<Option|mixed>|null
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option>")
     * @Serializer\Groups({"frontend"})
     */
    public $selectedoptions;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"frontend"})
     */
    public $optionsCount;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $hiddenInFrontend;

    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $hiddenInPdf;

    /**
     * @var CreatorView|null
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CreatorView")
     * @Serializer\Groups({"frontend"})
     */
    public $creatorView;

    /**
     * @var array<Option|mixed>|null
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option>")
     * @Serializer\Groups({"frontend"})
     */
    public $options;
}
