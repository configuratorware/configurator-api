<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

class ConfigurationPrintFileSaveData
{
    /**
     * @var array
     */
    public $printfiles;

    /**
     * @var string
     */
    public $code;
}
