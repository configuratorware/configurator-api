<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

class ImageGalleryImage
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public string $imageUrl;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public string $thumbUrl;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public string $printUrl;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public string $title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public string $description;

    /**
     * @Serializer\Type("float")
     * @Serializer\Groups({"frontend"})
     */
    public ?float $price;

    /**
     * @Serializer\Type("float")
     * @Serializer\Groups({"frontend"})
     */
    public ?float $netPrice;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public string $priceFormatted;
}
