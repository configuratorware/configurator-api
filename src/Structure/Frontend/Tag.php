<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Tag.
 */
class Tag
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $title;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"frontend"})
     */
    public $is_multiselect;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     * @Serializer\Groups({"frontend"})
     */
    public $is_background;
}
