<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

final class CreatorView
{
    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $sequenceNumber;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $visualizationKey;

    /**
     * @param string|null $identifier
     * @param string|null $sequenceNumber
     */
    public function __construct(?string $identifier = null, ?string $sequenceNumber = null)
    {
        $this->identifier = $identifier;
        $this->sequenceNumber = $sequenceNumber;
        if (null !== $identifier && null !== $sequenceNumber) {
            $this->visualizationKey = $sequenceNumber . '_' . $identifier;
        }
    }
}
