<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Itemattribute.
 */
class AttributeRelation
{
    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $identifier;

    /**
     * title from Attributetext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $attributedatatype;

    /**
     * @var array<AttributeValue>
     * @Serializer\Type("array")
     * @Serializer\Groups({"frontend"})
     */
    public $values;
}
