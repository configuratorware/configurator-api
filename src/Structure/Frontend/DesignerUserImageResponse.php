<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Frontend;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class UserImage.
 */
class DesignerUserImageResponse
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileName;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $originalFileName;

    /**
     * @var array<DesignerUserImageResponseImage>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageResponseImage>")
     */
    public $original;

    /**
     * @var array<DesignerUserImageResponseImage>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageResponseImage>")
     */
    public $preview;

    /**
     * @var array<DesignerUserImageResponseImage>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageResponseImage>")
     */
    public $thumbnail;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $message;
}
