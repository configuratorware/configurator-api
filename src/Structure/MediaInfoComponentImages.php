<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

final class MediaInfoComponentImages
{
    /**
     * @var MediaInfoImage
     */
    public $componentThumbnail;

    /**
     * @param MediaInfoImage $componentThumbnail
     */
    private function __construct(MediaInfoImage $componentThumbnail)
    {
        $this->componentThumbnail = $componentThumbnail;
    }

    public static function from(MediaInfoImage $componentThumbnail): self
    {
        return new self($componentThumbnail);
    }
}
