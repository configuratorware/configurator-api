<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Assemblypoint.
 */
class Assemblypoint
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $identifier;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $option_id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $assemblypointimageelement_id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $x;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $y;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $rotation;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $optionclassification;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $imageSequenceNumber;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $imageSuffix;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $imageDirectory;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $parentSequenceNumber;
}
