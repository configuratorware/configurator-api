<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class CreatorViewText
{
    /**
     * @var string|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Regex("/[a-z]{2}_[A-Z]{2}/")
     * @Assert\NotBlank()
     */
    public $language;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $title;

    /**
     * @param string|null $id
     * @param string|null $language
     * @param string|null $title
     *
     * @psalm-suppress PossiblyNullPropertyAssignmentValue
     */
    public function __construct(?string $id = null, string $language = null, string $title = null)
    {
        $this->id = $id;
        $this->language = $language;
        $this->title = $title;
    }
}
