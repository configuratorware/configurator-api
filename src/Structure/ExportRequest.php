<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

class ExportRequest
{
    private array $itemIdentifiers;
    private bool $combine;

    public function __construct(array $itemIdentifiers, bool $combine)
    {
        $this->itemIdentifiers = $itemIdentifiers;
        $this->combine = $combine;
    }

    public function getItemIdentifiers(): array
    {
        return $this->itemIdentifiers;
    }

    public function isCombine(): bool
    {
        return $this->combine;
    }
}
