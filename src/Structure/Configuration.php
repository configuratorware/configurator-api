<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use DateTime;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Configuration.
 */
class Configuration
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $code;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $name;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Item")
     * @Serializer\Groups({"administrationarea"})
     */
    public $item;

    /**
     * @var Client
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Client")
     */
    public $client;

    /**
     * Below are selected options ->selectedoptions.
     *
     * @var array<OptionClassification>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification>")
     * @Serializer\Groups({"administrationarea"})
     */
    public $optionclassifications;

    /**
     * URL of the preview image (= screenshot).
     *
     * @var string|null
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $previewImageURL;

    /**
     * Save type of the configuration (can be user, cart, designtemplate etc.).
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"frontend"})
     */
    public $configurationType;

    /**
     * @var object
     * @Serializer\Type("object")
     * @Serializer\Groups({"administrationarea"})
     */
    public $customdata;

    /**
     * @var DateTime
     * @Serializer\Type("DateTime")
     * @Serializer\Groups({"administrationarea"})
     */
    public $date_created;
}
