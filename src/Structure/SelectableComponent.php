<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class SelectableComponent
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var array<SelectableOption>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOption>")
     */
    public $selectableOptions;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $isMandatory;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $isMultiselect;

    /**
     * @var int|null
     */
    public $creatorViewId;

    public function __construct(
        int $id = 0,
        string $identifier = null,
        int $sequenceNumber = null,
        string $title = null,
        array $selectableOptions = [],
        bool $isMandatory = true,
        bool $isMultiselect = false,
        int $creatorViewId = null
    ) {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->sequenceNumber = $sequenceNumber;
        $this->title = $title;
        $this->selectableOptions = $selectableOptions;
        $this->isMandatory = $isMandatory;
        $this->isMultiselect = $isMultiselect;
        $this->creatorViewId = $creatorViewId;
    }
}
