<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use Symfony\Component\Validator\Constraints as Assert;

class InspirationText
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string|null
     */
    public $title;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var string
     * @Assert\NotBlank
     * @Assert\Locale
     */
    public $language;

    /**
     * InspirationText constructor.
     *
     * @param string $language
     * @param int|null $id
     * @param string|null $title
     * @param string|null $description
     */
    public function __construct(int $id = null, string $title = null, string $description = null, string $language = '')
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->language = $language;
    }
}
