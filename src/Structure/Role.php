<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role.
 */
class Role
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $role;

    /**
     * @var array<Credential>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Credential>")
     */
    public $credentials;
}
