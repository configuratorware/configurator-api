<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ConfigurationValidatorResult.
 */
class ConfigurationComponentValidationResult
{
    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $valid = true;

    /**
     * @var array<ConfigurationComponentValidationError>
     * @Serializer\Type("array")
     */
    public $errors = [];

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $componentTitle;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $optionTitle;

    /**
     * @param bool $valid
     * @param array<ConfigurationComponentValidationError> $errors
     * @param string $componentTitle
     * @param string $optionTitle
     */
    public function __construct(bool $valid = true, array $errors = [], ?string $componentTitle = '', ?string $optionTitle = '')
    {
        $this->valid = $valid;
        $this->errors = $errors;
        $this->componentTitle = $componentTitle;
        $this->optionTitle = $optionTitle;
    }
}
