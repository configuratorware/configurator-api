<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

final class MediaInfoOptionImages
{
    /**
     * @var MediaInfoImage
     */
    public $optionThumbnail;

    /**
     * @var MediaInfoImage
     */
    public $optionDetailImage;

    /**
     * @var array<mixed>|array<MediaInfoImage>
     */
    public $visualizationImages = [];

    /**
     * @param MediaInfoImage    $optionThumbnail
     * @param MediaInfoImage    $optionDetailImage
     * @param array|MediaInfoImage[] $visualizationImages
     */
    private function __construct(MediaInfoImage $optionThumbnail, MediaInfoImage $optionDetailImage, array $visualizationImages = [])
    {
        $this->visualizationImages = $visualizationImages;
        $this->optionThumbnail = $optionThumbnail;
        $this->optionDetailImage = $optionDetailImage;
    }

    /**
     * @param MediaInfoImage               $optionThumbnail
     * @param MediaInfoImage               $optionDetailImage
     * @param MediaInfoImage[] $layerImages
     *
     * @return self
     */
    public static function from(MediaInfoImage $optionThumbnail, MediaInfoImage $optionDetailImage, array $layerImages): self
    {
        return new self($optionThumbnail, $optionDetailImage, $layerImages);
    }
}
