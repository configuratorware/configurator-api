<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Redhotmagma\ConfiguratorApiBundle\Entity\User as UserEntity;
use Symfony\Component\Validator\Constraints as Assert;

class ApiUser
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var bool|null
     * @Serializer\Type("boolean")
     */
    public $active;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $username;

    /**
     * @var string|null
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    public $email;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $api_key;

    /**
     * @param int|null $id
     * @param bool|null $active
     * @param string|null $username
     * @param string|null $email
     * @param string|null $api_key
     */
    public function __construct(?int $id = null, ?bool $active = false, ?string $username = null, ?string $email = null, ?string $api_key = null)
    {
        $this->id = $id;
        $this->active = $active;
        $this->username = $username;
        $this->email = $email;
        $this->api_key = $api_key;
    }

    public static function from(UserEntity $user): self
    {
        return new self($user->getId(), $user->getIsActive(), $user->getUsername(), $user->getEmail(), $user->getApiKey());
    }
}
