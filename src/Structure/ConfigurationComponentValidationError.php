<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

class ConfigurationComponentValidationError
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $message;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $detailMessage;

    /**
     * @var string|null
     * @Serializer\Type("string")
     */
    public $value;

    /**
     * @var array<mixed>
     * @Serializer\Type("array")
     */
    public $options;

    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $amounts;

    /**
     * @param string $message
     * @param string $detailMessage
     * @param string|null $value
     */
    public function __construct(string $message = '', string $detailMessage = '', string $value = null, array $options = [], array $amounts = [])
    {
        $this->message = $message;
        $this->detailMessage = $detailMessage;
        $this->value = $value;
        $this->options = $options;
        $this->amounts = $amounts;
    }
}
