<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;

final class MediaInfoComponent
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $translated_title;

    /**
     * @var MediaInfoComponentImages
     */
    public $images;

    /**
     * @var array<MediaInfoOption>
     */
    public $options;

    /**
     * @param int $id
     * @param string $identifier
     * @param string $translated_title
     * @param MediaInfoComponentImages $images
     * @param MediaInfoOption[] $options
     */
    private function __construct(int $id, string $identifier, string $translated_title, MediaInfoComponentImages $images, array $options)
    {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->translated_title = $translated_title;
        $this->images = $images;
        $this->options = $options;
    }

    /**
     * @param Optionclassification $componentEntity
     * @param MediaInfoImage $componentThumbnail
     * @param MediaInfoOption[] $options
     *
     * @return static
     */
    public static function from(Optionclassification $componentEntity, MediaInfoImage $componentThumbnail, array $options): self
    {
        return new self(
            (int)$componentEntity->getId(),
            $componentEntity->getIdentifier(),
            $componentEntity->getTranslatedTitle(),
            MediaInfoComponentImages::from($componentThumbnail),
            $options
        );
    }
}
