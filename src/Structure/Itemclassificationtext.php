<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Attributetext.
 */
class Itemclassificationtext
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\Regex("/[a-z]{2}_[A-Z]{2}/")
     * @Serializer\Groups({"administrationarea"})
     */
    public $language;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $description;
}
