<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item as ItemEntity;

class ItemInspiration
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string
     */
    public $title;

    /**
     * @var int
     */
    public $inspirationAmount;

    /**
     * @param int $id
     * @param string $identifier
     * @param string $title
     * @param int $inspirationAmount
     */
    public function __construct(int $id, string $identifier, string $title, int $inspirationAmount)
    {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->title = $title;
        $this->inspirationAmount = $inspirationAmount;
    }

    /**
     * @param ItemEntity $item
     * @param array<int, int> $amountMap
     * @param string $language
     *
     * @return self
     */
    public static function fromEntity(ItemEntity $item, array $amountMap): self
    {
        $count = $amountMap[$item->getId()] ?? 0;

        return new self((int)$item->getId(), $item->getIdentifier(), $item->getTranslatedTitle(), $count);
    }
}
