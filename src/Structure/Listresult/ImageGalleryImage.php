<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Role.
 */
class ImageGalleryImage
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<ImageGalleryImage>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage>")
     */
    public $data;
}
