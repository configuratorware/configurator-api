<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Role.
 */
class Tag
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Tag>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Tag>")
     */
    public $data;
}
