<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Codesnippet.
 */
class Codesnippet
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Codesnippet>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Codesnippet>")
     */
    public $data;
}
