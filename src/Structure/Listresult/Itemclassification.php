<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Itemclassification.
 */
class Itemclassification
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $metadata;

    /**
     * @var array<\Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification>")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $data;
}
