<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Role.
 */
class Role
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Role>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Role>")
     */
    public $data;
}
