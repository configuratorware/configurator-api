<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Item.
 */
class Item
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $metadata;

    /**
     * @var array<Item>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Item>")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $data;
}
