<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Option.
 */
class Option
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $metadata;

    /**
     * @var array<Option>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Option>")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $data;
}
