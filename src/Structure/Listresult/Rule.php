<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Rule.
 */
class Rule
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Rule>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Rule>")
     */
    public $data;
}
