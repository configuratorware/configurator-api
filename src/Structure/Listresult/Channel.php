<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Channel.
 */
class Channel
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<\Redhotmagma\ConfiguratorApiBundle\Structure\Channel>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Channel>")
     */
    public $data;
}
