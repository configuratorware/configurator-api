<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Assemblypoint.
 */
class Assemblypoint
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Assemblypoint>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Assemblypoint>")
     */
    public $data;
}
