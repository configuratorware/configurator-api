<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Metadata.
 */
class Metadata
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $totalcount;
}
