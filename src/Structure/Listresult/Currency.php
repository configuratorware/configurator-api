<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Currency.
 */
class Currency
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Currency>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Currency>")
     */
    public $data;
}
