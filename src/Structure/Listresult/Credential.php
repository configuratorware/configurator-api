<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Credential.
 */
class Credential
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Credential>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Credential>")
     */
    public $data;
}
