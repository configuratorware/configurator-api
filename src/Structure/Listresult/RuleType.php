<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class RuleType.
 */
class RuleType
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<RuleType>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\RuleType>")
     */
    public $data;
}
