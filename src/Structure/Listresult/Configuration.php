<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Listresult;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class Configuration.
 */
class Configuration
{
    /**
     * @var Metadata
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Listresult\Metadata")
     */
    public $metadata;

    /**
     * @var array<Configuration>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Configuration>")
     */
    public $data;
}
