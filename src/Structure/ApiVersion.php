<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ApiVersion.
 */
class ApiVersion
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $version;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $latestReleasedVersion;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $reference;
}
