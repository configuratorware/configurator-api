<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class QuestionTree
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * Title from QuestionTreeText for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $translated_title;

    /**
     * @var array<QuestionTreeText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTreeText>")
     * @Assert\Valid
     */
    public $texts = [];

    /**
     * @var array<QuestionTreeData>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTreeData>")
     * @Assert\Valid
     */
    public $data = [];
}
