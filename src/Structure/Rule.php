<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Role.
 */
class Rule
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @Serializer\Type("object")
     * @Assert\NotBlank()
     */
    public $data;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\RuleType")
     * @Assert\NotBlank()
     */
    public $ruleType;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Item")
     */
    public $item;

    /**
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Option")
     */
    public $option;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;
}
