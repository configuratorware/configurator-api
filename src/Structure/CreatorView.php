<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

final class CreatorView
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $sequenceNumber;

    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $itemId;

    /**
     * @var array<DesignAreaRelation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaRelation>")
     */
    public $designAreas;

    /**
     * @var array<CreatorViewText>
     *
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\CreatorViewText>")
     */
    public $texts;

    /**
     * @Serializer\Type("array")
     */
    public $layerImages;

    /**
     * @param int|null $id
     * @param string|null $identifier
     * @param string|null $sequenceNumber
     * @param int|null $itemId
     * @param array $designAreas
     * @param array $texts
     *
     * @psalm-suppress PossiblyNullPropertyAssignmentValue
     */
    public function __construct(?int $id = null, string $identifier = null, string $sequenceNumber = null, int $itemId = null, array $designAreas = [], array $texts = [])
    {
        $this->id = $id;
        $this->identifier = $identifier;
        $this->sequenceNumber = $sequenceNumber;
        $this->itemId = $itemId;
        $this->designAreas = $designAreas;
        $this->texts = $texts;
    }
}
