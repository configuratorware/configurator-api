<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

final class MediaInfo
{
    /**
     * @var array
     */
    public $viewMediaInfo;

    /**
     * @var array
     */
    public $componentMediaInfo;

    /**
     * @var array
     */
    public $possibleNamingIssues;

    /**
     * @param array $viewMediaInfo
     * @param array $componentMediaInfo
     * @param array $possibleNamingIssues
     */
    private function __construct(array $viewMediaInfo, array $componentMediaInfo, array $possibleNamingIssues)
    {
        $this->viewMediaInfo = $viewMediaInfo;
        $this->componentMediaInfo = $componentMediaInfo;
        $this->possibleNamingIssues = $possibleNamingIssues;
    }

    /**
     * @param array $viewMediaInfo
     * @param array $componentMediaInfo
     * @param array $possibleNamingIssues
     *
     * @return MediaInfo
     */
    public static function from(array $viewMediaInfo, array $componentMediaInfo, array $possibleNamingIssues): MediaInfo
    {
        return new self($viewMediaInfo, $componentMediaInfo, $possibleNamingIssues);
    }
}
