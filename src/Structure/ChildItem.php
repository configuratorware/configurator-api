<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ChildItem
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * title from Itemtext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var array<GroupRelation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\GroupRelation>")
     */
    public $groups;
}
