<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class CurrentClient
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @Serializer\Type("object")
     */
    public $theme;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactName;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactStreet;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactPostCode;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactCity;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactPhone;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactEmail;

    /**
     * @var array<ClientText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ClientText>")
     */
    public $texts;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customCss;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $callToAction;

    /**
     * @var string|null
     * @Assert\Email()
     */
    public $fromEmailAddress;

    /**
     * @var string|null
     */
    public $toEmailAddresses;

    /**
     * @var string|null
     */
    public $ccEmailAddresses;

    /**
     * @var string|null
     */
    public $bccEmailAddresses;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfHeaderHtml;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfFooterHtml;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfHeaderText;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfFooterText;
}
