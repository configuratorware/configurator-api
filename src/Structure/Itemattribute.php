<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Itemattribute.
 */
class Itemattribute
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $attribute_id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $identifier;

    /**
     * title from Attributetext for current language, read-only.
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $translated_title;

    /**
     * @var array<Attributetext>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Attributetext>")
     * @Serializer\Groups({"administrationarea"})
     */
    public $texts;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $attributedatatype;

    /**
     * @var array<Attributevalue>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue>")
     * @Serializer\Groups({"administrationarea","frontend"})
     * @Assert\Valid
     */
    public $values;
}
