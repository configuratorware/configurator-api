<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Attributevalue.
 */
class Attributevalue
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $value;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $translated_value;

    /**
     * @var array<Attributevaluetranslation>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Attributevaluetranslation>")
     * @Serializer\Groups({"administrationarea"})
     * @Assert\Valid
     */
    public $translations;
}
