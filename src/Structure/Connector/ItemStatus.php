<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure\Connector;

use JMS\Serializer\Annotation as Serializer;

class ItemStatus
{
    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $itemIdentifier;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $configurable;
}
