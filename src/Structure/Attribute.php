<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Attribute.
 */
class Attribute
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /** @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $identifier;

    /**
     * @Serializer\Type("string")
     * @Assert\NotNull()
     * @Serializer\Groups({"administrationarea"})
     */
    public bool $writeProtected = false;

    /**
     * read only field.
     *
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $translated_title;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Serializer\Groups({"administrationarea"})
     */
    public $externalid;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $attributedatatype;

    /**
     * @var array<Attributetext>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Attributetext>")
     * @Serializer\Groups({"administrationarea"})
     */
    public $texts;
}
