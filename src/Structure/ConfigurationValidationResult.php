<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

/**
 * Class ConfigurationValidatorResult.
 */
class ConfigurationValidationResult
{
    /**
     * @var bool
     * @Serializer\Type("bool")
     */
    public $valid;

    /**
     * @var array
     * @Serializer\Type("array")
     */
    public $validationErrors = [];
}
