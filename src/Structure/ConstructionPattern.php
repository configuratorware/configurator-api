<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;

class ConstructionPattern
{
    /**
     * @var Item
     * @Serializer\Type("Redhotmagma\ConfiguratorApiBundle\Structure\Item")
     */
    public $item;

    /**
     * @var array<SelectableComponent>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\SelectableComponent>")
     */
    public $selectableComponents = [];
}
