<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignProductionMethodPrice
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea"})
     */
    public $channel;

    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $amountFrom;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $colorAmountFrom;

    /**
     * @var float
     * @Serializer\Type("float")
     * @Serializer\Groups({"administrationarea"})
     */
    public $price;

    /**
     * @var float
     * @Serializer\Type("float")
     * @Serializer\Groups({"administrationarea"})
     */
    public $priceNet;
}
