<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignerProductionCalculationTypeDesignAreaRelation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $colorAmountDependent;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $itemAmountDependent;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $selectableByUser;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @var array<DesignAreaDesignProductionMethodPrice>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaDesignProductionMethodPrice>")
     */
    public $prices;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $bulkNameDependent;
}
