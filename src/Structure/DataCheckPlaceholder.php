<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

class DataCheckPlaceholder
{
    public const KEY_AVAILABLE = 'item_available';
    public const KEY_AMOUNT = 'amount';
    public const KEY_COMPONENTS_AMOUNT = 'components_amount';
    public const KEY_COMPONENT_NAME = 'component_name';
    public const KEY_TITLE = 'title';
    public const KEY_OPTIONS_AMOUNT = 'options_amount';
    public const KEY_OPTIONS = 'options';
    public const KEY_OPTION = 'option';

    /**
     * @var string
     */
    public $key;

    /**
     * @var string
     */
    public $value;

    /**
     * @param string $key
     * @param string|int|bool $value
     */
    public function __construct(string $key, $value)
    {
        $this->key = $key;
        $this->value = $value;
    }
}
