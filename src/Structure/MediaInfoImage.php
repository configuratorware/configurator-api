<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

final class MediaInfoImage
{
    /**
     * @var string|null
     */
    public $fileName;

    /**
     * @var string|null
     */
    public $src;

    /**
     * @var string|null
     */
    public $expectedSrc;

    /**
     * @param string|null $fileName
     * @param string|null $src
     * @param string|null $expectedSrc
     */
    private function __construct(string $fileName = null, string $src = null, string $expectedSrc = null)
    {
        $this->fileName = $fileName;
        $this->src = $src;
        $this->expectedSrc = $expectedSrc;
    }

    /**
     * @param string $path
     *
     * @return self
     */
    public static function fromPath(string $path): self
    {
        return new self(pathinfo($path)['basename'], $path);
    }

    /**
     * @param string $expectedSrc
     *
     * @return self
     */
    public static function forMissing(string $expectedSrc): self
    {
        return new self(null, null, $expectedSrc);
    }
}
