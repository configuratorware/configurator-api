<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Client
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $email;

    /**
     * @Serializer\Type("object")
     */
    public $theme;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactName;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactStreet;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactPostCode;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactCity;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactPhone;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $contactEmail;

    /**
     * @var array<ClientText>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\ClientText>")
     */
    public $texts;

    /**
     * @var array<Channel>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\Channel>")
     */
    public $channels;

    /**
     * @var array<User>
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\User>")
     */
    public $users;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $customCss;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $callToAction;

    /**
     * @var string|null
     * @Assert\Email()
     */
    public $fromEmailAddress;

    /**
     * @var string|null
     */
    public $toEmailAddresses;

    /**
     * @var string|null
     */
    public $ccEmailAddresses;

    /**
     * @var string|null
     */
    public $bccEmailAddresses;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfHeaderHtml;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfFooterHtml;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfHeaderText;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $pdfFooterText;

    /**
     * @var bool|null
     *
     * @Serializer\Type("boolean")
     */
    public $sendOfferRequestMailToCustomer;
}
