<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class ColorPaletteRelation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $identifier;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;
}
