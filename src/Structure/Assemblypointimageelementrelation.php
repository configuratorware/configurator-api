<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Assemblypointimageelementrelation.
 */
class Assemblypointimageelementrelation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     * @Serializer\Groups({"administrationarea"})
     */
    public $id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $assemblypointimageelement_id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $parent_assemblypointimageelement_id;

    /**
     * @var int
     * @Serializer\Type("integer")
     * @Assert\NotBlank()
     * @Serializer\Groups({"administrationarea","frontend"})
     */
    public $sequencenumber;
}
