<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class Font
{
    /**
     * @var int|null
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $active;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $isDefault;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameRegular;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameBold;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameItalic;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $fileNameBoldItalic;

    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $sequenceNumber;
}
