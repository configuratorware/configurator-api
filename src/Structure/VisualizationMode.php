<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

class VisualizationMode
{
    /**
     * @var int|null
     */
    public $id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var array<string>
     */
    public $validForConfigurationModes;

    /**
     * @var string
     */
    public $label;
}
