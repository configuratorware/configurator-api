<?php

namespace Redhotmagma\ConfiguratorApiBundle\Structure;

use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

class DesignerGlobalCalculationTypeRelation
{
    /**
     * @var int
     * @Serializer\Type("integer")
     */
    public $id;

    /**
     * @var string
     * @Serializer\Type("string")
     * @Assert\NotBlank()
     */
    public $identifier;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $itemAmountDependent;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $selectableByUser;

    /**
     * @var bool
     * @Serializer\Type("boolean")
     */
    public $imageDataDependent;

    /**
     * @var string
     * @Serializer\Type("string")
     */
    public $title;

    /**
     * @Serializer\Type("array<Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPrice>")
     */
    public $prices;
}
