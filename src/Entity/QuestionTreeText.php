<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Repository\QuestionTreeTextRepository;

/**
 * @ORM\Entity(repositoryClass=QuestionTreeTextRepository::class)
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *          name="uq_default",
 *          columns={"date_deleted","question_tree_id","language_id"}
 *     )}
 * )
 */
class QuestionTreeText
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=Language::class)
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity=QuestionTree::class, inversedBy="texts")
     * @ORM\JoinColumn(name="question_tree_id", referencedColumnName="id", nullable=false)
     */
    private $questionTree;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    public function __construct()
    {
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getQuestionTree(): ?QuestionTree
    {
        return $this->questionTree;
    }

    public function setQuestionTree(?QuestionTree $questionTree): self
    {
        $this->questionTree = $questionTree;

        return $this;
    }

    public function setDateCreated(\DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->date_created;
    }

    public function setDateUpdated(?\DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    public function getDateUpdated(): \DateTime
    {
        return $this->date_updated;
    }

    public function setDateDeleted(\DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    public function getDateDeleted(): \DateTime
    {
        return $this->date_deleted;
    }

    public function setUserCreatedId(?int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    public function setUserUpdatedId(?int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    public function setUserDeletedId(?int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }
}
