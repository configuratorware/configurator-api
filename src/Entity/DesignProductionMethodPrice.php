<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodPriceRepository")
 * @ORM\Table(name="design_production_method_price")
 */
class DesignProductionMethodPrice
{
    /**
     * @var
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amount_from;

    /**
     * @ORM\Column(type="integer", nullable=true) int|null
     */
    private $color_amount_from;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5) string|null
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5) string|null
     */
    private $price_net;

    /**
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted = '0001-01-01 00:00:00';

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\Channel
     */
    private $channel;

    /**
     * @var
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType",
     *     inversedBy="designProductionMethodPrice"
     * )
     * @ORM\JoinColumn(name="designer_production_calculation_type_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType
     */
    private $designerProductionCalculationType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getAmountFrom(): ?int
    {
        return $this->amount_from;
    }

    /**
     * @param int|null $amount_from
     *
     * @return DesignProductionMethodPrice
     */
    public function setAmountFrom(?int $amount_from): DesignProductionMethodPrice
    {
        $this->amount_from = $amount_from;

        return $this;
    }

    /**
     * @param int|null $colorAmountFrom
     *
     * @return DesignProductionMethodPrice
     */
    public function setColorAmountFrom(?int $colorAmountFrom = null): DesignProductionMethodPrice
    {
        $this->color_amount_from = $colorAmountFrom;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getColorAmountFrom(): ?int
    {
        return $this->color_amount_from;
    }

    /**
     * @param string|null $price
     *
     * @return DesignProductionMethodPrice
     */
    public function setPrice(?string $price = null): DesignProductionMethodPrice
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrice(): ?string
    {
        return $this->price;
    }

    /**
     * @param string|null $priceNet
     *
     * @return DesignProductionMethodPrice
     */
    public function setPriceNet(?string $priceNet = null): DesignProductionMethodPrice
    {
        $this->price_net = $priceNet;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPriceNet(): ?string
    {
        return $this->price_net;
    }

    /**
     * @param \DateTime $dateCreated
     *
     * @return DesignProductionMethodPrice
     */
    public function setDateCreated(\DateTime $dateCreated): DesignProductionMethodPrice
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCreated(): ?\DateTime
    {
        return $this->date_created;
    }

    /**
     * @param \DateTime|null $dateUpdated
     *
     * @return DesignProductionMethodPrice
     */
    public function setDateUpdated(?\DateTime $dateUpdated = null): DesignProductionMethodPrice
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateUpdated(): \DateTime
    {
        return $this->date_updated;
    }

    /**
     * @param \DateTime $dateDeleted
     *
     * @return DesignProductionMethodPrice
     */
    public function setDateDeleted(\DateTime $dateDeleted): DesignProductionMethodPrice
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateDeleted(): \DateTime
    {
        return $this->date_deleted;
    }

    /**
     * @param int|null $userCreatedId
     *
     * @return DesignProductionMethodPrice
     */
    public function setUserCreatedId(?int $userCreatedId = null): DesignProductionMethodPrice
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * @param int|null $userUpdatedId
     *
     * @return DesignProductionMethodPrice
     */
    public function setUserUpdatedId(?int $userUpdatedId = null): DesignProductionMethodPrice
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * @param int|null $userDeletedId
     *
     * @return DesignProductionMethodPrice
     */
    public function setUserDeletedId(?int $userDeletedId = null): DesignProductionMethodPrice
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    /**
     * @param Channel|null $channel
     *
     * @return DesignProductionMethodPrice
     */
    public function setChannel(?Channel $channel): DesignProductionMethodPrice
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return Channel|null
     */
    public function getChannel(): ?Channel
    {
        return $this->channel;
    }

    /**
     * @param DesignerProductionCalculationType|null $designerProductionCalculationType
     *
     * @return DesignProductionMethodPrice
     */
    public function setDesignerProductionCalculationType(
        DesignerProductionCalculationType $designerProductionCalculationType = null
    ): DesignProductionMethodPrice {
        $this->designerProductionCalculationType = $designerProductionCalculationType;

        return $this;
    }

    /**
     * @return DesignerProductionCalculationType|null
     */
    public function getDesignerProductionCalculationType(): ?DesignerProductionCalculationType
    {
        return $this->designerProductionCalculationType;
    }
}
