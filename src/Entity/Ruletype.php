<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\RuletypeRepository")
 * @ORM\Table(name="ruletype")
 */
class Ruletype
{
    public const OPTION_EXCLUSION = 'optionexclusion';
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $sequencenumber;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $availablerulefields;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $itemrule;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $optionrule;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $globalrule;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\RuletypeRulefeedback",
     *     mappedBy="ruletype"
     * )
     */
    private $ruletypeRulefeedback;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rule", mappedBy="ruletype")
     */
    private $rule;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext", mappedBy="ruletype")
     */
    private $rulefeedbacktext;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\RuleTypeText", mappedBy="ruletype")
     */
    private $ruleTypeText;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->ruletypeRulefeedback = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rule = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rulefeedbacktext = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ruleTypeText = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Ruletype
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set sequencenumber.
     *
     * @param int $sequencenumber
     *
     * @return Ruletype
     */
    public function setSequencenumber($sequencenumber)
    {
        $this->sequencenumber = $sequencenumber;

        return $this;
    }

    /**
     * Get sequencenumber.
     *
     * @return int
     */
    public function getSequencenumber()
    {
        return $this->sequencenumber;
    }

    /**
     * Set availablerulefields.
     *
     * @param string $availablerulefields
     *
     * @return Ruletype
     */
    public function setAvailablerulefields($availablerulefields)
    {
        if ($availablerulefields) {
            $availablerulefields = json_encode($availablerulefields);
        }

        $this->availablerulefields = $availablerulefields;

        return $this;
    }

    /**
     * Get availablerulefields.
     *
     * @return string
     */
    public function getAvailablerulefields()
    {
        return json_decode($this->availablerulefields);
    }

    /**
     * Set itemrule.
     *
     * @param bool $itemrule
     *
     * @return Ruletype
     */
    public function setItemrule($itemrule)
    {
        $this->itemrule = $itemrule;

        return $this;
    }

    /**
     * Get itemrule.
     *
     * @return bool
     */
    public function getItemrule()
    {
        return $this->itemrule;
    }

    /**
     * @return mixed
     */
    public function getOptionrule()
    {
        return $this->optionrule;
    }

    /**
     * @param mixed $optionrule
     */
    public function setOptionrule($optionrule)
    {
        $this->optionrule = $optionrule;
    }

    /**
     * Set globalrule.
     *
     * @param bool $globalrule
     *
     * @return Ruletype
     */
    public function setGlobalrule($globalrule)
    {
        $this->globalrule = $globalrule;

        return $this;
    }

    /**
     * Get globalrule.
     *
     * @return bool
     */
    public function getGlobalrule()
    {
        return $this->globalrule;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Ruletype
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Ruletype
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Ruletype
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Ruletype
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Ruletype
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Ruletype
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add ruletypeRulefeedback.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RuletypeRulefeedback $ruletypeRulefeedback
     *
     * @return Ruletype
     */
    public function addRuletypeRulefeedback(
        RuletypeRulefeedback $ruletypeRulefeedback
    ) {
        $this->ruletypeRulefeedback[] = $ruletypeRulefeedback;

        return $this;
    }

    /**
     * Remove ruletypeRulefeedback.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RuletypeRulefeedback $ruletypeRulefeedback
     */
    public function removeRuletypeRulefeedback(
        RuletypeRulefeedback $ruletypeRulefeedback
    ) {
        $this->ruletypeRulefeedback->removeElement($ruletypeRulefeedback);
    }

    /**
     * Get ruletypeRulefeedback.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRuletypeRulefeedback()
    {
        return $this->ruletypeRulefeedback;
    }

    /**
     * Add rule.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $rule
     *
     * @return Ruletype
     */
    public function addRule(Rule $rule)
    {
        $this->rule[] = $rule;

        return $this;
    }

    /**
     * Remove rule.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $rule
     */
    public function removeRule(Rule $rule)
    {
        $this->rule->removeElement($rule);
    }

    /**
     * Get rule.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Add rulefeedbacktext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext $rulefeedbacktext
     *
     * @return Ruletype
     */
    public function addRulefeedbacktext(Rulefeedbacktext $rulefeedbacktext)
    {
        $this->rulefeedbacktext[] = $rulefeedbacktext;

        return $this;
    }

    /**
     * Remove rulefeedbacktext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext $rulefeedbacktext
     */
    public function removeRulefeedbacktext(Rulefeedbacktext $rulefeedbacktext)
    {
        $this->rulefeedbacktext->removeElement($rulefeedbacktext);
    }

    /**
     * Get rulefeedbacktext.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRulefeedbacktext()
    {
        return $this->rulefeedbacktext;
    }

    /**
     * Add ruleTypeText.
     *
     * @param RuleTypeText $ruleTypeText
     *
     * @return Ruletype
     */
    public function addRuleTypeText(RuleTypeText $ruleTypeText)
    {
        $this->ruleTypeText[] = $ruleTypeText;

        return $this;
    }

    /**
     * Remove ruleTypeText.
     *
     * @param RuleTypeText $ruleTypeText
     */
    public function removeRuleTypeText(RuleTypeText $ruleTypeText)
    {
        $this->ruleTypeText->removeElement($ruleTypeText);
    }

    /**
     * Get ruleTypeText.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRuleTypeText()
    {
        return $this->ruleTypeText;
    }

    /**
     * returns the translated title of the rule type for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle()
    {
        $title = $this->getIdentifier();

        $ruleTypeTexts = $this->getRuleTypeText();

        /**
         * @var $ruleTypeText RuleTypeText
         */
        foreach ($ruleTypeTexts as $ruleTypeText) {
            if (C_LANGUAGE_ISO == $ruleTypeText->getLanguage()
                    ->getIso() && '' != $ruleTypeText->getTitle()
            ) {
                $title = $ruleTypeText->getTitle();

                break;
            }
        }

        return $title;
    }

    /**
     * returns the translated rule title of the rule type for the current Language.
     *
     * @return  string
     */
    public function getTranslatedRuleTitle($languageIso = null)
    {
        $language = C_LANGUAGE_ISO;
        if (!empty($languageIso)) {
            $language = $languageIso;
        }

        $ruleTitle = '';

        $ruleTypeTexts = $this->getRuleTypeText();

        /**
         * @var $ruleTypeText RuleTypeText
         */
        foreach ($ruleTypeTexts as $ruleTypeText) {
            if ($ruleTypeText->getLanguage()
                    ->getIso() == $language && '' != $ruleTypeText->getRuleTitle()
            ) {
                $ruleTitle = $ruleTypeText->getRuleTitle();

                break;
            }
        }

        return $ruleTitle;
    }
}
