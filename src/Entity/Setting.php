<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository")
 * @ORM\Table(name="setting", uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted"})})
 */
class Setting
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $visualizationcomponent;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $visualizationsettings;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $calculationmethod;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $shareurl;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $defaultmailsenderaddress;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $customsettings;

    /**
     * @ORM\Column(type="float", nullable=false, options={"default":"2"})
     */
    private $max_zoom_2d = 2;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $custom_css;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $default_visualization_mode_label;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $show_item_identifier = false;

    /**
     * @ORM\Column(type="string", nullable=false, options={"default":"addToCart"})
     *
     * @deprecated use client.call_to_action instead
     */
    private $call_to_action = 'addToCart';

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $configurations_client_restricted;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $clear_cache_on_translations_upload;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set calculationmethod.
     *
     * @param string $calculationmethod
     *
     * @return Setting
     */
    public function setCalculationmethod($calculationmethod)
    {
        $this->calculationmethod = $calculationmethod;

        return $this;
    }

    /**
     * Get calculationmethod.
     *
     * @return string
     */
    public function getCalculationmethod()
    {
        return $this->calculationmethod;
    }

    /**
     * Set shareurl.
     *
     * @param string $shareurl
     *
     * @return Setting
     */
    public function setShareurl($shareurl)
    {
        $this->shareurl = $shareurl;

        return $this;
    }

    /**
     * Get shareurl.
     *
     * @return string
     */
    public function getShareurl()
    {
        return $this->shareurl;
    }

    /**
     * Set defaultmailsenderaddress.
     *
     * @param string $defaultmailsenderaddress
     *
     * @return Setting
     */
    public function setDefaultmailsenderaddress($defaultmailsenderaddress)
    {
        $this->defaultmailsenderaddress = $defaultmailsenderaddress;

        return $this;
    }

    /**
     * Get defaultmailsenderaddress.
     *
     * @return string
     */
    public function getDefaultmailsenderaddress()
    {
        return $this->defaultmailsenderaddress;
    }

    /**
     * @param string|null $customCss
     *
     * @return Setting
     */
    public function setCustomCss(?string $customCss): self
    {
        $this->custom_css = $customCss;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomCss(): ?string
    {
        return $this->custom_css;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Setting
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Setting
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Setting
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Setting
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Setting
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Setting
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set visualizationcomponent.
     *
     * @param string $visualizationcomponent
     *
     * @return Setting
     */
    public function setVisualizationcomponent($visualizationcomponent)
    {
        $this->visualizationcomponent = $visualizationcomponent;

        return $this;
    }

    /**
     * Get visualizationcomponent.
     *
     * @return string
     */
    public function getVisualizationcomponent()
    {
        return $this->visualizationcomponent;
    }

    /**
     * Set visualizationsettings.
     *
     * @param string $visualizationsettings
     *
     * @return Setting
     */
    public function setVisualizationsettings($visualizationsettings)
    {
        $this->visualizationsettings = $visualizationsettings;

        return $this;
    }

    /**
     * Get visualizationsettings.
     *
     * @return string
     */
    public function getVisualizationsettings()
    {
        return $this->visualizationsettings;
    }

    /**
     * @return mixed
     */
    public function getCustomsettings()
    {
        return $this->customsettings;
    }

    /**
     * @param mixed $customsettings
     *
     * @return Setting
     */
    public function setCustomsettings($customsettings)
    {
        $this->customsettings = $customsettings;

        return $this;
    }

    /**
     * @return float
     */
    public function getMaxZoom2d(): float
    {
        return $this->max_zoom_2d;
    }

    /**
     * @param float $maxZoom2d
     *
     * @return Setting
     */
    public function setMaxZoom2d(float $maxZoom2d): self
    {
        $this->max_zoom_2d = $maxZoom2d;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultVisualizationModeLabel(): ?string
    {
        return $this->default_visualization_mode_label;
    }

    /**
     * @param string|null $defaultVisualizationModeLabel
     */
    public function setDefaultVisualizationModeLabel($defaultVisualizationModeLabel): void
    {
        $this->default_visualization_mode_label = $defaultVisualizationModeLabel;
    }

    /**
     * @return bool
     */
    public function getShowItemIdentifier(): bool
    {
        return $this->show_item_identifier;
    }

    /**
     * @param bool $showItemIdentifier
     */
    public function setShowItemIdentifier(bool $showItemIdentifier): void
    {
        $this->show_item_identifier = $showItemIdentifier;
    }

    /**
     * @return string
     *
     * @deprecated use client.getCallToAction() instead
     */
    public function getCallToAction(): string
    {
        return $this->call_to_action;
    }

    /**
     * @param string $callToAction
     *
     * @deprecated use client.setCallToAction() instead
     */
    public function setCallToAction($callToAction): void
    {
        $this->call_to_action = $callToAction;
    }

    /**
     * @return bool
     */
    public function getConfigurationsClientRestricted()
    {
        return $this->configurations_client_restricted;
    }

    /**
     * @param bool $configurations_client_restricted
     *
     * @return self
     */
    public function setConfigurationsClientRestricted(bool $configurations_client_restricted): self
    {
        $this->configurations_client_restricted = $configurations_client_restricted;

        return $this;
    }

    public function getClearCacheOnTranslationsUpload(): bool
    {
        return (bool)$this->clear_cache_on_translations_upload;
    }

    public function setClearCacheOnTranslationsUpload(bool $clearCacheOnTranslationsUpload): void
    {
        $this->clear_cache_on_translations_upload = $clearCacheOnTranslationsUpload;
    }
}
