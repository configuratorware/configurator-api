<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\RulefeedbackRepository")
 * @ORM\Table(name="rulefeedback")
 */
class Rulefeedback
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\RuletypeRulefeedback",
     *     mappedBy="rulefeedback"
     * )
     */
    private $ruletypeRulefeedback;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rule", mappedBy="rulefeedback")
     */
    private $rule;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext",
     *     mappedBy="rulefeedback"
     * )
     */
    private $rulefeedbacktext;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->ruletypeRulefeedback = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rule = new \Doctrine\Common\Collections\ArrayCollection();
        $this->rulefeedbacktext = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Rulefeedback
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Rulefeedback
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Rulefeedback
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Rulefeedback
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Rulefeedback
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Rulefeedback
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Rulefeedback
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add ruletypeRulefeedback.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RuletypeRulefeedback $ruletypeRulefeedback
     *
     * @return Rulefeedback
     */
    public function addRuletypeRulefeedback(RuletypeRulefeedback $ruletypeRulefeedback)
    {
        $this->ruletypeRulefeedback[] = $ruletypeRulefeedback;

        return $this;
    }

    /**
     * Remove ruletypeRulefeedback.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RuletypeRulefeedback $ruletypeRulefeedback
     */
    public function removeRuletypeRulefeedback(RuletypeRulefeedback $ruletypeRulefeedback)
    {
        $this->ruletypeRulefeedback->removeElement($ruletypeRulefeedback);
    }

    /**
     * Get ruletypeRulefeedback.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRuletypeRulefeedback()
    {
        return $this->ruletypeRulefeedback;
    }

    /**
     * Add rule.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $rule
     *
     * @return Rulefeedback
     */
    public function addRule(Rule $rule)
    {
        $this->rule[] = $rule;

        return $this;
    }

    /**
     * Remove rule.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $rule
     */
    public function removeRule(Rule $rule)
    {
        $this->rule->removeElement($rule);
    }

    /**
     * Get rule.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * Add rulefeedbacktext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext $rulefeedbacktext
     *
     * @return Rulefeedback
     */
    public function addRulefeedbacktext(Rulefeedbacktext $rulefeedbacktext)
    {
        $this->rulefeedbacktext[] = $rulefeedbacktext;

        return $this;
    }

    /**
     * Remove rulefeedbacktext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext $rulefeedbacktext
     */
    public function removeRulefeedbacktext(Rulefeedbacktext $rulefeedbacktext)
    {
        $this->rulefeedbacktext->removeElement($rulefeedbacktext);
    }

    /**
     * Get rulefeedbacktext.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRulefeedbacktext()
    {
        return $this->rulefeedbacktext;
    }
}
