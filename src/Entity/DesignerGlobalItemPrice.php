<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * DesignerGlobalItemPrice.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalItemPriceRepository")
 * @ORM\Table(name="designer_global_item_price")
 */
class DesignerGlobalItemPrice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $amount_from;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     *
     * @var string|null
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     *
     * @var string|null
     */
    private $price_net;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @var DateTime
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime|null
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     *
     * @var DateTime
     */
    private DateTime $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     *
     * @var Channel|null
     */
    private ?Channel $channel;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType",
     *     inversedBy="designerGlobalItemPrice"
     * )
     * @ORM\JoinColumn(name="designer_global_calculation_type_id", referencedColumnName="id")
     *
     * @var DesignerGlobalCalculationType|null
     */
    private ?DesignerGlobalCalculationType $designerGlobalCalculationType;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item",
     *     inversedBy="designerGlobalItemPrice"
     * )
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     *
     * @var Item|null
     */
    private ?Item $item;

    public function __construct()
    {
        $this->date_deleted = new DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amountFrom.
     *
     * @param int|null $amountFrom
     *
     * @return DesignerGlobalItemPrice
     */
    public function setAmountFrom($amountFrom = null)
    {
        $this->amount_from = $amountFrom;

        return $this;
    }

    /**
     * Get amountFrom.
     *
     * @return int|null
     */
    public function getAmountFrom()
    {
        return $this->amount_from;
    }

    /**
     * Set price.
     *
     * @param string|null $price
     *
     * @return DesignerGlobalItemPrice
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set priceNet.
     *
     * @param string|null $priceNet
     *
     * @return DesignerGlobalItemPrice
     */
    public function setPriceNet($priceNet = null)
    {
        $this->price_net = $priceNet;

        return $this;
    }

    /**
     * Get priceNet.
     *
     * @return string|null
     */
    public function getPriceNet()
    {
        return $this->price_net;
    }

    /**
     * Set dateCreated.
     *
     * @param DateTime $dateCreated
     *
     * @return DesignerGlobalItemPrice
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param DateTime|null $dateUpdated
     *
     * @return DesignerGlobalItemPrice
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param DateTime $dateDeleted
     *
     * @return DesignerGlobalItemPrice
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignerGlobalItemPrice
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignerGlobalItemPrice
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignerGlobalItemPrice
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set channel.
     *
     * @param Channel|null $channel
     *
     * @return DesignerGlobalItemPrice
     */
    public function setChannel(Channel $channel = null)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel.
     *
     * @return Channel|null
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set designerGlobalCalculationType.
     *
     * @param DesignerGlobalCalculationType|null $designerGlobalCalculationType
     *
     * @return DesignerGlobalItemPrice
     */
    public function setDesignerGlobalCalculationType(DesignerGlobalCalculationType $designerGlobalCalculationType = null)
    {
        $this->designerGlobalCalculationType = $designerGlobalCalculationType;

        return $this;
    }

    /**
     * Get designerGlobalCalculationType.
     *
     * @return DesignerGlobalCalculationType|null
     */
    public function getDesignerGlobalCalculationType()
    {
        return $this->designerGlobalCalculationType;
    }

    /**
     * Set item.
     *
     * @param Item|null $item
     *
     * @return DesignerGlobalItemPrice
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return Item|null
     */
    public function getItem()
    {
        return $this->item;
    }
}
