<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionRepository")
 * @ORM\Table(
 *     name="item_optionclassification_option",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","item_optionclassification_id","option_id"})}
 * )
 */
class ItemOptionclassificationOption
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private bool $amountisselectable = false;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":0})
     */
    private ?int $sequenceNumber = null;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private \DateTime $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOptionDeltaprice",
     *     mappedBy="itemOptionclassificationOption",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, ItemOptionclassificationOptionDeltaprice>
     */
    private Collection $itemOptionclassificationOptionDeltaprice;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification",
     *     inversedBy="itemOptionclassificationOption"
     * )
     * @ORM\JoinColumn(name="item_optionclassification_id", referencedColumnName="id")
     */
    private ItemOptionclassification $itemOptionclassification;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option",
     *     inversedBy="itemOptionclassificationOption"
     * )
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private Option $option;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->itemOptionclassificationOptionDeltaprice = new ArrayCollection();
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime('0001-01-01 00:00:00');
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setDateCreated(\DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->date_created;
    }

    public function setDateUpdated(?\DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    public function getDateUpdated(): ?\DateTime
    {
        return $this->date_updated;
    }

    public function setDateDeleted(\DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    public function getDateDeleted(): \DateTime
    {
        return $this->date_deleted;
    }

    public function setUserCreatedId(?int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    public function setUserUpdatedId(?int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    public function setUserDeletedId(?int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    public function addItemOptionclassificationOptionDeltaprice(ItemOptionclassificationOptionDeltaprice $itemOptionClassificationOptionDeltaPrice): self
    {
        $itemOptionClassificationOptionDeltaPrice->setItemOptionclassificationOption($this);

        $this->itemOptionclassificationOptionDeltaprice[] = $itemOptionClassificationOptionDeltaPrice;

        return $this;
    }

    public function removeItemOptionclassificationOptionDeltaprice(ItemOptionclassificationOptionDeltaprice $itemOptionClassificationOptionDeltaPrice): void
    {
        $key = $this->itemOptionclassificationOptionDeltaprice->indexOf($itemOptionClassificationOptionDeltaPrice);
        if (false === $key) {
            return;
        }

        $element = $this->itemOptionclassificationOptionDeltaprice->get($key);
        if (null !== $element) {
            $element->setDateDeleted(new \DateTime());
        }
    }

    /**
     * @return Collection<array-key, ItemOptionclassificationOptionDeltaprice>
     */
    public function getItemOptionclassificationOptionDeltaprice(): Collection
    {
        return $this->itemOptionclassificationOptionDeltaprice;
    }

    public function setItemOptionclassification(ItemOptionclassification $itemOptionclassification): self
    {
        $this->itemOptionclassification = $itemOptionclassification;

        return $this;
    }

    public function getItemOptionclassification(): ItemOptionclassification
    {
        return $this->itemOptionclassification;
    }

    public function setOption(Option $option): self
    {
        $this->option = $option;

        return $this;
    }

    public function getOption(): Option
    {
        return $this->option;
    }

    public function setAmountisselectable(bool $amountisselectable): self
    {
        $this->amountisselectable = $amountisselectable;

        return $this;
    }

    public function getAmountisselectable(): bool
    {
        return $this->amountisselectable;
    }

    public function getSequenceNumber(): ?int
    {
        return $this->sequenceNumber;
    }

    public function setSequenceNumber(?int $sequenceNumber): self
    {
        $this->sequenceNumber = $sequenceNumber;

        return $this;
    }
}
