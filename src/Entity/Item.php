<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository")
 * @ORM\Table(
 *     name="item",
 *     indexes={@ORM\Index(name="ix_parent", columns={"parent_id"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_identifier_date_deleted", columns={"identifier","date_deleted"})}
 * )
 */
class Item
{
    public const CONFIGURATION_MODE_CREATOR = 'creator';
    public const CONFIGURATION_MODE_DESIGNER = 'designer';
    public const CONFIGURATION_MODE_CREATOR_DESIGNER = 'creator+designer';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private string $identifier = '';

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $externalid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $configurable;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private bool $deactivated = false;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":1})
     */
    private ?int $minimum_order_amount;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":true})
     */
    private bool $accumulate_amounts = true;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $configuration_mode = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $call_to_action;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private bool $overwrite_component_order = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private bool $overrideOptionOrder = false;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private \DateTime $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_deleted_id;

    /**
     * @ORM\OneToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemsetting", mappedBy="item")
     */
    private $itemsetting;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, Itemtext>
     */
    private Collection $itemtext;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, Itemprice>
     */
    private Collection $itemprice;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, ItemAttribute>
     */
    private Collection $itemAttribute;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, ItemItemclassification>
     */
    private Collection $item_itemclassification;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, ItemOptionclassification>
     */
    private Collection $itemOptionclassification;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", mappedBy="parent")
     *
     * @var Collection<array-key, Item>
     */
    private Collection $item;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="item")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private ?Item $parent = null;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode", inversedBy="item")
     * @ORM\JoinColumn(name="visualization_mode_id", referencedColumnName="id")
     */
    private ?VisualizationMode $visualizationMode = null;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Stock",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, Stock>
     */
    private Collection $stock;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rule", mappedBy="item", cascade={"persist"})
     *
     * @var Collection<array-key, Rule>
     */
    private Collection $rule;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, ItemItemgroup>
     */
    private Collection $itemItemgroup;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, ItemItemgroupentry>
     */
    private Collection $itemItemgroupentry;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignArea>
     */
    private Collection $designArea;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignView",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignView>
     */
    private Collection $designView;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemConfigurationModeItemStatus",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     */
    private $itemConfigurationModeItemStatus;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus", inversedBy="item")
     * @ORM\JoinColumn(name="item_status_id", referencedColumnName="id")
     */
    private ?ItemStatus $itemStatus = null;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignerGlobalItemPrice>
     */
    private Collection $designerGlobalItemPrice;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemPool", mappedBy="item")
     *
     * @var Collection<array-key, ItemItemPool>
     */
    private Collection $itemItemPool;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView",
     *     mappedBy="item",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, CreatorView>
     */
    private Collection $creatorView;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->item = new ArrayCollection();
        $this->itemprice = new ArrayCollection();
        $this->itemtext = new ArrayCollection();
        $this->itemAttribute = new ArrayCollection();
        $this->itemOptionclassification = new ArrayCollection();
        $this->item_itemclassification = new ArrayCollection();
        $this->stock = new ArrayCollection();
        $this->designArea = new ArrayCollection();
        $this->designView = new ArrayCollection();
        $this->designerGlobalItemPrice = new ArrayCollection();
        $this->rule = new ArrayCollection();
        $this->itemItemgroup = new ArrayCollection();
        $this->itemItemgroupentry = new ArrayCollection();
        $this->itemItemPool = new ArrayCollection();
        $this->creatorView = new ArrayCollection();
        $this->date_created = new DateTime();
        $this->date_deleted = new DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function setExternalid(?string $externalid): self
    {
        $this->externalid = $externalid;

        return $this;
    }

    public function getExternalid(): ?string
    {
        return $this->externalid;
    }

    public function setConfigurable(?bool $configurable): self
    {
        $this->configurable = $configurable;

        return $this;
    }

    public function getConfigurable(): ?bool
    {
        return $this->configurable;
    }

    public function setDeactivated(bool $deactivated): self
    {
        $this->deactivated = $deactivated;

        return $this;
    }

    public function getDeactivated(): bool
    {
        return $this->deactivated;
    }

    public function setAccumulateAmounts(bool $accumulateAmounts): self
    {
        $this->accumulate_amounts = $accumulateAmounts;

        return $this;
    }

    public function getAccumulateAmounts(): bool
    {
        return $this->accumulate_amounts;
    }

    public function setDateCreated(DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    public function getDateCreated(): DateTime
    {
        return $this->date_created;
    }

    public function setDateUpdated(?DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    public function getDateUpdated(): ?DateTime
    {
        return $this->date_updated;
    }

    public function setDateDeleted(DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    public function getDateDeleted(): DateTime
    {
        return $this->date_deleted;
    }

    public function setUserCreatedId(?int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    public function setUserUpdatedId(?int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    public function setUserDeletedId(?int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    public function addItemtext(Itemtext $itemtext): self
    {
        $this->itemtext->add($itemtext);

        return $this;
    }

    /**
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeItemtext(Itemtext $itemtext): bool
    {
        return $this->itemtext->removeElement($itemtext);
    }

    public function clearItemtext(): self
    {
        foreach ($this->itemtext as $itemtext) {
            $itemtext->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, Itemtext>
     */
    public function getItemtext(): Collection
    {
        return $this->itemtext;
    }

    public function addItemAttribute(ItemAttribute $itemAttribute): self
    {
        $this->itemAttribute->add($itemAttribute);

        return $this;
    }

    /**
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeItemAttribute(ItemAttribute $itemAttribute): bool
    {
        return $this->itemAttribute->removeElement($itemAttribute);
    }

    public function clearItemAttribute(): self
    {
        foreach ($this->itemAttribute as $itemAttribute) {
            $itemAttribute->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, ItemAttribute>
     */
    public function getItemAttribute(): Collection
    {
        return $this->itemAttribute;
    }

    public function addStock(Stock $stock): self
    {
        $this->stock->add($stock);

        return $this;
    }

    /**
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeStock(Stock $stock): bool
    {
        return $this->stock->removeElement($stock);
    }

    public function clearStock(): self
    {
        foreach ($this->stock as $stock) {
            $stock->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, Stock>
     */
    public function getStock(): Collection
    {
        return $this->stock;
    }

    public function getCurrentChannelStock(): ?Stock
    {
        $stock = null;

        $stockentities = $this->getStock();

        if (!$stockentities->isEmpty()) {
            foreach ($stockentities as $stockentity) {
                if (C_CHANNEL == $stockentity->getChannel()->getIdentifier()) {
                    $stock = $stockentity;

                    break;
                }
            }
        }

        return $stock;
    }

    // TRANSLATION GETTERS

    /**
     * returns the translated title of the Item for the current Language.
     *
     * @return  string
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>     *
     */
    public function getTranslatedTitle()
    {
        $strTranslation = '{' . $this->getIdentifier() . '}';

        $arrTranslations = $this->getItemtext();

        /**
         * @var $objTranslation Itemtext
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getTitle()
            ) {
                $strTranslation = $objTranslation->getTitle();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * returns the translated abstract of the Item for the current Language.
     *
     * @return  string
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function getTranslatedAbstract()
    {
        $strTranslation = '';

        $arrTranslations = $this->getItemtext();

        /**
         * @var $objTranslation Itemtext
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getAbstract()
            ) {
                $strTranslation = $objTranslation->getAbstract();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * returns the translated description of the Item for the current Language.
     *
     * @return  string
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function getTranslatedDescription()
    {
        $strTranslation = '';

        $arrTranslations = $this->getItemtext();

        /**
         * @var $objTranslation Itemtext
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getDescription()
            ) {
                $strTranslation = $objTranslation->getDescription();

                break;
            }
        }

        return $strTranslation;
    }

    public function addItemprice(Itemprice $itemprice): self
    {
        $this->itemprice->add($itemprice);

        return $this;
    }

    public function removeItemprice(Itemprice $itemprice): void
    {
        $this->itemprice->removeElement($itemprice);
    }

    public function clearItemprice(): self
    {
        foreach ($this->itemprice as $itemprice) {
            $itemprice->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, Itemprice>
     */
    public function getItemprice(): Collection
    {
        return $this->itemprice;
    }

    /**
     * returns the price entity for the current channel.
     *
     * @return  Itemprice|null
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function getCurrentChannelItemprice(): ?Itemprice
    {
        $price = null;

        $priceentities = $this->getItemprice();

        if (!$priceentities->isEmpty()) {
            foreach ($priceentities as $priceentitiy) {
                if (C_CHANNEL == $priceentitiy->getChannel()->getIdentifier()) {
                    $price = $priceentitiy;

                    break;
                }
            }
        }

        return $price;
    }

    public function addItemItemclassification(ItemItemclassification $itemItemclassification): self
    {
        $this->item_itemclassification->add($itemItemclassification);

        return $this;
    }

    public function removeItemItemclassification(ItemItemclassification $itemItemclassification): bool
    {
        return $this->item_itemclassification->removeElement($itemItemclassification);
    }

    public function clearItemItemclassification(): self
    {
        foreach ($this->item_itemclassification as $itemItemclassification) {
            $itemItemclassification->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, ItemItemclassification>
     */
    public function getItemItemclassification(): Collection
    {
        return $this->item_itemclassification;
    }

    public function addItemOptionclassification(ItemOptionclassification $itemOptionClassification): self
    {
        $itemOptionClassification->setItem($this);

        $this->itemOptionclassification->add($itemOptionClassification);

        return $this;
    }

    public function removeItemOptionclassification(ItemOptionclassification $itemOptionClassification): void
    {
        $key = $this->itemOptionclassification->indexOf($itemOptionClassification);

        if (false === $key) {
            return;
        }

        /** @var ItemOptionclassification $element */
        $element = $this->itemOptionclassification->get($key);

        foreach ($element->getItemOptionClassificationOption() as $itemOptionClassificationOption) {
            $element->removeItemOptionclassificationOption($itemOptionClassificationOption);
        }

        $element->setDateDeleted(new DateTime());
    }

    public function clearItemOptionclassification(): self
    {
        foreach ($this->itemOptionclassification as $itemOptionclassification) {
            $itemOptionclassification->setDateDeleted(new DateTime());

            foreach ($itemOptionclassification->getItemOptionClassificationOption() as $itemOptionClassificationOption) {
                $itemOptionClassificationOption->setDateDeleted(new DateTime());
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ItemOptionclassification>
     */
    public function getItemOptionclassification(): Collection
    {
        return $this->itemOptionclassification;
    }

    public function addRule(Rule $rule): self
    {
        $this->rule->add($rule);

        return $this;
    }

    public function removeRule(Rule $rule): bool
    {
        return $this->rule->removeElement($rule);
    }

    public function clearRule(): self
    {
        foreach ($this->rule as $rule) {
            $rule->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, Rule>
     */
    public function getRule(): Collection
    {
        return $this->rule;
    }

    /**
     * get the items rules ordered by the sequence number of their rule type.
     *
     * @return  array
     */
    public function getSortedRules()
    {
        $itemRules = [];
        foreach ($this->rule as $rule) {
            if (null === $rule->getOption()) {
                $itemRules[] = $rule;
            }
        }

        usort($itemRules, function ($a, $b) {
            return $a->getRuletype()->getSequencenumber() > $b->getRuletype()->getSequencenumber();
        });

        return $itemRules;
    }

    public function getParent(): ?Item
    {
        return $this->parent;
    }

    public function getVisualizationMode(): ?VisualizationMode
    {
        return $this->visualizationMode;
    }

    public function setVisualizationMode(?VisualizationMode $visualizationMode): void
    {
        $this->visualizationMode = $visualizationMode;
    }

    public function addItemItemgroup(ItemItemgroup $itemItemgroup): self
    {
        $this->itemItemgroup->add($itemItemgroup);

        return $this;
    }

    public function removeItemItemgroup(ItemItemgroup $itemItemgroup): bool
    {
        return $this->itemItemgroup->removeElement($itemItemgroup);
    }

    public function clearItemItemgroup(): self
    {
        foreach ($this->itemItemgroup as $itemItemgroup) {
            $itemItemgroup->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, ItemItemgroup>
     */
    public function getItemItemgroup(): Collection
    {
        return $this->itemItemgroup;
    }

    public function addItemItemgroupentry(ItemItemgroupentry $itemItemgroupentry): self
    {
        $this->itemItemgroupentry->add($itemItemgroupentry);

        return $this;
    }

    public function removeItemItemgroupentry(ItemItemgroupentry $itemItemgroupentry): bool
    {
        return $this->itemItemgroupentry->removeElement($itemItemgroupentry);
    }

    public function clearItemItemgroupentry(): self
    {
        foreach ($this->itemItemgroupentry as $itemItemgroupentry) {
            $itemItemgroupentry->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, ItemItemgroupentry>
     */
    public function getItemItemgroupentry(): Collection
    {
        return $this->itemItemgroupentry;
    }

    public function setParent(?Item $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function addItem(Item $item): self
    {
        $this->item->add($item);

        return $this;
    }

    public function removeItem(Item $item): void
    {
        $this->item->removeElement($item);
    }

    /**
     * @return Collection<array-key, Item>
     */
    public function getItem(): Collection
    {
        return $this->item;
    }

    /**
     * @param ?ItemStatus $itemStatus
     */
    public function setItemStatus(?ItemStatus $itemStatus): void
    {
        $this->itemStatus = $itemStatus;
    }

    public function getItemStatus(): ?ItemStatus
    {
        return $this->itemStatus;
    }

    public function addDesignArea(DesignArea $designArea): Item
    {
        $this->designArea->add($designArea);

        return $this;
    }

    public function removeDesignArea(DesignArea $designArea): bool
    {
        return $this->designArea->removeElement($designArea);
    }

    public function clearDesignArea(): self
    {
        foreach ($this->designArea as $designArea) {
            $designArea->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, DesignArea>
     */
    public function getDesignArea(): Collection
    {
        return $this->designArea;
    }

    public function addDesignView(DesignView $designView): self
    {
        $this->designView->add($designView);

        return $this;
    }

    public function removeDesignView(DesignView $designView): bool
    {
        return $this->designView->removeElement($designView);
    }

    public function clearDesignView(): self
    {
        foreach ($this->designView as $designView) {
            $designView->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, DesignView>
     */
    public function getDesignView(): Collection
    {
        return $this->designView;
    }

    public function addCreatorView(CreatorView $creatorView): Item
    {
        $this->creatorView->add($creatorView);

        return $this;
    }

    public function removeCreatorView(CreatorView $creatorView): void
    {
        $key = $this->creatorView->indexOf($creatorView);

        if (false === $key) {
            return;
        }

        /** @var CreatorView $element */
        $element = $this->creatorView->get($key);

        $element->setDateDeleted(new \DateTime());
    }

    public function clearCreatorView(): self
    {
        foreach ($this->creatorView as $creatorView) {
            $creatorView->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, CreatorView>
     */
    public function getCreatorView(): Collection
    {
        return $this->creatorView;
    }

    public function addDesignerGlobalItemPrice(DesignerGlobalItemPrice $designerGlobalItemPrice): self
    {
        $this->designerGlobalItemPrice->add($designerGlobalItemPrice);

        return $this;
    }

    /**
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignerGlobalItemPrice(DesignerGlobalItemPrice $designerGlobalItemPrice): bool
    {
        return $this->designerGlobalItemPrice->removeElement($designerGlobalItemPrice);
    }

    public function clearDesignerGlobalItemPrice(): self
    {
        foreach ($this->designerGlobalItemPrice as $designerGlobalItemPrice) {
            $designerGlobalItemPrice->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, DesignerGlobalItemPrice>
     */
    public function getDesignerGlobalItemPrice(): Collection
    {
        return $this->designerGlobalItemPrice;
    }

    public function addItemItemPool(ItemItemPool $itemItemPool): self
    {
        $this->itemItemPool->add($itemItemPool);

        return $this;
    }

    /**
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeItemItemPool(ItemItemPool $itemItemPool): bool
    {
        return $this->itemItemPool->removeElement($itemItemPool);
    }

    public function clearItemItemPool(): self
    {
        foreach ($this->itemItemPool as $itemItemPool) {
            $itemItemPool->setDateDeleted(new DateTime());
        }

        return $this;
    }

    /**
     * @return Collection<array-key, ItemItemPool>
     */
    public function getItemItemPool(): Collection
    {
        return $this->itemItemPool;
    }

    public function getMinimumOrderAmount(): ?int
    {
        if ($this->item_itemclassification->isEmpty()) {
            return $this->minimum_order_amount;
        }

        $minimumOrderAmounts = $this->item_itemclassification
            ->map(static function (ItemItemclassification $itemItemClassification) {
                return null === $itemItemClassification->getItemclassification()
                    ? 1
                    : $itemItemClassification->getItemclassification()->getMinimumOrderAmount();
            });

        $maxMinimumOrderAmount = max($minimumOrderAmounts->getValues());
        $maxMinimumOrderAmount = max($this->minimum_order_amount, $maxMinimumOrderAmount);

        return false === $maxMinimumOrderAmount ? null : $maxMinimumOrderAmount;
    }

    public function setMinimumOrderAmount(?int $minimumOrderAmount): void
    {
        $this->minimum_order_amount = $minimumOrderAmount;
    }

    public function getConfigurationMode(): ?string
    {
        return $this->configuration_mode;
    }

    public function setConfigurationMode(?string $configurationMode): void
    {
        $this->configuration_mode = $configurationMode;
    }

    public function getCallToAction(): ?string
    {
        return $this->call_to_action;
    }

    public function setCallToAction(?string $callToAction): void
    {
        $this->call_to_action = $callToAction;
    }

    public function getOverwriteComponentOrder(): bool
    {
        return $this->overwrite_component_order;
    }

    public function setOverwriteComponentOrder(bool $overwriteComponentOrder): void
    {
        $this->overwrite_component_order = $overwriteComponentOrder;
    }

    public function getOverrideOptionOrder(): bool
    {
        return $this->overrideOptionOrder;
    }

    public function setOverrideOptionOrder(bool $overrideOptionOrder): self
    {
        $this->overrideOptionOrder = $overrideOptionOrder;

        return $this;
    }

    public function isDesigner(): bool
    {
        return self::CONFIGURATION_MODE_DESIGNER === $this->getConfigurationMode() || self::CONFIGURATION_MODE_CREATOR_DESIGNER === $this->getConfigurationMode();
    }
}
