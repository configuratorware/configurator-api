<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="color_text")
 */
class ColorText
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Color", inversedBy="colorText")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * Get Id.
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get Title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title.
     *
     * @param string title
     *
     * @return ColorText
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get Date Created.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set Date Created.
     *
     * @param \DateTime date_created
     *
     * @return ColorText
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get Date Updated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set Date Updated.
     *
     * @param \DateTime date_updated
     *
     * @return ColorText
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Get Date Deleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set Date Deleted.
     *
     * @param \DateTime date_deleted
     *
     * @return ColorText
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;

        return $this;
    }

    /**
     * Get User Created Id.
     *
     * @return User
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set User Created Id.
     *
     * @param User user_created_id
     *
     * @return Colortext
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;

        return $this;
    }

    /**
     * Get User Updated Id.
     *
     * @return User
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set User Updated Id.
     *
     * @param User user_updated_id
     *
     * @return ColorText
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;

        return $this;
    }

    /**
     * Get User Deleted Id.
     *
     * @return User
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set User Deleted Id.
     *
     * @param User user_deleted_id
     *
     * @return ColorText
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;

        return $this;
    }

    /**
     * Get Color.
     *
     * @return Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add Color.
     *
     * @param Color $color
     *
     * @return ColorText
     */
    public function setColor(Color $color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get Language.
     *
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set Language.
     *
     * @param Language language
     *
     * @return ColorText
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }
}
