<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointRepository")
 * @ORM\Table(name="assemblypoint")
 */
class Assemblypoint
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    private $parent_option_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $x;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $y;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rotation;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypointimageelement",
     *     inversedBy="assemblypoint"
     * )
     * @ORM\JoinColumn(name="assemblypointimageelement_id", referencedColumnName="id")
     */
    private $assemblypointimageelement;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option", inversedBy="assemblypoint")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set x.
     *
     * @param int $x
     *
     * @return Assemblypoint
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x.
     *
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y.
     *
     * @param int $y
     *
     * @return Assemblypoint
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y.
     *
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set rotation.
     *
     * @param int $rotation
     *
     * @return Assemblypoint
     */
    public function setRotation($rotation)
    {
        $this->rotation = $rotation;

        return $this;
    }

    /**
     * Get rotation.
     *
     * @return int
     */
    public function getRotation()
    {
        return $this->rotation;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Assemblypoint
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Assemblypoint
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Assemblypoint
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Assemblypoint
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Assemblypoint
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Assemblypoint
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set option.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option
     *
     * @return Assemblypoint
     */
    public function setOption(Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Set assemblypointimageelement.
     *
     * @param Assemblypointimageelement $assemblypointimageelement
     *
     * @return Assemblypoint
     */
    public function setAssemblypointimageelement(Assemblypointimageelement $assemblypointimageelement = null)
    {
        $this->assemblypointimageelement = $assemblypointimageelement;

        return $this;
    }

    /**
     * Get assemblypointimageelement.
     *
     * @return Assemblypointimageelement
     */
    public function getAssemblypointimageelement()
    {
        return $this->assemblypointimageelement;
    }

    /**
     * Set parentOptionId.
     *
     * @param int $parentOptionId
     *
     * @return Assemblypoint
     */
    public function setParentOptionId($parentOptionId)
    {
        $this->parent_option_id = $parentOptionId;

        return $this;
    }

    /**
     * Get parentOptionId.
     *
     * @return int
     */
    public function getParentOptionId()
    {
        return $this->parent_option_id;
    }
}
