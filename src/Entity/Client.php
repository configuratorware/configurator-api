<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidClientIdentifierException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository")
 * @ORM\Table(name="client")
 */
class Client
{
    public const DEFAULT_IDENTIFIER = '_default';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $theme;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $contact_name;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $contact_street;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $contact_post_code;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $contact_city;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $contact_phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $contact_email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $custom_css;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $custom_data;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $call_to_action;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pdf_header_html;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pdf_footer_html;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $from_email_address;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $to_email_addresses;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cc_email_addresses;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $bcc_email_addresses;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pdf_header_text;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pdf_footer_text;

    /**
     * @var bool|null
     *
     * @ORM\Column(type="boolean", nullable=true, options={"default": 0}) bool|null
     */
    private $send_offer_request_mail_to_customer;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage", mappedBy="client")
     */
    private $imageGalleryImage;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel",
     *     mappedBy="client",
     *     cascade={"persist"}
     * )
     *
     * @var ArrayCollection
     */
    private $clientChannel;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser",
     *     mappedBy="client",
     *     cascade={"persist"}
     * )
     */
    private $clientUser;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ClientText",
     *     mappedBy="client",
     *     cascade={"persist"}
     * )
     */
    private $clientText;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime(EntityListener::DATE_DELETED_DEFAULT);
        $this->clientUser = new ArrayCollection();
        $this->clientChannel = new ArrayCollection();
        $this->clientText = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string|null $identifier
     *
     * @return Client
     *
     * @throws InvalidClientIdentifierException
     */
    public function setIdentifier($identifier = null)
    {
        if (ClientRepository::DEFAULT_CLIENT_IDENTIFIER === $this->identifier && ClientRepository::DEFAULT_CLIENT_IDENTIFIER !== $identifier) {
            throw InvalidClientIdentifierException::mustBeDefault();
        }
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string|null
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string|null $email
     *
     * @return self
     *
     * @deprecated use setFromEmailAddress() instead
     */
    public function setEmail(?string $email): self
    {
        $this->setToEmailAddresses($email);

        return $this;
    }

    /**
     * @deprecated use getFromEmailAddress() instead
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->getToEmailAddresses();
    }

    /**
     * @param bool|null $value
     *
     * @return $this
     */
    public function setSendOfferRequestMailToCustomer($value = null)
    {
        $this->send_offer_request_mail_to_customer = $value;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSendOfferRequestMailToCustomer(): bool
    {
        return (bool)$this->send_offer_request_mail_to_customer;
    }

    /**
     * Set theme.
     *
     * @param \stdClass $theme
     *
     * @return Client
     */
    public function setTheme($theme = null)
    {
        if ($theme && !is_string($theme)) {
            $theme = json_encode($theme);
        }

        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme.
     *
     * @return \stdClass
     */
    public function getTheme()
    {
        return json_decode($this->theme);
    }

    /**
     * @return string|null
     */
    public function getContactName(): ?string
    {
        return $this->contact_name;
    }

    /**
     * @param string|null $contact_name
     *
     * @return self
     */
    public function setContactName(?string $contact_name): self
    {
        $this->contact_name = $contact_name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactStreet(): ?string
    {
        return $this->contact_street;
    }

    /**
     * @param string|null $contact_street
     *
     * @return self
     */
    public function setContactStreet(?string $contact_street): self
    {
        $this->contact_street = $contact_street;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactPostCode(): ?string
    {
        return $this->contact_post_code;
    }

    /**
     * @param string|null $contact_post_code
     *
     * @return self
     */
    public function setContactPostCode(?string $contact_post_code): self
    {
        $this->contact_post_code = $contact_post_code;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactCity(): ?string
    {
        return $this->contact_city;
    }

    /**
     * @param string|null $contact_city
     *
     * @return self
     */
    public function setContactCity(?string $contact_city): self
    {
        $this->contact_city = $contact_city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactPhone(): ?string
    {
        return $this->contact_phone;
    }

    /**
     * @param string|null $contact_phone
     *
     * @return self
     */
    public function setContactPhone(?string $contact_phone): self
    {
        $this->contact_phone = $contact_phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContactEmail(): ?string
    {
        return $this->contact_email;
    }

    /**
     * @param string|null $contact_email
     *
     * @return self
     */
    public function setContactEmail(?string $contact_email): self
    {
        $this->contact_email = $contact_email;

        return $this;
    }

    /**
     * @param string|null $customCss
     *
     * @return Client
     */
    public function setCustomCss(?string $customCss): self
    {
        $this->custom_css = $customCss;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomCss(): ?string
    {
        return $this->custom_css;
    }

    /**
     * @return \stdClass|null
     */
    public function getCustomData(): ?\stdClass
    {
        $customData = null;

        if (null !== $this->custom_data) {
            $customData = json_decode($this->custom_data, false);
        }

        return $customData;
    }

    /**
     * @param object|array|string|null $customData
     */
    public function setCustomData($customData): void
    {
        if (null !== $customData && !is_string($customData)) {
            $customData = json_encode($customData);
        }

        $this->custom_data = $customData;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Client
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return Client
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Client
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return Client
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return Client
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return Client
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add imageGalleryImage.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage $imageGalleryImage
     *
     * @return Client
     */
    public function addImageGalleryImage(ImageGalleryImage $imageGalleryImage)
    {
        $this->imageGalleryImage[] = $imageGalleryImage;

        return $this;
    }

    /**
     * Remove imageGalleryImage.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage $imageGalleryImage
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeImageGalleryImage(
        ImageGalleryImage $imageGalleryImage
    ) {
        return $this->imageGalleryImage->removeElement($imageGalleryImage);
    }

    /**
     * Get imageGalleryImage.
     *
     * @return Collection
     */
    public function getImageGalleryImage()
    {
        return $this->imageGalleryImage;
    }

    /**
     * Add clientChannel.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel $clientChannel
     *
     * @return Client
     */
    public function addClientChannel(ClientChannel $clientChannel)
    {
        $this->clientChannel[] = $clientChannel;

        return $this;
    }

    /**
     * Remove clientChannel.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel $clientChannel
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeClientChannel(ClientChannel $clientChannel)
    {
        return $this->clientChannel->removeElement($clientChannel);
    }

    /**
     * Get clientChannel.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getClientChannel()
    {
        return $this->clientChannel;
    }

    /**
     * Add clientUser.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser $clientUser
     *
     * @return Client
     */
    public function addClientUser(ClientUser $clientUser)
    {
        $this->clientUser[] = $clientUser;

        return $this;
    }

    /**
     * Remove clientUser.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser $clientUser
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeClientUser(ClientUser $clientUser)
    {
        return $this->clientUser->removeElement($clientUser);
    }

    /**
     * Get clientUser.
     *
     * @return Collection|ClientUser[]
     */
    public function getClientUser()
    {
        return $this->clientUser;
    }

    /**
     * @param ClientText $clientText
     *
     * @return Client
     */
    public function addClientText(ClientText $clientText): self
    {
        $this->clientText[] = $clientText;

        return $this;
    }

    /**
     * @param ClientText $clientText
     *
     * @return bool
     */
    public function removeClientText(ClientText $clientText): bool
    {
        return $this->clientText->removeElement($clientText);
    }

    /**
     * @return Collection
     */
    public function getClientText(): Collection
    {
        return $this->clientText;
    }

    /**
     * @return string|null
     */
    public function getCallToAction(): ?string
    {
        return $this->call_to_action;
    }

    /**
     * @param string|null $callToAction
     *
     * @return self
     */
    public function setCallToAction(?string $callToAction): self
    {
        $this->call_to_action = $callToAction;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFromEmailAddress(): ?string
    {
        return $this->from_email_address;
    }

    /**
     * @param string|null $from_email_address
     *
     * @return self
     */
    public function setFromEmailAddress(?string $from_email_address): self
    {
        $this->from_email_address = $from_email_address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getToEmailAddresses(): ?string
    {
        return $this->to_email_addresses;
    }

    /**
     * @param string|null $to_email_addresses
     *
     * @return self
     */
    public function setToEmailAddresses(?string $to_email_addresses): self
    {
        $this->to_email_addresses = $to_email_addresses;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCcEmailAddresses(): ?string
    {
        return $this->cc_email_addresses;
    }

    /**
     * @param string|null $cc_email_addresses
     *
     * @return self
     */
    public function setCcEmailAddresses(?string $cc_email_addresses): self
    {
        $this->cc_email_addresses = $cc_email_addresses;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBccEmailAddresses(): ?string
    {
        return $this->bcc_email_addresses;
    }

    /**
     * @param string|null $bcc_email_addresses
     *
     * @return self
     */
    public function setBccEmailAddresses(?string $bcc_email_addresses): self
    {
        $this->bcc_email_addresses = $bcc_email_addresses;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdfHeaderHtml(): ?string
    {
        return $this->pdf_header_html;
    }

    /**
     * @param string|null $pdfHeaderHtml
     *
     * @return self
     */
    public function setPdfHeaderHtml(?string $pdfHeaderHtml): self
    {
        $this->pdf_header_html = $pdfHeaderHtml;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdfFooterHtml(): ?string
    {
        return $this->pdf_footer_html;
    }

    /**
     * @param string|null $pdfFooterHtml
     *
     * @return self
     */
    public function setPdfFooterHtml(?string $pdfFooterHtml): self
    {
        $this->pdf_footer_html = $pdfFooterHtml;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdfHeaderText(): ?string
    {
        return $this->pdf_header_text;
    }

    /**
     * @param string|null $pdfHeaderText
     *
     * @return self
     */
    public function setPdfHeaderText(?string $pdfHeaderText): self
    {
        $this->pdf_header_text = $pdfHeaderText;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPdfFooterText(): ?string
    {
        return $this->pdf_footer_text;
    }

    /**
     * @param string|null $pdfFooterText
     *
     * @return self
     */
    public function setPdfFooterText(?string $pdfFooterText): self
    {
        $this->pdf_footer_text = $pdfFooterText;

        return $this;
    }
}
