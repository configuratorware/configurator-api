<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\OptionAttributeRepository")
 * @ORM\Table(
 *     name="option_attribute",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","option_id","attribute_id","attributevalue_id"})}
 * )
 */
class OptionAttribute
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $sequencenumber;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option", inversedBy="optionAttribute")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Attribute")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     */
    private $attribute;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue")
     * @ORM\JoinColumn(name="attributevalue_id", referencedColumnName="id")
     */
    private $attributevalue;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSequencenumber()
    {
        return $this->sequencenumber;
    }

    /**
     * @param mixed $sequencenumber
     */
    public function setSequencenumber($sequencenumber)
    {
        $this->sequencenumber = $sequencenumber;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param mixed $date_deleted
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;
    }

    /**
     * @return mixed
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * @param mixed $user_created_id
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;
    }

    /**
     * @return mixed
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * @param mixed $user_updated_id
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;
    }

    /**
     * @return mixed
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * @param mixed $user_deleted_id
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;
    }

    /**
     * @return mixed
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param mixed $option
     */
    public function setOption($option)
    {
        $this->option = $option;
    }

    /**
     * @return mixed
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @return mixed
     */
    public function getAttributevalue()
    {
        return $this->attributevalue;
    }

    /**
     * @param mixed $attributevalue
     */
    public function setAttributevalue($attributevalue)
    {
        $this->attributevalue = $attributevalue;
    }
}
