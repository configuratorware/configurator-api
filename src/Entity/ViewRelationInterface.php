<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

interface ViewRelationInterface
{
    public function getBaseShape();

    public function setBaseShape(?string $baseShape);

    public function getDesignArea(): DesignArea;

    public function getPosition();

    public function setPosition(?string $position);

    public function getView(): ?ViewInterface;
}
