<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\RulefeedbacktextRepository")
 * @ORM\Table(name="rulefeedbacktext")
 */
class Rulefeedbacktext
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktexttranslation",
     *     mappedBy="rulefeedbacktext"
     * )
     */
    private $rulefeedbacktexttranslation;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback",
     *     inversedBy="rulefeedbacktext"
     * )
     * @ORM\JoinColumn(name="rulefeedback_id", referencedColumnName="id")
     */
    private $rulefeedback;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype", inversedBy="rulefeedbacktext")
     * @ORM\JoinColumn(name="ruletype_id", referencedColumnName="id")
     */
    private $ruletype;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->rulefeedbacktexttranslation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Rulefeedbacktext
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Rulefeedbacktext
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Rulefeedbacktext
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Rulefeedbacktext
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Rulefeedbacktext
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Rulefeedbacktext
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Rulefeedbacktext
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add rulefeedbacktexttranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktexttranslation $rulefeedbacktexttranslation
     *
     * @return Rulefeedbacktext
     */
    public function addRulefeedbacktexttranslation(Rulefeedbacktexttranslation $rulefeedbacktexttranslation)
    {
        $this->rulefeedbacktexttranslation[] = $rulefeedbacktexttranslation;

        return $this;
    }

    /**
     * Remove rulefeedbacktexttranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktexttranslation $rulefeedbacktexttranslation
     */
    public function removeRulefeedbacktexttranslation(Rulefeedbacktexttranslation $rulefeedbacktexttranslation)
    {
        $this->rulefeedbacktexttranslation->removeElement($rulefeedbacktexttranslation);
    }

    /**
     * Get rulefeedbacktexttranslation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRulefeedbacktexttranslation()
    {
        return $this->rulefeedbacktexttranslation;
    }

    /**
     * returns the translated title of the Item for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>     *
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  string
     */
    public function getTranslatedRulefeedbacktext()
    {
        $strTranslation = '{' . $this->getIdentifier() . '}';

        $arrTranslations = $this->getRulefeedbacktexttranslation();

        /**
         * @var $objTranslation Rulefeedbacktexttranslation
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getTranslation()
            ) {
                $strTranslation = $objTranslation->getTranslation();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * Set rulefeedback.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback $rulefeedback
     *
     * @return Rulefeedbacktext
     */
    public function setRulefeedback(Rulefeedback $rulefeedback = null)
    {
        $this->rulefeedback = $rulefeedback;

        return $this;
    }

    /**
     * Get rulefeedback.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback
     */
    public function getRulefeedback()
    {
        return $this->rulefeedback;
    }

    /**
     * Set ruletype.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype $ruletype
     *
     * @return Rulefeedbacktext
     */
    public function setRuletype(Ruletype $ruletype = null)
    {
        $this->ruletype = $ruletype;

        return $this;
    }

    /**
     * Get ruletype.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype
     */
    public function getRuletype()
    {
        return $this->ruletype;
    }
}
