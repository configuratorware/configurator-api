<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository")
 * @ORM\Table(
 *     name="language",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_iso_date_deleted", columns={"iso","date_deleted"})}
 * )
 */
class Language
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $iso;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $dateformat;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $datetimeformat;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default":2})
     */
    private $pricedecimals = 2;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default":2})
     */
    private $numberdecimals;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $decimalpoint;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $thousandsseparator;

    /**
     * @ORM\Column(type="string", nullable=false, options={"default":"right"})
     */
    private $currencysymbolposition = 'right';

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $is_default;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgrouptranslation",
     *     mappedBy="language"
     * )
     */
    private $itemgrouptranslation;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentrytranslation",
     *     mappedBy="language"
     * )
     */
    private $itemgroupentrytranslation;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set iso.
     *
     * @param string $iso
     *
     * @return Language
     */
    public function setIso($iso)
    {
        $this->iso = $iso;

        return $this;
    }

    /**
     * Get iso.
     *
     * @return string
     */
    public function getIso()
    {
        return $this->iso;
    }

    /**
     * Set dateformat.
     *
     * @param string $dateformat
     *
     * @return Language
     */
    public function setDateformat($dateformat)
    {
        $this->dateformat = $dateformat;

        return $this;
    }

    /**
     * Get dateformat.
     *
     * @return string
     */
    public function getDateformat()
    {
        return $this->dateformat;
    }

    /**
     * Set datetimeformat.
     *
     * @param string $datetimeformat
     *
     * @return Language
     */
    public function setDatetimeformat($datetimeformat)
    {
        $this->datetimeformat = $datetimeformat;

        return $this;
    }

    /**
     * Get datetimeformat.
     *
     * @return string
     */
    public function getDatetimeformat()
    {
        return $this->datetimeformat;
    }

    /**
     * Set decimalpoint.
     *
     * @param string $decimalpoint
     *
     * @return Language
     */
    public function setDecimalpoint($decimalpoint)
    {
        $this->decimalpoint = $decimalpoint;

        return $this;
    }

    /**
     * Get decimalpoint.
     *
     * @return string
     */
    public function getDecimalpoint()
    {
        return $this->decimalpoint;
    }

    /**
     * Set thousandsseparator.
     *
     * @param string $thousandsseparator
     *
     * @return Language
     */
    public function setThousandsseparator($thousandsseparator)
    {
        $this->thousandsseparator = $thousandsseparator;

        return $this;
    }

    /**
     * Get thousandsseparator.
     *
     * @return string
     */
    public function getThousandsseparator()
    {
        return $this->thousandsseparator;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Language
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Language
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Language
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Language
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Language
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Language
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set pricedecimals.
     *
     * @param int $pricedecimals
     *
     * @return Language
     */
    public function setPricedecimals($pricedecimals)
    {
        $this->pricedecimals = $pricedecimals;

        return $this;
    }

    /**
     * Get pricedecimals.
     *
     * @return int
     */
    public function getPricedecimals()
    {
        return $this->pricedecimals;
    }

    /**
     * Set currencysymbolposition.
     *
     * @param string $currencysymbolposition
     *
     * @return Language
     */
    public function setCurrencysymbolposition($currencysymbolposition)
    {
        $this->currencysymbolposition = $currencysymbolposition;

        return $this;
    }

    /**
     * Get currencysymbolposition.
     *
     * @return string
     */
    public function getCurrencysymbolposition()
    {
        return $this->currencysymbolposition;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->itemgrouptranslation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->itemgroupentrytranslation = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add itemgrouptranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgrouptranslation $itemgrouptranslation
     *
     * @return Language
     */
    public function addItemgrouptranslation(
        Itemgrouptranslation $itemgrouptranslation
    ) {
        $this->itemgrouptranslation[] = $itemgrouptranslation;

        return $this;
    }

    /**
     * Remove itemgrouptranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgrouptranslation $itemgrouptranslation
     */
    public function removeItemgrouptranslation(
        Itemgrouptranslation $itemgrouptranslation
    ) {
        $this->itemgrouptranslation->removeElement($itemgrouptranslation);
    }

    /**
     * Get itemgrouptranslation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemgrouptranslation()
    {
        return $this->itemgrouptranslation;
    }

    /**
     * Add itemgroupentrytranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentrytranslation $itemgroupentrytranslation
     *
     * @return Language
     */
    public function addItemgroupentrytranslation(
        Itemgroupentrytranslation $itemgroupentrytranslation
    ) {
        $this->itemgroupentrytranslation[] = $itemgroupentrytranslation;

        return $this;
    }

    /**
     * Remove itemgroupentrytranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentrytranslation $itemgroupentrytranslation
     */
    public function removeItemgroupentrytranslation(
        Itemgroupentrytranslation $itemgroupentrytranslation
    ) {
        $this->itemgroupentrytranslation->removeElement($itemgroupentrytranslation);
    }

    /**
     * Get itemgroupentrytranslation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemgroupentrytranslation()
    {
        return $this->itemgroupentrytranslation;
    }

    /**
     * Set numberdecimals.
     *
     * @param int $numberdecimals
     *
     * @return Language
     */
    public function setNumberdecimals($numberdecimals)
    {
        $this->numberdecimals = $numberdecimals;

        return $this;
    }

    /**
     * Get numberdecimals.
     *
     * @return int
     */
    public function getNumberdecimals()
    {
        return $this->numberdecimals;
    }

    /**
     * @param bool $is_default
     */
    public function setIsDefault(bool $is_default): void
    {
        $this->is_default = $is_default;
    }

    /**
     * @return bool|null
     */
    public function getIsDefault(): bool
    {
        return $this->is_default;
    }
}
