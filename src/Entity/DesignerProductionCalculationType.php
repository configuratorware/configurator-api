<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesignerProductionCalculationType.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignerProductionCalculationTypeRepository")
 * @ORM\Table(
 *     name="designer_production_calculation_type",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"identifier","date_deleted"})}
 * )
 */
class DesignerProductionCalculationType
{
    /**
     * @var
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="string", nullable=true) string|null
     */
    private $identifier;

    /**
     * @var
     * @ORM\Column(type="boolean", nullable=true) bool|null
     */
    private $color_amount_dependent = false;

    /**
     * @var
     * @ORM\Column(type="boolean", nullable=true) bool|null
     */
    private $item_amount_dependent = false;

    /**
     * @var
     * @ORM\Column(type="boolean", nullable=true, options={"default":0}) bool|null
     */
    private $selectable_by_user = false;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $declare_separately = false;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $bulk_name_dependent = false;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":1})
     */
    private $price_per_item = true;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted = '0001-01-01 00:00:00';

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;

    /**
     * @var
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationTypeText",
     *     mappedBy="designerProductionCalculationType",
     *     cascade={"persist"}
     * ) \Doctrine\Common\Collections\Collection
     */
    private $designerProductionCalculationTypeText;

    /**
     * @var
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice",
     *     mappedBy="designerProductionCalculationType",
     *     cascade={"persist"}
     * ) \Doctrine\Common\Collections\Collection
     */
    private $designProductionMethodPrice;

    /**
     * @var
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice",
     *     mappedBy="designerProductionCalculationType"
     * ) \Doctrine\Common\Collections\Collection
     */
    private $designAreaDesignProductionMethodPrice;

    /**
     * @var
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod",
     *     inversedBy="designerProductionCalculationType"
     * )
     * @ORM\JoinColumn(name="design_production_method_id", referencedColumnName="id")
     *                                                     \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod
     */
    private $designProductionMethod;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->designerProductionCalculationTypeText = new \Doctrine\Common\Collections\ArrayCollection();
        $this->designProductionMethodPrice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->designAreaDesignProductionMethodPrice = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get identifier.
     *
     * @return string|null
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set identifier.
     *
     * @param string|null $identifier
     *
     * @return DesignerProductionCalculationType
     */
    public function setIdentifier($identifier = null): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get colorAmountDependent.
     *
     * @return bool
     */
    public function getColorAmountDependent()
    {
        return (bool)$this->color_amount_dependent;
    }

    /**
     * Set colorAmountDependent.
     *
     * @param bool|null $colorAmountDependent
     *
     * @return DesignerProductionCalculationType
     */
    public function setColorAmountDependent(?bool $colorAmountDependent): self
    {
        $this->color_amount_dependent = (bool)$colorAmountDependent;

        return $this;
    }

    /**
     * Get itemAmountDependent.
     *
     * @return bool
     */
    public function getItemAmountDependent()
    {
        return (bool)$this->item_amount_dependent;
    }

    /**
     * Set itemAmountDependent.
     *
     * @param bool|null $itemAmountDependent
     *
     * @return DesignerProductionCalculationType
     */
    public function setItemAmountDependent(?bool $itemAmountDependent): self
    {
        $this->item_amount_dependent = (bool)$itemAmountDependent;

        return $this;
    }

    /**
     * Get selectableByUser.
     *
     * @return bool
     */
    public function getSelectableByUser()
    {
        return (bool)$this->selectable_by_user;
    }

    /**
     * Set selectableByUser.
     *
     * @param bool|null $selectableByUser
     *
     * @return DesignerProductionCalculationType
     */
    public function setSelectableByUser(?bool $selectableByUser): self
    {
        $this->selectable_by_user = (bool)$selectableByUser;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDeclareSeparately(): bool
    {
        return (bool)$this->declare_separately;
    }

    /**
     * @param bool|null $declare_separately
     *
     * @return DesignerProductionCalculationType
     */
    public function setDeclareSeparately(?bool $declare_separately): self
    {
        $this->declare_separately = (bool)$declare_separately;

        return $this;
    }

    /**
     * @return bool
     */
    public function getBulkNameDependent(): bool
    {
        return (bool)$this->bulk_name_dependent;
    }

    /**
     * @param bool|null $bulkNameDependent
     *
     * @return DesignerProductionCalculationType
     */
    public function setBulkNameDependent(?bool $bulkNameDependent): self
    {
        $this->bulk_name_dependent = (bool)$bulkNameDependent;

        return $this;
    }

    /**
     * @return bool
     */
    public function getPricePerItem(): bool
    {
        return (bool)$this->price_per_item;
    }

    /**
     * @param bool|null $pricePerItem
     *
     * @return $this
     */
    public function setPricePerItem(?bool $pricePerItem): self
    {
        $this->price_per_item = $pricePerItem;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return DesignerProductionCalculationType
     */
    public function setDateCreated($dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return DesignerProductionCalculationType
     */
    public function setDateUpdated($dateUpdated = null): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return DesignerProductionCalculationType
     */
    public function setDateDeleted($dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignerProductionCalculationType
     */
    public function setUserCreatedId($userCreatedId = null): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignerProductionCalculationType
     */
    public function setUserUpdatedId($userUpdatedId = null): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignerProductionCalculationType
     */
    public function setUserDeletedId($userDeletedId = null): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Add designerProductionCalculationTypeText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationTypeText $designerProductionCalculationTypeText
     *
     * @return DesignerProductionCalculationType
     */
    public function addDesignerProductionCalculationTypeText(
        DesignerProductionCalculationTypeText $designerProductionCalculationTypeText
    ) {
        $this->designerProductionCalculationTypeText[] = $designerProductionCalculationTypeText;

        return $this;
    }

    /**
     * Remove designerProductionCalculationTypeText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationTypeText $designerProductionCalculationTypeText
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignerProductionCalculationTypeText(
        DesignerProductionCalculationTypeText $designerProductionCalculationTypeText
    ) {
        return $this->designerProductionCalculationTypeText->removeElement($designerProductionCalculationTypeText);
    }

    /**
     * Get designerProductionCalculationTypeText.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignerProductionCalculationTypeText()
    {
        return $this->designerProductionCalculationTypeText;
    }

    /**
     * Add designProductionMethodPrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice $designProductionMethodPrice
     *
     * @return DesignerProductionCalculationType
     */
    public function addDesignProductionMethodPrice(
        DesignProductionMethodPrice $designProductionMethodPrice
    ) {
        $this->designProductionMethodPrice[] = $designProductionMethodPrice;

        return $this;
    }

    /**
     * Remove designProductionMethodPrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice $designProductionMethodPrice
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignProductionMethodPrice(
        DesignProductionMethodPrice $designProductionMethodPrice
    ) {
        return $this->designProductionMethodPrice->removeElement($designProductionMethodPrice);
    }

    /**
     * Get designProductionMethodPrice.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignProductionMethodPrice()
    {
        return $this->designProductionMethodPrice;
    }

    /**
     * Add designAreaDesignProductionMethodPrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
     *
     * @return DesignerProductionCalculationType
     */
    public function addDesignAreaDesignProductionMethodPrice(
        DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
    ) {
        $this->designAreaDesignProductionMethodPrice[] = $designAreaDesignProductionMethodPrice;

        return $this;
    }

    /**
     * Remove designAreaDesignProductionMethodPrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignAreaDesignProductionMethodPrice(
        DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
    ) {
        return $this->designAreaDesignProductionMethodPrice->removeElement($designAreaDesignProductionMethodPrice);
    }

    /**
     * Get designAreaDesignProductionMethodPrice.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignAreaDesignProductionMethodPrice()
    {
        return $this->designAreaDesignProductionMethodPrice;
    }

    /**
     * Get designProductionMethod.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod|null
     */
    public function getDesignProductionMethod()
    {
        return $this->designProductionMethod;
    }

    /**
     * Set designProductionMethod.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod|null $designProductionMethod
     *
     * @return DesignerProductionCalculationType
     */
    public function setDesignProductionMethod(
        DesignProductionMethod $designProductionMethod = null
    ) {
        $this->designProductionMethod = $designProductionMethod;

        return $this;
    }

    /**
     * returns the translated title of the design production method for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle()
    {
        $title = '';

        $texts = $this->getDesignerProductionCalculationTypeText();

        /**
         * @var DesignProductionMethodText $text
         */
        foreach ($texts as $text) {
            if (C_LANGUAGE_ISO === $text->getLanguage()->getIso() && '' !== $text->getTitle()) {
                $title = $text->getTitle();

                break;
            }
        }

        return $title;
    }
}
