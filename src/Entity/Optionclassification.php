<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository")
 * @ORM\Table(
 *     name="optionclassification",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"identifier","date_deleted"})}
 * )
 */
class Optionclassification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":NULL})
     */
    private $parent_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":0})
     */
    private $sequencenumber;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     */
    private $hidden_in_frontend = false;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":false})
     */
    private $hidden_in_pdf = false;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification",
     *     mappedBy="optionclassification"
     * )
     */
    private $itemOptionclassification;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypointimageelement",
     *     mappedBy="optionclassification"
     * )
     */
    private $assemblypointimageelement;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationAttribute",
     *     mappedBy="optionclassification",
     *     cascade={"persist"}
     * )
     */
    private $optionClassificationAttribute;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationText",
     *     mappedBy="optionClassification",
     *     cascade={"persist"}
     * )
     */
    private $optionClassificationText;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->itemOptionclassification = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionClassificationText = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionClassificationAttribute = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Optionclassification
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Optionclassification
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Optionclassification
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Optionclassification
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Optionclassification
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Optionclassification
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add itemOptionclassification.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification $itemOptionclassification
     *
     * @return Optionclassification
     */
    public function addItemOptionclassification(
        ItemOptionclassification $itemOptionclassification
    ) {
        $this->itemOptionclassification[] = $itemOptionclassification;

        return $this;
    }

    /**
     * Remove itemOptionclassification.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification $itemOptionclassification
     */
    public function removeItemOptionclassification(
        ItemOptionclassification $itemOptionclassification
    ) {
        $this->itemOptionclassification->removeElement($itemOptionclassification);
    }

    /**
     * Get itemOptionclassification.
     *
     * @return Collection<int, ItemOptionclassification>
     */
    public function getItemOptionclassification()
    {
        return $this->itemOptionclassification;
    }

    /**
     * Add assemblypointimageelement.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypointimageelement $assemblypointimageelement
     *
     * @return Optionclassification
     */
    public function addAssemblypointimageelement(
        Assemblypointimageelement $assemblypointimageelement
    ) {
        $this->assemblypointimageelement[] = $assemblypointimageelement;

        return $this;
    }

    /**
     * Remove assemblypointimageelement.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypointimageelement $assemblypointimageelement
     */
    public function removeAssemblypointimageelement(
        Assemblypointimageelement $assemblypointimageelement
    ) {
        $this->assemblypointimageelement->removeElement($assemblypointimageelement);
    }

    /**
     * Get assemblypointimageelement.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssemblypointimageelement()
    {
        return $this->assemblypointimageelement;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return Optionclassification
     */
    public function setIdentifier($identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return int
     */
    public function getSequencenumber()
    {
        return $this->sequencenumber;
    }

    /**
     * @param int $sequencenumber
     */
    public function setSequencenumber($sequencenumber)
    {
        $this->sequencenumber = $sequencenumber;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionClassificationAttribute()
    {
        return $this->optionClassificationAttribute;
    }

    /**
     * Add optionClassificationAttribute.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemclassificationAttribute $optionClassificationAttribute
     *
     * @return Optionclassification
     */
    public function addOptionClassificationAttribute(
        OptionClassificationAttribute $optionClassificationAttribute
    ) {
        $this->optionClassificationAttribute[] = $optionClassificationAttribute;

        return $this;
    }

    /**
     * Remove itemclassificationAttribute.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationAttribute $itemclassificationAttribute
     */
    public function removeOptionClassificationAttribute(
        OptionClassificationAttribute $optionClassificationAttribute
    ) {
        $this->optionClassificationAttribute->removeElement($optionClassificationAttribute);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionClassificationText()
    {
        return $this->optionClassificationText;
    }

    /**
     * Add optionClassificationText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationText $optionClassificationText
     *
     * @return Optionclassification
     */
    public function addOptionClassificationText(
        OptionClassificationText $optionClassificationText
    ) {
        $this->optionClassificationText[] = $optionClassificationText;

        return $this;
    }

    /**
     * Remove optionClassificationText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationText $optionClassificationText
     */
    public function removeOptionClassificationText(
        OptionClassificationText $optionClassificationText
    ) {
        $this->optionClassificationText->removeElement($optionClassificationText);
    }

    /**
     * returns the translated title of the option classification for the current Language.
     *
     * @return  string
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function getTranslatedTitle($languageIso = null)
    {
        $language = 'en_GB';

        if (defined('C_LANGUAGE_ISO')) {
            $language = C_LANGUAGE_ISO;
        }

        if (null !== $languageIso) {
            $language = $languageIso;
        }

        $strTranslation = '{' . $this->getIdentifier() . '}';

        $arrTranslations = $this->getOptionClassificationText();

        /**
         * @var $objTranslation OptionClassificationText
         */
        foreach ($arrTranslations as $objTranslation) {
            if ($objTranslation->getLanguage()
                    ->getIso() == $language && '' != $objTranslation->getTitle()
            ) {
                $strTranslation = $objTranslation->getTitle();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * returns the translated description of the option classification for the current Language.
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @return string
     */
    public function getTranslatedDescription()
    {
        $strTranslation = '';

        $arrTranslations = $this->getOptionClassificationText();

        /** @var OptionClassificationText $objTranslation */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getDescription()
            ) {
                $strTranslation = $objTranslation->getDescription();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * @return bool
     */
    public function getHiddenInFrontend(): bool
    {
        return (bool)$this->hidden_in_frontend;
    }

    /**
     * @param bool $hidden_in_frontend
     */
    public function setHiddenInFrontend(bool $hidden_in_frontend): void
    {
        $this->hidden_in_frontend = $hidden_in_frontend;
    }

    /**
     * @return bool
     */
    public function getHiddenInPdf(): bool
    {
        return (bool)$this->hidden_in_pdf;
    }

    /**
     * @param bool $hidden_in_pdf
     */
    public function setHiddenInPdf(bool $hidden_in_pdf): void
    {
        $this->hidden_in_pdf = $hidden_in_pdf;
    }
}
