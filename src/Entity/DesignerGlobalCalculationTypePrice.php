<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesignerGlobalCalculationPrice.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypePriceRepository")
 * @ORM\Table(
 *     name="designer_global_calculation_type_price",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *             name="uq_default",
 *             columns={"channel_id","designer_global_calculation_type_id","date_deleted"}
 *         )}
 * )
 */
class DesignerGlobalCalculationTypePrice
{
    /**
     * @var
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5) string|null
     */
    private $price;

    /**
     * @var
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5) string|null
     */
    private $price_net;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted = '0001-01-01 00:00:00';

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\Channel
     */
    private $channel;

    /**
     * @var
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType",
     *     inversedBy="designerGlobalCalculationTypePrice"
     * )
     * @ORM\JoinColumn(name="designer_global_calculation_type_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType
     */
    private $designerGlobalCalculationType;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price.
     *
     * @param string|null $price
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set priceNet.
     *
     * @param string|null $priceNet
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setPriceNet($priceNet = null)
    {
        $this->price_net = $priceNet;

        return $this;
    }

    /**
     * Get priceNet.
     *
     * @return string|null
     */
    public function getPriceNet()
    {
        return $this->price_net;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set channel.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Channel|null $channel
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setChannel(Channel $channel = null)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Channel|null
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set designerGlobalCalculationType.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType|null $designerGlobalCalculationType
     *
     * @return DesignerGlobalCalculationTypePrice
     */
    public function setDesignerGlobalCalculationType(
        DesignerGlobalCalculationType $designerGlobalCalculationType = null
    ) {
        $this->designerGlobalCalculationType = $designerGlobalCalculationType;

        return $this;
    }

    /**
     * Get designerGlobalCalculationType.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType|null
     */
    public function getDesignerGlobalCalculationType()
    {
        return $this->designerGlobalCalculationType;
    }
}
