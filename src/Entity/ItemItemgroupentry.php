<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="item_itemgroupentry",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","item_id","itemgroupentry_id","itemgroup_id"})}
 * )
 */
class ItemItemgroupentry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="itemItemgroupentry")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry",
     *     inversedBy="itemItemgroupentry"
     * )
     * @ORM\JoinColumn(name="itemgroupentry_id", referencedColumnName="id")
     */
    private $itemgroupentry;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup",
     *     inversedBy="itemItemgroupentry"
     * )
     * @ORM\JoinColumn(name="itemgroup_id", referencedColumnName="id")
     */
    private $itemgroup;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return ItemItemgroupentry
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return ItemItemgroupentry
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return ItemItemgroupentry
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return ItemItemgroupentry
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return ItemItemgroupentry
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return ItemItemgroupentry
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set item.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     *
     * @return ItemItemgroupentry
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set itemgroupentry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry $itemgroupentry
     *
     * @return ItemItemgroupentry
     */
    public function setItemgroupentry(Itemgroupentry $itemgroupentry = null)
    {
        $this->itemgroupentry = $itemgroupentry;

        return $this;
    }

    /**
     * Get itemgroupentry.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry
     */
    public function getItemgroupentry()
    {
        return $this->itemgroupentry;
    }

    /**
     * Set itemgroup.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup $itemgroup
     *
     * @return ItemItemgroupentry
     */
    public function setItemgroup(Itemgroup $itemgroup = null)
    {
        $this->itemgroup = $itemgroup;

        return $this;
    }

    /**
     * Get itemgroup.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup
     */
    public function getItemgroup()
    {
        return $this->itemgroup;
    }
}
