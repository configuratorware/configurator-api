<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository")
 * @ORM\Table(name="rule")
 */
class Rule
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $data;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $action;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $orderable;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rule", mappedBy="reverseRule")
     */
    private $rule;

    /**
     * @var self|null
     *
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rule", inversedBy="rule")
     * @ORM\JoinColumn(name="reverse_rule_id", referencedColumnName="id")
     */
    private $reverseRule;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\RuleText",
     *     mappedBy="rule",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, RuleText>
     */
    private Collection $ruleText;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype", inversedBy="rule")
     * @ORM\JoinColumn(name="ruletype_id", referencedColumnName="id")
     */
    private $ruletype;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback", inversedBy="rule")
     * @ORM\JoinColumn(name="rulefeedback_id", referencedColumnName="id")
     */
    private $rulefeedback;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="rule")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option", inversedBy="rule")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->ruleText = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data.
     *
     * @param string $data
     *
     * @return Rule
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data.
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set action.
     *
     * @param string $action
     *
     * @return Rule
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set orderable.
     *
     * @param bool $orderable
     *
     * @return Rule
     */
    public function setOrderable($orderable)
    {
        $this->orderable = $orderable;

        return $this;
    }

    /**
     * Get orderable.
     *
     * @return bool
     */
    public function getOrderable()
    {
        return $this->orderable;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Rule
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Rule
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Rule
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Rule
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Rule
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Rule
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set ruletype.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype $ruletype
     *
     * @return Rule
     */
    public function setRuletype(Ruletype $ruletype = null)
    {
        $this->ruletype = $ruletype;

        return $this;
    }

    /**
     * Get ruletype.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype
     */
    public function getRuletype()
    {
        return $this->ruletype;
    }

    /**
     * Set rulefeedback.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback $rulefeedback
     *
     * @return Rule
     */
    public function setRulefeedback(Rulefeedback $rulefeedback = null)
    {
        $this->rulefeedback = $rulefeedback;

        return $this;
    }

    /**
     * Get rulefeedback.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback
     */
    public function getRulefeedback()
    {
        return $this->rulefeedback;
    }

    /**
     * Set item.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     *
     * @return Rule
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set option.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option
     *
     * @return Rule
     */
    public function setOption(Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Option
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * Add ruleText.
     *
     * @param RuleText $ruleText
     *
     * @return Rule
     */
    public function addRuleText(RuleText $ruleText): self
    {
        $this->ruleText->add($ruleText);

        return $this;
    }

    /**
     * Remove ruleText.
     *
     * @param RuleText $ruleText
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeRuleText(RuleText $ruleText): bool
    {
        return $this->ruleText->removeElement($ruleText);
    }

    /**
     * Get ruleText.
     *
     * @return Collection<array-key, RuleText>
     */
    public function getRuleText(): Collection
    {
        return $this->ruleText;
    }

    /**
     * returns the translated title of the rule type for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle()
    {
        $title = '';

        $ruleTexts = $this->getRuleText();

        /**
         * @var $ruleText RuleText
         */
        foreach ($ruleTexts as $ruleText) {
            if (C_LANGUAGE_ISO == $ruleText->getLanguage()
                    ->getIso() && '' != $ruleText->getTitle()
            ) {
                $title = $ruleText->getTitle();

                break;
            }
        }

        return $title;
    }

    /**
     * @return Rule|null
     */
    public function getReverseRule(): ?Rule
    {
        return $this->reverseRule;
    }

    /**
     * @param Rule $reverseRule
     *
     * @return self
     */
    public function setReverseRule(Rule $reverseRule): self
    {
        $this->reverseRule = $reverseRule;

        return $this;
    }
}
