<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository")
 * @ORM\Table(
 *     name="imagegalleryimage",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_efault", columns={"imagename","date_deleted"})}
 * )
 */
class ImageGalleryImage
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $imagename;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $thumbname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $printname;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $sequence_number;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageText",
     *     mappedBy="imageGalleryImage",
     *     cascade={"persist"}
     * )
     */
    private $imageGalleryImageText;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageTag",
     *     mappedBy="imageGalleryImage",
     *     cascade={"persist"}
     * )
     */
    private $imageGalleryImageTag;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Client", inversedBy="imageGalleryImage")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImagePrice",
     *     mappedBy="imageGalleryImage",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, ImageGalleryImagePrice>
     */
    private Collection $imageGalleryImagePrice;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->imageGalleryImageText = new ArrayCollection();
        $this->imageGalleryImageTag = new ArrayCollection();
        $this->imageGalleryImagePrice = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getImagename()
    {
        return $this->imagename;
    }

    /**
     * @param mixed $imagename
     */
    public function setImagename($imagename)
    {
        $this->imagename = $imagename;
    }

    /**
     * @return mixed
     */
    public function getThumbname()
    {
        return $this->thumbname;
    }

    /**
     * @param mixed $thumbname
     */
    public function setThumbname($thumbname)
    {
        $this->thumbname = $thumbname;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param mixed $date_deleted
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;
    }

    /**
     * @return mixed
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * @param mixed $user_created_id
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;
    }

    /**
     * @return mixed
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * @param mixed $user_updated_id
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;
    }

    /**
     * @return mixed
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * @param mixed $user_deleted_id
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;
    }

    /**
     * @return mixed
     */
    public function getImageGalleryImageText()
    {
        return $this->imageGalleryImageText;
    }

    /**
     * @param mixed $imageGalleryImageText
     */
    public function setImageGalleryImageText($imageGalleryImageText)
    {
        $this->imageGalleryImageText = $imageGalleryImageText;
    }

    /**
     * Add ImageGalleryImageText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageText $imageGalleryImageText
     *
     * @return ImageGalleryImage
     */
    public function addImageGalleryImageText(
        ImageGalleryImageText $imageGalleryImageText
    ) {
        $this->imageGalleryImageText[] = $imageGalleryImageText;

        return $this;
    }

    /**
     * Remove ImageGalleryImageText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageText $imageGalleryImageText
     */
    public function removeImageGalleryImageText(
        ImageGalleryImageText $imageGalleryImageText
    ) {
        $this->imageGalleryImageText->removeElement($imageGalleryImageText);
    }

    /**
     * @return mixed
     */
    public function getImageGalleryImageTag()
    {
        return $this->imageGalleryImageTag;
    }

    /**
     * @param mixed $imageGalleryImageTag
     */
    public function setImageGalleryImageTag($imageGalleryImageTag)
    {
        $this->imageGalleryImageTag = $imageGalleryImageTag;
    }

    /**
     * Add ImageGalleryImageText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageTag $imageGalleryImageTag
     *
     * @return ImageGalleryImage
     */
    public function addImageGalleryImageTag(
        ImageGalleryImageTag $imageGalleryImageTag
    ) {
        $this->imageGalleryImageTag[] = $imageGalleryImageTag;

        return $this;
    }

    /**
     * Remove ImageGalleryImageText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageTag $imageGalleryImageTag
     */
    public function removeImageGalleryImageTag(
        ImageGalleryImageTag $imageGalleryImageTag
    ) {
        $this->imageGalleryImageTag->removeElement($imageGalleryImageTag);
    }

    /**
     * @return mixed
     */
    public function getPrintname()
    {
        return $this->printname;
    }

    /**
     * @param mixed $printname
     */
    public function setPrintname($printname)
    {
        $this->printname = $printname;
    }

    /**
     * returns the translated title of the ImageGalleryImage for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @return  string
     */
    public function getTranslatedTitle()
    {
        $strTranslation = '';

        $arrTranslations = $this->getImageGalleryImageText();

        /**
         * @var $objTranslation Itemtext
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getTitle()
            ) {
                $strTranslation = $objTranslation->getTitle();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * returns the translated description of the ImageGalleryImage for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @return  string
     */
    public function getTranslatedDescription()
    {
        $strTranslation = '';

        $arrTranslations = $this->getImageGalleryImageText();

        /**
         * @var $objTranslation Itemtext
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getDescription()
            ) {
                $strTranslation = $objTranslation->getDescription();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * Set client.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Client|null $client
     *
     * @return ImageGalleryImage
     */
    public function setClient(Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Client|null
     */
    public function getClient()
    {
        return $this->client;
    }

    public function getSequenceNumber(): ?int
    {
        return $this->sequence_number;
    }

    public function setSequenceNumber(int $sequence_number): self
    {
        $this->sequence_number = $sequence_number;

        return $this;
    }

    /**
     * @return Collection<int, ImageGalleryImagePrice>
     */
    public function getImageGalleryImagePrice(): Collection
    {
        return $this->imageGalleryImagePrice;
    }

    /**
     * @param Collection<int, ImageGalleryImagePrice> $imageGalleryImagePrice
     *
     * @return ImageGalleryImage
     */
    public function setImageGalleryImagePrice(Collection $imageGalleryImagePrice): self
    {
        $this->imageGalleryImagePrice = $imageGalleryImagePrice;

        return $this;
    }

    public function addImageGalleryImagePrice(
        ImageGalleryImagePrice $imageGalleryImagePrice
    ): self {
        $this->imageGalleryImagePrice[] = $imageGalleryImagePrice;

        return $this;
    }

    public function removeImageGalleryImagePrice(
        ImageGalleryImagePrice $imageGalleryImagePrice
    ): void {
        $this->imageGalleryImagePrice->removeElement($imageGalleryImagePrice);
    }

    public function getCurrentChannelImagePrice(): ?ImageGalleryImagePrice
    {
        $priceEntities = $this->getImageGalleryImagePrice();
        foreach ($priceEntities as $priceEntity) {
            if (C_CHANNEL == $priceEntity->getChannel()->getIdentifier()) {
                return $priceEntity;
            }
        }

        return null;
    }
}
