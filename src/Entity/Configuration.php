<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository")
 * @ORM\Table(
 *     name="configuration",
 *     indexes={@ORM\Index(name="ix_partslisthash", columns={"partslisthash"})},
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","user_created_id","code"}),
 *         @ORM\UniqueConstraint(name="uq_code", columns={"code"})
 *     }
 * )
 */
class Configuration
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $externaluser_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $selectedoptions;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $designdata;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $customdata;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $partslisthash;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="json", nullable=true)
     */
    private $calculationResult;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype",
     *     inversedBy="configuration"
     * )
     * @ORM\JoinColumn(name="configurationtype_id", referencedColumnName="id")
     */
    private $configurationtype;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var Channel|null
     *
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     */
    private $channel;

    /**
     * @var Language|null
     *
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $finderResult;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Configuration
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Configuration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDesigndata()
    {
        if (is_string($this->designdata)) {
            $designdata = json_decode($this->designdata);
        } else {
            $designdata = $this->designdata;
        }

        return $designdata;
    }

    /**
     * @param mixed $designdata
     *
     * @return self
     */
    public function setDesigndata($designdata): self
    {
        if ($designdata && !is_string($designdata)) {
            $designdata = json_encode($designdata);
        }

        $this->designdata = $designdata;

        return $this;
    }

    /**
     * Set customdata.
     *
     * @param array|\stdClass $customdata
     *
     * @return Configuration
     */
    public function setCustomdata($customdata)
    {
        if ($customdata && !is_string($customdata)) {
            $customdata = json_encode($customdata);
        }

        $this->customdata = $customdata;

        return $this;
    }

    /**
     * Get customdata.
     *
     * @return \stdClass
     */
    public function getCustomdata()
    {
        if (is_string($this->customdata)) {
            $customdata = json_decode($this->customdata);
        } else {
            $customdata = $this->customdata;
        }

        return $customdata;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Configuration
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Configuration
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Configuration
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Configuration
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Configuration
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Configuration
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set configurationtype.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype $configurationtype
     *
     * @return Configuration
     */
    public function setConfigurationtype(
        Configurationtype $configurationtype = null
    ) {
        $this->configurationtype = $configurationtype;

        return $this;
    }

    /**
     * Get configurationtype.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Configurationtype
     */
    public function getConfigurationtype()
    {
        return $this->configurationtype;
    }

    /**
     * Set item.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     *
     * @return Configuration
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set client.
     *
     * @param Client $client
     *
     * @return Configuration
     */
    public function setClient(Client $client = null): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client.
     *
     * @return Client|null
     */
    public function getClient(): ?Client
    {
        return $this->client;
    }

    /**
     * @return string|null
     */
    public function getClientIdentifier(): ?string
    {
        return null !== $this->client ? $this->client->getIdentifier() : null;
    }

    /**
     * Set selectedoptions.
     *
     * @param array $selectedoptions
     *
     * @return Configuration
     */
    public function setSelectedoptions($selectedoptions)
    {
        $this->selectedoptions = $selectedoptions;

        return $this;
    }

    /**
     * Get selectedoptions.
     *
     * @return array
     */
    public function getSelectedoptions()
    {
        return $this->selectedoptions;
    }

    /**
     * Set externaluserId.
     *
     * @param string $externaluserId
     *
     * @return Configuration
     */
    public function setExternaluserId($externaluserId)
    {
        $this->externaluser_id = $externaluserId;

        return $this;
    }

    /**
     * Get externaluserId.
     *
     * @return string
     */
    public function getExternaluserId()
    {
        return $this->externaluser_id;
    }

    /**
     * @return mixed
     */
    public function getPartslisthash()
    {
        return $this->partslisthash;
    }

    /**
     * @param mixed $partslisthash
     *
     * @return self
     */
    public function setPartslisthash($partslisthash): self
    {
        $this->partslisthash = $partslisthash;

        return $this;
    }

    /**
     * @return string
     */
    public function getCalculationResult(): ?string
    {
        return $this->calculationResult;
    }

    /**
     * @param string $calculationResult
     *
     * @return self
     */
    public function setCalculationResult(string $calculationResult): self
    {
        $this->calculationResult = $calculationResult;

        return $this;
    }

    /**
     * @return Channel|null
     */
    public function getChannel(): ?Channel
    {
        return $this->channel;
    }

    /**
     * @param Channel|null $channel
     *
     * @return self
     */
    public function setChannel(?Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return Language|null
     */
    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    /**
     * @param Language|null $language
     *
     * @return self
     */
    public function setLanguage(?Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @param string|null $finderResult
     *
     * @return Configuration
     */
    public function setFinderResult($finderResult): self
    {
        if ($finderResult && !is_string($finderResult)) {
            $finderResult = json_encode($finderResult);
        }

        $this->finderResult = $finderResult;

        return $this;
    }

    /**
     * @return \stdClass|null
     */
    public function getFinderResult()
    {
        if (is_string($this->finderResult)) {
            return json_decode($this->finderResult);
        } else {
            return $this->finderResult;
        }
    }
}
