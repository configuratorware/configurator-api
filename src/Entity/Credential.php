<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\CredentialRepository")
 * @ORM\Table(
 *     name="credential",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"area","date_deleted"})}
 * )
 */
class Credential
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $area;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential",
     *     mappedBy="credential",
     *     cascade={"persist"}
     * )
     */
    private $roleCredential;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CredentialText",
     *     mappedBy="credential",
     *     cascade={"persist"}
     * )
     */
    private $credentialText;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->roleCredential = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set area.
     *
     * @param string $area
     *
     * @return Credential
     */
    public function setArea($area)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area.
     *
     * @return string
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Credential
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Credential
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Credential
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Credential
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Credential
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Credential
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Credential
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add roleCredential.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential $roleCredential
     *
     * @return Credential
     */
    public function addRoleCredential(RoleCredential $roleCredential)
    {
        $this->roleCredential[] = $roleCredential;

        return $this;
    }

    /**
     * Remove roleCredential.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential $roleCredential
     */
    public function removeRoleCredential(RoleCredential $roleCredential)
    {
        $this->roleCredential->removeElement($roleCredential);
    }

    /**
     * Get roleCredential.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoleCredential()
    {
        return $this->roleCredential;
    }

    /**
     * Add credentialText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\CredentialText $credentialText
     *
     * @return Credential
     */
    public function addCredentialText(CredentialText $credentialText)
    {
        $this->credentialText[] = $credentialText;

        return $this;
    }

    /**
     * Remove credentialText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\CredentialText $credentialText
     */
    public function removeCredentialText(CredentialText $credentialText)
    {
        $this->credentialText->removeElement($credentialText);
    }

    /**
     * Get credentialText.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCredentialText()
    {
        return $this->credentialText;
    }

    /**
     * Returns translated title field.
     *
     * @param string $languageIso
     *
     * @return string
     */
    public function getTranslatedTitle($languageIso = null)
    {
        $language = C_LANGUAGE_ISO;
        if (!empty($languageIso)) {
            $language = $languageIso;
        }

        $title = $this->getIdentifier();

        $credentialTexts = $this->getCredentialText();

        if (!empty($credentialTexts)) {
            foreach ($credentialTexts as $credentialText) {
                if ($credentialText->getLanguage()->getIso() === $language) {
                    $title = $credentialText->getTitle();

                    break;
                }
            }
        }

        return $title;
    }
}
