<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="user_role",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"user_id","role_id","date_deleted"})}
 * )
 */
class UserRole
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @var int|null
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @var int|null
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @var int|null
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\User", inversedBy="userRole")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var Role|null
     *
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Role", inversedBy="userRole")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    private $role;

    /**
     * @param User|null $user
     * @param Role|null $role
     */
    public function __construct(?User $user = null, ?Role $role = null)
    {
        $this->user = $user;
        $this->role = $role;
    }

    /**
     * @param Role $role
     *
     * @return self
     */
    public static function forRole(Role $role): self
    {
        return new self(null, $role);
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime|null $dateCreated
     *
     * @return self
     */
    public function setDateCreated(?\DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime|null
     */
    public function getDateCreated(): ?\DateTime
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return self
     */
    public function setDateUpdated(?\DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated(): ?\DateTime
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime|null $dateDeleted
     *
     * @return self
     */
    public function setDateDeleted(?\DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime|null
     */
    public function getDateDeleted(): ?\DateTime
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return self
     */
    public function setUserCreatedId(int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return self
     */
    public function setUserUpdatedId(int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return UserRole
     */
    public function setUserDeletedId(int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    /**
     * Set user.
     *
     * @param User|null $user
     *
     * @return UserRole
     */
    public function setUser(User $user = null): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set role.
     *
     * @param Role|null $role
     *
     * @return UserRole
     */
    public function setRole(Role $role = null): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role.
     *
     * @return Role|null
     */
    public function getRole(): ?Role
    {
        return $this->role;
    }
}
