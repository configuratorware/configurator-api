<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository")
 * @ORM\Table(
 *     name="`option`",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"identifier","date_deleted"})}
 * )
 */
class Option
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":0})
     */
    private $sequencenumber;

    private $parent_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $deactivated;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $hasTextinput = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $inputValidationType = null;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption",
     *     mappedBy="option"
     * )
     */
    private $itemOptionclassificationOption;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint", mappedBy="option")
     */
    private $assemblypoint;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rule", mappedBy="option")
     */
    private $rule;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionText",
     *     mappedBy="option",
     *     cascade={"persist"}
     * )
     */
    private $optionText;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute",
     *     mappedBy="option",
     *     cascade={"persist"}
     * )
     */
    private $optionAttribute;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionPrice",
     *     mappedBy="option",
     *     cascade={"persist"}
     * )
     */
    private $optionPrice;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroup", mappedBy="option")
     */
    private $optionGroup;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroupEntry", mappedBy="option")
     */
    private $optionGroupEntry;

    private $optionOptionClassification;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionStock",
     *     mappedBy="option",
     *     cascade={"persist"}
     * )
     */
    private $optionStock;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionOptionPool", mappedBy="option")
     */
    private $optionOptionPool;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option", inversedBy="option")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option", mappedBy="parent")
     */
    private $option;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->optionAttribute = new ArrayCollection();
        $this->optionText = new ArrayCollection();
        $this->optionStock = new ArrayCollection();
        $this->optionPrice = new ArrayCollection();
        $this->optionGroup = new ArrayCollection();
        $this->optionGroupEntry = new ArrayCollection();
        $this->rule = new ArrayCollection();
        $this->itemOptionclassificationOption = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Option
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Option
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Option
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Option
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Option
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Option
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    public function addItemOptionclassificationOption(ItemOptionclassificationOption $itemOptionclassificationOption): Option
    {
        $this->itemOptionclassificationOption->add($itemOptionclassificationOption);

        return $this;
    }

    public function removeItemOptionclassificationOption(ItemOptionclassificationOption $itemOptionclassificationOption)
    {
        $this->itemOptionclassificationOption->removeElement($itemOptionclassificationOption);
    }

    /**
     * Get itemOptionclassificationOption.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemOptionclassificationOption()
    {
        return $this->itemOptionclassificationOption;
    }

    /**
     * Add assemblypoint.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint $assemblypoint
     *
     * @return Option
     */
    public function addAssemblypoint(Assemblypoint $assemblypoint)
    {
        $this->assemblypoint[] = $assemblypoint;

        return $this;
    }

    /**
     * Remove assemblypoint.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint $assemblypoint
     */
    public function removeAssemblypoint(Assemblypoint $assemblypoint)
    {
        $this->assemblypoint->removeElement($assemblypoint);
    }

    /**
     * Get assemblypoint.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssemblypoint()
    {
        return $this->assemblypoint;
    }

    public function addRule(Rule $rule): Option
    {
        $this->rule->add($rule);

        return $this;
    }

    public function removeRule(Rule $rule)
    {
        $this->rule->removeElement($rule);
    }

    /**
     * Get rule.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRule()
    {
        return $this->rule;
    }

    /**
     * get the items rules ordered by the sequence number of their rule type.
     *
     * @param string $itemIdentifier
     *
     * @return  array
     */
    public function getSortedRules($itemIdentifier)
    {
        $optionRules = [];
        foreach ($this->rule as $rule) {
            if (null === $rule->getItem() || $rule->getItem()->getIdentifier() === $itemIdentifier) {
                $optionRules[] = $rule;
            }
        }

        usort($optionRules, function ($a, $b) {
            return $a->getRuletype()->getSequencenumber() > $b->getRuletype()->getSequencenumber();
        });

        return $optionRules;
    }

    /**
     * Set sequencenumber.
     *
     * @param int $sequencenumber
     *
     * @return Option
     */
    public function setSequencenumber($sequencenumber)
    {
        $this->sequencenumber = $sequencenumber;

        return $this;
    }

    /**
     * Get sequencenumber.
     *
     * @return int
     */
    public function getSequencenumber()
    {
        return $this->sequencenumber;
    }

    /**
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     *
     * @return Option
     */
    public function setIdentifier($identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExternalid()
    {
        return $this->externalid;
    }

    /**
     * @param mixed $externalid
     */
    public function setExternalid($externalid)
    {
        $this->externalid = $externalid;
    }

    /**
     * @return mixed
     */
    public function getDeactivated()
    {
        return $this->deactivated;
    }

    /**
     * @param mixed $deactivated
     */
    public function setDeactivated($deactivated)
    {
        $this->deactivated = $deactivated;
    }

    /**
     * @return mixed
     */
    public function getOptionText()
    {
        return $this->optionText;
    }

    /**
     * @param mixed $optionText
     */
    public function setOptionText($optionText)
    {
        $this->optionText = $optionText;
    }

    public function addOptionText(OptionText $optionText): Option
    {
        $this->optionText->add($optionText);

        return $this;
    }

    /**
     * Remove option text.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionText $optionText
     */
    public function removeOptionText(OptionText $optionText)
    {
        $this->optionText->removeElement($optionText);
    }

    /**
     * @return mixed
     */
    public function getOptionAttribute()
    {
        return $this->optionAttribute;
    }

    /**
     * @param mixed $optionAttribute
     */
    public function setOptionAttribute($optionAttribute)
    {
        $this->optionAttribute = $optionAttribute;
    }

    /**
     * Remove opionAttribute.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute $optionAttribute
     *
     * @deprecated use removeOptionAttribute() instead
     */
    public function removeItemAttribute(OptionAttribute $optionAttribute)
    {
        $this->optionAttribute->removeElement($optionAttribute);
    }

    public function removeOptionAttribute(OptionAttribute $optionAttribute)
    {
        $this->optionAttribute->removeElement($optionAttribute);
    }

    public function addOptionAttribute(OptionAttribute $optionAttribute): Option
    {
        $this->optionAttribute->add($optionAttribute);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOptionPrice()
    {
        return $this->optionPrice;
    }

    /**
     * @param mixed $optionPrice
     */
    public function setOptionPrice($optionPrice)
    {
        $this->optionPrice = $optionPrice;
    }

    public function addOptionPrice(OptionPrice $optionPrice): Option
    {
        $this->optionPrice->add($optionPrice);

        return $this;
    }

    public function removeOptionPrice(OptionPrice $optionPrice)
    {
        $this->optionPrice->removeElement($optionPrice);
    }

    /**
     * @return mixed
     */
    public function getOptionGroup()
    {
        return $this->optionGroup;
    }

    /**
     * @param mixed $optionGroup
     */
    public function setOptionGroup($optionGroup)
    {
        $this->optionGroup = $optionGroup;
    }

    /**
     * @return mixed
     */
    public function getOptionGroupEntry()
    {
        return $this->optionGroupEntry;
    }

    /**
     * @param mixed $optionGroupEntry
     */
    public function setOptionGroupEntry($optionGroupEntry)
    {
        $this->optionGroupEntry = $optionGroupEntry;
    }

    /**
     * @return mixed
     */
    public function getOptionOptionClassification()
    {
        return $this->optionOptionClassification;
    }

    /**
     * @param mixed $optionOptionClassification
     */
    public function setOptionOptionClassification($optionOptionClassification)
    {
        $this->optionOptionClassification = $optionOptionClassification;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionStock()
    {
        return $this->optionStock;
    }

    /**
     * @param mixed $optionStock
     */
    public function setOptionStock($optionStock)
    {
        $this->optionStock = $optionStock;
    }

    public function addOptionStock(OptionStock $optionStock): Option
    {
        $this->optionStock->add($optionStock);

        return $this;
    }

    public function removeStock(OptionStock $optionStock)
    {
        $this->optionStock->removeElement($optionStock);
    }

    /**
     * returns the translated title of the option for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>     *
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  string
     */
    public function getTranslatedTitle($languageIso = null)
    {
        $language = 'en_GB';

        if (defined('C_LANGUAGE_ISO')) {
            $language = C_LANGUAGE_ISO;
        }

        if (null !== $languageIso) {
            $language = $languageIso;
        }

        $strTranslation = '{' . $this->getIdentifier() . '}';

        $arrTranslations = $this->getOptionText();

        /**
         * @var $objTranslation OptionText
         */
        foreach ($arrTranslations as $objTranslation) {
            if ($objTranslation->getLanguage()->getIso() == $language && '' != $objTranslation->getTitle()
            ) {
                $strTranslation = $objTranslation->getTitle();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * returns the translated abstract of the option for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  string
     */
    public function getTranslatedAbstract()
    {
        $strTranslation = '';

        $arrTranslations = $this->getOptionText();

        /**
         * @var $objTranslation OptionText
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getAbstract()
            ) {
                $strTranslation = $objTranslation->getAbstract();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * returns the translated description of the option for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  string
     */
    public function getTranslatedDescription()
    {
        $strTranslation = '';

        $arrTranslations = $this->getOptionText();

        /**
         * @var $objTranslation OptionText
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getDescription()
            ) {
                $strTranslation = $objTranslation->getDescription();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * returns the price entity for the current channel.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  Itemprice
     */
    public function getCurrentChannelPrice()
    {
        $price = null;

        $priceentities = $this->getOptionPrice();

        if (!empty($priceentities)) {
            foreach ($priceentities as $priceentitiy) {
                if (C_CHANNEL == $priceentitiy->getChannel()->getIdentifier()) {
                    $price = $priceentitiy;

                    break;
                }
            }
        }

        return $price;
    }

    /**
     * returns the stock entity for the current channel.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>     *
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  OptionStock
     */
    public function getCurrentChannelStock()
    {
        $stock = null;

        $stockentities = $this->getOptionStock();

        if (!empty($stockentities)) {
            foreach ($stockentities as $stockentity) {
                if (C_CHANNEL == $stockentity->getChannel()->getIdentifier()) {
                    $stock = $stockentity;

                    break;
                }
            }
        }

        return $stock;
    }

    public function getOptionOptionPool()
    {
        return $this->optionOptionPool;
    }

    /**
     * @param mixed $optionOptionPool
     */
    public function setOptionOptionPool($optionOptionPool)
    {
        $this->optionOptionPool = $optionOptionPool;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param mixed $option
     */
    public function setOption($option)
    {
        $this->option = $option;
    }

    /**
     * @return bool|null
     */
    public function getHasTextinput(): ?bool
    {
        return $this->hasTextinput;
    }

    /**
     * @param bool|null $hasTextinput
     */
    public function setHasTextinput(?bool $hasTextinput): void
    {
        $this->hasTextinput = $hasTextinput;
    }

    /**
     * @return string|null
     */
    public function getInputValidationType(): ?string
    {
        return $this->inputValidationType;
    }

    /**
     * @param string|null $inputValidationType
     */
    public function setInputValidationType(?string $inputValidationType): void
    {
        $this->inputValidationType = $inputValidationType;
    }
}
