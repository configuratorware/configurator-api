<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository")
 * @ORM\Table(
 *     name="itemclassification",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *             name="uq_parent_id_identifier_date_deleted",
 *             columns={"parent_id","date_deleted","identifier"}
 *         )}
 * )
 */
class Itemclassification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":0})
     */
    private $parent_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":0})
     */
    private $sequencenumber;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassificationtext",
     *     mappedBy="itemclassification",
     *     cascade={"persist"}
     * )
     */
    private $itemclassificationtext;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification",
     *     mappedBy="itemclassification"
     * )
     */
    private $item_itemclassification;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemclassificationAttribute",
     *     mappedBy="itemclassification",
     *     cascade={"persist"}
     * )
     */
    private $itemclassificationAttribute;

    /**
     * @var int|null
     * @ORM\Column(type="bigint", nullable=true, options={"default":null})
     */
    private $minimum_order_amount;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->itemclassificationAttribute = new ArrayCollection();
        $this->itemclassificationtext = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set parentId.
     *
     * @param int $parentId
     *
     * @return Itemclassification
     */
    public function setParentId(?int $parentId): self
    {
        $this->parent_id = $parentId;

        return $this;
    }

    /**
     * Get parentId.
     *
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Itemclassification
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * Set sequencenumber.
     *
     * @param int $sequencenumber
     *
     * @return Itemclassification
     */
    public function setSequencenumber(?int $sequencenumber): self
    {
        $this->sequencenumber = (int)$sequencenumber;

        return $this;
    }

    /**
     * Get sequencenumber.
     *
     * @return int|null
     */
    public function getSequencenumber(): ?int
    {
        return $this->sequencenumber;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Itemclassification
     */
    public function setDateCreated(\DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime|null
     */
    public function getDateCreated(): ?\DateTime
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Itemclassification
     */
    public function setDateUpdated(\DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated(): ?\DateTime
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Itemclassification
     */
    public function setDateDeleted(\DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime|null
     */
    public function getDateDeleted(): ?\DateTime
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Itemclassification
     */
    public function setUserCreatedId(int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Itemclassification
     */
    public function setUserUpdatedId(int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Itemclassification
     */
    public function setUserDeletedId(int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    /**
     * Add itemclassificationAttribute.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemclassificationAttribute $itemclassificationAttribute
     *
     * @return Itemclassification
     */
    public function addItemclassificationAttribute(
        ItemclassificationAttribute $itemclassificationAttribute
    ): Itemclassification {
        $this->itemclassificationAttribute[] = $itemclassificationAttribute;

        return $this;
    }

    /**
     * Remove itemclassificationAttribute.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemclassificationAttribute $itemclassificationAttribute
     */
    public function removeItemclassificationAttribute(
        ItemclassificationAttribute $itemclassificationAttribute
    ) {
        $this->itemclassificationAttribute->removeElement($itemclassificationAttribute);
    }

    /**
     * Get itemclassificationAttribute.
     *
     * @return Collection
     */
    public function getItemclassificationAttribute(): Collection
    {
        return $this->itemclassificationAttribute;
    }

    /**
     * Add itemclassificationtext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassificationtext $itemclassificationtext
     *
     * @return Itemclassification
     */
    public function addItemclassificationtext(
        Itemclassificationtext $itemclassificationtext
    ): Itemclassification {
        $this->itemclassificationtext[] = $itemclassificationtext;

        return $this;
    }

    /**
     * Remove itemclassificationtext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassificationtext $itemclassificationtext
     */
    public function removeItemclassificationtext(
        Itemclassificationtext $itemclassificationtext
    ) {
        $this->itemclassificationtext->removeElement($itemclassificationtext);
    }

    /**
     * Get itemclassificationtext.
     *
     * @return Collection
     */
    public function getItemclassificationtext(): Collection
    {
        return $this->itemclassificationtext;
    }

    /**
     * Add itemItemclassification.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification $itemItemclassification
     *
     * @return Itemclassification
     */
    public function addItemItemclassification(
        ItemItemclassification $itemItemclassification
    ) {
        $this->item_itemclassification[] = $itemItemclassification;

        return $this;
    }

    /**
     * Remove itemItemclassification.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification $itemItemclassification
     */
    public function removeItemItemclassification(
        ItemItemclassification $itemItemclassification
    ) {
        $this->item_itemclassification->removeElement($itemItemclassification);
    }

    /**
     * Get itemItemclassification.
     *
     * @return Collection
     */
    public function getItemItemclassification(): Collection
    {
        return $this->item_itemclassification;
    }

    /**
     * @return  string
     */
    public function getTranslatedTitle()
    {
        $strTranslation = '{' . $this->getIdentifier() . '}';

        $arrTranslations = $this->getItemclassificationtext();

        /**
         * @var $objTranslation Itemtext
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getTitle()
            ) {
                $strTranslation = $objTranslation->getTitle();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * @return  string
     */
    public function getTranslatedDescription()
    {
        $strTranslation = '';

        $arrTranslations = $this->getItemclassificationtext();

        /**
         * @var $objTranslation Itemtext
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getDescription()
            ) {
                $strTranslation = $objTranslation->getDescription();

                break;
            }
        }

        return $strTranslation;
    }

    /**
     * @return int|null
     */
    public function getMinimumOrderAmount(): ?int
    {
        return $this->minimum_order_amount;
    }

    /**
     * @param int|null $minimumOrderAmount
     */
    public function setMinimumOrderAmount(?int $minimumOrderAmount): self
    {
        $this->minimum_order_amount = $minimumOrderAmount;

        return $this;
    }
}
