<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * migration entity is only used for doctrine fixture loader to also reset the migration version.
 *
 * @ORM\Entity()
 * @ORM\Table(
 *     name="migration_versions"
 * )
 */
class MigrationVersions
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string", nullable=true)
     */
    private $version;

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }
}
