<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @deprecated will be removed in next version, replaced by item.visualization_mode
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemSettingRepository")
 * @ORM\Table(
 *     name="itemsetting",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","item_id"})}
 * )
 */
class Itemsetting
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $itemgroupdisplaytype;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $visualizationCreator;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $visualizationDesigner;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="itemsetting")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id", nullable=false, unique=true)
     */
    private $item;
}
