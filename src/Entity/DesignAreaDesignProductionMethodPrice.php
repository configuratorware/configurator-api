<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodPriceRepository")
 * @ORM\Table(
 *     name="design_area_design_production_method_price",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *             name="uq_default",
 *             columns={
 *                 "channel_id",
 *                 "design_area_design_production_method_id",
 *                 "designer_production_calculation_type_id",
 *                 "amount_from",
 *                 "color_amount_from",
 *                 "date_deleted"
 *             }
 *         )}
 * )
 */
class DesignAreaDesignProductionMethodPrice
{
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $amount_from;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $color_amount_from;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     */
    private $price_net;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     */
    private $channel;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType",
     *     inversedBy="designAreaDesignProductionMethodPrice"
     * )
     * @ORM\JoinColumn(name="designer_production_calculation_type_id", referencedColumnName="id")
     */
    private $designerProductionCalculationType;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod",
     *     inversedBy="designAreaDesignProductionMethodPrice"
     * )
     * @ORM\JoinColumn(name="design_area_design_production_method_id", referencedColumnName="id")
     */
    private $designAreaDesignProductionMethod;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amountFrom.
     *
     * @param int|null $amountFrom
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setAmountFrom($amountFrom = null)
    {
        $this->amount_from = $amountFrom;

        return $this;
    }

    /**
     * Get amountFrom.
     *
     * @return int|null
     */
    public function getAmountFrom()
    {
        return $this->amount_from;
    }

    /**
     * Set colorAmountFrom.
     *
     * @param int|null $colorAmountFrom
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setColorAmountFrom($colorAmountFrom = null)
    {
        $this->color_amount_from = $colorAmountFrom;

        return $this;
    }

    /**
     * Get colorAmountFrom.
     *
     * @return int|null
     */
    public function getColorAmountFrom()
    {
        return $this->color_amount_from;
    }

    /**
     * Set price.
     *
     * @param string|null $price
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setPrice($price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set priceNet.
     *
     * @param string|null $priceNet
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setPriceNet($priceNet = null)
    {
        $this->price_net = $priceNet;

        return $this;
    }

    /**
     * Get priceNet.
     *
     * @return string|null
     */
    public function getPriceNet()
    {
        return $this->price_net;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set channel.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Channel|null $channel
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setChannel(Channel $channel = null)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Channel|null
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set designerProductionCalculationType.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType|null $designerProductionCalculationType
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setDesignerProductionCalculationType(
        DesignerProductionCalculationType $designerProductionCalculationType = null
    ) {
        $this->designerProductionCalculationType = $designerProductionCalculationType;

        return $this;
    }

    /**
     * Get designerProductionCalculationType.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType|null
     */
    public function getDesignerProductionCalculationType()
    {
        return $this->designerProductionCalculationType;
    }

    /**
     * Set designAreaDesignProductionMethod.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod|null $designAreaDesignProductionMethod
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    public function setDesignAreaDesignProductionMethod(
        DesignAreaDesignProductionMethod $designAreaDesignProductionMethod = null
    ) {
        $this->designAreaDesignProductionMethod = $designAreaDesignProductionMethod;

        return $this;
    }

    /**
     * Get designAreaDesignProductionMethod.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod|null
     */
    public function getDesignAreaDesignProductionMethod()
    {
        return $this->designAreaDesignProductionMethod;
    }
}
