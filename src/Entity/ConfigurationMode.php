<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @deprecated will be removed in next version, replaced by item.configuration_mode = string
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationModeRepository")
 * @ORM\Table(
 *     name="configuration_mode",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_identifier_date_deleted", columns={"identifier","date_deleted"})}
 * )
 */
class ConfigurationMode
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true) string|null
     */
    private $identifier;

    /**
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted = '0001-01-01 00:00:00';

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;
}
