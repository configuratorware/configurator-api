<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * DesignView.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository")
 * @ORM\Table(name="design_view")
 */
class DesignView implements ViewInterface
{
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $identifier;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $sequence_number;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $custom_data;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @var DateTime
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime|null
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     *
     * @var DateTime
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea",
     *     mappedBy="designView",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignViewDesignArea>
     */
    private Collection $designViewDesignArea;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewText",
     *     mappedBy="designView",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignViewText>
     */
    private Collection $designViewText;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="designView")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     *
     * @var Item|null
     */
    private ?Item $item;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     *
     * @var Option|null
     */
    private ?Option $option;

    /**
     * Constructor.
     *
     * @param string|null $identifier
     * @param Item|null $item
     * @param int|null $sequenceNumber
     */
    public function __construct(string $identifier = null, Item $item = null, int $sequenceNumber = null)
    {
        $this->identifier = $identifier;
        $this->item = $item;
        $this->sequence_number = $sequenceNumber;
        $this->date_created = new DateTime();
        $this->date_deleted = new DateTime(EntityListener::DATE_DELETED_DEFAULT);
        $this->designViewText = new ArrayCollection();
        $this->designViewDesignArea = new ArrayCollection();
    }

    /**
     * @param CreatorView $creatorView
     * @param Item $item
     *
     * @return DesignView
     */
    public static function initFromCreatorView(CreatorView $creatorView, Item $item): DesignView
    {
        return new self($creatorView->getIdentifier(), $item, (int)$creatorView->getSequenceNumber());
    }

    /**
     * @param CreatorView $creatorView
     *
     * @return DesignView
     */
    public static function copyFromCreatorView(CreatorView $creatorView): DesignView
    {
        $designView = new self($creatorView->getIdentifier(), $creatorView->getItem(), (int)$creatorView->getSequenceNumber());

        foreach ($creatorView->getCreatorViewDesignArea() as $creatorViewArea) {
            $designView->addDesignViewDesignArea(DesignViewDesignArea::fromCreatorViewDesignArea($creatorViewArea, $designView, $creatorViewArea->getDesignArea()));
        }

        foreach ($creatorView->getCreatorViewText() as $text) {
            $designView->addDesignViewText(DesignViewText::fromCreatorViewText($text, $designView));
        }

        return $designView;
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string|null $identifier
     *
     * @return self
     */
    public function setIdentifier(?string $identifier = null): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * Set customData.
     *
     * @param string|null $customData
     *
     * @return DesignView
     */
    public function setCustomData($customData = null): DesignView
    {
        if ($customData && !is_string($customData)) {
            $customData = json_encode($customData);
        }

        $this->custom_data = $customData;

        return $this;
    }

    /**
     * Get customData.
     *
     * @return string|null
     */
    public function getCustomData()
    {
        $customData = $this->custom_data;

        if (null === $customData) {
            return null;
        }

        $decoded = json_decode($customData);
        if ($decoded) {
            $customData = $decoded;
        }

        return $customData;
    }

    /**
     * Set dateCreated.
     *
     * @param DateTime $dateCreated
     *
     * @return DesignView
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param DateTime|null $dateUpdated
     *
     * @return DesignView
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param DateTime $dateDeleted
     *
     * @return DesignView
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignView
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignView
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignView
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * @param DesignViewDesignArea $designViewDesignArea
     *
     * @return self
     */
    public function addDesignViewDesignArea(DesignViewDesignArea $designViewDesignArea): self
    {
        $this->designViewDesignArea->add($designViewDesignArea);

        return $this;
    }

    /**
     * @param DesignViewDesignArea $designViewDesignArea
     *
     * @return bool
     */
    public function removeDesignViewDesignArea(DesignViewDesignArea $designViewDesignArea): bool
    {
        return $this->designViewDesignArea->removeElement($designViewDesignArea);
    }

    /**
     * Get designViewDesignArea.
     *
     * @return Collection
     */
    public function getDesignViewDesignArea()
    {
        return $this->designViewDesignArea;
    }

    /**
     * Add designViewText.
     *
     * @param DesignViewText $designViewText
     *
     * @return self
     */
    public function addDesignViewText(DesignViewText $designViewText): self
    {
        $this->designViewText->add($designViewText);

        return $this;
    }

    /**
     * @param DesignViewText $designViewText
     *
     * @return bool
     */
    public function removeDesignViewText(DesignViewText $designViewText): bool
    {
        return $this->designViewText->removeElement($designViewText);
    }

    /**
     * Get designViewText.
     *
     * @return Collection
     */
    public function getDesignViewText()
    {
        return $this->designViewText;
    }

    /**
     * Set item.
     *
     * @param Item|null $item
     *
     * @return DesignView
     */
    public function setItem(Item $item = null): DesignView
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return Item|null
     */
    public function getItem(): ?Item
    {
        return $this->item;
    }

    /**
     * Set option.
     *
     * @param Option|null $option
     *
     * @return DesignView
     */
    public function setOption(Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option.
     *
     * @return Option|null
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @return mixed
     */
    public function getSequenceNumber()
    {
        return $this->sequence_number;
    }

    /**
     * @param mixed $sequence_number
     */
    public function setSequenceNumber($sequence_number)
    {
        $this->sequence_number = $sequence_number;
    }

    /**
     * returns the translated title for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle(): string
    {
        $title = '';

        $texts = $this->getDesignViewText();

        /**
         * @var DesignProductionMethodText $text
         */
        foreach ($texts as $text) {
            if ('' !== $text->getTitle() && C_LANGUAGE_ISO === $text->getLanguage()->getIso()) {
                $title = $text->getTitle();

                break;
            }
        }

        return $title;
    }
}
