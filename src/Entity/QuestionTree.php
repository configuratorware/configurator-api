<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Repository\QuestionTreeRepository;

/**
 * @ORM\Entity(repositoryClass=QuestionTreeRepository::class)
 * @ORM\Table(
 *     name="question_tree",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *          name="uq_identifier_date_deleted",
 *          columns={"identifier","date_deleted"}
 *     )}
 * )
 */
class QuestionTree
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $identifier;

    /**
     * @ORM\OneToMany(
     *     targetEntity=QuestionTreeData::class,
     *     mappedBy="questionTree",
     *     cascade={"persist"},
     *     orphanRemoval=true
     * )
     */
    private $data;

    /**
     * @ORM\OneToMany(
     *     targetEntity=QuestionTreeText::class,
     *     mappedBy="questionTree",
     *     cascade={"persist"}
     * )
     */
    private $texts;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    public function __construct()
    {
        $this->data = new ArrayCollection();
        $this->texts = new ArrayCollection();
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return Collection<int, QuestionTreeData>
     */
    public function getQuestionTreeData(): Collection
    {
        return $this->data;
    }

    public function addQuestionTreeData(QuestionTreeData $data): self
    {
        if (!$this->data->contains($data)) {
            $this->data[] = $data;
            $data->setQuestionTree($this);
        }

        return $this;
    }

    public function removeQuestionTreeData(QuestionTreeData $data): self
    {
        if ($this->data->removeElement($data)) {
            // set the owning side to null (unless already changed)
            if ($data->getQuestionTree() === $this) {
                $data->setQuestionTree(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, QuestionTreeText>
     */
    public function getQuestionTreeText(): Collection
    {
        return $this->texts;
    }

    public function addQuestionTreeText(QuestionTreeText $questionTreeText): self
    {
        if (!$this->texts->contains($questionTreeText)) {
            $this->texts[] = $questionTreeText;
            $questionTreeText->setQuestionTree($this);
        }

        return $this;
    }

    public function removeQuestionTreeText(QuestionTreeText $questionTreeText): self
    {
        if ($this->texts->removeElement($questionTreeText)) {
            // set the owning side to null (unless already changed)
            if ($questionTreeText->getQuestionTree() === $this) {
                $questionTreeText->setQuestionTree(null);
            }
        }

        return $this;
    }

    public function setDateCreated(\DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->date_created;
    }

    public function setDateUpdated(?\DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    public function getDateUpdated(): \DateTime
    {
        return $this->date_updated;
    }

    public function setDateDeleted(\DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    public function getDateDeleted(): \DateTime
    {
        return $this->date_deleted;
    }

    public function setUserCreatedId(?int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    public function setUserUpdatedId(?int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    public function setUserDeletedId(?int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }
}
