<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository")
 * @ORM\Table(
 *     name="font",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"name","date_deleted"})}
 * )
 */
class Font
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $active;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_default;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $file_name_regular;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $file_name_bold;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $file_name_italic;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $file_name_bold_italic;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $sequence_number;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }

    /**
     * @param mixed $is_default
     */
    public function setIsDefault($is_default): void
    {
        $this->is_default = $is_default;
    }

    /**
     * @return mixed
     */
    public function getFileNameRegular()
    {
        return $this->file_name_regular;
    }

    /**
     * @param mixed $file_name_regular
     */
    public function setFileNameRegular($file_name_regular)
    {
        $this->file_name_regular = $file_name_regular;
    }

    /**
     * @return mixed
     */
    public function getFileNameBold()
    {
        return $this->file_name_bold;
    }

    /**
     * @param mixed $file_name_bold
     */
    public function setFileNameBold($file_name_bold)
    {
        $this->file_name_bold = $file_name_bold;
    }

    /**
     * @return mixed
     */
    public function getFileNameItalic()
    {
        return $this->file_name_italic;
    }

    /**
     * @param mixed $file_name_italic
     */
    public function setFileNameItalic($file_name_italic)
    {
        $this->file_name_italic = $file_name_italic;
    }

    /**
     * @return mixed
     */
    public function getFileNameBoldItalic()
    {
        return $this->file_name_bold_italic;
    }

    /**
     * @param mixed $file_name_bold_italic
     */
    public function setFileNameBoldItalic($file_name_bold_italic)
    {
        $this->file_name_bold_italic = $file_name_bold_italic;
    }

    /**
     * @return mixed
     */
    public function getSequenceNumber()
    {
        return $this->sequence_number;
    }

    /**
     * @param mixed $sequence_number
     */
    public function setSequenceNumber($sequence_number)
    {
        $this->sequence_number = $sequence_number;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param mixed $date_deleted
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;
    }

    /**
     * @return mixed
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * @param mixed $user_created_id
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;
    }

    /**
     * @return mixed
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * @param mixed $user_updated_id
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;
    }

    /**
     * @return mixed
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * @param mixed $user_deleted_id
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;
    }
}
