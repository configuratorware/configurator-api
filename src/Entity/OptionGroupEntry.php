<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="option_group_entry",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","option_id","group_id","group_entry_id"})}
 * )
 */
class OptionGroupEntry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option", inversedBy="optionGroupEntry")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup", inversedBy="optionGroupEntry")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry",
     *     inversedBy="optionGroupEntry"
     * )
     * @ORM\JoinColumn(name="group_entry_id", referencedColumnName="id")
     */
    private $groupEntry;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param mixed $date_deleted
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;
    }

    /**
     * @return mixed
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * @param mixed $user_created_id
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;
    }

    /**
     * @return mixed
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * @param mixed $user_updated_id
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;
    }

    /**
     * @return mixed
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * @param mixed $user_deleted_id
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;
    }

    /**
     * @return mixed
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @param mixed $option
     */
    public function setOption($option)
    {
        $this->option = $option;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getGroupEntry()
    {
        return $this->groupEntry;
    }

    /**
     * @param mixed $groupEntry
     */
    public function setGroupEntry($groupEntry)
    {
        $this->groupEntry = $groupEntry;
    }
}
