<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="creator_view_text",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"creator_view_id","language_id","date_deleted"})}
 * )
 */
class CreatorViewText
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView",
     *     inversedBy="creatorViewText"
     * )
     * @ORM\JoinColumn(name="creator_view_id", referencedColumnName="id")
     */
    private $creatorView;

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @param string $title
     * @param CreatorView $creatorView
     * @param Language|null $language
     */
    private function __construct(string $title, CreatorView $creatorView, Language $language = null)
    {
        $this->title = $title;
        $this->language = $language;
        $this->creatorView = $creatorView;
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    /**
     * @param string $title
     * @param CreatorView $creatorView
     * @param Language $language
     *
     * @return CreatorViewText
     */
    public static function from(string $title, CreatorView $creatorView, Language $language): CreatorViewText
    {
        return new self($title, $creatorView, $language);
    }

    /**
     * @param DesignViewText $designViewText
     * @param CreatorView $creatorView
     *
     * @return CreatorViewText
     */
    public static function fromDesignViewText(DesignViewText $designViewText, CreatorView $creatorView): CreatorViewText
    {
        return new self($designViewText->getTitle() ?? '', $creatorView, $designViewText->getLanguage());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CreatorView
     */
    public function getCreatorView(): CreatorView
    {
        return $this->creatorView;
    }

    /**
     * @param CreatorView $creatorView
     */
    public function setCreatorView(CreatorView $creatorView): void
    {
        $this->creatorView = $creatorView;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }

    /**
     * @param Language|null $language
     */
    public function setLanguage(?Language $language = null): void
    {
        $this->language = $language;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @param \DateTime $dateCreated
     *
     * @return CreatorViewText
     */
    public function setDateCreated($dateCreated): CreatorViewText
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param \DateTime|null $dateUpdated
     *
     * @return CreatorViewText
     */
    public function setDateUpdated($dateUpdated = null): CreatorViewText
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param \DateTime $dateDeleted
     *
     * @return CreatorViewText
     */
    public function setDateDeleted($dateDeleted): CreatorViewText
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param int|null $userCreatedId
     *
     * @return CreatorViewText
     */
    public function setUserCreatedId($userCreatedId = null): CreatorViewText
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * @param int|null $userUpdatedId
     *
     * @return CreatorViewText
     */
    public function setUserUpdatedId($userUpdatedId = null): CreatorViewText
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * @param int|null $userDeletedId
     *
     * @return CreatorViewText
     */
    public function setUserDeletedId($userDeletedId = null): CreatorViewText
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }
}
