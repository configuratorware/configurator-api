<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @deprecated will be removed in next version
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemConfigurationModeItemStatusRepository")
 * @ORM\Table(
 *     name="item_configuration_mode_item_status",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *             name="uq_default",
 *             columns={"item_id","configuration_mode_id","item_status_id","date_deleted"}
 *         )}
 * )
 */
class ItemConfigurationModeItemStatus
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted = '0001-01-01 00:00:00';

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item",
     *     inversedBy="itemConfigurationModeItemStatus"
     * )
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\Item
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus")
     * @ORM\JoinColumn(name="item_status_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus
     */
    private $itemStatus;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ConfigurationMode")
     * @ORM\JoinColumn(name="configuration_mode_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\ConfigurationMode
     */
    private $configurationMode;
}
