<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionDeltapriceRepository")
 * @ORM\Table(
 *     name="itemoptionclassificationoptiondeltaprice",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *             name="uq_default",
 *             columns={"channel_id","date_deleted","item_optionclassification_option_id"}
 *         )}
 * )
 */
class ItemOptionclassificationOptionDeltaprice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     */
    private $price;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     */
    private $price_net;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     */
    private $channel;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption",
     *     inversedBy="itemOptionclassificationOptionDeltaprice"
     * )
     * @ORM\JoinColumn(name="item_optionclassification_option_id", referencedColumnName="id")
     */
    private $itemOptionclassificationOption;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param float|null $price
     *
     * @return self
     */
    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @return float|null
     */
    public function getPriceNet(): ?float
    {
        return $this->price_net;
    }

    /**
     * @param float|null $price_net
     *
     * @return ItemOptionclassificationOptionDeltaprice
     */
    public function setPriceNet(?float $price_net): self
    {
        $this->price_net = $price_net;

        return $this;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime|string $dateCreated
     *
     * @return self
     */
    public function setDateCreated($dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateCreated(): ?\DateTime
    {
        return $this->date_created;
    }

    /**
     * @param \DateTime|string $dateUpdated
     *
     * @return self
     */
    public function setDateUpdated($dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateUpdated(): ?\DateTime
    {
        return $this->date_updated;
    }

    /**
     * @param \DateTime|string $dateDeleted
     *
     * @return self
     */
    public function setDateDeleted($dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateDeleted(): ?\DateTime
    {
        return $this->date_deleted;
    }

    /**
     * @param int $userCreatedId
     *
     * @return self
     */
    public function setUserCreatedId(int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return self
     */
    public function setUserUpdatedId(int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * @param int $userDeletedId
     *
     * @return self
     */
    public function setUserDeletedId(int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    /**
     * Set channel.
     *
     * @param Channel|null $channel
     *
     * @return self
     */
    public function setChannel(?Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return Channel|null
     */
    public function getChannel(): ?Channel
    {
        return $this->channel;
    }

    /**
     * @param ItemOptionclassificationOption|null $itemOptionclassificationOption
     *
     * @return self
     */
    public function setItemOptionclassificationOption(ItemOptionclassificationOption $itemOptionclassificationOption = null): self
    {
        $this->itemOptionclassificationOption = $itemOptionclassificationOption;

        return $this;
    }

    /**
     * @return ItemOptionclassificationOption|null
     */
    public function getItemOptionclassificationOption(): ?ItemOptionclassificationOption
    {
        return $this->itemOptionclassificationOption;
    }
}
