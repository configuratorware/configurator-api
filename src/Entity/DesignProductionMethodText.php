<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesignProductionMethodText.
 *
 * @ORM\Entity
 * @ORM\Table(
 *     name="design_production_method_text",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"design_production_method_id","language_id","date_deleted"})}
 * )
 */
class DesignProductionMethodText
{
    /**
     * @var
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="string", nullable=true) string|null
     */
    private $title;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;

    /**
     * @var
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod",
     *     inversedBy="designProductionMethodText"
     * )
     * @ORM\JoinColumn(name="design_production_method_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod
     */
    private $designProductionMethod;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\Language
     */
    private $language;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return DesignProductionMethodText
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return DesignProductionMethodText
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return DesignProductionMethodText
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return DesignProductionMethodText
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignProductionMethodText
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignProductionMethodText
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignProductionMethodText
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set designProductionMethod.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod|null $designProductionMethod
     *
     * @return DesignProductionMethodText
     */
    public function setDesignProductionMethod(DesignProductionMethod $designProductionMethod = null)
    {
        $this->designProductionMethod = $designProductionMethod;

        return $this;
    }

    /**
     * Get designProductionMethod.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod|null
     */
    public function getDesignProductionMethod()
    {
        return $this->designProductionMethod;
    }

    /**
     * Set language.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Language|null $language
     *
     * @return DesignProductionMethodText
     */
    public function setLanguage(Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Language|null
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
