<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\VisualizationModeRepository")
 */
class VisualizationMode
{
    public const IDENTIFIER_2D_LAYER = '2dLayer';
    public const IDENTIFIER_2D_VARIANT_OVERLAY = '2dVariantOverlay';
    public const IDENTIFIER_2D_VARIANT = '2dVariant';
    public const IDENTIFIER_3D_SCENE = '3dScene';
    public const IDENTIFIER_3D_VARIANT = '3dVariant';

    public static function is2dVariantVisualization(?string $visualizationModeIdentifier): bool
    {
        return in_array($visualizationModeIdentifier, [self::IDENTIFIER_2D_VARIANT, self::IDENTIFIER_2D_VARIANT_OVERLAY], true);
    }

    public static function is2dVisualization(?string $visualizationModeIdentifier): bool
    {
        return in_array($visualizationModeIdentifier, [self::IDENTIFIER_2D_LAYER, self::IDENTIFIER_2D_VARIANT, self::IDENTIFIER_2D_VARIANT_OVERLAY], true);
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $valid_for_configuration_modes;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", mappedBy="visualizationMode")
     */
    private $item;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string[]
     */
    public function getValidForConfigurationModes(): array
    {
        return $this->valid_for_configuration_modes;
    }

    /**
     * @param string[] $validForConfigurationModes
     */
    public function setValidForConfigurationModes(array $validForConfigurationModes): void
    {
        $this->valid_for_configuration_modes = $validForConfigurationModes;
    }

    /**
     * @return DateTime|null
     */
    public function getDateCreated(): ?DateTime
    {
        return $this->date_created;
    }

    /**
     * @param DateTime|null $dateCreated
     */
    public function setDateCreated(?DateTime $dateCreated): void
    {
        $this->date_created = $dateCreated;
    }

    /**
     * @return DateTime|null
     */
    public function getDateUpdated(): ?DateTime
    {
        return $this->date_updated;
    }

    /**
     * @param DateTime|null $dateUpdated
     */
    public function setDateUpdated(?DateTime $dateUpdated): void
    {
        $this->date_updated = $dateUpdated;
    }

    /**
     * @return DateTime|null
     */
    public function getDateDeleted(): ?DateTime
    {
        return $this->date_deleted;
    }

    /**
     * @param DateTime|null $dateDeleted
     */
    public function setDateDeleted(?DateTime $dateDeleted): void
    {
        $this->date_deleted = $dateDeleted;
    }

    /**
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * @param int|null $userCreatedId
     */
    public function setUserCreatedId(?int $userCreatedId): void
    {
        $this->user_created_id = $userCreatedId;
    }

    /**
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * @param int|null $userUpdatedId
     */
    public function setUserUpdatedId(?int $userUpdatedId): void
    {
        $this->user_updated_id = $userUpdatedId;
    }

    /**
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    /**
     * @param int|null $userDeletedId
     */
    public function setUserDeletedId(?int $userDeletedId): void
    {
        $this->user_deleted_id = $userDeletedId;
    }
}
