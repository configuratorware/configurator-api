<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImagePriceRepository")
 * @ORM\Table(
 *     name="image_gallery_image_price",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","channel_id","imagegalleryimage_id"})}
 * )
 */
class ImageGalleryImagePrice
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     */
    private float $price;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=25, scale=5)
     */
    private float $price_net;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private \DateTime $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private \DateTime $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private int $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private int $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private int $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id")
     */
    private Channel $channel;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage", inversedBy="imageGalleryImageprice")
     * @ORM\JoinColumn(name="imagegalleryimage_id", referencedColumnName="id")
     */
    private ImageGalleryImage $imageGalleryImage;

    public function __construct()
    {
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPriceNet(float $priceNet): self
    {
        $this->price_net = $priceNet;

        return $this;
    }

    public function getPriceNet(): float
    {
        return $this->price_net;
    }

    public function setDateCreated(\DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    public function getDateCreated(): \DateTime
    {
        return $this->date_created;
    }

    public function setDateUpdated(\DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    public function getDateUpdated(): \DateTime
    {
        return $this->date_updated;
    }

    public function setDateDeleted(\DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    public function getDateDeleted(): \DateTime
    {
        return $this->date_deleted;
    }

    public function setUserCreatedId(int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    public function getUserCreatedId(): int
    {
        return $this->user_created_id;
    }

    public function setUserUpdatedId(int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    public function getUserUpdatedId(): int
    {
        return $this->user_updated_id;
    }

    public function setUserDeletedId(int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    public function getUserDeletedId(): int
    {
        return $this->user_deleted_id;
    }

    public function setChannel(Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    public function getChannel(): Channel
    {
        return $this->channel;
    }

    public function setImageGalleryImage(ImageGalleryImage $imageGalleryImage): self
    {
        $this->imageGalleryImage = $imageGalleryImage;

        return $this;
    }

    public function getImageGalleryImage(): ImageGalleryImage
    {
        return $this->imageGalleryImage;
    }
}
