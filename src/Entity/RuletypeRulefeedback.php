<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\RuletypeRulefeedbackRepository")
 * @ORM\Table(name="ruletype_rulefeedback")
 */
class RuletypeRulefeedback
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype",
     *     inversedBy="ruletypeRulefeedback"
     * )
     * @ORM\JoinColumn(name="ruletype_id", referencedColumnName="id")
     */
    private $ruletype;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback",
     *     inversedBy="ruletypeRulefeedback"
     * )
     * @ORM\JoinColumn(name="rulefeedback_id", referencedColumnName="id")
     */
    private $rulefeedback;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return RuletypeRulefeedback
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return RuletypeRulefeedback
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return RuletypeRulefeedback
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return RuletypeRulefeedback
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return RuletypeRulefeedback
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return RuletypeRulefeedback
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set ruletype.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype $ruletype
     *
     * @return RuletypeRulefeedback
     */
    public function setRuletype(Ruletype $ruletype = null)
    {
        $this->ruletype = $ruletype;

        return $this;
    }

    /**
     * Get ruletype.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype
     */
    public function getRuletype()
    {
        return $this->ruletype;
    }

    /**
     * Set rulefeedback.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback $rulefeedback
     *
     * @return RuletypeRulefeedback
     */
    public function setRulefeedback(Rulefeedback $rulefeedback = null)
    {
        $this->rulefeedback = $rulefeedback;

        return $this;
    }

    /**
     * Get rulefeedback.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedback
     */
    public function getRulefeedback()
    {
        return $this->rulefeedback;
    }
}
