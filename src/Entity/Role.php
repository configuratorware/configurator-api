<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository")
 * @ORM\Table(
 *     name="role",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"name","date_deleted"})}
 * )
 */
class Role
{
    public const ADMIN_ROLE = 'ROLE_ADMIN';
    public const CLIENT_ROLE = 'ROLE_CLIENT';
    public const CONNECTOR_ROLE = 'ROLE_CONNECTOR';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $role;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\UserRole", mappedBy="role")
     */
    private $userRole;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential",
     *     mappedBy="role",
     *     cascade={"persist"}
     * )
     */
    private $roleCredential;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->userRole = new \Doctrine\Common\Collections\ArrayCollection();
        $this->roleCredential = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role.
     *
     * @param string $role
     *
     * @return Role
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role.
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Role
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Role
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Role
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Role
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Role
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Role
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add userRole.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\UserRole $userRole
     *
     * @return Role
     */
    public function addUserRole(UserRole $userRole)
    {
        $this->userRole[] = $userRole;

        return $this;
    }

    /**
     * Remove userRole.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\UserRole $userRole
     */
    public function removeUserRole(UserRole $userRole)
    {
        $this->userRole->removeElement($userRole);
    }

    /**
     * Get userRole.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    /**
     * Add roleCredential.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential $roleCredential
     *
     * @return Role
     */
    public function addRoleCredential(RoleCredential $roleCredential)
    {
        $this->roleCredential[] = $roleCredential;

        return $this;
    }

    /**
     * Remove roleCredential.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential $roleCredential
     */
    public function removeRoleCredential(RoleCredential $roleCredential)
    {
        $this->roleCredential->removeElement($roleCredential);
    }

    /**
     * Get roleCredential.
     *
     * @return \Doctrine\Common\Collections\Collection|RoleCredential[]
     */
    public function getRoleCredential()
    {
        return $this->roleCredential;
    }
}
