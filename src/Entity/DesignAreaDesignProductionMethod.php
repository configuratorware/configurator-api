<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * DesignAreaDesignProductionMethod.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodRepository")
 * @ORM\Table(
 *     name="design_area_design_production_method",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"design_area_id","design_production_method_id","date_deleted"})}
 * )
 */
class DesignAreaDesignProductionMethod
{
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $width;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $height;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $mask;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $additional_data;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $custom_data;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     *
     * @var bool
     */
    private $allow_bulk_names = false;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     *
     * @var bool
     */
    private $is_default = false;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $minimum_order_amount;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $default_colors;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":null})
     *
     * @var int|null
     */
    private $max_elements;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":null})
     *
     * @var int|null
     */
    private $max_texts;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":null})
     *
     * @var int|null
     */
    private $max_images;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     *
     * @var bool
     */
    private bool $one_line_text;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":false})
     *
     * @var bool|null
     */
    private $design_elements_locked;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @var DateTime
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime|null
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     *
     * @var DateTime
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice",
     *     mappedBy="designAreaDesignProductionMethod",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignAreaDesignProductionMethodPrice>
     */
    private Collection $designAreaDesignProductionMethodPrice;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea",
     *     inversedBy="designAreaDesignProductionMethod"
     * )
     * @ORM\JoinColumn(name="design_area_id", referencedColumnName="id")
     *
     * @var DesignArea|null
     */
    private $designArea;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod",
     *     inversedBy="designAreaDesignProductionMethod"
     * )
     * @ORM\JoinColumn(name="design_production_method_id", referencedColumnName="id")
     *
     * @var DesignProductionMethod|null
     */
    private $designProductionMethod;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->designAreaDesignProductionMethodPrice = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set width.
     *
     * @param int|null $width
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setWidth($width = null)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width.
     *
     * @return int|null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height.
     *
     * @param int|null $height
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setHeight($height = null)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height.
     *
     * @return int|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set customData.
     *
     * @param string|null $customData
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setCustomData($customData = null)
    {
        if ($customData && !is_string($customData)) {
            $customData = json_encode($customData);
        }

        $this->custom_data = $customData;

        return $this;
    }

    /**
     * Get customData.
     *
     * @return string|null
     */
    public function getCustomData()
    {
        $customData = $this->custom_data;

        if (null === $customData) {
            return null;
        }

        $decoded = json_decode($customData);
        if ($decoded) {
            $customData = $decoded;
        }

        return $customData;
    }

    /**
     * @return bool
     */
    public function getAllowBulkNames(): bool
    {
        return $this->allow_bulk_names;
    }

    /**
     * @param bool $allowBulkNames
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setAllowBulkNames(bool $allowBulkNames = false): DesignAreaDesignProductionMethod
    {
        $this->allow_bulk_names = $allowBulkNames;

        return $this;
    }

    /**
     * @return \stdClass|null
     */
    public function getMask(): ?\stdClass
    {
        if (null === $this->mask) {
            return null;
        }

        return json_decode($this->mask);
    }

    /**
     * @param \stdClass|null $mask
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setMask(?\stdClass $mask = null): DesignAreaDesignProductionMethod
    {
        if (null !== $mask) {
            $mask = json_encode($mask);
        }

        $this->mask = $mask ?: null;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsDefault(): bool
    {
        return $this->is_default;
    }

    /**
     * @param bool $is_default
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setIsDefault(bool $is_default = false): DesignAreaDesignProductionMethod
    {
        $this->is_default = $is_default;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinimumOrderAmount(): ?int
    {
        return $this->minimum_order_amount;
    }

    /**
     * @param int|null $minimum_order_amount
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setMinimumOrderAmount(?int $minimum_order_amount): self
    {
        $this->minimum_order_amount = $minimum_order_amount;

        return $this;
    }

    /**
     * Set additionalData.
     *
     * @param string|null $additionalData
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setAdditionalData($additionalData = null)
    {
        if ($additionalData && !is_string($additionalData)) {
            $additionalData = json_encode($additionalData);
        }

        $this->additional_data = $additionalData;

        return $this;
    }

    /**
     * Get additionalData.
     *
     * @return string|null
     */
    public function getAdditionalData()
    {
        $additionalData = $this->additional_data;

        if (null === $additionalData) {
            return null;
        }

        $decoded = json_decode($additionalData);
        if ($decoded) {
            $additionalData = $decoded;
        }

        return $additionalData;
    }

    /**
     * Set dateCreated.
     *
     * @param DateTime $dateCreated
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param DateTime|null $dateUpdated
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param DateTime $dateDeleted
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * @return \stdClass|null
     */
    public function getDefaultColors(): ?\stdClass
    {
        if (null === $this->default_colors) {
            return null;
        }

        return json_decode($this->default_colors);
    }

    /**
     * @param \stdClass|null $defaultColors
     */
    public function setDefaultColors(?\stdClass $defaultColors): void
    {
        if (null !== $defaultColors) {
            $defaultColors = json_encode($defaultColors);
        }

        $this->default_colors = false === $defaultColors ? null : $defaultColors;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add designAreaDesignProductionMethodPrice.
     *
     * @param DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function addDesignAreaDesignProductionMethodPrice(
        DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
    ): self {
        $this->designAreaDesignProductionMethodPrice->add($designAreaDesignProductionMethodPrice);

        return $this;
    }

    /**
     * Remove designAreaDesignProductionMethodPrice.
     *
     * @param DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignAreaDesignProductionMethodPrice(
        DesignAreaDesignProductionMethodPrice $designAreaDesignProductionMethodPrice
    ): bool {
        return $this->designAreaDesignProductionMethodPrice->removeElement($designAreaDesignProductionMethodPrice);
    }

    /**
     * Get designAreaDesignProductionMethodPrice.
     *
     * @return Collection<array-key, DesignAreaDesignProductionMethodPrice>
     */
    public function getDesignAreaDesignProductionMethodPrice(): Collection
    {
        return $this->designAreaDesignProductionMethodPrice;
    }

    /**
     * Set designArea.
     *
     * @param DesignArea|null $designArea
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setDesignArea(?DesignArea $designArea = null)
    {
        $this->designArea = $designArea;

        return $this;
    }

    /**
     * Get designArea.
     *
     * @return DesignArea|null
     */
    public function getDesignArea()
    {
        return $this->designArea;
    }

    /**
     * Set designProductionMethod.
     *
     * @param DesignProductionMethod|null $designProductionMethod
     *
     * @return DesignAreaDesignProductionMethod
     */
    public function setDesignProductionMethod(
        DesignProductionMethod $designProductionMethod = null
    ) {
        $this->designProductionMethod = $designProductionMethod;

        return $this;
    }

    /**
     * Get designProductionMethod.
     *
     * @return DesignProductionMethod|null
     */
    public function getDesignProductionMethod()
    {
        return $this->designProductionMethod;
    }

    /**
     * @return int|null
     */
    public function getMaxElements(): ?int
    {
        return $this->max_elements;
    }

    /**
     * @param int|null $maxElements
     *
     * @return self
     */
    public function setMaxElements(?int $maxElements): self
    {
        $this->max_elements = $maxElements;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxTexts(): ?int
    {
        return $this->max_texts;
    }

    /**
     * @param int|null $maxTexts
     *
     * @return self
     */
    public function setMaxTexts(?int $maxTexts): self
    {
        $this->max_texts = $maxTexts;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMaxImages(): ?int
    {
        return $this->max_images;
    }

    /**
     * @param int|null $maxImages
     *
     * @return self
     */
    public function setMaxImages(?int $maxImages): self
    {
        $this->max_images = $maxImages;

        return $this;
    }

    /**
     * @param bool $designElementsLocked
     *
     * @return self
     */
    public function setDesignElementsLocked(bool $designElementsLocked): self
    {
        $this->design_elements_locked = $designElementsLocked;

        return $this;
    }

    /**
     * @return bool
     */
    public function getDesignElementsLocked(): bool
    {
        return (bool)$this->design_elements_locked;
    }

    public function getOneLineText(): bool
    {
        return $this->one_line_text;
    }

    public function setOneLineText(?bool $oneLineText): self
    {
        $this->one_line_text = (bool) $oneLineText;

        return $this;
    }
}
