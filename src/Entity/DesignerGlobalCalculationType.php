<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * DesignerGlobalCalculationType.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository")
 * @ORM\Table(
 *     name="designer_global_calculation_type",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"identifier","date_deleted"})}
 * )
 */
class DesignerGlobalCalculationType
{
    public const IMAGE_DATA_DEPENDENT = 'image_data_dependent';
    public const USER_IMAGE_DEPENDENT = 'user_image_dependent';
    public const GALLERY_IMAGE_DEPENDENT = 'gallery_image_dependent';

    public const IMAGE__DEPENDENT_CONSTS = [
        self::GALLERY_IMAGE_DEPENDENT,
        self::USER_IMAGE_DEPENDENT,
        self::IMAGE_DATA_DEPENDENT,
    ];

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $identifier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $item_amount_dependent;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":1})
     */
    private ?bool $price_per_item = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $selectable_by_user;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private ?bool $bulk_name_dependent;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private \DateTime $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private \DateTime $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private ?int $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice",
     *     mappedBy="designerGlobalCalculationType"
     * )
     *
     * @var Collection<array-key, DesignerGlobalItemPrice>
     */
    private $designerGlobalItemPrice;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypeText",
     *     mappedBy="designerGlobalCalculationType",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignerGlobalCalculationTypeText>
     */
    private $designerGlobalCalculationTypeText;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypePrice",
     *     mappedBy="designerGlobalCalculationType",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignerGlobalCalculationTypePrice>
     */
    private Collection $designerGlobalCalculationTypePrice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $image_dependent;

    public function __construct()
    {
        $this->designerGlobalItemPrice = new ArrayCollection();
        $this->designerGlobalCalculationTypeText = new ArrayCollection();
        $this->designerGlobalCalculationTypePrice = new ArrayCollection();
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime('0001-01-01 00:00:00');
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string|null $identifier
     *
     * @return DesignerGlobalCalculationType
     */
    public function setIdentifier($identifier = null)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string|null
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set itemAmountDependent.
     *
     * @param bool|null $itemAmountDependent
     *
     * @return DesignerGlobalCalculationType
     */
    public function setItemAmountDependent($itemAmountDependent = null)
    {
        $this->item_amount_dependent = $itemAmountDependent;

        return $this;
    }

    /**
     * Get itemAmountDependent.
     *
     * @return bool|null
     */
    public function getItemAmountDependent()
    {
        return $this->item_amount_dependent;
    }

    public function getPricePerItem(): bool
    {
        return (bool)$this->price_per_item;
    }

    public function setPricePerItem(bool $pricePerItem): self
    {
        $this->price_per_item = $pricePerItem;

        return $this;
    }

    public function setSelectableByUser(?bool $selectableByUser = null): self
    {
        $this->selectable_by_user = $selectableByUser;

        return $this;
    }

    /**
     * Get selectableByUser.
     *
     * @return bool|null
     */
    public function getSelectableByUser()
    {
        return $this->selectable_by_user;
    }

    /**
     * Set imageDataDependent.
     *
     * @param bool|null $imageDataDependent
     *
     * @return DesignerGlobalCalculationType
     */
    public function setImageDataDependent($imageDataDependent = null)
    {
        if ($imageDataDependent) {
            $this->image_dependent = self::IMAGE_DATA_DEPENDENT;
        }

        return $this;
    }

    public function getImageDataDependent(): bool
    {
        return self::IMAGE_DATA_DEPENDENT === $this->image_dependent;
    }

    public function getUserImageDependent(): bool
    {
        return self::USER_IMAGE_DEPENDENT === $this->image_dependent;
    }

    public function getGalleryImageDependent(): bool
    {
        return self::GALLERY_IMAGE_DEPENDENT === $this->image_dependent;
    }

    /**
     * @return bool|null
     */
    public function getBulkNameDependent()
    {
        return $this->bulk_name_dependent;
    }

    /**
     * @param bool $bulkNameDependent
     */
    public function setBulkNameDependent($bulkNameDependent): void
    {
        $this->bulk_name_dependent = $bulkNameDependent;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return DesignerGlobalCalculationType
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return DesignerGlobalCalculationType
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return DesignerGlobalCalculationType
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignerGlobalCalculationType
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignerGlobalCalculationType
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignerGlobalCalculationType
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add designerGlobalItemPrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice $designerGlobalItemPrice
     *
     * @return DesignerGlobalCalculationType
     */
    public function addDesignerGlobalItemPrice(DesignerGlobalItemPrice $designerGlobalItemPrice)
    {
        $this->designerGlobalItemPrice[] = $designerGlobalItemPrice;

        return $this;
    }

    /**
     * Remove designerGlobalItemPrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice $designerGlobalItemPrice
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignerGlobalItemPrice(DesignerGlobalItemPrice $designerGlobalItemPrice)
    {
        return $this->designerGlobalItemPrice->removeElement($designerGlobalItemPrice);
    }

    /**
     * Get designerGlobalItemPrice.
     *
     * @return Collection
     */
    public function getDesignerGlobalItemPrice()
    {
        return $this->designerGlobalItemPrice;
    }

    /**
     * Add designerGlobalCalculationTypeText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypeText $designerGlobalCalculationTypeText
     *
     * @return DesignerGlobalCalculationType
     */
    public function addDesignerGlobalCalculationTypeText(DesignerGlobalCalculationTypeText $designerGlobalCalculationTypeText)
    {
        $this->designerGlobalCalculationTypeText[] = $designerGlobalCalculationTypeText;

        return $this;
    }

    /**
     * Remove designerGlobalCalculationTypeText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypeText $designerGlobalCalculationTypeText
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignerGlobalCalculationTypeText(DesignerGlobalCalculationTypeText $designerGlobalCalculationTypeText)
    {
        return $this->designerGlobalCalculationTypeText->removeElement($designerGlobalCalculationTypeText);
    }

    /**
     * Get designerGlobalCalculationTypeText.
     *
     * @return Collection
     */
    public function getDesignerGlobalCalculationTypeText()
    {
        return $this->designerGlobalCalculationTypeText;
    }

    /**
     * Add designerGlobalCalculationTypePrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypePrice $designerGlobalCalculationTypePrice
     *
     * @return DesignerGlobalCalculationType
     */
    public function addDesignerGlobalCalculationTypePrice(DesignerGlobalCalculationTypePrice $designerGlobalCalculationTypePrice)
    {
        $this->designerGlobalCalculationTypePrice[] = $designerGlobalCalculationTypePrice;

        return $this;
    }

    /**
     * Remove designerGlobalCalculationTypePrice.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypePrice $designerGlobalCalculationTypePrice
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignerGlobalCalculationTypePrice(DesignerGlobalCalculationTypePrice $designerGlobalCalculationTypePrice)
    {
        return $this->designerGlobalCalculationTypePrice->removeElement($designerGlobalCalculationTypePrice);
    }

    /**
     * Get designerGlobalCalculationTypePrice.
     *
     * @return Collection
     */
    public function getDesignerGlobalCalculationTypePrice()
    {
        return $this->designerGlobalCalculationTypePrice;
    }

    /**
     * @return string
     */
    public function getTranslatedTitle()
    {
        $title = '';

        $texts = $this->getDesignerGlobalCalculationTypeText();

        /**
         * @var DesignProductionMethodText $text
         */
        foreach ($texts as $text) {
            if (C_LANGUAGE_ISO === $text->getLanguage()->getIso() && '' !== $text->getTitle()) {
                $title = $text->getTitle();

                break;
            }
        }

        return $title;
    }

    public function getImageDependent(): ?string
    {
        return $this->image_dependent;
    }

    public function setImageDependent(?string $image_dependent): self
    {
        $this->image_dependent = $image_dependent;

        return $this;
    }
}
