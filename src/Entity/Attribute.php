<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ConfiguratorApiBundle\Cache\HasIdInterface;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository")
 * @ORM\Table(
 *     name="attribute",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_identifier_date_deleted", columns={"date_deleted","identifier"})}
 * )
 */
class Attribute implements HasIdInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalid;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private $attributedatatype;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $is_compatibility_attribute;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $hide_in_frontend;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private bool $writeProtected = false;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Attributetext",
     *     mappedBy="attribute",
     *     cascade={"persist"}
     * )
     */
    private $attributetext;

    /**
     * Constructor.
     */
    public function __construct()
    {
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Attribute
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set externalid.
     *
     * @param string $externalid
     *
     * @return Attribute
     */
    public function setExternalid($externalid)
    {
        $this->externalid = $externalid;

        return $this;
    }

    /**
     * Get externalid.
     *
     * @return string
     */
    public function getExternalid()
    {
        return $this->externalid;
    }

    /**
     * Set attributedatatype.
     *
     * @param string $attributedatatype
     *
     * @return Attribute
     */
    public function setAttributedatatype($attributedatatype)
    {
        $this->attributedatatype = $attributedatatype;

        return $this;
    }

    /**
     * Get attributedatatype.
     *
     * @return string
     */
    public function getAttributedatatype()
    {
        return $this->attributedatatype;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Attribute
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Attribute
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Attribute
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Attribute
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Attribute
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Attribute
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add attributetext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributetext $attributetext
     *
     * @return Attribute
     */
    public function addAttributetext(Attributetext $attributetext)
    {
        $this->attributetext[] = $attributetext;

        return $this;
    }

    /**
     * Remove attributetext.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributetext $attributetext
     */
    public function removeAttributetext(Attributetext $attributetext)
    {
        $this->attributetext->removeElement($attributetext);
    }

    /**
     * Get attributetext.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributetext()
    {
        return $this->attributetext;
    }

    /**
     * Returns translated title field.
     *
     * @return string
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     */
    public function getTranslatedTitle($languageIso = null)
    {
        $language = C_LANGUAGE_ISO;
        if (!empty($languageIso)) {
            $language = $languageIso;
        }

        $title = '{' . $this->getIdentifier() . '}';

        $attributetexts = $this->getAttributetext();

        if (!empty($attributetexts)) {
            foreach ($attributetexts as $attributetext) {
                if ($attributetext->getLanguage()->getIso() === $language) {
                    $title = $attributetext->getTitle();

                    break;
                }
            }
        }

        return $title;
    }

    /**
     * @return mixed
     */
    public function getisCompatibilityAttribute()
    {
        return $this->is_compatibility_attribute;
    }

    /**
     * @param mixed $is_compatibility_attribute
     */
    public function setIsCompatibilityAttribute($is_compatibility_attribute)
    {
        $this->is_compatibility_attribute = $is_compatibility_attribute;
    }

    /**
     * @return mixed
     */
    public function getHideInFrontend()
    {
        return $this->hide_in_frontend;
    }

    /**
     * @param mixed $hide_in_frontend
     */
    public function setHideInFrontend($hide_in_frontend)
    {
        $this->hide_in_frontend = $hide_in_frontend;
    }

    public function getWriteProtected(): bool
    {
        return $this->writeProtected;
    }

    public function setWriteProtected(bool $writeProtected): self
    {
        $this->writeProtected = $writeProtected;

        return $this;
    }
}
