<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DesignProductionMethod.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository")
 * @ORM\Table(
 *     name="design_production_method",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"identifier","date_deleted"})}
 * )
 */
class DesignProductionMethod
{
    /**
     * @var
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="string", nullable=true) string|null
     */
    private $identifier;

    /**
     * @var
     * @ORM\Column(type="text", nullable=true) string|null
     */
    private $options;

    /**
     * @var
     * @ORM\Column(type="text", nullable=true) string|null
     */
    private $custom_data;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $minimum_order_amount;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;

    /**
     * @var
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod",
     *     mappedBy="designProductionMethod"
     * ) \Doctrine\Common\Collections\Collection
     */
    private $designAreaDesignProductionMethod;

    /**
     * @var
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette",
     *     mappedBy="designProductionMethod",
     *     cascade={"persist"}
     * ) \Doctrine\Common\Collections\Collection
     */
    private $designProductionMethodColorPalette;

    /**
     * @var
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodText",
     *     mappedBy="designProductionMethod",
     *     cascade={"persist"}
     * ) \Doctrine\Common\Collections\Collection
     */
    private $designProductionMethodText;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType",
     *     mappedBy="designProductionMethod",
     *     cascade={"persist"}
     * )
     */
    private $designerProductionCalculationType;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private bool $vectorizedLogoMandatory = false;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->designAreaDesignProductionMethod = new \Doctrine\Common\Collections\ArrayCollection();
        $this->designProductionMethodColorPalette = new \Doctrine\Common\Collections\ArrayCollection();
        $this->designProductionMethodText = new \Doctrine\Common\Collections\ArrayCollection();
        $this->designerProductionCalculationType = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string|null $identifier
     *
     * @return DesignProductionMethod
     */
    public function setIdentifier($identifier = null): DesignProductionMethod
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * Set options.
     *
     * @param string|null $options
     *
     * @return DesignProductionMethod
     */
    public function setOptions($options = null)
    {
        if ($options && !is_string($options)) {
            $options = json_encode($options);
        }

        $this->options = $options;

        return $this;
    }

    /**
     * Get options.
     *
     * @return string|null
     */
    public function getOptions()
    {
        $options = $this->options;

        $decoded = json_decode($options);
        if ($decoded) {
            $options = $decoded;
        }

        return $options;
    }

    /**
     * Set customData.
     *
     * @param string|null $customData
     *
     * @return DesignProductionMethod
     */
    public function setCustomData($customData = null)
    {
        if ($customData && !is_string($customData)) {
            $customData = json_encode($customData);
        }

        $this->custom_data = $customData;

        return $this;
    }

    /**
     * Get customData.
     *
     * @return string|null
     */
    public function getCustomData()
    {
        $customData = $this->custom_data;

        $decoded = json_decode($customData);
        if ($decoded) {
            $customData = $decoded;
        }

        return $customData;
    }

    /**
     * @param int|null $minimum_order_amount
     *
     * @return DesignProductionMethod
     */
    public function setMinimumOrderAmount(?int $minimum_order_amount): self
    {
        $this->minimum_order_amount = $minimum_order_amount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinimumOrderAmount(): ?int
    {
        return $this->minimum_order_amount;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return DesignProductionMethod
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return DesignProductionMethod
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return DesignProductionMethod
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignProductionMethod
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignProductionMethod
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignProductionMethod
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add designAreaDesignProductionMethod.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
     *
     * @return DesignProductionMethod
     */
    public function addDesignAreaDesignProductionMethod(
        DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
    ) {
        $this->designAreaDesignProductionMethod[] = $designAreaDesignProductionMethod;

        return $this;
    }

    /**
     * Remove designAreaDesignProductionMethod.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignAreaDesignProductionMethod(
        DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
    ) {
        return $this->designAreaDesignProductionMethod->removeElement($designAreaDesignProductionMethod);
    }

    /**
     * Get designAreaDesignProductionMethod.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignAreaDesignProductionMethod()
    {
        return $this->designAreaDesignProductionMethod;
    }

    /**
     * Add designProductionMethodColorPalette.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette $designProductionMethodColorPalette
     *
     * @return DesignProductionMethod
     */
    public function addDesignProductionMethodColorPalette(
        DesignProductionMethodColorPalette $designProductionMethodColorPalette
    ) {
        $this->designProductionMethodColorPalette[] = $designProductionMethodColorPalette;

        return $this;
    }

    /**
     * Remove designProductionMethodColorPalette.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette $designProductionMethodColorPalette
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignProductionMethodColorPalette(
        DesignProductionMethodColorPalette $designProductionMethodColorPalette
    ) {
        return $this->designProductionMethodColorPalette->removeElement($designProductionMethodColorPalette);
    }

    /**
     * Get designProductionMethodColorPalette.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignProductionMethodColorPalette()
    {
        return $this->designProductionMethodColorPalette;
    }

    /**
     * Add designProductionMethodText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodText $designProductionMethodText
     *
     * @return DesignProductionMethod
     */
    public function addDesignProductionMethodText(
        DesignProductionMethodText $designProductionMethodText
    ) {
        $this->designProductionMethodText[] = $designProductionMethodText;

        return $this;
    }

    /**
     * Remove designProductionMethodText.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodText $designProductionMethodText
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignProductionMethodText(
        DesignProductionMethodText $designProductionMethodText
    ) {
        return $this->designProductionMethodText->removeElement($designProductionMethodText);
    }

    /**
     * Get designProductionMethodText.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignProductionMethodText()
    {
        return $this->designProductionMethodText;
    }

    /**
     * returns the translated title of the design production method for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle(): string
    {
        $title = '';

        $texts = $this->getDesignProductionMethodText();

        /**
         * @var DesignProductionMethodText $text
         */
        foreach ($texts as $text) {
            if ('' !== $text->getTitle() && C_LANGUAGE_ISO === $text->getLanguage()->getIso()) {
                $title = $text->getTitle();

                break;
            }
        }

        return $title;
    }

    /**
     * Add designerProductionCalculation.
     *
     * @param DesignerProductionCalculationType $designerProductionCalculationType
     *
     * @return DesignProductionMethod
     */
    public function addDesignerProductionCalculationType(
        DesignerProductionCalculationType $designerProductionCalculationType
    ) {
        $this->designerProductionCalculationType[] = $designerProductionCalculationType;

        return $this;
    }

    /**
     * Remove designerProductionCalculation.
     *
     * @param DesignerProductionCalculationType $designerProductionCalculationType
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignerProductionCalculationType(
        DesignerProductionCalculationType $designerProductionCalculationType
    ) {
        return $this->designerProductionCalculationType->removeElement($designerProductionCalculationType);
    }

    /**
     * Get designerProductionCalculation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignerProductionCalculationType()
    {
        return $this->designerProductionCalculationType;
    }

    public function getVectorizedLogoMandatory(): bool
    {
        return $this->vectorizedLogoMandatory;
    }

    public function setVectorizedLogoMandatory(bool $vectorizedLogoMandatory): void
    {
        $this->vectorizedLogoMandatory = $vectorizedLogoMandatory;
    }
}
