<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="itemgrouptranslation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","itemgroup_id","language_id"})}
 * )
 */
class Itemgrouptranslation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $translation;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup",
     *     inversedBy="itemgrouptranslation"
     * )
     * @ORM\JoinColumn(name="itemgroup_id", referencedColumnName="id")
     */
    private $itemgroup;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Language",
     *     inversedBy="itemgrouptranslation"
     * )
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set translation.
     *
     * @param string $translation
     *
     * @return Itemgrouptranslation
     */
    public function setTranslation($translation)
    {
        $this->translation = $translation;

        return $this;
    }

    /**
     * Get translation.
     *
     * @return string
     */
    public function getTranslation()
    {
        return $this->translation;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Itemgrouptranslation
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Itemgrouptranslation
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Itemgrouptranslation
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Itemgrouptranslation
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Itemgrouptranslation
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Itemgrouptranslation
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set itemgroup.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup $itemgroup
     *
     * @return Itemgrouptranslation
     */
    public function setItemgroup(Itemgroup $itemgroup = null)
    {
        $this->itemgroup = $itemgroup;

        return $this;
    }

    /**
     * Get itemgroup.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup
     */
    public function getItemgroup()
    {
        return $this->itemgroup;
    }

    /**
     * Set language.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Language $language
     *
     * @return Itemgrouptranslation
     */
    public function setLanguage(Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
