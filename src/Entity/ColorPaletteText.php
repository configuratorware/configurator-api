<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="color_palette_text")
 */
class ColorPaletteText
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette",
     *     inversedBy="colorPaletteText"
     * )
     * @ORM\JoinColumn(name="color_palette_id", referencedColumnName="id")
     */
    private $colorPalette;

    /**
     * Get the value of Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Title.
     *
     * @param string title
     *
     * @return ColorPaletteText
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Date Created.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of Date Created.
     *
     * @param \DateTime date_created
     *
     * @return ColorPaletteText
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get the value of Date Updated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of Date Updated.
     *
     * @param \DateTime date_updated
     *
     * @return ColorPaletteText
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Get the value of Date Deleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set the value of Date Deleted.
     *
     * @param \DateTime date_deleted
     *
     * @return ColorPaletteText
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;

        return $this;
    }

    /**
     * Get the value of User Created Id.
     *
     * @return User
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set the value of User Created Id.
     *
     * @param User user_created_id
     *
     * @return ColorPaletteText
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;

        return $this;
    }

    /**
     * Get the value of User Updated Id.
     *
     * @return User
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set the value of User Updated Id.
     *
     * @param User user_updated_id
     *
     * @return ColorPaletteText
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;

        return $this;
    }

    /**
     * Get the value of User Deleted Id.
     *
     * @return User
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set the value of User Deleted Id.
     *
     * @param User user_deleted_id
     *
     * @return ColorPaletteText
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;

        return $this;
    }

    /**
     * Get the value of Language.
     *
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set the value of Language.
     *
     * @param Language language
     *
     * @return ColorPaletteText
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get the value of Color Palette.
     *
     * @return ColorPalette
     */
    public function getColorPalette()
    {
        return $this->colorPalette;
    }

    /**
     * Set the value of Color Palette.
     *
     * @param ColorPalette colorPalette
     *
     * @return ColorPaletteText
     */
    public function setColorPalette($colorPalette)
    {
        $this->colorPalette = $colorPalette;

        return $this;
    }
}
