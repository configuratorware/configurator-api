<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

interface ViewInterface
{
    public function getItem(): ?Item;

    public function getIdentifier(): ?string;
}
