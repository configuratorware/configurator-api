<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDesignView;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository")
 * @ORM\Table(name="creator_view")
 */
class CreatorView implements ViewInterface
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $directory;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $sequence_number;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $custom_data;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @var int|null
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @var int|null
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @var int|null
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @var Collection<array-key, CreatorViewText>
     *
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewText",
     *     mappedBy="creatorView",
     *     cascade={"persist"}
     * )
     */
    private Collection $creatorViewText;

    /**
     * @var Collection<array-key, CreatorViewDesignArea>
     *
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea",
     *     mappedBy="creatorView",
     *     cascade={"persist"}
     * )
     */
    private Collection $creatorViewDesignArea;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="creatorView")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @param string|null $directory
     * @param string $identifier
     * @param Item $item
     * @param string $sequenceNumber
     */
    private function __construct(?string $directory, string $identifier, Item $item, string $sequenceNumber)
    {
        $this->directory = $directory;
        $this->identifier = $identifier;
        $this->item = $item;
        $this->sequence_number = $sequenceNumber;
        $this->date_deleted = new DateTime(EntityListener::DATE_DELETED_DEFAULT);
        $this->creatorViewText = new ArrayCollection();
        $this->creatorViewDesignArea = new ArrayCollection();
    }

    /**
     * @param string|null $directory
     * @param string $identifier
     * @param Item $item
     * @param string $sequenceNumber
     *
     * @return self
     */
    public static function from(?string $directory, string $identifier, Item $item, string $sequenceNumber): CreatorView
    {
        return new self($directory, $identifier, $item, $sequenceNumber);
    }

    /**
     * @param DesignView $designView
     * @param Item $item
     *
     * @return CreatorView
     */
    public static function initFromDesignView(DesignView $designView, Item $item): CreatorView
    {
        return new self(null, $designView->getIdentifier() ?? '', $item, $designView->getSequenceNumber());
    }

    /**
     * @param DesignView $designView
     *
     * @return CreatorView
     */
    public static function copyFromDesignView(DesignView $designView): CreatorView
    {
        $item = $designView->getItem();
        if (null === $item) {
            throw InvalidDesignView::missingItem();
        }

        $creatorView = new self(null, $designView->getIdentifier() ?? '', $item, $designView->getSequenceNumber() ?? '');

        foreach ($designView->getDesignViewDesignArea() as $designViewArea) {
            $creatorView->addCreatorViewDesignArea(CreatorViewDesignArea::fromDesignViewDesignArea($designViewArea, $creatorView, $designViewArea->getDesignArea()));
        }

        foreach ($designView->getDesignViewText() as $text) {
            $creatorView->addCreatorViewText(CreatorViewText::fromDesignViewText($text, $creatorView));
        }

        return $creatorView;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getDirectory(): ?string
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     */
    public function setDirectory(string $directory): void
    {
        $this->directory = $directory;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return self
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getSequenceNumber(): string
    {
        return $this->sequence_number;
    }

    /**
     * @param string $sequenceNumber
     */
    public function setSequenceNumber(string $sequenceNumber): void
    {
        $this->sequence_number = $sequenceNumber;
    }

    /**
     * @return Item
     */
    public function getItem(): Item
    {
        return $this->item;
    }

    /**
     * @return string
     */
    public function getItemIdentifier(): string
    {
        return $this->item->getIdentifier();
    }

    /**
     * @param Item $item
     */
    public function setItem(Item $item): void
    {
        $this->item = $item;
    }

    /**
     * @param CreatorViewText $creatorViewText
     *
     * @return CreatorView
     */
    public function addCreatorViewText(CreatorViewText $creatorViewText): CreatorView
    {
        $this->creatorViewText->add($creatorViewText);

        return $this;
    }

    /**
     * @param CreatorViewText $creatorViewText
     */
    public function removeCreatorViewText(CreatorViewText $creatorViewText): void
    {
        $key = $this->creatorViewText->indexOf($creatorViewText);

        if (false === $key) {
            return;
        }

        /** @var CreatorViewText $element */
        $element = $this->creatorViewText->get($key);

        $element->setDateDeleted(new \DateTime());
    }

    /**
     * @param CreatorViewDesignArea $creatorViewDesignArea
     *
     * @return CreatorView
     */
    public function addCreatorViewDesignArea(CreatorViewDesignArea $creatorViewDesignArea): CreatorView
    {
        $this->creatorViewDesignArea->add($creatorViewDesignArea);

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCreatorViewText(): Collection
    {
        return $this->creatorViewText;
    }

    /**
     * @return string|null
     */
    public function getTranslatedTitle(): ?string
    {
        $title = $this->identifier;

        if (defined('C_LANGUAGE_ISO')) {
            foreach ($this->creatorViewText as $text) {
                if ('' !== $text->getTitle() && C_LANGUAGE_ISO === $text->getLanguage()->getIso()) {
                    return $text->getTitle();
                }
            }
        }

        return $title;
    }

    /**
     * @return Collection<int, CreatorViewDesignArea>
     */
    public function getCreatorViewDesignArea(): Collection
    {
        return $this->creatorViewDesignArea;
    }

    /**
     * @param CreatorViewDesignArea $creatorViewDesignArea
     */
    public function removeCreatorViewDesignArea(CreatorViewDesignArea $creatorViewDesignArea): void
    {
        $key = $this->creatorViewDesignArea->indexOf($creatorViewDesignArea);

        if (false === $key) {
            return;
        }

        /** @var CreatorViewDesignArea $element */
        $element = $this->creatorViewDesignArea->get($key);

        $element->setDateDeleted(new \DateTime());
    }

    /**
     * @param DateTime $dateCreated
     *
     * @return CreatorView
     */
    public function setDateCreated($dateCreated): CreatorView
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param DateTime|null $dateUpdated
     *
     * @return CreatorView
     */
    public function setDateUpdated($dateUpdated = null): CreatorView
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param DateTime $dateDeleted
     *
     * @return CreatorView
     */
    public function setDateDeleted($dateDeleted): CreatorView
    {
        $this->date_deleted = $dateDeleted;

        if (EntityListener::DATE_DELETED_DEFAULT !== $this->date_deleted->format('Y-m-d H:i:s')) {
            foreach ($this->creatorViewText as $text) {
                $this->removeCreatorViewText($text);
            }

            foreach ($this->creatorViewDesignArea as $viewArea) {
                $this->removeCreatorViewDesignArea($viewArea);
            }
        }

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param int|null $userCreatedId
     *
     * @return CreatorView
     */
    public function setUserCreatedId($userCreatedId = null): CreatorView
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * @param int|null $userUpdatedId
     *
     * @return CreatorView
     */
    public function setUserUpdatedId($userUpdatedId = null): CreatorView
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * @param int|null $userDeletedId
     *
     * @return CreatorView
     */
    public function setUserDeletedId($userDeletedId = null): CreatorView
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }
}
