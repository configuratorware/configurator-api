<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ConfiguratorApiBundle\Cache\HasIdInterface;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository")
 * @ORM\Table(
 *     name="attributevalue",
 *     indexes={@ORM\Index(name="ix_value", columns={"value"})},
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"value","date_deleted"})}
 * )
 */
class Attributevalue implements HasIdInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $value;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Attributevaluetranslation",
     *     mappedBy="attributevalue",
     *     cascade={"persist"}
     * )
     */
    private $attributevaluetranslation;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->attributevaluetranslation = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value.
     *
     * @param string $value
     *
     * @return Attributevalue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Attributevalue
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Attributevalue
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Attributevalue
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Attributevalue
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Attributevalue
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Attributevalue
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add attributevaluetranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevaluetranslation $attributevaluetranslation
     *
     * @return Attributevalue
     */
    public function addAttributevaluetranslation(
        Attributevaluetranslation $attributevaluetranslation
    ) {
        $this->attributevaluetranslation[] = $attributevaluetranslation;

        return $this;
    }

    /**
     * Remove attributevaluetranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevaluetranslation $attributevaluetranslation
     */
    public function removeAttributevaluetranslation(
        Attributevaluetranslation $attributevaluetranslation
    ) {
        $this->attributevaluetranslation->removeElement($attributevaluetranslation);
    }

    /**
     * @return \Doctrine\Common\Collections\Collection|Attributevaluetranslation[]
     */
    public function getAttributevaluetranslation()
    {
        return $this->attributevaluetranslation;
    }

    /**
     * Returns translated title field.
     *
     * @return  string
     */
    public function getTranslatedValue()
    {
        $translatedValue = $this->getValue() ?? '';

        $attributetexts = $this->getAttributevaluetranslation();

        foreach ($attributetexts as $attributetext) {
            if (C_LANGUAGE_ISO === $attributetext->getLanguage()
                    ->getIso()
            ) {
                $translatedValue = $attributetext->getTranslation();

                break;
            }
        }

        return $translatedValue;
    }
}
