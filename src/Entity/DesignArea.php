<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use stdClass;

/**
 * DesignArea.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository")
 * @ORM\Table(name="design_area")
 */
class DesignArea
{
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int|null
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string|null
     */
    private $identifier;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $sequence_number;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $height;

    /**
     * @ORM\Column(type="integer", nullable=true)
     *
     * @var int|null
     */
    private $width;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $mask;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $custom_data;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @var DateTime
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @var DateTime|null
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     *
     * @var DateTime
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     *
     * @var int|null
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod",
     *     mappedBy="designArea",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignAreaDesignProductionMethod>
     */
    private Collection $designAreaDesignProductionMethod;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea",
     *     mappedBy="designArea"
     * )
     *
     * @var Collection<array-key, DesignViewDesignArea>
     */
    private Collection $designViewDesignArea;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea",
     *     mappedBy="designArea"
     * )
     *
     * @var Collection<array-key, CreatorViewDesignArea>
     */
    private Collection $creatorViewDesignArea;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText",
     *     mappedBy="designArea",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<array-key, DesignAreaText>
     */
    private Collection $designAreaText;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="designArea")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     *
     * @var Item|null
     */
    private ?Item $item;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Option")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     *
     * @var Option|null
     */
    private ?Option $option;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->designAreaDesignProductionMethod = new ArrayCollection();
        $this->designViewDesignArea = new ArrayCollection();
        $this->creatorViewDesignArea = new ArrayCollection();
        $this->designAreaText = new ArrayCollection();
        $this->date_created = new DateTime();
        $this->date_deleted = new DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string|null $identifier
     *
     * @return DesignArea
     */
    public function setIdentifier($identifier = null)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string|null
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set height.
     *
     * @param int|null $height
     *
     * @return DesignArea
     */
    public function setHeight($height = null)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height.
     *
     * @return int|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set width.
     *
     * @param int|null $width
     *
     * @return DesignArea
     */
    public function setWidth($width = null)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width.
     *
     * @return int|null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set customData.
     *
     * @param string|null $customData
     *
     * @return DesignArea
     */
    public function setCustomData($customData = null)
    {
        if ($customData && !is_string($customData)) {
            $customData = json_encode($customData);
        }

        $this->custom_data = $customData;

        return $this;
    }

    /**
     * Get customData.
     *
     * @return string|null
     */
    public function getCustomData()
    {
        $customData = $this->custom_data;

        if (null === $customData) {
            return null;
        }

        $decoded = json_decode($customData);
        if ($decoded) {
            $customData = $decoded;
        }

        return $customData;
    }

    /**
     * Set dateCreated.
     *
     * @param DateTime $dateCreated
     *
     * @return DesignArea
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param DateTime|null $dateUpdated
     *
     * @return DesignArea
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param DateTime $dateDeleted
     *
     * @return DesignArea
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignArea
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignArea
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignArea
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add designAreaDesignProductionMethod.
     *
     * @param DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
     *
     * @return DesignArea
     */
    public function addDesignAreaDesignProductionMethod(
        DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
    ) {
        $this->designAreaDesignProductionMethod->add($designAreaDesignProductionMethod);

        return $this;
    }

    /**
     * Remove designAreaDesignProductionMethod.
     *
     * @param DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignAreaDesignProductionMethod(
        DesignAreaDesignProductionMethod $designAreaDesignProductionMethod
    ): bool {
        return $this->designAreaDesignProductionMethod->removeElement($designAreaDesignProductionMethod);
    }

    /**
     * Get designAreaDesignProductionMethod.
     *
     * @return Collection<array-key, DesignAreaDesignProductionMethod>
     */
    public function getDesignAreaDesignProductionMethod(): Collection
    {
        return $this->designAreaDesignProductionMethod;
    }

    /**
     * Add designViewDesignArea.
     *
     * @param DesignViewDesignArea $designViewDesignArea
     *
     * @return DesignArea
     */
    public function addDesignViewDesignArea(
        DesignViewDesignArea $designViewDesignArea
    ) {
        $this->designViewDesignArea[] = $designViewDesignArea;

        return $this;
    }

    /**
     * Remove designViewDesignArea.
     *
     * @param DesignViewDesignArea $designViewDesignArea
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignViewDesignArea(
        DesignViewDesignArea $designViewDesignArea
    ) {
        return $this->designViewDesignArea->removeElement($designViewDesignArea);
    }

    /**
     * Get designViewDesignArea.
     *
     * @return Collection
     */
    public function getDesignViewDesignArea()
    {
        return $this->designViewDesignArea;
    }

    /**
     * Add creatorViewDesignArea.
     *
     * @param CreatorViewDesignArea $creatorViewDesignArea
     *
     * @return DesignArea
     */
    public function addCreatorViewDesignArea(CreatorViewDesignArea $creatorViewDesignArea)
    {
        $this->creatorViewDesignArea[] = $creatorViewDesignArea;

        return $this;
    }

    /**
     * Remove creatorViewDesignArea.
     *
     * @param CreatorViewDesignArea $creatorViewDesignArea
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeCreatorViewDesignArea(CreatorViewDesignArea $creatorViewDesignArea)
    {
        return $this->creatorViewDesignArea->removeElement($creatorViewDesignArea);
    }

    /**
     * Get creatorViewDesignArea.
     *
     * @return Collection
     */
    public function getCreatorViewDesignArea()
    {
        return $this->creatorViewDesignArea;
    }

    /**
     * Add designAreaText.
     *
     * @param DesignAreaText $designAreaText
     *
     * @return DesignArea
     */
    public function addDesignAreaText(DesignAreaText $designAreaText): self
    {
        $this->designAreaText->add($designAreaText);

        return $this;
    }

    /**
     * Remove designAreaText.
     *
     * @param DesignAreaText $designAreaText
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignAreaText(DesignAreaText $designAreaText): bool
    {
        return $this->designAreaText->removeElement($designAreaText);
    }

    /**
     * Get designAreaText.
     *
     * @return Collection<array-key, DesignAreaText>
     */
    public function getDesignAreaText(): Collection
    {
        return $this->designAreaText;
    }

    /**
     * Set item.
     *
     * @param Item|null $item
     *
     * @return DesignArea
     */
    public function setItem(Item $item = null): self
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return Item|null
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set option.
     *
     * @param Option|null $option
     *
     * @return DesignArea
     */
    public function setOption(Option $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option.
     *
     * @return Option|null
     */
    public function getOption()
    {
        return $this->option;
    }

    /**
     * @return mixed
     */
    public function getSequenceNumber()
    {
        return $this->sequence_number;
    }

    /**
     * @param mixed $sequence_number
     */
    public function setSequenceNumber($sequence_number)
    {
        $this->sequence_number = $sequence_number;
    }

    /**
     * @return stdClass|null
     */
    public function getMask(): ?stdClass
    {
        if (null === $this->mask) {
            return null;
        }

        return json_decode($this->mask);
    }

    /**
     * @param stdClass|null $mask
     *
     * @return DesignArea
     */
    public function setMask(?stdClass $mask = null): DesignArea
    {
        if (null !== $mask) {
            $mask = json_encode($mask);
        }

        $this->mask = false === $mask ? null : $mask;

        return $this;
    }

    /**
     * returns the translated title of the design area for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle()
    {
        $title = '';

        $texts = $this->getDesignAreaText();

        /**
         * @var DesignProductionMethodText $text
         */
        foreach ($texts as $text) {
            if (C_LANGUAGE_ISO === $text->getLanguage()->getIso() && '' !== $text->getTitle()) {
                $title = $text->getTitle();

                break;
            }
        }

        return $title;
    }

    /**
     * @param string $designProductionMethodIdentifier
     *
     * @return int
     */
    public function getMethodWidth(string $designProductionMethodIdentifier): int
    {
        /** @var DesignAreaDesignProductionMethod|false $areaMethod */
        $areaMethod = $this->designAreaDesignProductionMethod->filter(
            static function (DesignAreaDesignProductionMethod $method) use ($designProductionMethodIdentifier) {
                return null !== $method->getDesignProductionMethod() && $method->getDesignProductionMethod()->getIdentifier() === $designProductionMethodIdentifier;
            }
        )->first();

        if (false === $areaMethod) {
            return $this->getWidth();
        }

        return ($areaMethod->getWidth() * $areaMethod->getHeight()) > 0 ? $areaMethod->getWidth() : $this->getWidth();
    }

    /**
     * @param string $designProductionMethodIdentifier
     *
     * @return int
     */
    public function getMethodHeight(string $designProductionMethodIdentifier): int
    {
        /** @var DesignAreaDesignProductionMethod|false $areaMethod */
        $areaMethod = $this->designAreaDesignProductionMethod->filter(
            static function (DesignAreaDesignProductionMethod $method) use ($designProductionMethodIdentifier) {
                return null !== $method->getDesignProductionMethod() && $method->getDesignProductionMethod()->getIdentifier() === $designProductionMethodIdentifier;
            }
        )->first();

        if (false === $areaMethod) {
            return $this->getHeight();
        }

        return ($areaMethod->getWidth() * $areaMethod->getHeight()) > 0 ? $areaMethod->getHeight() : $this->getHeight();
    }
}
