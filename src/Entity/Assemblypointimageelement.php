<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointimageelementRepository")
 * @ORM\Table(name="assemblypointimageelement")
 */
class Assemblypointimageelement
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $elementidentifier;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $is_root;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\AssemblypointimageelementLayer",
     *     mappedBy="assemblypointimageelement"
     * )
     */
    private $assemblypointimageelementLayer;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\AssemblypointimageelementRelation",
     *     mappedBy="assemblypointimageelement"
     * )
     */
    private $assemblypointimageelementRelation;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\AssemblypointimageelementRelation",
     *     mappedBy="parentAssemblypointimageelement"
     * )
     */
    private $parentAssemblypointimageelementRelation;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint",
     *     mappedBy="assemblypointimageelement"
     * )
     */
    private $assemblypoint;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification",
     *     inversedBy="assemblypointimageelement"
     * )
     * @ORM\JoinColumn(name="optionclassification_id", referencedColumnName="id")
     */
    private $optionclassification;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->optionclassificationlayerAssemblypointimageelementImagelayer = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionclassificationAssemblypointimageelementParentoptionclassification = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionclassificationAssemblypointimageelementParent = new \Doctrine\Common\Collections\ArrayCollection();
        $this->assemblypoint = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set elementidentifier.
     *
     * @param string $elementidentifier
     *
     * @return Assemblypointimageelement
     */
    public function setElementidentifier($elementidentifier)
    {
        $this->elementidentifier = $elementidentifier;

        return $this;
    }

    /**
     * Get elementidentifier.
     *
     * @return string
     */
    public function getElementidentifier()
    {
        return $this->elementidentifier;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Assemblypointimageelement
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Assemblypointimageelement
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Assemblypointimageelement
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Assemblypointimageelement
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Assemblypointimageelement
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Assemblypointimageelement
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add assemblypoint.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint $assemblypoint
     *
     * @return Assemblypointimageelement
     */
    public function addAssemblypoint(Assemblypoint $assemblypoint)
    {
        $this->assemblypoint[] = $assemblypoint;

        return $this;
    }

    /**
     * Remove assemblypoint.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint $assemblypoint
     */
    public function removeAssemblypoint(Assemblypoint $assemblypoint)
    {
        $this->assemblypoint->removeElement($assemblypoint);
    }

    /**
     * Get assemblypoint.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssemblypoint()
    {
        return $this->assemblypoint;
    }

    /**
     * Set optionclassification.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification $optionclassification
     *
     * @return Assemblypointimageelement
     */
    public function setOptionclassification(Optionclassification $optionclassification = null)
    {
        $this->optionclassification = $optionclassification;

        return $this;
    }

    /**
     * Get optionclassification.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification
     */
    public function getOptionclassification()
    {
        return $this->optionclassification;
    }

    /**
     * Add assemblypointimageelementLayer.
     *
     * @param AssemblypointimageelementLayer $assemblypointimageelementLayer
     *
     * @return Assemblypointimageelement
     */
    public function addAssemblypointimageelementLayer(AssemblypointimageelementLayer $assemblypointimageelementLayer)
    {
        $this->assemblypointimageelementLayer[] = $assemblypointimageelementLayer;

        return $this;
    }

    /**
     * Remove assemblypointimageelementLayer.
     *
     * @param AssemblypointimageelementLayer $assemblypointimageelementLayer
     */
    public function removeAssemblypointimageelementLayer(AssemblypointimageelementLayer $assemblypointimageelementLayer)
    {
        $this->assemblypointimageelementLayer->removeElement($assemblypointimageelementLayer);
    }

    /**
     * Get assemblypointimageelementLayer.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssemblypointimageelementLayer()
    {
        return $this->assemblypointimageelementLayer;
    }

    /**
     * Add assemblypointimageelementRelation.
     *
     * @param AssemblypointimageelementRelation $assemblypointimageelementRelation
     *
     * @return Assemblypointimageelement
     */
    public function addAssemblypointimageelementRelation(AssemblypointimageelementRelation $assemblypointimageelementRelation)
    {
        $this->assemblypointimageelementRelation[] = $assemblypointimageelementRelation;

        return $this;
    }

    /**
     * Remove assemblypointimageelementRelation.
     *
     * @param AssemblypointimageelementRelation $assemblypointimageelementRelation
     */
    public function removeAssemblypointimageelementRelation(AssemblypointimageelementRelation $assemblypointimageelementRelation)
    {
        $this->assemblypointimageelementRelation->removeElement($assemblypointimageelementRelation);
    }

    /**
     * Get assemblypointimageelementRelation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssemblypointimageelementRelation()
    {
        return $this->assemblypointimageelementRelation;
    }

    /**
     * Set isRoot.
     *
     * @param bool $isRoot
     *
     * @return Assemblypointimageelement
     */
    public function setIsRoot($isRoot)
    {
        $this->is_root = $isRoot;

        return $this;
    }

    /**
     * Get isRoot.
     *
     * @return bool
     */
    public function getIsRoot()
    {
        return $this->is_root;
    }

    /**
     * Add parentAssemblypointimageelementRelation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\AssemblypointimageelementRelation $parentAssemblypointimageelementRelation
     *
     * @return Assemblypointimageelement
     */
    public function addParentAssemblypointimageelementRelation(AssemblypointimageelementRelation $parentAssemblypointimageelementRelation)
    {
        $this->parentAssemblypointimageelementRelation[] = $parentAssemblypointimageelementRelation;

        return $this;
    }

    /**
     * Remove parentAssemblypointimageelementRelation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\AssemblypointimageelementRelation $parentAssemblypointimageelementRelation
     */
    public function removeParentAssemblypointimageelementRelation(AssemblypointimageelementRelation $parentAssemblypointimageelementRelation)
    {
        $this->parentAssemblypointimageelementRelation->removeElement($parentAssemblypointimageelementRelation);
    }

    /**
     * Get parentAssemblypointimageelementRelation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParentAssemblypointimageelementRelation()
    {
        return $this->parentAssemblypointimageelementRelation;
    }
}
