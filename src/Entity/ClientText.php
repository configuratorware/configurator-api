<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ClientTextRepository")
 * @ORM\Table(
 *     name="client_text",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"client_id","channel_id","language_id","date_deleted"})}
 * )
 */
class ClientText
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string|null
     */
    private $data_privacy_link;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $terms_and_conditions_link;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Client", inversedBy="clientText")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", nullable=false)
     */
    private $client;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Channel")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="id", nullable=false)
     */
    private $channel;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=false)
     */
    private $language;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string|null $termsAndConditionsLink
     *
     * @return ClientText
     */
    public function setTermsAndConditionsLink(?string $termsAndConditionsLink): self
    {
        $this->terms_and_conditions_link = $termsAndConditionsLink;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTermsAndConditionsLink(): ?string
    {
        return $this->terms_and_conditions_link;
    }

    /**
     * @param string|null $dataPrivacyLink
     *
     * @return ClientText
     */
    public function setDataPrivacyLink(?string $dataPrivacyLink): self
    {
        $this->data_privacy_link = $dataPrivacyLink;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDataPrivacyLink(): ?string
    {
        return $this->data_privacy_link;
    }

    /**
     * @param DateTime $dateCreated
     *
     * @return ClientText
     */
    public function setDateCreated(DateTime $dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateCreated(): ?DateTime
    {
        return $this->date_created;
    }

    /**
     * @param DateTime|null $dateUpdated
     *
     * @return ClientText
     */
    public function setDateUpdated(?DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateUpdated(): ?DateTime
    {
        return $this->date_updated;
    }

    /**
     * @param DateTime|null $dateDeleted
     *
     * @return ClientText
     */
    public function setDateDeleted(?DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateDeleted(): ?DateTime
    {
        return $this->date_deleted;
    }

    /**
     * @param int|null $userCreatedId
     *
     * @return ClientText
     */
    public function setUserCreatedId(?int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * @param int|null $userUpdatedId
     *
     * @return ClientText
     */
    public function setUserUpdatedId(?int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * @param int|null $userDeletedId
     *
     * @return ClientText
     */
    public function setUserDeletedId(?int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }

    /**
     * @param $client
     *
     * @return ClientText
     */
    public function setClient($client): self
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->client;
    }

    /**
     * @param Channel $channel
     *
     * @return ClientText
     */
    public function setChannel(Channel $channel): self
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * @return Channel
     */
    public function getChannel(): Channel
    {
        return $this->channel;
    }

    /**
     * @param Language $language
     *
     * @return ClientText
     */
    public function setLanguage(Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return Language
     */
    public function getLanguage(): Language
    {
        return $this->language;
    }
}
