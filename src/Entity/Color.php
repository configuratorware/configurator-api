<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ColorRepository")
 * @ORM\Table(name="color")
 */
class Color
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $hex_value;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":null})
     */
    private $sequence_number;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ColorText",
     *     mappedBy="color",
     *     cascade={"persist"}
     * )
     */
    private $colorText;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette", inversedBy="color")
     * @ORM\JoinColumn(name="color_palette_id", referencedColumnName="id")
     */
    private $colorPalette;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->colorText = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set the value of Identifier.
     *
     * @param string $identifier
     *
     * @return Color
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get the value of Hex Value.
     *
     * @return string
     */
    public function getHexValue()
    {
        return $this->hex_value;
    }

    /**
     * Set the value of Hex Value.
     *
     * @param string $hex_value
     *
     * @return Color
     */
    public function setHexValue($hex_value)
    {
        $this->hex_value = $hex_value;

        return $this;
    }

    /**
     * Get the value of Date Created.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of Date Created.
     *
     * @param \DateTime $date_created
     *
     * @return Color
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get the value of Date Updated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of Date Updated.
     *
     * @param \DateTime $date_updated
     *
     * @return Color
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Get the value of Date Deleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set the value of Date Deleted.
     *
     * @param \DateTime $date_deleted
     *
     * @return Color
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;

        return $this;
    }

    /**
     * Get the User who created the record.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set the User who created the record.
     *
     * @param int $user_created_id
     *
     * @return Color
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;

        return $this;
    }

    /**
     * Get the User who updated the record.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set the User who updated the record.
     *
     * @param int $user_updated_id
     *
     * @return Color
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;

        return $this;
    }

    /**
     * Get the User who deleted the record.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set the User who deleted the record.
     *
     * @param int $user_deleted_id
     *
     * @return Color
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;

        return $this;
    }

    /**
     * Get the ColorText.
     *
     * @return Collection
     */
    public function getColorText()
    {
        return $this->colorText;
    }

    /**
     * Add ColorText.
     *
     * @param ColorText $colorText
     *
     * @return Color
     */
    public function addColorText(ColorText $colorText)
    {
        $this->colorText[] = $colorText;

        return $this;
    }

    /**
     * Remove ColorText.
     *
     * @param ColorText $colorText
     */
    public function removeColorText(ColorText $colorText)
    {
        $this->colorText->removeElement($colorText);
    }

    /**
     * Get the Color Palette of this record.
     *
     * @return ColorPalette
     */
    public function getColorPalette()
    {
        return $this->colorPalette;
    }

    /**
     * Set the Color Palette of this record.
     *
     * @param ColorPalette $colorPalette
     *
     * @return Color
     */
    public function setColorPalette(ColorPalette $colorPalette)
    {
        $this->colorPalette = $colorPalette;

        return $this;
    }

    /**
     * returns the translated title for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle()
    {
        $title = '';

        $texts = $this->getColorText();

        /**
         * @var DesignProductionMethodText $text
         */
        foreach ($texts as $text) {
            if (C_LANGUAGE_ISO === $text->getLanguage()->getIso() && '' !== $text->getTitle()) {
                $title = $text->getTitle();

                break;
            }
        }

        return $title;
    }

    /**
     * @return numeric|null
     */
    public function getSequenceNumber()
    {
        return $this->sequence_number;
    }

    /**
     * @param numeric|null $sequence_number
     */
    public function setSequenceNumber($sequence_number): self
    {
        $this->sequence_number = $sequence_number;

        return $this;
    }
}
