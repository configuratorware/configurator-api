<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewDesignAreaRepository")
 * @ORM\Table(
 *     name="creator_view_design_area",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"creator_view_id","design_area_id","date_deleted"})}
 * )
 */
class CreatorViewDesignArea implements ViewRelationInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $base_shape;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $position;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $custom_data;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $is_default;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea",
     *     inversedBy="creatorViewDesignArea"
     * )
     * @ORM\JoinColumn(name="design_area_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea
     */
    private $designArea;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView",
     *     inversedBy="creatorViewDesignArea"
     * )
     * @ORM\JoinColumn(name="creator_view_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView
     */
    private $creatorView;

    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @param CreatorView $creatorView
     * @param DesignArea $designArea
     * @param $baseShape
     * @param $position
     * @param $customData
     * @param bool|null $isDefault
     */
    private function __construct(CreatorView $creatorView, DesignArea $designArea, $baseShape = null, $position = null, $customData = null, bool $isDefault = null)
    {
        $this->creatorView = $creatorView;
        $this->designArea = $designArea;
        $this->base_shape = $baseShape;
        $this->position = $position;
        $this->custom_data = $customData;
        $this->is_default = $isDefault;
        $this->date_created = new DateTime();
        $this->date_deleted = new DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    /**
     * @param CreatorView $creatorView
     * @param DesignArea $designArea
     *
     * @return self
     */
    public static function from(CreatorView $creatorView, DesignArea $designArea): self
    {
        return new self($creatorView, $designArea);
    }

    /**
     * @param CreatorView $creatorView
     * @param DesignArea $designArea
     * @param $baseShape
     * @param $position
     *
     * @return self
     */
    public static function fromDefault(CreatorView $creatorView, DesignArea $designArea, $baseShape = null, $position = null): self
    {
        return new self($creatorView, $designArea, $baseShape, $position);
    }

    /**
     * @param DesignViewDesignArea $designViewDesignArea
     * @param CreatorView $creatorView
     * @param DesignArea $designArea
     *
     * @return self
     */
    public static function fromDesignViewDesignArea(DesignViewDesignArea $designViewDesignArea, CreatorView $creatorView, DesignArea $designArea): self
    {
        return new self($creatorView, $designArea, $designViewDesignArea->getBaseShape(), $designViewDesignArea->getPosition(), $designViewDesignArea->getCustomData(), $designViewDesignArea->getIsDefault());
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getBaseShape()
    {
        $baseShape = $this->base_shape;

        $decoded = json_decode($baseShape);
        if ($decoded) {
            $baseShape = $decoded;
        }

        return $baseShape;
    }

    /**
     * @param string|null $baseShape
     *
     * @return self
     */
    public function setBaseShape($baseShape = null)
    {
        if ($baseShape && !is_string($baseShape)) {
            $baseShape = json_encode($baseShape);
        }

        $this->base_shape = $baseShape;

        return $this;
    }

    /**
     * Set position.
     *
     * @param string|null $position
     *
     * @return self
     */
    public function setPosition($position = null): self
    {
        if ($position && !is_string($position)) {
            $position = json_encode($position);
        }

        $this->position = $position;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPosition()
    {
        $position = $this->position;

        $decoded = json_decode($position);
        if ($decoded) {
            $position = $decoded;
        }

        return $position;
    }

    /**
     * @param string|null $customData
     *
     * @return self
     */
    public function setCustomData($customData = null): self
    {
        if ($customData && !is_string($customData)) {
            $customData = json_encode($customData);
        }

        $this->custom_data = $customData;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomData()
    {
        $customData = $this->custom_data;

        $decoded = json_decode($customData);
        if ($decoded) {
            $customData = $decoded;
        }

        return $customData;
    }

    /**
     * @return bool
     */
    public function getIsDefault(): bool
    {
        return (bool)$this->is_default;
    }

    /**
     * @param bool $is_default
     *
     * @return self
     */
    public function setIsDefault(bool $is_default): self
    {
        $this->is_default = $is_default;

        return $this;
    }

    /**
     * @return DesignArea
     */
    public function getDesignArea(): DesignArea
    {
        return $this->designArea;
    }

    /**
     * @param DesignArea $designArea
     */
    public function setDesignArea(DesignArea $designArea): void
    {
        $this->designArea = $designArea;
    }

    /**
     * @return CreatorView
     */
    public function getCreatorView(): CreatorView
    {
        return $this->creatorView;
    }

    /**
     * @param CreatorView $creatorView
     */
    public function setCreatorView(CreatorView $creatorView): void
    {
        $this->creatorView = $creatorView;
    }

    /**
     * @return ViewInterface|null
     */
    public function getView(): ?ViewInterface
    {
        return $this->creatorView;
    }

    /**
     * @param DateTime $dateCreated
     *
     * @return self
     */
    public function setDateCreated($dateCreated): self
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param DateTime|null $dateUpdated
     *
     * @return self
     */
    public function setDateUpdated($dateUpdated = null): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param DateTime $dateDeleted
     *
     * @return self
     */
    public function setDateDeleted($dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param int|null $userCreatedId
     *
     * @return self
     */
    public function setUserCreatedId($userCreatedId = null): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserCreatedId(): ?int
    {
        return $this->user_created_id;
    }

    /**
     * @param int|null $userUpdatedId
     *
     * @return self
     */
    public function setUserUpdatedId($userUpdatedId = null): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserUpdatedId(): ?int
    {
        return $this->user_updated_id;
    }

    /**
     * @param int|null $userDeletedId
     *
     * @return self
     */
    public function setUserDeletedId($userDeletedId = null): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserDeletedId(): ?int
    {
        return $this->user_deleted_id;
    }
}
