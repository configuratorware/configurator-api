<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\TagRepository")
 * @ORM\Table(name="tag")
 */
class Tag
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $is_multiselect;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $is_background;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $sequence_number;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\TagTranslation",
     *     mappedBy="tag",
     *     cascade={"persist"}
     * )
     */
    private $tagTranslation;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param mixed $date_deleted
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;
    }

    /**
     * @return mixed
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * @param mixed $user_created_id
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;
    }

    /**
     * @return mixed
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * @param mixed $user_updated_id
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;
    }

    /**
     * @return mixed
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * @param mixed $user_deleted_id
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;
    }

    /**
     * Add TagTranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\TagTranslation $tagTranslation
     *
     * @return Tag
     */
    public function addTagTranslation(TagTranslation $tagTranslation)
    {
        $this->tagTranslation[] = $tagTranslation;

        return $this;
    }

    /**
     * Remove TagTranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\TagTranslation $tagTranslation
     */
    public function removeTagTranslation(TagTranslation $tagTranslation)
    {
        $this->tagTranslation->removeElement($tagTranslation);
    }

    /**
     * @return mixed
     */
    public function getTagTranslation()
    {
        return $this->tagTranslation;
    }

    /**
     * @param mixed $tagTranslation
     */
    public function setTagTranslation($tagTranslation)
    {
        $this->tagTranslation = $tagTranslation;
    }

    /**
     * returns the translated title of the Tag for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @return  string
     */
    public function getTranslatedTitle()
    {
        $strTranslation = '';

        $arrTranslations = $this->getTagTranslation();

        /*
         * @var $objTranslation TagTranslation
         */
        if (!empty($arrTranslations)) {
            foreach ($arrTranslations as $objTranslation) {
                if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                        ->getIso() && '' != $objTranslation->getTranslation()
                ) {
                    $strTranslation = $objTranslation->getTranslation();

                    break;
                }
            }
        }

        return $strTranslation;
    }

    /**
     * @return mixed
     */
    public function getisMultiselect()
    {
        return $this->is_multiselect;
    }

    /**
     * @param mixed $is_multiselect
     */
    public function setIsMultiselect($is_multiselect)
    {
        $this->is_multiselect = $is_multiselect;
    }

    /**
     * @return mixed
     */
    public function getisBackground()
    {
        return $this->is_background;
    }

    /**
     * @param mixed $is_background
     */
    public function setIsBackground($is_background)
    {
        $this->is_background = $is_background;
    }

    /**
     * @return int
     */
    public function getSequenceNumber(): ?int
    {
        return $this->sequence_number;
    }

    /**
     * @param int $sequence_number
     *
     * @return Tag
     */
    public function setSequenceNumber(int $sequence_number): Tag
    {
        $this->sequence_number = $sequence_number;

        return $this;
    }
}
