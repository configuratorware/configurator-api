<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointimageelementRelationRepository")
 * @ORM\Table(name="assemblypointimageelementrelation")
 */
class AssemblypointimageelementRelation
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default":0})
     */
    private $sequencenumber;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypointimageelement",
     *     inversedBy="assemblypointimageelementRelation"
     * )
     * @ORM\JoinColumn(name="assemblypointimageelement_id", referencedColumnName="id")
     */
    private $assemblypointimageelement;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypointimageelement",
     *     inversedBy="parentAssemblypointimageelementRelation"
     * )
     * @ORM\JoinColumn(name="parent_assemblypointimageelement_id", referencedColumnName="id")
     */
    private $parentAssemblypointimageelement;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sequencenumber.
     *
     * @param int $sequencenumber
     *
     * @return AssemblypointimageelementRelation
     */
    public function setSequencenumber($sequencenumber)
    {
        $this->sequencenumber = $sequencenumber;

        return $this;
    }

    /**
     * Get sequencenumber.
     *
     * @return int
     */
    public function getSequencenumber()
    {
        return $this->sequencenumber;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return AssemblypointimageelementRelation
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return AssemblypointimageelementRelation
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return AssemblypointimageelementRelation
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return AssemblypointimageelementRelation
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return AssemblypointimageelementRelation
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return AssemblypointimageelementRelation
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set assemblypointimageelement.
     *
     * @param Assemblypointimageelement $assemblypointimageelement
     *
     * @return AssemblypointimageelementRelation
     */
    public function setAssemblypointimageelement(Assemblypointimageelement $assemblypointimageelement = null)
    {
        $this->assemblypointimageelement = $assemblypointimageelement;

        return $this;
    }

    /**
     * Get assemblypointimageelement.
     *
     * @return Assemblypointimageelement
     */
    public function getAssemblypointimageelement()
    {
        return $this->assemblypointimageelement;
    }

    /**
     * Set parentAssemblypointimageelement.
     *
     * @param Assemblypointimageelement $parentAssemblypointimageelement
     *
     * @return AssemblypointimageelementRelation
     */
    public function setParentAssemblypointimageelement(Assemblypointimageelement $parentAssemblypointimageelement = null)
    {
        $this->parentAssemblypointimageelement = $parentAssemblypointimageelement;

        return $this;
    }

    /**
     * Get parentAssemblypointimageelement.
     *
     * @return Assemblypointimageelement
     */
    public function getParentAssemblypointimageelement()
    {
        return $this->parentAssemblypointimageelement;
    }
}
