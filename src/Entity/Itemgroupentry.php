<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository")
 * @ORM\Table(
 *     name="itemgroupentry",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","identifier"})}
 * )
 */
class Itemgroupentry
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":0})
     */
    private $sequencenumber;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentrytranslation",
     *     mappedBy="itemgroupentry",
     *     cascade={"persist"}
     * )
     */
    private $itemgroupentrytranslation;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry",
     *     mappedBy="itemgroupentry"
     * )
     */
    private $itemItemgroupentry;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroupEntry", mappedBy="groupEntry")
     */
    private $optionGroupEntry;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->itemgroupentrytranslation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->itemItemgroupentry = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionGroupEntry = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Itemgroupentry
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set sequencenumber.
     *
     * @param int $sequencenumber
     *
     * @return Itemgroupentry
     */
    public function setSequencenumber($sequencenumber)
    {
        $this->sequencenumber = $sequencenumber;

        return $this;
    }

    /**
     * Get sequencenumber.
     *
     * @return int
     */
    public function getSequencenumber()
    {
        return $this->sequencenumber;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Itemgroupentry
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Itemgroupentry
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Itemgroupentry
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Itemgroupentry
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Itemgroupentry
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Itemgroupentry
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add itemgroupentrytranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentrytranslation $itemgroupentrytranslation
     *
     * @return Itemgroupentry
     */
    public function addItemgroupentrytranslation(Itemgroupentrytranslation $itemgroupentrytranslation)
    {
        $this->itemgroupentrytranslation[] = $itemgroupentrytranslation;

        return $this;
    }

    /**
     * Remove itemgroupentrytranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentrytranslation $itemgroupentrytranslation
     */
    public function removeItemgroupentrytranslation(Itemgroupentrytranslation $itemgroupentrytranslation)
    {
        $this->itemgroupentrytranslation->removeElement($itemgroupentrytranslation);
    }

    /**
     * Get itemgroupentrytranslation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemgroupentrytranslation()
    {
        return $this->itemgroupentrytranslation;
    }

    /**
     * Add itemItemgroupentry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry $itemItemgroupentry
     *
     * @return Itemgroupentry
     */
    public function addItemItemgroupentry(ItemItemgroupentry $itemItemgroupentry)
    {
        $this->itemItemgroupentry[] = $itemItemgroupentry;

        return $this;
    }

    /**
     * Remove itemItemgroupentry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry $itemItemgroupentry
     */
    public function removeItemItemgroupentry(ItemItemgroupentry $itemItemgroupentry)
    {
        $this->itemItemgroupentry->removeElement($itemItemgroupentry);
    }

    /**
     * Get itemItemgroupentry.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemItemgroupentry()
    {
        return $this->itemItemgroupentry;
    }

    /**
     * Add optionGroupEntry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroupEntry $optionGroupEntry
     *
     * @return Itemgroupentry
     */
    public function addOptionGroupEntry(OptionGroupEntry $optionGroupEntry)
    {
        $this->optionGroupEntry[] = $optionGroupEntry;

        return $this;
    }

    /**
     * Remove optionGroupEntry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroupEntry $optionGroupEntry
     */
    public function removeOptionGroupEntry(OptionGroupEntry $optionGroupEntry)
    {
        $this->optionGroupEntry->removeElement($optionGroupEntry);
    }

    /**
     * Get optionGroupEntry.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionGroupEntry()
    {
        return $this->optionGroupEntry;
    }

    /**
     * returns the translated title of the Itemgroupentry for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>     *
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  string
     */
    public function getTranslatedTitle()
    {
        $strTranslation = $this->getIdentifier();

        $arrTranslations = $this->getItemgroupentrytranslation();

        /**
         * @var $objTranslation Itemgrouptranslation
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getTranslation()
            ) {
                $strTranslation = $objTranslation->getTranslation();

                break;
            }
        }

        return $strTranslation;
    }
}
