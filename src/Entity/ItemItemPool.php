<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="item_item_pool",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","item_id","item_pool_id"})}
 * )
 */
class ItemItemPool
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item", inversedBy="itemItemPool")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemPool", inversedBy="itemItemPool")
     * @ORM\JoinColumn(name="item_pool_id", referencedColumnName="id")
     */
    private $itemPool;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return ItemItemPool
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime|null $dateUpdated
     *
     * @return ItemItemPool
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return ItemItemPool
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return ItemItemPool
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return ItemItemPool
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return ItemItemPool
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set item.
     *
     * @param Item|null $item
     *
     * @return ItemItemPool
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return Item|null
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set itemPool.
     *
     * @param ItemPool|null $itemPool
     *
     * @return ItemItemPool
     */
    public function setItemPool(ItemPool $itemPool = null)
    {
        $this->itemPool = $itemPool;

        return $this;
    }

    /**
     * Get itemPool.
     *
     * @return ItemPool|null
     */
    public function getItemPool()
    {
        return $this->itemPool;
    }
}
