<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * DesignViewDesignArea.
 *
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewDesignAreaRepository")
 * @ORM\Table(
 *     name="design_view_design_area",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"design_view_id","design_area_id","date_deleted"})}
 * )
 */
class DesignViewDesignArea implements ViewRelationInterface
{
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @var
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO") int
     */
    private $id;

    /**
     * @var
     * @ORM\Column(type="text", nullable=true) string|null
     */
    private $base_shape;

    /**
     * @var
     * @ORM\Column(type="text", nullable=true) string|null
     */
    private $position;

    /**
     * @var
     * @ORM\Column(type="text", nullable=true) string|null
     */
    private $custom_data;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $is_default;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false) \DateTime
     */
    private $date_created;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=true) \DateTime|null
     */
    private $date_updated;

    /**
     * @var
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"}) \DateTime
     */
    private $date_deleted;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_created_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_updated_id;

    /**
     * @var
     * @ORM\Column(type="bigint", nullable=true) int|null
     */
    private $user_deleted_id;

    /**
     * @var
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignView",
     *     inversedBy="designViewDesignArea"
     * )
     * @ORM\JoinColumn(name="design_view_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\DesignView
     */
    private $designView;

    /**
     * @var
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea",
     *     inversedBy="designViewDesignArea"
     * )
     * @ORM\JoinColumn(name="design_area_id", referencedColumnName="id") \Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea
     */
    private $designArea;

    /**
     * @param DesignView|null $designView
     * @param DesignArea|null $designArea
     * @param $baseShape
     * @param $position
     * @param $customData
     * @param bool|null $isDefault
     */
    public function __construct(DesignView $designView = null, DesignArea $designArea = null, $baseShape = null, $position = null, $customData = null, bool $isDefault = null)
    {
        $this->designView = $designView;
        $this->designArea = $designArea;
        $this->base_shape = $baseShape;
        $this->position = $position;
        $this->custom_data = $customData;
        $this->is_default = $isDefault;
        $this->date_created = new DateTime();
        $this->date_deleted = new DateTime(EntityListener::DATE_DELETED_DEFAULT);
    }

    /**
     * @param CreatorViewDesignArea $creatorViewDesignArea
     * @param DesignView $designView
     * @param DesignArea $designArea
     *
     * @return DesignViewDesignArea
     */
    public static function fromCreatorViewDesignArea(CreatorViewDesignArea $creatorViewDesignArea, DesignView $designView, DesignArea $designArea): DesignViewDesignArea
    {
        return new self($designView, $designArea, $creatorViewDesignArea->getBaseShape(), $creatorViewDesignArea->getPosition(), $creatorViewDesignArea->getCustomData(), $creatorViewDesignArea->getIsDefault());
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set baseShape.
     *
     * @param string|null $baseShape
     *
     * @return DesignViewDesignArea
     */
    public function setBaseShape($baseShape = null)
    {
        if ($baseShape && !is_string($baseShape)) {
            $baseShape = json_encode($baseShape);
        }

        $this->base_shape = $baseShape;

        return $this;
    }

    /**
     * Get baseShape.
     *
     * @return string|null
     */
    public function getBaseShape()
    {
        $baseShape = $this->base_shape;

        $decoded = json_decode($baseShape);
        if ($decoded) {
            $baseShape = $decoded;
        }

        return $baseShape;
    }

    /**
     * Set position.
     *
     * @param string|null $position
     *
     * @return DesignViewDesignArea
     */
    public function setPosition($position = null)
    {
        if ($position && !is_string($position)) {
            $position = json_encode($position);
        }

        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string|null
     */
    public function getPosition()
    {
        $position = $this->position;

        $decoded = json_decode($position);
        if ($decoded) {
            $position = $decoded;
        }

        return $position;
    }

    /**
     * Set customData.
     *
     * @param string|null $customData
     *
     * @return DesignViewDesignArea
     */
    public function setCustomData($customData = null)
    {
        if ($customData && !is_string($customData)) {
            $customData = json_encode($customData);
        }

        $this->custom_data = $customData;

        return $this;
    }

    /**
     * Get customData.
     *
     * @return string|null
     */
    public function getCustomData()
    {
        $customData = $this->custom_data;

        $decoded = json_decode($customData);
        if ($decoded) {
            $customData = $decoded;
        }

        return $customData;
    }

    /**
     * Set isDefault.
     *
     * @param bool $isDefault
     *
     * @return DesignViewDesignArea
     */
    public function setIsDefault($isDefault): DesignViewDesignArea
    {
        $this->is_default = $isDefault;

        return $this;
    }

    /**
     * Get isDefault.
     *
     * @return bool
     */
    public function getIsDefault(): bool
    {
        return (bool)$this->is_default;
    }

    /**
     * Set dateCreated.
     *
     * @param DateTime $dateCreated
     *
     * @return DesignViewDesignArea
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param DateTime|null $dateUpdated
     *
     * @return DesignViewDesignArea
     */
    public function setDateUpdated($dateUpdated = null)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return DateTime|null
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param DateTime $dateDeleted
     *
     * @return DesignViewDesignArea
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int|null $userCreatedId
     *
     * @return DesignViewDesignArea
     */
    public function setUserCreatedId($userCreatedId = null)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int|null
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int|null $userUpdatedId
     *
     * @return DesignViewDesignArea
     */
    public function setUserUpdatedId($userUpdatedId = null)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int|null
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int|null $userDeletedId
     *
     * @return DesignViewDesignArea
     */
    public function setUserDeletedId($userDeletedId = null)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int|null
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set designView.
     *
     * @param DesignView|null $designView
     *
     * @return DesignViewDesignArea
     */
    public function setDesignView(DesignView $designView = null)
    {
        $this->designView = $designView;

        return $this;
    }

    /**
     * Get designView.
     *
     * @return DesignView|null
     */
    public function getDesignView()
    {
        return $this->designView;
    }

    /**
     * @return ViewInterface|null
     */
    public function getView(): ?ViewInterface
    {
        return $this->designView;
    }

    /**
     * Set designArea.
     *
     * @param DesignArea|null $designArea
     *
     * @return DesignViewDesignArea
     */
    public function setDesignArea(DesignArea $designArea = null)
    {
        $this->designArea = $designArea;

        return $this;
    }

    /**
     * Get designArea.
     *
     * @return DesignArea
     */
    public function getDesignArea(): DesignArea
    {
        return $this->designArea;
    }
}
