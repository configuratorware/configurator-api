<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\InspirationRepository")
 * @ORM\Table(name="inspiration")
 */
class Inspiration
{
    /**
     * @var int|null
     *
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, options={"default":0})
     */
    private $active = false;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $configuration_code;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $thumbnail;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @var int
     *
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @var Collection<int, InspirationText>
     *
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\InspirationText",
     *     mappedBy="inspiration",
     *     cascade={"persist"}
     * )
     */
    private $inspirationText;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item")
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @param Item $item
     */
    private function __construct(Item $item)
    {
        $this->date_created = new \DateTime();
        $this->date_deleted = new \DateTime('0001-01-01 00:00:00');
        $this->inspirationText = new ArrayCollection();
        $this->item = $item;
    }

    /**
     * @param Item $item
     *
     * @return self
     */
    public static function forItem(Item $item): self
    {
        return new self($item);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @return string|null
     */
    public function getConfigurationCode(): ?string
    {
        return $this->configuration_code;
    }

    /**
     * @return string|null
     */
    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    /**
     * @return Item
     */
    public function getItem(): Item
    {
        return $this->item;
    }

    /**
     * @return Collection<int, InspirationText>
     */
    public function getInspirationText(): Collection
    {
        return $this->inspirationText;
    }

    /**
     * @param bool $active
     *
     * @return self
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @param string|null $configuration_code
     *
     * @return self
     */
    public function setConfigurationCode(?string $configuration_code): self
    {
        $this->configuration_code = $configuration_code;

        return $this;
    }

    /**
     * @param string|null $thumbnail
     */
    public function setThumbnail(?string $thumbnail): void
    {
        $this->thumbnail = $thumbnail;
    }

    /**
     * @param InspirationText $inspirationText
     *
     * @return Inspiration
     */
    public function removeInspirationText(InspirationText $inspirationText): self
    {
        $inspirationText->setDateDeleted(new \DateTime());

        return $this;
    }

    /**
     * @param InspirationText $inspirationText
     *
     * @return Inspiration
     */
    public function addInspirationText(InspirationText $inspirationText): self
    {
        $this->inspirationText->add($inspirationText);

        return $this;
    }

    /**
     * @param \DateTime $dateUpdated
     *
     * @return self
     */
    public function setDateUpdated(\DateTime $dateUpdated): self
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * @param \DateTime $dateDeleted
     *
     * @return self
     */
    public function setDateDeleted(\DateTime $dateDeleted): self
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * @param int $userCreatedId
     *
     * @return self
     */
    public function setUserCreatedId(int $userCreatedId): self
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * @param int $userUpdatedId
     *
     * @return self
     */
    public function setUserUpdatedId(int $userUpdatedId): self
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * @param int $userDeletedId
     *
     * @return self
     */
    public function setUserDeletedId(int $userDeletedId): self
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }
}
