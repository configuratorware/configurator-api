<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ColorPaletteRepository")
 * @ORM\Table(name="color_palette")
 */
class ColorPalette
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ColorPaletteText",
     *     mappedBy="colorPalette",
     *     cascade={"persist"}
     * )
     */
    private $colorPaletteText;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Color",
     *     mappedBy="colorPalette",
     *     cascade={"persist"}
     * )
     */
    private $color;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette",
     *     mappedBy="colorPalette",
     *     cascade={"persist"}
     * ) \Doctrine\Common\Collections\Collection
     */
    private $designProductionMethodColorPalette;

    /**
     * @var
     * @ORM\JoinColumn(name="default_color_id", referencedColumnName="id", unique=true)
     * @ORM\OneToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Color") Color
     */
    private $defaultColor;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->color = new ArrayCollection();
        $this->colorPaletteText = new ArrayCollection();
        $this->designProductionMethodColorPalette = new ArrayCollection();
    }

    /**
     * Get the Id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of Identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set the value of Identifier.
     *
     * @param string $identifier
     *
     * @return ColorPalette
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get the value of Date Created.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set the value of Date Created.
     *
     * @param \DateTime $date_created
     *
     * @return ColorPalette
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Get the value of Date Updated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set the value of Date Updated.
     *
     * @param \DateTime $date_updated
     *
     * @return ColorPalette
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Get the value of Date Deleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set the value of Date Deleted.
     *
     * @param \DateTime $date_deleted
     *
     * @return ColorPalette
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;

        return $this;
    }

    /**
     * Get the User who created the record.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set the User who created the record.
     *
     * @param int $user_created_id
     *
     * @return ColorPalette
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;

        return $this;
    }

    /**
     * Get the User who did the last update.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set the User who did the last update.
     *
     * @param int $user_updated_id
     *
     * @return ColorPalette
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;

        return $this;
    }

    /**
     * Get the User who deleted the record.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set the User who deleted the record.
     *
     * @param int $user_deleted_id
     *
     * @return ColorPalette
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;

        return $this;
    }

    /**
     * Get ColorPaletteText.
     *
     * @return Collection
     */
    public function getColorPaletteText()
    {
        return $this->colorPaletteText;
    }

    /**
     * Add ColorPaletteText.
     *
     * @param ColorPaletteText $colorPaletteText
     *
     * @return ColorPalette
     */
    public function addColorPaletteText(ColorPaletteText $colorPaletteText)
    {
        $this->colorPaletteText[] = $colorPaletteText;

        return $this;
    }

    /**
     * Remove ColorPaletteText.
     *
     * @param ColorPaletteText $colorPaletteText
     */
    public function removeColorPaletteText(ColorPaletteText $colorPaletteText)
    {
        $this->colorPaletteText->removeElement($colorPaletteText);
    }

    /**
     * Get the Color.
     *
     * @return Collection
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add Color.
     *
     * @param Color $color
     *
     * @return ColorPalette
     */
    public function addColor(Color $color)
    {
        $this->color[] = $color;

        return $this;
    }

    /**
     * Remove Color.
     *
     * @param Color $color
     */
    public function removeColor(Color $color)
    {
        $this->color->removeElement($color);
    }

    /**
     * Get designProductionMethodColorPalette.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDesignProductionMethodColorPalette()
    {
        return $this->designProductionMethodColorPalette;
    }

    /**
     * Add designProductionMethodColorPalette.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette $designProductionMethodColorPalette
     *
     * @return ColorPalette
     */
    public function addDesignProductionMethodColorPalette(
        DesignProductionMethodColorPalette $designProductionMethodColorPalette
    ) {
        $this->designProductionMethodColorPalette[] = $designProductionMethodColorPalette;

        return $this;
    }

    /**
     * Remove designProductionMethodColorPalette.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette $designProductionMethodColorPalette
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeDesignProductionMethodColorPalette(
        DesignProductionMethodColorPalette $designProductionMethodColorPalette
    ) {
        return $this->designProductionMethodColorPalette->removeElement($designProductionMethodColorPalette);
    }

    /**
     * returns the translated title of the color palette for the current Language.
     *
     * @return string
     */
    public function getTranslatedTitle()
    {
        $title = '';

        $texts = $this->getColorPaletteText();

        /**
         * @var ColorPaletteText $text
         */
        foreach ($texts as $text) {
            if (C_LANGUAGE_ISO == $text->getLanguage()->getIso() && '' != $text->getTitle()) {
                $title = $text->getTitle();

                break;
            }
        }

        return $title;
    }

    /**
     * @return Color|null
     */
    public function getDefaultColor(): ?Color
    {
        return $this->defaultColor;
    }

    /**
     * @param Color|null $defaultColor
     *
     * @return ColorPalette
     */
    public function setDefaultColor(?Color $defaultColor): self
    {
        $this->defaultColor = $defaultColor;

        return $this;
    }
}
