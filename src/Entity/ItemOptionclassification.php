<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationRepository")
 * @ORM\Table(
 *     name="item_optionclassification",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"item_id","date_deleted","optionclassification_id"})}
 * )
 */
class ItemOptionclassification
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $is_multiselect;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $minamount;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $maxamount;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":1})
     */
    private $is_mandatory;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $sequence_number;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption",
     *     mappedBy="itemOptionclassification",
     *     cascade={"persist"}
     * )
     */
    private $itemOptionclassificationOption;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Item",
     *     inversedBy="itemOptionclassification"
     * )
     * @ORM\JoinColumn(name="item_id", referencedColumnName="id")
     */
    private $item;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification",
     *     inversedBy="itemOptionclassification"
     * )
     * @ORM\JoinColumn(name="optionclassification_id", referencedColumnName="id")
     */
    private $optionclassification;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView", cascade={"persist"})
     * @ORM\JoinColumn(name="creator_view_id", referencedColumnName="id")
     */
    private $creatorView;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->itemReplacementoptionclassificationReplacementoption = new ArrayCollection();
        $this->itemOptionclassificationOption = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isMultiselect.
     *
     * @param bool $isMultiselect
     *
     * @return ItemOptionclassification
     */
    public function setIsMultiselect($isMultiselect)
    {
        $this->is_multiselect = $isMultiselect;

        return $this;
    }

    /**
     * Get isMultiselect.
     *
     * @return bool
     */
    public function getIsMultiselect()
    {
        return $this->is_multiselect;
    }

    /**
     * @return int|null
     */
    public function getSequenceNumber(): ?int
    {
        return $this->sequence_number;
    }

    /**
     * @param int|null $sequenceNumber
     */
    public function setSequenceNumber(?int $sequenceNumber): self
    {
        $this->sequence_number = $sequenceNumber;

        return $this;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return ItemOptionclassification
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return ItemOptionclassification
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return ItemOptionclassification
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return ItemOptionclassification
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return ItemOptionclassification
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return ItemOptionclassification
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set item.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     *
     * @return ItemOptionclassification
     */
    public function setItem(Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get item.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set optionclassification.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification $optionclassification
     *
     * @return ItemOptionclassification
     */
    public function setOptionclassification(
        Optionclassification $optionclassification = null
    ) {
        $this->optionclassification = $optionclassification;

        return $this;
    }

    /**
     * Get optionclassification.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification
     */
    public function getOptionclassification()
    {
        return $this->optionclassification;
    }

    /**
     * Set minamount.
     *
     * @param int $minamount
     *
     * @return ItemOptionclassification
     */
    public function setMinamount($minamount)
    {
        $this->minamount = $minamount;

        return $this;
    }

    /**
     * Get minamount.
     *
     * @return int
     */
    public function getMinamount()
    {
        return $this->minamount;
    }

    /**
     * Set maxamount.
     *
     * @param int $maxamount
     *
     * @return ItemOptionclassification
     */
    public function setMaxamount($maxamount)
    {
        $this->maxamount = $maxamount;

        return $this;
    }

    /**
     * Get maxamount.
     *
     * @return int
     */
    public function getMaxamount()
    {
        return $this->maxamount;
    }

    /**
     * @param ItemOptionclassificationOption $itemOptionClassificationOption
     *
     * @return ItemOptionclassification
     */
    public function addItemOptionclassificationOption(ItemOptionclassificationOption $itemOptionClassificationOption): self
    {
        $itemOptionClassificationOption->setItemOptionclassification($this);

        $this->itemOptionclassificationOption[] = $itemOptionClassificationOption;

        return $this;
    }

    /**
     * @param ItemOptionclassificationOption $itemOptionClassificationOption
     */
    public function removeItemOptionclassificationOption(ItemOptionclassificationOption $itemOptionClassificationOption): void
    {
        $key = $this->itemOptionclassificationOption->indexOf($itemOptionClassificationOption);

        /** @var ItemOptionclassificationOption $element */
        $element = $this->itemOptionclassificationOption->get($key);

        foreach ($element->getItemOptionclassificationOptionDeltaprice() as $deltaPrice) {
            $element->removeItemOptionclassificationOptionDeltaprice($deltaPrice);
        }

        $element->setDateDeleted(new \DateTime());
    }

    /**
     * @return Collection<int, ItemOptionclassificationOption>
     */
    public function getItemOptionclassificationOption()
    {
        return $this->itemOptionclassificationOption;
    }

    /**
     * @return mixed
     */
    public function getIsMandatory()
    {
        return $this->is_mandatory;
    }

    /**
     * @param mixed $is_mandatory
     */
    public function setIsMandatory($is_mandatory)
    {
        $this->is_mandatory = $is_mandatory;
    }

    /**
     * @return CreatorView|null
     */
    public function getCreatorView(): ?CreatorView
    {
        return $this->creatorView;
    }

    /**
     * @param CreatorView|null $creatorView
     */
    public function setCreatorView(?CreatorView $creatorView): void
    {
        $this->creatorView = $creatorView;
    }
}
