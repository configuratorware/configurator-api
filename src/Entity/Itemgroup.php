<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository")
 * @ORM\Table(
 *     name="itemgroup",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"identifier","date_deleted"})}
 * )
 */
class Itemgroup
{
    public const IDENTIFIER_COLOR_GROUP = 'color';

    public const IDENTIFIER_SIZE_GROUP = 'size';
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="bigint", nullable=true, options={"default":0})
     */
    private $sequencenumber;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     *
     * @deprecated use item pools instead
     */
    private $configurationvariant;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemgrouptranslation",
     *     mappedBy="itemgroup",
     *     cascade={"persist"}
     * )
     */
    private $itemgrouptranslation;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup",
     *     mappedBy="itemgroup",
     *     cascade={"persist"}
     * )
     */
    private $itemItemgroup;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry", mappedBy="itemgroup")
     */
    private $itemItemgroupentry;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroup", mappedBy="group")
     */
    private $optionGroup;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroupEntry", mappedBy="group")
     */
    private $optionGroupEntry;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Itemgroup
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Itemgroup
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Itemgroup
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Itemgroup
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Itemgroup
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Itemgroup
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Itemgroup
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->itemgrouptranslation = new \Doctrine\Common\Collections\ArrayCollection();
        $this->itemItemgroup = new \Doctrine\Common\Collections\ArrayCollection();
        $this->itemItemgroupentry = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionGroup = new \Doctrine\Common\Collections\ArrayCollection();
        $this->optionGroupEntry = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set sequencenumber.
     *
     * @param int $sequencenumber
     *
     * @return Itemgroup
     */
    public function setSequencenumber($sequencenumber)
    {
        $this->sequencenumber = $sequencenumber;

        return $this;
    }

    /**
     * Get sequencenumber.
     *
     * @return int
     */
    public function getSequencenumber()
    {
        return $this->sequencenumber;
    }

    /**
     * Add itemgrouptranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgrouptranslation $itemgrouptranslation
     *
     * @return Itemgroup
     */
    public function addItemgrouptranslation(Itemgrouptranslation $itemgrouptranslation)
    {
        $this->itemgrouptranslation[] = $itemgrouptranslation;

        return $this;
    }

    /**
     * Remove itemgrouptranslation.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemgrouptranslation $itemgrouptranslation
     */
    public function removeItemgrouptranslation(Itemgrouptranslation $itemgrouptranslation)
    {
        $this->itemgrouptranslation->removeElement($itemgrouptranslation);
    }

    /**
     * Get itemgrouptranslation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemgrouptranslation()
    {
        return $this->itemgrouptranslation;
    }

    /**
     * Add itemItemgroup.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup $itemItemgroup
     *
     * @return Itemgroup
     */
    public function addItemItemgroup(ItemItemgroup $itemItemgroup)
    {
        $this->itemItemgroup[] = $itemItemgroup;

        return $this;
    }

    /**
     * Remove itemItemgroup.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup $itemItemgroup
     */
    public function removeItemItemgroup(ItemItemgroup $itemItemgroup)
    {
        $this->itemItemgroup->removeElement($itemItemgroup);
    }

    /**
     * Get itemItemgroup.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemItemgroup()
    {
        return $this->itemItemgroup;
    }

    /**
     * Add optionGroup.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroup $optionGroup
     *
     * @return Itemgroup
     */
    public function addOptionGroup(OptionGroup $optionGroup)
    {
        $this->optionGroup[] = $optionGroup;

        return $this;
    }

    /**
     * Remove optionGroup.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroup $optionGroup
     */
    public function removeOptionGroup(OptionGroup $optionGroup)
    {
        $this->optionGroup->removeElement($optionGroup);
    }

    /**
     * Get optionGroup.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionGroup()
    {
        return $this->optionGroup;
    }

    /**
     * Add itemItemgroupentry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry $itemItemgroupentry
     *
     * @return Itemgroup
     */
    public function addItemItemgroupentry(ItemItemgroupentry $itemItemgroupentry)
    {
        $this->itemItemgroupentry[] = $itemItemgroupentry;

        return $this;
    }

    /**
     * Remove itemItemgroupentry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry $itemItemgroupentry
     */
    public function removeItemItemgroupentry(ItemItemgroupentry $itemItemgroupentry)
    {
        $this->itemItemgroupentry->removeElement($itemItemgroupentry);
    }

    /**
     * Get itemItemgroupentry.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItemItemgroupentry()
    {
        return $this->itemItemgroupentry;
    }

    /**
     * Add optionGroupEntry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroupEntry $optionGroupEntry
     *
     * @return Itemgroup
     */
    public function addOptionGroupEntry(OptionGroupEntry $optionGroupEntry)
    {
        $this->optionGroupEntry[] = $optionGroupEntry;

        return $this;
    }

    /**
     * Remove optionGroupEntry.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionGroupEntry $optionGroupEntry
     */
    public function removeOptionGroupEntry(OptionGroupEntry $optionGroupEntry)
    {
        $this->optionGroupEntry->removeElement($optionGroupEntry);
    }

    /**
     * Get optionGroupEntry.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionGroupEntry()
    {
        return $this->optionGroupEntry;
    }

    /**
     * Set configurationvariant.
     *
     * @deprecated use item pools instead
     *
     * @param bool $configurationvariant
     *
     * @return Itemgroup
     */
    public function setConfigurationvariant($configurationvariant)
    {
        $this->configurationvariant = $configurationvariant;

        return $this;
    }

    /**
     * Get configurationvariant.
     *
     * @deprecated use item pools instead
     *
     * @return bool
     */
    public function getConfigurationvariant()
    {
        return $this->configurationvariant;
    }

    /**
     * returns the translated title of the Itemgroup for the current Language.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>     *
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @return  string
     */
    public function getTranslatedTitle()
    {
        $strTranslation = $this->getIdentifier();

        $arrTranslations = $this->getItemgrouptranslation();

        /**
         * @var $objTranslation Itemgrouptranslation
         */
        foreach ($arrTranslations as $objTranslation) {
            if (C_LANGUAGE_ISO == $objTranslation->getLanguage()
                    ->getIso() && '' != $objTranslation->getTranslation()
            ) {
                $strTranslation = $objTranslation->getTranslation();

                break;
            }
        }

        return $strTranslation;
    }
}
