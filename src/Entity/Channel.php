<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository")
 * @ORM\Table(
 *     name="channel",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"identifier","date_deleted"})}
 * )
 */
class Channel
{
    public const DEFAULT_IDENTIFIER = '_default';

    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $settings = [];

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default":0})
     */
    private $is_default;

    /**
     * @ORM\Column(type="decimal", nullable=true, precision=15, scale=5)
     */
    private $global_discount_percentage;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Stock", mappedBy="channel")
     */
    private $stock;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel", mappedBy="channel")
     */
    private $clientChannel;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->stock = new \Doctrine\Common\Collections\ArrayCollection();
        $this->clientChannel = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set settings.
     *
     * @param array $settings
     *
     * @return Channel
     */
    public function setSettings($settings)
    {
        $this->settings = $settings;

        return $this;
    }

    /**
     * Get settings.
     *
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return Channel
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return Channel
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return Channel
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return Channel
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return Channel
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return Channel
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add stock.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Stock $stock
     *
     * @return Channel
     */
    public function addStock(Stock $stock)
    {
        $this->stock[] = $stock;

        return $this;
    }

    /**
     * Remove stock.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Stock $stock
     */
    public function removeStock(Stock $stock)
    {
        $this->stock->removeElement($stock);
    }

    /**
     * Get stock.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Set currency.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Currency $currency
     *
     * @return Channel
     */
    public function setCurrency(Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Returns the currency id.
     *
     * @return int|null
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     */
    public function getCurrencyId()
    {
        $currencyId = null;

        if (!empty($this->currency)) {
            $currencyId = $this->currency->getId();
        }

        return $currencyId;
    }

    /**
     * Set identifier.
     *
     * @param string $identifier
     *
     * @return Channel
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set isDefault.
     *
     * @param bool $isDefault
     *
     * @return Channel
     */
    public function setIsDefault($isDefault)
    {
        $this->is_default = $isDefault;

        return $this;
    }

    /**
     * Get isDefault.
     *
     * @return bool
     */
    public function getIsDefault()
    {
        return $this->is_default;
    }

    /**
     * Add clientChannel.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel $clientChannel
     *
     * @return Channel
     */
    public function addClientChannel(ClientChannel $clientChannel)
    {
        $this->clientChannel[] = $clientChannel;

        return $this;
    }

    /**
     * Remove clientChannel.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel $clientChannel
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeClientChannel(ClientChannel $clientChannel)
    {
        return $this->clientChannel->removeElement($clientChannel);
    }

    /**
     * Get clientChannel.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientChannel()
    {
        return $this->clientChannel;
    }

    /**
     * @return mixed
     */
    public function getGlobalDiscountPercentage()
    {
        return $this->global_discount_percentage;
    }

    /**
     * @param mixed $global_discount_percentage
     */
    public function setGlobalDiscountPercentage($global_discount_percentage): void
    {
        $this->global_discount_percentage = $global_discount_percentage;
    }
}
