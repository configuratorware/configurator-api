<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationattributeRepository")
 * @ORM\Table(
 *     name="itemclassification_attribute",
 *     uniqueConstraints={@ORM\UniqueConstraint(
 *             name="uq_itemarticle_id_itemarticleattribute_id_etc",
 *             columns={"attribute_id","attributevalue_id","date_deleted","itemclassification_id"}
 *         )}
 * )
 */
class ItemclassificationAttribute
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Attribute")
     * @ORM\JoinColumn(name="attribute_id", referencedColumnName="id")
     */
    private $attribute;

    /**
     * @ORM\ManyToOne(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue")
     * @ORM\JoinColumn(name="attributevalue_id", referencedColumnName="id")
     */
    private $attributevalue;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification",
     *     inversedBy="itemclassificationAttribute"
     * )
     * @ORM\JoinColumn(name="itemclassification_id", referencedColumnName="id")
     */
    private $itemclassification;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return ItemclassificationAttribute
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return ItemclassificationAttribute
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return ItemclassificationAttribute
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return ItemclassificationAttribute
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return ItemclassificationAttribute
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return ItemclassificationAttribute
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Set itemclassification.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification $itemclassification
     *
     * @return ItemclassificationAttribute
     */
    public function setItemclassification(
        Itemclassification $itemclassification = null
    ) {
        $this->itemclassification = $itemclassification;

        return $this;
    }

    /**
     * Get itemclassification.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification
     */
    public function getItemclassification()
    {
        return $this->itemclassification;
    }

    /**
     * Set attribute.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute $attribute
     *
     * @return ItemclassificationAttribute
     */
    public function setAttribute(Attribute $attribute = null)
    {
        $this->attribute = $attribute;

        return $this;
    }

    /**
     * Get attribute.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set attributevalue.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue $attributevalue
     *
     * @return ItemclassificationAttribute
     */
    public function setAttributevalue(Attributevalue $attributevalue = null)
    {
        $this->attributevalue = $attributevalue;

        return $this;
    }

    /**
     * Get attributevalue.
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue
     */
    public function getAttributevalue()
    {
        return $this->attributevalue;
    }
}
