<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidEntity;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Table(
 *     name="user",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="uq_default", columns={"email","date_deleted"}),
 *         @ORM\UniqueConstraint(name="uq_username", columns={"username","date_deleted"}),
 *         @ORM\UniqueConstraint(name="uq_apikey", columns={"date_deleted","api_key"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository")
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @deprecated use Role::ADMIN_ROLE instead
     */
    public const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $externalid;

    /**
     * @var string|null
     *
     * @Serializer\Expose()
     * @ORM\Column(type="string", nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $lastname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_active;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $api_key;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=true, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\UserRole",
     *     mappedBy="user",
     *     cascade={"persist"}
     * )
     *
     * @var Collection<int, UserRole>
     */
    private $userRole;

    /**
     * @ORM\OneToMany(targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser", mappedBy="user")
     *
     * @var Collection<int, ClientUser>
     */
    private $clientUser;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->clientUser = new ArrayCollection();
        $this->userRole = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUserIdentifier(): string
    {
        if (null === $this->username) {
            throw InvalidEntity::withProperty(self::class, 'username');
        }

        return $this->username;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->getUserIdentifier();
    }

    /**
     * @param string|null $username
     */
    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword($password, UserPasswordHasherInterface $passwordHasher): self
    {
        $this->password = $passwordHasher->hashPassword($this, $password);

        return $this;
    }

    public function getRoles(): array
    {
        $roles = ['ROLE_USER'];
        foreach ($this->userRole as $userRole) {
            if (null !== $userRole->getRole()) {
                /** @psalm-suppress PossiblyNullReference */
                $roles[] = $userRole->getRole()->getRole();
            }
        }

        return array_unique($roles);
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials(): void
    {
    }

    /**
     * Set externalid.
     *
     * @param string $externalid
     *
     * @return User
     */
    public function setExternalid($externalid)
    {
        $this->externalid = $externalid;

        return $this;
    }

    /**
     * Get externalid.
     *
     * @return string
     */
    public function getExternalid()
    {
        return $this->externalid;
    }

    /**
     * Set firstname.
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname.
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname.
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname.
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->is_active = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->is_active;
    }

    /**
     * @return string|null
     */
    public function getApiKey(): ?string
    {
        return $this->api_key;
    }

    /**
     * @param string|null $api_key
     *
     * @return User
     */
    public function setApiKey(?string $api_key): self
    {
        $this->api_key = $api_key;

        return $this;
    }

    /**
     * Set dateCreated.
     *
     * @param \DateTime $dateCreated
     *
     * @return User
     */
    public function setDateCreated($dateCreated)
    {
        $this->date_created = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated.
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * Set dateUpdated.
     *
     * @param \DateTime $dateUpdated
     *
     * @return User
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->date_updated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated.
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * Set dateDeleted.
     *
     * @param \DateTime $dateDeleted
     *
     * @return User
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->date_deleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted.
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * Set userCreatedId.
     *
     * @param int $userCreatedId
     *
     * @return User
     */
    public function setUserCreatedId($userCreatedId)
    {
        $this->user_created_id = $userCreatedId;

        return $this;
    }

    /**
     * Get userCreatedId.
     *
     * @return int
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * Set userUpdatedId.
     *
     * @param int $userUpdatedId
     *
     * @return User
     */
    public function setUserUpdatedId($userUpdatedId)
    {
        $this->user_updated_id = $userUpdatedId;

        return $this;
    }

    /**
     * Get userUpdatedId.
     *
     * @return int
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * Set userDeletedId.
     *
     * @param int $userDeletedId
     *
     * @return User
     */
    public function setUserDeletedId($userDeletedId)
    {
        $this->user_deleted_id = $userDeletedId;

        return $this;
    }

    /**
     * Get userDeletedId.
     *
     * @return int
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * Add userRole.
     *
     * @param UserRole $userRole
     *
     * @return User
     */
    public function addUserRole(UserRole $userRole): self
    {
        $userRole->setUser($this);

        $this->userRole->add($userRole);

        return $this;
    }

    /**
     * Remove userRole.
     *
     * @param UserRole $userRole
     */
    public function removeUserRole(UserRole $userRole): void
    {
        $key = $this->userRole->indexOf($userRole);

        if (false === $key) {
            return;
        }

        $element = $this->userRole->get($key);

        if (null === $element) {
            return;
        }

        $element->setDateDeleted(new \DateTime());
    }

    /**
     * Get userRole.
     *
     * @return Collection|UserRole[]
     */
    public function getUserRole()
    {
        return $this->userRole;
    }

    public function setUserRole($userRoles)
    {
        $this->userRole = $userRoles;
    }

    /**
     * Add clientUser.
     *
     * @param ClientUser $clientUser
     *
     * @return Client
     */
    public function addClientUser(ClientUser $clientUser)
    {
        $this->clientUser[] = $clientUser;

        return $this;
    }

    /**
     * @param ClientUser $clientUser
     */
    public function removeClientUser(ClientUser $clientUser): void
    {
        $key = $this->clientUser->indexOf($clientUser);

        if (false === $key) {
            return;
        }

        $element = $this->clientUser->get($key);

        if (null === $element) {
            return;
        }

        $element->setDateDeleted(new \DateTime());
    }

    /**
     * Get clientUser.
     *
     * @return Collection
     */
    public function getClientUser()
    {
        return $this->clientUser;
    }

    /**
     * Get the main (first) client if available.
     *
     * @return Client|null
     */
    public function getMainClient(): ?Client
    {
        $clientUser = $this->getClientUser()->first();

        return $clientUser ? $clientUser->getClient() : null;
    }

    /**
     * @return bool
     *
     * @deprecated use hasRole($roleName) instead
     */
    public function hasAdminRole(): bool
    {
        return $this->hasRole(Role::ADMIN_ROLE);
    }

    /**
     * @return bool
     *
     * @deprecated use hasRole($roleName) instead
     */
    public function hasClientRole(): bool
    {
        return $this->hasRole(Role::CLIENT_ROLE);
    }

    /**
     * @param string $roleName
     *
     * @return bool
     */
    public function hasRole(string $roleName): bool
    {
        foreach ($this->userRole as $userRole) {
            $role = $userRole->getRole();
            if (null !== $role && $roleName === $role->getRole()) {
                return true;
            }
        }

        return false;
    }

    public function hasClient(Client $client): bool
    {
        foreach ($this->getClientUser() as $clientUser) {
            if ($clientUser->getClient()->getId() === $client->getId()) {
                return true;
            }
        }

        return false;
    }
}
