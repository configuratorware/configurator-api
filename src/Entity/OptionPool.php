<?php

namespace Redhotmagma\ConfiguratorApiBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Redhotmagma\ConfiguratorApiBundle\Repository\OptionPoolRepository")
 * @ORM\Table(
 *     name="option_pool",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_default", columns={"date_deleted","identifier"})}
 * )
 */
class OptionPool
{
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $identifier;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $date_created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_updated;

    /**
     * @ORM\Column(type="datetime", nullable=false, options={"default":"0001-01-01 00:00:00"})
     */
    private $date_deleted;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_created_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_updated_id;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $user_deleted_id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionOptionPool",
     *     mappedBy="optionPool",
     *     cascade={"persist"}
     * )
     */
    private $optionOptionPool;

    /**
     * @ORM\OneToMany(
     *     targetEntity="Redhotmagma\ConfiguratorApiBundle\Entity\OptionPoolText",
     *     mappedBy="optionPool",
     *     cascade={"persist"}
     * )
     */
    private $optionPoolText;

    public function __construct()
    {
        $this->optionOptionPool = new ArrayCollection();
        $this->optionPoolText = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->date_updated;
    }

    /**
     * @param mixed $date_updated
     */
    public function setDateUpdated($date_updated)
    {
        $this->date_updated = $date_updated;
    }

    /**
     * @return mixed
     */
    public function getDateDeleted()
    {
        return $this->date_deleted;
    }

    /**
     * @param mixed $date_deleted
     */
    public function setDateDeleted($date_deleted)
    {
        $this->date_deleted = $date_deleted;
    }

    /**
     * @return mixed
     */
    public function getUserCreatedId()
    {
        return $this->user_created_id;
    }

    /**
     * @param mixed $user_created_id
     */
    public function setUserCreatedId($user_created_id)
    {
        $this->user_created_id = $user_created_id;
    }

    /**
     * @return mixed
     */
    public function getUserUpdatedId()
    {
        return $this->user_updated_id;
    }

    /**
     * @param mixed $user_updated_id
     */
    public function setUserUpdatedId($user_updated_id)
    {
        $this->user_updated_id = $user_updated_id;
    }

    /**
     * @return mixed
     */
    public function getUserDeletedId()
    {
        return $this->user_deleted_id;
    }

    /**
     * @param mixed $user_deleted_id
     */
    public function setUserDeletedId($user_deleted_id)
    {
        $this->user_deleted_id = $user_deleted_id;
    }

    /**
     * Add option option pool.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionOptionPool $optionOptionPool
     *
     * @return OptionPool
     */
    public function addOptionOptionPool(OptionOptionPool $optionOptionPool)
    {
        $this->optionOptionPool[] = $optionOptionPool;

        return $this;
    }

    /**
     * Remove optionOptionPool.
     *
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionOptionPool $optionOptionPool
     */
    public function removeOptionOptionPool(OptionOptionPool $optionOptionPool)
    {
        $this->optionOptionPool->removeElement($optionOptionPool);
    }

    /**
     * Get optionOptionPool.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOptionOptionPool()
    {
        return $this->optionOptionPool;
    }

    public function setOptionOptionPool($optionOptionPools)
    {
        $this->optionOptionPool = $optionOptionPools;
    }

    /**
     * @return OptionPoolText[]&Collection
     */
    public function getOptionPoolText(): Collection
    {
        return $this->optionPoolText;
    }

    /**
     * @param OptionPoolText $optionPoolText
     *
     * @return self
     */
    public function addOptionPoolText(OptionPoolText $optionPoolText): self
    {
        $this->optionPoolText->add($optionPoolText);

        return $this;
    }

    /**
     * @param OptionPoolText $optionPoolText
     *
     * @return self
     */
    public function removeOptionPoolText(OptionPoolText $optionPoolText): self
    {
        $this->optionPoolText->removeElement($optionPoolText);

        return $this;
    }
}
