<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Cache;

interface HasIdInterface
{
    /**
     * @return int
     */
    public function getId();
}
