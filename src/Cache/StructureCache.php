<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Cache;

use Doctrine\Common\Cache\ArrayCache;

/**
 * @internal
 */
final class StructureCache
{
    /**
     * @var ArrayCache
     */
    private $cache;

    /**
     * @var bool
     */
    private $isEnabled;

    /**
     * @param bool $isEnabled
     */
    public function __construct(bool $isEnabled = true)
    {
        $this->cache = new ArrayCache();
        $this->isEnabled = $isEnabled;
    }

    /**
     * @param HasIdInterface $entity
     *
     * @return mixed
     */
    public function findStructureForEntity(HasIdInterface $entity)
    {
        if (!$this->isEnabled) {
            return null;
        }

        $identifier = $this->generateIdentifierFor($entity);
        $structure = $this->cache->fetch($identifier);

        return $structure ?: null;
    }

    /**
     * @param HasIdInterface                $entity
     * @param                               $toStore
     */
    public function store(HasIdInterface $entity, $toStore): void
    {
        if (!$this->isEnabled) {
            return;
        }

        $identifier = $this->generateIdentifierFor($entity);
        $this->cache->save($identifier, $toStore);
    }

    /**
     * @param HasIdInterface $entity
     *
     * @return string
     */
    private function generateIdentifierFor(HasIdInterface $entity): string
    {
        $classIdentifier = get_class($entity);
        $elementIdentifier = $entity->getId();

        return $classIdentifier . $elementIdentifier;
    }
}
