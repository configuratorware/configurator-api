<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface FontPathsInterface
{
    public function getFontPath(): string;

    public function getFontPathRelative(): string;
}
