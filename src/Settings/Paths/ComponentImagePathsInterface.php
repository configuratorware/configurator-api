<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface ComponentImagePathsInterface
{
    public function getComponentThumbnailPath(): string;

    public function getComponentThumbnailPathRelative(): string;
}
