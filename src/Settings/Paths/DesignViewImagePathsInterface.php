<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface DesignViewImagePathsInterface
{
    public function getDesignViewImagePath(): string;

    public function getDesignViewImagePathRelative(): string;

    public function getDesignViewThumbnailPath(): string;

    public function getDesignViewThumbnailPathRelative(): string;
}
