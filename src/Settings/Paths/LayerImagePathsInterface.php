<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface LayerImagePathsInterface
{
    public function getLayerImagePath(): string;

    public function getLayerImagePathRelative(): string;
}
