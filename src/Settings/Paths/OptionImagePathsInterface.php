<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface OptionImagePathsInterface
{
    public function getOptionThumbnailPath(): string;

    public function getOptionThumbnailPathRelative(): string;
}
