<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
final class Paths implements ClientResourcePathsInterface, ComponentImagePathsInterface, DesignAreaPrintfilePathInterface, FontPathsInterface, DesignViewImagePathsInterface, ItemImagePathsInterface, LayerImagePathsInterface, LicensePathInterface, OptionImagePathsInterface, ConfigurationImagePathsInterface
{
    private string $fontPath;

    private string $componentThumbnailPath;

    private string $designAreaPrintfilePath;

    private string $designViewImagePath;

    private string $designViewThumbnailPath;

    private string $itemDetailImagePath;

    private string $itemThumbnailPath;

    private string $licenseFilePath;

    private string $layerImagePath;

    private string $mediaBasePath;

    private string $optionImagePath;

    private string $configurationImagePath;

    public function __construct(
        string $componentThumbnailPath,
        string $designAreaPrintfilePath,
        string $designViewImagePath,
        string $designViewThumbnailPath,
        string $fontPath,
        string $itemDetailImagePath,
        string $itemThumbnailPath,
        string $layerImagePath,
        string $licenseFilePath,
        string $mediaBasePath,
        string $optionImagePath,
        string $configurationImagePath
    ) {
        $this->componentThumbnailPath = trim($componentThumbnailPath, '/');
        $this->designAreaPrintfilePath = $designAreaPrintfilePath;
        $this->designViewImagePath = trim($designViewImagePath, '/');
        $this->designViewThumbnailPath = trim($designViewThumbnailPath, '/');
        $this->fontPath = trim($fontPath, '/');
        $this->itemDetailImagePath = trim($itemDetailImagePath, '/');
        $this->itemThumbnailPath = trim($itemThumbnailPath, '/');
        $this->licenseFilePath = $licenseFilePath;
        $this->layerImagePath = trim($layerImagePath, '/');
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
        $this->optionImagePath = trim($optionImagePath, '/');
        $this->configurationImagePath = trim($configurationImagePath, '/');
    }

    public function getClientFontPath(string $clientIdentifier): string
    {
        return $this->addBasePath($this->getClientFontPathRelative($clientIdentifier));
    }

    public function getClientFontPathRelative(string $clientIdentifier): string
    {
        return 'client/' . $clientIdentifier . '/font';
    }

    public function getClientLogoPath(string $clientIdentifier): string
    {
        return $this->addBasePath($this->getClientLogoPathRelative($clientIdentifier));
    }

    public function getClientLogoPathRelative(string $clientIdentifier): string
    {
        return 'client/' . $clientIdentifier . '/logo';
    }

    public function getComponentThumbnailPath(): string
    {
        return $this->addBasePath($this->componentThumbnailPath);
    }

    public function getComponentThumbnailPathRelative(): string
    {
        return $this->componentThumbnailPath;
    }

    public function getDesignAreaPrintfilePath(): string
    {
        return $this->addBasePath($this->designAreaPrintfilePath);
    }

    public function getDesignViewImagePath(): string
    {
        return $this->addBasePath($this->designViewImagePath);
    }

    public function getDesignViewImagePathRelative(): string
    {
        return $this->designViewImagePath;
    }

    public function getDesignViewThumbnailPath(): string
    {
        return $this->addBasePath($this->designViewThumbnailPath);
    }

    public function getDesignViewThumbnailPathRelative(): string
    {
        return $this->designViewThumbnailPath;
    }

    public function getFontPath(): string
    {
        return $this->addBasePath($this->fontPath);
    }

    public function getFontPathRelative(): string
    {
        return $this->fontPath;
    }

    public function getItemThumbnailPath(): string
    {
        return $this->addBasePath($this->itemThumbnailPath);
    }

    public function getItemDetailImagePathRelative(): string
    {
        return $this->itemDetailImagePath;
    }

    public function getItemDetailImagePath(): string
    {
        return $this->addBasePath($this->itemDetailImagePath);
    }

    public function getItemThumbnailPathRelative(): string
    {
        return $this->itemThumbnailPath;
    }

    public function getLayerImagePath(): string
    {
        return $this->addBasePath($this->layerImagePath);
    }

    public function getLayerImagePathRelative(): string
    {
        return $this->layerImagePath;
    }

    public function getLicenseFilePath(): string
    {
        return $this->licenseFilePath;
    }

    public function getOptionThumbnailPath(): string
    {
        return $this->addBasePath($this->optionImagePath);
    }

    public function getOptionThumbnailPathRelative(): string
    {
        return $this->optionImagePath;
    }

    public function getConfigurationImagePath(): string
    {
        return $this->addBasePath($this->configurationImagePath);
    }

    public function getConfigurationImagePathRelative(): string
    {
        return $this->configurationImagePath;
    }

    private function addBasePath(string $relativePath): string
    {
        return $this->mediaBasePath . DIRECTORY_SEPARATOR . $relativePath;
    }
}
