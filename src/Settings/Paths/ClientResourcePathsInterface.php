<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface ClientResourcePathsInterface
{
    public function getClientFontPath(string $clientIdentifier): string;

    public function getClientFontPathRelative(string $clientIdentifier): string;

    public function getClientLogoPath(string $clientIdentifier): string;

    public function getClientLogoPathRelative(string $clientIdentifier): string;
}
