<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface ConfigurationImagePathsInterface
{
    public function getConfigurationImagePath(): string;

    public function getConfigurationImagePathRelative(): string;
}
