<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
final class TranslationsPaths
{
    public const TRANSLATIONS_DEFAULT_DOMAIN = 'messages';

    private string $basePath;

    private string $publicPath;

    private string $cachedPath;

    private string $customPath;

    private string $userPath;

    public function __construct(string $basePath, string $publicPath, string $cachedPath, string $customPath, string $userPath)
    {
        $this->basePath = $basePath;
        $this->publicPath = $publicPath;
        $this->cachedPath = $cachedPath;
        $this->customPath = $customPath;
        $this->userPath = $userPath;
    }

    public function getBasePath(): string
    {
        return $this->basePath;
    }

    public function getPublicPath(): string
    {
        return $this->publicPath;
    }

    public function getCachedPath(): string
    {
        return $this->cachedPath;
    }

    public function getCustomPath(): string
    {
        return $this->customPath;
    }

    public function getUserPath(): string
    {
        return $this->userPath;
    }

    public function getBaseIsoFilePath(string $iso): string
    {
        $filename = sprintf('%s.%s.json', self::TRANSLATIONS_DEFAULT_DOMAIN, $iso);

        return $this->getBasePath() . DIRECTORY_SEPARATOR . $filename;
    }

    public function getPublicIsoFilePath(string $iso): string
    {
        return $this->getPublicPath() . DIRECTORY_SEPARATOR . $iso . '.json';
    }

    public function getCustomIsoFilePath(string $iso): string
    {
        return $this->getCustomPath() . DIRECTORY_SEPARATOR . $iso . '.json';
    }

    public function getUserIsoFilePath(string $iso): string
    {
        return $this->getUserPath() . DIRECTORY_SEPARATOR . $iso . '.json';
    }

    public function getCachedIsoFilePath(string $iso): string
    {
        $filename = sprintf('%s.%s.json', self::TRANSLATIONS_DEFAULT_DOMAIN, $iso);

        return $this->getCachedPath() . DIRECTORY_SEPARATOR . $filename;
    }
}
