<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface LicensePathInterface
{
    public function getLicenseFilePath(): string;
}
