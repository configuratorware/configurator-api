<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface DesignAreaPrintfilePathInterface
{
    public function getDesignAreaPrintfilePath(): string;
}
