<?php

namespace Redhotmagma\ConfiguratorApiBundle\Settings\Paths;

/**
 * @internal
 */
interface ItemImagePathsInterface
{
    public function getItemDetailImagePath(): string;

    public function getItemDetailImagePathRelative(): string;

    public function getItemThumbnailPath(): string;

    public function getItemThumbnailPathRelative(): string;
}
