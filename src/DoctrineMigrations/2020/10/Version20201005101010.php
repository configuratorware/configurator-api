<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Exception as DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201005101010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO `visualization_mode` (`id`, `identifier`, `valid_for_configuration_modes`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`) VALUES (1, '2dLayer', '[\"creator\",\"creator+designer\"]', '2020-10-05 13:08:03', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL), (2, '2dVariant', '[\"designer\"]', '2020-10-05 13:08:03', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),  (3, '3dScene', '[\"creator\",\"creator+designer\"]', '2020-10-05 13:08:03', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL), (4, '3dVariant', '[\"designer\"]', '2020-10-05 13:08:03', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);");
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM `visualization_mode`;');
    }
}
