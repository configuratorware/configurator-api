<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add "configurations" credential to "client" role.
 */
final class Version20200401175258 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // add "configurations" credential to "client" role
        $this->addSql('
            INSERT IGNORE INTO role_credential (date_created, date_deleted, role_id, credential_id, user_created_id) 
            VALUES (NOW(), \'0001-01-01 00:00:00\', (SELECT id FROM role WHERE name="client"), (SELECT id FROM credential WHERE area="configurations"), 0);');
    }

    public function down(Schema $schema): void
    {
    }
}
