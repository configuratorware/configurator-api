<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Assign all current configurations to "_default" client.
 */
final class Version20200331173938 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('
            UPDATE configuration 
                SET client_id = (SELECT id FROM client where identifier = "_default") 
                WHERE client_id IS NULL
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
