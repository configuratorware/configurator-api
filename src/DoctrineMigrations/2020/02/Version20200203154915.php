<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200203154915 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // create connector role
        $this->addSql('
            INSERT INTO role(name, role, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) 
                VALUES(\'connector\', \'ROLE_CONNECTOR\', NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
        ');

        // connect role to credential
        $this->addSql('
            INSERT INTO role_credential (date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id)
                VALUES (NOW(), null, \'0001-01-01 00:00:00\', 0, 0, 0, (SELECT r.id FROM role r WHERE r.name = \'connector\'), (SELECT c.id FROM credential c WHERE c.area = \'connector\'));
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
