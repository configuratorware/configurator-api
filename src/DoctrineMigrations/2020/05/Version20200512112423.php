<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200512112423 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('
            UPDATE role_credential SET `date_deleted` = NOW()
            WHERE role_credential.role_id IN (SELECT id FROM role WHERE role.name = \'client\')
            AND role_credential.credential_id IN (SELECT id FROM credential WHERE area = \'clients\');
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
