<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200828061315 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $default = $this->connection->query('SELECT * FROM `language` WHERE is_default = true')->fetchAll();

        if ([] === $default) {
            $this->addSql('UPDATE language SET is_default = true WHERE iso = "en_GB"');
        }
    }

    public function down(Schema $schema): void
    {
    }
}
