<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * migrate configuration variants from item groups to item pools.
 */
final class Version20190124084826 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // move item groups to item pools
        $this->addSql('
            INSERT INTO item_pool (id, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
                SELECT i.id, i.identifier, i.date_created, i.date_updated, i.date_deleted, i.user_created_id, i.user_updated_id, i.user_deleted_id FROM itemgroup i WHERE i.configurationvariant = 1 AND i.id NOT IN (SELECT id FROM item_pool);
            INSERT INTO item_item_pool (id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, item_id, item_pool_id)
                SELECT ii.id, ii.date_created, ii.date_updated, ii.date_deleted, ii.user_created_id, ii.user_updated_id, ii.user_deleted_id, ii.item_id, ii.itemgroup_id FROM item_itemgroup ii LEFT JOIN itemgroup i ON i.id = ii.itemgroup_id WHERE i.configurationvariant = 1 AND ii.id NOT IN (SELECT id FROM item_item_pool);
            
            UPDATE item i LEFT JOIN item i1 ON i1.parent_id = i.id LEFT JOIN item_itemgroup ii ON i1.id = ii.item_id LEFT JOIN itemgroup ig ON ig.id = itemgroup_id set i.date_deleted = NOW() WHERE ig.configurationvariant = 1;
            DELETE item_itemgroup FROM item_itemgroup LEFT JOIN itemgroup i ON i.id = itemgroup_id WHERE i.configurationvariant = 1;
            DELETE FROM itemgroup WHERE configurationvariant = 1;
            
            UPDATE item SET parent_id = null WHERE id IN (SELECT ii.item_id FROM item_item_pool ii);
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
