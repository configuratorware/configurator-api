<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190102155006 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // Add item_status
        $this->addSql('
            INSERT INTO `item_status` (`id`, `identifier`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`) VALUES (NULL, \'not_available\', NOW(), NULL, \'0001-01-01 00:00:00\', 1, NULL, NULL), (NULL, \'available\', NOW(), NULL, \'0001-01-01 00:00:00\', 1, NULL, NULL) ON DUPLICATE KEY UPDATE identifier = VALUES(identifier);
        ');

        // Add configuration_mode
        $this->addSql('
            INSERT INTO `configuration_mode` (`id`, `identifier`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`) VALUES (NULL, \'creator\', NOW(), NULL, \'0001-01-01 00:00:00\', 1, NULL, NULL), (NULL, \'designer\', NOW(), NULL, \'0001-01-01 00:00:00\', 1, NULL, NULL) ON DUPLICATE KEY UPDATE identifier = VALUES(identifier);
        ');

        // Add item_configuration_mode_item_status
        $this->addSql('
            INSERT INTO `item_configuration_mode_item_status`(`id`, `date_created`, `date_updated`, `date_deleted`, `user_created_id`, `user_updated_id`, `user_deleted_id`, `item_id`, `item_status_id`, `configuration_mode_id`) SELECT NULL AS `id`, NOW() AS `date_created`, NULL as `date_updated`, \'0001-01-01 00:00:00\' AS `date_deleted`, 1 as `user_created_id`, NULL AS `user_updated_id`, NULL AS `user_deleted_id`, `id` AS `item_id`, IF(`configurable` = true, (SELECT `id` FROM `item_status` WHERE `identifier` = \'available\'), (SELECT `id` FROM `item_status` WHERE `identifier` = \'not_available\')) AS `item_status_id`, (SELECT `id` FROM `configuration_mode` WHERE `identifier` = \'creator\') AS `configuration_mode_id` FROM `item` WHERE `item`.`id` NOT IN (SELECT `item_id` FROM `item_configuration_mode_item_status`)
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
