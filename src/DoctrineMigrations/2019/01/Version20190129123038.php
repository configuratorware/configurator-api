<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190129123038 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // Add credential: designer_global_item_prices
        $this->addSql('
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(NULL, \'designer_global_item_prices\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL)       
        ');

        // Add credential to admin role
        $this->addSql('
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(NULL, NOW(), null, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, (SELECT id FROM credential WHERE area = \'designer_global_item_prices\'))
        ');

        // Reset base ruleset of credentials in licence
        $this->addSql('
            UPDATE license SET license = \'["users","credentials","configurations","production_view","tags","image_gallery","items","options","option_classifications","attributes","attribute_values","inspirations","design_templates","option_pools","item_classifications","configuration_variants","code_snippets","assembly_points","channels","roles","currencies","color_palettes","design_production_methods","design_areas","design_views","designer_global_item_prices"]\' WHERE identifier = \'base\''
        );

        // Add designer_global_item_prices Licence to current application
        $license = $this->connection->query('SELECT license FROM license WHERE identifier = \'license_file\'')->fetchAll();
        $license = json_decode($license[0]['license']);
        $license[] = 'designer_global_item_prices';
        $license = json_encode($license);
        $this->addSql('UPDATE license SET license = \'' . $license . '\' WHERE identifier = \'license_file\'');
    }

    public function down(Schema $schema): void
    {
    }
}
