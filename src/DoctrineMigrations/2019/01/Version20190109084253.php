<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190109084253 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // Update item_status if identifier is available
        $this->addSql('
            UPDATE `item_status` SET `item_available` = true WHERE `identifier` = \'available\'       
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
