<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190813142724 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // remove migration for default color palette creation
        $this->addSql('DELETE FROM migration_versions WHERE version = \'20181217124054\'');
    }

    public function down(Schema $schema): void
    {
    }
}
