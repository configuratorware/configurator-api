<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221103038 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // Add new configuration type "offer_request"
        $this->addSql('
            INSERT INTO configurationtype (identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
            VALUES (\'offer_request\', NOW(), null, "0001-01-01 00:00:00", 0, 0, 0);       
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
