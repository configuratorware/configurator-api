<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190208092434 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // Add credential: clients
        $this->addSql('
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(NULL, \'clients\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL)       
        ');

        // Add credential to admin role
        $this->addSql('
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(NULL, NOW(), null, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, (SELECT id FROM credential WHERE area = \'clients\'))
        ');

        // Reset base ruleset of credentials in licence
        $this->addSql('
            UPDATE license SET license = \'["users","credentials","configurations","production_view","tags","image_gallery","items","options","option_classifications","attributes","attribute_values","inspirations","design_templates","option_pools","item_classifications","configuration_variants","code_snippets","assembly_points","channels","roles","currencies","color_palettes","design_production_methods","design_areas","design_views","clients"]\' WHERE identifier = \'base\''
        );

        // Add clients Licence to current application
        $license = $this->connection->query('SELECT license FROM license WHERE identifier = \'license_file\'')->fetchAll();
        $license = json_decode($license[0]['license']);
        $license[] = 'clients';
        $license = json_encode($license);
        $this->addSql('UPDATE license SET license = \'' . $license . '\' WHERE identifier = \'license_file\'');

        // create default client
        $this->addSql('
            INSERT INTO client(identifier, email, theme, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) 
                VALUES(\'_default\', \'\', \'{"highlightColor":"#ed1c29"}\', NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
        ');

        // create client role
        $this->addSql('
            INSERT INTO role(name, role, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) 
                VALUES(\'client\', \'ROLE_CLIENT\', NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
        ');

        $this->addSql('
            INSERT INTO role_credential (date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id)
                VALUES (NOW(), null, \'0001-01-01 00:00:00\', 0, 0, 0, (SELECT r.id FROM role r WHERE r.name = \'client\'), (SELECT c.id FROM credential c WHERE c.area = \'clients\'));
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
