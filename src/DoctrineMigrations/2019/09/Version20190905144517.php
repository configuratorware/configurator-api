<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190905144517 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE designer_production_calculation_type SET declare_separately = 0 WHERE declare_separately IS NULL');
    }

    public function down(Schema $schema): void
    {
    }
}
