<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * CI-378.
 */
final class Version20190312124440 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // delete no longer existing migration versions from the database
        $this->addSql('
            DELETE FROM 
                migration_versions
            WHERE
                version <= \'20180820110318\';
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
