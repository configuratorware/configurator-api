<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190529121915 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $sql = 'UPDATE setting SET visualizationcomponent = \'3d\' WHERE id = 1';

        $isAlreadySet = $this->connection->query('SELECT * FROM setting')->fetchAll();

        if (!$isAlreadySet) {
            $sql = 'INSERT INTO setting(id, visualizationcomponent, calculationmethod, shareurl, date_created, date_deleted, user_created_id) VALUES(1, \'3d\', \'sumofall\', \'http://localhost:10030/code:{code}\',NOW(), \'0001-01-01 00:00:00\', 0)';
        }

        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
    }
}
