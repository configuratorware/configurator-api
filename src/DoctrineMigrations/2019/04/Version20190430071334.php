<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190430071334 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // Add credential: designer_global_item_prices
        $this->addSql('
            INSERT INTO credential(area, date_created, date_deleted, user_created_id) VALUES(\'connector\', NOW(), \'0001-01-01 00:00:00\', 0)       
        ');

        // Add credential to admin role
        $this->addSql('
            INSERT INTO role_credential(date_created, date_deleted, user_created_id, role_id, credential_id) VALUES(NOW(), \'0001-01-01 00:00:00\', 0, 1, (SELECT id FROM credential WHERE area = \'connector\'))
        ');

        // Reset base ruleset of credentials in licence
        $this->addSql('
            UPDATE license SET license = \'["users","credentials","configurations","production_view","tags","image_gallery","items","options","option_classifications","attributes","attribute_values","inspirations","design_templates","option_pools","item_classifications","configuration_variants","code_snippets","assembly_points","channels","roles","currencies","color_palettes","design_production_methods","design_areas","design_views","item_statuses","designer_global_calculation_types","designer_global_item_prices","clients","fonts","connector"]\' WHERE identifier = \'base\''
        );

        // Add designer_global_item_prices Licence to current application
        $license = $this->connection->query('SELECT license FROM license WHERE identifier = \'license_file\'')->fetchAll();
        $license = json_decode($license[0]['license']);
        $license[] = 'connector';
        $license = json_encode($license);
        $this->addSql('UPDATE license SET license = \'' . $license . '\' WHERE identifier = \'license_file\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
