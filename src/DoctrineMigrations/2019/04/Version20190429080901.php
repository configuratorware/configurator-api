<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190429080901 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $isExistent = $this->connection->query('SELECT identifier FROM configurationtype WHERE identifier = \'widget\'')->fetchAll();

        if (!$isExistent) {
            $this->addSql('INSERT INTO `configurationtype` (`identifier`, `date_created`, `date_deleted`, `user_created_id`) VALUES (\'widget\',NOW(),\'0001-01-01 00:00:00\',0);');
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
