<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180827132240 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->write('creating rule ty texts');
        $this->addSql('
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(1, \'One option depends on another\', \'The option "{rule_option}" expects option "{rule_data_option}" to be selected.\', \'2018-08-27 11:47:10\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 1);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(2, \'Eine Option setzt eine andere voraus\', \'Um die Option "{rule_option}" zu wählen muss die Option "{rule_data_option}" gewählt sein.\', \'2018-08-27 11:47:32\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 1);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(3, \'One option excludes another\', \'The options "{rule_option}" and "{rule_data_option}" are incompatible.\', \'2018-08-27 14:49:24\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 2);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(4, \'Expliziter Ausschluss zweier Optinen\', \'Die Optionen "{rule_option}" und "{rule_data_option}" sind inkompatibel.\', \'2018-08-27 14:49:38\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 2);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(5, \'Minimum amount of options in a category\', \'The minimum amount of options in the category "{rule_data_option_classification}" is "{rule_data_min_amount}".\', \'2018-08-27 14:49:49\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 3);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) #
                    VALUES(6, \'Minimalmenge wählbarer Optionen an einer Kategorie\', \'In der Kategorie "{rule_data_option_classification}" müssen mindestens "{rule_data_min_amount}" Optionen gewählt sein.\', \'2018-08-27 14:49:59\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 3);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(7, \'Maximum amount of options in a category\', \'The maximum amount of options in the category "{rule_data_option_classification}" is "{rule_data_max_amount}".\', \'2018-08-27 14:50:09\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 4);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(8, \'Maximalmenge wählbarer Optionen an einer Kategorie\', \'In der Kategorie "{rule_data_option_classification}" dürfen maximal "{rule_data_max_amount}" Optionen gewählt sein.\', \'2018-08-27 14:50:18\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 4);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(9, \'Non matching attributes lead to an exclusion\', \'There has to be a matching value für the attribute "{rule_data_attribute}".\', \'2018-08-27 14:50:30\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 5);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(10, \'Ungleiche Attributwerte führen zu Ausschluss\', \'Für das Attribut "{rule_data_attribute}" muss es übereinstimmende Werte geben.\', \'2018-08-27 14:50:39\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 5);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(11, \'Maximum sum of values for an attribute\', \'The maximum sum of all values for attribute "{rule_data_attribute}" is "{rule_data_max_amount}".\', \'2018-08-27 14:50:48\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 6);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(12, \'Maximale Summe für Werte eines Attributes\', \'Die Summe aller Werte für das Attribut "{rule_data_attribute}" darf "{rule_data_max_amount}" nicht überschreiten.\', \'2018-08-27 14:50:57\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 6);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(13, \'Maximum amount of one option\', \'The maximum amount for the option "{rule_option}" is "{rule_data_max_amount}".\', \'2018-08-27 14:52:19\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 7);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(14, \'Maximalmenge einer Option\', \'Die maximale Menge für die Option "{rule_option}" ist "{rule_data_max_amount}".\', \'2018-08-27 14:52:27\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 7);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(15, \'Default option if not incompatible\', \'The option "{rule_data_option}" is automatically selected if compatible.\', \'2018-08-27 14:52:36\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 8);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(16, \'Standardoption die gewählt wird wenn sie kompatibel ist\', \'Die Option "{rule_data_option}" wird automatisch ausgewählt falls sie kompatibel ist.\', \'2018-08-27 14:52:44\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 8);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(17, \'Automatic selection of an option\', \'When selecting the option "{rule_option}" the option "{rule_data_option}" is also selected.\', \'2018-08-27 14:52:53\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 9);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(18, \'Automatische Auswahl einer Option\', \'Wenn die Option "{rule_option}" gewählt wird, wird zusätzlich die Option "{rule_data_option}" gewählt.\', \'2018-08-27 14:53:01\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 9);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(19, \'Grouped option switch based on an attribute\', \'Options are automatically switched based on the attribute "{rule_data_attribute}". The rule is applied per category: {rule_data_apply_per_option_classification[Yes|No]}.\', \'2018-08-27 14:53:10\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 1, 10);
                INSERT IGNORE INTO rule_type_text(id, title, rule_title, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, language_id, ruletype_id) 
                    VALUES(20, \'Tauschen von Optionsgruppen basierend auf einem Attribut\', \'Optionen werden automatisch auf Basis des Attributs "{rule_data_attribute}" getauscht. Die Regel wird pro Kategorie angewandt: {rule_data_apply_per_option_classification[Ja|Nein]}\', \'2018-08-27 14:53:20\', NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL, 2, 10);
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
