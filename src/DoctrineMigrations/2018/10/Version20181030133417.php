<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181030133417 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->write('creating credentials and assign them to the admin user');
        $this->addSql('TRUNCATE credential;');
        $this->addSql('TRUNCATE role_credential;');

        $this->addSql('
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(1, \'users\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(2, \'credentials\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(3, \'configurations\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(4, \'production_view\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(5, \'tags\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(6, \'image_gallery\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(7, \'items\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(8, \'options\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(9, \'option_classifications\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(10, \'attributes\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(11, \'attribute_values\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(12, \'inspirations\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(13, \'design_templates\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(14, \'option_pools\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(15, \'item_classifications\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(16, \'configuration_variants\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(17, \'code_snippets\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(18, \'assembly_points\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(19, \'channels\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(20, \'roles\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT INTO credential(id, area, identifier, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(21, \'currencies\', NULL, NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
        ');

        $this->addSql('
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(1, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 1);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(2, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 2);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(3, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 3);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(4, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 4);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(5, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 5);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(6, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 6);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(7, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 7);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(8, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 8);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(9, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 9);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(10, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 10);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(11, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 11);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(12, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 12);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(13, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 13);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(14, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 14);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(15, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 15);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(16, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 16);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(17, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 17);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(18, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 18);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(19, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 19);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(20, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 20);
            INSERT INTO role_credential(id, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, role_id, credential_id) VALUES(21, NOW(), NULL, \'0001-01-01 00:00:00\', 0, NULL, NULL, 1, 21);
        ');

        $this->addSql('
            INSERT IGNORE INTO license(id, identifier, license, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(1, \'base\', \'["users","credentials","configurations","production_view","tags","image_gallery","items","options","option_classifications","attributes","attribute_values","inspirations","design_templates","option_pools","item_classifications","configuration_variants","code_snippets","assembly_points","channels","roles","currencies"]\', NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
            INSERT IGNORE INTO license(id, identifier, license, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id) VALUES(2, \'license_file\', \'["users","credentials","configurations","production_view","tags","image_gallery","items","options","option_classifications","attributes","attribute_values","inspirations","design_templates","option_pools","item_classifications","configuration_variants","code_snippets","assembly_points","channels","roles","currencies"]\', NOW(), NULL, \'0001-01-01 00:00:00\', NULL, NULL, NULL);
        ');
    }

    public function down(Schema $schema): void
    {
    }
}
