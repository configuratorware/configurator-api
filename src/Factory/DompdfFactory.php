<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Factory;

use Dompdf\Dompdf;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDirectory;

/**
 * @internal
 */
final class DompdfFactory
{
    /**
     * @var string
     */
    private $webDocumentRoot;

    /**
     * @param string $webDocumentRoot
     */
    public function __construct(string $webDocumentRoot)
    {
        $this->webDocumentRoot = $webDocumentRoot;
    }

    public function create(): Dompdf
    {
        $dir = $this->webDocumentRoot;

        if (!is_dir($dir)) {
            throw InvalidDirectory::notFound($dir);
        }

        if ($dataFolder = $this->findRealDataFolderPath()) {
            $dir = implode(',', [$dir, $dataFolder]);
        }

        $dompdf = new Dompdf();
        $dompdf->getOptions()->setChroot($dir);
        $dompdf->getOptions()->setIsRemoteEnabled(true);

        return $dompdf;
    }

    private function findRealDataFolderPath(): ?string
    {
        $clientFolder = $this->webDocumentRoot . DIRECTORY_SEPARATOR . 'client';
        $additionalChroot = null;
        if (is_link($clientFolder)) {
            $realFolder = readlink($clientFolder);
            $additionalChroot = str_replace(DIRECTORY_SEPARATOR . 'client', '', $realFolder);
        }

        return $additionalChroot;
    }
}
