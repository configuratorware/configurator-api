<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Factory;

use Symfony\Component\Process\Process;

/**
 * @internal
 */
class ProcessFactory
{
    /**
     * @param array $command
     *
     * @return Process
     *
     * @deprecated calling the factory static is deprecated, use `create` instead
     */
    public static function createWithDefaults(array $command): Process
    {
        return new Process($command);
    }

    /**
     * @param array $command
     *
     * @return Process
     */
    public function create(array $command): Process
    {
        return new Process($command);
    }
}
