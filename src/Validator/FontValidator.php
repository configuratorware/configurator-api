<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;

/**
 * @internal
 */
class FontValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var FontRepository
     */
    private $fontRepository;

    /**
     * ClientValidator constructor.
     *
     * @param FontRepository $fontRepository
     * @param StructureValidator $structureValidator
     */
    public function __construct(
        FontRepository $fontRepository,
        StructureValidator $structureValidator
    ) {
        $this->fontRepository = $fontRepository;
        $this->structureValidator = $structureValidator;
    }

    /**
     * @param FontStructure $structure
     *
     * @throws \Exception
     */
    public function validate(FontStructure $structure)
    {
        $violations = $this->structureValidator->validate($structure);

        if (!empty($structure->name)) {
            $font = $this->fontRepository->findOneBy(['name' => $structure->name]);

            // validate if identifier is unique
            if (!empty($font) && $structure->id != $font->getId()) {
                $violations[] = [
                    'property' => 'name',
                    'invalidvalue' => $structure->name,
                    'message' => 'duplicate_entry',
                ];
            }

            // validate if filename contains name to make sure, that filenames are unique
            $fileNameProperties = ['fileNameRegular', 'fileNameBold', 'fileNameItalic', 'fileNameBoldItalic'];
            foreach ($fileNameProperties as $property) {
                if (!empty($structure->$property)) {
                    $fontNameChunks = explode(' ', $structure->name);

                    foreach ($fontNameChunks as $chunk) {
                        $valid = strpos($structure->$property, $chunk);
                        if (false === $valid) {
                            $violations[] = [
                                'property' => $property,
                                'invalidvalue' => $structure->$property,
                                'message' => 'invalid_input',
                            ];

                            break;
                        }
                    }
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
