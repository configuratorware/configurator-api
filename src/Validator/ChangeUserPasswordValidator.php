<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Exception\UserNotProvided;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChangeUserPassword;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @internal
 */
class ChangeUserPasswordValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    /**
     * @var Security
     */
    private $security;

    /**
     * ChangeUserPasswordValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param UserPasswordHasherInterface $passwordHasher
     * @param Security $security
     */
    public function __construct(
        StructureValidator $structureValidator,
        UserPasswordHasherInterface $passwordHasher,
        Security $security
    ) {
        $this->structureValidator = $structureValidator;
        $this->passwordHasher = $passwordHasher;
        $this->security = $security;
    }

    /**
     * @param ChangeUserPassword $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        /** @var User|null $user */
        $user = $this->security->getUser();
        if (null === $user) {
            throw UserNotProvided::bySecurity();
        }

        if (!$this->passwordHasher->isPasswordValid($user, $structure->oldPassword)) {
            $violations[] = [
                'property' => 'oldPassword',
                'invalidvalue' => '',
                'message' => 'old_password_mismatch',
            ];
        }

        if ($structure->newPassword != $structure->newPasswordRepeat) {
            $violations[] = [
                'property' => 'newPasswordRepeat',
                'invalidvalue' => '',
                'message' => 'new_password_mismatch',
            ];
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
