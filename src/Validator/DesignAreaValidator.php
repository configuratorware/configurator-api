<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerProductionCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaDesignProductionMethodPrice;

/**
 * @internal
 */
class DesignAreaValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var DesignerProductionCalculationTypeRepository
     */
    private $designerProductionCalculationTypeRepository;

    /**
     * DesignAreaValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param DesignAreaRepository $designAreaRepository
     * @param DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        DesignAreaRepository $designAreaRepository,
        DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->designAreaRepository = $designAreaRepository;
        $this->designerProductionCalculationTypeRepository = $designerProductionCalculationTypeRepository;
    }

    /**
     * @param DesignAreaStructure $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique per item/option
        if (!empty($structure->identifier)) {
            if (!empty($structure->item)) {
                $designArea = $this->designAreaRepository->findOneBy(['identifier' => $structure->identifier, 'item' => $structure->item->id]);
            }

            if (!empty($structure->option)) {
                $designArea = $this->designAreaRepository->findOneBy(['identifier' => $structure->identifier, 'option' => $structure->option->id]);
            }

            if (!empty($designArea) && $designArea->getId() != $structure->id) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate if item or option is set
        if (empty($structure->item) && empty($structure->option)) {
            $violations[] = [
                'property' => 'item',
                'invalidvalue' => $structure->item,
                'message' => 'empty_relation',
            ];
            $violations[] = [
                'property' => 'option',
                'invalidvalue' => $structure->option,
                'message' => 'empty_relation',
            ];
        }

        // validate Item
        if (!empty($structure->item) && empty($structure->option)) {
            $violations = $this->structureValidator->validateSubStructureOne(
                $structure->item,
                $violations,
                'item'
            );
        }

        // validate Option
        if (empty($structure->item) && !empty($structure->option)) {
            $violations = $this->structureValidator->validateSubStructureOne(
                $structure->option,
                $violations,
                'option'
            );
        }

        // validate if only item OR option is set in structure
        if (!empty($structure->item) && !empty($structure->option)) {
            $violations[] = [
                'property' => 'item',
                'invalidvalue' => $structure->item,
                'message' => 'multiple_relations',
            ];
            $violations[] = [
                'property' => 'option',
                'invalidvalue' => $structure->option,
                'message' => 'multiple_relations',
            ];
        }

        // validate texts
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->texts,
            $violations,
            'texts'
        );

        // validate designProductionMethods
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->designProductionMethods,
            $violations,
            'designProductionMethods'
        );

        // validate Calculation Types inside DesignProductionMethods
        foreach ($structure->designProductionMethods as $i => $method) {
            // General Type Validation
            $methodMarker = 'designProductionMethod[' . $i . ']';

            // match between design Production Method and Calculation Type
            $validCalculationTypes = $this->designerProductionCalculationTypeRepository->getByDesignProductionMethod((int)$method->id);
            $validCalculationTypes = array_column($validCalculationTypes, 'id');
            $calculationTypes = $method->calculationTypes ?? [];
            foreach ($calculationTypes as $j => $type) {
                if (!in_array($type->id, $validCalculationTypes)) {
                    $violations[] = [
                        'property' => $methodMarker . '.calculationTypes[' . $j . ']',
                        'invalidvalue' => $type->id,
                        'message' => 'no_relation',
                    ];
                }

                $preViolations = $this->structureValidator->validate($type);
                foreach ($preViolations as $k => $violation) {
                    $preViolations[$k]['property'] = $methodMarker . '.' . $violation['property'];
                }

                $violations = array_merge($violations, $preViolations);

                // validate prices
                $violations = $this->structureValidator->validateSubStructureMany(
                    $type->prices,
                    $violations,
                    $methodMarker . '.prices'
                );
            }
        }

        // check and add bulk price violations
        $violations = array_merge($violations, $this->validateBulkPrices($structure->designProductionMethods));

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }

    /**
     * @param array $designProductionMethods
     *
     * @return array Returns violations
     */
    private function validateBulkPrices(array $designProductionMethods): array
    {
        /* @var $price DesignAreaDesignProductionMethodPrice */

        $violations = [];
        $productionCalculationBulkPriceHash = [];

        foreach ($designProductionMethods as $i => $method) {
            $calculationTypes = $method->calculationTypes ?? [];
            foreach ($calculationTypes as $j => $type) {
                foreach ($type->prices as $k => $price) {
                    $bulkPriceHash = $this->createHashFromPrice($method->identifier, $type->identifier, $price);

                    // this price combination is already present
                    if (in_array($bulkPriceHash, $productionCalculationBulkPriceHash, true)) {
                        $violations[] = $this->createDuplicateBulkPriceViolation(
                            'designProductionMethod[' . $i . '].calculationTypes[' . $j . '].prices[' . $k . ']',
                            $price
                        );

                        continue;
                    }
                    $productionCalculationBulkPriceHash[] = $bulkPriceHash;
                }
            }
        }

        return $violations;
    }

    /**
     * @param string $property
     * @param DesignAreaDesignProductionMethodPrice $price
     *
     * @return array
     */
    private function createDuplicateBulkPriceViolation(string $property, DesignAreaDesignProductionMethodPrice $price): array
    {
        return [
            'property' => $property,
            'invalidvalue' => json_encode([
                'channel' => $price->channel,
                'colorAmountFrom' => $price->colorAmountFrom,
                'amountFrom' => $price->amountFrom,
            ]),
            'message' => 'duplicate_bulk_price',
        ];
    }

    /**
     * @param string $methodIdentifier
     * @param string $typeIdentifier
     * @param DesignAreaDesignProductionMethodPrice $price
     *
     * @return string
     */
    private function createHashFromPrice(string $methodIdentifier, string $typeIdentifier, DesignAreaDesignProductionMethodPrice $price): string
    {
        return implode('#', [$methodIdentifier, $typeIdentifier, $price->channel, $price->colorAmountFrom, $price->amountFrom]);
    }
}
