<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification;

/**
 * @internal
 */
class OptionClassificationValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var OptionclassificationRepository
     */
    private $optionClassificationRepository;

    /**
     * OptionClassificationValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param OptionclassificationRepository $optionClassificationRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        OptionclassificationRepository $optionClassificationRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->optionClassificationRepository = $optionClassificationRepository;
    }

    /**
     * @param OptionClassification $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $optionClassification = $this->optionClassificationRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($optionClassification)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate optionClassification texts
        if (!empty($structure->texts)) {
            foreach ($structure->texts as $optionClassificationText) {
                $needsValidation = $this->structureValidator->needsValidation($optionClassificationText);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($optionClassificationText);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
