<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeStructure;

/**
 * @internal
 */
class DesignerGlobalCalculationTypeValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var DesignerGlobalCalculationTypeRepository
     */
    private $designerGlobalCalculationTypeRepository;

    /**
     * @param StructureValidator $structureValidator
     * @param DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->designerGlobalCalculationTypeRepository = $designerGlobalCalculationTypeRepository;
    }

    /**
     * @param DesignerGlobalCalculationTypeStructure $structure
     *
     * @throws \Exception
     */
    public function validate(
        $structure
    ) {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (!empty($structure->identifier)) {
            $designerGlobalCalculationType = $this->designerGlobalCalculationTypeRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($designerGlobalCalculationType) && $designerGlobalCalculationType->getId() != $structure->id) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate if amount amount dependant or has prices
        if (true === $structure->pricePerItem && !empty($structure->prices)) {
            $violations[] = [
                'property' => 'pricePerItem',
                'invalidvalue' => $structure->pricePerItem,
                'message' => 'conflict',
            ];
            $violations[] = [
                'property' => 'prices',
                'invalidvalue' => $structure->prices,
                'message' => 'conflict',
            ];
        }

        // validate that only one price is allowed per DesignerGlobalCalculationType + Channel
        $countsByChannel = [];
        $pricesByChannel = [];
        foreach ($structure->prices as $price) {
            if (isset($countsByChannel[$price->channel])) {
                ++$countsByChannel[$price->channel];
            } else {
                $countsByChannel[$price->channel] = 1;
            }
            $pricesByChannel[$price->channel][] = $price;
        }

        foreach ($countsByChannel as $channel => $count) {
            if ($count > 1) {
                $violations[] = [
                    'property' => 'prices',
                    'invalidvalue' => $pricesByChannel[$channel],
                    'message' => 'duplicate_entry',
                ];
            }
        }

        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->texts,
            $violations,
            'price'
        );

        if ($structure->prices) {
            $violations = $this->structureValidator->validateSubStructureMany(
                $structure->prices,
                $violations,
                'text'
            );
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
