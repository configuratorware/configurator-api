<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;

/**
 * @internal
 */
class OptionValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * OptionValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param OptionRepository $optionRepository
     */
    public function __construct(StructureValidator $structureValidator, OptionRepository $optionRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param Option $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $option = $this->optionRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($option)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate option texts
        if (!empty($structure->texts)) {
            foreach ($structure->texts as $optionText) {
                $needsValidation = $this->structureValidator->needsValidation($optionText);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($optionText);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
