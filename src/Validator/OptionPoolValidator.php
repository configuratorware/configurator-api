<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionPoolRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool;

/**
 * @internal
 */
class OptionPoolValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var OptionPoolRepository
     */
    private $optionPoolRepository;

    /**
     * OptionPoolValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param OptionPoolRepository $optionPoolRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        OptionPoolRepository $optionPoolRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->optionPoolRepository = $optionPoolRepository;
    }

    /**
     * @param OptionPool $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $optionPool = $this->optionPoolRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($optionPool)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
