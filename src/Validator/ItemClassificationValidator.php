<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification;

/**
 * @internal
 */
class ItemClassificationValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ItemclassificationRepository
     */
    private $itemClassificationRepository;

    /**
     * ItemClassificationValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param ItemclassificationRepository $itemClassificationRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        ItemclassificationRepository $itemClassificationRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->itemClassificationRepository = $itemClassificationRepository;
    }

    /**
     * @param ItemClassification $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $itemClassification = $this->itemClassificationRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($itemClassification)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate itemClassification texts
        if (!empty($structure->texts)) {
            foreach ($structure->texts as $itemClassificationText) {
                $needsValidation = $this->structureValidator->needsValidation($itemClassificationText);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($itemClassificationText);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
