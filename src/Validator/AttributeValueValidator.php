<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue;

/**
 * @internal
 */
class AttributeValueValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var AttributevalueRepository
     */
    private $attributeValueRepository;

    /**
     * AttributeValueValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param AttributevalueRepository $attributeValueRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        AttributevalueRepository $attributeValueRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->attributeValueRepository = $attributeValueRepository;
    }

    /**
     * @param Attributevalue $structure
     *
     * @throws \Exception
     */
    public function validate(Attributevalue $structure): void
    {
        $violations = $this->structureValidator->validate($structure);

        if (empty($structure->id) && !empty($structure->value)) {
            $attribute = $this->attributeValueRepository->findOneByValue($structure->value);

            if (!empty($attribute)) {
                $violations[] = [
                    'property' => 'value',
                    'invalidvalue' => $structure->value,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate translations
        if (!empty($structure->translations)) {
            foreach ($structure->translations as $attributeValueTranslation) {
                $needsValidation = $this->structureValidator->needsValidation($attributeValueTranslation);

                if (true === $needsValidation) {
                    $translationViolations = $this->structureValidator->validate($attributeValueTranslation);

                    $violations = array_merge($violations, $translationViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
