<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignDataTransfer;

/**
 * @internal
 */
class DesignDataTransferValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * DesignDataTransferValidator constructor.
     *
     * @param StructureValidator $structureValidator
     */
    public function __construct(StructureValidator $structureValidator)
    {
        $this->structureValidator = $structureValidator;
    }

    /**
     * @param DesignDataTransfer $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
