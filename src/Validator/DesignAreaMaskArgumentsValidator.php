<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DesignAreaMaskArguments;

/**
 * @internal
 */
class DesignAreaMaskArgumentsValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @param StructureValidator $structureValidator
     */
    public function __construct(StructureValidator $structureValidator)
    {
        $this->structureValidator = $structureValidator;
    }

    /**
     * @param DesignAreaMaskArguments $structure
     *
     * @throws \Exception
     */
    public function validate(DesignAreaMaskArguments $structure): void
    {
        $violations = $this->structureValidator->validate($structure);

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
