<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\TagRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Tag;

/**
 * @internal
 */
class TagValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * TagValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param TagRepository $tagRepository
     */
    public function __construct(StructureValidator $structureValidator, TagRepository $tagRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param Tag $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate tag translations
        if (!empty($structure->translations)) {
            foreach ($structure->translations as $tagTranslation) {
                $needsValidation = $this->structureValidator->needsValidation($tagTranslation);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($tagTranslation);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
