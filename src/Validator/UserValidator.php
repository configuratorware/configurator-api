<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;

/**
 * @internal
 */
class UserValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param UserRepository $userRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        UserRepository $userRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->userRepository = $userRepository;
    }

    /**
     * @param User $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if user is unique (username and email)
        if (empty($structure->id) && (!empty($structure->username) || !empty($structure->email))) {
            $user = $this->userRepository->findOneBy(['username' => $structure->username]);

            if (!empty($user)) {
                $violations[] = [
                    'property' => 'username',
                    'invalidvalue' => $structure->username,
                    'message' => 'duplicate_entry',
                ];
            }

            $user = $this->userRepository->findOneBy(['email' => $structure->email]);

            if (!empty($user)) {
                $violations[] = [
                    'property' => 'email',
                    'invalidvalue' => $structure->email,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
