<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerProductionCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod as DesignProductionMethodStructure;

/**
 * @internal
 */
class DesignProductionMethodValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var DesignerProductionCalculationTypeRepository
     */
    private $designerProductionCalculationTypeRepository;

    /**
     * @var DesignProductionMethodRepository
     */
    private $designProductionMethodRepository;

    /**
     * DesignProductionMethodValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository
     * @param DesignProductionMethodRepository $designProductionMethodRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository,
        DesignProductionMethodRepository $designProductionMethodRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->designerProductionCalculationTypeRepository = $designerProductionCalculationTypeRepository;
        $this->designProductionMethodRepository = $designProductionMethodRepository;
    }

    /**
     * @param DesignProductionMethodStructure $structure
     *
     * @throws \Exception
     */
    public function validate(
        $structure
    ) {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (!empty($structure->identifier)) {
            $designProductionMethod = $this->designProductionMethodRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($designProductionMethod) && $designProductionMethod->getId() != $structure->id) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate texts
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->texts,
            $violations,
            'texts'
        );

        // validate colorPalettes
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->colorPalettes,
            $violations,
            'colorPalettes'
        );

        foreach ($structure->calculationTypes as $i => $type) {
            // General Type Validation
            $calculationTypeMarker = 'calculationTypes[' . $i . ']';

            $preViolations = $this->structureValidator->validate($type);
            foreach ($preViolations as $k => $violation) {
                $preViolations[$k]['property'] = $calculationTypeMarker . '.' . $violation['property'];
            }

            $violations = array_merge($violations, $preViolations);

            // validate if identifier is unique
            if (!empty($structure->identifier)) {
                $designerProductionCalculationType = $this->designerProductionCalculationTypeRepository->findOneBy(
                    ['identifier' => $type->identifier]
                );

                if (!empty($designerProductionCalculationType) && $designerProductionCalculationType->getId() != $type->id) {
                    $violations[] = [
                        'property' => $calculationTypeMarker . '.identifier',
                        'invalidvalue' => $type->identifier,
                        'message' => 'duplicate_entry',
                    ];
                }
            }

            // validate texts
            if ($type->texts) {
                $violations = $this->structureValidator->validateSubStructureMany(
                    $type->texts,
                    $violations,
                    $calculationTypeMarker . 'texts'
                );
            }

            // validate colorPalettes
            if ($type->prices) {
                $violations = $this->structureValidator->validateSubStructureMany(
                    $type->prices,
                    $violations,
                    $calculationTypeMarker . 'prices'
                );
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
