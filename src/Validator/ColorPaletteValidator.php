<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorPaletteRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPalette;

/**
 * @internal
 */
class ColorPaletteValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ColorPaletteRepository
     */
    private $colorPaletteRepository;

    /**
     * @var ColorRepository
     */
    private $colorRepository;

    /**
     * @param StructureValidator $structureValidator
     * @param ColorPaletteRepository $colorPaletteRepository
     * @param ColorRepository $colorRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        ColorPaletteRepository $colorPaletteRepository,
        ColorRepository $colorRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->colorPaletteRepository = $colorPaletteRepository;
        $this->colorRepository = $colorRepository;
    }

    /**
     * @param ColorPalette $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (!empty($structure->identifier)) {
            $colorPalette = $this->colorPaletteRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($colorPalette) && $colorPalette->getId() != $structure->id) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate texts
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->texts,
            $violations,
            'texts'
        );

        // validate colors
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->colors,
            $violations,
            'colors'
        );

        // Validate if color identifier is unique per color palette
        if (!empty($structure->colors)) {
            $uniqueColors = [];
            $colorViolations = [];
            foreach ($structure->colors as $i => $colorStructure) {
                // count color entries per identifier to violate duplicate entries
                if (!isset($uniqueColors[$colorStructure->identifier])) {
                    $uniqueColors[$colorStructure->identifier] = 1;
                } else {
                    $colorViolations[$colorStructure->identifier] = [
                        'property' => 'colors[' . $i . '].identifier',
                        'invalidvalue' => $colorStructure->identifier,
                        'message' => 'duplicate_entry',
                    ];

                    ++$uniqueColors[$colorStructure->identifier];

                    continue;
                }

                // unique colors per color palette do not have to be validated against the database if a new color palette is created
                // if there is already a violation for the current color do not check against the database again
                if (!empty($structure->id)
                    && !isset($colorViolations[$colorStructure->identifier])) {
                    $color = $this->colorRepository->findOneBy([
                        'identifier' => $colorStructure->identifier,
                        'colorPalette' => $structure->id,
                    ]);
                    if (!empty($color)
                        && $colorStructure->id != $color->getId()) {
                        $colorViolations[$colorStructure->identifier] = [
                            'property' => 'colors[' . $i . '].identifier',
                            'invalidvalue' => $colorStructure->identifier,
                            'message' => 'duplicate_entry',
                        ];
                    }
                }
            }

            // merge violations
            if (!empty($colorViolations)) {
                $violations = array_merge($violations, array_values($colorViolations));
            }
        }

        // validate colorTexts
        foreach ($structure->colors as $color) {
            $violations = $this->structureValidator->validateSubStructureMany(
                $color->texts,
                $violations,
                'colorTexts'
            );
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
