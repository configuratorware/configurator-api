<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Channel;

/**
 * @internal
 */
class ChannelValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * ChannelValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param ChannelRepository $channelRepository
     */
    public function __construct(StructureValidator $structureValidator, ChannelRepository $channelRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->channelRepository = $channelRepository;
    }

    /**
     * @param Channel $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $channel = $this->channelRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($channel)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
