<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequest as DesignerUserImageRequestStructure;

/**
 * @internal
 */
class UserImageEditDataValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * DesignAreaValidator constructor.
     *
     * @param StructureValidator $structureValidator
     */
    public function __construct(
        StructureValidator $structureValidator
    ) {
        $this->structureValidator = $structureValidator;
    }

    /**
     * @param DesignerUserImageRequestStructure $structure
     *
     * @throws \Exception
     */
    public function validate(
        DesignerUserImageRequestStructure $structure
    ) {
        $violations = $this->structureValidator->validate($structure);

        // validate if filename is set while uploadedFile is not send
        if (!$structure->uploadedFile && empty($structure->fileName)) {
            $violations[] = [
                'property' => 'fileName',
                'invalidvalue' => $structure->fileName,
                'message' => 'empty',
            ];
        }

        // validate operations
        $violations = $this->structureValidator->validateSubStructureMany(
            (array)$structure->operations,
            $violations,
            'operations'
        );

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
