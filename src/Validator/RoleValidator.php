<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Role;

/**
 * @internal
 */
class RoleValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * RoleValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param RoleRepository $roleRepository
     */
    public function __construct(StructureValidator $structureValidator, RoleRepository $roleRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param Role $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if name is unique
        if (empty($structure->id) && !empty($structure->name)) {
            $role = $this->roleRepository->findOneBy(['name' => $structure->name]);

            if (!empty($role)) {
                $violations[] = [
                    'property' => 'name',
                    'invalidvalue' => $structure->name,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
