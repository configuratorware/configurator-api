<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Group;

/**
 * @internal
 */
class GroupValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ItemgroupRepository
     */
    private $groupRepository;

    /**
     * GroupValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param ItemgroupRepository $groupRepository
     */
    public function __construct(StructureValidator $structureValidator, ItemgroupRepository $groupRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->groupRepository = $groupRepository;
    }

    /**
     * @param Group $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $group = $this->groupRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($group)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate group texts
        if (!empty($structure->texts)) {
            foreach ($structure->texts as $groupText) {
                $needsValidation = $this->structureValidator->needsValidation($groupText);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($groupText);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
