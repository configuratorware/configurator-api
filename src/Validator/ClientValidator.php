<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientUserCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Client;

/**
 * @internal
 */
class ClientValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ClientUserCheck
     */
    private $clientUserCheck;

    /**
     * ClientValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param ClientRepository $clientRepository
     * @param ClientUserCheck $clientUserCheck
     */
    public function __construct(
        StructureValidator $structureValidator,
        ClientRepository $clientRepository,
        ClientUserCheck $clientUserCheck
    ) {
        $this->structureValidator = $structureValidator;
        $this->clientRepository = $clientRepository;
        $this->clientUserCheck = $clientUserCheck;
    }

    /**
     * @param Client $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $client = $this->clientRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($client)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (isset($structure->texts)) {
            foreach ($structure->texts as $text) {
                if (!isset($text->id)) {
                    $violations[] = [
                        'property' => 'link',
                        'invalidvalue' => $text,
                        'message' => 'id_missing',
                    ];
                }
            }
        }

        // check if the given users are already connected to another client
        if (isset($structure->users)) {
            foreach ($structure->users as $user) {
                $userAlreadyRelated = $this->clientUserCheck->isUserEmailValidForClientId($user->email, $structure->id);
                if (false === $userAlreadyRelated) {
                    $violations[] = [
                        'property' => 'users',
                        'invalidvalue' => $user,
                        'message' => 'already_related_to_another_client',
                    ];
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
