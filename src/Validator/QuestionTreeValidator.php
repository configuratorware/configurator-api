<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\QuestionTreeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTree as QuestionTreeStructure;

/**
 * @internal
 */
class QuestionTreeValidator
{
    private StructureValidator $structureValidator;

    private QuestionTreeRepository $repository;

    public function __construct(QuestionTreeRepository $repository, StructureValidator $structureValidator)
    {
        $this->repository = $repository;
        $this->structureValidator = $structureValidator;
    }

    /**
     * @param QuestionTreeStructure $structure
     *
     * @throws ValidationException|\Exception
     */
    public function validate(QuestionTreeStructure $structure): void
    {
        $violations = $this->structureValidator->validate($structure);

        if (!empty($structure->identifier)) {
            $violations = $this->validateDuplicateIdentifier($structure, $violations);
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateDuplicateIdentifier(QuestionTreeStructure $structure, array $violations): array
    {
        $questionTree = $this->repository->findOneBy(['identifier' => $structure->identifier]);

        if ($questionTree instanceof QuestionTree && $structure->id !== $questionTree->getId()) {
            $violations[] = [
                'property' => 'identifier',
                'invalidvalue' => $structure->identifier,
                'message' => 'duplicate_entry',
            ];
        }

        return $violations;
    }
}
