<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;

/**
 * @internal
 */
class ReceiveOfferRequestValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * ReceiveOfferRequestValidator constructor.
     *
     * @param StructureValidator $structureValidator
     */
    public function __construct(
        StructureValidator $structureValidator
    ) {
        $this->structureValidator = $structureValidator;
    }

    /**
     * @param ReceiveOfferRequestStructure $structure
     *
     * @throws \Exception
     */
    public function validate(
        ReceiveOfferRequestStructure $structure
    ) {
        $violations = $this->structureValidator->validate($structure);

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
