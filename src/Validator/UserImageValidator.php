<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageDataProvider;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @internal
 */
class UserImageValidator
{
    /**
     * list of allowed mime types.
     *
     * @var array
     */
    private $allowedMimeTypes = [
        'image/bmp', // BMP
        'image/x-bmp',
        'image/x-ms-bmp',
        'image/gif', // GIF
        'image/jpeg', // JPG
        'image/pjpeg',
        'image/png', // PNG
        'image/tiff', // TIFF
        'image/tiff-fx',
        'application/pdf', // PDF
        'application/x-pdf',
        'application/x-bzpdf',
        'application/x-gzpdf',
        'image/svg+xml', // SVG
        'image/eps', // EPS
        'image/x-eps',
        'image/x-ept',
        'application/eps',
        'application/x-eps',
        'application/x-ept',
        'application/postscript', // AI, EPS, PS
    ];

    /**
     * @var ImageDataProvider
     */
    private $imageDataProvider;

    public function __construct(
        ImageDataProvider $imageDataProvider
    ) {
        $this->imageDataProvider = $imageDataProvider;
    }

    /**
     * @param UploadedFile|null $imageFormData
     */
    public function validate(?UploadedFile $imageFormData): void
    {
        $violations = [];

        if (null === $imageFormData || 0 === $imageFormData->getSize()) {
            $violations[] = [
                'property' => 'uploaded_file',
                'invalidvalue' => 'uploaded_file',
                'message' => 'error_uploading_file',
            ];

            throw new ValidationException($violations);
        }

        // validate file size
        if (0 === $imageFormData->getSize() || $imageFormData->getSize() > $this->getMaxAllowedUploadFileSizeInKB() * 1024) {
            $violations[] = [
                'property' => 'uploaded_file',
                'invalidvalue' => 'uploaded_file',
                'message' => 'file_too_big',
            ];
        }

        // validate mime type

        // if an svg does not contain the xml tag the mime type is not guessed correctly
        // so we add the tag if its not present
        if ('svg' === strtolower($imageFormData->getClientOriginalExtension())) {
            $svgContent = file_get_contents($imageFormData->getPathName());
            if (0 !== strpos($svgContent, '<?xml')) {
                file_put_contents($imageFormData->getPathName(), '<?xml version="1.0" encoding="utf-8"?>' . $svgContent);
            }
        }

        if (!in_array($imageFormData->getMimeType(), $this->allowedMimeTypes, true)) {
            $fileTypeNotAllowed = [
                'property' => 'uploaded_file',
                'invalidvalue' => 'uploaded_file',
                'message' => 'file_type_not_allowed',
            ];

            if ('eps' === strtolower($imageFormData->getClientOriginalExtension())) {
                // $imageFormData->getMimeType() has issues to find correct mime type for eps
                // therefore a check with imagick is implemented if client says eps
                $imageData = $this->imageDataProvider->provide($imageFormData->getPathName());
                if (!in_array($imageData->getMimeType(), $this->allowedMimeTypes, true)) {
                    $violations[] = $fileTypeNotAllowed;
                }
            } else {
                $violations[] = $fileTypeNotAllowed;
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }

    /**
     * read max upload file size from php config and convert to KB.
     *
     * @return int
     */
    private function getMaxAllowedUploadFileSizeInKB(): int
    {
        $maxUploadFileSizeInKB = 0;

        $maxUploadFileSize = ini_get('upload_max_filesize');

        if (!empty($maxUploadFileSize)) {
            $maxUploadFileSize = trim($maxUploadFileSize);

            $unit = $maxUploadFileSize[strlen($maxUploadFileSize) - 1];

            $unit = strtoupper($unit);

            switch ($unit) {
                case 'G':
                    $maxUploadFileSizeInKB = (int)$maxUploadFileSize * 1024 * 1024;

                    break;
                case 'M':
                    $maxUploadFileSizeInKB = (int)$maxUploadFileSize * 1024;

                    break;
                case 'K':
                    $maxUploadFileSizeInKB = (int)$maxUploadFileSize;

                    break;
                default:
                    $maxUploadFileSizeInKB = (int)$maxUploadFileSize / 1024;

                    break;
            }
        }

        return (int)$maxUploadFileSizeInKB;
    }
}
