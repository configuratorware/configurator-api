<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ApiUser;

/**
 * @internal
 */
class ApiUserValidator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserValidator constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param ApiUser $structure
     */
    public function validate(ApiUser $structure): void
    {
        $violations = [];

        // validate if APIuser has a username
        if (empty($structure->username)) {
            $violations[] = [
                'property' => 'username',
                'invalidvalue' => $structure->username,
                'message' => 'This value should not be blank.',
            ];
        }

        if (empty($structure->id)) {
            // validate if username is unique
            if (!empty($structure->username)) {
                $user = $this->userRepository->findOneBy(['username' => $structure->username]);
                if (null !== $user) {
                    $violations[] = [
                        'property' => 'username',
                        'invalidvalue' => $structure->username,
                        'message' => 'duplicate_entry',
                    ];
                }
            }

            // validate if email is unique
            if (!empty($structure->email)) {
                $user = $this->userRepository->findOneBy(['email' => $structure->email]);
                if (null !== $user) {
                    $violations[] = [
                        'property' => 'email',
                        'invalidvalue' => $structure->email,
                        'message' => 'duplicate_entry',
                    ];
                }
            }

            // validate if api_key is unique
            if (!empty($structure->api_key)) {
                $user = $this->userRepository->findOneBy(['api_key' => $structure->api_key]);
                if (null !== $user) {
                    $violations[] = [
                        'property' => 'api_key',
                        'invalidvalue' => $structure->api_key,
                        'message' => 'duplicate_entry',
                    ];
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
