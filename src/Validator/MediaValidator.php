<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ConfiguratorApiBundle\Events\Media\MediaValidationEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\MediaArguments;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class MediaValidator
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * MediaValidator constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param MediaArguments $mediaArguments
     */
    public function validate(MediaArguments $mediaArguments)
    {
        $event = new MediaValidationEvent($mediaArguments);
        $this->eventDispatcher->dispatch($event, MediaValidationEvent::NAME);
    }
}
