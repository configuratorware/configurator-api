<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemGroup;

/**
 * @internal
 */
class ConfigurationVariantValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ItemgroupRepository
     */
    private $itemGroupRepository;

    /**
     * ItemGroupValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param ItemgroupRepository $itemGroupRepository
     */
    public function __construct(StructureValidator $structureValidator, ItemgroupRepository $itemGroupRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->itemGroupRepository = $itemGroupRepository;
    }

    /**
     * @param ItemGroup $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $channel = $this->itemGroupRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($channel)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
