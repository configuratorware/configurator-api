<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CurrencyRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Currency;

/**
 * @internal
 */
class CurrencyValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * CurrencyValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(StructureValidator $structureValidator, CurrencyRepository $currencyRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param Currency $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if iso is unique
        if (empty($structure->id) && !empty($structure->iso)) {
            $currency = $this->currencyRepository->findOneBy(['iso' => $structure->iso]);

            if (!empty($currency)) {
                $violations[] = [
                    'property' => 'iso',
                    'invalidvalue' => $structure->iso,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
