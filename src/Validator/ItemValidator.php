<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChildItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;

/**
 * @internal
 */
class ItemValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemgroupRepository
     */
    private $groupRepository;

    /**
     * @var ItemgroupentryRepository
     */
    private $groupEntryRepository;

    /**
     * ItemValidator constructor.
     *
     * @param StructureValidator       $structureValidator
     * @param ItemRepository           $itemRepository
     * @param ItemgroupRepository      $groupRepository
     * @param ItemgroupentryRepository $groupEntryRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        ItemRepository $itemRepository,
        ItemgroupRepository $groupRepository,
        ItemgroupentryRepository $groupEntryRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->itemRepository = $itemRepository;
        $this->groupRepository = $groupRepository;
        $this->groupEntryRepository = $groupEntryRepository;
    }

    /**
     * @param Item $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $item = $this->itemRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($item)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate item texts
        if (!empty($structure->texts)) {
            foreach ($structure->texts as $itemText) {
                $needsValidation = $this->structureValidator->needsValidation($itemText);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($itemText);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        foreach ($structure->children as $child) {
            $violations = array_merge($violations, $this->validateChildStructure($child));
        }
        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }

    /**
     * @param $child
     *
     * @return array
     */
    private function validateChildStructure(ChildItem $child): array
    {
        /* @var Itemgroupentry $groupEntry */

        $violations = [];

        foreach ($child->groups as $i => $group) {
            if (null === $group->id || null === $this->groupRepository->find($group->id)) {
                $violations[] = $this->createChildItemViolation('item.children[' . $child->id . '].groups[' . $i . '].id', $group->id);
            }
            if (null === $group->groupEntry->id || null === $this->groupEntryRepository->find($group->groupEntry->id)) {
                $violations[] = $this->createChildItemViolation('item.children[' . $child->id . '].groups[' . $i . '].groupEntry.id', $group->groupEntry->id);
            }
        }

        return $violations;
    }

    /**
     * @param string $property
     * @param mixed  $invalidValue
     *
     * @return array
     */
    private function createChildItemViolation(string $property, $invalidValue): array
    {
        return ['property' => $property, 'invalidvalue' => $invalidValue, 'message' => 'no_relation'];
    }
}
