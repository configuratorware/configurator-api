<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CodeSnippetRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet;

/**
 * @internal
 */
class CodeSnippetValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var CodeSnippetRepository
     */
    private $codeSnippetRepository;

    /**
     * CodeSnippetValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param CodeSnippetRepository $codeSnippetRepository
     */
    public function __construct(StructureValidator $structureValidator, CodeSnippetRepository $codeSnippetRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->codeSnippetRepository = $codeSnippetRepository;
    }

    /**
     * @param CodeSnippet $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $codeSnippet = $this->codeSnippetRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($codeSnippet)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
