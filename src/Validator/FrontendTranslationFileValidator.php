<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;

/**
 * @internal
 */
class FrontendTranslationFileValidator
{
    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param string $mediaBasePath
     */
    public function __construct(
        string $mediaBasePath
    ) {
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
    }

    /**
     * @param string $langCode
     * @param string $uploadedTranslation Json string of transaltions
     */
    public function validate(string $langCode, string $uploadedTranslation): void
    {
        // remove utf-8 BOM
        $bom = pack('H*', 'EFBBBF');
        // check if valid json
        $uploadedArray = json_decode(preg_replace("/^$bom/", '', $uploadedTranslation), true);
        if (null === $uploadedArray) {
            throw new ValidationException(['invalid_json_file']);
        }

        if ([] === $uploadedArray) {
            throw new ValidationException(['empty_translation_file']);
        }
    }
}
