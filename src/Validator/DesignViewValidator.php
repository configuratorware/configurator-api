<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;

/**
 * @internal
 */
class DesignViewValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @param StructureValidator $structureValidator
     * @param DesignViewRepository $designViewRepository
     */
    public function __construct(
        StructureValidator $structureValidator,
        DesignViewRepository $designViewRepository
    ) {
        $this->structureValidator = $structureValidator;
        $this->designViewRepository = $designViewRepository;
    }

    /**
     * @param DesignViewStructure $structure
     *
     * @throws \Exception
     */
    public function validate(
        $structure
    ) {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique per item/option
        if (!empty($structure->identifier)) {
            if (!empty($structure->item)) {
                $designView = $this->designViewRepository->findOneBy(['identifier' => $structure->identifier, 'item' => $structure->item->id]);
            }

            if (!empty($structure->option)) {
                $designView = $this->designViewRepository->findOneBy(['identifier' => $structure->identifier, 'option' => $structure->option->id]);
            }

            if (!empty($designView) && $designView->getId() != $structure->id) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate if item or option is set
        if (empty($structure->item) && empty($structure->option)) {
            $violations[] = [
                'property' => 'item',
                'invalidvalue' => $structure->item,
                'message' => 'empty_relation',
            ];
            $violations[] = [
                'property' => 'option',
                'invalidvalue' => $structure->option,
                'message' => 'empty_relation',
            ];
        }

        // validate Item
        if (!empty($structure->item) && empty($structure->option)) {
            $violations = $this->structureValidator->validateSubStructureOne(
                $structure->item,
                $violations,
                'item'
            );
        }

        // validate Option
        if (empty($structure->item) && !empty($structure->option)) {
            $violations = $this->structureValidator->validateSubStructureOne(
                $structure->option,
                $violations,
                'option'
            );
        }

        // validate if only item OR option is set in structure
        if (!empty($structure->item) && !empty($structure->option)) {
            $violations[] = [
                'property' => 'item',
                'invalidvalue' => $structure->item,
                'message' => 'multiple_relations',
            ];
            $violations[] = [
                'property' => 'option',
                'invalidvalue' => $structure->option,
                'message' => 'multiple_relations',
            ];
        }

        // validate texts
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->texts,
            $violations,
            'texts'
        );

        // validate designAreas
        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->designAreas,
            $violations,
            'designAreas'
        );

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
