<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attribute;

/**
 * @internal
 */
class AttributeValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * AttributeValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param AttributeRepository $attributeRepository
     */
    public function __construct(StructureValidator $structureValidator, AttributeRepository $attributeRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * @param Attribute $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $attribute = $this->attributeRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($attribute)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate attribute texts
        if (!empty($structure->texts)) {
            foreach ($structure->texts as $attributeText) {
                $needsValidation = $this->structureValidator->needsValidation($attributeText);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($attributeText);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
