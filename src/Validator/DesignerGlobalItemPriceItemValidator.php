<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPriceItem;

/**
 * @internal
 */
class DesignerGlobalItemPriceItemValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * DesignerGlobalItemPriceValidator constructor.
     *
     * @param StructureValidator $structureValidator
     */
    public function __construct(StructureValidator $structureValidator)
    {
        $this->structureValidator = $structureValidator;
    }

    /**
     * @param DesignerGlobalItemPriceItem $structure
     *
     * @throws \Exception
     */
    public function validate(
        $structure
    ) {
        $violations = $this->structureValidator->validate($structure);

        $violations = $this->structureValidator->validateSubStructureMany(
            $structure->calculationTypes,
            $violations,
            'calculationTypes'
        );

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
