<?php

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @internal
 */
class FileValidator
{
    /**
     * @param UploadedFile $uploadedFile
     * @param array $allowedMimeTypes
     * @param string $regExStringForCharacterCheckOnFileName
     */
    public function validate(
        UploadedFile $uploadedFile,
        array $allowedMimeTypes,
        string $regExStringForCharacterCheckOnFileName = ''
    ) {
        $violations = $this->check(
            $uploadedFile,
            $allowedMimeTypes,
            $regExStringForCharacterCheckOnFileName
        );

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }

    /**
     * @param array $files
     * @param array $allowedMimeTypes
     * @param string $regExStringForCharacterCheckOnFileName
     */
    public function validateMany(
        array $files,
        array $allowedMimeTypes,
        string $regExStringForCharacterCheckOnFileName = ''
    ) {
        $violations = [];

        foreach ($files as $file) {
            $fileViolations = $this->check(
                $file,
                $allowedMimeTypes,
                $regExStringForCharacterCheckOnFileName
            );
            $violations = array_merge($violations, $fileViolations);
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }

    /**
     * @param UploadedFile $uploadedFile
     * @param array $allowedMimeTypes
     * @param string $regExStringForCharacterCheckOnFileName
     *
     * @return array
     */
    private function check(
        UploadedFile $uploadedFile,
        array $allowedMimeTypes,
        string $regExStringForCharacterCheckOnFileName = ''
    ) {
        $violations = [];

        if (empty($uploadedFile) || 0 == $uploadedFile->getSize()) {
            $violations[] = [
                'property' => 'uploaded_file',
                'invalidvalue' => $uploadedFile->getClientOriginalName(),
                'message' => 'error_uploading_file',
            ];
        } else {
            // validate mime type
            if (!in_array($uploadedFile->getMimeType(), $allowedMimeTypes)) {
                $violations[] = [
                    'property' => 'uploaded_file',
                    'invalidvalue' => $uploadedFile->getClientOriginalName(),
                    'message' => 'file_type_not_allowed',
                ];
            }
            // validate Filename
            if (!empty($regExStringForCharacterCheckOnFileName) &&
                preg_match(
                    $regExStringForCharacterCheckOnFileName,
                    $uploadedFile->getClientOriginalName()
                )
            ) {
                $violations[] = [
                    'property' => 'uploaded_file',
                    'invalidvalue' => $uploadedFile->getClientOriginalName(),
                    'message' => 'invalid_file_name',
                ];
            }
        }

        return $violations;
    }
}
