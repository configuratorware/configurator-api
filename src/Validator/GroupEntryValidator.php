<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Validator;

use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Group;

/**
 * @internal
 */
class GroupEntryValidator
{
    /**
     * @var StructureValidator
     */
    private $structureValidator;

    /**
     * @var ItemgroupentryRepository
     */
    private $groupEntryRepository;

    /**
     * GroupEntryValidator constructor.
     *
     * @param StructureValidator $structureValidator
     * @param ItemgroupentryRepository $groupEntryRepository
     */
    public function __construct(StructureValidator $structureValidator, ItemgroupentryRepository $groupEntryRepository)
    {
        $this->structureValidator = $structureValidator;
        $this->groupEntryRepository = $groupEntryRepository;
    }

    /**
     * @param Group $structure
     *
     * @throws \Exception
     */
    public function validate($structure)
    {
        $violations = $this->structureValidator->validate($structure);

        // validate if identifier is unique
        if (empty($structure->id) && !empty($structure->identifier)) {
            $group = $this->groupEntryRepository->findOneBy(['identifier' => $structure->identifier]);

            if (!empty($group)) {
                $violations[] = [
                    'property' => 'identifier',
                    'invalidvalue' => $structure->identifier,
                    'message' => 'duplicate_entry',
                ];
            }
        }

        // validate group texts
        if (!empty($structure->texts)) {
            foreach ($structure->texts as $groupText) {
                $needsValidation = $this->structureValidator->needsValidation($groupText);

                if (true === $needsValidation) {
                    $textViolations = $this->structureValidator->validate($groupText);
                    $violations = array_merge($violations, $textViolations);
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }
    }
}
