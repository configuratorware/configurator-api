<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AdminMode;

/**
 * @internal
 */
class AdminModeHelper
{
    /**
     * @return bool
     *
     * @deprecated will be removed in a later version, C_ADMIN_MODE will be removed completely
     */
    public function isAdminModeActive()
    {
        $isActive = false;
        if (defined('C_ADMIN_MODE') && C_ADMIN_MODE === true) {
            $isActive = true;
        }

        return $isActive;
    }
}
