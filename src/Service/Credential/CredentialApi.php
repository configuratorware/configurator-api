<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Credential;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Repository\CredentialRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class CredentialApi
{
    /**
     * @var CredentialRepository
     */
    private $credentialRepository;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * CredentialApi constructor.
     *
     * @param CredentialRepository $credentialRepository
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        CredentialRepository $credentialRepository,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->credentialRepository = $credentialRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->credentialRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->structureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->credentialRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['credential.area', 'credential.identifier'];
    }
}
