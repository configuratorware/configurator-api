<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Credential;

use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserCredentials;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @internal
 */
class CredentialChecker
{
    /**
     * @var UserCredentials
     */
    private $userCredentials;

    /**
     * CredentialChecker constructor.
     *
     * @param UserCredentials $userCredentials
     */
    public function __construct(UserCredentials $userCredentials)
    {
        $this->userCredentials = $userCredentials;
    }

    /**
     * @param User|UserInterface|null $user
     * @param string|array $credentials
     *
     * @return bool
     */
    public function check(?UserInterface $user = null, $credentials = '')
    {
        // shortcut checks
        if (empty($credentials)) {
            return true;
        }
        if (empty($user)) {
            return false;
        }

        $userCredentials = $this->userCredentials->getByUser($user);

        if (is_array($credentials)) {
            return [] !== array_intersect($credentials, $userCredentials);
        }

        return in_array($credentials, $userCredentials);
    }
}
