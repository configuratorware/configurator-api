<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\LayerImageRepositoryInterface;

/**
 * @internal
 */
final class PossibleNamingIssues
{
    /**
     * @var LayerImageRepositoryInterface
     */
    private $layerImageRepository;

    public function __construct(LayerImageRepositoryInterface $layerImageRepository)
    {
        $this->layerImageRepository = $layerImageRepository;
    }

    public function getPossibleNamingIssues(Item $item): array
    {
        $existingOptionsIdentifierHashes = [];

        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($item->getItemOptionclassification() as $itemOptionClassification) {
            foreach ($itemOptionClassification->getItemOptionclassificationOption() as $itemOptionClassificationOption) {
                $existingOptionsIdentifierHashes[] = $itemOptionClassification->getOptionclassification()->getIdentifier() . $itemOptionClassificationOption->getOption()->getIdentifier();
            }
        }

        $possibleNamingIssues = [];
        $possibleLayerImages = $this->layerImageRepository->findLayerImages($item);

        foreach ($possibleLayerImages as $possibleLayerImage) {
            $hash = $possibleLayerImage->getComponentIdentifier() . $possibleLayerImage->getOptionIdentifier();
            if (!in_array($hash, $existingOptionsIdentifierHashes, true)) {
                $possibleNamingIssues[] = $possibleLayerImage->getUrl();
            }
        }

        return $possibleNamingIssues;
    }
}
