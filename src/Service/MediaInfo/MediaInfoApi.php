<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\MediaInfo;

final class MediaInfoApi
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**co
     * @var ViewMediaInfo
     */
    private $viewMediaInfo;

    /**
     * @var ComponentMediaInfo
     */
    private $componentMediaInfo;
    /**
     * @var PossibleNamingIssues
     */
    private $possibleNamingIssues;

    /**
     * @param ItemRepository $itemRepository
     * @param ViewMediaInfo $viewMediaInfo
     * @param ComponentMediaInfo $componentMediaInfo
     * @param PossibleNamingIssues $possibleNamingIssues
     */
    public function __construct(
        ItemRepository $itemRepository,
        ViewMediaInfo $viewMediaInfo,
        ComponentMediaInfo $componentMediaInfo,
        PossibleNamingIssues $possibleNamingIssues
    ) {
        $this->itemRepository = $itemRepository;
        $this->viewMediaInfo = $viewMediaInfo;
        $this->componentMediaInfo = $componentMediaInfo;
        $this->possibleNamingIssues = $possibleNamingIssues;
    }

    /**
     * @param int $id
     *
     * @return MediaInfo
     */
    public function findMediaInfo(int $id): MediaInfo
    {
        $item = $this->itemRepository->findOneById($id);

        if (null === $item) {
            throw NotFoundException::entityWithId(Item::class, $id);
        }

        return MediaInfo::from($this->viewMediaInfo->getViewMediaInfo($item), $this->componentMediaInfo->getComponentMediaInfo($item), $this->possibleNamingIssues->getPossibleNamingIssues($item));
    }
}
