<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;

/**
 * @internal
 */
final class DefaultExpectedMediaPath implements ExpectedMediaPathInterface
{
    private const EXPECTED_FILE_EXT = '.(jpeg|jpg|png)';
    private const OPTION_DETAIL_IMAGE_SUFFIX = '_fullsize';
    private const LAYER_IMAGE_SORT_ORDER = '01_';

    /**
     * @var string
     */
    private $layerViewThumbnailPath;

    /**
     * @var string
     */
    private $componentThumbnailPath;
    /**
     * @var string
     */
    private $optionImagePath;
    /**
     * @var string
     */
    private $layerImagePath;

    /**
     * @param string $layerViewThumbnailPath
     * @param string $componentThumbnailPath
     * @param string $optionImagePath
     * @param string $layerImagePath
     */
    public function __construct(string $layerViewThumbnailPath, string $componentThumbnailPath, string $optionImagePath, string $layerImagePath)
    {
        $this->layerViewThumbnailPath = trim($layerViewThumbnailPath, DIRECTORY_SEPARATOR);
        $this->componentThumbnailPath = trim($componentThumbnailPath, DIRECTORY_SEPARATOR);
        $this->optionImagePath = trim($optionImagePath, DIRECTORY_SEPARATOR);
        $this->layerImagePath = trim($layerImagePath, DIRECTORY_SEPARATOR);
    }

    public function forViewThumbnail(CreatorView $view): string
    {
        return DIRECTORY_SEPARATOR . $this->layerViewThumbnailPath . DIRECTORY_SEPARATOR . $view->getIdentifier() . self::EXPECTED_FILE_EXT;
    }

    public function forComponentThumbnail(Optionclassification $component): string
    {
        return DIRECTORY_SEPARATOR . $this->componentThumbnailPath . DIRECTORY_SEPARATOR . $component->getIdentifier() . self::EXPECTED_FILE_EXT;
    }

    public function forOptionDetailImage(Option $option): string
    {
        return DIRECTORY_SEPARATOR . $this->optionImagePath . DIRECTORY_SEPARATOR . $option->getIdentifier() . self::OPTION_DETAIL_IMAGE_SUFFIX . self::EXPECTED_FILE_EXT;
    }

    public function forOptionThumbnail(Option $option): string
    {
        return DIRECTORY_SEPARATOR . $this->optionImagePath . DIRECTORY_SEPARATOR . $option->getIdentifier() . self::EXPECTED_FILE_EXT;
    }

    public function forLayerImage(Item $item, Optionclassification $component, Option $option, CreatorView $view): string
    {
        return DIRECTORY_SEPARATOR . $this->layerImagePath . DIRECTORY_SEPARATOR . $item->getIdentifier()
            . DIRECTORY_SEPARATOR . self::LAYER_IMAGE_SORT_ORDER . $view->getIdentifier()
            . DIRECTORY_SEPARATOR . self::LAYER_IMAGE_SORT_ORDER . $component->getIdentifier()
            . DIRECTORY_SEPARATOR . $option->getIdentifier() . self::EXPECTED_FILE_EXT;
    }
}
