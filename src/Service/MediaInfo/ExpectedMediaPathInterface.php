<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;

interface ExpectedMediaPathInterface
{
    public function forViewThumbnail(CreatorView $view): string;

    public function forComponentThumbnail(Optionclassification $component): string;

    public function forOptionDetailImage(Option $option): string;

    public function forOptionThumbnail(Option $option): string;

    public function forLayerImage(Item $item, Optionclassification $component, Option $option, CreatorView $view): string;
}
