<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ComponentThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\LayerImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\LayerImage;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\MediaInfoComponent;
use Redhotmagma\ConfiguratorApiBundle\Structure\MediaInfoImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\MediaInfoOption;
use Redhotmagma\ConfiguratorApiBundle\Structure\MediaInfoOptionImages;

/**
 * @internal
 */
final class ComponentMediaInfo
{
    /**
     * @var ComponentThumbnailRepositoryInterface
     */
    private $componentImageRepository;

    /**
     * @var ExpectedMediaPathInterface
     */
    private $expectedMediaPath;

    /**
     * @var LayerImageRepositoryInterface
     */
    private $layerImageRepository;

    /**
     * @var OptionThumbnailRepositoryInterface
     */
    private $optionThumbnailRepository;

    /**
     * @var OptionDetailImageRepositoryInterface
     */
    private $optionDetailImageRepository;

    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @param ComponentThumbnailRepositoryInterface $componentImageRepository
     * @param ExpectedMediaPathInterface $expectedMediaPath
     * @param LayerImageRepositoryInterface $layerImageRepository
     * @param OptionThumbnailRepositoryInterface $optionThumbnailRepository
     * @param OptionDetailImageRepositoryInterface $optionDetailImageRepository
     * @param CreatorViewRepository $creatorViewRepository
     */
    public function __construct(
        ComponentThumbnailRepositoryInterface $componentImageRepository,
        ExpectedMediaPathInterface $expectedMediaPath,
        LayerImageRepositoryInterface $layerImageRepository,
        OptionThumbnailRepositoryInterface $optionThumbnailRepository,
        OptionDetailImageRepositoryInterface $optionDetailImageRepository,
        CreatorViewRepository $creatorViewRepository
    ) {
        $this->componentImageRepository = $componentImageRepository;
        $this->expectedMediaPath = $expectedMediaPath;
        $this->layerImageRepository = $layerImageRepository;
        $this->optionThumbnailRepository = $optionThumbnailRepository;
        $this->optionDetailImageRepository = $optionDetailImageRepository;
        $this->creatorViewRepository = $creatorViewRepository;
    }

    /**
     * @param Item $item
     *
     * @return array|MediaInfoComponent[]
     */
    public function getComponentMediaInfo(Item $item): array
    {
        $views = $this->creatorViewRepository->findByItemId((int)$item->getId());

        $itemOptionClassifications = $item->getItemOptionclassification();
        $componentEntities = $itemOptionClassifications->map(static function (ItemOptionclassification $itemOptionClassification) {
            return $itemOptionClassification->getOptionclassification();
        })->toArray();
        $componentThumbnailsMap = $this->generateImageMap($this->componentImageRepository->findThumbnails($componentEntities));

        $componentMediaInfoStructures = [];
        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($itemOptionClassifications as $itemOptionClassification) {
            $componentEntity = $itemOptionClassification->getOptionclassification();
            if (array_key_exists($componentEntity->getIdentifier(), $componentThumbnailsMap)) {
                $componentThumbnail = MediaInfoImage::fromPath($componentThumbnailsMap[$componentEntity->getIdentifier()]);
            } else {
                $componentThumbnail = MediaInfoImage::forMissing($this->expectedMediaPath->forComponentThumbnail($componentEntity));
            }

            $componentMediaInfoStructures[] = MediaInfoComponent::from($componentEntity, $componentThumbnail, $this->getOptionsForComponent($itemOptionClassification, $views));
        }

        return $componentMediaInfoStructures;
    }

    /**
     * @param ItemOptionclassification $itemOptionClassification
     * @param array|CreatorView[] $views
     *
     * @return array|MediaInfoOption[]
     */
    private function getOptionsForComponent(ItemOptionclassification $itemOptionClassification, array $views): array
    {
        $item = $itemOptionClassification->getItem();
        $component = $itemOptionClassification->getOptionclassification();
        $componentOptionEntities = $itemOptionClassification->getItemOptionclassificationOption()->map(
            static function (ItemOptionclassificationOption $itemOption) {
                return $itemOption->getOption();
            })->toArray();

        $componentLayerImagesMap = $this->generateLayerImageMap($this->layerImageRepository->findLayerImagesForComponent($item, $component));
        $optionThumbnailImageMap = $this->generateImageMap($this->optionThumbnailRepository->findThumbnails($componentOptionEntities));
        $optionDetailImageMap = $this->generateImageMap($this->optionDetailImageRepository->findDetailImages($componentOptionEntities));

        $optionInfos = [];
        foreach ($componentOptionEntities as $optionEntity) {
            $optionIdentifier = $optionEntity->getIdentifier();
            $optionThumbnail = array_key_exists($optionIdentifier, $optionThumbnailImageMap) ?
                MediaInfoImage::fromPath($optionThumbnailImageMap[$optionIdentifier]) : MediaInfoImage::forMissing($this->expectedMediaPath->forOptionThumbnail($optionEntity));

            $optionDetailImage = array_key_exists($optionIdentifier, $optionDetailImageMap) ?
                MediaInfoImage::fromPath($optionDetailImageMap[$optionIdentifier]) : MediaInfoImage::forMissing($this->expectedMediaPath->forOptionDetailImage($optionEntity));

            $optionInfos[] = MediaInfoOption::from(
                $optionEntity,
                MediaInfoOptionImages::from($optionThumbnail, $optionDetailImage, $this->retrieveLayerImages($views, $item, $component, $optionEntity, $componentLayerImagesMap))
            );
        }

        return $optionInfos;
    }

    /**
     * @param array|LayerImage[] $layerImages
     *
     * @return array
     */
    private function generateLayerImageMap(array $layerImages): array
    {
        $map = [];
        foreach ($layerImages as $layerImage) {
            $map[$layerImage->getOptionIdentifier()][$layerImage->getViewIdentifier()] = $layerImage->getUrl();
        }

        return $map;
    }

    /**
     * @param CreatorView[] $views
     * @param Item $item
     * @param Optionclassification $component
     * @param Option $option
     * @param array $componentLayerImagesMap
     *
     * @return array
     */
    private function retrieveLayerImages(array $views, Item $item, Optionclassification $component, Option $option, array $componentLayerImagesMap): array
    {
        $layerImages = [];
        $optionIdentifier = $option->getIdentifier();

        foreach ($views as $view) {
            $viewIdentifier = $view->getIdentifier();

            if (array_key_exists($optionIdentifier, $componentLayerImagesMap) && array_key_exists($viewIdentifier, $componentLayerImagesMap[$optionIdentifier])) {
                $viewMediaInfoImage = MediaInfoImage::fromPath($componentLayerImagesMap[$optionIdentifier][$viewIdentifier]);
            } else {
                $viewMediaInfoImage = MediaInfoImage::forMissing($this->expectedMediaPath->forLayerImage($item, $component, $option, $view));
            }

            $layerImages[$viewIdentifier] = $viewMediaInfoImage;
        }

        return $layerImages;
    }

    /**
     * @param array $images
     *
     * @return array|string[]
     */
    private function generateImageMap(array $images): array
    {
        $map = [];
        foreach ($images as $image) {
            $map += $image->toMap();
        }

        return $map;
    }
}
