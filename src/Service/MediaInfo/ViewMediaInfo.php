<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\MediaInfo;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewFileRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\DTO\CreatorView as CreatorViewDTO;
use Redhotmagma\ConfiguratorApiBundle\Structure\ViewMediaInfo as ViewMediaInfoStructure;

/**
 * @internal
 */
final class ViewMediaInfo
{
    /**
     * @var ExpectedMediaPathInterface
     */
    private $expectedMediaPath;

    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var CreatorViewFileRepositoryInterface
     */
    private $creatorViewFileRepository;

    /**
     * @var CreatorViewThumbnailRepositoryInterface
     */
    private $viewThumbnailRepository;

    /**
     * @var CreatorViewDelete
     */
    private $creatorViewDelete;

    /**
     * ViewFiles constructor.
     *
     * @param ExpectedMediaPathInterface $expectedMediaPath
     * @param CreatorViewRepository $creatorViewRepository
     * @param CreatorViewThumbnailRepositoryInterface $viewThumbnailRepository
     * @param CreatorViewFileRepositoryInterface $creatorViewFileRepository
     * @param CreatorViewDelete $creatorViewDelete
     */
    public function __construct(ExpectedMediaPathInterface $expectedMediaPath, CreatorViewRepository $creatorViewRepository, CreatorViewThumbnailRepositoryInterface $viewThumbnailRepository, CreatorViewFileRepositoryInterface $creatorViewFileRepository, CreatorViewDelete $creatorViewDelete)
    {
        $this->expectedMediaPath = $expectedMediaPath;
        $this->creatorViewRepository = $creatorViewRepository;
        $this->viewThumbnailRepository = $viewThumbnailRepository;
        $this->creatorViewFileRepository = $creatorViewFileRepository;
        $this->creatorViewDelete = $creatorViewDelete;
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    public function getViewMediaInfo(Item $item): array
    {
        $viewMediaInfos = [];
        $this->syncCreatorViews($item);
        $views = $this->creatorViewRepository->findByItemId((int)$item->getId());
        $viewThumbnailsMap = $this->generateViewThumbnailsMap($item);
        foreach ($views as $view) {
            if (array_key_exists($view->getIdentifier(), $viewThumbnailsMap)) {
                $viewMediaInfos[] = ViewMediaInfoStructure::fromViewThumbnail($view, $viewThumbnailsMap[$view->getIdentifier()]);
            } else {
                $viewMediaInfos[] = ViewMediaInfoStructure::forMissing($view, $this->expectedMediaPath->forViewThumbnail($view));
            }
        }

        return $viewMediaInfos;
    }

    /**
     * @param Item $item
     */
    private function syncCreatorViews(Item $item): void
    {
        $creatorViewEntities = $this->creatorViewRepository->findByItemId((int)$item->getId());
        $creatorViewsFromDirectory = $this->creatorViewFileRepository->findCreatorViews($item);

        $this->createMissingCreatorViews($creatorViewEntities, $creatorViewsFromDirectory, $item);
        $this->deleteNotExistingCreatorViews($creatorViewEntities, $creatorViewsFromDirectory);
    }

    /**
     * @param array|CreatorView[] $creatorViewEntities
     * @param array|CreatorViewDTO[] $creatorViewsFromDirectory
     * @param Item $item
     */
    private function createMissingCreatorViews(array $creatorViewEntities, array $creatorViewsFromDirectory, Item $item): void
    {
        $identifiers = array_map(function ($creatorViewEntity) { return $creatorViewEntity->getIdentifier(); }, $creatorViewEntities);
        foreach ($creatorViewsFromDirectory as $creatorViewFromDirectory) {
            if (!in_array($creatorViewFromDirectory->getIdentifier(), $identifiers, true)) {
                $this->creatorViewRepository->save(CreatorView::from(null, $creatorViewFromDirectory->getIdentifier(), $item, $creatorViewFromDirectory->getSequenceNumber()));
            }
        }
    }

    /**
     * @param array|CreatorView[] $creatorViewEntities
     * @param array|CreatorViewDTO[] $creatorViewsFromDirectory
     */
    private function deleteNotExistingCreatorViews(array $creatorViewEntities, array $creatorViewsFromDirectory): void
    {
        $identifiers = array_map(function ($creatorViewFromDirectory) { return $creatorViewFromDirectory->getIdentifier(); }, $creatorViewsFromDirectory);
        foreach ($creatorViewEntities as $creatorViewEntity) {
            if (!in_array($creatorViewEntity->getIdentifier(), $identifiers, true)) {
                $this->creatorViewDelete->deleteEntity($creatorViewEntity);
            }
        }
    }

    /**
     * @param Item $item
     *
     * @return array|CreatorViewThumbnail[]
     */
    private function generateViewThumbnailsMap(Item $item): array
    {
        $viewThumbnails = $this->viewThumbnailRepository->findThumbnails($item);
        $viewThumbnailsMap = [];
        foreach ($viewThumbnails as $viewThumbnail) {
            $viewThumbnailsMap += $viewThumbnail->toCreatorViewIdentifierMap();
        }

        return $viewThumbnailsMap;
    }
}
