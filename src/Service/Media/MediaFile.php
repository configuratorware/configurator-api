<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Media;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\MediaItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\MediaArguments;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @internal
 */
class MediaFile
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var string
     */
    private $basePath;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var string
     */
    private $pathPattern;

    /**
     * @var string
     */
    private $memoryLimit = '1G';

    /**
     * @var string
     */
    private $maxExecutionTime = '300s';

    /**
     * MediaFile constructor.
     *
     * @param RequestStack $requestStack
     * @param string       $basePath
     * @param string       $cacheDir
     * @param string       $pathPattern
     */
    public function __construct(RequestStack $requestStack, string $basePath, string $cacheDir, string $pathPattern)
    {
        $this->requestStack = $requestStack;
        $this->basePath = $basePath;
        $this->cacheDir = $cacheDir;
        $this->pathPattern = $pathPattern;
    }

    /**
     * @param MediaArguments $mediaArguments
     *
     * @return mixed
     */
    public function get(MediaArguments $mediaArguments)
    {
        $request = $this->requestStack->getCurrentRequest();

        try {
            $requestETag = $request->headers->get('If-None-Match');

            $mediaItem = $this->getMediaItem($mediaArguments->path, $mediaArguments->scale / 100);

            $response = new Response();
            $response->headers->set('Content-Type', $mediaItem->contentType);
            $response->headers->set('ETag', $mediaItem->eTag);
            $response->headers->set('Access-Control-Allow-Origin', '*');

            if ($this->hasETagMatch($mediaItem->eTag, $requestETag)) {
                $response->setStatusCode(Response::HTTP_NOT_MODIFIED);
            } else {
                $response->setStatusCode(Response::HTTP_OK);
                $response->setContent(stream_get_contents($mediaItem->fileStream));
                fclose($mediaItem->fileStream);
            }

            return $response;
        } catch (FileNotFoundException $exception) {
            throw new NotFoundHttpException('The file \'' . $mediaArguments->path . '\' was not found on the server.');
        }
    }

    /**
     * @param string $fileETag
     * @param        $reqTags
     *
     * @return bool
     */
    private function hasETagMatch(string $fileETag, $reqTags): bool
    {
        $reqTags = is_array($reqTags) ? $reqTags : [$reqTags];

        return false !== array_search($fileETag, $reqTags, true);
    }

    /**
     * @param string $relativePathname
     * @param float  $scale
     *
     * @return MediaItem
     */
    private function getMediaItem(string $relativePathname, float $scale): MediaItem
    {
        $this->assertValidPath($relativePathname);

        $filePath = $this->basePath . '/' . $relativePathname;

        // Make sure that path names do not break out of the base dir when resolved. Usually, the request won't get that
        // far (a 404 is returned), but at least with the test client we arrive here. This section is absolutely critical
        // as it may compromise the whole system.
        if (!$this->isPathInsideBaseDir($filePath)) {
            throw new AccessDeniedHttpException();
        }

        try {
            ini_set('memory_limit', $this->memoryLimit);
            ini_set('max_execution_time', $this->maxExecutionTime);

            $file = new File($filePath, true);

            $mediaItem = new MediaItem();
            $mediaItem->relativePathname = $relativePathname;
            $mediaItem->contentType = $file->getMimeType();
            $mediaItem->fileStream = $this->getMediaFileScaled($filePath, $mediaItem->contentType, $scale);
            $mediaItem->eTag = $this->getFileETag($file);

            return $mediaItem;
        } catch (\Symfony\Component\Filesystem\Exception\FileNotFoundException $fileNotFoundException) {
            throw new FileNotFoundException($relativePathname);
        }
    }

    /**
     * @param string $path
     */
    private function assertValidPath(string $path)
    {
        if (!preg_match($this->pathPattern, $path)) {
            throw new AccessDeniedHttpException();
        }
    }

    /**
     * @param string $path
     *
     * @return bool
     */
    private function isPathInsideBaseDir(string $path)
    {
        $basePath = new File($this->basePath, false);
        $file = new File($path, false);
        $realPath = $file->getRealPath();
        $basePathReal = $basePath->getRealPath();

        if (false !== $realPath && false !== $basePath && 0 !== strpos($realPath, $basePathReal)) {
            return false;
        }

        return true;
    }

    /**
     * @param File $file
     *
     * @return string
     */
    private function getFileETag(File $file): string
    {
        $fileInfo = $file->getFileInfo();
        $mtime = $fileInfo->getMTime();
        $size = $fileInfo->getSize();

        $etag = sprintf('"%x-%x"', $size, (float)str_pad($mtime, 16, '0'));

        return $etag;
    }

    /**
     * @param string $filePath
     * @param string $contentType
     * @param float  $scale
     *
     * @return bool|resource
     */
    private function getMediaFileScaled(string $filePath, string $contentType, float $scale = 1)
    {
        // Early out if no scaling is needed
        if (1.0 === $scale) {
            return fopen($filePath, 'r');
        }

        $originalFile = new File($filePath, false);
        $relPath = str_replace($this->basePath . '/', '', $filePath);
        $cacheItem = $this->lookupInCache($relPath, $scale, $originalFile->getMTime());

        if ($cacheItem) {
            return $cacheItem;
        }

        $isImageFile = false;

        // If the file is an image file, scale it and save to cache, then return an open file handle to the cached file
        $filenameInCache = $this->getCacheFileName($relPath, $scale);

        // Make sure the path to the file in cache exists
        $fs = new Filesystem();
        $fs->mkdir(dirname($filenameInCache));

        $outputFile = null;
        switch ($contentType) {
            case 'image/png':
                $isImageFile = true;
                $resource = imagecreatefrompng($filePath);
                if ($resource) {
                    $scaledImage = $this->scaleImage($resource, $scale);
                    if ($scaledImage) {
                        imagepng($scaledImage, $filenameInCache);
                    }
                }

                break;
            case 'image/jpeg':
                $isImageFile = true;
                $resource = imagecreatefromjpeg($filePath);
                if ($resource) {
                    $scaledImage = $this->scaleImage($resource, $scale);
                    if ($scaledImage) {
                        imagejpeg($scaledImage, $filenameInCache, 90);
                    }
                }

                break;
            case 'image/gif':
                $isImageFile = true;
                $resource = imagecreatefromgif($filePath);
                if ($resource) {
                    $scaledImage = $this->scaleImage($resource, $scale);
                    if ($scaledImage) {
                        imagegif($scaledImage, $filenameInCache);
                    }
                }

                break;
            default:
                $outputFile = fopen($filePath, 'r');
        }

        if (!empty($resource)) {
            imagedestroy($resource);
        }
        if (!empty($scaledImage)) {
            imagedestroy($scaledImage);
        }

        if ($isImageFile) {
            $outputFile = fopen($filenameInCache, 'r');
        }

        return $outputFile;
    }

    private function scaleImage(\GdImage $resource, float $scale): \GdImage|false
    {
        $orgWidth = imagesx($resource);
        $orgHeight = imagesy($resource);
        $width = round($orgWidth * $scale);
        $height = round($orgHeight * $scale);

        $outputImage = imagecreatetruecolor($width, $height);
        $transparent = imagecolorallocatealpha($outputImage, 255, 255, 255, 127);
        imagefill($outputImage, 0, 0, $transparent);
        imagecolortransparent($outputImage, $transparent);
        imagesetinterpolation($outputImage, IMG_SINC);
        imagecopyresampled($outputImage, $resource, 0, 0, 0, 0, $width, $height, $orgWidth, $orgHeight);
        imagesavealpha($outputImage, true);

        return $outputImage;
    }

    /**
     * @param string $relPath
     * @param float  $scale
     * @param        $minMtime
     *
     * @return bool|resource
     */
    private function lookupInCache(string $relPath, float $scale, $minMtime)
    {
        $path = $this->getCacheFileName($relPath, $scale);
        $fs = new Filesystem();
        if ($fs->exists($path)) {
            $file = new File($path);
            if ($file->getMTime() > $minMtime) {
                return fopen($file->getPathname(), 'r');
            }
        }

        return false;
    }

    /**
     * @param string $relPath
     * @param float  $scale
     *
     * @return string
     */
    private function getCacheFileName(string $relPath, float $scale): string
    {
        $filename = $this->cacheDir . '/' . $this->makeCacheKey($relPath, $scale);

        return $filename;
    }

    /**
     * @param string $relPath
     * @param float  $scale
     *
     * @return string
     */
    private function makeCacheKey(string $relPath, float $scale): string
    {
        $scale = round($scale * 100);
        $cacheKey = dirname($relPath) . '/' . $scale . '_' . basename($relPath);

        return $cacheKey;
    }
}
