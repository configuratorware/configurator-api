<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Media;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @internal
 *
 * @deprecated use interfaces in Settings\Paths instead
 */
class MediaHelper
{
    /**
     * @var string
     */
    private $basePath;

    /**
     * @var string
     */
    private $baseUrl;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * MediaHelper constructor.
     *
     * @param string $basePath
     * @param string $baseUrl
     * @param RequestStack $requestStack
     */
    public function __construct(string $basePath, string $baseUrl, RequestStack $requestStack)
    {
        $this->basePath = rtrim($basePath, '/');
        $this->baseUrl = $baseUrl;
        $this->requestStack = $requestStack;
    }

    /**
     * @return string
     */
    public function getBasePath(): string
    {
        return $this->basePath;
    }

    /**
     * @return string
     */
    public function getBaseUrl(): string
    {
        return $this->baseUrl;
    }

    /**
     * @return string
     */
    public function getImageBasePath(): string
    {
        return $this->getBasePath() . DIRECTORY_SEPARATOR . 'images';
    }

    /**
     * @return string
     */
    public function getImageBaseUrl(): string
    {
        $mediaBaseUrl = $this->getBaseUrl();

        if (!empty($mediaBaseUrl)) {
            return $mediaBaseUrl;
        }

        $request = $this->requestStack->getCurrentRequest();
        if (null === $request) {
            return $mediaBaseUrl;
        }

        return '//' . $request->getHttpHost() . $request->getBasePath();
    }
}
