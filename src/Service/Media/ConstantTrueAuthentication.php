<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Media;

use Symfony\Component\HttpFoundation\Request;

/**
 * @internal
 */
class ConstantTrueAuthentication implements MediaAuthenticationInterface
{
    /**
     * {@inheritdoc}
     *
     * @author Daniel Klier <klier@redhotmagma.de>
     */
    public function isAuthenticated(Request $request): bool
    {
        return true;
    }
}
