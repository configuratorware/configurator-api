<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Media;

use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\MediaArguments;

class MediaApi
{
    /**
     * @var MediaFile
     */
    private $mediaFile;

    /**
     * MediaApi constructor.
     *
     * @param MediaFile $mediaFile
     */
    public function __construct(MediaFile $mediaFile)
    {
        $this->mediaFile = $mediaFile;
    }

    /**
     * @param MediaArguments $mediaArguments
     *
     * @return mixed
     */
    public function getMedia(MediaArguments $mediaArguments)
    {
        $response = $this->mediaFile->get($mediaArguments);

        return $response;
    }
}
