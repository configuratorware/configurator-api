<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Media;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface MediaAuhthenticationInterface.
 *
 * @author Daniel Klier <klier@redhotmagma.de>
 */
interface MediaAuthenticationInterface
{
    /**
     * @author Daniel Klier <klier@redhotmagma.de>
     *
     * @param Request $request
     *
     * @return bool
     */
    public function isAuthenticated(Request $request): bool;
}
