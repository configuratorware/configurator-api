<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\GroupEntry;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry;

/**
 * @internal
 */
class GroupEntryEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * GroupEntityFromStructureConverter constructor.
     *
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(EntityFromStructureConverter $entityFromStructureConverter)
    {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param GroupEntry $structure
     * @param Itemgroupentry $entity
     * @param string $entityClassName
     *
     * @return Itemgroupentry
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = Itemgroupentry::class
    ): Itemgroupentry {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var Itemgroupentry $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($localStructure, $entity,
            'translations',
            'Itemgroupentrytranslation');
        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations($localStructure, $entity,
            'translations',
            'Itemgroupentrytranslation');

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = GroupEntry::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
