<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\GroupEntry;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupTranslation;

/**
 * @internal
 */
class GroupEntryStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Itemgroupentry $entity
     * @param string $structureClassName
     *
     * @return GroupEntry
     */
    public function convertOne($entity, $structureClassName = GroupEntry::class)
    {
        /** @var GroupEntry $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $structure = $this->addTranslationsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = GroupEntry::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param GroupEntry $structure
     * @param Itemgroupentry $entity
     *
     * @return GroupEntry
     */
    protected function addTranslationsToStructure($structure, $entity)
    {
        $translations = [];

        $groupEntryTranslations = $entity->getItemgroupentrytranslation();

        if (!empty($groupEntryTranslations)) {
            foreach ($groupEntryTranslations as $groupEntryTranslation) {
                $groupEntryTranslationStructure = $this->structureFromEntityConverter->convertOne($groupEntryTranslation,
                    GroupTranslation::class);

                if (!empty($groupEntryTranslationStructure)) {
                    $groupEntryTranslationStructure->language = $groupEntryTranslation->getLanguage()
                        ->getIso();
                    $translations[] = $groupEntryTranslationStructure;
                }
            }
        }

        $structure->translations = $translations;

        return $structure;
    }
}
