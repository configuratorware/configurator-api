<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\GroupEntry;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

class GroupEntryApi
{
    /**
     * @var ItemgroupentryRepository
     */
    private $groupEntryRepository;

    /**
     * @var GroupEntryStructureFromEntityConverter
     */
    private $groupEntryStructureFromEntityConverter;

    /**
     * @var GroupEntrySave
     */
    private $groupEntrySave;

    /**
     * @var GroupEntryDelete
     */
    private $groupEntryDelete;

    /**
     * GroupEntryApi constructor.
     *
     * @param ItemgroupentryRepository $groupEntryRepository
     * @param GroupEntryStructureFromEntityConverter $groupEntryStructureFromEntityConverter
     * @param GroupEntrySave $groupEntrySave
     * @param GroupEntryDelete $groupEntryDelete
     */
    public function __construct(
        ItemgroupentryRepository $groupEntryRepository,
        GroupEntryStructureFromEntityConverter $groupEntryStructureFromEntityConverter,
        GroupEntrySave $groupEntrySave,
        GroupEntryDelete $groupEntryDelete
    ) {
        $this->groupEntryRepository = $groupEntryRepository;
        $this->groupEntryStructureFromEntityConverter = $groupEntryStructureFromEntityConverter;
        $this->groupEntrySave = $groupEntrySave;
        $this->groupEntryDelete = $groupEntryDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->groupEntryRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->groupEntryStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->groupEntryRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return GroupEntry
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): GroupEntry
    {
        /** @var Itemgroupentry $entity */
        $entity = $this->groupEntryRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->groupEntryStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param GroupEntry $groupEntry
     *
     * @return GroupEntry
     */
    public function save(GroupEntry $groupEntry): GroupEntry
    {
        $structure = $this->groupEntrySave->save($groupEntry);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->groupEntryDelete->delete($id);
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments)
    {
        /** @var SequenceNumberData $sequenceNumberData */
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            /** @var Itemgroupentry $entity */
            $entity = $this->groupEntryRepository->findOneBy(['id' => $sequenceNumberData->id]);

            if (is_object($entity)) {
                $entity->setSequencenumber($sequenceNumberData->sequencenumber);
                $this->groupEntryRepository->save($entity, false);
            }
        }

        $this->groupEntryRepository->flush();
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier', 'itemgroupentrytranslation.translation'];
    }
}
