<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\GroupEntry;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;

/**
 * @internal
 */
class GroupEntryDelete
{
    /**
     * @var ItemgroupentryRepository
     */
    private $groupEntryRepository;

    /**
     * GroupEntryDelete constructor.
     *
     * @param ItemgroupentryRepository $groupEntryRepository
     */
    public function __construct(ItemgroupentryRepository $groupEntryRepository)
    {
        $this->groupEntryRepository = $groupEntryRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Itemgroupentry $entity */
            $entity = $this->groupEntryRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            $this->deleteRelation($entity, 'Itemgroupentrytranslation');
            $this->deleteRelation($entity, 'ItemItemgroupentry');
            $this->deleteRelation($entity, 'OptionGroupEntry');

            $this->groupEntryRepository->delete($entity);
        }
    }

    /**
     * @param Itemgroupentry $entity
     * @param $relation
     */
    private function deleteRelation(Itemgroupentry $entity, string $relation)
    {
        $relationGetter = 'get' . $relation;

        if (!empty($entity->$relationGetter()->toArray())) {
            foreach ($entity->$relationGetter() as $relationEntity) {
                $this->groupEntryRepository->delete($relationEntity);
            }
        }
    }
}
