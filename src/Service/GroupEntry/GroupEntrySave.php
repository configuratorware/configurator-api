<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\GroupEntry;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry;

/**
 * @internal
 */
class GroupEntrySave
{
    /**
     * @var ItemgroupentryRepository
     */
    private $groupEntryRepository;

    /**
     * @var GroupEntryEntityFromStructureConverter
     */
    private $groupEntryEntityFromStructureConverter;

    /**
     * @var GroupEntryStructureFromEntityConverter
     */
    private $groupEntryStructureFromEntityConverter;

    /**
     * GroupEntrySave constructor.
     *
     * @param ItemgroupentryRepository $groupEntryRepository
     * @param GroupEntryEntityFromStructureConverter $groupEntryEntityFromStructureConverter
     * @param GroupEntryStructureFromEntityConverter $groupEntryStructureFromEntityConverter
     */
    public function __construct(
        ItemgroupentryRepository $groupEntryRepository,
        GroupEntryEntityFromStructureConverter $groupEntryEntityFromStructureConverter,
        GroupEntryStructureFromEntityConverter $groupEntryStructureFromEntityConverter
    ) {
        $this->groupEntryRepository = $groupEntryRepository;
        $this->groupEntryEntityFromStructureConverter = $groupEntryEntityFromStructureConverter;
        $this->groupEntryStructureFromEntityConverter = $groupEntryStructureFromEntityConverter;
    }

    /**
     * @param GroupEntry $groupEntry
     *
     * @return GroupEntry
     */
    public function save(GroupEntry $groupEntry)
    {
        $entity = null;
        if (isset($groupEntry->id) && $groupEntry->id > 0) {
            $entity = $this->groupEntryRepository->findOneBy(['id' => $groupEntry->id]);
        }

        $entity = $this->groupEntryEntityFromStructureConverter->convertOne($groupEntry, $entity);
        $entity = $this->groupEntryRepository->save($entity);

        /** @var Itemgroupentry $entity */
        $entity = $this->groupEntryRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->groupEntryStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
