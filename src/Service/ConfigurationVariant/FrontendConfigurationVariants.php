<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemPool;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemAvailability;

/**
 * @internal
 */
class FrontendConfigurationVariants
{
    /**
     * @var FrontendItemStructureFromEntityConverter
     */
    private $frontendItemStructureFromEntityConverter;

    /**
     * @var ItemAvailability
     */
    private $itemAvailability;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @param FrontendItemStructureFromEntityConverter $frontendItemStructureFromEntityConverter
     * @param ItemAvailability $itemAvailability
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        FrontendItemStructureFromEntityConverter $frontendItemStructureFromEntityConverter,
        ItemAvailability $itemAvailability,
        ItemRepository $itemRepository
    ) {
        $this->frontendItemStructureFromEntityConverter = $frontendItemStructureFromEntityConverter;
        $this->itemAvailability = $itemAvailability;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    public function getConfigurationVariantsForItem($itemIdentifier): array
    {
        $item = $this->itemRepository->findOneBy(['identifier' => $itemIdentifier]);
        if (null === $item) {
            throw new NotFoundException();
        }

        $isAvailable = $this->itemAvailability->checkAvailability($item);
        if (!$isAvailable) {
            throw new NotFoundException();
        }

        /** @var ItemItemPool $itemItemPool */
        $itemItemPool = $item->getItemItemPool()->first();
        if (false === $itemItemPool) {
            return [];
        }

        $siblings = [];
        foreach ($itemItemPool->getItemPool()->getItemItemPool() as $siblingRelation) {
            $siblings[] = $siblingRelation->getItem();
        }

        return $this->frontendItemStructureFromEntityConverter->convertMany($siblings);
    }
}
