<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemPool;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemPool;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemPoolRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationVariant;

/**
 * @internal
 */
class ConfigurationVariantSave
{
    /**
     * @var ItemPoolRepository
     */
    private $itemPoolRepository;

    /**
     * @var ConfigurationVariantEntityFromStructureConverter
     */
    private $configurationVariantEntityFromStructureConverter;

    /**
     * @var ConfigurationVariantStructureFromEntityConverter
     */
    private $configurationVariantStructureFromEntityConverter;

    /**
     * ConfigurationVariantSave constructor.
     *
     * @param ItemPoolRepository $itemPoolRepository
     * @param ConfigurationVariantEntityFromStructureConverter $configurationVariantEntityFromStructureConverter
     * @param ConfigurationVariantStructureFromEntityConverter $configurationVariantStructureFromEntityConverter
     */
    public function __construct(
        ItemPoolRepository $itemPoolRepository,
        ConfigurationVariantEntityFromStructureConverter $configurationVariantEntityFromStructureConverter,
        ConfigurationVariantStructureFromEntityConverter $configurationVariantStructureFromEntityConverter
    ) {
        $this->itemPoolRepository = $itemPoolRepository;
        $this->configurationVariantEntityFromStructureConverter = $configurationVariantEntityFromStructureConverter;
        $this->configurationVariantStructureFromEntityConverter = $configurationVariantStructureFromEntityConverter;
    }

    /**
     * @param ConfigurationVariant $configurationVariant
     *
     * @return ConfigurationVariant
     */
    public function save(ConfigurationVariant $configurationVariant)
    {
        $entity = null;
        if (isset($configurationVariant->id) && $configurationVariant->id > 0) {
            $entity = $this->itemPoolRepository->findOneBy(['id' => $configurationVariant->id]);
        }
        $entity = $this->configurationVariantEntityFromStructureConverter->convertOne($configurationVariant, $entity,
            ItemPool::class);

        $entity = $this->itemPoolRepository->save($entity);

        /** @var ItemPool $entity */
        $entity = $this->itemPoolRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->configurationVariantStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param Item $item
     * @param ItemPool $itemPool
     */
    protected function deleteOtherItemGroupRelations($item, $itemPool)
    {
        /** @var ItemItemPool $itemItemPool */
        foreach ($item->getItemItemgroup() as $itemItemPool) {
            if ($itemItemPool->getItemPool()->getId() !== $itemPool->getId()) {
                $itemItemPool->setDateDeleted(new \DateTime());
                $this->itemPoolRepository->save($itemItemPool);
            }
        }
    }
}
