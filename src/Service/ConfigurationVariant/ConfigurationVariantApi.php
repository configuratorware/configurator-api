<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationVariant\GetFrontendConfigurationVariantsEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemPoolRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationVariant;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ConfigurationVariantApi
{
    /**
     * @var ConfigurationVariantDelete
     */
    private $configurationVariantDelete;

    /**
     * @var ItemPoolRepository
     */
    private $itemPoolRepository;

    /**
     * @var ConfigurationVariantSave
     */
    private $configurationVariantSave;

    /**
     * @var ConfigurationVariantStructureFromEntityConverter
     */
    private $configurationVariantStructureFromEntityConverter;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ItemSearch
     */
    private $itemSearch;

    /**
     * ConfigurationVariantApi constructor.
     *
     * @param ConfigurationVariantDelete $configurationVariantDelete
     * @param ItemPoolRepository $itemPoolRepository
     * @param ConfigurationVariantSave $configurationVariantSave
     * @param ConfigurationVariantStructureFromEntityConverter $configurationVariantStructureFromEntityConverter
     * @param EventDispatcherInterface $eventDispatcher
     * @param ItemSearch $itemSearch
     */
    public function __construct(
        ConfigurationVariantDelete $configurationVariantDelete,
        ItemPoolRepository $itemPoolRepository,
        ConfigurationVariantSave $configurationVariantSave,
        ConfigurationVariantStructureFromEntityConverter $configurationVariantStructureFromEntityConverter,
        EventDispatcherInterface $eventDispatcher,
        ItemSearch $itemSearch
    ) {
        $this->configurationVariantDelete = $configurationVariantDelete;
        $this->itemPoolRepository = $itemPoolRepository;
        $this->configurationVariantSave = $configurationVariantSave;
        $this->configurationVariantStructureFromEntityConverter = $configurationVariantStructureFromEntityConverter;
        $this->eventDispatcher = $eventDispatcher;
        $this->itemSearch = $itemSearch;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->itemPoolRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->configurationVariantStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->itemPoolRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return ConfigurationVariant
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): ConfigurationVariant
    {
        /** @var Itemgroup $entity */
        $entity = $this->itemPoolRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->configurationVariantStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param ConfigurationVariant $configurationVariant
     *
     * @return ConfigurationVariant
     */
    public function save(ConfigurationVariant $configurationVariant)
    {
        $structure = $this->configurationVariantSave->save($configurationVariant);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->configurationVariantDelete->delete($id);
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function findItems(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = $this->itemSearch->findItems($arguments);

        return $paginationResult;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    public function getFrontendConfigurationVariants($itemIdentifier)
    {
        $event = new GetFrontendConfigurationVariantsEvent($itemIdentifier);
        $this->eventDispatcher->dispatch($event, GetFrontendConfigurationVariantsEvent::NAME);
        $configurationVariants = $event->getConfigurationVariants();

        return $configurationVariants;
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier'];
    }
}
