<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemPool;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationVariant;

/**
 * @internal
 */
class ConfigurationVariantEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * ConfigurationVariantEntityFromStructureConverter constructor.
     *
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        ItemRepository $itemRepository
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param ConfigurationVariant $structure
     * @param Itemgroup $entity
     * @param string $entityClassName
     *
     * @return Itemgroup
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = Itemgroup::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var Itemgroup $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $this->entityFromStructureConverter->setManyToManyRelationsDeleted($localStructure, $entity, 'ItemItemPool',
            'items', 'Item');

        $this->entityFromStructureConverter->addNewManyToManyRelations($localStructure, $entity,
            ItemItemPool::class, 'items', 'Item',
            $this->itemRepository);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Itemgroup::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
