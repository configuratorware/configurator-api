<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemPoolRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\AdminAreaItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

/**
 * @internal
 */
class ItemSearch
{
    /**
     * @var AdminAreaItemStructureFromEntityConverter
     */
    private $adminAreaItemStructureFromEntityConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemPoolRepository
     */
    private $itemPoolRepository;

    /**
     * ItemSearch constructor.
     *
     * @param AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter
     * @param ItemRepository $itemRepository
     * @param ItemPoolRepository $itemPoolRepository
     */
    public function __construct(
        AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter,
        ItemRepository $itemRepository,
        ItemPoolRepository $itemPoolRepository
    ) {
        $this->adminAreaItemStructureFromEntityConverter = $adminAreaItemStructureFromEntityConverter;
        $this->itemRepository = $itemRepository;
        $this->itemPoolRepository = $itemPoolRepository;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function findItems(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->itemRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->adminAreaItemStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->itemRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        // mark already related items
        $paginationResult = $this->markRelatedItems($paginationResult);

        return $paginationResult;
    }

    /**
     * @param PaginationResult $paginationResult
     *
     * @return PaginationResult
     */
    protected function markRelatedItems(PaginationResult $paginationResult): PaginationResult
    {
        $itemIds = [];
        foreach ($paginationResult->data as $item) {
            $itemIds[] = $item->id;
        }

        $itemPoolMapping = $this->itemPoolRepository->getRelatedConfigurationVariantItemGroupsForItemIds($itemIds);

        /** @var Item $item */
        foreach ($paginationResult->data as $item) {
            if (isset($itemPoolMapping[$item->id])) {
                $configurationVariantInfo = new \stdClass();
                $configurationVariantInfo->item_group_identifier = $itemPoolMapping[$item->id]['item_pool_identifier'];

                if (!isset($item->additional_data)) {
                    $item->additional_data = new \stdClass();
                }

                $item->additional_data->configuration_variant_info = $configurationVariantInfo;
            }
        }

        return $paginationResult;
    }

    /**
     * @return array
     */
    protected function getSearchableFields()
    {
        return ['identifier', 'itemtext.title', 'itemtext.abstract', 'itemtext.description'];
    }
}
