<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemPool;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\AdminAreaItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationVariant;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;

/**
 * @internal
 */
class ConfigurationVariantStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var AdminAreaItemStructureFromEntityConverter
     */
    private $itemStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * ConfigurationVariantStructureFromEntityConverter constructor.
     *
     * @param AdminAreaItemStructureFromEntityConverter $itemStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        AdminAreaItemStructureFromEntityConverter $itemStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->itemStructureFromEntityConverter = $itemStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param ItemPool $entity
     * @param string $structureClassName
     *
     * @return ConfigurationVariant
     */
    public function convertOne($entity, $structureClassName = ConfigurationVariant::class)
    {
        /** @var ConfigurationVariant $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $items = [];
        foreach ($entity->getItemItemPool() as $itemItemPool) {
            $items[] = $this->itemStructureFromEntityConverter->convertOne($itemItemPool->getItem(), Item::class);
        }

        $structure->items = $items;

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ConfigurationVariant::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
