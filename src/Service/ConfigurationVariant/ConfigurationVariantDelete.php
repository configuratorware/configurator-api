<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationVariant;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemPool;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemPoolRepository;

/**
 * @internal
 */
class ConfigurationVariantDelete
{
    /**
     * @var ItemPoolRepository
     */
    private $itemPoolRepository;

    /**
     * ConfigurationVariantDelete constructor.
     *
     * @param ItemPoolRepository $itemPoolRepository
     */
    public function __construct(ItemPoolRepository $itemPoolRepository)
    {
        $this->itemPoolRepository = $itemPoolRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var ItemPool $entity */
            $entity = $this->itemPoolRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            foreach ($entity->getItemItemPool() as $itemItemPool) {
                $this->itemPoolRepository->delete($itemItemPool);
            }

            $this->itemPoolRepository->delete($entity);
        }
    }
}
