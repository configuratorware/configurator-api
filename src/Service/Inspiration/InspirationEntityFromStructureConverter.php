<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Inspiration;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Entity\InspirationText;
use Redhotmagma\ConfiguratorApiBundle\Structure\Inspiration as InspirationStructure;

/**
 * @internal
 */
final class InspirationEntityFromStructureConverter
{
    /**
     * @param InspirationStructure $structure
     * @param Inspiration $entity
     * @param array $languages
     *
     * @return Inspiration
     */
    public function convertOne(InspirationStructure $structure, Inspiration $entity, array $languages): Inspiration
    {
        $entity->setActive($structure->active);
        $entity->setConfigurationCode($structure->configurationCode);

        $inspirationEntityMap = $this->mapInspirationTexts($entity->getInspirationText());

        $textsToKeep = [];
        foreach ((array)$structure->texts as $textStructure) {
            if (!isset($textStructure->id) || !array_key_exists($textStructure->id, $inspirationEntityMap)) {
                $textEntity = InspirationText::forInspiration($entity, $languages[$textStructure->language]);
                $entity->addInspirationText($textEntity);
            } else {
                $textEntity = $inspirationEntityMap[$textStructure->id];
            }

            $textEntity->setTitle($textStructure->title)->setDescription($textStructure->description);
            $textsToKeep[] = $textEntity->getId();
        }

        $this->removeInspirationTexts($entity, $textsToKeep);

        return $entity;
    }

    /**
     * @param Collection<int, InspirationText> $inspirationTexts
     *
     * @return array
     */
    private function mapInspirationTexts(Collection $inspirationTexts): array
    {
        $map = [];
        foreach ($inspirationTexts as $inspirationText) {
            $id = $inspirationText->getId();
            if (null === $id) {
                continue;
            }

            $map[$id] = $inspirationText;
        }

        return $map;
    }

    /**
     * @param Inspiration $entity
     * @param array $textsToKeep
     */
    private function removeInspirationTexts(Inspiration $entity, array $textsToKeep): void
    {
        /** @var InspirationText $inspirationText */
        foreach ($entity->getInspirationText() as $inspirationText) {
            if (!\in_array($inspirationText->getId(), $textsToKeep, false)) {
                $entity->removeInspirationText($inspirationText);
            }
        }
    }
}
