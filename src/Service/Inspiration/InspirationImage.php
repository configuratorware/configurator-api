<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Inspiration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\InspirationThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\InspirationThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\InspirationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadInspirationThumbnail;

/**
 * @internal
 */
final class InspirationImage
{
    /**
     * @var InspirationRepository
     */
    private $inspirationRepository;

    /**
     * @var InspirationThumbnailRepositoryInterface
     */
    private $inspirationThumbnailRepository;

    /**
     * @param InspirationRepository $inspirationRepository
     * @param InspirationThumbnailRepositoryInterface $inspirationThumbnailRepository
     */
    public function __construct(InspirationRepository $inspirationRepository, InspirationThumbnailRepositoryInterface $inspirationThumbnailRepository)
    {
        $this->inspirationRepository = $inspirationRepository;
        $this->inspirationThumbnailRepository = $inspirationThumbnailRepository;
    }

    /**
     * @param UploadInspirationThumbnail $uploadInspirationThumbnail
     */
    public function uploadThumbnail(UploadInspirationThumbnail $uploadInspirationThumbnail): void
    {
        $inspiration = $this->inspirationRepository->find($uploadInspirationThumbnail->inspirationId);

        $inspirationId = $inspiration->getId();
        if (null === $inspirationId) {
            throw NotFoundException::entityWithId(Inspiration::class, $uploadInspirationThumbnail->inspirationId);
        }

        $thumbnailToCreate = InspirationThumbnail::from(null, $inspirationId, $inspiration->getItem()->getIdentifier(), $uploadInspirationThumbnail->realPath, $uploadInspirationThumbnail->filename);

        $newThumbnail = $this->inspirationThumbnailRepository->saveThumbnail($thumbnailToCreate, $inspiration);

        $inspiration->setThumbnail($newThumbnail->getUrl());

        $this->inspirationRepository->save($inspiration);
    }

    /**
     * @param int $id
     */
    public function deleteThumbnail(int $id): void
    {
        $inspiration = $this->inspirationRepository->find($id);

        $thumbnail = $this->inspirationThumbnailRepository->findThumbnail($inspiration);

        $this->inspirationThumbnailRepository->deleteThumbnail($thumbnail);

        $inspiration->setThumbnail(null);

        $this->inspirationRepository->save($inspiration);
    }
}
