<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Inspiration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Entity\InspirationText;
use Redhotmagma\ConfiguratorApiBundle\Structure\Inspiration as InspirationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\InspirationText as InspirationTextStructure;

/**
 * @internal
 */
final class InspirationStructureFromEntityConverter
{
    /**
     * @param Inspiration $entity
     *
     * @return InspirationStructure
     */
    public function convertOne(Inspiration $entity): InspirationStructure
    {
        $texts = [];
        foreach ($entity->getInspirationText() as $entityText) {
            $texts[] = $this->createInspirationText($entityText);
        }

        return new InspirationStructure($entity->getId(), $entity->isActive(), $entity->getConfigurationCode(), $texts, $entity->getThumbnail(), (int)$entity->getItem()->getId());
    }

    /**
     * @param InspirationText $entity
     *
     * @return InspirationTextStructure
     */
    private function createInspirationText(InspirationText $entity): InspirationTextStructure
    {
        return new InspirationTextStructure($entity->getId(), $entity->getTitle(), $entity->getDescription(), $entity->getLanguage()->getIso());
    }
}
