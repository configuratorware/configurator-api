<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Inspiration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Entity\InspirationText;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationImages;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Inspiration as InspirationStructure;

/**
 * @internal
 */
class FrontendInspirationStructureFromEntity
{
    /**
     * @var ConfigurationImages
     */
    private $configurationImages;

    /**
     * @param ConfigurationImages $configurationImages
     */
    public function __construct(ConfigurationImages $configurationImages)
    {
        $this->configurationImages = $configurationImages;
    }

    /**
     * @param Inspiration $entity
     * @param string $languageIso
     *
     * @return InspirationStructure
     */
    public function fromEntity(Inspiration $entity, string $languageIso): InspirationStructure
    {
        $code = $entity->getConfigurationCode();

        $previewImage = null;
        if (null !== $code) {
            $previewImage = $this->configurationImages->getPreviewImage($code);
        }

        $translationText = $this->findTranslationText($entity, $languageIso);

        $title = null;
        $description = null;
        if (null !== $translationText) {
            $title = $translationText->getTitle();
            $description = $translationText->getDescription();
        }

        return new InspirationStructure($code, $title, $description, $entity->getThumbnail(), $previewImage);
    }

    /**
     * @param Inspiration $entity
     * @param string $languageIso
     *
     * @return InspirationText
     */
    private function findTranslationText(Inspiration $entity, string $languageIso): ?InspirationText
    {
        /** @var InspirationText $inspirationText */
        foreach ($entity->getInspirationText() as $inspirationText) {
            if ($inspirationText->getLanguage()->getIso() === $languageIso) {
                return $inspirationText;
            }
        }

        return null;
    }
}
