<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Inspiration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Inspiration;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\InspirationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Inspiration as FrontendInspirationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Inspiration as InspirationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadInspirationThumbnail;

final class InspirationApi
{
    /**
     * @var FrontendInspirationStructureFromEntity
     */
    private $frontendInspirationStructureFromEntity;

    /**
     * @var InspirationEntityFromStructureConverter
     */
    private $inspirationEntityFromStructureConverter;

    /**
     * @var InspirationImage
     */
    private $inspirationImage;

    /**
     * @var InspirationStructureFromEntityConverter
     */
    private $inspirationStructureFromEntityConverter;

    /**
     * @var InspirationRepository
     */
    private $inspirationRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @param FrontendInspirationStructureFromEntity $frontendInspirationStructureFromEntity
     * @param InspirationEntityFromStructureConverter $inspirationEntityFromStructureConverter
     * @param InspirationImage $inspirationImage
     * @param InspirationStructureFromEntityConverter $inspirationStructureFromEntityConverter
     * @param InspirationRepository $inspirationRepository
     * @param ItemRepository $itemRepository
     * @param LanguageRepository $languageRepository
     */
    public function __construct(FrontendInspirationStructureFromEntity $frontendInspirationStructureFromEntity, InspirationEntityFromStructureConverter $inspirationEntityFromStructureConverter, InspirationImage $inspirationImage, InspirationStructureFromEntityConverter $inspirationStructureFromEntityConverter, InspirationRepository $inspirationRepository, ItemRepository $itemRepository, LanguageRepository $languageRepository)
    {
        $this->frontendInspirationStructureFromEntity = $frontendInspirationStructureFromEntity;
        $this->inspirationEntityFromStructureConverter = $inspirationEntityFromStructureConverter;
        $this->inspirationImage = $inspirationImage;
        $this->inspirationStructureFromEntityConverter = $inspirationStructureFromEntityConverter;
        $this->inspirationRepository = $inspirationRepository;
        $this->itemRepository = $itemRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @param string $itemIdentifier
     * @param string $languageIso
     *
     * @return FrontendInspirationStructure[]
     */
    public function frontendList(string $itemIdentifier, string $languageIso): array
    {
        $entities = $this->inspirationRepository->fetchActiveByItemIdentifier($itemIdentifier);

        $frontendStructures = [];
        foreach ($entities as $entity) {
            $frontendStructures[] = $this->frontendInspirationStructureFromEntity->fromEntity($entity, $languageIso);
        }

        return $frontendStructures;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $entities = $this->inspirationRepository->fetchList($arguments, $this->getSearchableFields());

        $data = [];
        foreach ($entities as $entity) {
            $data[] = $this->inspirationStructureFromEntityConverter->convertOne($entity);
        }

        $count = $this->inspirationRepository->fetchListCount($arguments, $this->getSearchableFields());

        return new PaginationResult($data, (int)$count);
    }

    /**
     * @param int $id
     *
     * @return InspirationStructure
     */
    public function getOne(int $id): InspirationStructure
    {
        $entity = $this->inspirationRepository->find($id);

        if (null === $entity) {
            throw NotFoundException::entityWithId(Inspiration::class, $id);
        }

        return $this->inspirationStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @param InspirationStructure $inspirationStructure
     *
     * @return InspirationStructure
     */
    public function save(InspirationStructure $inspirationStructure): InspirationStructure
    {
        $languages = $this->languageRepository->findAllIsoMapped();
        $item = $this->itemRepository->find($inspirationStructure->itemId);

        $inspirationEntity = null;
        if (isset($inspirationStructure->id)) {
            $inspirationEntity = $this->inspirationRepository->find($inspirationStructure->id);
        }

        if (null === $inspirationEntity) {
            $inspirationEntity = Inspiration::forItem($item);
        }

        $inspirationEntity = $this->inspirationEntityFromStructureConverter->convertOne($inspirationStructure, $inspirationEntity, $languages);

        $this->inspirationRepository->save($inspirationEntity);
        $this->inspirationRepository->clear();

        return $this->inspirationStructureFromEntityConverter->convertOne($this->inspirationRepository->find($inspirationEntity->getId()));
    }

    /**
     * @param string $idParam
     */
    public function delete(string $idParam): void
    {
        $ids = explode(',', $idParam);

        foreach ($ids as $id) {
            $inspiration = $this->inspirationRepository->find($id);

            if (null === $inspiration) {
                continue;
            }

            foreach ($inspiration->getInspirationText() as $text) {
                $this->inspirationRepository->delete($text);
            }

            $this->inspirationRepository->delete($inspiration);
        }
    }

    /**
     * @param UploadInspirationThumbnail $uploadInspirationThumbnail
     */
    public function uploadThumbnail(UploadInspirationThumbnail $uploadInspirationThumbnail): void
    {
        $this->inspirationImage->uploadThumbnail($uploadInspirationThumbnail);
    }

    /**
     * @param int $id
     */
    public function deleteThumbnail(int $id): void
    {
        $this->inspirationImage->deleteThumbnail($id);
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    private function getSearchableFields(): array
    {
        return ['configuration_code', 'inspirationText.title'];
    }
}
