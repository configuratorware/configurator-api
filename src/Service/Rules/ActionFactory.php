<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules;

/**
 * @internal
 */
class ActionFactory
{
    /**
     * create and return an instance of an action class.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   string $action
     *
     * @return  mixed|null
     */
    public function create(string $action)
    {
        $actionClassName = 'Actions\\' . ucfirst($action) . 'Action';

        $actionClass = null;
        if (class_exists($actionClassName)) {
            $actionClass = new $actionClassName();
        }

        return $actionClass;
    }
}
