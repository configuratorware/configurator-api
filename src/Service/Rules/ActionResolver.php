<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules;

use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Class ActionResolver.
 *
 * handles execution of (multiple) actions for a rule result
 *
 * @internal
 */
class ActionResolver
{
    /**
     * @var ActionFactory
     */
    private $actionFactory;

    /**
     * ActionResolver constructor.
     *
     * @param ActionFactory $actionFactory
     */
    public function __construct(ActionFactory $actionFactory)
    {
        $this->actionFactory = $actionFactory;
    }

    /**
     * execute the rules actions based on the check result for the given configuration.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   Rule $rule
     * @param   CheckResult $checkResult
     * @param   Configuration $configuration
     *
     * @return  Configuration
     */
    public function executeRuleActions(Rule $rule, CheckResult $checkResult, Configuration $configuration): Configuration
    {
        if (!empty($rule->getAction())) {
            $actions = json_decode($rule->getAction());

            if (!empty($actions)) {
                foreach ($actions as $actionData) {
                    $action = $this->actionFactory->create($actionData->identifier);

                    if (!empty($action)) {
                        $configuration = $action->execute($action, $checkResult, $configuration);
                    }
                }
            }
        }

        return $configuration;
    }
}
