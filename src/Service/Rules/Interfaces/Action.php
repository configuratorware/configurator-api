<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces;

use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Interface Action.
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 *
 * @version 1.0
 */
interface Action
{
    /**
     * execute a an action on a configuration, and return if the execution was successful.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   mixed           $action
     * @param   CheckResult     $checkResult
     * @param   Configuration   $configuration
     *
     * @return  bool
     */
    public function execute(mixed $action, CheckResult $checkResult, Configuration $configuration): bool;
}
