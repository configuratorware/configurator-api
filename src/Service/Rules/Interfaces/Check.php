<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Interface Check.
 *
 * @author  Michael Aichele <aichele@redhotmagma.de>
 *
 * @since   1.0
 *
 * @version 1.0
 */
interface Check
{
    /**
     * execute a Rule on a configuration.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   Rule            $rule
     * @param   Configuration   $configuration
     * @param   DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult;
}
