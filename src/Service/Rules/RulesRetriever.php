<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\ValueObject\RulesExistCacheKey;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * @internal
 */
class RulesRetriever
{
    private RuleRepository $ruleRepository;

    private CacheInterface $cachePool;

    public function __construct(RuleRepository $ruleRepository, CacheInterface $cachePool)
    {
        $this->ruleRepository = $ruleRepository;
        $this->cachePool = $cachePool;
    }

    /**
     * @param Item|Option $entity
     *
     * @return Rule[]
     */
    public function getFilteredRules($entity, Configuration $configuration): array
    {
        // get the rules for the current Option/Item
        if ($entity instanceof Option) {
            $rules = $entity->getSortedRules($configuration->item->identifier);
        } else {
            $rules = $entity->getSortedRules();
        }

        // filter rules, some rule types are excluded for item checks and handled somewhere else
        $excludedRuleTypes = ['optionclassificationminamount'];
        /** @var Rule $rule */
        foreach ($rules as $i => $rule) {
            if (in_array($rule->getRuletype()->getIdentifier(), $excludedRuleTypes)) {
                unset($rules[$i]);
            }
        }

        return $rules;
    }

    /**
     * @param Item|Option $entity
     *
     * @return Rule[]
     */
    public function getRules($entity, Configuration $configuration): array
    {
        if ($entity instanceof Option) {
            return $entity->getSortedRules($configuration->item->identifier);
        }

        return $entity->getSortedRules();
    }

    public function hasRulesFor(Configuration $configuration): bool
    {
        $itemIdentifier = $configuration->item->identifier;

        return $this->cachePool->get(
            RulesExistCacheKey::for($itemIdentifier),
            function (ItemInterface $item) use ($itemIdentifier): bool {
                $item->expiresAfter(null);

                return $this->ruleRepository->getCountForItem($itemIdentifier) > 0;
            }
        );
    }
}
