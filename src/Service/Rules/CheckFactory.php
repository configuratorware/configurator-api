<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules;

use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionAttributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSwitchOption;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSwitchOptionNoCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check;

/**
 * @internal
 */
final class CheckFactory
{
    /**
     * @var OptionAttributeRepository
     */
    private $optionAttributeRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionThumbnailRepositoryInterface
     */
    private $optionThumbnailRepository;

    /**
     * @var ConfigurationSwitchOption
     */
    private $configurationSwitchOption;

    /**
     * @var ConfigurationSwitchOptionNoCheck
     */
    private $configurationSwitchOptionNoCheck;

    /**
     * @param OptionAttributeRepository $optionAttributeRepository
     * @param ItemRepository $itemRepository
     * @param OptionRepository $optionRepository
     * @param OptionThumbnailRepositoryInterface $optionThumbnailRepository
     * @param ConfigurationSwitchOption $configurationSwitchOption
     * @param ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck
     */
    public function __construct(
        OptionAttributeRepository $optionAttributeRepository,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository,
        OptionThumbnailRepositoryInterface $optionThumbnailRepository,
        ConfigurationSwitchOption $configurationSwitchOption,
        ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck
    ) {
        $this->optionAttributeRepository = $optionAttributeRepository;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
        $this->optionThumbnailRepository = $optionThumbnailRepository;
        $this->configurationSwitchOption = $configurationSwitchOption;
        $this->configurationSwitchOptionNoCheck = $configurationSwitchOptionNoCheck;
    }

    /**
     * create and return an instance of the correct check class for the given rule.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   Rule $rule
     * @param   bool $reverse
     *
     * @return  Check|null
     */
    public function create(Rule $rule, bool $reverse = false)
    {
        $ruleType = $rule->getRuletype();
        if (true === $reverse) {
            $checkClassName = __NAMESPACE__ . '\\Checks\\' . $this->toCamelCase($ruleType->getIdentifier()) . 'ReverseCheck';
        } else {
            $checkClassName = __NAMESPACE__ . '\\Checks\\' . $this->toCamelCase($ruleType->getIdentifier()) . 'Check';
        }

        if (!class_exists($checkClassName)) {
            return null;
        }

        $checkClass = new $checkClassName();
        if (!$checkClass instanceof Check) {
            return null;
        }

        $checkClass->setOptionAttributeRepository($this->optionAttributeRepository);
        $checkClass->setItemRepository($this->itemRepository);
        $checkClass->setOptionRepository($this->optionRepository);
        $checkClass->setConfigurationSwitchOption($this->configurationSwitchOption);
        $checkClass->setConfigurationSwitchOptionNoCheck($this->configurationSwitchOptionNoCheck);
        $checkClass->setOptionThumbnailRepository($this->optionThumbnailRepository);

        return $checkClass;
    }

    /**
     * convert a string to camel case by a list of given separators.
     *
     * @param string $input
     *
     * @return string
     */
    private function toCamelCase(string $input): string
    {
        $separators = ['-', '_', ' '];

        return str_replace($separators, '', ucwords($input, implode('', $separators)));
    }
}
