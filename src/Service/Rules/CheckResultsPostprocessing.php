<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules;

use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSwitchOptionNoCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
class CheckResultsPostprocessing
{
    /**
     * @var ConfigurationSwitchOptionNoCheck
     */
    private $configurationSwitchOptionNoCheck;

    /**
     * @var CheckResolver
     */
    private $checkResolver;

    /**
     * CheckResultsPostprocessing constructor.
     *
     * @param ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck
     * @param CheckResolver $checkResolver
     */
    public function __construct(
        ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck,
        CheckResolver $checkResolver
    ) {
        $this->configurationSwitchOptionNoCheck = $configurationSwitchOptionNoCheck;
        $this->checkResolver = $checkResolver;
    }

    /**
     * check results postprocessing
     * filter combinations of failed results to be nicer for user.
     *
     * @param Configuration $configuration
     *
     * @return Configuration
     */
    public function checkResultsPostprocessing($configuration)
    {
        $checkResults = $configuration->check_results;

        // if an option exclusion failed and there was a max amount failure
        // the max amount failure should not be presented to user
        // because it most likely will be fixed by resolving the exclusion
        $hasOptionExclusionFailure = false;
        $hasMaxAmountFailure = false;
        if (false === $checkResults->status) {
            foreach ($checkResults->check_results as $checkResult) {
                if (false === $checkResult->status) {
                    if ('optionexclusion' === $checkResult->rule_type) {
                        $hasOptionExclusionFailure = true;
                    } else {
                        if ('optionclassificationmaxamount' === $checkResult->rule_type) {
                            $hasMaxAmountFailure = true;
                        }
                    }
                }
            }

            if ($hasOptionExclusionFailure && $hasMaxAmountFailure) {
                foreach ($checkResults->check_results as $checkResult) {
                    if ('optionclassificationmaxamount' === $checkResult->rule_type) {
                        $checkResult->status = true;
                    }
                }
            }
        }

        $configuration->check_results = $checkResults;

        // handle default options check results
        $configuration = $this->handleDefaultOptionsCheckResults($configuration);

        // handle AttributeValueGroupAutoSwitchCheck
        $configuration = $this->handleAttributeValueGroupAutoSwitchCheckResults($configuration);

        return $configuration;
    }

    /**
     * if a defaultoption check has failed, try to add the required default option.
     *
     * @param Configuration $configuration
     *
     * @return Configuration
     */
    protected function handleDefaultOptionsCheckResults($configuration)
    {
        $checkResults = $configuration->check_results;

        if (!empty($checkResults->check_results)) {
            foreach ($checkResults->check_results as $checkResult) {
                if ('defaultoption' === $checkResult->rule_type) {
                    // remove default option first
                    $switchOptionObjects = [];
                    foreach ($checkResult->data['switchOptions'] as $itemClassificationIdentifier => $switchOptions) {
                        foreach ($switchOptions as $i => $switchOption) {
                            $switchOptionObjects[$itemClassificationIdentifier] = json_decode(json_encode($switchOption));
                            $switchOptionObjects[$itemClassificationIdentifier]->amount = 0;
                        }
                    }

                    if (!empty($switchOptionObjects)) {
                        $configurationClone = serialize($configuration);

                        $configurationOut = $this->configurationSwitchOptionNoCheck->switchOption(unserialize($configurationClone),
                            $switchOptionObjects);

                        $switchOptionObjects = [];
                        foreach ($checkResult->data['switchOptions'] as $itemClassificationIdentifier => $switchOptions) {
                            foreach ($switchOptions as $i => $switchOption) {
                                $switchOptionObjects[$itemClassificationIdentifier] = json_decode(json_encode($switchOption));
                            }
                        }

                        $defaultOptionCheckResults = $this->checkResolver->runChecksForSwitchOptions($switchOptionObjects,
                            $configurationOut);

                        if (true == $defaultOptionCheckResults->status) {
                            $configurationOut = $this->configurationSwitchOptionNoCheck->switchOption($configurationOut,
                                $switchOptionObjects);
                        }
                        $configuration->optionclassifications = $configurationOut->optionclassifications;
                    }
                }
            }
        }

        return $configuration;
    }

    /**
     * if an AttributeValueGroupAutoSwitchCheck is set to "silent" directly resolve the conflict.
     *
     * @param $configuration
     *
     * @return mixed
     */
    protected function handleAttributeValueGroupAutoSwitchCheckResults($configuration)
    {
        $checkResults = $configuration->check_results;

        if (!empty($checkResults->check_results)) {
            foreach ($checkResults->check_results as $checkResult) {
                if ('attribute_value_group_auto_switch' === $checkResult->rule_type) {
                    if (false == $checkResult->status
                        && isset($checkResult->data['rulefeedback'])
                        && !empty($checkResult->data['switchOptions'])) {
                        // directly resolve conflict or deliver it to gui to be handled there
                        if ('silent' == $checkResult->data['rulefeedback']) {
                            $configurationClone = serialize($configuration);

                            $configurationOut = null;
                            foreach ($checkResult->data['switchOptions'] as $optionClassificationIdentifier => $switchOptions) {
                                foreach ($switchOptions as $i => $switchOption) {
                                    $switchOptionObjects = [];
                                    $switchOptionObjects[$optionClassificationIdentifier] = json_decode(json_encode($switchOption));

                                    if (isset($configurationOut)) {
                                        $configurationClone = serialize($configurationOut);
                                    }

                                    $configurationOut = $this->configurationSwitchOptionNoCheck->switchOption(unserialize($configurationClone),
                                        $switchOptionObjects);
                                }
                            }

                            $configuration->optionclassifications = $configurationOut->optionclassifications;

                            // update check result status
                            $checkResult->status = true;
                        }
                    }

                    // if the rule is silent, do not show an error
                    if (isset($checkResult->data['rulefeedback'])
                        && 'silent' == $checkResult->data['rulefeedback']) {
                        // update check result status
                        $checkResult->status = true;
                    }
                }
            }

            $status = true;
            foreach ($checkResults->check_results as $checkResult) {
                if (false == $checkResult->status) {
                    $status = false;
                }
            }
            $checkResults->status = $status;
        }

        return $configuration;
    }
}
