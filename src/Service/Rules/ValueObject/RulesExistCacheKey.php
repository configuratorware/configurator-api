<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\ValueObject;

class RulesExistCacheKey
{
    public const RULES_EXIST_CACHE_KEY_PREFIX = 'rules_exist_item_';

    public static function for(string $itemIdentifier): string
    {
        return self::RULES_EXIST_CACHE_KEY_PREFIX . $itemIdentifier;
    }
}
