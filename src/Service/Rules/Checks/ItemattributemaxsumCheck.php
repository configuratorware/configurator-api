<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Class AttributeMaxSumCheck.
 *
 * checks if all values for the given attribute in the current configuration exceed the max sum
 *
 * @internal
 */
class ItemattributemaxsumCheck extends BaseCheck implements CheckInterface
{
    /**
     * checks if the selected amount of options within an option classification exceeds the max amount.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkResult = new CheckResult();

        $ruleData = json_decode($rule->getData());

        $attributeIdentifier = $ruleData->attribute_identifier;
        $optionIdentifiersAmount = [];

        foreach ($configuration->optionclassifications as $optionClassification) {
            if (is_array($optionClassification->selectedoptions)) {
                foreach ($optionClassification->selectedoptions as $selectedOption) {
                    if (!isset($optionIdentifiersAmount[$selectedOption->identifier])) {
                        $optionIdentifiersAmount[$selectedOption->identifier] = $selectedOption->amount;
                    } else {
                        $optionIdentifiersAmount[$selectedOption->identifier] += $selectedOption->amount;
                    }
                }
            }
        }

        /** @var string[] $optionIdentifierList */
        $optionIdentifierList = array_keys($optionIdentifiersAmount);
        $amountAttributes = $this->getOptionAttributeRepository()
            ->fetchByOptionIdentifiersAndAttributeIdentifier($optionIdentifierList, $attributeIdentifier);

        $sumIsValid = true;
        $attributeSum = 0;
        foreach ($amountAttributes as $attribute) {
            $amount = $optionIdentifiersAmount[$attribute->getOption()->getIdentifier()];
            $attributeSum += $amount * $attribute->getAttributevalue()->getValue();
            if ($attributeSum > $ruleData->maxamount) {
                $sumIsValid = false;

                break;
            }
        }

        $checkResult->status = $sumIsValid;
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();
        $checkResult->checked_option = null;

        // generate message if the required option was not found
        if (false == $checkResult->status) {
            $feedbackText = $this->getRulefeedbacktext($rule);
            $message = $feedbackText->getTranslatedRulefeedbacktext();

            $message = str_replace(
                ['[attribute]'],
                [$amountAttributes[0]->getAttribute()->getTranslatedTitle()],
                $message);

            $checkResult->message = $message;
            $checkResult->data = null;
        }

        return $checkResult;
    }
}
