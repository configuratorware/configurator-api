<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResultOptionInformation;

/**
 * Class OptionmaxamountCheck.
 *
 * checks if the max amount for one option exceeded
 *
 * @internal
 */
class OptionmaxamountCheck extends BaseCheck implements CheckInterface
{
    /**
     * checks if the selected amount of options within an option classification exceeds the max amount.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkedOption = $rule->getOption();

        $checkResult = new CheckResult();
        $checkResult->status = true;

        $selectedAmount = 0;
        $switched = false;

        $ruleData = json_decode($rule->getData());

        if (isset($configuration->switchOptions)) {
            foreach ($configuration->switchOptions as $switchOption) {
                if ($switchOption instanceof \stdClass) {
                    $switchOption = [$switchOption];
                }

                foreach ($switchOption as $switchOptionElement) {
                    if ($switchOptionElement->identifier == $checkedOption->getIdentifier()) {
                        $selectedAmount += (int)$switchOptionElement->amount;
                        $switched = true;
                    }
                }
            }
        }

        // if the option was not found in switched options, look for it in selected options
        if (false === $switched) {
            foreach ($configuration->optionclassifications as $optionClassification) {
                if (is_array($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        if ($selectedOption->identifier == $checkedOption->getIdentifier()) {
                            $selectedAmount += (int)$selectedOption->amount;

                            break 2;
                        }
                    }
                }
            }
        }

        if ($selectedAmount > (int)$ruleData->maxamount) {
            $checkResult->status = false;
        }

        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();

        $checkedItemInformation = new CheckResultOptionInformation();
        $checkedItemInformation->identifier = $checkedOption->getIdentifier();
        $checkedItemInformation->title = $checkedOption->getTranslatedTitle();

        try {
            $checkedItemInformation->thumbnail = $this->getOptionThumbnailRepository()->findThumbnail($checkedOption)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }

        $checkResult->checked_option = $checkedItemInformation;

        // generate message if the max amount was exceeded
        if (false == $checkResult->status) {
            $feedbackText = $this->getRulefeedbacktext($rule);
            $message = $feedbackText->getTranslatedRulefeedbacktext();

            $message = str_replace(
                ['[option]'],
                [$checkedItemInformation->title],
                $message);

            $checkResult->message = $message;
            $checkResult->data = null;
        }

        return $checkResult;
    }
}
