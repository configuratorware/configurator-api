<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rulefeedbacktext;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionAttributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSwitchOption;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSwitchOptionNoCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
abstract class Check implements \Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check
{
    /**
     * @var OptionAttributeRepository
     */
    private $optionAttributeRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionThumbnailRepositoryInterface
     */
    private $optionThumbnailRepository;

    /**
     * @var ConfigurationSwitchOption
     */
    private $configurationSwitchOption;

    /**
     * @var ConfigurationSwitchOptionNoCheck
     */
    private $configurationSwitchOptionNoCheck;

    /**
     * @return OptionAttributeRepository
     */
    public function getOptionAttributeRepository(): OptionAttributeRepository
    {
        return $this->optionAttributeRepository;
    }

    /**
     * @param OptionAttributeRepository $optionAttributeRepository
     */
    public function setOptionAttributeRepository(OptionAttributeRepository $optionAttributeRepository)
    {
        $this->optionAttributeRepository = $optionAttributeRepository;
    }

    /**
     * @return ItemRepository
     */
    public function getItemRepository(): ItemRepository
    {
        return $this->itemRepository;
    }

    /**
     * @param ItemRepository $itemRepository
     */
    public function setItemRepository(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @return OptionRepository
     */
    public function getOptionRepository(): OptionRepository
    {
        return $this->optionRepository;
    }

    /**
     * @param OptionRepository $optionRepository
     */
    public function setOptionRepository(OptionRepository $optionRepository)
    {
        $this->optionRepository = $optionRepository;
    }

    /**
     * @return ConfigurationSwitchOption
     */
    public function getConfigurationSwitchOption(): ConfigurationSwitchOption
    {
        return $this->configurationSwitchOption;
    }

    /**
     * @param ConfigurationSwitchOption $configurationSwitchOption
     */
    public function setConfigurationSwitchOption(ConfigurationSwitchOption $configurationSwitchOption): void
    {
        $this->configurationSwitchOption = $configurationSwitchOption;
    }

    /**
     * @return ConfigurationSwitchOptionNoCheck
     */
    public function getConfigurationSwitchOptionNoCheck(): ConfigurationSwitchOptionNoCheck
    {
        return $this->configurationSwitchOptionNoCheck;
    }

    /**
     * @param ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck
     */
    public function setConfigurationSwitchOptionNoCheck(
        ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck
    ): void {
        $this->configurationSwitchOptionNoCheck = $configurationSwitchOptionNoCheck;
    }

    /**
     * @return OptionThumbnailRepositoryInterface
     */
    public function getOptionThumbnailRepository(): OptionThumbnailRepositoryInterface
    {
        return $this->optionThumbnailRepository;
    }

    /**
     * @param OptionThumbnailRepositoryInterface $optionThumbnailRepository
     */
    public function setOptionThumbnailRepository(OptionThumbnailRepositoryInterface $optionThumbnailRepository): void
    {
        $this->optionThumbnailRepository = $optionThumbnailRepository;
    }

    /**
     * get Rulefeedbacktext for given Rule.
     *
     * @param Rule $rule
     *
     * @return  Rulefeedbacktext
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     */
    protected function getRulefeedbacktext(Rule $rule): Rulefeedbacktext
    {
        $feedbackText = null;
        foreach ($rule->getRulefeedback()->getRulefeedbacktext() as $ruleFeedbackText) {
            if ($ruleFeedbackText->getRuletype()->getId() == $rule->getRuletype()->getId()) {
                $feedbackText = $ruleFeedbackText;

                break;
            }
        }

        return $feedbackText;
    }

    /**
     * Returns nested array with component identifier as key.
     *
     * @param string $componentIdentifier
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return array<string, array<string, string|int>>|false
     */
    protected function tryDefaultOptionForComponent(
        string $componentIdentifier,
        Configuration $configuration,
        DefaultConfiguration $defaultConfiguration
    ) {
        $defaultOptionSwitch = false;

        // calledByTryDefaultOptionForComponent: is set to true before switching in the default option for a component
        // if then this check is called again we prevent endless recursion by always returning false if it was called from here
        if (isset($configuration->calledByTryDefaultOptionForComponent)
            && true === $configuration->calledByTryDefaultOptionForComponent) {
            return false;
        }

        // in a multi select component we never want to select the default option automatically
        foreach ($configuration->optionclassifications as $component) {
            if ($component->identifier === $componentIdentifier && true === $component->is_multiselect) {
                return false;
            }
        }

        // get the default option for the current component
        $defaultOptions = $defaultConfiguration->getSelectedoptions();
        if (!empty($defaultOptions)) {
            if (isset($defaultOptions[$componentIdentifier]) && !empty($defaultOptions[$componentIdentifier])) {
                $defaultOption = $defaultOptions[$componentIdentifier][0];

                $switchOptions = [];
                $switchOption = new \stdClass();
                $switchOption->identifier = $defaultOption['identifier'];
                $switchOption->amount = 1;
                $switchOptions[$componentIdentifier] = $switchOption;

                $configuration->calledByTryDefaultOptionForComponent = true;
                $backupConfiguration = serialize($configuration);

                // if we have a switch option add it first to make sure the default option fits the option we are currently selecting
                if (isset($configuration->switchOptions)) {
                    $configuration = $this->configurationSwitchOptionNoCheck->switchOption($configuration,
                        $configuration->switchOptions);
                }

                // try to add the found default option to the configuration
                $configuration = $this->configurationSwitchOption->switchOption($configuration, $switchOptions);

                // convert the switch options to an array for later use
                $switchOption = (array)$switchOptions[$componentIdentifier];
                $switchOptions[$componentIdentifier] = [$switchOption];

                // if the default option is valid return the switch option array to resolve the conflict later
                if (true === $configuration->check_results->status) {
                    /** @var array<string, array<string, string|int>> $defaultOptionSwitch */
                    $defaultOptionSwitch = $switchOptions;
                }

                $configuration = unserialize($backupConfiguration);
                unset($configuration->calledByTryDefaultOptionForComponent);
            }
        }

        return $defaultOptionSwitch;
    }
}
