<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Class OptionclassificationminamountCheck.
 *
 * checks if the selected amount of options within an option classification comes below the min amount
 *
 * @internal
 */
class OptionclassificationminamountCheck extends BaseCheck implements CheckInterface
{
    /**
     * checks if the selected amount of options within an option classification comes below the min amount.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkResult = new CheckResult();

        $ruleData = json_decode($rule->getData());

        $optionClassificationIdentifier = $ruleData->optionclassification_identifier;
        $minAmount = $ruleData->minamount;

        $optionClassificationStructure = null;
        $selectedAmount = 0;
        foreach ($configuration->optionclassifications as $optionClassification) {
            if ($optionClassification->identifier == $optionClassificationIdentifier) {
                $optionClassificationStructure = $optionClassification;
                if (is_array($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        $selectedAmount += $selectedOption->amount;
                    }

                    break;
                }
            }
        }

        $checkResult->status = $selectedAmount >= $minAmount;
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();
        $checkResult->checked_option = $optionClassificationIdentifier;

        // generate message if the required option was not found
        if (!empty($optionClassificationStructure) && false == $checkResult->status) {
            $feedbackText = $this->getRulefeedbacktext($rule);
            $message = $feedbackText->getTranslatedRulefeedbacktext();

            $message = str_replace(
                ['[optionclassificationidentifier]', '[minamount]'],
                [$optionClassificationStructure->title, $minAmount],
                $message);

            $checkResult->message = $message;

            $amounts = new \stdClass();
            $amounts->minAmount = $minAmount;
            $amounts->selectedAmount = $selectedAmount;
            $checkResult->data = $amounts;
        }

        return $checkResult;
    }
}
