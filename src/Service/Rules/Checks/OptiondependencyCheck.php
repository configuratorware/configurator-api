<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResultOptionInformation;

/**
 * Class OptiondependencyCheck.
 *
 * checks if an option is already selected in the configuration
 * test fails if the option is not selected
 *
 * @internal
 */
class OptiondependencyCheck extends BaseCheck implements CheckInterface
{
    /**
     * checks if an option is already selected in the configuration
     * test fails if the option is not selected.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkResult = new CheckResult();

        $ruleData = json_decode($rule->getData());
        $dependentOptionIdentifier = $ruleData->option_identifier;

        $dependentOptionFound = false;

        if (!empty($dependentOptionIdentifier)) {
            foreach ($configuration->optionclassifications as $optionClassification) {
                if (is_array($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        if ($selectedOption->identifier == $dependentOptionIdentifier) {
                            $dependentOptionFound = true;

                            break;
                        }
                    }
                }

                if (true === $dependentOptionFound) {
                    break;
                }
            }
        }

        $checkedOption = $rule->getOption();
        $checkResult->status = $dependentOptionFound;
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();

        $itemClassificationIdentifiers = $this->getOptionRepository()->getOptionClassificationIdentifiersByOptionIdentifierAndBaseItemIdentifier($dependentOptionIdentifier,
            $configuration->item->identifier);

        $checkedOptionItemClassificationIdentifiers = $this->getOptionRepository()->getOptionClassificationIdentifiersByOptionIdentifierAndBaseItemIdentifier($checkedOption->getIdentifier(), $configuration->item->identifier);

        $checkedItemInformation = new CheckResultOptionInformation();
        $checkedItemInformation->identifier = $checkedOption->getIdentifier();
        $checkedItemInformation->title = $checkedOption->getTranslatedTitle();
        $checkedItemInformation->target_optionclassification_identifiers = $itemClassificationIdentifiers;
        $checkedItemInformation->optionclassification_identifiers = $checkedOptionItemClassificationIdentifiers;

        try {
            $checkedItemInformation->thumbnail = $this->getOptionThumbnailRepository()->findThumbnail($checkedOption)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }

        $checkResult->checked_option = $checkedItemInformation;

        // generate message if the required option was not found
        if (false === $dependentOptionFound) {
            $feedbackText = $this->getRulefeedbacktext($rule);
            $message = $feedbackText->getTranslatedRulefeedbacktext();

            $dependentItem = $this->getOptionRepository()->findOneByIdentifier($dependentOptionIdentifier);

            // if the dependent item is no longer found the check will not fail
            if (empty($dependentItem)) {
                $checkResult->status = true;
            } else {
                $dependentItemInformation = new CheckResultOptionInformation();
                $dependentItemInformation->identifier = $dependentItem->getIdentifier();
                $dependentItemInformation->title = $dependentItem->getTranslatedTitle();
                $dependentItemInformation->optionclassification_identifiers = $itemClassificationIdentifiers;

                try {
                    $dependentItemInformation->thumbnail = $this->getOptionThumbnailRepository()->findThumbnail($dependentItem)->getUrl();
                } catch (FileException $e) {
                    // file can be non existent
                }

                $checkResult->conflicting_options = [$dependentItemInformation];

                $message = str_replace(
                    ['[optionidentifier_needed]', '[optionidentifier_selected]'],
                    [$dependentItem->getTranslatedTitle(), $rule->getItem()->getTranslatedTitle()],
                    $message);

                $checkResult->message = $message;

                $data = [];
                $switchOptions = [];
                foreach ($itemClassificationIdentifiers as $itemClassificationIdentifier) {
                    $switchOptions[$itemClassificationIdentifier][] = [
                        'identifier' => $dependentOptionIdentifier,
                        'amount' => 1,
                    ];
                }

                $data['switchOptions'] = $switchOptions;

                $checkResult->data = $data;
            }
        }

        return $checkResult;
    }
}
