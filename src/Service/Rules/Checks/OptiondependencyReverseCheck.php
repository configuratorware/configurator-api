<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResultOptionInformation;

/**
 * Class OptiondependencyCheck.
 *
 * checks if an option is already selected in the configuration
 * test fails if the option is not selected
 *
 * @internal
 */
class OptiondependencyReverseCheck extends BaseCheck implements CheckInterface
{
    /**
     * checks if an option is already selected in the configuration
     * test fails if the option is not selected.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkResult = new CheckResult();

        $ruleData = json_decode($rule->getData());
        $dependentOptionIdentifier = $ruleData->option_identifier;
        $dependentOptionFound = false;

        $checkedItemClassificationIdentifiers = $this->getOptionRepository()->getOptionClassificationIdentifiersByOptionIdentifierAndBaseItemIdentifier($dependentOptionIdentifier,
            $configuration->item->identifier);

        $checkedOption = null;

        if (!empty($dependentOptionIdentifier)) {
            foreach ($configuration->optionclassifications as $optionClassification) {
                if (is_array($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        if ($selectedOption->identifier == $dependentOptionIdentifier) {
                            $dependentOptionFound = true;

                            break;
                        }

                        if (in_array($optionClassification->identifier, $checkedItemClassificationIdentifiers)) {
                            $checkedOption = $selectedOption;
                        }
                    }
                }

                if (true === $dependentOptionFound) {
                    break;
                }
            }
        }

        $checkResult->status = $dependentOptionFound;
        // a reverse dependency is handled like an exclusion
        $checkResult->rule_type = 'optionexclusion';

        if (!empty($checkedOption)) {
            $checkedItem = $this->getOptionRepository()->findOneByIdentifier($checkedOption->identifier);
        } else {
            $checkedItem = $this->getOptionRepository()->findOneByIdentifier($rule->getOption()->getIdentifier());
        }

        $checkedItemInformation = new CheckResultOptionInformation();
        $checkedItemInformation->identifier = $checkedItem->getIdentifier();
        $checkedItemInformation->title = $checkedItem->getTranslatedTitle();

        try {
            $checkedItemInformation->thumbnail = $this->getOptionThumbnailRepository()->findThumbnail($checkedItem)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }

        $conflictingComponentIdentifiers = $this->getOptionRepository()->getOptionClassificationIdentifiersByOptionIdentifierAndBaseItemIdentifier($rule->getOption()->getIdentifier(),
            $configuration->item->identifier);
        $checkedItemInformation->target_optionclassification_identifiers = $conflictingComponentIdentifiers;
        $checkedItemInformation->optionclassification_identifiers = $conflictingComponentIdentifiers;

        $checkResult->checked_option = $checkedItemInformation;

        // work on a copy of the configuration to prevent manipulation
        $workingConfiguration = unserialize(serialize($configuration));

        // generate message if the required option was not found
        if (true === !$dependentOptionFound) {
            $switchOptions = [];
            foreach ($conflictingComponentIdentifiers as $componentIdentifier) {
                // if selecting the default option is not valid remove the currently selected option to resolve the conflict
                $switchOptions = $this->tryDefaultOptionForComponent($componentIdentifier,
                    $workingConfiguration, $defaultConfiguration);
                if (false === $switchOptions) {
                    $switchOptions[$componentIdentifier] = [[
                        'identifier' => $rule->getOption()->getIdentifier(),
                        'amount' => 0,
                    ]];
                }
            }

            $feedbackText = $this->getRulefeedbacktext($rule);
            $message = $feedbackText->getTranslatedRulefeedbacktext();

            $conflictingOption = $rule->getOption();

            $conflictingItemInformation = new CheckResultOptionInformation();
            $conflictingItemInformation->identifier = $conflictingOption->getIdentifier();
            $conflictingItemInformation->title = $conflictingOption->getTranslatedTitle();
            $conflictingItemInformation->optionclassification_identifiers = array_keys($switchOptions);

            try {
                $conflictingItemInformation->thumbnail = $this->getOptionThumbnailRepository()->findThumbnail($conflictingOption)->getUrl();
            } catch (FileException $e) {
                // file can be non existent
            }

            $checkResult->conflicting_options = [$conflictingItemInformation];

            $message = str_replace(
                ['[optionidentifier_excluded]', '[optionidentifier_selected]'],
                [$conflictingOption->getTranslatedTitle(), $rule->getItem()->getIdentifier()],
                $message);

            $checkResult->message = $message;

            $data = [];
            $data['switchOptions'] = $switchOptions;
            $data['reverseCheck'] = true;

            $checkResult->data = $data;
        }

        return $checkResult;
    }
}
