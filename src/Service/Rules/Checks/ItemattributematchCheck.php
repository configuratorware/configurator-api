<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Class ItemattributematchCheck.
 *
 * checks if there is an attribute mismatch in the configuration for the attribute given in the rule
 *
 * @internal
 */
class ItemattributematchCheck extends BaseCheck
{
    /**
     * checks if the selected amount of options within an option classification exceeds the max amount.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(
        Rule $rule,
        Configuration $configuration,
        DefaultConfiguration $defaultConfiguration
    ): CheckResult {
        $checkResult = new CheckResult();

        $ruleData = json_decode($rule->getData());

        $attributeIdentifier = $ruleData->attribute_identifier;
        $switchOptionIdentifiers = $this->findSwitchOptionIdentifiers($configuration);
        $validAttributeValues = $this->getSwitchOptionAttributeValues($switchOptionIdentifiers, $attributeIdentifier);

        $optionIdentifiers = $this->findSelectedOptionIdentifiers($configuration);
        $attributeCheckResult = $this->getOptionAttributeRepository()
            ->checkCommonAttributeValues($optionIdentifiers, $attributeIdentifier, $validAttributeValues);

        // filter out the currently selected option from the conflicting option
        if (isset($configuration->switchOptions)) {
            foreach ($configuration->switchOptions as $switchOption) {
                if ($switchOption instanceof \stdClass) {
                    $switchOption = [$switchOption];
                }
                foreach ($switchOption as $switchOptionItem) {
                    $checkedOption = $switchOptionItem;
                }
            }
        }

        $checkResult->status = $attributeCheckResult->hasCommonValues;
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();
        $checkResult->checked_option = $checkedOption ?? null;
        $checkResult->conflicting_options = [];

        if ($attributeCheckResult->hasCommonValues) {
            return $checkResult;
        }

        // work on a copy of the configuration to prevent manipulation
        $workingConfiguration = unserialize(serialize($configuration));

        // remove all conflicting options
        $componentsToResolve = [];
        $conflictingOptions = [];
        foreach ($workingConfiguration->optionclassifications as $component) {
            if (!is_array($component->selectedoptions)) {
                continue;
            }

            foreach ($component->selectedoptions as $selectedOption) {
                if (in_array($selectedOption->identifier, $attributeCheckResult->conflictingOptionIdentifiers, true)) {
                    if (!isset($componentsToResolve[$component->identifier])) {
                        $componentsToResolve[$component->identifier] = [];
                    }
                    $componentsToResolve[$component->identifier][] = $selectedOption->identifier;
                    $component->selectedoptions = [];
                    $conflictingOptions[] = $selectedOption;
                }
            }
        }

        // add the originally selected option again
        if (isset($workingConfiguration->switchOptions)) {
            $workingConfiguration = $this->getConfigurationSwitchOptionNoCheck()->switchOption($workingConfiguration,
                $workingConfiguration->switchOptions);
        }

        // find solutions for the conflicts
        // try default options, if defaults do not match remove the conflicting option
        $switchOptions = [];
        foreach ($componentsToResolve as $componentIdentifier => $optionsToRemove) {
            if (!isset($switchOptions[$componentIdentifier])) {
                $switchOptions[$componentIdentifier] = [];
            }

            $defaultOptionSwitch = $this->tryDefaultOptionForComponent($componentIdentifier, $workingConfiguration, $defaultConfiguration);
            if (false === $defaultOptionSwitch) {
                foreach ($optionsToRemove as $optionToRemove) {
                    $switchOptions[$componentIdentifier][] = [
                        'identifier' => $optionToRemove,
                        'amount' => 0,
                    ];
                }
            } else {
                $switchOptions = array_merge_recursive($switchOptions, $defaultOptionSwitch);
            }
        }

        // generate message if the required option was not found
        if (!$checkResult->status) {
            $feedbackText = $this->getRulefeedbacktext($rule);
            $message = $feedbackText->getTranslatedRulefeedbacktext();

            $checkResult->message = str_replace(['[attribute]'], [$attributeCheckResult->attributeTitle], $message);
            $checkResult->data = [
                'switchOptions' => $switchOptions,
            ];
            $checkResult->conflicting_options = $conflictingOptions;
        }

        return $checkResult;
    }

    /**
     * @return string[]
     */
    private function findSelectedOptionIdentifiers(Configuration $configuration): array
    {
        $optionIdentifiers = [];

        foreach ($configuration->optionclassifications as $component) {
            if (is_array($component->selectedoptions)) {
                foreach ($component->selectedoptions as $selectedOption) {
                    if (!isset($selectedOption->identifier)) {
                        continue;
                    }

                    $optionIdentifiers[] = $selectedOption->identifier;
                }
            }
        }

        return $optionIdentifiers;
    }

    /**
     * @return array|string[]
     */
    private function findSwitchOptionIdentifiers(Configuration $configuration): array
    {
        $switchedOptionIdentifiers = [];
        if (!property_exists($configuration, 'switchOptions') || !is_array($configuration->switchOptions)) {
            return $switchedOptionIdentifiers;
        }

        foreach ($configuration->switchOptions as $switchOption) {
            if ($switchOption instanceof \stdClass) {
                $switchOption = [$switchOption];
            }

            foreach ($switchOption as $switchOptionElement) {
                $switchedOptionIdentifiers[] = $switchOptionElement->identifier;
            }
        }

        return $switchedOptionIdentifiers;
    }

    /**
     * @param array|string[] $switchOptionIdentifiers
     *
     * @return array|string[]
     */
    private function getSwitchOptionAttributeValues(array $switchOptionIdentifiers, string $attributeIdentifier): array
    {
        if (empty($switchOptionIdentifiers)) {
            return [];
        }

        $switchedOptionAttributes = $this->getOptionAttributeRepository()
            ->fetchByOptionIdentifiersAndAttributeIdentifier($switchOptionIdentifiers, $attributeIdentifier);

        return array_map(static function (OptionAttribute $optionAttribute) {
            return $optionAttribute->getAttributevalue()->getValue();
        }, $switchedOptionAttributes);
    }
}
