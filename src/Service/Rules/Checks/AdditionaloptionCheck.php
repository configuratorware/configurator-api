<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Class AdditionaloptionCheck.
 *
 * the rule holds an option that should be selected in the configuration.
 * this option is automatically selected when the option that hold this rule is selected.
 * it will be deselected when the option holding this rule is removed
 *
 * @internal
 */
class AdditionaloptionCheck extends BaseCheck implements CheckInterface
{
    /**
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.60
     *
     * @version 1.0.60
     *
     * @param   Rule $rule
     * @param   Configuration $configuration
     * @param   DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkResult = new CheckResult();
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();
        $checkResult->checked_option = null;

        $ruleData = json_decode($rule->getData());
        $additionalOptionIdentifier = $ruleData->option_identifier;

        $checkResult->additional_option = $additionalOptionIdentifier;

        $checkResult->checked_option = $rule->getOption()->getIdentifier();
        $checkResult->status = true;

        return $checkResult;
    }
}
