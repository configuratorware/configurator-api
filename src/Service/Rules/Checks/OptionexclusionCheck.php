<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResultOptionInformation;

/**
 * Class OptionexclusionCheck.
 *
 * checks if another option is already selected in the configuration
 * test fails if the other option is selected
 *
 * @internal
 */
class OptionexclusionCheck extends BaseCheck implements CheckInterface
{
    /**
     * checks if another option is already selected in the configuration
     * test fails if the other option is selected.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(
        Rule $rule,
        Configuration $configuration,
        DefaultConfiguration $defaultConfiguration
    ): CheckResult {
        $checkResult = new CheckResult();

        $ruleData = json_decode($rule->getData());
        $excludedOptionIdentifier = $ruleData->option_identifier;

        $excludedOptionFound = false;
        $switchOptions = [];

        if (!empty($excludedOptionIdentifier)) {
            // work on a copy of the configuration to prevent manipulation
            $workingConfiguration = unserialize(serialize($configuration));

            foreach ($workingConfiguration->optionclassifications as $component) {
                if (is_array($component->selectedoptions)) {
                    foreach ($component->selectedoptions as $selectedOption) {
                        if ($selectedOption->identifier == $excludedOptionIdentifier) {
                            $excludedOptionFound = true;
                            if (!isset($switchOptions[$component->identifier])) {
                                $switchOptions[$component->identifier] = [];
                            }

                            // if selecting the default option is not valid remove the currently selected option to resolve the conflict
                            $switchOptions = $this->tryDefaultOptionForComponent($component->identifier,
                                $workingConfiguration, $defaultConfiguration);

                            if (false === $switchOptions) {
                                $switchOptions[$component->identifier][] = [
                                    'identifier' => $excludedOptionIdentifier,
                                    'amount' => 0,
                                ];
                            }
                        }
                    }
                }
            }
        }

        $checkResult->status = !$excludedOptionFound;
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();

        $checkedOption = $rule->getOption();
        $checkedItemInformation = new CheckResultOptionInformation();
        $checkedItemInformation->identifier = $checkedOption->getIdentifier();
        $checkedItemInformation->title = $checkedOption->getTranslatedTitle();

        try {
            $checkedItemInformation->thumbnail = $this->getOptionThumbnailRepository()->findThumbnail($checkedOption)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }

        $checkResult->checked_option = $checkedItemInformation;

        // generate message if the required option was not found
        if (true === $excludedOptionFound) {
            $feedbackText = $this->getRulefeedbacktext($rule);
            $message = $feedbackText->getTranslatedRulefeedbacktext();

            $excludedItem = $this->getOptionRepository()->findOneByIdentifier($excludedOptionIdentifier);

            $excludedItemInformation = new CheckResultOptionInformation();
            $excludedItemInformation->identifier = $excludedItem->getIdentifier();
            $excludedItemInformation->title = $excludedItem->getTranslatedTitle();
            $excludedItemInformation->optionclassification_identifiers = array_keys($switchOptions);

            try {
                $excludedItemInformation->thumbnail = $this->getOptionThumbnailRepository()->findThumbnail($excludedItem)->getUrl();
            } catch (FileException $e) {
                // file can be non existent
            }

            $checkResult->conflicting_options = [$excludedItemInformation];

            $message = str_replace(
                ['[optionidentifier_excluded]', '[optionidentifier_selected]'],
                [$excludedItem->getTranslatedTitle(), $checkedOption->getIdentifier()],
                $message);

            $checkResult->message = $message;

            $data = [];
            $data['switchOptions'] = $switchOptions;

            $checkResult->data = $data;
        }

        return $checkResult;
    }
}
