<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Class AttributeValueGroupAutoSwitchCheck.
 *
 * Checks if all options have a matching value for the given attribute.
 * Options are considered per item or optionclassification, according to apply_per_option_classification.
 * Returns solutions (options to switch) to get the attribute values matching.
 *
 * @internal
 */
class AttributeValueGroupAutoSwitchCheck extends BaseCheck
{
    /**
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkResult = new CheckResult();

        $ruleData = json_decode($rule->getData());

        $attributeIdentifier = $ruleData->attribute_identifier;
        $applyPerOptionClassification = $ruleData->apply_per_option_classification ?? false;

        $optionIdentifierPools = [];
        foreach ($configuration->optionclassifications as $optionClassification) {
            if ($optionClassification->is_multiselect || !$applyPerOptionClassification) {
                if (is_array($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        $optionClassificationIdentifier = 'configuration';
                        if ($applyPerOptionClassification) {
                            $optionClassificationIdentifier = $optionClassification->identifier;
                        }
                        $optionIdentifierPools[$optionClassificationIdentifier][] = $selectedOption->identifier;
                    }
                }
            }
        }

        $checkResult->status = true;
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();
        $checkResult->checked_option = null;
        $data = [];

        foreach ($optionIdentifierPools as $optionPoolIdentifier => $optionIdentifiers) {
            // first execute a regular attribute compatibility check
            // if that check fails we have to find matching options in option pools to resolve that conflict
            $attributeCheckResult = $this->getOptionAttributeRepository()
                ->checkCommonAttributeValues($optionIdentifiers, $attributeIdentifier);

            // skip if common attribute value for option pool was found, otherwise generate message
            if ($attributeCheckResult->hasCommonValues) {
                continue;
            }

            $checkResult->status = false;

            // search in the failed options option pools for matching options to resolve this conflict
            $alternativeOptionMappingData = $this->getOptionRepository()
                ->getForAlternateOptionsMappingForAttributeValueGroup($optionIdentifiers, $attributeIdentifier);

            if (!empty($alternativeOptionMappingData)) {
                $alternativeOptionMapping = $this->buildAlternateOptionMapping($alternativeOptionMappingData,
                    $configuration);

                if (!empty($configuration->switchOptions)) {
                    foreach ($configuration->switchOptions as $switchOption) {
                        if ($switchOption instanceof \stdClass) {
                            $switchOption = [$switchOption];
                        }

                        foreach ($switchOption as $switchOptionElement) {
                            if (isset($alternativeOptionMapping[$switchOptionElement->identifier])) {
                                $data['switchOptions'] = $alternativeOptionMapping[$switchOptionElement->identifier]->switchOptions;
                            }
                        }
                    }

                    foreach ($data['switchOptions'] as $optionClassificationIdentifier => $switchOption) {
                        if ($applyPerOptionClassification && $optionPoolIdentifier !== $optionClassificationIdentifier) {
                            unset($data['switchOptions'][$optionClassificationIdentifier]);
                        }
                    }
                }

                $data['rulefeedback'] = $rule->getRulefeedback()->getIdentifier();
            }
        }

        $feedbackText = $this->getRulefeedbacktext($rule);
        $message = $feedbackText->getTranslatedRulefeedbacktext();

        $message = str_replace(
            ['[attribute]'],
            [$attributeIdentifier],
            $message);

        $checkResult->message = $message;
        $checkResult->data = $data;

        return $checkResult;
    }

    /**
     * build a mapping (tree) that can be used to resolve a failed AttributeValueGroupAutoSwitchCheck for the current configuration.
     *
     * @param array $alternativeOptionMappingData
     * @param   Configuration $configuration
     *
     * @return array
     */
    protected function buildAlternateOptionMapping($alternativeOptionMappingData, $configuration)
    {
        $alternateOptionMapping = [];

        $optionUsage = $this->getOptionUsageFromConfiguration($configuration);

        foreach ($alternativeOptionMappingData as $optionRow) {
            $optionItem = new \stdClass();
            $optionItem->identifier = $optionRow['optionIdentifier'];
            $optionItem->attributeValue = $optionRow['attributeValue'];
            $optionItem->switchOptions = [];

            foreach ($alternativeOptionMappingData as $mappingRow) {
                if ($optionRow['optionIdentifier'] != $mappingRow['optionIdentifier']) {
                    // options with the same attribute value are set to add
                    if ($optionRow['attributeValue'] != $mappingRow['attributeValue']) {
                        foreach ($optionUsage as $optionClassificationIdentifier => $usage) {
                            if (isset($usage[$mappingRow['optionIdentifier']])) {
                                if (!isset($optionItem->switchOptions[$optionClassificationIdentifier])) {
                                    $optionItem->switchOptions[$optionClassificationIdentifier] = [];
                                }

                                // remove option
                                $mappingItem = [];
                                $mappingItem['identifier'] = $mappingRow['optionIdentifier'];

                                $mappingItem['amount'] = 0;
                                $optionItem->switchOptions[$optionClassificationIdentifier][] = $mappingItem;

                                // add matching alternative with the remoed options original amount
                                $mappingItem = [];
                                $mappingItem['identifier'] = $mappingRow['alternateOptionIdentifier'];

                                $mappingItem['amount'] = $usage[$mappingRow['optionIdentifier']];
                                $optionItem->switchOptions[$optionClassificationIdentifier][] = $mappingItem;
                            }
                        }
                    }
                }
            }

            $alternateOptionMapping[$optionItem->identifier] = $optionItem;
        }

        return $alternateOptionMapping;
    }

    /**
     * get a list of options used in the current configuration.
     *
     * @param Configuration $configuration
     *
     * @return array
     */
    protected function getOptionUsageFromConfiguration($configuration)
    {
        $optionUsage = [];

        if (!empty($configuration->optionclassifications)) {
            foreach ($configuration->optionclassifications as $optionClassification) {
                if (!empty($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        $optionUsage[$optionClassification->identifier][$selectedOption->identifier] = $selectedOption->amount;
                    }
                }
            }
        }

        return $optionUsage;
    }
}
