<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as DefaultConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Checks\Check as BaseCheck;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\Interfaces\Check as CheckInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResult;

/**
 * Class DefaultOptionCheck.
 *
 * the rule holds an options that should be selected in the configuration.
 * this option is automatically selected as long as it not violates another rule.
 *
 * @internal
 */
class DefaultoptionCheck extends BaseCheck implements CheckInterface
{
    /**
     * the rule holds an options that should be selected in the configuration.
     * this option is automatically selected as long as it not violates another rule.
     *
     * @param Rule $rule
     * @param Configuration $configuration
     * @param DefaultConfiguration $defaultConfiguration
     *
     * @return  CheckResult
     *
     * @since   1.0.60
     *
     * @version 1.0.60
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     */
    public function execute(Rule $rule, Configuration $configuration, DefaultConfiguration $defaultConfiguration): CheckResult
    {
        $checkResult = new CheckResult();
        $checkResult->rule_type = $rule->getRuletype()->getIdentifier();
        $checkResult->checked_option = null;

        $ruleData = json_decode($rule->getData());
        $defaultOptionIdentifier = $ruleData->option_identifier;

        $defaultAlreadySelected = false;

        if (!empty($defaultOptionIdentifier)) {
            foreach ($configuration->optionclassifications as $optionClassification) {
                if (is_array($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        if ($selectedOption->identifier == $defaultOptionIdentifier) {
                            $defaultAlreadySelected = true;

                            break;
                        }
                    }
                }

                if (true === $defaultAlreadySelected) {
                    break;
                }
            }
        }

        $itemClassificationIdentifiers = $this->getOptionRepository()->getOptionClassificationIdentifiersByOptionIdentifierAndBaseItemIdentifier($defaultOptionIdentifier, $configuration->item->identifier);

        $data = [];
        $switchOptions = [];
        foreach ($itemClassificationIdentifiers as $itemClassificationIdentifier) {
            $switchOptions[$itemClassificationIdentifier][] = [
                'identifier' => $defaultOptionIdentifier,
                'amount' => 1,
            ];
        }

        $data['switchOptions'] = $switchOptions;

        $checkResult->data = $data;

        $checkResult->status = true;

        return $checkResult;
    }
}
