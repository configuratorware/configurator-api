<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rules;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\SwitchOptionEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Rules\RunChecksForRulesEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResults;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class CheckResolver.
 *
 * handles execution of (multiple) checks on item level or global
 *
 * @internal
 */
class CheckResolver
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var RuleRepository
     */
    private $ruleRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var RulesRetriever
     */
    private $rulesRetriever;

    /**
     * CheckResolver constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param RuleRepository $ruleRepository
     * @param OptionRepository $optionRepository
     * @param RulesRetriever $rulesRetriever
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        RuleRepository $ruleRepository,
        OptionRepository $optionRepository,
        RulesRetriever $rulesRetriever
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->ruleRepository = $ruleRepository;
        $this->optionRepository = $optionRepository;
        $this->rulesRetriever = $rulesRetriever;
    }

    /**
     * run checks for all options that shall be switched for a given configuration
     * and merge the results into one CheckResults object.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   array $switchOptions
     * @param   Configuration $configuration
     *
     * @return  CheckResults
     */
    public function runChecksForSwitchOptions($switchOptions, Configuration $configuration): CheckResults
    {
        $checkResults = new CheckResults();
        foreach ($switchOptions as $switchOption) {
            if ($switchOption instanceof \stdClass) {
                $switchOption = [$switchOption];
            }

            foreach ($switchOption as $switchOptionElement) {
                /** @var Option $option */
                $option = $this->optionRepository->findOneBy(['identifier' => $switchOptionElement->identifier]);
                $optionCheckResults = $this->executeItemChecks($option, $configuration, $switchOptions);

                if (empty($checkResults->status)) {
                    $checkResults = $optionCheckResults;
                } // merge results
                else {
                    $checkResults->status = false === $optionCheckResults->status ? $optionCheckResults->status : $checkResults->status;
                    $checkResults->check_results = array_merge(
                        $checkResults->check_results,
                        $optionCheckResults->check_results
                    );
                    $checkResults->configuration = $optionCheckResults->configuration;
                }
            }
        }

        return $checkResults;
    }

    /**
     * executes checks for one Item.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   Item|Option $entity
     * @param   Configuration $configuration
     * @param   array $switchOptions
     *
     * @return  CheckResults
     */
    public function executeItemChecks($entity, Configuration $configuration, $switchOptions = []): CheckResults
    {
        // add switchOptions information to configuration for some checks
        $configuration->switchOptions = $switchOptions;

        $rules = $this->rulesRetriever->getFilteredRules($entity, $configuration);

        $checkResults = $this->runChecksForRules($rules, $configuration);

        // remove switchOptions information after checks are done
        if (isset($checkResults->configuration->switchOptions)) {
            unset($checkResults->configuration->switchOptions);
        }

        return $checkResults;
    }

    /**
     * executes global checks for one Configuration.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param   Configuration $configuration
     *
     * @return  CheckResults
     */
    public function executeGlobalChecks(Configuration $configuration): CheckResults
    {
        // get all rules that have no item relation ordered by their sequence number
        $rules = $this->ruleRepository->getSortedGlobalRules();

        $checkResults = $this->runChecksForRules($rules, $configuration);

        return $checkResults;
    }

    /**
     * checks if one of the removed options is required by one of the selected options.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.27
     *
     * @version 1.0.27
     *
     * @param Configuration $configuration
     * @param string $optionIdentifier
     *
     * @return  CheckResults
     */
    public function reverseOptionDependencyCheck(Configuration $configuration, $optionIdentifier)
    {
        $selectedOptionIdentifiers = [];
        if (!empty($configuration->optionclassifications)) {
            foreach ($configuration->optionclassifications as $optionClassification) {
                if (!empty($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $selectedOption) {
                        $selectedOptionIdentifiers[] = $selectedOption->identifier;
                    }
                }
            }
        }

        // find rules by the configured identifier in the data field
        $reverseRules = $this->ruleRepository->getForReverseOptionDependencyCheck(
            $optionIdentifier,
            $selectedOptionIdentifiers
        );

        $rulesToCheck = [];
        if (!empty($reverseRules)) {
            /** @var Rule $reverseRule */
            foreach ($reverseRules as $reverseRule) {
                if (in_array($reverseRule->getOption()->getIdentifier(), $selectedOptionIdentifiers)) {
                    $rulesToCheck[$reverseRule->getOption()->getId()] = $reverseRule;
                }
            }
        }

        $checkResults = $this->runChecksForRules($rulesToCheck, $configuration, true);

        // if a reverse check fails but there is already a failed check in a option classification with no multiselect
        // remove the failed reverse check, it will be resolved by resolving the other failed check
        if (!empty($configuration->check_results->check_results) && !empty($checkResults->check_results)) {
            foreach ($checkResults->check_results as $i => $checkResult) {
                if (false == $checkResult->status) {
                    $optionClassification = key($checkResult->data['switchOptions']);
                    $optionClassificationMatch = false;

                    foreach ($configuration->check_results->check_results as $configurationCheckResult) {
                        if ('optiondependency' == $configurationCheckResult->rule_type && false == $configurationCheckResult->status) {
                            $configurationOptionClassification = key($checkResult->data['switchOptions']);

                            if ($configurationOptionClassification == $optionClassification) {
                                $optionClassificationMatch = true;
                            }
                        }
                    }

                    if (true === $optionClassificationMatch) {
                        foreach ($configuration->optionclassifications as $optionclassification) {
                            if ($optionclassification->identifier == $optionClassification && false == $optionclassification->is_multiselect) {
                                unset($checkResults->check_results[$i]);
                            }
                        }
                    }
                }
            }

            if (empty($checkResults->check_results)) {
                $checkResults->status = true;
            }
        }

        return $checkResults;
    }

    /**
     * get additional option rules for removed options
     * compare check results and add or remove additional options.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.63
     *
     * @version 1.0.63
     *
     * @param Configuration $configuration
     * @param array $removedOptions
     *
     * @return Configuration
     */
    public function handleAdditionalOptionRules($configuration, $removedOptions)
    {
        $additionalOptions = [];

        if (!empty($removedOptions)) {
            $rules = $this->ruleRepository->getAdditionalOptionsRules(
                $configuration->item->identifier,
                $removedOptions
            );

            /** @var Rule $rule */
            foreach ($rules as $rule) {
                $ruleData = json_decode($rule->getData());
                $additionalOptions[$ruleData->option_identifier] = 0;
            }
        }

        if (!empty($configuration->check_results->check_results)) {
            foreach ($configuration->check_results->check_results as $checkResult) {
                if ('additionaloption' === $checkResult->rule_type) {
                    // if the checked option is no longer in the configuration do not add the additional option
                    $checkedOptionFound = false;
                    foreach ($configuration->optionclassifications as $optionClassification) {
                        if (is_array($optionClassification->selectedoptions)) {
                            foreach ($optionClassification->selectedoptions as $selectedOption) {
                                if ($selectedOption->identifier == $checkResult->checked_option) {
                                    $checkedOptionFound = true;

                                    break;
                                }
                            }
                        }

                        if (true === $checkedOptionFound) {
                            break;
                        }
                    }

                    if (true === $checkedOptionFound) {
                        $additionalOptions[$checkResult->additional_option] = 1;
                    }
                }
            }
        }

        $switchOptions = [];
        foreach ($additionalOptions as $additionalOptionIdentifier => $amount) {
            $itemClassificationIdentifiers = $this->optionRepository->getOptionClassificationIdentifiersByOptionIdentifierAndBaseItemIdentifier(
                $additionalOptionIdentifier,
                $configuration->item->identifier
            );

            if (!empty($itemClassificationIdentifiers)) {
                $switchOption = new \stdClass();
                $switchOption->identifier = $additionalOptionIdentifier;
                $switchOption->amount = $amount;

                $switchOptions[$itemClassificationIdentifiers[0]] = $switchOption;
            }
        }

        // switch option, but keep check results
        if (!empty($switchOptions)) {
            $backupConfiguration = serialize($configuration);
            $backupConfiguration = unserialize($backupConfiguration);

            $event = new SwitchOptionEvent($configuration, $switchOptions);
            $this->eventDispatcher->dispatch($event, SwitchOptionEvent::NAME);
            $configuration = $event->getConfiguration();

            $configuration->check_results = $backupConfiguration->check_results;
        }

        return $configuration;
    }

    /**
     * check the min amount for an option classification a.
     *
     * @author  Michael Aichele <aichele@redhotmagma.de>
     *
     * @since   1.0.65
     *
     * @version 1.0.65
     *
     * @param   string $optionClassificationIdentifier
     * @param   string $baseItemIdentifier
     * @param   Configuration $configuration
     *
     * @return CheckResults
     */
    public function checkOptionClassificationMinAmount(
        $optionClassificationIdentifier,
        $baseItemIdentifier,
        $configuration
    ) {
        $checkResults = null;
        $rules = $this->ruleRepository->getOptionClassificationMinAmountRules(
            $configuration->item->identifier,
            $optionClassificationIdentifier
        );

        if (!empty($rules)) {
            $checkResults = $this->runChecksForRules($rules, $configuration);
            unset($checkResults->configuration);
        }

        return $checkResults;
    }

    /**
     * run checks for a given set of rules.
     *
     * @param   array $rules
     * @param   $configuration
     * @param   bool $reverse
     *
     * @return  CheckResults
     */
    protected function runChecksForRules(array $rules, $configuration, $reverse = false): CheckResults
    {
        $event = new RunChecksForRulesEvent($configuration, $rules, $reverse);
        $this->eventDispatcher->dispatch($event, RunChecksForRulesEvent::NAME);
        $checkResults = $event->getCheckResults();

        return $checkResults;
    }
}
