<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignViewDesignArea;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ViewRelationInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewDesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;
use SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class BaseShape2dPositionCalculation
{
    /**
     * @var DesignViewDesignAreaRepository
     */
    private $designViewDesignAreaRepository;

    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var MediaHelper
     */
    private $mediaHelper;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * BaseShape2dPositionCalculation constructor.
     *
     * @param DesignViewDesignAreaRepository $designViewDesignAreaRepository
     * @param SettingRepository $settingRepository
     * @param Filesystem $filesystem
     * @param MediaHelper $mediaHelper
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        DesignViewDesignAreaRepository $designViewDesignAreaRepository,
        SettingRepository $settingRepository,
        Filesystem $filesystem,
        MediaHelper $mediaHelper,
        ItemRepository $itemRepository
    ) {
        $this->designViewDesignAreaRepository = $designViewDesignAreaRepository;
        $this->settingRepository = $settingRepository;
        $this->filesystem = $filesystem;
        $this->mediaHelper = $mediaHelper;
        $this->itemRepository = $itemRepository;
    }

    /**
     * if the coordinates for the base shape are not set yet and the item is in 2d mode
     * we can calculate the base shape position and size from the position data.
     *
     * @param ViewRelationInterface $viewDesignAreaRelation
     *
     * @return ViewRelationInterface
     */
    public function calculateBaseShapePosition(ViewRelationInterface $viewDesignAreaRelation)
    {
        $baseShape = $viewDesignAreaRelation->getBaseShape();

        if (!empty($baseShape)
            && $viewDesignAreaRelation->getView()->getItem()
            && !isset($baseShape->x)) {
            $item = $viewDesignAreaRelation->getView()->getItem();
            if (VisualizationMode::is2dVariantVisualization($this->getItemVisualizationMode($item))) {
                $path = $this->mediaHelper->getImageBasePath() . '/item/view/' . $item->getIdentifier();
                $imageBasePath = $path . '/' . $viewDesignAreaRelation->getView()->getIdentifier();

                if (is_dir($imageBasePath)) {
                    $imagePath = $this->getFullImagePath($imageBasePath, $item->getIdentifier());
                    if ($imagePath) {
                        $imageSize = getimagesize($imagePath);

                        return $this->updateBaseShape($viewDesignAreaRelation, $imageSize);
                    }

                    // check children until the first image is found
                    $children = $this->itemRepository->getItemsByParent($item->getIdentifier());
                    foreach ((array)$children as $child) {
                        $imagePath = $this->getFullImagePath($imageBasePath, $child->getIdentifier());
                        if ($imagePath) {
                            $imageSize = getimagesize($imagePath);

                            return $this->updateBaseShape($viewDesignAreaRelation, $imageSize);
                        }
                    }
                }
            }
        }

        return $viewDesignAreaRelation;
    }

    /**
     * search the file name for a path
     * since the extension can vary we search for all images with the given identifier as base name and take the newest one.
     *
     * @param string $imageBasePath
     * @param string $identifier
     *
     * @return string|null
     */
    private function getFullImagePath(string $imageBasePath, string $identifier): ?string
    {
        $finder = new Finder();
        $images = $finder->files()->in($imageBasePath)->name($identifier . '.*');

        if (count($images)) {
            $images->sort(
                function (SplFileInfo $a, SplFileInfo $b) {
                    return $a->getCTime() < $b->getCTime();
                }
            );

            $image = $images->getIterator()->current();

            return $image->getPathname();
        }

        return null;
    }

    /**
     * @param ViewRelationInterface $viewDesignAreaRelation
     * @param array $imageSize
     *
     * @return ViewRelationInterface
     */
    private function updateBaseShape(ViewRelationInterface $viewDesignAreaRelation, array $imageSize)
    {
        $baseShape = $viewDesignAreaRelation->getBaseShape();
        $position = $viewDesignAreaRelation->getPosition();

        $imageWidth = $imageSize[0];
        $imageHeight = $imageSize[1];

        $baseShape->x = $position->x - $imageWidth / 2;
        $baseShape->y = $imageHeight / 2 - $position->y;
        $baseShape->width = $position->width;
        $baseShape->height = $position->height;

        $position->x = $position->width / 2;
        $position->y = $position->height / 2;

        $viewDesignAreaRelation->setBaseShape($baseShape);
        $viewDesignAreaRelation->setPosition($position);
        $this->designViewDesignAreaRepository->save($viewDesignAreaRelation);

        return $viewDesignAreaRelation;
    }

    /**
     * @param Item $item
     *
     * @return string
     */
    private function getItemVisualizationMode(Item $item): ?string
    {
        return null !== $item->getVisualizationMode() ? $item->getVisualizationMode()->getIdentifier() : null;
    }
}
