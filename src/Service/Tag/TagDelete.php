<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Tag;

use Redhotmagma\ConfiguratorApiBundle\Entity\Tag;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\TagRepository;

/**
 * @internal
 */
class TagDelete
{
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * TagDelete constructor.
     *
     * @param TagRepository $tagRepository
     */
    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Tag $entity */
            $entity = $this->tagRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            if (!empty($entity->getTagTranslation()->toArray())) {
                foreach ($entity->getTagTranslation() as $tagTranslation) {
                    $this->tagRepository->delete($tagTranslation);
                }
            }

            $this->tagRepository->delete($entity);
        }
    }
}
