<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Tag;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Tag;
use Redhotmagma\ConfiguratorApiBundle\Structure\Tagtranslation;

/**
 * @internal
 */
class TagStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Tag $entity
     * @param string $structureClassName
     *
     * @return Tag
     */
    public function convertOne($entity, $structureClassName = Tag::class)
    {
        /** @var Tag $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addTagTranslationsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Tag::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param Tag $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Tag $entity
     *
     * @return mixed
     */
    protected function addTagTranslationsToStructure($structure, $entity)
    {
        $translations = [];

        $tagTranslations = $entity->getTagTranslation();

        if (!empty($tagTranslations)) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\TagTranslation $tagTranslation */
            foreach ($tagTranslations as $tagTranslation) {
                $tagTranslationStructure = $this->structureFromEntityConverter->convertOne($tagTranslation,
                    Tagtranslation::class);

                if (!empty($tagTranslationStructure)) {
                    $tagTranslationStructure->language = $tagTranslation->getLanguage()
                        ->getIso();
                    $translations[] = $tagTranslationStructure;
                }
            }
        }

        $structure->translations = $translations;

        return $structure;
    }
}
