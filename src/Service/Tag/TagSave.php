<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Tag;

use Redhotmagma\ConfiguratorApiBundle\Entity\Tag;
use Redhotmagma\ConfiguratorApiBundle\Repository\TagRepository;

/**
 * @internal
 */
class TagSave
{
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var TagEntityFromStructureConverter
     */
    private $tagEntityFromStructureConverter;

    /**
     * @var TagStructureFromEntityConverter
     */
    private $tagStructureFromEntityConverter;

    /**
     * @param TagRepository $optionPoolRepository
     * @param TagEntityFromStructureConverter $optionPoolEntityFromStructureConverter
     * @param TagStructureFromEntityConverter $optionPoolStructureFromEntityConverter
     */
    public function __construct(
        TagRepository $optionPoolRepository,
        TagEntityFromStructureConverter $optionPoolEntityFromStructureConverter,
        TagStructureFromEntityConverter $optionPoolStructureFromEntityConverter
    ) {
        $this->tagRepository = $optionPoolRepository;
        $this->tagEntityFromStructureConverter = $optionPoolEntityFromStructureConverter;
        $this->tagStructureFromEntityConverter = $optionPoolStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Tag $tag
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Tag
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Tag $tag)
    {
        $entity = null;
        if (isset($tag->id) && $tag->id > 0) {
            $entity = $this->tagRepository->findOneBy(['id' => $tag->id]);
        }
        $entity = $this->tagEntityFromStructureConverter->convertOne($tag, $entity);
        $entity = $this->tagRepository->save($entity);

        /** @var Tag $entity */
        $entity = $this->tagRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->tagStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
