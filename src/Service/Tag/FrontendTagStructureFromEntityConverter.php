<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Tag;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Tag;

/**
 * @internal
 */
class FrontendTagStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * FrontendTagStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Tag $entity
     * @param string $structureClassName
     *
     * @return Tag
     */
    public function convertOne($entity, $structureClassName = Tag::class)
    {
        /** @var Tag $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure->title = $entity->getTranslatedTitle();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Tag::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
