<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Tag;

use Redhotmagma\ConfiguratorApiBundle\Entity\Tag;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\TagRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

class TagApi
{
    /**
     * @var TagRepository
     */
    private $tagRepository;

    /**
     * @var TagStructureFromEntityConverter
     */
    private $tagStructureFromEntityConverter;

    /**
     * @var TagSave
     */
    private $tagSave;

    /**
     * @var TagDelete
     */
    private $tagDelete;

    /**
     * TagApi constructor.
     *
     * @param TagRepository $tagRepository
     * @param TagStructureFromEntityConverter $tagStructureFromEntityConverter
     * @param TagSave $tagSave
     * @param TagDelete $tagDelete
     */
    public function __construct(
        TagRepository $tagRepository,
        TagStructureFromEntityConverter $tagStructureFromEntityConverter,
        TagSave $tagSave,
        TagDelete $tagDelete
    ) {
        $this->tagRepository = $tagRepository;
        $this->tagStructureFromEntityConverter = $tagStructureFromEntityConverter;
        $this->tagSave = $tagSave;
        $this->tagDelete = $tagDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->tagRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->tagStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->tagRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Tag
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Tag
    {
        /** @var Tag $entity */
        $entity = $this->tagRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->tagStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Tag $tag
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Tag
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Tag $tag)
    {
        $structure = $this->tagSave->save($tag);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->tagDelete->delete($id);
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments): void
    {
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            $entity = $this->tagRepository->findOneBy(['id' => $sequenceNumberData->id]);
            if ($entity) {
                $entity->setSequenceNumber($sequenceNumberData->sequencenumber);
                $this->tagRepository->save($entity, false);
            }
        }

        $this->tagRepository->flush();
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['tagTranslation.translation'];
    }
}
