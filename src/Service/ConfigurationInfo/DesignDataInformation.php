<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationInfo;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationInfo;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignDataInfo;

/**
 * @internal
 */
class DesignDataInformation
{
    /**
     * @param \stdClass $designData
     *
     * @return array
     */
    public function parseDesignData(?\stdClass $designData): array
    {
        $parsedDesignData = [];
        if (!empty($designData)) {
            foreach ($designData as $designAreaIdentifier => $designAreaData) {
                if (empty($designAreaData->canvasData)) {
                    continue;
                }

                $designDataInfo = new DesignDataInfo();
                $designDataInfo->designAreaIdentifier = $designAreaIdentifier;
                $designDataInfo->designProductionMethodIdentifier = $designAreaData->designProductionMethodIdentifier;
                if (isset($designAreaData->colorAmount)) {
                    $designDataInfo->colorAmount = $designAreaData->colorAmount;
                }

                foreach ($designAreaData->canvasData->objects as $canvasData) {
                    if ('Text' === $canvasData->type) {
                        $textData = $this->handleText($canvasData);
                        $designDataInfo->texts[] = $textData;
                    } else {
                        if ('Image' === $canvasData->type) {
                            $imageData = $this->handleImage($canvasData);
                            $designDataInfo->images[] = $imageData;
                        }
                    }
                }

                $parsedDesignData[] = $designDataInfo;
            }
        }

        return $parsedDesignData;
    }

    /**
     * @param ConfigurationInfo $configurationInfo
     * @param Configuration $configurationData
     *
     * @return ConfigurationInfo
     */
    public function addDesignDataToConfigurationInfo(
        ConfigurationInfo $configurationInfo,
        Configuration $configurationData
    ): ConfigurationInfo {
        $designData = $this->parseDesignData($configurationData->designdata);

        $configurationInfo->designData[] = $designData;

        return $configurationInfo;
    }

    /**
     * @param \stdClass $canvasData
     *
     * @return \stdClass
     */
    private function handleText(\stdClass $canvasData)
    {
        $textData = new \stdClass();
        $textData->text = strip_tags($canvasData->content);
        $textData->fontSize = $canvasData->style->fontSize;
        $textData->fontFamily = $canvasData->style->fontFamily;
        $textData->customFont = false;
        $textData->isBulkName = $canvasData->isBulkName ?? false;

        if (isset($canvasData->fontItem, $canvasData->fontItem->isCustomFont, $canvasData->fontItem->customName) && true === $canvasData->fontItem->isCustomFont) {
            $textData->customFont = true;
            $textData->fontFamily = $canvasData->fontItem->customName;
        }

        $textData->fontWeight = $canvasData->style->fontWeight;
        $textData->fontStyle = $canvasData->style->fontStyle;
        $textData->colors = $canvasData->style->colors;

        return $textData;
    }

    /**
     * @param \stdClass $canvasData
     *
     * @return \stdClass
     */
    private function handleImage(\stdClass $canvasData)
    {
        $imageData = new \stdClass();
        $imageData->image = $canvasData->src;

        return $imageData;
    }
}
