<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationInfo;

use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationImages;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationInfo;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class ConfigurationStructureToConfigurationInfoConverter
{
    private ConfigurationImages $configurationImages;

    private DesignDataInformation $designDataInformation;

    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ConfigurationImages $configurationImages,
        DesignDataInformation $designDataInformation,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->configurationImages = $configurationImages;
        $this->designDataInformation = $designDataInformation;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function convert(Configuration $configuration, bool $extendedData): ConfigurationInfo
    {
        $configurationInfo = new ConfigurationInfo();
        $configurationInfo->code = $configuration->code;
        $configurationInfo->name = $configuration->name;
        $configurationInfo->item = $extendedData ? $configuration->item : $configuration->item->title;
        $configurationInfo->itemIdentifier = $configuration->item->identifier;
        $configurationInfo->date_created = $configuration->date_created;
        $configurationInfo->finderResult = $configuration->finderResult;

        // add custom data
        if (!empty($configuration->customdata)) {
            $configurationInfo->customdata = $configuration->customdata;
        }

        // add parentCode if it was saved to custom data (e.g. due to frontend parameter "_addparentcodeonsave")
        if (!empty($configuration->customdata->parentCode)) {
            $configurationInfo->parentCode = $configuration->customdata->parentCode;
        }

        // recalculate price
        $configurationInfo = $this->addCalculationToConfigurationInfo($configurationInfo, $configuration);

        // add image data
        $configurationInfo = $this->addImageDataToConfigurationInfo($configurationInfo);

        // add option classification
        $configurationInfo = $this->addOptionClassificationsToConfigurationInfo($configurationInfo, $configuration);

        $configurationInfo = $this->designDataInformation->addDesignDataToConfigurationInfo(
            $configurationInfo,
            $configuration
        );

        return $configurationInfo;
    }

    protected function addCalculationToConfigurationInfo(
        ConfigurationInfo $configurationInfo,
        Configuration $configurationData
    ): ConfigurationInfo {
        $event = new CalculateEvent($configurationData);
        $this->eventDispatcher->dispatch($event, CalculateEvent::NAME);
        $calculationResult = $event->getCalculationResult();

        $configurationInfo->calculation = $calculationResult;

        return $configurationInfo;
    }

    protected function addImageDataToConfigurationInfo(ConfigurationInfo $configurationInfo): ConfigurationInfo
    {
        $images = $this->configurationImages->getImagesForConfiguration($configurationInfo->code);
        $configurationInfo->images = $images;

        $printImages = $this->configurationImages->getImagesForConfiguration($configurationInfo->code, 'printfiles');
        $configurationInfo->printImages = $printImages;

        return $configurationInfo;
    }

    protected function addOptionClassificationsToConfigurationInfo(
        ConfigurationInfo $configurationInfo,
        Configuration $configurationData
    ): ConfigurationInfo {
        $optionClassifications = [];
        foreach ($configurationData->optionclassifications as $optionClassification) {
            $selectedOptions = [];
            if (is_array($optionClassification->selectedoptions)) {
                foreach ($optionClassification->selectedoptions as $selectedOption) {
                    $selectedOptionInfo = new \stdClass();
                    $selectedOptionInfo->identifier = $selectedOption->identifier;
                    $selectedOptionInfo->title = $selectedOption->title;

                    $selectedAmount = 1;
                    if (isset($selectedOption->amount)) {
                        $selectedAmount = $selectedOption->amount;
                    }
                    if (!empty($selectedOption->inputText)) {
                        $selectedOptionInfo->inputText = $selectedOption->inputText;
                    }
                    $selectedOptionInfo->amount = $selectedAmount;

                    $selectedOptions[] = $selectedOptionInfo;
                }
            }

            $optionClassificationInfo = new \stdClass();
            $optionClassificationInfo->identifier = $optionClassification->identifier;
            $optionClassificationInfo->title = $optionClassification->title;
            $optionClassificationInfo->selectedoptions = $selectedOptions;

            if (!empty($selectedOptions)) {
                $optionClassifications[] = $optionClassificationInfo;
            }
        }
        $configurationInfo->optionclassifications = $optionClassifications;

        return $configurationInfo;
    }
}
