<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationInfo;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationInfo\GetConfigurationInfoEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationInfo\GetProductionInfoEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationInfo;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ConfigurationInfoApi
{
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getConfigurationInfo(string $configurationCode, bool $extendedData = false): ConfigurationInfo
    {
        $event = new GetConfigurationInfoEvent($configurationCode);
        $event->setExtendedData($extendedData);
        $this->eventDispatcher->dispatch($event, GetConfigurationInfoEvent::NAME);
        $configurationInfo = $event->getConfigurationInfo();

        return $configurationInfo;
    }

    public function getProductionInfo(string $configurationCode): ConfigurationInfo
    {
        $event = new GetProductionInfoEvent($configurationCode);
        $this->eventDispatcher->dispatch($event, GetProductionInfoEvent::NAME);
        $productionInfo = $event->getConfigurationInfo();

        return $productionInfo;
    }
}
