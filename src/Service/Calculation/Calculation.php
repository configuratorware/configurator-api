<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
interface Calculation
{
    /**
     * calculate values of the given configuration.
     *
     * @param Configuration $configuration
     *
     * @return CalculationResult
     */
    public function calculate(Configuration $configuration);
}
