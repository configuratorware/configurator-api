<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\ConfigurationFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\ConfigurationModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\DesignAreaFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage\FrontendImageGalleryImageStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as ConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage;

/**
 * @internal
 */
class ConfigurationProvider
{
    private const CANVAS_TYPE_IMAGE = 'Image';
    private const USER_IMAGE_TYPE = 'USER_IMAGE_TYPE';
    private const GALLERY_IMAGE_TYPE = 'GALLERY_IMAGE_TYPE';
    private const CANVAS_TYPE_TEXT = 'Text';

    private ItemRepository $itemRepository;

    private ImageGalleryImageRepository $imageGalleryImageRepository;

    private FrontendImageGalleryImageStructureFromEntityConverter $frontendImageGalleryImageStructureFromEntityConverter;

    public function __construct(
        ItemRepository $itemRepository,
        ImageGalleryImageRepository $imageGalleryImageRepository,
        FrontendImageGalleryImageStructureFromEntityConverter $frontendImageGalleryImageStructureFromEntityConverter
    ) {
        $this->itemRepository = $itemRepository;
        $this->imageGalleryImageRepository = $imageGalleryImageRepository;
        $this->frontendImageGalleryImageStructureFromEntityConverter = $frontendImageGalleryImageStructureFromEntityConverter;
    }

    /**
     * @param ConfigurationStructure $configurationStructure
     *
     * @return ConfigurationModel
     */
    public function getConfiguration(ConfigurationStructure $configurationStructure): ConfigurationModel
    {
        $item = $this->itemRepository->findOneByIdentifier($configurationStructure->item->identifier);
        if (null !== $item->getParent()) {
            $item = $item->getParent();
        }

        $designAreas = $this->getDesignAreas($configurationStructure, $item);

        $imageDataTypes = $this->findUsedImageDataTypes($configurationStructure);
        $imageGalleryImage = $this->getImageGalleryImage($configurationStructure);
        $userSelectedCalculation = new \stdClass();
        if (isset($configurationStructure->customdata)
            && isset($configurationStructure->customdata->userSelectedCalculation)
            && !empty($configurationStructure->customdata->userSelectedCalculation)) {
            $userSelectedCalculation = $configurationStructure->customdata->userSelectedCalculation;
        }

        $totalAmount = 1;
        if (isset($configurationStructure->customdata)
            && isset($configurationStructure->customdata->selectedAmounts)
            && !empty($configurationStructure->customdata->selectedAmounts)) {
            $calculatedAmount = 0;
            foreach ($configurationStructure->customdata->selectedAmounts as $itemIdentifier => $itemAmount) {
                $calculatedAmount += $itemAmount;
            }

            if ($calculatedAmount > 0) {
                $totalAmount = $calculatedAmount;
            }
        }

        $bulkNamesPresent = $this->checkBulkNamesPresent($configurationStructure);

        return ConfigurationFactory::create(
            $configurationStructure->item->identifier,
            $totalAmount,
            $designAreas,
            $item,
            $userSelectedCalculation,
            $bulkNamesPresent,
            $imageGalleryImage,
            in_array(self::USER_IMAGE_TYPE, $imageDataTypes, true),
            in_array(self::GALLERY_IMAGE_TYPE, $imageDataTypes, true)
        );
    }

    /**
     * @param ConfigurationStructure $configurationStructure
     * @param Item $item
     *
     * @return array
     */
    protected function getDesignAreas(ConfigurationStructure $configurationStructure, Item $item): array
    {
        $designAreas = [];

        /* @var \StdClass $area */
        if (!empty($configurationStructure->designdata)) {
            foreach ($configurationStructure->designdata as $designAreaIdentifier => $designData) {
                // only calculate prices if the design production method is known and the design area has elements
                if (isset($designData->designProductionMethodIdentifier)
                    && !empty($designData->canvasData)
                    && !empty($designData->canvasData->objects)) {
                    // default to one color
                    $colorAmount = $designData->colorAmount ?? 1;

                    $designArea = DesignAreaFactory::create(
                        $designAreaIdentifier,
                        $designData->designProductionMethodIdentifier,
                        $colorAmount,
                        $item
                    );

                    $designAreas[] = $designArea;
                }
            }
        }

        return $designAreas;
    }

    /**
     * @return string[]
     */
    protected function findUsedImageDataTypes(ConfigurationStructure $configurationStructure): array
    {
        $imageTypes = [];
        if (is_object($configurationStructure->designdata)) {
            foreach ($configurationStructure->designdata as $designData) {
                if (isset($designData->canvasData->objects) && is_iterable($designData->canvasData->objects)) {
                    foreach ($designData->canvasData->objects as $canvasObject) {
                        if (self::CANVAS_TYPE_IMAGE === $canvasObject->type) {
                            if (isset($canvasObject->imageData, $canvasObject->imageData->gallery) && $canvasObject->imageData && $canvasObject->imageData->gallery) {
                                $imageTypes[] = self::GALLERY_IMAGE_TYPE;
                            } else {
                                $imageTypes[] = self::USER_IMAGE_TYPE;
                            }
                        }
                    }
                }
            }
        }

        return array_unique($imageTypes);
    }

    /**
     * Get canvas element containing image data
     * this is used for calculation types that are image data dependent.
     *
     * @param ConfigurationStructure $configurationStructure
     *
     * @return ImageGalleryImage[]
     */
    protected function getImageGalleryImage(ConfigurationStructure $configurationStructure): array
    {
        if (!is_object($configurationStructure->designdata)) {
            return [];
        }

        $imageGalleryImages = [];
        foreach ($configurationStructure->designdata as $designData) {
            if (isset($designData->canvasData->objects) && is_iterable($designData->canvasData->objects)) {
                foreach ($designData->canvasData->objects as $canvasObject) {
                    if (self::CANVAS_TYPE_IMAGE === $canvasObject->type && null !== $canvasObject->src) {
                        $imageGalleryImage = $this->imageGalleryImageRepository->findOneByImagename(basename($canvasObject->src));
                        if ($imageGalleryImage) {
                            $imageGalleryImages[] = $this->frontendImageGalleryImageStructureFromEntityConverter->convertOne($imageGalleryImage);
                        }
                    }
                }
            }
        }

        return $imageGalleryImages;
    }

    /**
     * Check if a Text object with bulk names is present in the configuration
     * used for bulk name dependent calculation types.
     *
     * @param ConfigurationStructure $configurationStructure
     *
     * @return bool
     */
    private function checkBulkNamesPresent(ConfigurationStructure $configurationStructure): bool
    {
        if (is_object($configurationStructure->designdata)) {
            foreach ($configurationStructure->designdata as $designData) {
                if (isset($designData->canvasData->objects) && is_iterable($designData->canvasData->objects)) {
                    foreach ($designData->canvasData->objects as $canvasObject) {
                        if (self::CANVAS_TYPE_TEXT === $canvasObject->type
                            && isset($canvasObject->isBulkName) && true === $canvasObject->isBulkName
                        ) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }
}
