<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ImageGalleryCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\CalculationTypeApiInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\ConfigurationModel;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage;

class ImageGalleryCalculationTypeApi implements CalculationTypeApiInterface
{
    private const CALCULATION_TYPE_IDENTIFIER = 'gallery_image';

    /**
     * @return CalculationTypePriceModel[]
     */
    public function getCalculationTypePrices(ConfigurationModel $configuration): array
    {
        $calculationTypePrices = [];
        foreach ($configuration->getImageGalleryImage() as $imageGalleryImage) {
            $calculationTypePrices[] = $this->createCalculationTypePrice($imageGalleryImage);
        }

        return $calculationTypePrices;
    }

    protected function createCalculationTypePrice(ImageGalleryImage $imageGalleryImage): CalculationTypePriceModel
    {
        return CalculationTypePriceFactory::create(
            $imageGalleryImage->title,
            self::CALCULATION_TYPE_IDENTIFIER,
            true,
            true,
            false,
            true,
            $imageGalleryImage->price ?? 0.0,
            $imageGalleryImage->netPrice ?? 0.0
        );
    }
}
