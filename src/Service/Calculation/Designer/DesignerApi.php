<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType\GlobalCalculationTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ImageGalleryCalculationType\ImageGalleryCalculationTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType\ProductionCalculationTypeApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult as CalculationResultStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResultPosition as CalculationResultPositionStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as ConfigurationStructure;

class DesignerApi
{
    /**
     * @var ConfigurationProvider
     */
    private ConfigurationProvider $configurationProvider;

    /**
     * @var GlobalCalculationTypeApi
     */
    private GlobalCalculationTypeApi $globalCalculationTypeApi;

    /**
     * @var FormatterService
     */
    private FormatterService $formatterService;

    /**
     * @var ItemRepository
     */
    private ItemRepository $itemRepository;

    /**
     * @var PositionStructureFromDataConverter
     */
    private PositionStructureFromDataConverter $positionStructureFromDataConverter;

    /**
     * @var ProductionCalculationTypeApi
     */
    private ProductionCalculationTypeApi $productionCalculationTypeApi;

    /**
     * @var ImageGalleryCalculationTypeApi
     */
    private ImageGalleryCalculationTypeApi $imageGalleryCalculationTypeApi;

    /**
     * DesignerApi constructor.
     *
     * @param ConfigurationProvider $configurationProvider
     * @param GlobalCalculationTypeApi $globalCalculationTypeApi
     * @param FormatterService $formatterService
     * @param ItemRepository $itemRepository
     * @param ProductionCalculationTypeApi $productionCalculationTypeApi
     * @param PositionStructureFromDataConverter $positionStructureFromDataConverter
     * @param ImageGalleryCalculationTypeApi $imageGalleryCalculationTypeApi
     */
    public function __construct(
        ConfigurationProvider $configurationProvider,
        GlobalCalculationTypeApi $globalCalculationTypeApi,
        FormatterService $formatterService,
        ItemRepository $itemRepository,
        ProductionCalculationTypeApi $productionCalculationTypeApi,
        PositionStructureFromDataConverter $positionStructureFromDataConverter,
        ImageGalleryCalculationTypeApi $imageGalleryCalculationTypeApi
    ) {
        $this->configurationProvider = $configurationProvider;
        $this->globalCalculationTypeApi = $globalCalculationTypeApi;
        $this->formatterService = $formatterService;
        $this->itemRepository = $itemRepository;
        $this->productionCalculationTypeApi = $productionCalculationTypeApi;
        $this->positionStructureFromDataConverter = $positionStructureFromDataConverter;
        $this->imageGalleryCalculationTypeApi = $imageGalleryCalculationTypeApi;
    }

    /**
     * @param CalculationResultStructure $calculationResultStructure
     * @param ConfigurationStructure $configurationStructure
     * @param array $result
     *
     * @return CalculationTypePriceModel[]
     */
    public function fakeItemPosition(
        CalculationResultStructure $calculationResultStructure,
        ConfigurationStructure $configurationStructure,
        array $result = []
    ): array {
        $item = $this->itemRepository->findOneByIdentifier($configurationStructure->item->identifier);
        if (null !== $item->getParent()) {
            $item = $item->getParent();
        }

        $calculationTypePrice = CalculationTypePriceFactory::create(
            $item->getTranslatedTitle(),
            'item',
            true,
            true,
            false,
            true,
            $calculationResultStructure->total,
            $calculationResultStructure->netTotal
        );

        $result[] = $calculationTypePrice;

        return $result;
    }

    /**
     * @param ConfigurationStructure $configurationStructure
     * @param CalculationTypePriceModel[] $result
     *
     * @return CalculationTypePriceModel[]
     */
    public function getAllCalculationTypePrices(ConfigurationStructure $configurationStructure, array $result = []): array
    {
        $configuration = $this->configurationProvider->getConfiguration($configurationStructure);

        return array_merge(
            $result,
            $this->productionCalculationTypeApi->getCalculationTypePrices($configuration),
            $this->globalCalculationTypeApi->getCalculationTypePrices($configuration),
            $this->imageGalleryCalculationTypeApi->getCalculationTypePrices($configuration)
        );
    }

    /**
     * @param CalculationTypePriceModel[] $designerCalculationResult
     */
    public function addPositionsToStructure(
        CalculationResultStructure $calculationResultStructure,
        array $designerCalculationResult
    ): CalculationResultStructure {
        $calculationResultStructure->positions = $this->positionStructureFromDataConverter->convertMany(
            $designerCalculationResult,
            CalculationResultPositionStructure::class
        );

        return $calculationResultStructure;
    }

    /**
     * @param $calculationResultStructure
     * @param $designerCalculationResult
     *
     * @return CalculationResultStructure
     */
    public function updateTotals(
        $calculationResultStructure,
        $designerCalculationResult
    ): CalculationResultStructure {
        $total = 0;
        $netTotal = 0;

        /** @var CalculationTypePriceModel $prices */
        foreach ($designerCalculationResult as $prices) {
            if (!(true === $prices->isSelectableByUser() &&
                false === $prices->isSelected())) {
                $total += $prices->getPrice();
                $netTotal += $prices->getPriceNet();
            }
        }

        $total = round($total, 10);
        $calculationResultStructure->total = $total;
        $calculationResultStructure->totalFormatted = $this->formatterService->formatPrice($total);

        $netTotal = round($netTotal, 10);
        $calculationResultStructure->netTotal = $netTotal;
        $calculationResultStructure->netTotalFormatted = $this->formatterService->formatPrice($netTotal);

        if ($total > 0 && $calculationResultStructure->vatRate > 0) {
            $netTotalFromGrossTotal = round($total / (100 + $calculationResultStructure->vatRate) * 100, 10);
            $calculationResultStructure->netTotalFromGrossTotal = $netTotalFromGrossTotal;
            $calculationResultStructure->netTotalFromGrossTotalFormatted = $this->formatterService->formatPrice($netTotalFromGrossTotal);
        }

        return $calculationResultStructure;
    }
}
