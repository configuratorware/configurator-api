<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer;

use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\ConfigurationModel;

/**
 * @internal
 */
interface CalculationTypeApiInterface
{
    public function getCalculationTypePrices(ConfigurationModel $configuration): array;
}
