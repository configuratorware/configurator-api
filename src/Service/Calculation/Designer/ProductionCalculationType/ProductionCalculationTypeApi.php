<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerProductionCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\CalculationTypeApiInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\ConfigurationModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\DesignAreaModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\VatCalculationService;

class ProductionCalculationTypeApi implements CalculationTypeApiInterface
{
    private Calculator $calculator;

    private DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository;

    private ItemPriceStrategy $itemPriceStrategy;

    private PriceStrategy $priceStrategy;

    private VatCalculationService $vatCalculationService;

    public function __construct(
        Calculator $calculator,
        DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository,
        ItemPriceStrategy $itemPriceStrategy,
        PriceStrategy $priceStrategy,
        VatCalculationService $vatCalculationService
    ) {
        $this->calculator = $calculator;
        $this->designerProductionCalculationTypeRepository = $designerProductionCalculationTypeRepository;
        $this->itemPriceStrategy = $itemPriceStrategy;
        $this->priceStrategy = $priceStrategy;
        $this->vatCalculationService = $vatCalculationService;
    }

    /**
     * @return CalculationTypePriceModel[]
     */
    public function getCalculationTypePrices(ConfigurationModel $configuration): array
    {
        $calculationTypePrices = [];

        /** @var DesignAreaModel $designArea */
        foreach ($configuration->getDesignAreas() as $designArea) {
            $calculationTypes = $this->designerProductionCalculationTypeRepository
                ->findByDesignProductionMethodIdentifier(
                    $designArea->getDesignProductionMethodIdentifier()
                );

            /** @var DesignerProductionCalculationType $calculationType */
            foreach ($calculationTypes as $calculationType) {
                if (true === $calculationType->getBulkNameDependent()
                    && true !== $configuration->getBulkNamesPresent()
                ) {
                    continue;
                }

                if (true === $calculationType->getItemAmountDependent()) {
                    $strategy = $this->itemPriceStrategy;
                } else {
                    $strategy = $this->priceStrategy;
                }

                $pricesByColorAmount = $strategy->providePrices(
                    $calculationType,
                    $configuration->getItemAmount(),
                    $designArea
                );

                if ($pricesByColorAmount) {
                    // ensure that the prices are only multiplied by the selected color amount
                    // if the calculation type is configured this way
                    $colorAmount = $calculationType->getColorAmountDependent() ? $designArea->getColorAmount() : 1;

                    $prices = $this->calculator->calculateByColorAmount(
                        $pricesByColorAmount,
                        $colorAmount
                    );

                    $calculationTypePrices[] = $this->createCalculationTypePrice(
                        $calculationType,
                        $configuration,
                        $strategy,
                        $prices
                    );
                }
            }
        }

        return $calculationTypePrices;
    }

    protected function createCalculationTypePrice(
        DesignerProductionCalculationType $calculationType,
        ConfigurationModel $configuration,
        ProductionCalculationTypeInterface $strategy,
        array $prices
    ): CalculationTypePriceModel {
        $vatPrices = $this->vatCalculationService->getPricesAdapter($prices['price'], $prices['priceNet']);

        $calculatedPrices = $strategy->calculate($vatPrices, $configuration->getItemAmount());

        $selected = true;
        $userSelectedCalculation = $configuration->getUserSelectedCalculation();
        $typeIdentifier = 'production_' . $calculationType->getIdentifier();
        if ($calculationType->getSelectableByUser()
            && (!isset($userSelectedCalculation->$typeIdentifier)
                || $userSelectedCalculation->$typeIdentifier === false)) {
            $selected = false;
        }

        return CalculationTypePriceFactory::create(
            $calculationType->getTranslatedTitle(),
            $typeIdentifier,
            $calculationType->getDeclareSeparately(),
            $calculationType->getItemAmountDependent(),
            $calculationType->getSelectableByUser(),
            $selected,
            $calculatedPrices['price'],
            $calculatedPrices['priceNet']
        );
    }
}
