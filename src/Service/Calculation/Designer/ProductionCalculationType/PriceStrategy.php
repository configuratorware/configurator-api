<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\DesignAreaModel;

/**
 * @internal
 */
class PriceStrategy implements ProductionCalculationTypeInterface
{
    /**
     * {@inheritdoc}
     */
    public function providePrices(
        DesignerProductionCalculationType $calculationType,
        int $itemAmount,
        DesignAreaModel $designArea
    ): array {
        $pricesCollection = $calculationType->getDesignProductionMethodPrice()->filter(
            function ($entry) {
                /* @var  DesignProductionMethodPrice $entry */
                return C_CHANNEL === $entry->getChannel()->getIdentifier();
            }
        );

        $result = [];

        /** @var DesignProductionMethodPrice $price */
        foreach ($pricesCollection as $price) {
            $result[] = [
                'color_amount_from' => $price->getColorAmountFrom(),
                'price' => $price->getPrice(),
                'priceNet' => $price->getPriceNet(),
            ];
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(array $prices, int $factor): array
    {
        return $prices;
    }
}
