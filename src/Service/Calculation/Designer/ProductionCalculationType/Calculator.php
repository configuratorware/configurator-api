<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType;

/**
 * @internal
 */
class Calculator
{
    public function calculateByColorAmount(array $pricesByColorAmount, int $colorAmount): array
    {
        $prices = [
            'price' => 0,
            'priceNet' => 0,
        ];

        for ($i = 1; $i <= $colorAmount; ++$i) {
            $relevantPrices = array_filter($pricesByColorAmount, function ($price) use ($i) {
                return $price['color_amount_from'] <= $i;
            });

            uasort($relevantPrices, function ($a, $b) {
                return ($a['color_amount_from'] < $b['color_amount_from']) ? 1 : -1;
            });

            $first = array_shift($relevantPrices);

            if ($first) {
                $prices['price'] += $first['price'];
                $prices['priceNet'] += $first['priceNet'];
            }
        }

        return $prices;
    }
}
