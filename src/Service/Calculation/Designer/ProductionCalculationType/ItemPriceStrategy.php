<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodPriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\ColorAmountPriceModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\DesignAreaModel;

/**
 * @internal
 */
class ItemPriceStrategy implements ProductionCalculationTypeInterface
{
    /**
     * @var DesignAreaDesignProductionMethodPriceRepository
     */
    private $designAreaDesignProductionMethodPriceRepository;

    /**
     * ItemPriceStrategy constructor.
     *
     * @param DesignAreaDesignProductionMethodPriceRepository $designAreaDesignProductionMethodPriceRepository
     */
    public function __construct(
        DesignAreaDesignProductionMethodPriceRepository $designAreaDesignProductionMethodPriceRepository
    ) {
        $this->designAreaDesignProductionMethodPriceRepository = $designAreaDesignProductionMethodPriceRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function providePrices(
        DesignerProductionCalculationType $calculationType,
        int $itemAmount,
        DesignAreaModel $designArea
    ): array {
        if ($calculationType->getPricePerItem()) {
            return $this->providePricesByDesignAreaDesignProductionMethodPrice($calculationType, $itemAmount, $designArea);
        }

        return $this->providePricesByDesignProductionMethodPrice($calculationType, $itemAmount);
    }

    /**
     * @param DesignerProductionCalculationType $calculationType
     * @param int $itemAmount
     *
     * @return array
     */
    private function providePricesByDesignProductionMethodPrice(DesignerProductionCalculationType $calculationType, int $itemAmount): array
    {
        /** @var DesignProductionMethodPrice[] $prices */
        $prices = $calculationType->getDesignProductionMethodPrice()->filter(static function (DesignProductionMethodPrice $price) use ($itemAmount) {
            return C_CHANNEL === $price->getChannel()->getIdentifier() && $price->getAmountFrom() <= $itemAmount;
        })->toArray();

        usort($prices, static function (DesignProductionMethodPrice $a, DesignProductionMethodPrice $b) {
            return ($a->getAmountFrom() <=> $b->getAmountFrom()) * -1;
        });

        $result = [];
        $itemAmountFrom = null;
        foreach ($prices as $price) {
            if (null === $itemAmountFrom) {
                $itemAmountFrom = $price->getAmountFrom();
            }

            if ($itemAmountFrom === $price->getAmountFrom()) {
                $result[] = ColorAmountPriceModel::fromDesignProductionMethodPrice($price)->toArray();
            }
        }

        return $result;
    }

    /**
     * @param int $itemAmount
     * @param DesignAreaModel $designArea
     * @param DesignerProductionCalculationType $calculationType
     *
     * @return array
     */
    private function providePricesByDesignAreaDesignProductionMethodPrice(
        DesignerProductionCalculationType $calculationType,
        int $itemAmount,
        DesignAreaModel $designArea
    ): array {
        $prices = $this->designAreaDesignProductionMethodPriceRepository->getCalculationPrices(
            $calculationType,
            $designArea->getItem(),
            $designArea->getDesignProductionMethodIdentifier(),
            $designArea->getDesignAreaIdentifier(),
            $itemAmount
        );

        $result = [];
        $itemAmountFrom = null;
        foreach ($prices as $price) {
            if (null === $itemAmountFrom) {
                $itemAmountFrom = $price->getAmountFrom();
            }

            if ($itemAmountFrom === $price->getAmountFrom()) {
                $result[] = ColorAmountPriceModel::fromDesignAreaDesignProductionMethodPrice($price)->toArray();
            }
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(array $prices, int $factor): array
    {
        foreach ($prices as $key => $price) {
            $prices[$key] = round($price * $factor, 10);
        }

        return $prices;
    }
}
