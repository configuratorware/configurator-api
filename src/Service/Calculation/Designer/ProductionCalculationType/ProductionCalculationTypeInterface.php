<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\ProductionCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\DesignAreaModel;

interface ProductionCalculationTypeInterface
{
    /**
     * @param DesignerProductionCalculationType $calculationType
     * @param int $itemAmount
     * @param DesignAreaModel $designArea
     *
     * @return array
     */
    public function providePrices(DesignerProductionCalculationType $calculationType, int $itemAmount, DesignAreaModel $designArea): array;

    /**
     * @param array $prices
     * @param int $factor
     *
     * @return array
     */
    public function calculate(array $prices, int $factor): array;
}
