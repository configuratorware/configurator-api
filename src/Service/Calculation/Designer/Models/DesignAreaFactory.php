<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @internal
 */
class DesignAreaFactory
{
    /**
     * @param string $designAreaIdentifier
     * @param string $designProductionMethodIdentifier
     * @param int $colorAmount
     * @param Item $item
     *
     * @return DesignAreaModel
     */
    public static function create(
        string $designAreaIdentifier,
        string $designProductionMethodIdentifier,
        int $colorAmount,
        Item $item
    ) {
        $configurationModel = new DesignAreaModel(
            $designAreaIdentifier,
            $designProductionMethodIdentifier,
            $colorAmount,
            $item
        );

        return $configurationModel;
    }
}
