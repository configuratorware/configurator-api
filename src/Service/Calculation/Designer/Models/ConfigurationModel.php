<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage;

/**
 * @internal
 */
class ConfigurationModel
{
    private string $itemIdentifier;

    private int $itemAmount;

    /**
     * @var DesignAreaModel[]
     */
    private array $designAreas;

    private Item $item;

    private \stdClass $userSelectedCalculation;

    private bool $bulkNamesPresent;

    /**
     * @var ImageGalleryImage[]
     */
    private array $imageGalleryImage;

    private bool $userImage;

    private bool $galleryImage;

    /**
     * @param ImageGalleryImage[] $imageGalleryImage
     */
    public function __construct(
        string $itemIdentifier,
        int $itemAmount,
        array $designAreas,
        Item $item,
        \stdClass $userSelectedCalculation,
        bool $bulkNamesPresent,
        array $imageGalleryImage,
        bool $userImage,
        bool $galleryImage
    ) {
        $this->itemIdentifier = $itemIdentifier;
        $this->itemAmount = $itemAmount;
        $this->designAreas = $designAreas;
        $this->item = $item;
        $this->userSelectedCalculation = $userSelectedCalculation;
        $this->bulkNamesPresent = $bulkNamesPresent;
        $this->imageGalleryImage = $imageGalleryImage;
        $this->userImage = $userImage;
        $this->galleryImage = $galleryImage;
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        return [
            'itemIdentifier' => $this->itemIdentifier,
            'itemAmount' => $this->itemAmount,
            'designAreas' => $this->designAreas,
        ];
    }

    public function getItemIdentifier(): string
    {
        return $this->itemIdentifier;
    }

    public function getItemAmount(): int
    {
        return $this->itemAmount;
    }

    public function getDesignAreas(): array
    {
        return $this->designAreas;
    }

    public function hasImageData(): bool
    {
        return $this->userImage || $this->galleryImage;
    }

    /**
     * @return ImageGalleryImage[]
     */
    public function getImageGalleryImage(): array
    {
        return $this->imageGalleryImage;
    }

    public function getItem(): Item
    {
        return $this->item;
    }

    public function getUserSelectedCalculation(): \stdClass
    {
        return $this->userSelectedCalculation;
    }

    public function getBulkNamesPresent(): bool
    {
        return $this->bulkNamesPresent;
    }

    public function hasUserImage(): bool
    {
        return $this->userImage;
    }

    public function hasGalleryImage(): bool
    {
        return $this->galleryImage;
    }
}
