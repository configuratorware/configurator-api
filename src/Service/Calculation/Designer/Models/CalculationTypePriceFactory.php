<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models;

/**
 * @internal
 */
class CalculationTypePriceFactory
{
    public static function create(
        string $title,
        string $calculationTypeIdentifier,
        bool $declareSeparately,
        bool $itemAmountDependent,
        bool $selectableByUser,
        bool $selected,
        float $price,
        float $priceNet
    ): CalculationTypePriceModel {
        return new CalculationTypePriceModel(
            $title,
            $calculationTypeIdentifier,
            $declareSeparately,
            $itemAmountDependent,
            $selectableByUser,
            $selected,
            $price,
            $priceNet
        );
    }
}
