<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @internal
 */
class DesignAreaModel
{
    /**
     * @var string
     */
    private $designAreaIdentifier;

    /**
     * @var string
     */
    private $designProductionMethodIdentifier;

    /**
     * @var int
     */
    private $colorAmount;

    /**
     * @var Item
     */
    private $item;

    /**
     * DesignAreaModel constructor.
     *
     * @param string $designAreaIdentifier
     * @param string $designProductionMethodIdentifier
     * @param int $colorAmount
     * @param Item $item
     */
    public function __construct(
        string $designAreaIdentifier,
        string $designProductionMethodIdentifier,
        int $colorAmount,
        Item $item
    ) {
        $this->designAreaIdentifier = $designAreaIdentifier;
        $this->designProductionMethodIdentifier = $designProductionMethodIdentifier;
        $this->colorAmount = $colorAmount;
        $this->item = $item;
    }

    /**
     * @return array
     */
    public function getArray(): array
    {
        $designArea = [
            'designAreaIdentifier' => $this->designAreaIdentifier,
            'designProductionMethodIdentifier' => $this->designProductionMethodIdentifier,
            'colorAmount' => $this->colorAmount,
        ];

        return $designArea;
    }

    /**
     * @return string
     */
    public function getDesignAreaIdentifier(): string
    {
        return $this->designAreaIdentifier;
    }

    /**
     * @return string
     */
    public function getDesignProductionMethodIdentifier(): string
    {
        return $this->designProductionMethodIdentifier;
    }

    /**
     * @return int
     */
    public function getColorAmount(): int
    {
        return $this->colorAmount;
    }

    /**
     * @return Item
     */
    public function getItem(): Item
    {
        return $this->item;
    }
}
