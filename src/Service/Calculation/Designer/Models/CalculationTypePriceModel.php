<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models;

/**
 * @internal
 */
class CalculationTypePriceModel
{
    private string $title;

    private string $calculationTypeIdentifier;

    private bool $declareSeparately;

    private bool $itemAmountDependent;

    private bool $selectableByUser;

    private bool $selected;

    private float $price;

    private float $priceNet;

    public function __construct(
        string $title,
        string $calculationTypeIdentifier,
        bool $declareSeparately,
        bool $itemAmountDependent,
        bool $selectableByUser,
        bool $selected,
        float $price,
        float $priceNet
    ) {
        $this->title = $title;
        $this->calculationTypeIdentifier = $calculationTypeIdentifier;
        $this->declareSeparately = $declareSeparately;
        $this->itemAmountDependent = $itemAmountDependent;
        $this->selectableByUser = $selectableByUser;
        $this->selected = $selected;
        $this->price = $price;
        $this->priceNet = $priceNet;
    }

    public function getData(): \stdClass
    {
        $configuration = new \stdClass();
        $configuration->title = $this->title;
        $configuration->calculationTypeIdentifier = $this->calculationTypeIdentifier;
        $configuration->declareSeparately = $this->declareSeparately;
        $configuration->itemAmountDependent = $this->itemAmountDependent;
        $configuration->selectableByUser = $this->selectableByUser;
        $configuration->selected = $this->selected;
        $configuration->price = $this->price;
        $configuration->priceNet = $this->priceNet;

        return $configuration;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getCalculationTypeIdentifier(): string
    {
        return $this->calculationTypeIdentifier;
    }

    public function isSelectableByUser(): bool
    {
        return $this->selectableByUser;
    }

    public function isSelected(): bool
    {
        return $this->selected;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getPriceNet(): float
    {
        return $this->priceNet;
    }
}
