<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice;

/**
 * @internal
 */
final class ColorAmountPriceModel
{
    /**
     * @var float
     */
    private $price;

    /**
     * @var float
     */
    private $priceNet;

    /**
     * @var int
     */
    private $colorAmountFrom;

    /**
     * @param int $colorAmountFrom
     * @param float $price
     * @param float $priceNet
     */
    private function __construct(float $price, float $priceNet, int $colorAmountFrom = 1)
    {
        $this->price = $price;
        $this->priceNet = $priceNet;
        $this->colorAmountFrom = $colorAmountFrom;
    }

    /**
     * @param DesignProductionMethodPrice $price
     *
     * @return static
     */
    public static function fromDesignProductionMethodPrice(DesignProductionMethodPrice $price): self
    {
        return new self(
            (float)($price->getPrice() ?? 0.0),
            (float)($price->getPriceNet() ?? 0.0),
            $price->getColorAmountFrom() ?? 0
        );
    }

    /**
     * @param DesignAreaDesignProductionMethodPrice $price
     *
     * @return static
     */
    public static function fromDesignAreaDesignProductionMethodPrice(DesignAreaDesignProductionMethodPrice $price): self
    {
        return new self((float)$price->getPrice(), (float)$price->getPriceNet(), $price->getColorAmountFrom() ?? 0);
    }

    /**
     * @return array<string, int|float>
     */
    public function toArray(): array
    {
        return [
            'color_amount_from' => $this->colorAmountFrom,
            'price' => $this->price,
            'priceNet' => $this->priceNet,
        ];
    }
}
