<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage;

/**
 * @internal
 */
class ConfigurationFactory
{
    /**
     * @param ImageGalleryImage[] $imageGalleryImage
     */
    public static function create(
        string $itemIdentifier,
        int $itemAmount,
        array $designAreas,
        Item $item,
        \stdClass $userSelectedCalculation,
        bool $bulkNamesPresent,
        array $imageGalleryImage,
        bool $userImage,
        bool $galleryImage
    ): ConfigurationModel {
        return new ConfigurationModel(
            $itemIdentifier,
            $itemAmount,
            $designAreas,
            $item,
            $userSelectedCalculation,
            $bulkNamesPresent,
            $imageGalleryImage,
            $userImage,
            $galleryImage
        );
    }
}
