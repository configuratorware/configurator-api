<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;

/**
 * @internal
 */
class CalculationAggregator
{
    /**
     * @var FormatterService
     */
    private $formatterService;

    /**
     * CalculationAggregator constructor.
     *
     * @param FormatterService $formatterService
     */
    public function __construct(FormatterService $formatterService)
    {
        $this->formatterService = $formatterService;
    }

    /**
     * @param CalculationResult $calculationResult
     *
     * @return CalculationResult
     */
    public function aggregate(CalculationResult $calculationResult)
    {
        $aggregatedPositions = [];
        if (!empty($calculationResult->positions)) {
            foreach ($calculationResult->positions as $calculationPosition) {
                $aggregatedIdentifier = $calculationPosition->calculationTypeIdentifier;
                if (false === $calculationPosition->declareSeparately) {
                    if (0 === strpos($calculationPosition->calculationTypeIdentifier, 'production_')) {
                        $aggregatedIdentifier = 'setup_cost';
                        if (true === $calculationPosition->itemAmountDependent) {
                            $aggregatedIdentifier = 'production_cost';
                        }
                    }
                }

                if (!isset($aggregatedPositions[$aggregatedIdentifier])) {
                    $aggregatedPositions[$aggregatedIdentifier] = $calculationPosition;
                } else {
                    $aggregatedPositions[$aggregatedIdentifier]->title = '';
                    $aggregatedPositions[$aggregatedIdentifier]->calculationTypeIdentifier = $aggregatedIdentifier;

                    $price = $aggregatedPositions[$aggregatedIdentifier]->price + $calculationPosition->price;
                    $aggregatedPositions[$aggregatedIdentifier]->price = $this->roundPrice($price);
                    $aggregatedPositions[$aggregatedIdentifier]->priceFormatted = $this->formatterService->formatPrice($price);

                    $priceNet = $aggregatedPositions[$aggregatedIdentifier]->priceNet + $calculationPosition->priceNet;
                    $aggregatedPositions[$aggregatedIdentifier]->priceNet = $this->roundPrice($priceNet);
                    $aggregatedPositions[$aggregatedIdentifier]->priceNetFormatted = $this->formatterService->formatPrice($priceNet);
                }
            }

            foreach ($aggregatedPositions as $identifier => $aggregatedPosition) {
                $aggregatedIdentifier = $aggregatedPosition->calculationTypeIdentifier;
                if (0 === strpos($aggregatedPosition->calculationTypeIdentifier, 'production_')) {
                    $aggregatedIdentifier = 'setup_cost';
                    if (true === $aggregatedPosition->itemAmountDependent) {
                        $aggregatedIdentifier = 'production_cost';
                    }
                }

                // set the aggreagtion identifier for non aggregated positions too
                // this way the info icon in the frontend can be shown for them as well
                $aggregatedPositions[$identifier]->calculationTypeIdentifier = $aggregatedIdentifier;
            }

            $calculationResult->positions = array_values($aggregatedPositions);
        }

        return $calculationResult;
    }

    /**
     * @param float $price
     *
     * @return float
     */
    private function roundPrice($price)
    {
        return round($price, 10);
    }
}
