<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypePrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @internal
 */
class PriceStrategy implements GlobalCalculationTypeStrategyInterface
{
    /**
     * {@inheritdoc}
     */
    public function providePrices(DesignerGlobalCalculationType $calculationType, int $itemAmount, Item $item): array
    {
        $pricesCollection = $calculationType->getDesignerGlobalCalculationTypePrice()->filter(
            function ($entry) {
                /* @var  DesignerGlobalCalculationTypePrice $entry */
                return C_CHANNEL === $entry->getChannel()->getIdentifier();
            }
        );

        $result = [];

        if ($pricesCollection->count() > 0) {
            $price = $pricesCollection->first();

            $result = [
                'price' => $price->getPrice(),
                'priceNet' => $price->getPriceNet(),
            ];
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(array $prices, int $factor): array
    {
        foreach ($prices as $key => $price) {
            $prices[$key] = round($price * $factor, 10);
        }

        return $prices;
    }
}
