<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\CalculationTypeApiInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\ConfigurationModel;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\VatCalculationService;

class GlobalCalculationTypeApi implements CalculationTypeApiInterface
{
    private DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository;

    private VatCalculationService $vatCalculationService;

    private ItemPriceStrategy $itemPriceStrategy;

    private PriceStrategy $priceStrategy;

    public function __construct(
        DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository,
        VatCalculationService $vatCalculationService,
        ItemPriceStrategy $itemPriceStrategy,
        PriceStrategy $priceStrategy
    ) {
        $this->designerGlobalCalculationTypeRepository = $designerGlobalCalculationTypeRepository;
        $this->vatCalculationService = $vatCalculationService;
        $this->itemPriceStrategy = $itemPriceStrategy;
        $this->priceStrategy = $priceStrategy;
    }

    /**
     * @return CalculationTypePriceModel[]
     */
    public function getCalculationTypePrices(ConfigurationModel $configuration): array
    {
        $calculationTypePrices = [];
        /** @var DesignerGlobalCalculationType[] $calculationTypes */
        $calculationTypes = $this->designerGlobalCalculationTypeRepository->findAll();

        foreach ($calculationTypes as $calculationType) {
            if ($calculationType->getImageDataDependent() && !$configuration->hasImageData()) {
                continue;
            }
            if ($calculationType->getUserImageDependent() && !$configuration->hasUserImage()) {
                continue;
            }
            if ($calculationType->getGalleryImageDependent() && !$configuration->hasGalleryImage()) {
                continue;
            }

            if (true === $calculationType->getBulkNameDependent() && true !== $configuration->getBulkNamesPresent()) {
                continue;
            }
            if (true === $calculationType->getPricePerItem()) {
                $strategy = $this->itemPriceStrategy;
            } else {
                $strategy = $this->priceStrategy;
            }

            $prices = $strategy->providePrices($calculationType, $configuration->getItemAmount(),
                $configuration->getItem());

            if ($prices) {
                $calculationTypePrices[] = $this->createCalculationTypePrice(
                    $calculationType,
                    $configuration,
                    $strategy,
                    $prices
                );
            }
        }

        return $calculationTypePrices;
    }

    /**
     * @return CalculationTypePriceModel
     */
    protected function createCalculationTypePrice(
        DesignerGlobalCalculationType $calculationType,
        ConfigurationModel $configuration,
        GlobalCalculationTypeStrategyInterface $strategy,
        array $prices
    ): CalculationTypePriceModel {
        $vatPrices = $this->vatCalculationService->getPricesAdapter($prices['price'], $prices['priceNet']);

        $amount = true === $calculationType->getItemAmountDependent() ? $configuration->getItemAmount() : 1;
        $calculatedPrices = $strategy->calculate($vatPrices, $amount);

        $selected = true;
        $userSelectedCalculation = $configuration->getUserSelectedCalculation();

        $typeIdentifier = 'global_' . $calculationType->getIdentifier();
        if ($calculationType->getSelectableByUser()
            && (!isset($userSelectedCalculation->$typeIdentifier)
                || $userSelectedCalculation->$typeIdentifier === false)) {
            $selected = false;
        }

        return CalculationTypePriceFactory::create(
            $calculationType->getTranslatedTitle(),
            $typeIdentifier,
            false,
            $calculationType->getItemAmountDependent(),
            $calculationType->getSelectableByUser(),
            $selected,
            $calculatedPrices['price'],
            $calculatedPrices['priceNet']
        );
    }
}
