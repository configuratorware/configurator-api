<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalItemPriceRepository;

/**
 * @internal
 */
class ItemPriceStrategy implements GlobalCalculationTypeStrategyInterface
{
    /**
     * @var DesignerGlobalItemPriceRepository
     */
    private $designerGlobalItemPriceRepository;

    /**
     * PriceStrategy constructor.
     *
     * @param DesignerGlobalItemPriceRepository $designerGlobalItemPriceRepository
     */
    public function __construct(DesignerGlobalItemPriceRepository $designerGlobalItemPriceRepository)
    {
        $this->designerGlobalItemPriceRepository = $designerGlobalItemPriceRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function providePrices(DesignerGlobalCalculationType $calculationType, int $itemAmount, Item $item): array
    {
        /** @var DesignerGlobalItemPrice $priceEntity */
        $priceEntity = $this->designerGlobalItemPriceRepository->getCalculationPrice($item, $calculationType,
            $itemAmount);

        $price = [];
        if (!empty($priceEntity)) {
            $price = [
                'amount_from' => $priceEntity->getAmountFrom(),
                'price' => $priceEntity->getPrice(),
                'priceNet' => $priceEntity->getPriceNet(),
            ];
        }

        return $price;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(array $prices, int $factor): array
    {
        foreach ($prices as $key => $price) {
            $prices[$key] = round($price * $factor, 10);
        }

        return $prices;
    }
}
