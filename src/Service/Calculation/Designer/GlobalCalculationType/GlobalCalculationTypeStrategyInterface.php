<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\GlobalCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @internal
 */
interface GlobalCalculationTypeStrategyInterface
{
    /**
     * @param DesignerGlobalCalculationType $calculationType
     * @param int $itemAmount
     * @param Item $item
     *
     * @return array
     */
    public function providePrices(DesignerGlobalCalculationType $calculationType, int $itemAmount, Item $item): array;

    /**
     * @param array $prices
     * @param int $factor
     *
     * @return array
     */
    public function calculate(array $prices, int $factor): array;
}
