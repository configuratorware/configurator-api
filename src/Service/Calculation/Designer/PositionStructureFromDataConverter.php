<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Designer\Models\CalculationTypePriceModel;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResultPosition;

/**
 * @internal
 */
class PositionStructureFromDataConverter implements StructureFromDataConverterInterface
{
    /**
     * @var FormatterService
     */
    private $formatterService;

    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * PositionStructureFromDataConverter constructor.
     *
     * @param FormatterService $formatterService
     * @param StructureFromDataConverter $structureFromDataConverter
     */
    public function __construct(
        FormatterService $formatterService,
        StructureFromDataConverter $structureFromDataConverter
    ) {
        $this->formatterService = $formatterService;
        $this->structureFromDataConverter = $structureFromDataConverter;
    }

    /**
     * @param CalculationTypePriceModel $position
     * @param string $structureName
     *
     * @return CalculationResultPosition
     */
    public function convert($position, $structureName): CalculationResultPosition
    {
        $data = $position->getData();

        $data = $this->formatPrices($data);

        $structure = $this->structureFromDataConverter->convert($data, $structureName);

        unset($structure->_metadata);

        return $structure;
    }

    /**
     * @param array $positions
     * @param string $structureName
     *
     * @return array
     */
    public function convertMany(array $positions, string $structureName)
    {
        $positionStructures = [];

        foreach ($positions as $position) {
            $positionStructures[] = $this->convert($position, $structureName);
        }

        return $positionStructures;
    }

    /**
     * @param \StdClass $data
     *
     * @return \StdClass
     */
    protected function formatPrices(\stdClass $data): \stdClass
    {
        $data->priceFormatted = $this->formatterService->formatPrice($data->price);
        $data->priceNetFormatted = $this->formatterService->formatPrice($data->priceNet);

        return $data;
    }
}
