<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionDeltapriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItempriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionPriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Models\PriceModel;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
class PriceCalculation implements Calculation
{
    private const DEFAULT_PRICE = ['price' => 0.0, 'price_net' => 0.0];

    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @var ItempriceRepository
     */
    private $itemPriceRepository;

    /**
     * @var OptionPriceRepository
     */
    private $optionPriceRepository;

    /**
     * @var ItemOptionclassificationOptionDeltapriceRepository
     */
    private $deltaPriceRepository;

    /**
     * @var FormatterService
     */
    private $formatterService;

    /**
     * @var MinimumOrderAmount
     */
    private $minimumOrderAmount;

    /**
     * @var VatCalculationService
     */
    private $vatCalculationService;

    /**
     * @var ItempriceRepository
     */
    private $itemRepository;

    /**
     * PriceCalculation constructor.
     *
     * @param SettingRepository $settingRepository
     * @param ItempriceRepository $itemPriceRepository
     * @param OptionPriceRepository $optionPriceRepository
     * @param ItemOptionclassificationOptionDeltapriceRepository $deltaPriceRepository
     * @param FormatterService $formatterService
     * @param MinimumOrderAmount $minimumOrderAmount
     * @param VatCalculationService $vatCalculationService
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        SettingRepository $settingRepository,
        ItempriceRepository $itemPriceRepository,
        OptionPriceRepository $optionPriceRepository,
        ItemOptionclassificationOptionDeltapriceRepository $deltaPriceRepository,
        FormatterService $formatterService,
        MinimumOrderAmount $minimumOrderAmount,
        VatCalculationService $vatCalculationService,
        ItemRepository $itemRepository
    ) {
        $this->settingRepository = $settingRepository;
        $this->itemPriceRepository = $itemPriceRepository;
        $this->optionPriceRepository = $optionPriceRepository;
        $this->deltaPriceRepository = $deltaPriceRepository;
        $this->formatterService = $formatterService;
        $this->minimumOrderAmount = $minimumOrderAmount;
        $this->vatCalculationService = $vatCalculationService;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @return VatCalculationService
     */
    public function getVatCalculationService(): VatCalculationService
    {
        return $this->vatCalculationService;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate(Configuration $configuration): CalculationResult
    {
        $itemIdentifier = $configuration->item->identifier;

        // get the base prices of the item
        $vatPrices = $this->getItemPrice($configuration);

        // calculate the options' price
        $optionsPrice = $this->calculatePriceForOptions($configuration, $itemIdentifier, $this->useDeltaPrice(),
            C_CHANNEL);

        $optionsNetPrice = $this->calculatePriceForOptions($configuration, $itemIdentifier, $this->useDeltaPrice(),
            C_CHANNEL, true);

        // create calculation result
        $calculationResult = new CalculationResult();

        // add price
        $price = $vatPrices->getPrice();
        $calculationResult->total = $price + $optionsPrice;
        $calculationResult->totalFormatted = $this->formatterService->formatPrice($calculationResult->total);

        // add net price
        $netPrice = $vatPrices->getPriceNet();
        $calculationResult->netTotal = $netPrice + $optionsNetPrice;
        $calculationResult->netTotalFormatted = $this->formatterService->formatPrice($calculationResult->netTotal);

        // set vat Rate
        $vatRate = $vatPrices->getVatRate() ?? 0;
        if (0 == $vatRate && $calculationResult->total > 0 && $calculationResult->netTotal > 0) {
            $vatRate = floor($calculationResult->total / $calculationResult->netTotal * 100) - 100;
        }
        $calculationResult->vatRate = $vatRate;

        // calculate net price from price
        if ($calculationResult->total > 0 && $vatRate > 0) {
            $calculationResult->netTotalFromGrossTotal =
                round($calculationResult->total / (100 + $vatRate) * 100, 10);
            $calculationResult->netTotalFromGrossTotalFormatted =
                $this->formatterService->formatPrice($calculationResult->netTotalFromGrossTotal);
        }

        return $calculationResult;
    }

    public function getItemPrice(Configuration $configuration): PriceModel
    {
        $basePrices = [
            'price' => 0,
            'netPrice' => 0,
        ];

        $selectedAmounts = $this->getSelectedAmounts($configuration);
        $totalAmount = array_sum($selectedAmounts);
        $item = $this->itemRepository->findOneByIdentifier($configuration->item->identifier);
        $parentItem = $item;
        if ($item->getParent()) {
            $parentItem = $item->getParent();
        }
        $accumulateAmounts = $parentItem->getAccumulateAmounts();

        // check minimum order amounts
        $this->minimumOrderAmount->checkMinimumOrderAmounts(
            $configuration->item->identifier,
            $totalAmount,
            $selectedAmounts,
            $configuration
        );

        $itemIdentifiers = array_keys($selectedAmounts);
        $itemIdentifiers[] = $parentItem->getIdentifier();
        $itemIdentifiers = array_unique($itemIdentifiers);
        $itemPrices = $this->itemPriceRepository->getBulkPricesMappedByIdentifierAndAmountFrom(
            $itemIdentifiers,
            C_CHANNEL
        );

        foreach ($selectedAmounts as $childIdentifier => $itemAmount) {
            // set default values
            $bulkAmount = $itemAmount;
            $identifierForBulkPriceLookup = $childIdentifier;

            // if the parent item accumulates amounts
            // the total selected amount is used
            if ($accumulateAmounts) {
                $bulkAmount = $totalAmount;
            }

            // if the parent item accumulates amounts and has prices
            // the parent items prices are used
            if ($accumulateAmounts && !$parentItem->getItemprice()->isEmpty()) {
                $identifierForBulkPriceLookup = $parentItem->getIdentifier();
            }

            $itemPrice = self::DEFAULT_PRICE;
            if (isset($itemPrices[$identifierForBulkPriceLookup])) {
                $itemPrice = $this->getBulkPrice($itemPrices[$identifierForBulkPriceLookup], $bulkAmount);
            }

            $basePrices['price'] += $itemAmount * (float)$itemPrice['price'];
            $basePrices['netPrice'] += $itemAmount * (float)$itemPrice['price_net'];
        }

        return $this->vatCalculationService->getPrices($basePrices['price'], $basePrices['netPrice']);
    }

    /**
     * @param Configuration $configuration
     *
     * @return array
     */
    private function getSelectedAmounts(Configuration $configuration): array
    {
        // check selected amounts, default to amount one for the configuration item
        if (!isset($configuration->customdata->selectedAmounts) || empty($configuration->customdata->selectedAmounts)) {
            return [$configuration->item->identifier => 1];
        }

        return (array)$configuration->customdata->selectedAmounts;
    }

    /**
     * walk through all selected options and add the sum of each delta price.
     *
     * @param Configuration $configuration
     * @param string $itemIdentifier
     * @param bool $useDeltaPrice
     * @param string $channel
     * @param bool $calculateNet
     *
     * @return float
     *
     * @author  Andreas Nölke <noelke@redhotmagma.de>
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     */
    protected function calculatePriceForOptions(
        Configuration $configuration,
        string $itemIdentifier,
        bool $useDeltaPrice,
        string $channel,
        bool $calculateNet = false
    ): float {
        $result = 0;
        $itemAmount = $this->getSelectedAmounts($configuration);
        $itemAmount = $itemAmount[$itemIdentifier] ?? 0;

        foreach ($configuration->optionclassifications as $optionClassification) {
            $optionClassificationIdentifier = $optionClassification->identifier;
            if (!isset($optionClassification->selectedoptions)) {
                continue;
            }
            foreach ($optionClassification->selectedoptions as $selectedOption) {
                $amount = isset($selectedOption->amount) ? $itemAmount * $selectedOption->amount : $itemAmount;

                if ($useDeltaPrice) {
                    $prices = $this->getDeltaPrice(
                        $itemIdentifier,
                        $optionClassificationIdentifier,
                        $selectedOption->identifier,
                        $channel
                    );
                } else {
                    $prices = $this->getOptionPrice($selectedOption->identifier);
                }

                $vatPrices = $this->vatCalculationService->getPrices($prices['price'], $prices['netPrice']);

                if (true === $calculateNet) {
                    $result += $amount * $vatPrices->getPriceNet();
                } else {
                    $result += $amount * $vatPrices->getPrice();
                }
            }
        }

        return $result;
    }

    /**
     * get the delta prices ['price' => ..., 'price_net' => ...].
     *
     * @param string $itemIdentifier
     * @param string $optionClassificationIdentifier
     * @param string $selectedOptionIdentifier
     * @param string $channel
     *
     * @return array
     */
    protected function getDeltaPrice(
        string $itemIdentifier,
        string $optionClassificationIdentifier,
        string $selectedOptionIdentifier,
        string $channel
    ): array {
        $prices = $this->deltaPriceRepository->getDeltaPriceByIdentifiers(
            $itemIdentifier,
            $optionClassificationIdentifier,
            $selectedOptionIdentifier,
            $channel
        );

        return $prices;
    }

    /**
     * get the option price ['price' => ..., 'price_net' => ...].
     *
     * @param string $selectedOptionIdentifier
     *
     * @return array
     */
    protected function getOptionPrice(string $selectedOptionIdentifier): array
    {
        $optionPrices = $this->optionPriceRepository->getOptionPriceByIdentifier(
            $selectedOptionIdentifier,
            C_CHANNEL
        );

        return $optionPrices;
    }

    /**
     * check if the delta price should be used for the calculation.
     *
     * @return bool
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Andreas Nölke <noelke@redhotmagma.de>
     */
    protected function useDeltaPrice(): bool
    {
        $calculationMethod = $this->settingRepository->getCalculationMethod();

        $useDeltaPrice = false;
        if (SettingRepository::DELTA_PRICES === $calculationMethod) {
            $useDeltaPrice = true;
        }

        return $useDeltaPrice;
    }

    /**
     * @param array $priceMap
     * @param int $bulkAmount
     *
     * @return array
     */
    private function getBulkPrice(array $priceMap, int $bulkAmount): array
    {
        foreach ($priceMap as $amountFrom => $price) {
            if ($amountFrom <= $bulkAmount) {
                return $price;
            }
        }

        return self::DEFAULT_PRICE;
    }
}
