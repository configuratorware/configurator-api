<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult as CalculationResultStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResultPosition;

/**
 * @internal
 */
class GlobalChannelDiscount
{
    /**
     * @var array
     */
    private $channelSettings;

    /**
     * @var FormatterService
     */
    private $formatterService;

    /**
     * @param FormatterService $formatterService
     */
    public function __construct(
        FormatterService $formatterService
    ) {
        $this->formatterService = $formatterService;
    }

    /**
     * @param array|null $settings
     */
    public function setUpChannelSettings(
        array $settings = null
    ) {
        if (null === $settings) {
            $this->channelSettings = C_CHANNEL_SETTINGS;
        } else {
            $this->channelSettings = $settings;
        }
    }

    /**
     * @param CalculationResultStructure $calculationResultStructure
     *
     * @return CalculationResultStructure
     */
    public function updateTotals(
        CalculationResultStructure $calculationResultStructure
    ): CalculationResultStructure {
        $channelDiscountFactor = $this->getChannelDiscountFactor();
        if (abs(1 - $channelDiscountFactor) > PHP_FLOAT_EPSILON) {
            $total = round($calculationResultStructure->total * $channelDiscountFactor, 10);
            $calculationResultStructure->total = $total;
            $calculationResultStructure->totalFormatted = $this->formatterService->formatPrice($total);

            $netTotal = round($calculationResultStructure->netTotal * $channelDiscountFactor, 10);
            $calculationResultStructure->netTotal = $netTotal;
            $calculationResultStructure->netTotalFormatted = $this->formatterService->formatPrice($netTotal);

            if ($total > 0 && $calculationResultStructure->vatRate > 0) {
                $netTotalFromGrossTotal = round($calculationResultStructure->netTotalFromGrossTotal * $channelDiscountFactor, 10);
                $calculationResultStructure->netTotalFromGrossTotal = $netTotalFromGrossTotal;
                $calculationResultStructure->netTotalFromGrossTotalFormatted = $this->formatterService->formatPrice($netTotalFromGrossTotal);
            }
        }

        return $calculationResultStructure;
    }

    /**
     * @param CalculationResultStructure $calculationResultStructure
     *
     * @return CalculationResultStructure
     */
    public function updatePositions(
        CalculationResultStructure $calculationResultStructure
    ): CalculationResultStructure {
        $channelDiscountFactor = $this->getChannelDiscountFactor();
        if (1 !== $channelDiscountFactor) {
            /** @var CalculationResultPosition $position */
            foreach ($calculationResultStructure->positions as $position) {
                $position->price = $position->price * $channelDiscountFactor;
                $position->priceFormatted = $this->formatterService->formatPrice($position->price);
                $position->priceNet = $position->priceNet * $channelDiscountFactor;
                $position->priceNetFormatted = $this->formatterService->formatPrice($position->priceNet);
            }
        }

        return $calculationResultStructure;
    }

    /**
     * @return float
     */
    private function getChannelDiscountFactor(): float
    {
        if (null === $this->channelSettings) {
            $this->setUpChannelSettings();
        }
        $channelDiscountFactor = 1;
        if (isset($this->channelSettings['globalDiscountPercentage'])) {
            $channelDiscountFactor = 1 - ($this->channelSettings['globalDiscountPercentage'] / 100);
        }

        return $channelDiscountFactor;
    }
}
