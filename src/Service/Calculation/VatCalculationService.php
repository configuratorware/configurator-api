<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Models\PriceModel;

/**
 * @internal
 */
class VatCalculationService
{
    public const PRICE_DATA_GROSS = 'gross';

    public const PRICE_DATA_NET = 'net';

    private ?array $channelSettings = null;

    private FormatterService $formatterService;

    public function __construct(FormatterService $formatterService)
    {
        $this->formatterService = $formatterService;
    }

    /**
     * @param        $price
     * @param        $priceNet
     * @param string $priceKey
     * @param string $priceNetKey
     * @param string $vatRateKey
     *
     * @return array
     */
    public function getPricesAdapter(
        $price,
        $priceNet,
        string $priceKey = 'price',
        string $priceNetKey = 'priceNet',
        string $vatRateKey = 'vatRate'
    ): array {
        $vatPrices = $this->getPrices(
            $price,
            $priceNet
        );

        $vatPricesByKey = [];
        $vatPricesByKey[$priceKey] = $vatPrices->getPrice();
        $vatPricesByKey[$priceNetKey] = $vatPrices->getPriceNet();
        $vatPricesByKey[$vatRateKey] = $vatPrices->getVatRate();

        return $vatPricesByKey;
    }

    /**
     * Returns ann array with 'price' and 'netPrice' based on the channel settings.
     *
     * {"pricesDataNetGross":"net","pricesDisplayedNetGross":"gross"}
     *
     * Fallbacks to returning the provided price if settings are incomplete
     *
     * @param float|string|null $price
     * @param float|string|null $priceNet
     * @param bool $roundPrices
     *
     * @return PriceModel
     */
    public function getPrices($price, $priceNet = 0.0, bool $roundPrices = true): PriceModel
    {
        $priceResult = (float)$price;
        $netPriceResult = (float)$priceNet;
        $vatRateResult = null;

        if (null === $this->channelSettings) {
            $this->setUpChannelSettings();
        }

        if (null !== $this->channelSettings) {
            $channelSettings = $this->channelSettings;

            if (isset($channelSettings['vatrate'])
            ) {
                $vatRateResult = (float)$channelSettings['vatrate'];
                if (
                    isset($channelSettings['pricesDisplayedNetGross'], $channelSettings['pricesDataNetGross'])
                ) {
                    if (self::PRICE_DATA_GROSS === $channelSettings['pricesDataNetGross']
                        && self::PRICE_DATA_GROSS === $channelSettings['pricesDisplayedNetGross']
                    ) {
                        // both values are gross: use the price directly and calculate net price
                        $netPriceResult = $this->getPriceResult((float) $price, $vatRateResult);
                    } elseif (self::PRICE_DATA_NET === $channelSettings['pricesDataNetGross']
                        && self::PRICE_DATA_GROSS === $channelSettings['pricesDisplayedNetGross']
                    ) {
                        // data is net, display should be gross -> calculate gross
                        $priceResult = (float)$price * (100 + $vatRateResult) / 100;
                        $netPriceResult = (float)$price;
                    } elseif (self::PRICE_DATA_GROSS === $channelSettings['pricesDataNetGross']
                        && self::PRICE_DATA_NET === $channelSettings['pricesDisplayedNetGross']
                    ) {
                        // data is gross, display should be net -> calculate net
                        $priceResult = $this->getPriceResult((float) $price, $vatRateResult);
                        $netPriceResult = $priceResult;
                    } elseif (self::PRICE_DATA_NET === $channelSettings['pricesDataNetGross']
                        && self::PRICE_DATA_NET === $channelSettings['pricesDisplayedNetGross']
                    ) {
                        // data is net, display should be net
                        $netPriceResult = $priceResult;
                    }
                }
            }
        }

        if (true === $roundPrices) {
            $priceResult = $this->roundResult($priceResult);
            $netPriceResult = $this->roundResult($netPriceResult);
        }

        return PriceModel::fromPrices($priceResult, $netPriceResult, $vatRateResult);
    }

    private function getPriceResult(float $price, float $vatRateResult): float
    {
        return $price / (100 + $vatRateResult) * 100;
    }

    /**
     * @param float|string|null $price
     * @param float|string|null $priceNet
     * @param bool $roundPrices
     *
     * @return PriceModel
     */
    public function getPricesFormatted($price, $priceNet = 0.0, bool $roundPrices = true): PriceModel
    {
        $prices = $this->getPrices($price, $priceNet, $roundPrices);
        $price = (float)$prices->getPrice();
        if (defined('CALCULATION_PRICES_NET') && true === CALCULATION_PRICES_NET) {
            $price = (float)$prices->getPriceNet();
        }
        $prices->setPriceFormatted($this->formatterService->formatPrice($price));

        return $prices;
    }

    /**
     * Sets up the channel settings. If not given via param it will take the ones from C_CHANNEL_SETTINGS
     * which is set in the channel listener.
     *
     * @param array|null $settings
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     */
    public function setUpChannelSettings(array $settings = null): void
    {
        if (null === $settings && true === defined('C_CHANNEL_SETTINGS')) {
            $this->channelSettings = C_CHANNEL_SETTINGS;
        } else {
            if (null !== $settings) {
                $this->channelSettings = $settings;
            }
        }
    }

    /**
     * @param float $result
     *
     * @return float
     */
    protected function roundResult($result): float
    {
        return round($result, 10);
    }

    /**
     * @param object $structure
     * @param float|string|null $price
     * @param float|string|null $priceNet
     *
     * @return void
     */
    public function addPricesToStructure(object $structure, $price, $priceNet): void
    {
        $vatPrices = $this->getPrices($price, $priceNet);
        if (property_exists($structure, 'price')) {
            $structure->price = $vatPrices->getPrice();
        }

        if (property_exists($structure, 'netPrice')) {
            $structure->netPrice = $vatPrices->getPriceNet();
        }

        if (property_exists($structure, 'priceFormatted')) {
            $displayPrice = (float)$vatPrices->getPrice();
            if (defined('CALCULATION_PRICES_NET') && true === CALCULATION_PRICES_NET) {
                $displayPrice = (float)$vatPrices->getPriceNet();
            }
            $structure->priceFormatted = $this->formatterService->formatPrice($displayPrice);
        }

        if (property_exists($structure, 'netPriceFormatted')) {
            $structure->netPriceFormatted = $this->formatterService->formatPrice((float)$vatPrices->getPriceNet());
        }
    }
}
