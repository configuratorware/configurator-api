<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\MinimumOrderAmountViolation;

/**
 * @internal
 */
class MinimumOrderAmountCheck
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var DesignAreaDesignProductionMethodRepository
     */
    private $designAreaDesignProductionMethodRepository;

    /**
     * MinimumOrderAmountCheck constructor.
     *
     * @param ItemRepository $itemRepository
     * @param DesignAreaDesignProductionMethodRepository $designAreaDesignProductionMethodRepository
     */
    public function __construct(
        ItemRepository $itemRepository,
        DesignAreaDesignProductionMethodRepository $designAreaDesignProductionMethodRepository
    ) {
        $this->itemRepository = $itemRepository;
        $this->designAreaDesignProductionMethodRepository = $designAreaDesignProductionMethodRepository;
    }

    /**
     * @param string $itemIdentifier
     * @param int $totalAmount
     * @param array $selectedAmounts
     * @param Configuration $configuration
     *
     * @return array
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function checkMinimumOrderAmountsForItems(string $itemIdentifier, int $totalAmount, array $selectedAmounts, Configuration $configuration): array
    {
        $violations = [];

        $productionMinAmount = $this->getProductionMethodMinAmount($configuration);

        // if the paren accumulates amounts check the total amount against the parents min amount
        // else check the individually selected amounts
        $parentItem = $this->itemRepository->findOneByIdentifier($itemIdentifier);
        if (null !== $parentItem->getParent()) {
            $parentItem = $parentItem->getParent();
        }
        if (null !== $parentItem->getMinimumOrderAmount() && $parentItem->getAccumulateAmounts()) {
            $minAmount = max($parentItem->getMinimumOrderAmount(), $productionMinAmount, 1);

            if ($totalAmount < $minAmount) {
                $violations[$parentItem->getIdentifier()] = $this->createItemMinimumOrderAmountViolation($parentItem, $totalAmount, $minAmount);
            }
        } else {
            // check each selected item
            foreach ($selectedAmounts as $selectedItemIdentifier => $selectedAmount) {
                $item = $this->itemRepository->findOneByIdentifier($selectedItemIdentifier);

                $minAmount = $item->getMinimumOrderAmount();
                if ($parentItem->getAccumulateAmounts() || empty($minAmount)) {
                    $minAmount = $parentItem->getMinimumOrderAmount();
                }
                $minAmount = max($minAmount, $productionMinAmount, 1);

                if ($parentItem->getAccumulateAmounts()) {
                    if ($totalAmount < $minAmount) {
                        $violations[$parentItem->getIdentifier()] = $this->createItemMinimumOrderAmountViolation($parentItem, $totalAmount, $minAmount);
                    }
                } else {
                    if ($selectedAmount < $minAmount) {
                        $violations[$item->getIdentifier()] = $this->createItemMinimumOrderAmountViolation($item, (int)$selectedAmount, $minAmount);
                    }
                }
            }
        }

        return array_values($violations);
    }

    /**
     * @param Configuration $configuration
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function getProductionMethodMinAmount(
        Configuration $configuration
    ): int {
        $minAmount = 0;

        // collect min amounts from all used design areas and their related production methods
        // the highest found value is used

        foreach ((array)$configuration->designdata as $areaIdentifier => $designData) {
            $areaIdentifier = (string)$areaIdentifier;
            if (!isset($designData->canvasData)) {
                continue;
            }

            // create design_view_design_production_method violation
            $designAreaDesignProductionMethod = $this->designAreaDesignProductionMethodRepository
                ->findOneByItemAndDesignAreaAreaAndProductionMethod(
                    $configuration->item->identifier,
                    $areaIdentifier,
                    $designData->designProductionMethodIdentifier
                );

            if (null !== $designAreaDesignProductionMethod
                && $designAreaDesignProductionMethod->getMinimumOrderAmount() > $minAmount
            ) {
                $minAmount = $designAreaDesignProductionMethod->getMinimumOrderAmount();

                $designProductionMethod = $designAreaDesignProductionMethod->getDesignProductionMethod();
                if (null !== $designProductionMethod && $designProductionMethod->getMinimumOrderAmount() > $minAmount) {
                    $minAmount = $designProductionMethod->getMinimumOrderAmount();
                }
            }
        }

        return $minAmount;
    }

    /**
     * @param Item $item
     * @param int|null $selectedAmount
     * @param int|null $minAmount
     *
     * @return MinimumOrderAmountViolation
     */
    private function createItemMinimumOrderAmountViolation(Item $item, ?int $selectedAmount, ?int $minAmount = null): MinimumOrderAmountViolation
    {
        $violation = new MinimumOrderAmountViolation();
        $violation->itemIdentifier = $item->getIdentifier();
        $violation->itemTitle = $item->getTranslatedTitle();
        $violation->selectedAmount = $selectedAmount;
        $violation->minimumAmount = $minAmount ?? (int)$item->getMinimumOrderAmount();

        return $violation;
    }
}
