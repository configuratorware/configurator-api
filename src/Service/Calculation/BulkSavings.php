<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaDesignProductionMethodPriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItempriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\BulkSavingsMessage;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
class BulkSavings
{
    /**
     * @var CalculationApi
     */
    private $calculationApi;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItempriceRepository
     */
    private $itemPriceRepository;

    /**
     * @var DesignAreaDesignProductionMethodPriceRepository
     */
    private $designAreaDesignProductionMethodPriceRepository;

    /**
     * BulkSavings constructor.
     *
     * @param CalculationApi $calculationApi
     * @param ItemRepository $itemRepository
     * @param ItempriceRepository $itemPriceRepository
     * @param DesignAreaDesignProductionMethodPriceRepository $designAreaDesignProductionMethodPriceRepository
     */
    public function __construct(
        CalculationApi $calculationApi,
        ItemRepository $itemRepository,
        ItempriceRepository $itemPriceRepository,
        DesignAreaDesignProductionMethodPriceRepository $designAreaDesignProductionMethodPriceRepository
    ) {
        $this->calculationApi = $calculationApi;
        $this->itemRepository = $itemRepository;
        $this->itemPriceRepository = $itemPriceRepository;
        $this->designAreaDesignProductionMethodPriceRepository = $designAreaDesignProductionMethodPriceRepository;
    }

    /**
     * @param CalculationResult $calculationResult
     * @param Configuration $configuration
     *
     * @return CalculationResult
     */
    public function addBulkBulkSavingsToCalculationResult(
        CalculationResult $calculationResult,
        Configuration $configuration
    ) {
        $bulkSavings = [];
        if (isset($configuration->customdata->selectedAmounts)) {
            $totalAmount = 0;
            foreach ($configuration->customdata->selectedAmounts as $itemIdentifier => $selectedAmount) {
                $totalAmount += (int)$selectedAmount;
            }

            // backup selected amounts to restore them before every manipulation for bulk calculation
            foreach ($configuration->customdata->selectedAmounts as $itemIdentifier => $selectedAmount) {
                $item = $this->itemRepository->findOneByIdentifier($itemIdentifier);

                $bulkAmounts = $this->getNextBulkAmounts(
                    $item, (int)$selectedAmount, $totalAmount, $configuration
                );

                $bulkSaving = $this->getBulkSaving(
                    $item, $bulkAmounts, $configuration, $calculationResult, $totalAmount
                );

                if (!empty($bulkSaving)) {
                    $bulkSavings[$itemIdentifier] = $bulkSaving;
                }
            }
        }

        $calculationResult->bulkSavings = array_values($this->filterBulkSavings(
            $bulkSavings,
            $configuration->item->identifier
        ));

        return $calculationResult;
    }

    /**
     * if all savings are the same only one entry for the savings is returned (the one for the parent item).
     *
     * @param $bulkSavings
     * @param $itemIdentifier
     *
     * @return array
     */
    private function filterBulkSavings($bulkSavings, $itemIdentifier)
    {
        $bulkSavingDistinct = [];
        foreach ($bulkSavings as $bulkSaving) {
            $bulkSavingDistinct[$bulkSaving->savingAmount] = $bulkSaving;
        }

        if (1 === count($bulkSavingDistinct)) {
            $bulkSaving = array_shift($bulkSavingDistinct);
            $item = $this->itemRepository->findOneByIdentifier($itemIdentifier);
            if (null !== $item->getParent()) {
                $itemIdentifier = $item->getParent()->getIdentifier();
            }
            $bulkSaving->itemIdentifier = $itemIdentifier;

            $bulkSavings = [$bulkSaving];
        }

        return $bulkSavings;
    }

    /**
     * @param Item $item
     * @param int $selectedAmount
     * @param int $totalAmount
     * @param Configuration $configuration
     *
     * @return array
     */
    private function getNextBulkAmounts(
        Item $item,
        int $selectedAmount,
        int $totalAmount,
        Configuration $configuration
    ) {
        $itemBulkSteps = $this->itemPriceRepository->getBulkSteps($item->getIdentifier());
        $productionBulkSteps = $this->getProductionBulkSteps($item, $configuration);

        $bulkAmounts = array_unique(array_merge($itemBulkSteps, $productionBulkSteps));

        // remove all bulk steps that are not greater than the selected amount
        // if amounts are accumulated use the total selected amount instead of the items selected amount
        if ((null !== $item->getParent() && $item->getParent()->getAccumulateAmounts())
            || (null === $item->getParent() && $item->getAccumulateAmounts())) {
            $selectedAmount = $totalAmount;
        }
        $bulkAmounts = array_filter(
            $bulkAmounts,
            function ($value) use ($selectedAmount) {
                return $value > $selectedAmount;
            }
        );
        asort($bulkAmounts);

        return $bulkAmounts;
    }

    /**
     * @param Item $item
     * @param Configuration $configuration
     *
     * @return array
     */
    private function getProductionBulkSteps(Item $item, Configuration $configuration)
    {
        $bulkSteps = [];
        if (isset($configuration->designdata) && !empty($configuration->designdata)) {
            foreach ($configuration->designdata as $designAreaIdentifier => $designData) {
                // only calculate prices if the design production method is known and the design area has elements
                if (isset($designData->designProductionMethodIdentifier)
                    && !empty($designData->canvasData)
                    && !empty($designData->canvasData->objects)) {
                    $itemIdentifier = $item->getIdentifier();
                    if (null !== $item->getParent()) {
                        $itemIdentifier = $item->getParent()->getIdentifier();
                    }

                    $productionBulkSteps = $this->designAreaDesignProductionMethodPriceRepository->getBulkSteps(
                        $itemIdentifier, $designAreaIdentifier, $designData->designProductionMethodIdentifier
                    );

                    $bulkSteps = array_merge($bulkSteps, $productionBulkSteps);
                }
            }
        }

        return $bulkSteps;
    }

    /**
     * @param Item $item
     * @param array $bulkAmounts
     * @param Configuration $configuration
     * @param CalculationResult $oldCalculationResult
     * @param int $totalAmount
     *
     * @return ?BulkSavingsMessage
     */
    private function getBulkSaving(
        Item $item,
        array $bulkAmounts,
        Configuration $configuration,
        CalculationResult $oldCalculationResult,
        int $totalAmount
    ) {
        $bulkSavingMessage = null;

        $backupSelectedAmounts = json_encode($configuration->customdata->selectedAmounts);

        if (!empty($bulkAmounts)) {
            foreach ($bulkAmounts as $bulkAmount) {
                $itemIdentifier = $item->getIdentifier();
                $amountToAdd = (int)$bulkAmount - $configuration->customdata->selectedAmounts->$itemIdentifier;
                if ((null !== $item->getParent() && $item->getParent()->getAccumulateAmounts())
                    || (null === $item->getParent() && $item->getAccumulateAmounts())) {
                    $amountToAdd = (int)$bulkAmount - $totalAmount;
                }

                $configuration->customdata->selectedAmounts->$itemIdentifier += $amountToAdd;
                $newCalculationResult = $this->calculationApi->calculate($configuration, false);
                // restore original selected amounts after calculating
                $configuration->customdata->selectedAmounts = json_decode($backupSelectedAmounts);

                $savings = $this->calculateSavings(
                    $oldCalculationResult, $newCalculationResult, $totalAmount, (int)$bulkAmount
                );

                // if a saving was found we do not have to search with higher amounts
                if ($savings > 0) {
                    $bulkSavingMessage = new BulkSavingsMessage();
                    $bulkSavingMessage->itemIdentifier = $itemIdentifier;
                    $bulkSavingMessage->itemTitle = $item->getTranslatedTitle();
                    $bulkSavingMessage->savingsBulkAmount = $amountToAdd;
                    $bulkSavingMessage->savingAmount = $savings;

                    break;
                }
            }
        }

        return $bulkSavingMessage;
    }

    /**
     * @param CalculationResult $oldCalculationResult
     * @param CalculationResult $newCalculationResult
     * @param int $oldTotalAmount
     * @param int $newTotalAmount
     *
     * @return int
     */
    private function calculateSavings(
        CalculationResult $oldCalculationResult,
        CalculationResult $newCalculationResult,
        int $oldTotalAmount,
        int $newTotalAmount
    ): int {
        if (defined('CALCULATION_PRICES_NET') && CALCULATION_PRICES_NET === true) {
            $oldItemPrice = $oldCalculationResult->netTotal / $oldTotalAmount;
            $newItemPrice = $newCalculationResult->netTotal / $newTotalAmount;
        } else {
            $oldItemPrice = $oldCalculationResult->total / $oldTotalAmount;
            $newItemPrice = $newCalculationResult->total / $newTotalAmount;
        }

        if (0.0 === (float)$oldItemPrice || 0.0 === (float)$newItemPrice) {
            return 0;
        }

        return (int)floor(100 - $newItemPrice / $oldItemPrice * 100);
    }
}
