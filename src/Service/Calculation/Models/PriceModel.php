<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation\Models;

/**
 * @internal
 */
final class PriceModel
{
    private ?float $price;

    private ?float $priceNet;

    private ?string $priceFormatted;

    private ?float $vatRate;

    public function __construct(?float $price, ?float $priceNet, ?float $vatRate = null, ?string $priceFormatted = null)
    {
        $this->price = $price;
        $this->priceNet = $priceNet;
        $this->priceFormatted = $priceFormatted;
        $this->vatRate = $vatRate;
    }

    public static function fromPrices(?float $price, ?float $priceNet, ?float $vatRate): self
    {
        return new self($price, $priceNet, $vatRate);
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function getPriceNet(): ?float
    {
        return $this->priceNet;
    }

    public function getPriceFormatted(): ?string
    {
        return $this->priceFormatted;
    }

    public function setPriceFormatted(string $priceFormatted): PriceModel
    {
        $this->priceFormatted = $priceFormatted;

        return $this;
    }

    public function getVatRate(): ?float
    {
        return $this->vatRate;
    }
}
