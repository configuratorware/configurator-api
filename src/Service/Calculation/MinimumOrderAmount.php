<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Doctrine\ORM\NonUniqueResultException;
use Redhotmagma\ConfiguratorApiBundle\Exception\MinimumOrderAmountException;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
class MinimumOrderAmount
{
    /**
     * @var MinimumOrderAmountCheck
     */
    private $minimumOrderAmountCheck;

    /**
     * MinimumOrderAmount constructor.
     *
     * @param MinimumOrderAmountCheck $minimumOrderAmountCheck
     */
    public function __construct(MinimumOrderAmountCheck $minimumOrderAmountCheck)
    {
        $this->minimumOrderAmountCheck = $minimumOrderAmountCheck;
    }

    /**
     * @param string $itemIdentifier
     * @param int    $totalAmount
     * @param array  $selectedAmounts
     * @param Configuration $configuration
     *
     * @throws MinimumOrderAmountException|NonUniqueResultException
     */
    public function checkMinimumOrderAmounts(string $itemIdentifier, int $totalAmount, array $selectedAmounts, Configuration $configuration): void
    {
        $violations = $this->minimumOrderAmountCheck->checkMinimumOrderAmountsForItems($itemIdentifier, $totalAmount, $selectedAmounts, $configuration);

        if (!empty($violations)) {
            throw new MinimumOrderAmountException($violations);
        }
    }
}
