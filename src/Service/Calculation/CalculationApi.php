<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Calculation;

use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class CalculationApi
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ConfigurationInfoApi constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Configuration $configuration
     * @param bool $calculateBulkSavings
     *
     * @return CalculationResult
     */
    public function calculate(Configuration $configuration, bool $calculateBulkSavings = true)
    {
        $event = new CalculateEvent($configuration, $calculateBulkSavings);
        $this->eventDispatcher->dispatch($event, CalculateEvent::NAME);
        $calculationResult = $event->getCalculationResult();

        return $calculationResult;
    }
}
