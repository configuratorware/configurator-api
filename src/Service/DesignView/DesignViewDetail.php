<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;

/**
 * @internal
 */
class DesignViewDetail
{
    /**
     * @var DesignViewImage
     */
    private $designViewImage;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * DesignViewDetail constructor.
     *
     * @param DesignViewImage $designViewImage
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        DesignViewImage $designViewImage,
        ItemRepository $itemRepository
    ) {
        $this->designViewImage = $designViewImage;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param DesignViewStructure $designView
     */
    public function addImageData(DesignViewStructure $designView): void
    {
        $designView->images = $this->designViewImage->getImages(
            $designView->identifier,
            $designView->item->identifier,
            $this->getChildIdentifiers($designView->item->identifier)
        );
    }

    /**
     * @param string $parentIdentifier
     *
     * @return array
     */
    private function getChildIdentifiers(string $parentIdentifier): array
    {
        $childIdentifiers = [];

        $children = $this->itemRepository->getItemsByParent($parentIdentifier);

        /* @var Item $child */
        foreach ($children as $child) {
            $childIdentifiers[] = $child->getIdentifier();
        }

        return $childIdentifiers;
    }
}
