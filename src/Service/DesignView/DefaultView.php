<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

abstract class DefaultView
{
    public const IDENTIFIER = 'default';
    public const BASE_SHAPE = '{"type":"Plane","x":0,"y":0,"width":0,"height":0}';
    public const POSITION = '{"x":0,"y":0,"width":0,"height":0}';
}
