<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewDesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;

/**
 * @internal
 */
class DesignViewEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @var DesignViewDesignAreaRepository
     */
    private $designViewDesignAreaRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * DesignViewEntityFromStructureConverter constructor.
     *
     * @param DesignViewRepository           $designViewRepository
     * @param DesignAreaRepository           $designAreaRepository
     * @param DesignViewDesignAreaRepository $designViewDesignAreaRepository
     * @param ItemRepository                 $itemRepository
     * @param OptionRepository               $optionRepository
     * @param EntityFromStructureConverter   $entityFromStructureConverter
     */
    public function __construct(
        DesignViewRepository $designViewRepository,
        DesignAreaRepository $designAreaRepository,
        DesignViewDesignAreaRepository $designViewDesignAreaRepository,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->designViewRepository = $designViewRepository;
        $this->designAreaRepository = $designAreaRepository;
        $this->designViewDesignAreaRepository = $designViewDesignAreaRepository;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param DesignViewStructure $structure
     * @param DesignView $designViewEntity
     * @param string $entityClassName
     *
     * @return DesignView
     */
    public function convertOne(
        $structure,
        $designViewEntity = null,
        $entityClassName = DesignView::class
    ): DesignView {
        /** @var DesignView $designViewEntity */
        $designViewEntity = $this->entityFromStructureConverter->convertOne($structure, $designViewEntity, $entityClassName);

        // Update Item
        if (isset($structure->item)) {
            $designViewEntity = $this->updateOneToManyRelation(
                $designViewEntity,
                $structure,
                'item',
                'Item',
                $this->itemRepository
            );
        }

        // Update Option
        if (isset($structure->option)) {
            $designViewEntity = $this->updateOneToManyRelation(
                $designViewEntity,
                $structure,
                'option',
                'Option',
                $this->optionRepository
            );
        }

        $designViewEntity = $this->updateTexts(
            $designViewEntity,
            $structure,
            'texts',
            'DesignViewText'
        );

        $designViewEntity = $this->updateDesignAreas(
            $designViewEntity,
            $structure,
            'DesignViewDesignArea',
            DesignViewDesignArea::class,
            'designAreas',
            'DesignArea',
            $this->designAreaRepository
        );

        // update default design view of design area
        $this->updateDefaultDesignView($structure->designAreas, $designViewEntity);

        return $designViewEntity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = DesignView::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param DesignView $entity
     * @param DesignViewStructure $structure
     * @param string $relatedStructureName
     * @param string $relatedEntityName
     * @param mixed $relatedRepository
     *
     * @return DesignView
     */
    protected function updateOneToManyRelation(
        DesignView $entity,
        DesignViewStructure $structure,
        string $relatedStructureName,
        string $relatedEntityName,
        $relatedRepository
    ): DesignView {
        /** @var DesignView $entity */
        $setter = 'set' . $relatedEntityName;
        $entity->$setter($relatedRepository->findOneBy(['id' => $structure->$relatedStructureName->id]));

        return $entity;
    }

    /**
     * @param DesignView $entity
     * @param DesignViewStructure $structure
     * @param string $relationStructureName
     * @param string $relationEntityName
     *
     * @return DesignView
     */
    protected function updateTexts(
        DesignView $entity,
        DesignViewStructure $structure,
        string $relationStructureName,
        string $relationEntityName
    ): DesignView {
        /** @var DesignView $entity */
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        return $entity;
    }

    /**
     * @param DesignView           $designViewEntity
     * @param DesignViewStructure  $structure
     * @param string               $relationEntityName
     * @param string               $relationEntityClass
     * @param string               $relatedStructureName
     * @param string               $relatedEntityName
     * @param DesignAreaRepository $relatedRepository
     *
     * @return DesignView
     */
    protected function updateDesignAreas(
        DesignView $designViewEntity,
        DesignViewStructure $structure,
        string $relationEntityName,
        string $relationEntityClass,
        string $relatedStructureName,
        string $relatedEntityName,
        DesignAreaRepository $relatedRepository
    ): DesignView {
        /** @var DesignView $designViewEntity */
        $designViewEntity = $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
            $structure,
            $designViewEntity,
            $relationEntityName,
            $relatedStructureName,
            $relatedEntityName
        );

        $designViewEntity = $this->entityFromStructureConverter->addNewManyToManyRelations(
            $structure,
            $designViewEntity,
            $relationEntityClass,
            $relatedStructureName,
            $relatedEntityName,
            $relatedRepository
        );

        // Push DesignAreas's id as key to array for easier access
        $designAreasById = null;
        foreach ($structure->designAreas as $area) {
            $designAreasById[$area->id] = $area;
        }

        // If Structure contains data for DesignViewDesignAreas, update entity
        $designViewDesignAreas = $designViewEntity->getDesignViewDesignArea();
        if (is_array($designAreasById) && is_countable($designViewDesignAreas)) {
            $amount = count($designViewDesignAreas);
            for ($i = 0; $i < $amount; ++$i) {
                $entityId = $designViewEntity->getDesignViewDesignArea()[$i]->getDesignArea()->getId();
                // Exclude entityIds which are not in structure because they cannot be updated and will get deleted
                if (in_array($entityId, array_keys($designAreasById))) {
                    $designViewEntity->getDesignViewDesignArea()[$i]->setBaseShape($designAreasById[$entityId]->baseShape);
                    $designViewEntity->getDesignViewDesignArea()[$i]->setPosition($designAreasById[$entityId]->position);
                    $designViewEntity->getDesignViewDesignArea()[$i]->setCustomData($designAreasById[$entityId]->customData);
                }
            }
        }

        return $designViewEntity;
    }

    /**
     * @param DesignAreaRelation[] $designAreaStructures
     * @param DesignView $designViewEntity
     */
    private function updateDefaultDesignView(array $designAreaStructures, DesignView $designViewEntity): void
    {
        /* @var $savedRelation DesignViewDesignArea */

        foreach ($designAreaStructures as $designAreaStructure) {
            $designAreaEntity = $this->designAreaRepository->find($designAreaStructure->id); // design area ID
            foreach ($designAreaEntity->getDesignViewDesignArea() as $savedRelation) { // walk through all related views
                $savedRelation->setIsDefault(false);
                if ($designAreaStructure->isDefault) {
                    // new default sent, reset all relations, but make this area+view the default
                    if ($designViewEntity->getId() === $savedRelation->getDesignView()->getId()) {
                        $savedRelation->setIsDefault(true);
                    }
                }
                $this->designViewDesignAreaRepository->save($savedRelation);
            }
        }
    }
}
