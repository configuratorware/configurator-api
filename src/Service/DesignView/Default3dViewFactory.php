<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @internal
 */
class Default3dViewFactory
{
    public const DEFAULT_3D_VIEW_IDENTIFIER = '3d';

    /**
     * @param Item $item
     *
     * @return DesignView
     */
    public function create3dDefaultDesignView(Item $item): DesignView
    {
        return new DesignView(self::DEFAULT_3D_VIEW_IDENTIFIER, $item);
    }

    /**
     * @param Item $item
     *
     * @return CreatorView
     */
    public function create3dDefaultCreatorView(Item $item): CreatorView
    {
        return CreatorView::from(null, self::DEFAULT_3D_VIEW_IDENTIFIER, $item, '01');
    }
}
