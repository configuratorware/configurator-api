<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\FrontendDesignAreaStructureFromDesignViewDesignAreaEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignView as DesignViewStructure;

/**
 * @internal
 */
class FrontendDesignViewStructureFromEntityConverter
{
    /**
     * @var FrontendDesignAreaStructureFromDesignViewDesignAreaEntityConverter
     */
    private $frontendDesignAreaStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * FrontendDesignViewStructureFromEntityConverter constructor.
     *
     * @param FrontendDesignAreaStructureFromDesignViewDesignAreaEntityConverter $frontendDesignAreaStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        FrontendDesignAreaStructureFromDesignViewDesignAreaEntityConverter $frontendDesignAreaStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->frontendDesignAreaStructureFromEntityConverter = $frontendDesignAreaStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param DesignView $entity
     *
     * @return DesignViewStructure
     */
    public function convertOne(DesignView $entity): DesignViewStructure
    {
        /** @var DesignViewStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, DesignViewStructure::class);

        $structure->title = $entity->getTranslatedTitle();

        $structure->designAreas = $this->frontendDesignAreaStructureFromEntityConverter->convertMany($entity->getDesignViewDesignArea()->toArray());

        return $structure;
    }

    /**
     * @param array $entities
     *
     * @return array
     */
    public function convertMany(array $entities): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }
}
