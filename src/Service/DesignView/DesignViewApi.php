<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDesignView;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDirectory;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadDesignViewDetailImageForGroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadDesignViewDetailImageForItem;

class DesignViewApi
{
    /**
     * @var DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @var DesignViewStructureFromEntityConverter
     */
    private $designViewStructureFromEntityConverter;

    /**
     * @var DesignViewSave
     */
    private $designViewSave;

    /**
     * @var DesignViewDelete
     */
    private $designViewDelete;

    /**
     * @var DesignViewDetail
     */
    private $designViewDetail;

    /**
     * @var DesignViewImage
     */
    private $designViewImage;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @param DesignViewRepository $designViewRepository
     * @param DesignViewStructureFromEntityConverter $designViewStructureFromEntityConverter
     * @param DesignViewSave $designViewSave
     * @param DesignViewDelete $designViewDelete
     * @param DesignViewDetail $designViewDetail
     * @param DesignViewImage $designViewImage
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        DesignViewRepository $designViewRepository,
        DesignViewStructureFromEntityConverter $designViewStructureFromEntityConverter,
        DesignViewSave $designViewSave,
        DesignViewDelete $designViewDelete,
        DesignViewDetail $designViewDetail,
        DesignViewImage $designViewImage,
        ItemRepository $itemRepository
    ) {
        $this->designViewRepository = $designViewRepository;
        $this->designViewStructureFromEntityConverter = $designViewStructureFromEntityConverter;
        $this->designViewSave = $designViewSave;
        $this->designViewDelete = $designViewDelete;
        $this->designViewDetail = $designViewDetail;
        $this->designViewImage = $designViewImage;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();
        $entities = $this->designViewRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult->data = $this->designViewStructureFromEntityConverter->convertMany($entities);
        $paginationResult->count = $this->designViewRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return DesignViewStructure
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): DesignViewStructure
    {
        /** @var DesignView $entity */
        $entity = $this->designViewRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->designViewStructureFromEntityConverter->convertOne($entity);
        $this->designViewDetail->addImageData($structure);

        return $structure;
    }

    /**
     * @param DesignViewStructure $structure
     *
     * @return DesignViewStructure
     */
    public function save(DesignViewStructure $structure): DesignViewStructure
    {
        $structure = $this->designViewSave->save($structure);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $ids = explode(',', $id);

        foreach ($ids as $id) {
            $this->designViewDelete->delete((int)$id);
        }
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments): void
    {
        /** @var SequenceNumberData $sequenceNumberData */
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            /** @var DesignView $entity */
            $entity = $this->designViewRepository->findOneBy(['id' => $sequenceNumberData->id]);
            if (is_object($entity)) {
                $entity->setSequencenumber($sequenceNumberData->sequencenumber);
                $this->designViewRepository->save($entity, false);
            }
        }

        $this->designViewRepository->flush();
    }

    /**
     * @param UploadDesignViewDetailImageForItem $uploadDesignViewImage
     */
    public function uploadDetailImageForItem(UploadDesignViewDetailImageForItem $uploadDesignViewImage): void
    {
        $item = $this->itemRepository->find($uploadDesignViewImage->itemId);
        $designView = $this->designViewRepository->find($uploadDesignViewImage->designViewId);

        $this->designViewImage->uploadDetailImage($item, $designView, $uploadDesignViewImage->realPath, $uploadDesignViewImage->filename);
    }

    /**
     * @param int $designViewId
     * @param int $itemId
     */
    public function deleteDetailImageForItem(int $designViewId, int $itemId): void
    {
        $item = $this->itemRepository->find($itemId);
        $designView = $this->designViewRepository->find($designViewId);

        $this->designViewImage->deleteDetailImage($item, $designView);
    }

    /**
     * @param UploadDesignViewDetailImageForGroupEntry $uploadDesignViewImage
     */
    public function uploadDetailImageForGroupEntry(UploadDesignViewDetailImageForGroupEntry $uploadDesignViewImage): void
    {
        $designView = $this->designViewRepository->find($uploadDesignViewImage->designViewId);

        $parentItem = $designView->getItem();
        if (null === $parentItem) {
            throw InvalidDesignView::missingItem();
        }

        $items = $this->itemRepository->findChildrenByGroupEntry($parentItem->getId(), $uploadDesignViewImage->groupEntryId);

        foreach ($items as $item) {
            $this->designViewImage->uploadDetailImage($item, $designView, $uploadDesignViewImage->realPath, $uploadDesignViewImage->filename);
        }
    }

    /**
     * @param int $designViewId
     * @param int $groupEntryId
     */
    public function deleteDetailImageForGroupEntry(int $designViewId, int $groupEntryId): void
    {
        $designView = $this->designViewRepository->find($designViewId);

        $parentItem = $designView->getItem();
        if (null === $parentItem) {
            throw InvalidDesignView::missingItem();
        }

        $items = $this->itemRepository->findChildrenByGroupEntry($parentItem->getId(), $groupEntryId);

        foreach ($items as $item) {
            try {
                $this->designViewImage->deleteDetailImage($item, $designView);
            } catch (FileException|InvalidDirectory $e) {
                // if there is no image, the loop continues
                continue;
            }
        }
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['designViewText.title'];
    }
}
