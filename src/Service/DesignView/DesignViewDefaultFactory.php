<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;

/**
 * @internal
 */
class DesignViewDefaultFactory
{
    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * FrontendDesignViewDefaultFactory constructor.
     *
     * @param DesignAreaRepository $designAreaRepository
     */
    public function __construct(DesignAreaRepository $designAreaRepository)
    {
        $this->designAreaRepository = $designAreaRepository;
    }

    /**
     * @param Item $item
     *
     * @return DesignView
     *
     * @throws Exception
     */
    public function create(Item $item): DesignView
    {
        $designAreas = $this->designAreaRepository->findByItemAndHasDesignProductionMethodRelation($item);

        $defaultDesignView = new DesignView(DefaultView::IDENTIFIER);

        foreach ($designAreas as $area) {
            $defaultDesignView->addDesignViewDesignArea($this->createViewArea($defaultDesignView, $area));
        }

        return $defaultDesignView;
    }

    /**
     * @param DesignView $view
     * @param DesignArea $area
     *
     * @return DesignViewDesignArea
     */
    private function createViewArea(DesignView $view, DesignArea $area): DesignViewDesignArea
    {
        return new DesignViewDesignArea($view, $area, DefaultView::BASE_SHAPE, DefaultView::POSITION);
    }
}
