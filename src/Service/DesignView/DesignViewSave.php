<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;

/**
 * @internal
 */
class DesignViewSave
{
    /**
     * @var DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @var DesignViewEntityFromStructureConverter
     */
    private $designViewEntityFromStructureConverter;

    /**
     * @var DesignViewStructureFromEntityConverter
     */
    private $designViewStructureFromEntityConverter;

    /**
     * @param DesignViewRepository $designViewRepository
     * @param DesignViewEntityFromStructureConverter $designViewEntityFromStructureConverter
     * @param DesignViewStructureFromEntityConverter $designViewStructureFromEntityConverter
     */
    public function __construct(
        DesignViewRepository $designViewRepository,
        DesignViewEntityFromStructureConverter $designViewEntityFromStructureConverter,
        DesignViewStructureFromEntityConverter $designViewStructureFromEntityConverter
    ) {
        $this->designViewRepository = $designViewRepository;
        $this->designViewEntityFromStructureConverter = $designViewEntityFromStructureConverter;
        $this->designViewStructureFromEntityConverter = $designViewStructureFromEntityConverter;
    }

    /**
     * @param DesignViewStructure $structure
     *
     * @return DesignViewStructure
     */
    public function save(DesignViewStructure $structure): DesignViewStructure
    {
        $entity = null;

        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->designViewRepository->findOneBy(['id' => $structure->id]);
        }

        $entity = $this->designViewEntityFromStructureConverter->convertOne($structure, $entity);

        $entity = $this->designViewRepository->save($entity);

        $this->designViewRepository->clear();

        /** @var DesignView $entity */
        $entity = $this->designViewRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->designViewStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
