<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class DesignViewDelete
{
    /**
     * @var DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var string
     */
    private $designViewThumbnailPath;

    /**
     * DesignViewDelete constructor.
     *
     * @param DesignViewRepository $designViewRepository
     * @param Filesystem $filesystem
     * @param string $mediaBasePath
     * @param string $designViewThumbnailPath
     */
    public function __construct(
        DesignViewRepository $designViewRepository,
        Filesystem $filesystem,
        string $mediaBasePath,
        string $designViewThumbnailPath
    ) {
        $this->designViewRepository = $designViewRepository;
        $this->filesystem = $filesystem;
        $this->mediaBasePath = $mediaBasePath;
        $this->designViewThumbnailPath = trim($designViewThumbnailPath, '/');
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        /**
         * Get Entity for given Id.
         *
         * @var DesignView $entity
         */
        $entity = $this->designViewRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }
        // remove related DesignViewTexts
        $this->removeRelation($entity, 'DesignViewText');

        // remove related DesignViewDesignAreas
        $this->removeRelation($entity, 'DesignViewDesignArea');

        $this->deleteDesignViewThumbnails($entity);

        $this->designViewRepository->delete($entity);
    }

    /**
     * @param DesignView $entity
     * @param string $relationName
     */
    private function removeRelation(
        DesignView $entity,
        string $relationName
    ): void {
        $callEntity = 'get' . $relationName;
        /** @var mixed $relations */
        $relations = $entity->$callEntity();
        if (!$relations->isEmpty()) {
            foreach ($relations as $relationEntry) {
                $this->designViewRepository->delete($relationEntry);
            }
        }
    }

    /**
     * @param DesignView $designView
     */
    private function deleteDesignViewThumbnails(DesignView $designView): void
    {
        $item = $designView->getItem();

        if (null === $item || '' === $this->mediaBasePath || '' === $this->designViewThumbnailPath) {
            return;
        }

        $dir = $this->mediaBasePath . '/' . $this->designViewThumbnailPath . '/' . $item->getIdentifier();

        if (is_dir($dir)) {
            $finder = new Finder();
            $finder->files()->in($dir)->name('*' . $designView->getIdentifier() . '.*')->depth('<2');

            foreach ($finder as $file) {
                $this->filesystem->remove($file->getPath() . '/' . $file->getFilename());
            }
        }
    }
}
