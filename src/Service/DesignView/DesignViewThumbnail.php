<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignViewImagePathsInterface;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class DesignViewThumbnail
{
    /**
     * @var DesignViewImagePathsInterface
     */
    private $designViewImagePaths;

    /**
     * @param DesignViewImagePathsInterface $designViewImagePaths
     */
    public function __construct(DesignViewImagePathsInterface $designViewImagePaths)
    {
        $this->designViewImagePaths = $designViewImagePaths;
    }

    /**
     * @param string $designViewIdentifier
     * @param string $itemIdentifier
     * @param array $childIdentifiers
     *
     * @return array
     */
    public function getImages(
        string $designViewIdentifier,
        string $itemIdentifier,
        array $childIdentifiers = []
    ): ?array {
        $images = [];

        try {
            $parentPath = $this->getImageLocation($itemIdentifier, $designViewIdentifier);

            if ([] === $childIdentifiers) {
                $images[$itemIdentifier] = $parentPath;
            }
        } catch (\Throwable $exception) {
            // if the item has children, there needs to be a check on it's files first
            if ([] === $childIdentifiers) {
                return null;
            }
        }

        foreach ($childIdentifiers as $childIdentifier) {
            try {
                $path = $this->getImageLocation($itemIdentifier . '/' . $childIdentifier, $designViewIdentifier);
            } catch (\Throwable $exception) {
                // if parent cannot be used as fallback, continue with the next child
                if (!isset($parentPath) || '' === $parentPath) {
                    continue;
                }
                $path = $parentPath;
            }
            $images[$childIdentifier] = $path;
        }

        return $images;
    }

    /**
     * @param string $dir
     * @param string $designViewIdentifier
     *
     * @return string
     */
    private function getImageLocation(string $dir, string $designViewIdentifier): string
    {
        $finder = new Finder();
        $finder->files()->in($this->designViewImagePaths->getDesignViewThumbnailPath() . '/' . $dir)->name($designViewIdentifier . '.*')->depth(0);

        $files = $finder->getIterator();
        $files->rewind();

        $fileName = $files->current()->getFileName();

        if (!is_string($fileName)) {
            throw new NotFoundException();
        }

        return '/' . $this->designViewImagePaths->getDesignViewThumbnailPathRelative() . '/' . $dir . '/' . $fileName;
    }
}
