<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewText;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignViewDesignArea\BaseShape2dPositionCalculation;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaRelation as DesignAreaRelationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignView as DesignViewStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignViewText as DesignViewTextStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemRelation as ItemRelationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionRelation as OptionRelationStructure;

/**
 * @internal
 */
class DesignViewStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var BaseShape2dPositionCalculation
     */
    private $baseShape2dPositionCalculation;

    /**
     * DesignViewStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param BaseShape2dPositionCalculation $baseShape2dPositionCalculation
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        BaseShape2dPositionCalculation $baseShape2dPositionCalculation
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->baseShape2dPositionCalculation = $baseShape2dPositionCalculation;
    }

    /**
     * @param DesignView $entity
     * @param string $structureClassName
     *
     * @return $structureClassName
     */
    public function convertOne(
        $entity,
        $structureClassName = DesignViewStructure::class
    ): DesignViewStructure {
        /** @var DesignViewStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        // Add Item or null to structure
        $structure = $this->addObjectToStructure(
            $structure,
            $entity,
            ItemRelationStructure::class,
            'item',
            'Item'
        );

        // Add Option or null to structure
        $structure = $this->addObjectToStructure(
            $structure,
            $entity,
            OptionRelationStructure::class,
            'option',
            'Option'
        );

        $structure = $this->addTextsToStructure(
            $structure,
            $entity,
            'DesignView',
            DesignViewTextStructure::class,
            'texts',
            'Text'
        );

        $structure = $this->addDesignAreasToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = DesignViewStructure::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param DesignViewStructure $structure
     * @param DesignView $entity
     * @param string $objectStructureClass
     * @param string $objectStructureName
     * @param string $objectEntityName
     *
     * @return DesignViewStructure
     */
    protected function addObjectToStructure(
        DesignViewStructure $structure,
        DesignView $entity,
        string $objectStructureClass,
        string $objectStructureName,
        string $objectEntityName
    ): DesignViewStructure {
        $callEntity = 'get' . $objectEntityName;

        /** @var mixed $object */
        $object = $entity->$callEntity();
        if ($object) {
            $structure->$objectStructureName = $this->structureFromEntityConverter->convertOne(
                $object,
                $objectStructureClass
            );

            if ($object->getTranslatedTitle()) {
                $structure->$objectStructureName->title = $object->getTranslatedTitle();
            }
        }

        return $structure;
    }

    /**
     * @param DesignViewStructure $structure
     * @param DesignView $entity
     * @param string $entityName
     * @param string $textStructureClass
     * @param string $textStructureName
     * @param string $textEntityName
     *
     * @return DesignViewStructure
     */
    protected function addTextsToStructure(
        DesignViewStructure $structure,
        DesignView $entity,
        string $entityName,
        string $textStructureClass,
        string $textStructureName,
        string $textEntityName
    ): DesignViewStructure {
        $callEntity = 'get' . $entityName . $textEntityName;

        /** @var DesignView $entity */
        $texts = $entity->$callEntity();

        $result = [];
        if (!empty($texts)) {
            /** @var DesignViewText $text */
            foreach ($texts as $text) {
                /** @var DesignViewTextStructure $textStructure */
                $textStructure = $this->structureFromEntityConverter->convertOne(
                    $text,
                    $textStructureClass
                );

                if (!empty($textStructure)) {
                    $textStructure->language = $text->getLanguage()->getIso();
                    $result[] = $textStructure;
                }
            }
        }

        $structure->$textStructureName = $result;

        return $structure;
    }

    /**
     * @param DesignViewStructure $structure
     * @param DesignView $entity
     *
     * @return DesignViewStructure
     */
    protected function addDesignAreasToStructure(
        DesignViewStructure $structure,
        DesignView $entity
    ): DesignViewStructure {
        $result = [];

        $relations = $entity->getDesignViewDesignArea();

        if (!empty($relations)) {
            foreach ($relations as $relation) {
                $relation = $this->baseShape2dPositionCalculation->calculateBaseShapePosition($relation);

                /** @var DesignViewStructure $textStructure */
                $relationStructure = $this->structureFromEntityConverter->convertOne(
                    $relation->getDesignArea(),
                    DesignAreaRelationStructure::class
                );

                /*
                 * Fields which cannot be populated by DesignArea
                 * receive data from relation: DesignViewDesignArea
                 */
                foreach ((array)$relationStructure as $key => $data) {
                    $propertyGetter = 'get' . ucfirst($key);

                    /*
                     * Hint: if property exists in DesignArea and DesignViewDesignArea
                     * data will be retrieved from DesignViewDesignArea if they are not excluded by LOCKED_ORIGIN_PROPERTIES
                     */
                    if (method_exists($relation, $propertyGetter) &&
                        null !== $relation->$propertyGetter() &&
                        !in_array($key, DesignAreaRelationStructure::LOCKED_ORIGIN_PROPERTIES)) {
                        $relationStructure->$key = $relation->$propertyGetter();
                    }
                }

                $relationStructure->title = $relation->getDesignArea()->getTranslatedTitle();

                if (!empty($relationStructure)) {
                    $result[] = $relationStructure;
                }
            }
        }

        $structure->designAreas = $result;

        return $structure;
    }
}
