<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignView;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidDesignView;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\DesignViewDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\DesignViewDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignViewImagePathsInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * @internal
 */
class DesignViewImage
{
    /**
     * @var DesignViewImagePathsInterface
     */
    private $designViewImagePaths;

    /**
     * @var DesignViewDetailImageRepositoryInterface
     */
    private $designViewImageRepository;

    /**
     * @param DesignViewImagePathsInterface $designViewImagePaths
     * @param DesignViewDetailImageRepositoryInterface $designViewImageRepository
     */
    public function __construct(DesignViewImagePathsInterface $designViewImagePaths, DesignViewDetailImageRepositoryInterface $designViewImageRepository)
    {
        $this->designViewImagePaths = $designViewImagePaths;
        $this->designViewImageRepository = $designViewImageRepository;
    }

    /**
     * @param Item $item
     * @param DesignView $designView
     * @param string $realPath
     * @param string $fileName
     */
    public function uploadDetailImage(Item $item, DesignView $designView, string $realPath, string $fileName): void
    {
        $parentIdentifier = null !== $item->getParent() ? $item->getParent()->getIdentifier() : $item->getIdentifier();

        $designViewIdentifier = $designView->getIdentifier();
        if (null === $designViewIdentifier) {
            throw InvalidDesignView::missingIdentifier();
        }

        $image = DesignViewDetailImage::from(null, $designViewIdentifier, $parentIdentifier, $item->getIdentifier(), $realPath, $fileName);

        $this->designViewImageRepository->saveDetailImage($image);
    }

    /**
     * @param Item $item
     * @param DesignView $designView
     */
    public function deleteDetailImage(Item $item, DesignView $designView): void
    {
        $image = $this->designViewImageRepository->findDetailImage($item, $designView);

        $this->designViewImageRepository->deleteDetailImage($image);
    }

    /**
     * @param string $designViewIdentifier
     * @param string $itemIdentifier
     * @param array $childIdentifiers
     *
     * @return array
     */
    public function getImages(string $designViewIdentifier, string $itemIdentifier, array $childIdentifiers = []): ?array
    {
        $images = [];
        $path = $itemIdentifier . '/' . $designViewIdentifier;

        if ([] === $childIdentifiers) {
            $image = $this->getImage($path, $itemIdentifier . '.*');
            if (null !== $image) {
                $images[$itemIdentifier] = $image;
            }
        }

        foreach ($childIdentifiers as $childIdentifier) {
            $image = $this->getImage($path, $childIdentifier . '.*');
            if (null !== $image) {
                $images[$childIdentifier] = $image;
            }
        }

        return $images;
    }

    /**
     * @param string $relativePath
     * @param string $pattern
     *
     * @return string
     */
    private function getImage(string $relativePath, string $pattern): ?string
    {
        $finder = new Finder();

        $searchDir = $this->designViewImagePaths->getDesignViewImagePath() . '/' . $relativePath;

        if (!is_dir($searchDir)) {
            return null;
        }

        /** @var \Iterator<SplFileInfo> $foundFiles */
        $foundFiles = $finder->files()->in($searchDir)->name($pattern)->depth(0);
        foreach ($foundFiles as $foundFile) {
            return '/' . $this->designViewImagePaths->getDesignViewImagePathRelative() . '/' . $relativePath . '/' . $foundFile->getFilename();
        }

        return null;
    }
}
