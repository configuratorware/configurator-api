<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Channel;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;

/**
 * @internal
 */
class ChannelSave
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var ChannelEntityFromStructureConverter
     */
    private $channelEntityFromStructureConverter;

    /**
     * @var ChannelStructureFromEntityConverter
     */
    private $channelStructureFromEntityConverter;

    /**
     * ChannelSave constructor.
     *
     * @param ChannelRepository $channelRepository
     * @param ChannelEntityFromStructureConverter $channelEntityFromStructureConverter
     * @param ChannelStructureFromEntityConverter $channelStructureFromEntityConverter
     */
    public function __construct(
        ChannelRepository $channelRepository,
        ChannelEntityFromStructureConverter $channelEntityFromStructureConverter,
        ChannelStructureFromEntityConverter $channelStructureFromEntityConverter
    ) {
        $this->channelRepository = $channelRepository;
        $this->channelEntityFromStructureConverter = $channelEntityFromStructureConverter;
        $this->channelStructureFromEntityConverter = $channelStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Channel $channel
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Channel
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Channel $channel)
    {
        $entity = null;
        if (isset($channel->id) && $channel->id > 0) {
            $entity = $this->channelRepository->findOneBy(['id' => $channel->id]);
        }
        $entity = $this->channelEntityFromStructureConverter->convertOne($channel, $entity);
        $entity = $this->channelRepository->save($entity);

        /** @var Channel $entity */
        $entity = $this->channelRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->channelStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
