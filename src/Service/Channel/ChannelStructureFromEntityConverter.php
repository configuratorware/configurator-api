<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Channel;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Channel;
use Redhotmagma\ConfiguratorApiBundle\Structure\Currency;

/**
 * @internal
 */
class ChannelStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Channel $entity
     * @param string $structureClassName
     *
     * @return Channel
     */
    public function convertOne($entity, $structureClassName = Channel::class)
    {
        /** @var Channel $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        if (!$entity->getSettings()) {
            $structure->settings = [];
        }
        // add currency to structure
        $structure->currency = $this->structureFromEntityConverter->convertOne($entity->getCurrency(), Currency::class);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Channel::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
