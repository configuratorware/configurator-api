<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Channel;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\CurrencyRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Channel;

/**
 * @internal
 */
class ChannelEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        CurrencyRepository $currencyRepository
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param Channel $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Channel $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Channel
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Channel::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        if (!empty($localStructure->currency)) {
            $currency = $this->currencyRepository->findOneBy(['id' => $localStructure->currency->id]);

            $localStructure->currency = $currency;
        }

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Channel $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Channel::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
