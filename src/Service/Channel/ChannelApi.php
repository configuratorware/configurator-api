<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Channel;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientUserCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Bundle\SecurityBundle\Security;

class ChannelApi
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var ChannelStructureFromEntityConverter
     */
    private $channelStructureFromEntityConverter;

    /**
     * @var ChannelSave
     */
    private $channelSave;

    /**
     * @var ChannelDelete
     */
    private $channelDelete;

    /**
     * @var ClientUserCheck
     */
    private $clientUserCheck;

    /**
     * @var Security
     */
    private $security;

    /**
     * @param ChannelRepository $channelRepository
     * @param ChannelStructureFromEntityConverter $channelStructureFromEntityConverter
     * @param ChannelSave $channelSave
     * @param ChannelDelete $channelDelete
     * @param ClientUserCheck $clientUserCheck
     * @param Security $security
     */
    public function __construct(
        ChannelRepository $channelRepository,
        ChannelStructureFromEntityConverter $channelStructureFromEntityConverter,
        ChannelSave $channelSave,
        ChannelDelete $channelDelete,
        ClientUserCheck $clientUserCheck,
        Security $security
    ) {
        $this->channelRepository = $channelRepository;
        $this->channelStructureFromEntityConverter = $channelStructureFromEntityConverter;
        $this->channelSave = $channelSave;
        $this->channelDelete = $channelDelete;
        $this->clientUserCheck = $clientUserCheck;
        $this->security = $security;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        if ($this->clientUserCheck->currentUserHasClientRole()) {
            $arguments->filters['_client_user_filter'] = $this->security->getUser();
        }

        $entities = $this->channelRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->channelStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->channelRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Channel
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Channel
    {
        /** @var Channel $entity */
        $entity = $this->channelRepository->findOneBy(['id' => $id]);

        if (null === $entity) {
            throw new NotFoundException();
        }

        $clients = [];
        foreach ($entity->getClientChannel() as $clientChannel) {
            $clients[] = $clientChannel->getClient();
        }

        $this->clientUserCheck->checkClients($clients);

        $structure = $this->channelStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Channel $channel
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Channel
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Channel $channel)
    {
        $structure = $this->channelSave->save($channel);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->channelDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier'];
    }
}
