<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Channel;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\CodeSnippetRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionDeltapriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItempriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionPriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionStockRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\StockRepository;

/**
 * @internal
 */
class ChannelDelete
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var ItempriceRepository
     */
    private $itemPriceRepository;

    /**
     * @var ItemOptionclassificationOptionDeltapriceRepository
     */
    private $itemOptionclassificationOptionDeltapriceRepository;

    /**
     * @var StockRepository
     */
    private $stockRepository;

    /**
     * @var CodeSnippetRepository
     */
    private $codeSnippetRepository;

    /**
     * @var OptionPriceRepository
     */
    private $optionPriceRepository;

    /**
     * @var OptionStockRepository
     */
    private $optionStockRepository;

    /**
     * ChannelDelete constructor.
     *
     * @param ChannelRepository $channelRepository
     * @param ItempriceRepository $itemPriceRepository
     * @param ItemOptionclassificationOptionDeltapriceRepository $itemOptionclassificationOptionDeltapriceRepository
     * @param StockRepository $stockRepository
     * @param CodeSnippetRepository $codeSnippetRepository
     * @param OptionPriceRepository $optionPriceRepository
     * @param OptionStockRepository $optionStockRepository
     */
    public function __construct(
        ChannelRepository $channelRepository,
        ItempriceRepository $itemPriceRepository,
        ItemOptionclassificationOptionDeltapriceRepository $itemOptionclassificationOptionDeltapriceRepository,
        StockRepository $stockRepository,
        CodeSnippetRepository $codeSnippetRepository,
        OptionPriceRepository $optionPriceRepository,
        OptionStockRepository $optionStockRepository
    ) {
        $this->channelRepository = $channelRepository;
        $this->itemPriceRepository = $itemPriceRepository;
        $this->itemOptionclassificationOptionDeltapriceRepository = $itemOptionclassificationOptionDeltapriceRepository;
        $this->stockRepository = $stockRepository;
        $this->codeSnippetRepository = $codeSnippetRepository;
        $this->optionPriceRepository = $optionPriceRepository;
        $this->optionStockRepository = $optionStockRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Channel $entity */
            $entity = $this->channelRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            // delete relations
            $this->deleteItemPrice($entity);
            $this->deleteItemOptionclassificationOptionDeltapriceRepository($entity);
            $this->deleteStock($entity);
            $this->deleteCodeSnippet($entity);
            $this->deleteOptionPrice($entity);
            $this->deleteOptionStock($entity);

            // delete channel
            $this->channelRepository->delete($entity);
        }
    }

    /**
     * @param Channel $channel
     */
    protected function deleteItemPrice($channel)
    {
        $relations = $this->itemPriceRepository->findBy(['channel' => $channel]);

        foreach ($relations as $relation) {
            $this->itemPriceRepository->delete($relation);
        }
    }

    /**
     * @param Channel $channel
     */
    protected function deleteItemOptionclassificationOptionDeltapriceRepository($channel)
    {
        $relations = $this->itemOptionclassificationOptionDeltapriceRepository->findBy(['channel' => $channel]);

        foreach ($relations as $relation) {
            $this->itemOptionclassificationOptionDeltapriceRepository->delete($relation);
        }
    }

    /**
     * @param Channel $channel
     */
    protected function deleteStock($channel)
    {
        $relations = $this->stockRepository->findBy(['channel' => $channel]);

        foreach ($relations as $relation) {
            $this->stockRepository->delete($relation);
        }
    }

    /**
     * @param Channel $channel
     */
    protected function deleteCodeSnippet($channel)
    {
        $relations = $this->codeSnippetRepository->findBy(['channel' => $channel]);

        foreach ($relations as $relation) {
            $this->codeSnippetRepository->delete($relation);
        }
    }

    /**
     * @param Channel $channel
     */
    protected function deleteOptionPrice($channel)
    {
        $relations = $this->optionPriceRepository->findBy(['channel' => $channel]);

        foreach ($relations as $relation) {
            $this->optionPriceRepository->delete($relation);
        }
    }

    /**
     * @param Channel $channel
     */
    protected function deleteOptionStock($channel)
    {
        $relations = $this->optionStockRepository->findBy(['channel' => $channel]);

        foreach ($relations as $itemPrice) {
            $this->optionStockRepository->delete($itemPrice);
        }
    }
}
