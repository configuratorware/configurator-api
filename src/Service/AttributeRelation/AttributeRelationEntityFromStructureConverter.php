<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationAttribute;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue\AttributeValueEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attribute;

/**
 * @internal
 */
class AttributeRelationEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * @var AttributevalueRepository
     */
    private $attributeValueRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var AttributeValueEntityFromStructureConverter
     */
    private $attributeValueEntityFromStructureConverter;

    /**
     * AttributeRelationEntityFromStructureConverter constructor.
     *
     * @param AttributeRepository $attributeRepository
     * @param AttributevalueRepository $attributeValueRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        AttributeRepository $attributeRepository,
        AttributevalueRepository $attributeValueRepository,
        EntityFromStructureConverter $entityFromStructureConverter,
        AttributeValueEntityFromStructureConverter $attributeValueEntityFromStructureConverter
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->attributeValueRepository = $attributeValueRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->attributeValueEntityFromStructureConverter = $attributeValueEntityFromStructureConverter;
    }

    /**
     * @param Attribute $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($localStructure, $entity, 'texts',
            'Attributetext');
        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations($localStructure, $entity, 'texts',
            'Attributetext');

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Attribute::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * handle multiple values when setting *_attribute relations as deleted.
     *
     * @param   mixed $structure
     * @param   mixed $entity
     * @param   string $relationName
     * @param   string $relationSourceName
     * @param   string $relationTargetName
     *
     * @return  mixed
     */
    public function setManyToManyRelationsDeleted(
        $structure,
        $entity,
        $relationName,
        $relationSourceName,
        $relationTargetName
    ) {
        $relationGetter = 'get' . $relationName;
        $relationTargetGetter = 'get' . $relationTargetName;

        /** @var ItemAttribute|OptionAttribute|OptionClassificationAttribute $relationEntity */
        foreach ($entity->$relationGetter() as $relationEntity) {
            $found = false;
            if (isset($structure->$relationSourceName)) {
                foreach ($structure->$relationSourceName as $relationStructure) {
                    foreach ($relationStructure->values as $attributeValueStructure) {
                        if ($relationEntity->$relationTargetGetter()->getId() == $relationStructure->attribute_id
                            && $relationEntity->getAttributevalue()->getId() == $attributeValueStructure->id
                        ) {
                            $found = true;
                        }
                    }
                }
            }

            if (!$found) {
                $relationEntity->setDateDeleted(new \DateTime());
            }
        }

        return $entity;
    }

    /**
     * handle multiple attribute values when adding item_attribute relations to the item entity.
     *
     * @param   mixed $structure
     * @param   mixed $entity
     * @param   string $relationClassName
     * @param   string $relationSourceName
     *
     * @return  \Doctrine\ORM\Mapping\Entity
     */
    public function addNewManyToManyRelations(
        $structure,
        $entity,
        $relationClassName,
        $relationSourceName
    ) {
        $classParts = explode('\\', $relationClassName);
        $relationName = $classParts[count($classParts) - 1];

        $relationGetter = 'get' . $relationName;
        $relationAdder = 'add' . $relationName;

        try {
            $sourceClass = new \ReflectionClass(get_class($entity));
        } catch (\ReflectionException $e) {
            $sourceClass = null;
        }

        if ($sourceClass) {
            $classParts = explode('\\', $sourceClass->name);
            $relationSourceSetter = 'set' . $classParts[count($classParts) - 1];

            if (isset($structure->$relationSourceName)) {
                foreach ($structure->$relationSourceName as $relationStructure) {
                    foreach ($relationStructure->values as $attributeValueStructure) {
                        $found = false;
                        /** @var ItemAttribute|OptionAttribute|OptionClassificationAttribute $relationEntity */
                        foreach ($entity->$relationGetter() as $relationEntity) {
                            if ($relationEntity->getAttribute()->getId() == $relationStructure->attribute_id
                                && $relationEntity->getAttributevalue()->getId() == $attributeValueStructure->id
                            ) {
                                $found = true;

                                break;
                            }
                        }

                        if (!$found) {
                            $targetEntity = $this->attributeRepository->findOneBy(['id' => $relationStructure->attribute_id]);
                            if (!empty($attributeValueStructure->id)) {
                                $attributeValue = $this->attributeValueRepository->findOneBy(['id' => $attributeValueStructure->id]);
                            } else {
                                $attributeValue = $this->attributeValueEntityFromStructureConverter->convertOne($attributeValueStructure);
                                $attributeValue = $this->attributeValueRepository->save($attributeValue);
                            }

                            /** @var ItemAttribute|OptionAttribute|OptionClassificationAttribute $newRelation */
                            $newRelation = new $relationClassName();
                            $newRelation->setAttribute($targetEntity);
                            $newRelation->$relationSourceSetter($entity);
                            $newRelation->setAttributevalue($attributeValue);

                            $entity->$relationAdder($newRelation);
                        }
                    }
                }
            }
        }

        return $entity;
    }
}
