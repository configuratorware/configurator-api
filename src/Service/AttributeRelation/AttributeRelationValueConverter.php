<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation;

class AttributeRelationValueConverter
{
    /**
     * @return bool|int|float|string
     */
    public static function convertAttributeValue(string $value, string $dataType)
    {
        switch ($dataType) {
            case 'boolean':
                return 'true' === $value || '1' === $value;
            case 'number':
                return (float)$value;
            case 'integer':
                return (int)$value;
            case 'string':
            default:
                return $value;
        }
    }
}
