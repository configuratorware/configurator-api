<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ApiBundle\Service\Converter\StructureHelper;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationAttribute;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\AttributeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\AttributeValue;

/**
 * @internal
 */
class FrontendAttributeRelationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureHelper
     */
    private $structureHelper;

    /**
     * FrontendAttributeRelationStructureFromEntityConverter constructor.
     *
     * @param StructureHelper $structureHelper
     */
    public function __construct(StructureHelper $structureHelper)
    {
        $this->structureHelper = $structureHelper;
    }

    /**
     * @param ItemAttribute|OptionAttribute|OptionClassificationAttribute $entity
     * @param string $structureClassName
     *
     * @return AttributeRelation
     */
    public function convertOne($entity, $structureClassName = AttributeRelation::class)
    {
        if (true === $entity->getAttribute()->getHideInFrontend()) {
            return null;
        }

        $attribute = $entity->getAttribute();
        $attributeIdentifier = $attribute->getIdentifier();

        /** @var AttributeRelation $structure */
        $structure = $this->structureHelper->getStructureClass($entity, $structureClassName);
        $structure->identifier = $attributeIdentifier;
        $structure->title = $attribute->getTranslatedTitle();
        $structure->attributedatatype = $attribute->getAttributedatatype();

        $attributeValue = $this->getAttributeValueToStructure($entity);
        $structure->values = [$attributeValue];

        return $structure;
    }

    /**
     * attribute values are collected and merged into the values array of one AttributeRelation structure
     * result is one AttributeRelation structure per attribute containing the accumulated values.
     *
     * @param array|Collection $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = AttributeRelation::class)
    {
        $structuresIndexed = [];
        /** @var ItemAttribute|OptionAttribute|OptionClassificationAttribute $entity */
        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);

            if (!isset($structuresIndexed[$structure->identifier])) {
                $structuresIndexed[$structure->identifier] = $structure;
            } else {
                $structuresIndexed[$structure->identifier]->values
                    = array_merge($structuresIndexed[$structure->identifier]->values, $structure->values);
            }
        }

        $structures = array_values($structuresIndexed);

        return $structures;
    }

    /**
     * @param ItemAttribute|OptionAttribute|OptionClassificationAttribute $attributeRelation
     *
     * @return AttributeValue
     */
    protected function getAttributeValueToStructure($attributeRelation)
    {
        /** @var AttributeValue $attributeValueStructure */
        $attributeValueStructure = $this->structureHelper->getStructureClass(null, AttributeValue::class);

        $attributeValueStructure->value = $attributeRelation->getAttributevalue()->getValue();
        $attributeValueStructure->translated_value = $attributeRelation->getAttributevalue()->getTranslatedValue();

        $convertedValue = AttributeRelationValueConverter::convertAttributeValue($attributeValueStructure->value, $attributeRelation->getAttribute()->getAttributedatatype());
        if (isset($convertedValue)) {
            $attributeValueStructure->translated_value = empty($attributeValueStructure->translated_value) ? $convertedValue : $attributeValueStructure->translated_value;
            $attributeValueStructure->value = $convertedValue;
        }

        return $attributeValueStructure;
    }
}
