<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationAttribute;
use Redhotmagma\ConfiguratorApiBundle\Service\Attribute\AttributeStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue\AttributeValueStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\AttributeRelation;

/**
 * @internal
 */
class AttributeRelationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var AttributeStructureFromEntityConverter
     */
    private $attributeStructureFromEntityConverter;

    /**
     * @var AttributeValueStructureFromEntityConverter
     */
    private $attributeValueStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * AttributeRelationStructureFromEntityConverter constructor.
     *
     * @param AttributeStructureFromEntityConverter $attributeStructureFromEntityConverter
     * @param AttributeValueStructureFromEntityConverter $attributeValueStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        AttributeStructureFromEntityConverter $attributeStructureFromEntityConverter,
        AttributeValueStructureFromEntityConverter $attributeValueStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->attributeStructureFromEntityConverter = $attributeStructureFromEntityConverter;
        $this->attributeValueStructureFromEntityConverter = $attributeValueStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param ItemAttribute|OptionAttribute|OptionClassificationAttribute $entity
     * @param string $structureClassName
     *
     * @return AttributeRelation
     */
    public function convertOne($entity, $structureClassName = AttributeRelation::class)
    {
        /** @var AttributeRelation $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addAttributeToStructure($structure, $entity->getAttribute());
        $structure = $this->addAttributeValueToStructure($structure, $entity->getAttributevalue());

        return $structure;
    }

    /**
     * attribute values are collected and merged into the values array of one AttributeRelation structure
     * result is one AttributeRelation structure per attribute containing the accumulated values.
     *
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = AttributeRelation::class)
    {
        $structuresIndexed = [];
        /** @var ItemAttribute|OptionAttribute|OptionClassificationAttribute $entity */
        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);

            if (!isset($structuresIndexed[$structure->identifier])) {
                $structuresIndexed[$structure->identifier] = $structure;
            } else {
                $structuresIndexed[$structure->identifier]->values
                    = array_merge($structuresIndexed[$structure->identifier]->values, $structure->values);
            }
        }

        $structures = array_values($structuresIndexed);

        return $structures;
    }

    /**
     * @param AttributeRelation $structure
     * @param Attribute $attribute
     *
     * @return AttributeRelation
     */
    protected function addAttributeToStructure($structure, $attribute)
    {
        $attributeStructure = $this->attributeStructureFromEntityConverter->convertOne($attribute);
        $structure->attribute_id = $attribute->getId();
        $structure->identifier = $attribute->getIdentifier();
        $structure->translated_title = $attribute->getTranslatedTitle();
        $structure->attributedatatype = $attribute->getAttributedatatype();
        $structure->texts = $attributeStructure->texts;

        return $structure;
    }

    /**
     * @param AttributeRelation $structure
     * @param $attributeValue
     *
     * @return AttributeRelation
     */
    protected function addAttributeValueToStructure($structure, $attributeValue)
    {
        $attributeValueStructure = $this->attributeValueStructureFromEntityConverter->convertOne($attributeValue);

        $convertedValue = AttributeRelationValueConverter::convertAttributeValue($attributeValueStructure->value, $structure->attributedatatype);
        if (isset($convertedValue)) {
            $attributeValueStructure->value = $convertedValue;
        }

        $structure->values[] = $attributeValueStructure;

        return $structure;
    }
}
