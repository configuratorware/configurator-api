<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rule;

use Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;

/**
 * @internal
 */
class RuleDelete
{
    /**
     * @var RuleRepository
     */
    private $ruleRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * RuleDelete constructor.
     *
     * @param RuleRepository $ruleRepository
     * @param OptionRepository $optionRepository
     */
    public function __construct(RuleRepository $ruleRepository, OptionRepository $optionRepository)
    {
        $this->ruleRepository = $ruleRepository;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);

        $additionalIdsToDelete = $this->getAdditionalRuleIdsForDelete($ids);
        $ids = array_merge($ids, $additionalIdsToDelete);

        foreach ($ids as $id) {
            $entity = $this->ruleRepository->find($id);

            if (null === $entity) {
                throw new NotFoundException();
            }

            // also delete the rules text entries
            if (!empty($entity->getRuleText()->toArray())) {
                foreach ($entity->getRuleText() as $ruleText) {
                    $this->ruleRepository->delete($ruleText);
                }
            }

            $this->ruleRepository->delete($entity);
        }
    }

    /**
     * @param $ids
     *
     * @return array
     */
    protected function getAdditionalRuleIdsForDelete($ids)
    {
        $idsToDelete = [];
        foreach ($ids as $id) {
            $entity = $this->ruleRepository->find($id);

            if (null !== $entity && Ruletype::OPTION_EXCLUSION === $entity->getRuletype()->getIdentifier()) {
                $reverseRule = $this->ruleRepository->findOptionExclusionReverseRule($id);

                $idsToDelete[] = $reverseRule->getId();
            }
        }

        return $idsToDelete;
    }
}
