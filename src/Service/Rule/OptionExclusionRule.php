<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rule;

use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidOptionExclusionRule;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rule as RuleStructure;

/**
 * @internal
 */
final class OptionExclusionRule
{
    /**
     * @var Rule
     */
    private $leadingRule;

    /**
     * @var Rule
     */
    private $reverseRule;

    /**
     * @param Rule $leadingRule
     * @param Rule $reverseRule
     */
    private function __construct(Rule $leadingRule, Rule $reverseRule)
    {
        $this->leadingRule = $leadingRule;
        $this->reverseRule = $reverseRule;
    }

    public static function initialize(): self
    {
        return new self(new Rule(), new Rule());
    }

    /**
     * @param Rule $leadingRule
     * @param Rule $reverseRule
     *
     * @return static
     */
    public static function fromEntities(Rule $leadingRule, Rule $reverseRule): self
    {
        return new self($leadingRule, $reverseRule);
    }

    /**
     * @param RuleStructure $structure
     * @param RuleEntityFromStructureConverter $converter
     */
    public function updateFromStructure(RuleStructure $structure, RuleEntityFromStructureConverter $converter): void
    {
        $this->leadingRule = $converter->convertOne($structure, $this->leadingRule);
        $this->reverseRule = $converter->convertOne($this->reverseFromLeading($structure), $this->reverseRule);
    }

    /**
     * @throws \Exception
     */
    public function updateReverseRules(): void
    {
        if (null === $this->leadingRule->getId() || null === $this->reverseRule->getId()) {
            throw InvalidOptionExclusionRule::missingReverseRuleIds();
        }

        $this->leadingRule->setReverseRule($this->reverseRule);
        $this->reverseRule->setReverseRule($this->leadingRule);
    }

    /**
     * @param RuleStructure $leadingRule
     *
     * @return RuleStructure
     */
    private function reverseFromLeading(RuleStructure $leadingRule): RuleStructure
    {
        $reverseRule = unserialize(serialize($leadingRule));
        $reverseRule->option->identifier = $leadingRule->data->option_identifier;
        $reverseRule->data->option_identifier = $leadingRule->option->identifier;

        return $reverseRule;
    }

    /**
     * @return Rule
     */
    public function getLeadingRule(): Rule
    {
        return $this->leadingRule;
    }

    /**
     * @return Rule
     */
    public function getReverseRule(): Rule
    {
        return $this->reverseRule;
    }
}
