<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rule;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\RuleText;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuletypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rule;

/**
 * @internal
 */
class RuleEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var RuletypeRepository
     */
    private $ruleTypeRepository;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var RuleTitleGenerator
     */
    private $ruleTitleGenerator;

    /**
     * RuleEntityFromStructureConverter constructor.
     *
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param ItemRepository $itemRepository
     * @param OptionRepository $optionRepository
     * @param RuletypeRepository $ruleTypeRepository
     * @param LanguageRepository $languageRepository
     * @param RuleTitleGenerator $ruleTitleGenerator
     */
    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository,
        RuletypeRepository $ruleTypeRepository,
        LanguageRepository $languageRepository,
        RuleTitleGenerator $ruleTitleGenerator
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
        $this->ruleTypeRepository = $ruleTypeRepository;
        $this->languageRepository = $languageRepository;
        $this->ruleTitleGenerator = $ruleTitleGenerator;
    }

    /**
     * @param Rule $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Rule
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Rule::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        $ruleFeedback = null;
        if (isset($structure->ruleType) && !empty($structure->ruleType->id)) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype $ruleType */
            $ruleType = $this->ruleTypeRepository->findOneBy(['id' => $structure->ruleType->id]);
            $localStructure->ruleType = $ruleType;

            // get rule feedback
            $ruleFeedback = $ruleType->getRuletypeRulefeedback()->first()->getRulefeedback();
        }

        if (isset($structure->item) && !empty($structure->item->id)) {
            $item = $this->itemRepository->findOneBy(['id' => $structure->item->id]);
            $localStructure->item = $item;
        }

        if (isset($structure->option) && !empty($structure->option->identifier)) {
            $option = $this->optionRepository->findOneBy(['identifier' => $structure->option->identifier]);
            $localStructure->option = $option;
        }

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity->setRulefeedback($ruleFeedback);
        $entity->setData(json_encode($localStructure->data));

        // generate/update rule texts
        $entity = $this->updateRuleTexts($entity);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $entityClassName
     *
     * @return array
     */
    public function convertMany($entities, $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Rule::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $entityClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $rule
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Rule
     */
    protected function updateRuleTexts($rule)
    {
        $languages = $this->languageRepository->findAll();

        /** @var Language $language */
        foreach ($languages as $language) {
            $found = false;
            /** @var RuleText $ruleText */
            foreach ($rule->getRuleText() as $ruleText) {
                if ($ruleText->getLanguage()->getId() == $language->getId()) {
                    $found = true;
                    $ruleTitle = $this->ruleTitleGenerator->getRuleTitle($rule, $language->getIso());
                    $ruleText->setTitle($ruleTitle);
                }
            }

            if (!$found) {
                $ruleText = new RuleText();
                $ruleText->setRule($rule);
                $ruleText->setLanguage($language);

                $ruleTitle = $this->ruleTitleGenerator->getRuleTitle($rule, $language->getIso());
                $ruleText->setTitle($ruleTitle);

                $rule->addRuleText($ruleText);
            }
        }

        return $rule;
    }
}
