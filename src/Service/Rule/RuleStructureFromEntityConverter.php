<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rule;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rule;
use Redhotmagma\ConfiguratorApiBundle\Structure\RuleType;

/**
 * @internal
 */
class RuleStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * RuleStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Rule $entity
     * @param string $structureClassName
     *
     * @return Rule
     */
    public function convertOne($entity, $structureClassName = Rule::class)
    {
        /** @var Rule $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        if (!empty($entity->getRuletype())) {
            $structure->ruleType = $this->structureFromEntityConverter->convertOne($entity->getRuletype(),
                RuleType::class);
        }

        if (!empty($entity->getItem())) {
            $structure->item = $this->structureFromEntityConverter->convertOne($entity->getItem(), Item::class);
        }

        if (!empty($entity->getOption())) {
            $structure->option = $this->structureFromEntityConverter->convertOne($entity->getOption(), Option::class);
        }

        $structure->data = json_decode($entity->getData());

        // generate and add rule title
        $structure->title = $entity->getTranslatedTitle();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Rule::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
