<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rule;

use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class RuleApi
{
    /**
     * @var RuleRepository
     */
    private $ruleRepository;

    /**
     * @var RuleStructureFromEntityConverter
     */
    private $ruleStructureFromEntityConverter;

    /**
     * @var RuleSave
     */
    private $ruleSave;

    /**
     * @var RuleDelete
     */
    private $ruleDelete;

    /**
     * RuleApi constructor.
     *
     * @param RuleRepository $ruleRepository
     * @param RuleStructureFromEntityConverter $ruleStructureFromEntityConverter
     * @param RuleSave $ruleSave
     * @param RuleDelete $ruleDelete
     */
    public function __construct(
        RuleRepository $ruleRepository,
        RuleStructureFromEntityConverter $ruleStructureFromEntityConverter,
        RuleSave $ruleSave,
        RuleDelete $ruleDelete
    ) {
        $this->ruleRepository = $ruleRepository;
        $this->ruleStructureFromEntityConverter = $ruleStructureFromEntityConverter;
        $this->ruleSave = $ruleSave;
        $this->ruleDelete = $ruleDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->ruleRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->ruleStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->ruleRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Rule
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Rule
    {
        /** @var Rule $entity */
        $entity = $this->ruleRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->ruleStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Rule $rule
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Rule
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Rule $rule)
    {
        $structure = $this->ruleSave->save($rule);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->ruleDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['ruleText.title'];
    }
}
