<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rule;

use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Entity\Ruletype;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuleRepository;

/**
 * @internal
 */
class RuleSave
{
    /**
     * @var RuleRepository
     */
    private $ruleRepository;

    /**
     * @var RuleEntityFromStructureConverter
     */
    private $ruleEntityFromStructureConverter;

    /**
     * @var RuleStructureFromEntityConverter
     */
    private $ruleStructureFromEntityConverter;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * RuleSave constructor.
     *
     * @param RuleRepository $ruleRepository
     * @param RuleEntityFromStructureConverter $ruleEntityFromStructureConverter
     * @param RuleStructureFromEntityConverter $ruleStructureFromEntityConverter
     * @param OptionRepository $optionRepository
     */
    public function __construct(
        RuleRepository $ruleRepository,
        RuleEntityFromStructureConverter $ruleEntityFromStructureConverter,
        RuleStructureFromEntityConverter $ruleStructureFromEntityConverter,
        OptionRepository $optionRepository
    ) {
        $this->ruleRepository = $ruleRepository;
        $this->ruleEntityFromStructureConverter = $ruleEntityFromStructureConverter;
        $this->ruleStructureFromEntityConverter = $ruleStructureFromEntityConverter;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Rule $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Rule
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Rule $structure)
    {
        if (Ruletype::OPTION_EXCLUSION === $structure->ruleType->identifier) {
            return $this->handleOptionExclusion($structure);
        }

        $entity = null;
        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->ruleRepository->findOneBy(['id' => $structure->id]);
        }
        $entity = $this->ruleEntityFromStructureConverter->convertOne($structure, $entity);
        $entity = $this->ruleRepository->save($entity);

        /** @var Rule $entity */
        $entity = $this->ruleRepository->findOneBy(['id' => $entity->getId()]);

        return $this->ruleStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Rule $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Rule
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function handleOptionExclusion(\Redhotmagma\ConfiguratorApiBundle\Structure\Rule $structure): \Redhotmagma\ConfiguratorApiBundle\Structure\Rule
    {
        if ($structure->id > 0) {
            $leadingRule = $this->ruleRepository->find($structure->id);
            $reverseRule = $this->ruleRepository->findOptionExclusionReverseRule($structure->id);
            $optionExclusionRule = OptionExclusionRule::fromEntities($leadingRule, $reverseRule);
        } else {
            $optionExclusionRule = OptionExclusionRule::initialize();
        }

        $optionExclusionRule->updateFromStructure($structure, $this->ruleEntityFromStructureConverter);

        // saving the rules twice is needed to get Ids  for reverseRules on initial save
        $this->ruleRepository->save($optionExclusionRule->getLeadingRule());
        $this->ruleRepository->save($optionExclusionRule->getReverseRule());

        $optionExclusionRule->updateReverseRules();

        $this->ruleRepository->save($optionExclusionRule->getLeadingRule());
        $this->ruleRepository->save($optionExclusionRule->getReverseRule());

        return $this->ruleStructureFromEntityConverter->convertOne($optionExclusionRule->getLeadingRule());
    }
}
