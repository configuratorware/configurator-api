<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Rule;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;

/**
 * @internal
 */
class RuleTitleGenerator
{
    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionclassificationRepository
     */
    private $optionClassificationRepository;

    /**
     * RuleTitleGenerator constructor.
     *
     * @param AttributeRepository $attributeRepository
     * @param OptionRepository $optionRepository
     * @param OptionclassificationRepository $optionClassificationRepository
     */
    public function __construct(
        AttributeRepository $attributeRepository,
        OptionRepository $optionRepository,
        OptionclassificationRepository $optionClassificationRepository
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->optionRepository = $optionRepository;
        $this->optionClassificationRepository = $optionClassificationRepository;
    }

    /**
     * $languageIso is optional, if not specified the current selected language is delivered.
     *
     * @param Rule $rule
     * @param string $languageIso
     *
     * @return string
     */
    public function getRuleTitle(Rule $rule, $languageIso = null): string
    {
        $title = '';

        $ruleTitleTemplate = $rule->getRuletype()->getTranslatedRuleTitle($languageIso);
        $ruleData = json_decode($rule->getData());

        if (!empty($ruleTitleTemplate)) {
            // search for placeholders and get the data to fill in
            preg_match_all('/{[^{}]*}/', $ruleTitleTemplate, $placeholders);

            $search = [];
            $replace = [];

            if (!empty($placeholders)) {
                foreach ($placeholders[0] as $placeholder) {
                    $search[] = $placeholder;

                    preg_match('/(\[.*\])/', $placeholder, $placeholderBoolSelectionMatches);

                    $placeholderBoolSelection = '';
                    if (!empty($placeholderBoolSelectionMatches)) {
                        $placeholderBoolSelection = $placeholderBoolSelectionMatches[0];
                    }
                    $placeholderSwitch = str_replace($placeholderBoolSelection, '', $placeholder);

                    switch ($placeholderSwitch) {
                        case '{rule_option}':
                            $replace[] = $rule->getOption()->getTranslatedTitle($languageIso);

                            break;

                        case '{rule_data_option}':
                            /** @var Option $option */
                            $option = $this->optionRepository->findOneBy(['identifier' => $ruleData->option_identifier]);
                            $replace[] = $option->getTranslatedTitle($languageIso);

                            break;

                        case '{rule_data_option_classification}':
                            /** @var Optionclassification $optionClassification */
                            $optionClassification = $this->optionClassificationRepository->findOneBy(['identifier' => $ruleData->optionclassification_identifier]);
                            $replace[] = $optionClassification->getTranslatedTitle($languageIso);

                            break;

                        case '{rule_data_min_amount}':
                            $replace[] = $ruleData->minamount;

                            break;

                        case '{rule_data_max_amount}':
                            $replace[] = $ruleData->maxamount;

                            break;

                        case '{rule_data_attribute}':
                            /** @var Attribute $attribute */
                            $attribute = $this->attributeRepository->findOneBy(['identifier' => $ruleData->attribute_identifier]);
                            $replace[] = $attribute->getTranslatedTitle($languageIso);

                            break;

                        case '{rule_data_apply_per_option_classification}':
                            $placeholderBoolSelections = explode('|', trim($placeholderBoolSelection, '[]'));
                            $perOptionClassification = $ruleData->apply_per_option_classification ? $placeholderBoolSelections[0] : $placeholderBoolSelections[1];

                            $replace[] = $perOptionClassification;

                            break;

                        default:
                            $replace[] = '';
                    }
                }

                $title = str_replace($search, $replace, $ruleTitleTemplate);
            }
        }

        return $title;
    }
}
