<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree as QuestionTreeEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTreeText;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTreeData;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTreeText as QuestionTreeTextStructure;

/**
 * @internal
 */
class QuestionTreeStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    private StructureFromEntityConverter $structureFromEntityConverter;

    private ?string $currentLanguageIso;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->currentLanguageIso = null;
    }

    public function withLanguageIsoCode(string $languageIsoCode): self
    {
        $this->currentLanguageIso = $languageIsoCode;

        return $this;
    }

    /**
     * @param QuestionTreeEntity $entity
     * @param string       $structureClassName
     *
     * @return QuestionTree
     */
    public function convertOne($entity, $structureClassName = QuestionTree::class): QuestionTree
    {
        /** @var QuestionTree $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $structure = $this->addQuestionTreeTextsToStructure($structure, $entity);
        $structure = $this->addQuestionTreeDataToStructure($structure, $entity);
        $structure = $this->addTranslatedTextToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param QuestionTreeEntity[]|array $entities
     * @param string               $structureClassName
     *
     * @return QuestionTree[]|array
     */
    public function convertMany($entities, $structureClassName = QuestionTree::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }

    protected function addQuestionTreeTextsToStructure(QuestionTree $structure, QuestionTreeEntity $entity): QuestionTree
    {
        /** @var QuestionTreeText $questionTreeText */
        foreach ($entity->getQuestionTreeText() as $questionTreeText) {
            $textStructure = $this->structureFromEntityConverter->convertOne(
                $questionTreeText,
                QuestionTreeTextStructure::class
            );

            if ($textStructure instanceof QuestionTreeTextStructure) {
                $textStructure->language = $questionTreeText->getLanguage()->getIso();
                $structure->texts[] = $textStructure;
            }
        }

        return $structure;
    }

    protected function addQuestionTreeDataToStructure(QuestionTree $structure, QuestionTreeEntity $entity): QuestionTree
    {
        /** @var QuestionTreeData $questionTreeData */
        foreach ($entity->getQuestionTreeData() as $questionTreeData) {
            $dataStructure = $this->structureFromEntityConverter->convertOne(
                $questionTreeData,
                QuestionTreeData::class
            );

            if ($dataStructure instanceof QuestionTreeData) {
                $dataStructure->language = $questionTreeData->getLanguage()->getIso();
                $structure->data[] = $dataStructure;
            }
        }

        return $structure;
    }

    protected function addTranslatedTextToStructure(QuestionTree $structure, QuestionTreeEntity $entity): QuestionTree
    {
        $title = null;

        /** @var QuestionTreeText $text */
        foreach ($entity->getQuestionTreeText() as $text) {
            $title = $text->getTitle();
            if ($text->getLanguage() instanceof Language && $this->currentLanguageIso === $text->getLanguage()->getIso()) {
                break;
            }
        }

        $structure->translated_title = $title;

        return $structure;
    }
}
