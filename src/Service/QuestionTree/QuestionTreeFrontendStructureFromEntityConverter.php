<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree as QuestionTreeEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTreeData;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTreeText;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\QuestionTree;

/**
 * @internal
 */
class QuestionTreeFrontendStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    private StructureFromEntityConverter $structureFromEntityConverter;

    private ?string $currentLanguageIso;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->currentLanguageIso = null;
    }

    public function withLanguageIsoCode(string $languageIsoCode): self
    {
        $this->currentLanguageIso = $languageIsoCode;

        return $this;
    }

    /**
     * @param QuestionTreeEntity $entity
     * @param string             $structureClassName
     *
     * @return QuestionTree
     */
    public function convertOne($entity, $structureClassName = QuestionTree::class): QuestionTree
    {
        /** @var QuestionTree $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $structure = $this->addQuestionTreeDataToStructure($structure, $entity);
        $structure = $this->addTranslatedTextToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param QuestionTreeEntity[]|array $entities
     * @param string                     $structureClassName
     *
     * @return QuestionTree[]|array
     */
    public function convertMany($entities, $structureClassName = QuestionTree::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }

    protected function addQuestionTreeDataToStructure(QuestionTree $structure, QuestionTreeEntity $entity): QuestionTree
    {
        /** @var QuestionTreeData $questionTreeData */
        foreach ($entity->getQuestionTreeData() as $questionTreeData) {
            $structure->data = $questionTreeData->getData();
        }

        return $structure;
    }

    protected function addTranslatedTextToStructure(QuestionTree $structure, QuestionTreeEntity $entity): QuestionTree
    {
        $title = null;

        /** @var QuestionTreeText $text */
        foreach ($entity->getQuestionTreeText() as $text) {
            $title = $text->getTitle();
            if ($text->getLanguage() instanceof Language && $this->currentLanguageIso === $text->getLanguage()->getIso()) {
                break;
            }
        }

        $structure->title = $title;

        return $structure;
    }
}
