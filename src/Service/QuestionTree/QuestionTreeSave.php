<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree;

use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Repository\QuestionTreeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree\Model\DefaultLanguage;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTree as QuestionTreeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;

/**
 * @internal
 */
class QuestionTreeSave
{
    private QuestionTreeRepository $repository;

    private QuestionTreeEntityFromStructureConverter $entityFromStructureConverter;

    private QuestionTreeStructureFromEntityConverter $structureFromEntityConverter;

    private DefaultLanguage $defaultLanguage;

    public function __construct(
        QuestionTreeRepository $repository,
        QuestionTreeEntityFromStructureConverter $entityFromStructureConverter,
        QuestionTreeStructureFromEntityConverter $structureFromEntityConverter,
        DefaultLanguage $defaultLanguage
    ) {
        $this->repository = $repository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->defaultLanguage = $defaultLanguage;
    }

    public function save(QuestionTreeStructure $structure, ControlParameters $parameters): QuestionTreeStructure
    {
        $entity = null;

        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->repository->findOneWithTexts($structure->id);
        }

        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity);
        $entity = $this->repository->save($entity);

        $this->repository->clear();

        /** @var QuestionTree $entity */
        $entity = $this->repository->findOneWithTexts($entity->getId());
        $language = $parameters->getParameter(ControlParameters::LANGUAGE, $this->defaultLanguage->getIso());

        $structure = $this->structureFromEntityConverter->withLanguageIsoCode($language)->convertOne($entity);

        return $structure;
    }
}
