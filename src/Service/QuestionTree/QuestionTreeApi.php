<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\QuestionTreeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree\Model\DefaultLanguage;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\QuestionTree as FrontendQuestionTreeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTree as QuestionTreeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\QuestionTreeUploadData;

class QuestionTreeApi
{
    private QuestionTreeRepository $questionTreeRepository;

    private QuestionTreeStructureFromEntityConverter $structureFromEntityConverter;

    private QuestionTreeFrontendStructureFromEntityConverter $frontendStructureFromEntityConverter;

    private QuestionTreeSave $questionTreeSave;

    private QuestionTreeUpload $questionTreeUpload;

    private DefaultLanguage $defaultLanguage;

    public function __construct(
        QuestionTreeRepository $questionTreeRepository,
        QuestionTreeStructureFromEntityConverter $structureFromEntityConverter,
        QuestionTreeFrontendStructureFromEntityConverter $frontendStructureFromEntityConverter,
        QuestionTreeSave $questionTreeSave,
        QuestionTreeUpload $questionTreeUpload,
        DefaultLanguage $defaultLanguage
    ) {
        $this->questionTreeRepository = $questionTreeRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->frontendStructureFromEntityConverter = $frontendStructureFromEntityConverter;
        $this->questionTreeSave = $questionTreeSave;
        $this->questionTreeUpload = $questionTreeUpload;
        $this->defaultLanguage = $defaultLanguage;
    }

    public function getFrontendDetail(string $identifier, ControlParameters $parameters): FrontendQuestionTreeStructure
    {
        $language = $parameters->getParameter(ControlParameters::LANGUAGE, $this->defaultLanguage->getIso());
        $entity = $this->questionTreeRepository->findOneByIdentifierAndLanguageISO($identifier, $language);

        if (null === $entity) {
            throw new NotFoundException();
        }

        return $this->frontendStructureFromEntityConverter->withLanguageIsoCode($language)->convertOne($entity);
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments, ControlParameters $parameters): PaginationResult
    {
        $language = $parameters->getParameter(ControlParameters::LANGUAGE, $this->defaultLanguage->getIso());
        $entities = $this->questionTreeRepository->fetchListWithTexts($arguments, $this->getSearchableFields());

        $paginationResult = new PaginationResult();
        $paginationResult->data = $this->structureFromEntityConverter->withLanguageIsoCode($language)
            ->convertMany($entities);
        $paginationResult->count = $this->questionTreeRepository->fetchListCount($arguments, $this->getSearchableFields());

        return $paginationResult;
    }

    public function getOne(int $id, ControlParameters $parameters): QuestionTreeStructure
    {
        $language = $parameters->getParameter(ControlParameters::LANGUAGE, $this->defaultLanguage->getIso());
        $entity = $this->questionTreeRepository->findOneWithTexts($id);

        if (null === $entity) {
            throw new NotFoundException();
        }

        return $this->structureFromEntityConverter->withLanguageIsoCode($language)->convertOne($entity);
    }

    public function save(QuestionTreeStructure $structure, ControlParameters $parameters): QuestionTreeStructure
    {
        $structure = $this->questionTreeSave->save($structure, $parameters);

        return $structure;
    }

    public function upload(QuestionTreeUploadData $uploadData): bool
    {
        return $this->questionTreeUpload->saveData($uploadData);
    }

    public function delete(array $idList): void
    {
        foreach ($idList as $id) {
            $entity = $this->questionTreeRepository->find($id);

            if (null === $entity) {
                throw new NotFoundException();
            }

            $this->questionTreeRepository->delete($entity);
        }
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['identifier', 'optionText.title'];
    }
}
