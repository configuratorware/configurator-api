<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree;

use Doctrine\ORM\EntityManagerInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTreeData;
use Redhotmagma\ConfiguratorApiBundle\Repository\QuestionTreeDataRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\QuestionTreeUploadData;

/**
 * @internal
 */
class QuestionTreeUpload
{
    private QuestionTreeDataRepository $repository;

    private EntityManagerInterface $entityManager;

    public function __construct(QuestionTreeDataRepository $repository, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }

    public function saveData(QuestionTreeUploadData $uploadData): bool
    {
        $entity = $this->repository->findOneByLanguageIdAndQuestionTreeId(
            $uploadData->languageId,
            $uploadData->questionTreeId
        );

        if (null === $entity) {
            $entity = new QuestionTreeData();
            $language = $this->entityManager->getReference(Language::class, $uploadData->languageId);
            $questionTree = $this->entityManager->getReference(QuestionTree::class, $uploadData->questionTreeId);
            $entity->setLanguage($language);
            $entity->setQuestionTree($questionTree);
        }

        $entity->setData($uploadData->dataFileFormData->getContent());
        $entity->setFileName($uploadData->dataFileFormData->getClientOriginalName());
        $entity = $this->repository->save($entity);

        return $entity instanceof QuestionTreeData;
    }
}
