<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree\Model;

use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;

class DefaultLanguage
{
    private LanguageRepository $repository;

    public function __construct(LanguageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getIso(): string
    {
        $defaultLanguage = $this->repository->findDefault();

        if ($defaultLanguage instanceof Language) {
            return $defaultLanguage->getIso();
        }

        return '';
    }
}
