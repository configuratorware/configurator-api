<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\QuestionTree;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\QuestionTree;
use Redhotmagma\ConfiguratorApiBundle\Structure\QuestionTree as QuestionTreeStructure;

/**
 * @internal
 */
class QuestionTreeEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    private EntityFromStructureConverter $entityFromStructureConverter;

    public function __construct(EntityFromStructureConverter $entityFromStructureConverter)
    {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param QuestionTreeStructure $structure
     * @param QuestionTree|null     $entity
     * @param string                $entityClassName
     *
     * @return QuestionTree
     */
    public function convertOne($structure, $entity = null, $entityClassName = QuestionTree::class): QuestionTree
    {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var QuestionTree $entity */
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($localStructure, $entity, 'texts',
            'QuestionTreeText');
        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations($localStructure, $entity, 'texts',
            'QuestionTreeText');

        return $entity;
    }
}
