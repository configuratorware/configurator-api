<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette;

use Redhotmagma\ConfiguratorApiBundle\Entity\Color;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorPaletteRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPalette as ColorPaletteStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ColorPaletteApi
{
    /**
     * @var ColorPaletteRepository
     */
    private $colorPaletteRepository;

    /**
     * @var ColorPaletteStructureFromEntityConverter
     */
    private $colorPaletteStructureFromEntityConverter;

    /**
     * @var ColorPaletteSave
     */
    private $colorPaletteSave;

    /**
     * @var ColorPaletteDelete
     */
    private $colorPaletteDelete;

    /**
     * @param ColorPaletteRepository $colorPaletteRepository
     * @param ColorPaletteStructureFromEntityConverter $colorPaletteStructureFromEntityConverter
     * @param ColorPaletteSave $colorPaletteSave
     * @param ColorPaletteDelete $colorPaletteDelete
     */
    public function __construct(
        ColorPaletteRepository $colorPaletteRepository,
        ColorPaletteStructureFromEntityConverter $colorPaletteStructureFromEntityConverter,
        ColorPaletteSave $colorPaletteSave,
        ColorPaletteDelete $colorPaletteDelete
    ) {
        $this->colorPaletteRepository = $colorPaletteRepository;
        $this->colorPaletteStructureFromEntityConverter = $colorPaletteStructureFromEntityConverter;
        $this->colorPaletteSave = $colorPaletteSave;
        $this->colorPaletteDelete = $colorPaletteDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(
        ListRequestArguments $arguments
    ): PaginationResult {
        $paginationResult = new PaginationResult();

        $entities = $this->colorPaletteRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult->data = $this->colorPaletteStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->colorPaletteRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return ColorPaletteStructure
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): ColorPaletteStructure
    {
        $entity = $this->colorPaletteRepository->findOneById($id);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->colorPaletteStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param ColorPaletteStructure $colorPalette
     *
     * @return ColorPaletteStructure
     */
    public function save(
        ColorPaletteStructure $colorPalette
    ): ColorPaletteStructure {
        $structure = $this->colorPaletteSave->save($colorPalette);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $this->colorPaletteDelete->delete($id);
    }

    public function saveColorSequenceNumbers($id, SequenceNumberArguments $sequenceNumbers)
    {
        $colorPalette = $this->colorPaletteRepository->findOneById($id);
        if (!$colorPalette) {
            throw new NotFoundHttpException("Color Palette {$id} not found.");
        }

        foreach ($sequenceNumbers->data as $sequenceData) {
            $color = $colorPalette->getColor()->filter(function (Color $color) use ($sequenceData) {
                return $color->getId() == $sequenceData->id;
            })->first();

            if ($color) {
                $color->setSequenceNumber($sequenceData->sequencenumber);
                $this->colorPaletteRepository->save($color, false);
            }
        }

        $this->colorPaletteRepository->flush();
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['identifier'];
    }
}
