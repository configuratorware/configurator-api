<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette;

use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorPaletteRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPalette as ColorPaletteStructure;

/**
 * @internal
 */
class ColorPaletteSave
{
    /**
     * @var ColorPaletteRepository
     */
    private $colorPaletteRepository;

    /**
     * @var ColorPaletteEntityFromStructureConverter
     */
    private $colorPaletteEntityFromStructureConverter;

    /**
     * @var ColorPaletteStructureFromEntityConverter
     */
    private $colorPaletteStructureFromEntityConverter;

    /**
     * @param ColorPaletteRepository $colorPaletteRepository
     * @param ColorPaletteEntityFromStructureConverter $colorPaletteEntityFromStructureConverter
     * @param ColorPaletteStructureFromEntityConverter $colorPaletteStructureFromEntityConverter
     */
    public function __construct(
        ColorPaletteRepository $colorPaletteRepository,
        ColorPaletteEntityFromStructureConverter $colorPaletteEntityFromStructureConverter,
        ColorPaletteStructureFromEntityConverter $colorPaletteStructureFromEntityConverter
    ) {
        $this->colorPaletteRepository = $colorPaletteRepository;
        $this->colorPaletteEntityFromStructureConverter = $colorPaletteEntityFromStructureConverter;
        $this->colorPaletteStructureFromEntityConverter = $colorPaletteStructureFromEntityConverter;
    }

    /**
     * @param ColorPaletteStructure $colorPalette
     *
     * @return ColorPaletteStructure
     */
    public function save(ColorPaletteStructure $colorPalette): ColorPaletteStructure
    {
        $entity = null;

        if (isset($colorPalette->id) && $colorPalette->id > 0) {
            $entity = $this->colorPaletteRepository->findOneBy(['id' => $colorPalette->id]);
        }

        $entity = $this->colorPaletteEntityFromStructureConverter->convertOne($colorPalette, $entity);
        $entity = $this->colorPaletteRepository->save($entity);

        $this->colorPaletteRepository->clear();

        /** @var ColorPalette $entity */
        $entity = $this->colorPaletteRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->colorPaletteStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
