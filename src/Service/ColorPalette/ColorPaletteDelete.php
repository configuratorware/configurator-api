<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette;

use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorPaletteRepository;

/**
 * @internal
 */
class ColorPaletteDelete
{
    /**
     * @var ColorPaletteRepository
     */
    private $colorPaletteRepository;

    /**
     * @param ColorPaletteRepository $colorPaletteRepository
     */
    public function __construct(
        ColorPaletteRepository $colorPaletteRepository
    ) {
        $this->colorPaletteRepository = $colorPaletteRepository;
    }

    /**
     * @param string $id
     */
    public function delete(string $id): void
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var ColorPalette $entity */
            $entity = $this->colorPaletteRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            // Delete the Colors and their texts added to the color palette
            if (!$entity->getColor()->isEmpty()) {
                foreach ($entity->getColor() as $color) {
                    if (!$color->getColorText()->isEmpty()) {
                        foreach ($color->getColorText() as $colorText) {
                            $this->colorPaletteRepository->delete($colorText);
                        }
                    }
                    $this->colorPaletteRepository->delete($color);
                }
            }

            // Delete the Colors palettes texts
            $this->removeRelation($entity, 'ColorPaletteText');

            // Delete the Colors palettes relation to Design Production Method
            $this->removeRelation($entity, 'DesignProductionMethodColorPalette');

            $this->colorPaletteRepository->delete($entity);
        }
    }

    /**
     * @param ColorPalette $entity
     * @param string $relationName
     */
    protected function removeRelation(ColorPalette $entity, string $relationName): void
    {
        $callEntity = 'get' . $relationName;
        /** @var mixed $relations */
        $relations = $entity->$callEntity();
        if (!$relations->isEmpty()) {
            foreach ($relations as $relationEntry) {
                $this->colorPaletteRepository->delete($relationEntry);
            }
        }
    }
}
