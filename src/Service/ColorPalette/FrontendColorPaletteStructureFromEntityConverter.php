<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Color as FrontendColorStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ColorPalette as FrontendColorPaletteStructure;

/**
 * @internal
 */
final class FrontendColorPaletteStructureFromEntityConverter
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param ColorPalette $entity
     *
     * @return FrontendColorPaletteStructure
     */
    public function convertOne(ColorPalette $entity): FrontendColorPaletteStructure
    {
        /* @var array $colorPaletteStructure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, FrontendColorPaletteStructure::class);

        $structure->title = $entity->getTranslatedTitle();

        $structure->colors = $this->addColors($entity);

        $structure->defaultColor = $this->addDefaultColor($entity);

        return $structure;
    }

    /**
     * @param ColorPalette[] $entities
     *
     * @return FrontendColorPaletteStructure[]
     */
    public function convertMany(array $entities): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    /**
     * @param ColorPalette $entity
     *
     * @return array
     */
    private function addColors(ColorPalette $entity): array
    {
        $colors = [];
        foreach ($entity->getColor() as $color) {
            $colorStructure = $this->structureFromEntityConverter->convertOne($color, FrontendColorStructure::class);

            $colorStructure->title = $color->getTranslatedTitle();
            $colors[] = $colorStructure;
        }

        return $colors;
    }

    /**
     * @param ColorPalette $entity
     *
     * @return FrontendColorStructure|null
     */
    private function addDefaultColor(ColorPalette $entity): ?FrontendColorStructure
    {
        $defaultColor = $entity->getDefaultColor();
        if ($defaultColor) {
            $colorStructure = $this->structureFromEntityConverter->convertOne($defaultColor, FrontendColorStructure::class);
            $colorStructure->title = $defaultColor->getTranslatedTitle();

            return $colorStructure;
        }

        return null;
    }
}
