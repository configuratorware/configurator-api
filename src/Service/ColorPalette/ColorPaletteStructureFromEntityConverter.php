<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Color;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPaletteText;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorText;
use Redhotmagma\ConfiguratorApiBundle\Structure\Color as ColorStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPalette as ColorPaletteStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPaletteText as ColorPaletteTextStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorText as ColorTextStructure;

/**
 * @internal
 */
class ColorPaletteStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette $entity
     * @param string $structureClassName
     *
     * @return ColorPaletteStructure
     */
    public function convertOne($entity, $structureClassName = ColorPaletteStructure::class)
    {
        /** @var ColorPaletteStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addTextsToStructure(
            $structure,
            $entity,
            'ColorPalette',
            ColorPaletteTextStructure::class,
            'texts',
            'Text'
        );

        $structure = $this->addColorsToStructure($structure, $entity);
        $structure = $this->addDefaultColorToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ColorPaletteStructure::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param ColorPaletteStructure $structure
     * @param ColorPalette $entity
     * @param string $entityName
     * @param string $textStructureClass
     * @param string $textStructureName
     * @param string $textEntityName
     *
     * @return ColorPaletteStructure
     */
    private function addTextsToStructure(
        ColorPaletteStructure $structure,
        ColorPalette $entity,
        string $entityName,
        string $textStructureClass,
        string $textStructureName,
        string $textEntityName
    ): ColorPaletteStructure {
        $callEntity = 'get' . $entityName . $textEntityName;

        /** @var ColorPalette $entity */
        $texts = $entity->$callEntity();

        $result = [];
        if (!empty($texts)) {
            /** @var ColorPaletteText $text */
            foreach ($texts as $text) {
                /** @var ColorPaletteTextStructure $textStructure */
                $textStructure = $this->structureFromEntityConverter->convertOne(
                    $text,
                    $textStructureClass
                );

                if (!empty($textStructure)) {
                    $textStructure->language = $text->getLanguage()->getIso();
                    $result[] = $textStructure;
                }
            }
        }

        $structure->$textStructureName = $result;

        return $structure;
    }

    /**
     * @param ColorPaletteStructure $structure
     * @param ColorPalette $entity
     *
     * @return ColorPaletteStructure
     */
    private function addColorsToStructure(
        ColorPaletteStructure $structure,
        ColorPalette $entity
    ): ColorPaletteStructure {
        $colors = [];
        $colorPaletteColors = $entity->getColor();

        if (!empty($colorPaletteColors)) {
            /** @var Color $colorPaletteColor */
            foreach ($colorPaletteColors as $colorPaletteColor) {
                /** @var ColorStructure $colorStructure */
                $colorStructure = $this->structureFromEntityConverter->convertOne(
                    $colorPaletteColor,
                    ColorStructure::class
                );
                $colorStructure = $this->addColorTextsToColorStructure(
                    $colorStructure,
                    $colorPaletteColor
                );
                $colors[] = $colorStructure;
            }
        }

        $structure->colors = $colors;

        return $structure;
    }

    /**
     * @param ColorStructure $structure
     * @param Color $entity
     *
     * @return ColorStructure
     */
    private function addColorTextsToColorStructure($structure, $entity): ColorStructure
    {
        $texts = [];

        $colorTexts = $entity->getColorText();

        if (!empty($colorTexts)) {
            /** @var ColorText $colorText */
            foreach ($colorTexts as $colorText) {
                $colorTextStructure = $this->structureFromEntityConverter->convertOne(
                    $colorText,
                    ColorTextStructure::class
                );

                if (!empty($colorTextStructure)) {
                    $colorTextStructure->language = $colorText->getLanguage()
                        ->getIso();
                    $texts[] = $colorTextStructure;
                }
            }
        }

        $structure->texts = $texts;

        return $structure;
    }

    /**
     * @param ColorPaletteStructure $structure
     * @param ColorPalette          $entity
     *
     * @return ColorPaletteStructure
     */
    private function addDefaultColorToStructure(
        ColorPaletteStructure $structure,
        ColorPalette $entity
    ): ColorPaletteStructure {
        $defaultColor = $entity->getDefaultColor();
        if ($defaultColor) {
            $structure->defaultColor = $this->structureFromEntityConverter->convertOne(
                $defaultColor,
                ColorStructure::class
            );
            $structure->defaultColor = $this->addColorTextsToColorStructure(
                $structure->defaultColor,
                $defaultColor
            );
        }

        return $structure;
    }
}
