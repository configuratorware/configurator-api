<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette;

use Doctrine\Common\Collections\Criteria;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Color;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Entity\ColorText;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Color as ColorStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPalette as ColorPaletteStructure;

/**
 * @internal
 */
class ColorPaletteEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var ColorRepository
     */
    private $colorRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    private const TEXT_STRUCTURE_NAME = 'texts';

    private const TEXT_ENTITY_NAME = 'ColorText';

    /**
     * @param ColorRepository $colorRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        ColorRepository $colorRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->colorRepository = $colorRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param ColorPaletteStructure $structure
     * @param ColorPalette $entity
     * @param string $entityClassName
     *
     * @return ColorPalette
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = ColorPalette::class
    ): ColorPalette {
        $defaultColorStructure = $structure->defaultColor;
        $structure->defaultColor = null;

        /** @var ColorPalette $entity */
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        $entity = $this->updateManyToOneRelations(
            $entity,
            $structure,
            'texts',
            'ColorPaletteText'
        );

        $entity = $this->updateManyToOneRelations(
            $entity,
            $structure,
            'colors',
            'Color'
        );

        /**
         * @var ColorPaletteStructure $colorStructure
         * @var ColorPalette $colorEntity
         */
        $colorStructureByIdentifierKey = [];
        foreach ($structure->colors as $colorStructure) {
            $colorStructureByIdentifierKey[$colorStructure->identifier] = $colorStructure;
        }
        $colorEntityByIdentifierKey = [];
        foreach ($entity->getColor() as $colorEntity) {
            $colorEntityByIdentifierKey[$colorEntity->getIdentifier()] = $colorEntity;
        }

        if ($defaultColorStructure instanceof ColorStructure && null !== $defaultColorStructure->identifier) {
            if ($this->isDefaultColorDeleted($defaultColorStructure->identifier, $colorEntityByIdentifierKey, $colorStructureByIdentifierKey)) {
                $entity->setDefaultColor(null);
            } else {
                $entity->setDefaultColor($this->findDefaultColor($entity, $defaultColorStructure));
            }
        }

        $entity = $this->updateRelationTexts(
            $entity,
            $colorEntityByIdentifierKey,
            $colorStructureByIdentifierKey
        );

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ColorPalette::class): array
    {
        $structures = [];
        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param ColorPalette $entity
     * @param ColorPaletteStructure $structure
     * @param string $relationStructureName
     * @param string $relationEntityName
     *
     * @return ColorPalette
     */
    protected function updateManyToOneRelations(
        ColorPalette $entity,
        ColorPaletteStructure $structure,
        string $relationStructureName,
        string $relationEntityName
    ): ColorPalette {
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        return $entity;
    }

    /**
     * @param ColorPalette $entity
     * @param array $colorEntityByIdentifierKey
     * @param array $colorStructureByIdentifierKey
     *
     * @return ColorPalette
     */
    protected function updateRelationTexts(
        ColorPalette $entity,
        array $colorEntityByIdentifierKey,
        array $colorStructureByIdentifierKey
    ): ColorPalette {
        /**
         * Update or create texts of colors from structure.
         *
         * @var Color $colorEntity
         */
        $changedColorEntities = array_intersect_key($colorEntityByIdentifierKey, $colorStructureByIdentifierKey);
        foreach ($changedColorEntities as $colorEntity) {
            // Add the colorEntity to the colorPalette Entity
            $entity->addColor($this->updateRelationRelations($colorEntity, $colorStructureByIdentifierKey[$colorEntity->getIdentifier()]));
        }

        /**
         * Delete texts of colors missing in structure.
         *
         * @var Color $colorEntity
         * @var ColorText $texts
         */
        $deletedColorEntities = array_diff_key($colorEntityByIdentifierKey, $colorStructureByIdentifierKey);
        foreach ($deletedColorEntities as $colorEntity) {
            foreach ($colorEntity->getColorText() as $text) {
                $this->colorRepository->delete($text);
            }
        }

        return $entity;
    }

    /**
     * @param Color $entity
     * @param ColorStructure $structure
     *
     * @return Color
     */
    protected function updateRelationRelations(
        Color $entity,
        ColorStructure $structure
    ): Color {
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            self::TEXT_STRUCTURE_NAME,
            self::TEXT_ENTITY_NAME
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            self::TEXT_STRUCTURE_NAME,
            self::TEXT_ENTITY_NAME
        );

        return $entity;
    }

    /**
     * @param string $defaultColorIdentifier
     * @param array $colorEntityByIdentifierKey
     * @param array $colorStructureByIdentifierKey
     *
     * @return bool
     */
    private function isDefaultColorDeleted(
        string $defaultColorIdentifier,
        array $colorEntityByIdentifierKey,
        array $colorStructureByIdentifierKey
    ): bool {
        $deletedColorEntities = array_keys(array_diff_key($colorEntityByIdentifierKey, $colorStructureByIdentifierKey));

        return in_array($defaultColorIdentifier, $deletedColorEntities, false);
    }

    /**
     * @param ColorPalette $entity
     * @param ColorStructure $defaultColorStructure
     *
     * @return Color|null
     */
    private function findDefaultColor(ColorPalette $entity, ColorStructure $defaultColorStructure): ?Color
    {
        $defaultColor = false;

        if (isset($defaultColorStructure->id)) {
            $criteria = Criteria::create()->where(Criteria::expr()->eq('id', $defaultColorStructure->id));
            $defaultColor = $entity->getColor()->matching($criteria)->first();
        }

        // if the color could not be found by id, try by identifier
        if (false === $defaultColor) {
            $criteria = Criteria::create()->where(Criteria::expr()->eq('identifier', $defaultColorStructure->identifier));
            $defaultColor = $entity->getColor()->matching($criteria)->first();
        }

        return $defaultColor ?: null;
    }
}
