<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;

/**
 * @internal
 */
class AssemblyPointOptionStructureFromEntityConverter
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * AssemblyPointOptionStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Option $entity
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Option
     */
    public function convertOne($entity)
    {
        // create minimal structure for assembly point list (better performance)
        $structure = new \Redhotmagma\ConfiguratorApiBundle\Structure\Option();
        $structure->id = $entity->getId();
        $structure->identifier = $entity->getIdentifier();
        $structure->translated_title = $entity->getTranslatedTitle();

        $optionClassificationTitle = $entity
            ->getItemOptionclassificationOption()
            ->first()
            ->getItemOptionclassification()
            ->getOptionclassification()
            ->getTranslatedTitle();

        $structure->itemclassifications = [$optionClassificationTitle];

        return $structure;
    }

    /**
     * @param array $entities
     *
     * @return array
     */
    public function convertMany($entities)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity);
            $structures[] = $structure;
        }

        return $structures;
    }
}
