<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Events\AssemblyPoint\SaveByOptionEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SampleOptionListRequestArguments;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class AssemblyPointApi
{
    /**
     * @var AssemblyPointOptionStructureFromEntityConverter
     */
    private $assemblyPointOptionStructureFromEntityConverter;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var AssemblyPointOption
     */
    private $assemblyPointOption;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionStructureFromEntityConverter
     */
    private $optionStructureFromEntityConverter;

    /**
     * @var SampleOptions
     */
    private $sampleOptions;

    /**
     * @var VisualizationData
     */
    private $visualizationData;

    /**
     * AssemblyPointApi constructor.
     *
     * @param AssemblyPointOptionStructureFromEntityConverter $assemblyPointOptionStructureFromEntityConverter
     * @param EventDispatcherInterface $eventDispatcher
     * @param AssemblyPointOption $assemblyPointOption
     * @param OptionRepository $optionRepository
     * @param OptionStructureFromEntityConverter $optionStructureFromEntityConverter
     * @param SampleOptions $sampleOptions
     * @param VisualizationData $visualizationData
     */
    public function __construct(
        AssemblyPointOptionStructureFromEntityConverter $assemblyPointOptionStructureFromEntityConverter,
        EventDispatcherInterface $eventDispatcher,
        AssemblyPointOption $assemblyPointOption,
        OptionRepository $optionRepository,
        OptionStructureFromEntityConverter $optionStructureFromEntityConverter,
        SampleOptions $sampleOptions,
        VisualizationData $visualizationData
    ) {
        $this->assemblyPointOptionStructureFromEntityConverter = $assemblyPointOptionStructureFromEntityConverter;
        $this->eventDispatcher = $eventDispatcher;
        $this->assemblyPointOption = $assemblyPointOption;
        $this->optionRepository = $optionRepository;
        $this->optionStructureFromEntityConverter = $optionStructureFromEntityConverter;
        $this->sampleOptions = $sampleOptions;
        $this->visualizationData = $visualizationData;
    }

    /**
     * @param int $optionId
     *
     * @return Option
     */
    public function getByOption($optionId)
    {
        $optionStructure = $this->assemblyPointOption->getByOption($optionId);

        return $optionStructure;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getOptionsList(ListRequestArguments $arguments)
    {
        $paginationResult = new PaginationResult();

        $optionEntities = $this->optionRepository->fetchListForAssemblyPointOptions(
            $arguments,
            $this->getSearchableFields()
        );

        $structures = $this->assemblyPointOptionStructureFromEntityConverter->convertMany($optionEntities);

        $paginationResult->data = $structures;

        $paginationResult->count = $this->optionRepository->fetchListCountForAssemblyPointOptions(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param Option $option
     *
     * @return Option
     */
    public function save(Option $option)
    {
        $event = new SaveByOptionEvent($option);
        $this->eventDispatcher->dispatch($event, SaveByOptionEvent::NAME);
        $option = $event->getOption();

        return $option;
    }

    /**
     * @param SampleOptionListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getSampleOptions(SampleOptionListRequestArguments $arguments)
    {
        $paginationResult = $this->sampleOptions->getSampleOptions($arguments, $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier', 'optionText.title', 'optionText.abstract', 'optionText.description'];
    }
}
