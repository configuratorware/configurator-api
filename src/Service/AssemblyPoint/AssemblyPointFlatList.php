<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;

/**
 * @internal
 */
class AssemblyPointFlatList
{
    /**
     * @var VisualizationData
     */
    private $visualizationData;

    /**
     * @var AssemblyPointImage
     */
    private $assemblyPointImage;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * AssemblyPointFlatList constructor.
     *
     * @param VisualizationData $visualizationData
     * @param AssemblyPointImage $assemblyPointImage
     * @param OptionRepository $optionRepository
     */
    public function __construct(
        VisualizationData $visualizationData,
        AssemblyPointImage $assemblyPointImage,
        OptionRepository $optionRepository
    ) {
        $this->visualizationData = $visualizationData;
        $this->assemblyPointImage = $assemblyPointImage;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param $assemblyPoint
     * @param $parentImageElement
     * @param $visualizationDataList
     * @param $selectedOptions
     * @param $configurationOptionClassificationIdentifiers
     *
     * @return mixed
     */
    public function buildAssemblyPointFlatlist(
        $assemblyPoint,
        $parentImageElement,
        $visualizationDataList,
        $selectedOptions,
        $configurationOptionClassificationIdentifiers
    ) {
        if (!empty($selectedOptions[$assemblyPoint->optionclassification])) {
            /** @var Option $option */
            $option = $this->optionRepository->findOneBy(['identifier' => $selectedOptions[$assemblyPoint->optionclassification]->identifier]);

            $imageElement = new \stdClass();
            $imageElement->path = $assemblyPoint->imageDirectory . '/' . $option->getIdentifier() . $assemblyPoint->imageSuffix;

            $dimensions = $this->assemblyPointImage->getImageDimensions($imageElement->path);

            if (!empty($dimensions) && !empty($parentImageElement->dimensions)) {
                $imageElement->parentSequenceNumber = $assemblyPoint->parentSequenceNumber;
                $imageElement->assemblypointimageelement_id = $assemblyPoint->assemblypointimageelement_id;
                $imageElement->assemblypointimageelement = $assemblyPoint->identifier;
                $imageElement->dimensions = $dimensions;
                $imageElement->rotation = $parentImageElement->rotation + $assemblyPoint->rotation;

                $rotationRad = deg2rad($parentImageElement->rotation);

                // get the center of the parent because this is our reference
                // coordinates always reference the center of the element we're adding to, also the added element is
                // added at its center
                $parentCenterToOriginX = $parentImageElement->dimensions->width / 2 + $parentImageElement->x;
                $parentCenterToOriginY = $parentImageElement->dimensions->height / 2 + $parentImageElement->y;

                // now get the distance from left corner to the center of the child.
                // We need this because the real image is placed at the top left corner in relation to the top left corner
                // of the canvas
                $childCenterToChildTopLeftX = $imageElement->dimensions->width / 2;
                $childCenterToChildTopLeftY = $imageElement->dimensions->height / 2;

                // if there's a rotation we have to alter the vector by that
                $assemblyPointWithRotationX = $assemblyPoint->x * cos($rotationRad) - $assemblyPoint->y * sin($rotationRad);
                $assemblyPointWithRotationY = $assemblyPoint->x * sin($rotationRad) + $assemblyPoint->y * cos($rotationRad);

                // now add the assemblyPoint coordinates to find the point to which we're adding
                $imageElement->x = $parentCenterToOriginX + $assemblyPointWithRotationX;
                $imageElement->y = $parentCenterToOriginY + $assemblyPointWithRotationY;

                // now subtract the distance to the image's center, because we're assembling at top left corner
                $imageElement->x -= $childCenterToChildTopLeftX;
                $imageElement->y -= $childCenterToChildTopLeftY;

                $visualizationDataList[$assemblyPoint->imageSequenceNumber] = $imageElement;
            }

            // we restrict the structure to the exact element we look at, so this is always an array with max 1 element
            $assemblyPointStructure = $this->visualizationData->getByOptionId($option->getId(), $assemblyPoint->identifier);

            if (!empty($assemblyPointStructure) && !empty($assemblyPointStructure[0]->assemblyPoints)) {
                foreach ($assemblyPointStructure[0]->assemblyPoints as $assemblyPointIndex => $assemblyPointChild) {
                    // if there is a part that has two or more parents: find out which one has priority (by sequencenumber)
                    foreach ($visualizationDataList as $visualizationDataEntryIndex => $visualizationDataEntry) {
                        // there is a colliding element (an element can have different possible parents e.g. a bike's wheel
                        // can be added to the fork or if the fork is part of the frame to the frame)
                        if (null !== $visualizationDataEntry->assemblypointimageelement_id
                            && null !== $assemblyPointChild->assemblypointimageelement_id
                            && $visualizationDataEntry->assemblypointimageelement_id === $assemblyPointChild->assemblypointimageelement_id
                        ) {
                            // if the next iteration's sequencenumber is lower (= higher priority), work with this
                            // one instead = remove the already processed, if not don't do recursion with next one
                            if ($assemblyPointChild->parentSequenceNumber < $visualizationDataEntry->parentSequenceNumber) {
                                unset($visualizationDataList[$visualizationDataEntryIndex]);
                            } else {
                                continue 2;
                            }
                        }
                    }

                    $visualizationDataList = $this->buildAssemblyPointFlatlist($assemblyPointChild, $imageElement,
                        $visualizationDataList, $selectedOptions, $configurationOptionClassificationIdentifiers);
                }
            }
        }

        return $visualizationDataList;
    }
}
