<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SampleOptionListRequestArguments;

/**
 * @internal
 */
class SampleOptions
{
    /**
     * @var AssemblyPointOption
     */
    private $assemblyPointOption;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionStructureFromEntityConverter
     */
    private $optionStructureFromEntityConverter;

    /**
     * SampleOptions constructor.
     *
     * @param AssemblyPointOption $assemblyPointOption
     * @param ItemRepository $itemRepository
     * @param OptionRepository $optionRepository
     * @param OptionStructureFromEntityConverter $optionStructureFromEntityConverter
     */
    public function __construct(
        AssemblyPointOption $assemblyPointOption,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository,
        OptionStructureFromEntityConverter $optionStructureFromEntityConverter
    ) {
        $this->assemblyPointOption = $assemblyPointOption;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
        $this->optionStructureFromEntityConverter = $optionStructureFromEntityConverter;
    }

    /**
     * @param SampleOptionListRequestArguments $arguments
     * @param array $searchableFields
     *
     * @return PaginationResult
     */
    public function getSampleOptions(SampleOptionListRequestArguments $arguments, $searchableFields)
    {
        // get base items
        $baseItemIds = [];
        $optionId = $arguments->optionId;
        if (!empty($optionId)) {
            $baseItems = $this->itemRepository->fetchBaseItemsByOptionId($optionId);
            if (!empty($baseItems)) {
                /** @var Item $baseItem */
                foreach ($baseItems as $baseItem) {
                    $baseItemIds[] = $baseItem->getId();
                }
            }
        }
        $arguments->baseItemIds = $baseItemIds;

        $data = $this->fetchListSampleOptions($arguments->imageElementIdentifier, $arguments, $searchableFields);
        $count = $this->optionRepository->fetchListCountForAssemblypointSampleOptions($arguments, $searchableFields);

        // if no results where found serch again with no restriction to the construction pattern
        if (0 === $count) {
            unset($arguments->baseItemIds);

            $data = $this->fetchListSampleOptions($arguments->imageElementIdentifier, $arguments, $searchableFields);
            $count = $this->optionRepository->fetchListCountForAssemblypointSampleOptions($arguments,
                $searchableFields);
        }

        $paginationResult = new PaginationResult();
        $paginationResult->data = $data;
        $paginationResult->count = $count;

        return $paginationResult;
    }

    /**
     * @param int $imageElement
     * @param array $parameters
     * @param array $searchableFields
     *
     * @return array
     */
    protected function fetchListSampleOptions($imageElement, $parameters = [], $searchableFields = [])
    {
        $data = $this->optionRepository->fetchListForAssemblypointSampleOptions($parameters, $searchableFields);
        $structures = $this->optionStructureFromEntityConverter->convertMany($data);

        foreach ($structures as $structure) {
            $assemblyPointOption = $this->assemblyPointOption->getByOption($structure->id);

            foreach ($assemblyPointOption->visualizationdata as $visualizationDataElement) {
                if ($visualizationDataElement->identifier === $imageElement) {
                    $structure->visualizationdata = $visualizationDataElement;

                    break;
                }
            }
        }

        return $structures;
    }
}
