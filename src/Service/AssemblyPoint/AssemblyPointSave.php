<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint;
use Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointimageelementRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;

/**
 * @internal
 */
class AssemblyPointSave
{
    /**
     * @var AssemblypointRepository
     */
    private $assemblyPointRepository;

    /**
     * @var AssemblypointimageelementRepository
     */
    private $assemblyPointImageElementRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * AssemblyPointSave constructor.
     *
     * @param AssemblypointRepository $assemblyPointRepository
     * @param AssemblypointimageelementRepository $assemblyPointImageElementRepository
     * @param OptionRepository $optionRepository
     */
    public function __construct(
        AssemblypointRepository $assemblyPointRepository,
        AssemblypointimageelementRepository $assemblyPointImageElementRepository,
        OptionRepository $optionRepository
    ) {
        $this->assemblyPointRepository = $assemblyPointRepository;
        $this->assemblyPointImageElementRepository = $assemblyPointImageElementRepository;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param Option $option
     *
     * @return Option
     */
    public function save(Option $option)
    {
        // now only look at the assembly points that are provided and save them
        if (!empty($option->visualizationdata)) {
            foreach ($option->visualizationdata as $visualizationDataElement) {
                if (!empty($visualizationDataElement->assemblyPoints)) {
                    foreach ($visualizationDataElement->assemblyPoints as $assemblyPoint) {
                        if (!empty($assemblyPoint->id)) {
                            $assemblyPointEntity = $this->assemblyPointRepository->findOneBy(['id' => $assemblyPoint->id]);
                        } else {
                            $assemblyPointEntity = new Assemblypoint();
                        }

                        $optionEntity = $this->optionRepository->findOneBy(['id' => $assemblyPoint->option_id]);
                        $assemblyPointImageElement = $this->assemblyPointImageElementRepository->findOneBy(['id' => $assemblyPoint->assemblypointimageelement_id]);

                        $assemblyPointEntity->setAssemblypointimageelement($assemblyPointImageElement);
                        $assemblyPointEntity->setOption($optionEntity);
                        $assemblyPointEntity->setX($assemblyPoint->x);
                        $assemblyPointEntity->setY($assemblyPoint->y);
                        $assemblyPointEntity->setRotation($assemblyPoint->rotation);

                        $this->assemblyPointRepository->save($assemblyPointEntity);
                    }
                }
            }
        }

        return $option;
    }
}
