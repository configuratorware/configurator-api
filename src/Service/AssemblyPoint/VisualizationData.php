<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypoint;
use Redhotmagma\ConfiguratorApiBundle\Entity\AssemblypointimageelementLayer;
use Redhotmagma\ConfiguratorApiBundle\Entity\AssemblypointimageelementRelation;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointimageelementRelationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointimageelementRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionRepository;

/**
 * @internal
 */
class VisualizationData
{
    /**
     * @var AssemblypointRepository
     */
    private $assemblyPointRepository;

    /**
     * @var AssemblypointimageelementRepository
     */
    private $assemblyPointImageElementRepository;

    /**
     * @var AssemblypointimageelementRelationRepository
     */
    private $assemblyPointImageElementRelationRepository;

    /**
     * @var ItemOptionclassificationOptionRepository
     */
    private $itemOptionClassificationOptionRepository;

    /**
     * VisualizationData constructor.
     *
     * @param AssemblypointRepository $assemblyPointRepository
     * @param AssemblypointimageelementRepository $assemblyPointImageElementRepository
     * @param AssemblypointimageelementRelationRepository $assemblyPointImageElementRelationRepository
     * @param ItemOptionclassificationOptionRepository $itemOptionClassificationOptionRepository
     */
    public function __construct(
        AssemblypointRepository $assemblyPointRepository,
        AssemblypointimageelementRepository $assemblyPointImageElementRepository,
        AssemblypointimageelementRelationRepository $assemblyPointImageElementRelationRepository,
        ItemOptionclassificationOptionRepository $itemOptionClassificationOptionRepository
    ) {
        $this->assemblyPointRepository = $assemblyPointRepository;
        $this->assemblyPointImageElementRepository = $assemblyPointImageElementRepository;
        $this->assemblyPointImageElementRelationRepository = $assemblyPointImageElementRelationRepository;
        $this->itemOptionClassificationOptionRepository = $itemOptionClassificationOptionRepository;
    }

    /**
     * @param int $optionId
     * @param bool $restrictToImageElementIdentifier
     * @param bool $createNewAssemblyPoints
     *
     * @return array
     */
    public function getByOptionId(
        $optionId,
        $restrictToImageElementIdentifier = false,
        $createNewAssemblyPoints = false
    ) {
        // first get the already saved data
        $assemblyPointData = $this->assemblyPointRepository->getByOptionId($optionId);

        $assemblyPointDataByElement = [];
        /** @var Assemblypoint $savedAssemblyPoint */
        foreach ($assemblyPointData as $savedAssemblyPoint) {
            $assemblyPointDataByElement[$savedAssemblyPoint->getAssemblypointimageelement()->getId()] = $savedAssemblyPoint;
        }

        // then get all corresponding image elements to the options classification
        $assemblyPointImageElements = $this->getAssemblyPointImageElementsByOptionId($optionId);

        if (false !== $restrictToImageElementIdentifier) {
            foreach ($assemblyPointImageElements as $i => $assemblyPointImageElementParent) {
                $identifier = $assemblyPointImageElementParent->getOptionclassification()->getIdentifier();

                if (!empty($assemblyPointImageElementParent->getElementidentifier())) {
                    $identifier .= '_' . $assemblyPointImageElementParent->getElementidentifier();
                }

                if ($identifier !== $restrictToImageElementIdentifier) {
                    unset($assemblyPointImageElements[$i]);
                }
            }
        }

        $result = [];
        foreach ($assemblyPointImageElements as $assemblyPointImageElementParent) {
            // get the ones where the current one is parent
            $assemblyPointImageElementRelationsByParent = $this->assemblyPointImageElementRelationRepository->getByParent(
                $assemblyPointImageElementParent->getId()
            );

            $assemblyPoints = [];

            /** @var AssemblypointimageelementRelation $assemblyPointImageElementRelation */
            foreach ($assemblyPointImageElementRelationsByParent as $assemblyPointImageElementRelation) {
                $assemblyPointImageElement = $assemblyPointImageElementRelation->getAssemblypointimageelement();

                // if no assembly point was saved skip creating the structure
                if (false === $createNewAssemblyPoints && !isset($assemblyPointDataByElement[$assemblyPointImageElement->getId()])) {
                    continue;
                }

                $assemblyPoint = new \Redhotmagma\ConfiguratorApiBundle\Structure\Assemblypoint();

                $assemblyPoint->option_id = $optionId;
                $assemblyPoint->assemblypointimageelement_id = $assemblyPointImageElement->getId();
                $assemblyPoint->optionclassification = $assemblyPointImageElement
                    ->getOptionclassification()->getIdentifier();
                $assemblyPoint->identifier = $assemblyPoint->optionclassification;

                $imageSuffix = '';
                if (!empty($assemblyPointImageElement->getElementidentifier())) {
                    $assemblyPoint->identifier .= '_' . $assemblyPointImageElement->getElementidentifier();
                    $imageSuffix .= '_' . $assemblyPointImageElement->getElementidentifier();
                }

                // add saved data if present
                if (isset($assemblyPointDataByElement[$assemblyPointImageElement->getId()])) {
                    $assemblyPoint->id = $assemblyPointDataByElement[$assemblyPointImageElement->getId()]->getId();
                    $assemblyPoint->x = $assemblyPointDataByElement[$assemblyPointImageElement->getId()]->getX();
                    $assemblyPoint->y = $assemblyPointDataByElement[$assemblyPointImageElement->getId()]->getY();
                    $assemblyPoint->rotation = $assemblyPointDataByElement[$assemblyPointImageElement->getId()]->getRotation();
                }

                // get sequencenumber from the image layer to know the global order in which the images should be displayed
                // 0 is the bottom layer
                $assemblyPoint->imageSequenceNumber = $assemblyPointImageElement->getAssemblypointimageelementLayer()->first()->getSequencenumber();
                $assemblyPoint->imageSuffix = $imageSuffix;
                $assemblyPoint->imageDirectory = $assemblyPoint->optionclassification;
                $assemblyPoint->parentSequenceNumber = $assemblyPointImageElementRelation->getSequencenumber();

                $assemblyPoints[] = $assemblyPoint;
            }

            // now we create an object that contains the parent (e.g. frame) and the children (assemblypoints and
            // image and layer information). If there are multiple layers multiple elements are added
            if (!empty($assemblyPointImageElementParent->getAssemblypointimageelementLayer())) {
                /** @var AssemblypointimageelementLayer $assemblyPointImageElementLayer */
                foreach ($assemblyPointImageElementParent->getAssemblypointimageelementLayer() as $assemblyPointImageElementLayer) {
                    $elementInfo = new \stdClass();
                    $elementInfo->imageSequenceNumber = $assemblyPointImageElementLayer->getSequencenumber();

                    $elementInfo->assemblyPoints = $assemblyPoints;

                    $elementInfo->optionclassification = $assemblyPointImageElementParent
                        ->getOptionclassification()->getIdentifier();
                    $elementInfo->identifier = $elementInfo->optionclassification;

                    $imageSuffix = '';
                    if (!empty($assemblyPointImageElementParent->getElementidentifier())) {
                        $elementInfo->identifier .= '_' . $assemblyPointImageElementParent->getElementidentifier();
                        $imageSuffix .= '_' . $assemblyPointImageElementParent->getElementidentifier();
                    }

                    if ($assemblyPointImageElementLayer->getLayeridentifier()) {
                        $elementInfo->identifier .= '_' . $assemblyPointImageElementLayer->getLayeridentifier();
                        $imageSuffix .= '_' . $assemblyPointImageElementLayer->getLayeridentifier();
                    }

                    $elementInfo->imageSuffix = $imageSuffix;
                    $elementInfo->imageDirectory = $elementInfo->optionclassification;

                    $result[] = $elementInfo;
                }
            }
        }

        return $result;
    }

    /**
     * @param int $optionId
     *
     * @return array
     */
    public function getAssemblyPointImageElementsByOptionId($optionId)
    {
        /** @var ItemOptionclassificationOption $itemOptionClassificationOption */
        $itemOptionClassificationOption = $this->itemOptionClassificationOptionRepository->findOneBy(['option' => $optionId]);

        $optionClassificationId = $itemOptionClassificationOption->getItemOptionclassification()->getOptionclassification()->getId();

        $assemblyPointImageElements = $this->assemblyPointImageElementRepository->getByOptionclassification(
            $optionClassificationId,
            'id'
        );

        return $assemblyPointImageElements;
    }
}
