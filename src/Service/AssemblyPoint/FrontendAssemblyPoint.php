<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Entity\Assemblypointimageelement;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Repository\AssemblypointimageelementRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
class FrontendAssemblyPoint
{
    /**
     * @var AssemblypointimageelementRepository
     */
    private $assemblyPointImageElementRepository;

    /**
     * @var AssemblyPointFlatList
     */
    private $assemblyPointFlatList;

    /**
     * @var VisualizationData
     */
    private $visualizationData;

    /**
     * @var AssemblyPointImage
     */
    private $assemblyPointImage;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * FrontendAssemblyPoint constructor.
     *
     * @param AssemblypointimageelementRepository $assemblyPointImageElementRepository
     * @param AssemblyPointFlatList $assemblyPointFlatList
     * @param VisualizationData $visualizationData
     * @param AssemblyPointImage $assemblyPointImage
     * @param OptionRepository $optionRepository
     */
    public function __construct(
        AssemblypointimageelementRepository $assemblyPointImageElementRepository,
        AssemblyPointFlatList $assemblyPointFlatList,
        VisualizationData $visualizationData,
        AssemblyPointImage $assemblyPointImage,
        OptionRepository $optionRepository
    ) {
        $this->assemblyPointImageElementRepository = $assemblyPointImageElementRepository;
        $this->assemblyPointFlatList = $assemblyPointFlatList;
        $this->visualizationData = $visualizationData;
        $this->assemblyPointImage = $assemblyPointImage;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param $configuration
     */
    public function addVisualizationData(Configuration $configuration): void
    {
        $visualizationData = $configuration->visualizationData;

        // first get the root element. This is the one that has no parent Assemblypointimageelements
        // everything will be assembled to this element
        $optionClassificationIdentifiers = [];

        $selectedOptions = [];

        foreach ($configuration->optionclassifications as $optionClassification) {
            $optionClassificationIdentifiers[] = $optionClassification->identifier;

            if (!empty($optionClassification->selectedoptions)) {
                $selectedOptions[$optionClassification->identifier] = $optionClassification->selectedoptions[0];
            }
        }

        /** @var Assemblypointimageelement $rootElement */
        $rootElement = $this->assemblyPointImageElementRepository->getRootElementForOptionclassificationIdentifiers($optionClassificationIdentifiers);

        $visualizationDataList = [];

        if (!empty($rootElement)) {
            $rootItemOptionClassificationIdentifier = $rootElement->getOptionclassification()->getIdentifier();

            if (!empty($selectedOptions[$rootItemOptionClassificationIdentifier])) {
                /** @var Option $rootOption */
                $rootOption = $this->optionRepository->findOneBy(['identifier' => $selectedOptions[$rootItemOptionClassificationIdentifier]->identifier]);
                if (!empty($rootOption)) {
                    $assemblyPointStructure = $this->visualizationData->getByOptionId($rootOption->getId());

                    // in the result possibly there could be more than one element for an option (e.g. if the wheel
                    // xy is used as wheel_front and wheel_back. Normally, especially for the root there is exactly
                    // one element.
                    foreach ($assemblyPointStructure as $assemblyPointStructureElement) {
                        $imageElement = new \stdClass();
                        $imageElement->path = $assemblyPointStructureElement->imageDirectory . '/' .
                            $this->optionRepository->getImageIdentifierForOption($rootOption) .
                            $assemblyPointStructureElement->imageSuffix;

                        $dimensions = $this->assemblyPointImage->getImageDimensions($imageElement->path);
                        $imageElement->dimensions = $dimensions;
                        $imageElement->parentSequenceNumber = 0;
                        $imageElement->assemblypointimageelement_id = null;
                        $imageElement->assemblypointimageelement = $assemblyPointStructureElement->identifier;
                        $imageElement->x = 0;
                        $imageElement->y = 0;
                        $imageElement->rotation = 0;

                        $visualizationData->rootImageMissing = null === $dimensions ? true : false;

                        $visualizationDataList[$assemblyPointStructureElement->imageSequenceNumber] = $imageElement;

                        // begin recursion
                        foreach ($assemblyPointStructureElement->assemblyPoints as $assemblyPoint) {
                            $visualizationDataList = $this->assemblyPointFlatList->buildAssemblyPointFlatlist($assemblyPoint, $imageElement,
                                $visualizationDataList, $selectedOptions, $optionClassificationIdentifiers);
                        }
                    }
                }
            }
        }

        $visualizationData->elementList = $visualizationDataList;
        if (!empty($imageElement)) {
            $visualizationData->fullDimensions = $imageElement->dimensions;
        }
    }
}
