<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionStructureFromEntityConverter;

/**
 * @internal
 */
class AssemblyPointOption
{
    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionStructureFromEntityConverter
     */
    private $optionStructureFromEntityConverter;

    /**
     * @var VisualizationData
     */
    private $visualizationData;

    /**
     * AssemblyPointOption constructor.
     *
     * @param OptionRepository $optionRepository
     * @param OptionStructureFromEntityConverter $optionStructureFromEntityConverter
     * @param VisualizationData $visualizationData
     */
    public function __construct(
        OptionRepository $optionRepository,
        OptionStructureFromEntityConverter $optionStructureFromEntityConverter,
        VisualizationData $visualizationData
    ) {
        $this->optionRepository = $optionRepository;
        $this->optionStructureFromEntityConverter = $optionStructureFromEntityConverter;
        $this->visualizationData = $visualizationData;
    }

    /**
     * @param int $optionId
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Option
     */
    public function getByOption($optionId)
    {
        /** @var Option $option */
        $option = $this->optionRepository->findOneBy(['id' => $optionId]);

        if (empty($option)) {
            throw new NotFoundException();
        }

        $optionStructure = $this->optionStructureFromEntityConverter->convertOne($option);

        $optionStructure->visualizationdata = $this->visualizationData->getByOptionId($optionId, false, true);

        foreach ($optionStructure->visualizationdata as $element) {
            // add image name based on option and suffixes
            $element->imageName = $this->optionRepository->getImageIdentifierForOption($option) . $element->imageSuffix;
        }

        return $optionStructure;
    }
}
