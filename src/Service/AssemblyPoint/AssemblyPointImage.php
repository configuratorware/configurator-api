<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AssemblyPoint;

use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;

/**
 * @internal
 */
class AssemblyPointImage
{
    /**
     * @var MediaHelper
     */
    private $mediaHelper;

    /**
     * AssemblyPointImage constructor.
     *
     * @param MediaHelper $mediaHelper
     */
    public function __construct(MediaHelper $mediaHelper)
    {
        $this->mediaHelper = $mediaHelper;
    }

    /**
     * @param string $path
     *
     * @return \stdClass|null
     */
    public function getImageDimensions($path)
    {
        $imageDir = '/configuratorimages/defaultview/';

        $imagePath = realpath($this->mediaHelper->getImageBasePath() . $imageDir . $path . '.png');

        if (false !== $imagePath) {
            $imageInfo = getimagesize($imagePath);

            if (false !== $imageInfo) {
                $imageDimensions = new \stdClass();
                $imageDimensions->width = $imageInfo[0];
                $imageDimensions->height = $imageInfo[1];
            } else {
                $imageDimensions = null;
            }
        } else {
            $imageDimensions = null;
        }

        return $imageDimensions;
    }
}
