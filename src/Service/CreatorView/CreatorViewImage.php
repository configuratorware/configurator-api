<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\CreatorViewThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadCreatorViewThumbnail;

/**
 * @internal
 */
final class CreatorViewImage
{
    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var CreatorViewThumbnailRepositoryInterface
     */
    private $creatorViewThumbnailRepository;

    /**
     * @var ConfigurationApi
     */
    private $configurationApi;

    /**
     * @param CreatorViewRepository $creatorViewRepository
     * @param ItemRepository $itemRepository
     * @param CreatorViewThumbnailRepositoryInterface $creatorViewThumbnailRepository
     * @param ConfigurationApi $configurationApi
     */
    public function __construct(CreatorViewRepository $creatorViewRepository, ItemRepository $itemRepository, CreatorViewThumbnailRepositoryInterface $creatorViewThumbnailRepository, ConfigurationApi $configurationApi)
    {
        $this->creatorViewRepository = $creatorViewRepository;
        $this->itemRepository = $itemRepository;
        $this->creatorViewThumbnailRepository = $creatorViewThumbnailRepository;
        $this->configurationApi = $configurationApi;
    }

    /**
     * @param UploadCreatorViewThumbnail $uploadViewThumbnail
     */
    public function uploadThumbnail(UploadCreatorViewThumbnail $uploadViewThumbnail): void
    {
        $creatorView = $this->creatorViewRepository->find($uploadViewThumbnail->viewId);

        $item = $this->itemRepository->findOneByIdentifier($creatorView->getItemIdentifier());

        $viewThumbnail = CreatorViewThumbnail::from(null, $creatorView->getIdentifier(), $creatorView->getItemIdentifier(), $uploadViewThumbnail->realPath, $uploadViewThumbnail->fileName);

        $this->creatorViewThumbnailRepository->saveThumbnail($viewThumbnail, $item, $creatorView);
    }

    /**
     * @param int $id
     */
    public function deleteThumbnail(int $id): void
    {
        $creatorView = $this->creatorViewRepository->find($id);

        $item = $this->itemRepository->findOneByIdentifier($creatorView->getItemIdentifier());

        $viewThumbnails = $this->creatorViewThumbnailRepository->findThumbnails($item);

        $viewThumbnailsToDelete = array_filter($viewThumbnails, static function (CreatorViewThumbnail $viewThumbnail) use ($creatorView) {
            return $viewThumbnail->isForCreatorView($creatorView->getIdentifier());
        });

        $this->creatorViewThumbnailRepository->deleteThumbnail(current($viewThumbnailsToDelete));
    }

    /**
     * @param Item $item
     *
     * @return \stdClass|null
     */
    public function createLayerImageIdentifierMap(Item $item): ?\stdClass
    {
        $frontendConfiguration = $this->configurationApi->loadByItemIdentifier($item->getIdentifier());

        if (!isset($frontendConfiguration->visualizationData->viewImages)) {
            return null;
        }

        $viewImages = $frontendConfiguration->visualizationData->viewImages;

        if ([] === get_object_vars($viewImages)) {
            return null;
        }

        return $viewImages;
    }
}
