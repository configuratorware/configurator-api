<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\FrontendDesignAreaStructureFromCreatorViewDesignAreaEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignView as DesignViewStructure;

/**
 * @internal
 */
class FrontendCreatorViewStructureFromEntityConverter
{
    /**
     * @var FrontendDesignAreaStructureFromCreatorViewDesignAreaEntityConverter
     */
    private $frontendDesignAreaStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * FrontendDesignViewStructureFromEntityConverter constructor.
     *
     * @param FrontendDesignAreaStructureFromCreatorViewDesignAreaEntityConverter $frontendDesignAreaStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        FrontendDesignAreaStructureFromCreatorViewDesignAreaEntityConverter $frontendDesignAreaStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->frontendDesignAreaStructureFromEntityConverter = $frontendDesignAreaStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param CreatorView $entity
     *
     * @return DesignViewStructure
     */
    public function convertOne(CreatorView $entity): DesignViewStructure
    {
        /** @var DesignViewStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, DesignViewStructure::class);

        $structure->title = $entity->getTranslatedTitle();

        $structure->designAreas = $this->frontendDesignAreaStructureFromEntityConverter->convertMany($entity->getCreatorViewDesignArea()->toArray());

        return $structure;
    }

    /**
     * @param array $entities
     *
     * @return array
     */
    public function convertMany(array $entities): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }
}
