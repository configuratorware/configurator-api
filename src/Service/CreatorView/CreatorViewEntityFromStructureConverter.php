<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewText;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\CreatorView as CreatorViewStructure;

/**
 * @internal
 */
class CreatorViewEntityFromStructureConverter
{
    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @param DesignAreaRepository $designAreaRepository
     * @param LanguageRepository $languageRepository
     * @param ItemRepository $itemRepository
     */
    public function __construct(DesignAreaRepository $designAreaRepository, LanguageRepository $languageRepository, ItemRepository $itemRepository)
    {
        $this->designAreaRepository = $designAreaRepository;
        $this->languageRepository = $languageRepository;
        $this->itemRepository = $itemRepository;
    }

    public function convertOne(CreatorViewStructure $structure, ?CreatorView $entity = null): CreatorView
    {
        if (null === $entity) {
            $item = $this->itemRepository->find($structure->itemId);

            $entity = CreatorView::from(null, $structure->identifier, $item, $structure->sequenceNumber);
        }

        $entity->setIdentifier($structure->identifier);

        if (null !== $structure->sequenceNumber) {
            $entity->setSequenceNumber($structure->sequenceNumber);
        }

        $this->handleDesignAreas($structure, $entity);

        $this->handleTexts($structure, $entity);

        return $entity;
    }

    /**
     * @param CreatorViewStructure $structure
     * @param CreatorView $entity
     */
    private function handleDesignAreas(CreatorViewStructure $structure, CreatorView $entity): void
    {
        $areaRelationsToKeep = [];
        foreach ($structure->designAreas as $designAreaStructure) {
            $filtered = $entity->getCreatorViewDesignArea()->filter(static function (CreatorViewDesignArea $viewAreaEntity) use ($designAreaStructure) {
                return $viewAreaEntity->getDesignArea()->getId() === $designAreaStructure->id;
            });

            $viewAreaEntity = $filtered->first();
            if (false === $viewAreaEntity) {
                $area = $this->designAreaRepository->find($designAreaStructure->id);
                $viewAreaEntity = CreatorViewDesignArea::from($entity, $area);
            } else {
                $areaRelationsToKeep[] = $viewAreaEntity->getId();
            }

            $viewAreaEntity->setIsDefault($designAreaStructure->isDefault)
                ->setBaseShape($designAreaStructure->baseShape)
                ->setPosition($designAreaStructure->position)
                ->setCustomData($designAreaStructure->customData);

            $entity->addCreatorViewDesignArea($viewAreaEntity);
        }

        foreach ($entity->getCreatorViewDesignArea() as $viewAreaEntity) {
            if (!in_array($viewAreaEntity->getId(), $areaRelationsToKeep, false)) {
                $entity->removeCreatorViewDesignArea($viewAreaEntity);
            }
        }
    }

    /**
     * @param CreatorViewStructure $structure
     * @param CreatorView $entity
     */
    private function handleTexts(CreatorViewStructure $structure, CreatorView $entity): void
    {
        $textsToKeep = [];
        foreach ($structure->texts as $textStructure) {
            $filtered = $entity->getCreatorViewText()->filter(static function (CreatorViewText $textEntity) use ($textStructure) {
                return isset($textStructure->id) && $textEntity->getId() === $textStructure->id;
            });

            $textEntity = $filtered->first();
            if (false === $textEntity) {
                $language = $this->languageRepository->findOneByIso($textStructure->language);
                $textEntity = CreatorViewText::from($textStructure->title, $entity, $language);
            } else {
                $textsToKeep[] = $textEntity->getId();
            }

            $textEntity->setTitle($textStructure->title);

            $entity->addCreatorViewText($textEntity);
        }

        foreach ($entity->getCreatorViewText() as $textEntity) {
            if (!in_array($textEntity->getId(), $textsToKeep, false)) {
                $entity->removeCreatorViewText($textEntity);
            }
        }
    }
}
