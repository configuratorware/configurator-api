<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DefaultView;

/**
 * @internal
 */
class CreatorViewDefaultFactory
{
    private const SEQUENCE_NUMBER = '01';

    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @param DesignAreaRepository $designAreaRepository
     */
    public function __construct(DesignAreaRepository $designAreaRepository)
    {
        $this->designAreaRepository = $designAreaRepository;
    }

    /**
     * @param Item $item
     *
     * @return CreatorView
     *
     * @throws Exception
     */
    public function create(Item $item): CreatorView
    {
        $designAreas = $this->designAreaRepository->findByItemAndHasDesignProductionMethodRelation($item);

        $defaultCreatorView = CreatorView::from(null, DefaultView::IDENTIFIER, $item, self::SEQUENCE_NUMBER);

        foreach ($designAreas as $area) {
            $defaultCreatorView->addCreatorViewDesignArea($this->createViewArea($defaultCreatorView, $area));
        }

        return $defaultCreatorView;
    }

    /**
     * @param CreatorView $view
     * @param DesignArea $area
     *
     * @return CreatorViewDesignArea
     */
    private function createViewArea(CreatorView $view, DesignArea $area): CreatorViewDesignArea
    {
        return CreatorViewDesignArea::fromDefault($view, $area, DefaultView::BASE_SHAPE, DefaultView::POSITION);
    }
}
