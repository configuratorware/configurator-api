<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadCreatorViewThumbnail;

final class CreatorViewApi
{
    private const SEARCHABLE_FIELDS = ['creatorViewText.title'];

    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var CreatorViewSave
     */
    private $creatorViewSave;

    /**
     * @var CreatorViewImage
     */
    private $creatorViewImage;

    /**
     * @var CreatorViewStructureFromEntityConverter
     */
    private $creatorViewStructureFromEntityConverter;

    /**
     * @var CreatorViewDelete
     */
    private $creatorViewDelete;

    /**
     * @param CreatorViewDelete $creatorViewDelete
     * @param CreatorViewRepository $creatorViewRepository
     * @param CreatorViewImage $creatorViewImage
     * @param CreatorViewSave $creatorViewSave
     * @param CreatorViewStructureFromEntityConverter $creatorViewStructureFromEntityConverter
     */
    public function __construct(CreatorViewDelete $creatorViewDelete, CreatorViewRepository $creatorViewRepository, CreatorViewImage $creatorViewImage, CreatorViewSave $creatorViewSave, CreatorViewStructureFromEntityConverter $creatorViewStructureFromEntityConverter)
    {
        $this->creatorViewDelete = $creatorViewDelete;
        $this->creatorViewRepository = $creatorViewRepository;
        $this->creatorViewImage = $creatorViewImage;
        $this->creatorViewSave = $creatorViewSave;
        $this->creatorViewStructureFromEntityConverter = $creatorViewStructureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $entities = $this->creatorViewRepository->fetchList($arguments, self::SEARCHABLE_FIELDS);
        $structures = $this->creatorViewStructureFromEntityConverter->convertMany($entities);
        $count = (int) $this->creatorViewRepository->fetchListCount($arguments, self::SEARCHABLE_FIELDS);

        return new PaginationResult($structures, $count);
    }

    /**
     * @param int $id
     *
     * @return CreatorView
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): CreatorView
    {
        $entity = $this->creatorViewRepository->find($id);

        if (null === $entity) {
            throw new NotFoundException();
        }

        $structure = $this->creatorViewStructureFromEntityConverter->convertOne($entity);
        $structure->layerImages = $this->creatorViewImage->createLayerImageIdentifierMap($entity->getItem());

        return $structure;
    }

    /**
     * @param CreatorView $creatorView
     *
     * @return CreatorView
     */
    public function save(CreatorView $creatorView): CreatorView
    {
        $id = $this->creatorViewSave->save($creatorView);

        $this->creatorViewRepository->clear();
        $entity = $this->creatorViewRepository->find($id);

        return $this->creatorViewStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $this->creatorViewDelete->delete($id);
    }

    /**
     * @param UploadCreatorViewThumbnail $uploadViewThumbnail
     */
    public function uploadThumbnail(UploadCreatorViewThumbnail $uploadViewThumbnail): void
    {
        $this->creatorViewImage->uploadThumbnail($uploadViewThumbnail);
    }

    /**
     * @param int $id
     */
    public function deleteThumbnail(int $id): void
    {
        $this->creatorViewImage->deleteThumbnail($id);
    }
}
