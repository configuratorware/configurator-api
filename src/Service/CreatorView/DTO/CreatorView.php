<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\DTO;

/**
 * @internal
 */
final class CreatorView
{
    /**
     * @var string
     */
    private $itemIdentifier;

    /**
     * @var string
     */
    private $viewIdentifier;

    /**
     * @var string
     */
    private $viewSequenceNumber;

    /**
     * @param string $viewIdentifier
     * @param string $viewSequenceNumber
     * @param string $itemIdentifier
     */
    private function __construct(string $viewIdentifier, string $viewSequenceNumber, string $itemIdentifier)
    {
        $this->viewIdentifier = $viewIdentifier;
        $this->viewSequenceNumber = $viewSequenceNumber;
        $this->itemIdentifier = $itemIdentifier;
    }

    /**
     * @param string $viewIdentifier
     * @param string $viewSequenceNumber
     * @param string $itemIdentifier
     *
     * @return static
     */
    public static function from(string $viewIdentifier, string $viewSequenceNumber, string $itemIdentifier): self
    {
        return new self($viewIdentifier, $viewSequenceNumber, $itemIdentifier);
    }

    /**
     * @return string
     */
    public function getItemIdentifier(): string
    {
        return $this->itemIdentifier;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->viewIdentifier;
    }

    /**
     * @return string
     */
    public function getSequenceNumber(): string
    {
        return $this->viewSequenceNumber;
    }
}
