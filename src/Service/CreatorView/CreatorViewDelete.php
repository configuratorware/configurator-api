<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewFileRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;

/**
 * @internal
 */
class CreatorViewDelete
{
    /**
     * @var CreatorViewFileRepositoryInterface
     */
    private $creatorViewFileRepository;

    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @param CreatorViewFileRepositoryInterface $creatorViewFileRepository
     * @param CreatorViewRepository $creatorViewRepository
     */
    public function __construct(CreatorViewFileRepositoryInterface $creatorViewFileRepository, CreatorViewRepository $creatorViewRepository)
    {
        $this->creatorViewFileRepository = $creatorViewFileRepository;
        $this->creatorViewRepository = $creatorViewRepository;
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $entity = $this->creatorViewRepository->find($id);

        if (null === $entity) {
            throw new NotFoundException();
        }

        $this->deleteFromFileSystem($entity);
        $this->deleteEntity($entity);
    }

    /**
     * @param CreatorView $creatorView
     */
    public function deleteFromFileSystem(CreatorView $creatorView): void
    {
        $this->creatorViewFileRepository->delete($creatorView);
    }

    /**
     * @param CreatorView $creatorView
     */
    public function deleteEntity(CreatorView $creatorView): void
    {
        foreach ($creatorView->getCreatorViewText() as $text) {
            $creatorView->removeCreatorViewText($text);
        }

        foreach ($creatorView->getCreatorViewDesignArea() as $viewArea) {
            $creatorView->removeCreatorViewDesignArea($viewArea);
        }

        $this->creatorViewRepository->delete($creatorView);
    }
}
