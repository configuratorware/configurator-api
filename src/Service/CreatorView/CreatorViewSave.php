<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidCreatorView;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\CreatorViewFileRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\LayerImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\CreatorView as CreatorViewStructure;

/**
 * @internal
 */
final class CreatorViewSave
{
    /**
     * @var CreatorViewEntityFromStructureConverter
     */
    private $creatorViewEntityFromStructureConverter;

    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var CreatorViewFileRepositoryInterface
     */
    private $creatorViewFileRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var LayerImageRepositoryInterface
     */
    private $layerImageRepository;

    /**
     * @param CreatorViewEntityFromStructureConverter $creatorViewEntityFromStructureConverter
     * @param CreatorViewRepository $creatorViewRepository
     * @param CreatorViewFileRepositoryInterface $creatorViewFileRepository
     * @param ItemRepository $itemRepository
     * @param LayerImageRepositoryInterface $layerImageRepository
     */
    public function __construct(CreatorViewEntityFromStructureConverter $creatorViewEntityFromStructureConverter, CreatorViewRepository $creatorViewRepository, CreatorViewFileRepositoryInterface $creatorViewFileRepository, ItemRepository $itemRepository, LayerImageRepositoryInterface $layerImageRepository)
    {
        $this->creatorViewEntityFromStructureConverter = $creatorViewEntityFromStructureConverter;
        $this->creatorViewRepository = $creatorViewRepository;
        $this->creatorViewFileRepository = $creatorViewFileRepository;
        $this->itemRepository = $itemRepository;
        $this->layerImageRepository = $layerImageRepository;
    }

    /**
     * @param CreatorViewStructure $creatorViewStructure
     *
     * @return int
     */
    public function save(CreatorViewStructure $creatorViewStructure): int
    {
        if (null === $creatorViewStructure->id) {
            return $this->createNewView($creatorViewStructure);
        }

        return $this->updateExistingView($creatorViewStructure);
    }

    /**
     * @param CreatorViewStructure $creatorViewStructure
     *
     * @return int
     */
    private function createNewView(CreatorViewStructure $creatorViewStructure): int
    {
        $view = $this->creatorViewEntityFromStructureConverter->convertOne($creatorViewStructure);

        $item = $this->itemRepository->find($creatorViewStructure->itemId);
        $directory = $this->layerImageRepository->createStores($view, $item);
        $view->setDirectory($directory);

        $view = $this->creatorViewRepository->save($view);

        $id = $view->getId();
        if (null === $id) {
            throw InvalidCreatorView::missingId();
        }

        return $id;
    }

    /**
     * @param CreatorViewStructure $creatorViewStructure
     *
     * @return int
     */
    private function updateExistingView(CreatorViewStructure $creatorViewStructure): int
    {
        $existingView = $this->creatorViewRepository->find($creatorViewStructure->id);
        $view = $this->creatorViewEntityFromStructureConverter->convertOne($creatorViewStructure, $existingView);

        $directory = $this->creatorViewFileRepository->save($view);
        $view->setDirectory($directory);

        $view = $this->creatorViewRepository->save($view);

        $id = $view->getId();
        if (null === $id) {
            throw InvalidCreatorView::missingId();
        }

        return $id;
    }
}
