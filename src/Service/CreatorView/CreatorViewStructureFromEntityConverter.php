<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorView;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView  as CreatorViewEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Structure\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Structure\CreatorViewText;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaRelation;

/**
 * @internal
 */
class CreatorViewStructureFromEntityConverter
{
    /**
     * @param CreatorViewEntity $entity
     *
     * @return CreatorView
     */
    public function convertOne(CreatorViewEntity $entity): CreatorView
    {
        $designAreas = $this->convertDesignAreas($entity->getCreatorViewDesignArea());

        $texts = $this->convertCreatorViewTexts($entity->getCreatorViewText());

        return new CreatorView($entity->getId(), $entity->getIdentifier(), $entity->getSequenceNumber(), (int)$entity->getItem()->getId(), $designAreas, $texts);
    }

    /**
     * @param array $entities
     *
     * @return array
     */
    public function convertMany(array $entities): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    /**
     * @param Collection<int, CreatorViewDesignArea> $creatorViewDesignAreas
     *
     * @return DesignAreaRelation[]
     */
    private function convertDesignAreas(Collection $creatorViewDesignAreas): array
    {
        $convertedAreas = [];

        /** @var CreatorViewDesignArea $viewArea */
        foreach ($creatorViewDesignAreas as $viewArea) {
            $designArea = $viewArea->getDesignArea();
            $convertedAreas[] = new DesignAreaRelation($designArea->getId(), $designArea->getIdentifier(), $viewArea->getIsDefault(), $designArea->getTranslatedTitle(), $viewArea->getBaseShape(), $viewArea->getPosition(), $viewArea->getCustomData());
        }

        return $convertedAreas;
    }

    /**
     * @param Collection $creatorViewTexts
     *
     * @return CreatorViewText[]
     */
    private function convertCreatorViewTexts(Collection $creatorViewTexts): array
    {
        $convertedTexts = [];

        foreach ($creatorViewTexts as $creatorViewText) {
            $convertedTexts[] = new CreatorViewText($creatorViewText->getId(), $creatorViewText->getLanguage()->getIso(), $creatorViewText->getTitle());
        }

        return $convertedTexts;
    }
}
