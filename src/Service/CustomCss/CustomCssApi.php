<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CustomCss;

use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;

class CustomCssApi
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @param ClientRepository $clientRepository
     * @param SettingRepository $settingRepository
     */
    public function __construct(ClientRepository $clientRepository, SettingRepository $settingRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->settingRepository = $settingRepository;
    }

    /**
     * @param string|null $clientIdentifier
     *
     * @return string
     */
    public function loadByClient(?string $clientIdentifier): string
    {
        $settings = $this->settingRepository->findAll();

        if (!isset($settings[0])) {
            return '';
        }

        $settingCss = $settings[0]->getCustomCss();

        $clientIdentifier = $clientIdentifier ?? '_default';

        $client = $this->clientRepository->findOneBy(['identifier' => $clientIdentifier]);

        if (null === $client) {
            return $settingCss;
        }

        return $settingCss . PHP_EOL . $client->getCustomCss();
    }
}
