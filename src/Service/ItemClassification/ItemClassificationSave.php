<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification as ItemClassificationStructure;

/**
 * @internal
 */
class ItemClassificationSave
{
    /**
     * @var ItemclassificationRepository
     */
    private ItemclassificationRepository $itemClassificationRepository;

    /**
     * @var ItemClassificationEntityFromStructureConverter
     */
    private ItemClassificationEntityFromStructureConverter $itemClassificationEntityFromStructureConverter;

    /**
     * @var ItemClassificationStructureFromEntityConverter
     */
    private ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter;

    /**
     * ItemClassificationSave constructor.
     *
     * @param ItemclassificationRepository $itemClassificationRepository
     * @param ItemClassificationEntityFromStructureConverter $itemClassificationEntityFromStructureConverter
     * @param ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter
     */
    public function __construct(
        ItemclassificationRepository $itemClassificationRepository,
        ItemClassificationEntityFromStructureConverter $itemClassificationEntityFromStructureConverter,
        ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter
    ) {
        $this->itemClassificationRepository = $itemClassificationRepository;
        $this->itemClassificationEntityFromStructureConverter = $itemClassificationEntityFromStructureConverter;
        $this->itemClassificationStructureFromEntityConverter = $itemClassificationStructureFromEntityConverter;
    }

    /**
     * @param ItemClassificationStructure $itemClassification
     *
     * @return ItemClassificationStructure
     */
    public function save(ItemClassificationStructure $itemClassification): ItemClassificationStructure
    {
        $entity = null;
        if (isset($itemClassification->id) && $itemClassification->id > 0) {
            $entity = $this->itemClassificationRepository->findOneBy(['id' => $itemClassification->id]);
        }
        $entity = $this->itemClassificationEntityFromStructureConverter->convertOne($itemClassification, $entity);

        $currentSequenceNumber = $entity->getSequencenumber();
        if (null === $currentSequenceNumber || 0 === $currentSequenceNumber) {
            $entity->setSequencenumber($this->itemClassificationRepository->getNextSequenceNumber());
        }
        $entity = $this->itemClassificationRepository->save($entity);

        /** @var ItemClassification $entity */
        $entity = $this->itemClassificationRepository->findOneBy(['id' => $entity->getId()]);

        return $this->itemClassificationStructureFromEntityConverter->convertOne($entity);
    }
}
