<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification as ItemClassificationEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemClassificationAttribute;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification;

/**
 * @internal
 */
class ItemClassificationEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var AttributeRelationEntityFromStructureConverter
     */
    private AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter;

    /**
     * @var EntityFromStructureConverter
     */
    private EntityFromStructureConverter $entityFromStructureConverter;

    /**
     * ItemClassificationEntityFromStructureConverter constructor.
     *
     * @param AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->attributeRelationEntityFromStructureConverter = $attributeRelationEntityFromStructureConverter;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param ItemClassification $structure
     * @param ItemClassificationEntity $entity
     * @param string $entityClassName
     *
     * @return ItemClassificationEntity
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = ItemClassificationEntity::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var ItemClassificationEntity $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->updateItemClassificationTexts($localStructure, $entity);
        $entity = $this->updateItemClassificationAttributes($localStructure, $entity);

        return $entity;
    }

    /**
     * @param ItemClassification $structure
     * @param ItemClassificationEntity $entity
     *
     * @return ItemClassificationEntity
     */
    protected function updateItemClassificationTexts(ItemClassification $structure, ItemClassificationEntity $entity): ItemClassificationEntity
    {
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            'texts',
            'Itemclassificationtext');

        return $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            'texts',
            'Itemclassificationtext');
    }

    /**
     * @param ItemClassification $structure
     * @param ItemClassificationEntity $entity
     *
     * @return ItemClassificationEntity
     */
    protected function updateItemClassificationAttributes(ItemClassification $structure, ItemClassificationEntity $entity): ItemClassificationEntity
    {
        /** @var ItemClassificationEntity $entity */
        $entity = $this->attributeRelationEntityFromStructureConverter
            ->setManyToManyRelationsDeleted(
                $structure,
                $entity,
                'ItemclassificationAttribute',
                'attributes',
                'Attribute'
            );

        /** @var ItemClassificationEntity $entity */
        $entity = $this->attributeRelationEntityFromStructureConverter
            ->addNewManyToManyRelations(
                $structure,
                $entity,
                ItemClassificationAttribute::class,
                'attributes'
            );

        return $entity;
    }
}
