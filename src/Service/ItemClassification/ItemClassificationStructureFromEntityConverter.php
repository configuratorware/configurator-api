<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassification as ItemClassificationEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemclassificationtext as ItemclassificationtextEntity;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\Itemclassificationtext;

/**
 * @internal
 */
class ItemClassificationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private StructureFromEntityConverter $structureFromEntityConverter;

    /**
     * @var AttributeRelationStructureFromEntityConverter
     */
    private AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter;

    /**
     * ItemClassificationStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ItemClassification $entity
     * @param string $structureClassName
     *
     * @return ItemClassification
     */
    public function convertOne($entity, $structureClassName = ItemClassification::class)
    {
        /** @var ItemClassification $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addItemClassificationTextsToStructure($structure, $entity);
        $structure = $this->addItemClassificationAttributesToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ItemClassification::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param ItemClassification $structure
     * @param ItemClassificationEntity $entity
     *
     * @return ItemClassification
     */
    protected function addItemClassificationTextsToStructure(ItemClassification $structure, ItemClassificationEntity $entity): ItemClassification
    {
        $texts = [];

        /** @var ItemclassificationtextEntity $itemClassificationText */
        foreach ($entity->getItemclassificationtext() as $itemClassificationText) {
            $itemClassificationTextStructure = $this->structureFromEntityConverter->convertOne($itemClassificationText,
                Itemclassificationtext::class);

            if (!empty($itemClassificationTextStructure)) {
                $itemClassificationTextStructure->language = $itemClassificationText->getLanguage()
                    ->getIso();
                $texts[] = $itemClassificationTextStructure;
            }
        }

        $structure->texts = $texts;

        return $structure;
    }

    /**
     * @param ItemClassification $structure
     * @param ItemClassificationEntity $entity
     *
     * @return ItemClassification
     */
    protected function addItemClassificationAttributesToStructure(ItemClassification $structure, ItemClassificationEntity $entity): ItemClassification
    {
        $itemClassificationAttributes = $this->attributeRelationStructureFromEntityConverter->convertMany($entity->getItemClassificationAttribute()->toArray());

        $structure->attributes = $itemClassificationAttributes;

        return $structure;
    }
}
