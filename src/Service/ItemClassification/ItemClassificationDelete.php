<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository;

/**
 * @internal
 */
class ItemClassificationDelete
{
    /**
     * @var ItemclassificationRepository
     */
    private ItemclassificationRepository $itemClassificationRepository;

    /**
     * ItemClassificationDelete constructor.
     *
     * @param ItemclassificationRepository $itemClassificationRepository
     */
    public function __construct(ItemclassificationRepository $itemClassificationRepository)
    {
        $this->itemClassificationRepository = $itemClassificationRepository;
    }

    /**
     * @param string $deleteIds
     */
    public function delete(string $deleteIds)
    {
        $ids = explode(',', $deleteIds);
        foreach ($ids as $id) {
            /** @var ItemClassification $entity */
            $entity = $this->itemClassificationRepository->find($id);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            // delete texts
            if (!empty($entity->getItemClassificationtext()->toArray())) {
                foreach ($entity->getItemClassificationtext() as $itemClassificationText) {
                    $this->itemClassificationRepository->delete($itemClassificationText);
                }
            }

            // delete attribute relations
            if (!empty($entity->getItemclassificationAttribute()->toArray())) {
                foreach ($entity->getItemClassificationAttribute() as $itemClassificationAttribute) {
                    $this->itemClassificationRepository->delete($itemClassificationAttribute);
                }
            }

            $this->itemClassificationRepository->delete($entity);
        }
    }
}
