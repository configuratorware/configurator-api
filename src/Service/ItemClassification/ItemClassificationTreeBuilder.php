<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

/**
 * @internal
 */
class ItemClassificationTreeBuilder
{
    /**
     * @var ItemclassificationRepository
     */
    private $itemClassificationRepository;

    /**
     * @var ItemClassificationStructureFromEntityConverter
     */
    private $itemClassificationStructureFromEntityConverter;

    /**
     * ItemClassificationTreeBuilder constructor.
     *
     * @param ItemclassificationRepository $itemClassificationRepository
     * @param ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter
     */
    public function __construct(
        ItemclassificationRepository $itemClassificationRepository,
        ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter
    ) {
        $this->itemClassificationRepository = $itemClassificationRepository;
        $this->itemClassificationStructureFromEntityConverter = $itemClassificationStructureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $parameters
     * @param int $parentId
     * @param array $searchableFields
     *
     * @return array
     */
    public function buildTree(ListRequestArguments $parameters, int $parentId = null, array $searchableFields = []): array
    {
        $structures = [];
        $parameters->filters['_parent_id'] = $parentId;

        $data = $this->itemClassificationRepository->fetchList($parameters, $searchableFields);

        if (!empty($data)) {
            $structures = $this->itemClassificationStructureFromEntityConverter->convertMany($data);

            foreach ($structures as $structure) {
                $structure->children = $this->buildTree($parameters, $structure->id, $searchableFields);
            }
        }

        return $structures;
    }
}
