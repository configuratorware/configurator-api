<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemclassification as ItemClassificationFrontendStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification as ItemClassificationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

class ItemClassificationApi
{
    /**
     * @var ItemclassificationRepository
     */
    private ItemclassificationRepository $itemClassificationRepository;

    /**
     * @var ItemClassificationStructureFromEntityConverter
     */
    private ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter;

    /**
     * @var ItemClassificationSave
     */
    private ItemClassificationSave $itemClassificationSave;

    /**
     * @var ItemClassificationDelete
     */
    private ItemClassificationDelete $itemClassificationDelete;

    /**
     * @var ItemClassificationTreeBuilder
     */
    private ItemClassificationTreeBuilder $itemClassificationTreeBuilder;

    /**
     * ItemClassificationApi constructor.
     *
     * @param ItemclassificationRepository $itemClassificationRepository
     * @param ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter
     * @param ItemClassificationSave $itemClassificationSave
     * @param ItemClassificationDelete $itemClassificationDelete
     * @param ItemClassificationTreeBuilder $itemClassificationTreeBuilder
     */
    public function __construct(
        ItemclassificationRepository $itemClassificationRepository,
        ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter,
        ItemClassificationSave $itemClassificationSave,
        ItemClassificationDelete $itemClassificationDelete,
        ItemClassificationTreeBuilder $itemClassificationTreeBuilder
    ) {
        $this->itemClassificationRepository = $itemClassificationRepository;
        $this->itemClassificationStructureFromEntityConverter = $itemClassificationStructureFromEntityConverter;
        $this->itemClassificationSave = $itemClassificationSave;
        $this->itemClassificationDelete = $itemClassificationDelete;
        $this->itemClassificationTreeBuilder = $itemClassificationTreeBuilder;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $paginationResult->data = $this->itemClassificationTreeBuilder->buildTree($arguments, null,
            $this->getSearchableFields());

        $arguments->filters['_parent_id'] = null;
        $paginationResult->count = $this->itemClassificationRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return ItemClassificationStructure
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): ItemClassificationStructure
    {
        /** @var ItemClassification $entity */
        $entity = $this->itemClassificationRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->itemClassificationStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param ItemClassificationStructure $itemClassification
     *
     * @return ItemClassificationStructure
     */
    public function save(ItemClassificationStructure $itemClassification): ItemClassificationStructure
    {
        $structure = $this->itemClassificationSave->save($itemClassification);

        return $structure;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments): void
    {
        /** @var SequenceNumberData $sequenceNumberData */
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            /** @var Itemclassification $entity */
            $entity = $this->itemClassificationRepository->findOneBy(['id' => $sequenceNumberData->id]);

            if (is_object($entity)) {
                $entity->setSequencenumber($sequenceNumberData->sequencenumber);
                $this->itemClassificationRepository->save($entity, false);
            }
        }

        $this->itemClassificationRepository->flush();
    }

    /**
     * @param string $id
     */
    public function delete(string $id): void
    {
        $this->itemClassificationDelete->delete($id);
    }

    /**
     * @return ItemClassificationFrontendStructure[]
     */
    public function frontendList(): array
    {
        $itemClassificationsEntities = $this->itemClassificationRepository->findAllHavingActiveItems();

        return $this->itemClassificationStructureFromEntityConverter->convertMany($itemClassificationsEntities);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['identifier', 'itemclassificationtext.title'];
    }
}
