<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Group;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Structure\Group;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupTranslation;

/**
 * @internal
 */
class GroupStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Itemgroup $entity
     * @param string $structureClassName
     *
     * @return Group
     */
    public function convertOne($entity, $structureClassName = Group::class)
    {
        /** @var Group $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $structure = $this->addTranslationsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Group::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param Group $structure
     * @param Itemgroup $entity
     *
     * @return Group
     */
    protected function addTranslationsToStructure($structure, $entity)
    {
        $translations = [];

        $groupTranslations = $entity->getItemgrouptranslation();

        foreach ($groupTranslations as $groupTranslation) {
            $groupTranslationStructure = $this->structureFromEntityConverter->convertOne($groupTranslation,
                GroupTranslation::class);

            if (!empty($groupTranslationStructure)) {
                $groupTranslationStructure->language = $groupTranslation->getLanguage()
                    ->getIso();
                $translations[] = $groupTranslationStructure;
            }
        }

        $structure->translations = $translations;

        return $structure;
    }
}
