<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Group;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Structure\Group;

/**
 * @internal
 */
class GroupEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * GroupEntityFromStructureConverter constructor.
     *
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(EntityFromStructureConverter $entityFromStructureConverter)
    {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param Group $structure
     * @param Itemgroup $entity
     * @param string $entityClassName
     *
     * @return Itemgroup
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = Itemgroup::class
    ): Itemgroup {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var Itemgroup $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);
        $entity = $this->setDefaultSequenceNumbers($entity);

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($localStructure, $entity,
            'translations',
            'Itemgrouptranslation');
        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations($localStructure, $entity,
            'translations',
            'Itemgrouptranslation');

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Group::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    private function setDefaultSequenceNumbers(Itemgroup $entity): Itemgroup
    {
        if (Itemgroup::IDENTIFIER_COLOR_GROUP === $entity->getIdentifier()) {
            $entity->setSequencenumber(1);
        }
        if (Itemgroup::IDENTIFIER_SIZE_GROUP === $entity->getIdentifier()) {
            $entity->setSequencenumber(2);
        }

        return $entity;
    }
}
