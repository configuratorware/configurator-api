<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Group;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupRelation;

/**
 * @internal
 */
class GroupRelationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Itemgroup $entity
     * @param string $structureClassName
     *
     * @return GroupRelation
     */
    public function convertOne($entity, $structureClassName = GroupRelation::class)
    {
        /** @var GroupRelation $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $structure->translation = $entity->getTranslatedTitle();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = GroupRelation::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
