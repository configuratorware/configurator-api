<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Group;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Group;

/**
 * @internal
 */
class GroupSave
{
    /**
     * @var ItemgroupRepository
     */
    private $groupRepository;

    /**
     * @var GroupEntityFromStructureConverter
     */
    private $groupEntityFromStructureConverter;

    /**
     * @var GroupStructureFromEntityConverter
     */
    private $groupStructureFromEntityConverter;

    /**
     * GroupSave constructor.
     *
     * @param ItemgroupRepository $groupRepository
     * @param GroupEntityFromStructureConverter $groupEntityFromStructureConverter
     * @param GroupStructureFromEntityConverter $groupStructureFromEntityConverter
     */
    public function __construct(
        ItemgroupRepository $groupRepository,
        GroupEntityFromStructureConverter $groupEntityFromStructureConverter,
        GroupStructureFromEntityConverter $groupStructureFromEntityConverter
    ) {
        $this->groupRepository = $groupRepository;
        $this->groupEntityFromStructureConverter = $groupEntityFromStructureConverter;
        $this->groupStructureFromEntityConverter = $groupStructureFromEntityConverter;
    }

    /**
     * @param Group $group
     *
     * @return Group
     */
    public function save(Group $group)
    {
        $entity = null;
        if (isset($group->id) && $group->id > 0) {
            $entity = $this->groupRepository->findOneBy(['id' => $group->id]);
        }

        $entity = $this->groupEntityFromStructureConverter->convertOne($group, $entity);
        $currentSequenceNumber = $entity->getSequencenumber();
        if (null === $currentSequenceNumber || 0 === $currentSequenceNumber) {
            $seqNumber = $this->groupRepository->getNextSequenceNumber();
            $entity->setSequencenumber($seqNumber);
        }

        $entity = $this->groupRepository->save($entity);

        /** @var Itemgroup $entity */
        $entity = $this->groupRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->groupStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
