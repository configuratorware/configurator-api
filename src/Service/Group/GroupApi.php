<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Group;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Group;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

class GroupApi
{
    /**
     * @var ItemgroupRepository
     */
    private $groupRepository;

    /**
     * @var GroupStructureFromEntityConverter
     */
    private $groupStructureFromEntityConverter;

    /**
     * @var GroupSave
     */
    private $groupSave;

    /**
     * @var GroupDelete
     */
    private $groupDelete;

    /**
     * GroupApi constructor.
     *
     * @param ItemgroupRepository $groupRepository
     * @param GroupStructureFromEntityConverter $groupStructureFromEntityConverter
     * @param GroupSave $groupSave
     * @param GroupDelete $groupDelete
     */
    public function __construct(
        ItemgroupRepository $groupRepository,
        GroupStructureFromEntityConverter $groupStructureFromEntityConverter,
        GroupSave $groupSave,
        GroupDelete $groupDelete
    ) {
        $this->groupRepository = $groupRepository;
        $this->groupStructureFromEntityConverter = $groupStructureFromEntityConverter;
        $this->groupSave = $groupSave;
        $this->groupDelete = $groupDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->groupRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->groupStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->groupRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return Group
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): Group
    {
        /** @var Itemgroup $entity */
        $entity = $this->groupRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->groupStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param Group $group
     *
     * @return Group
     */
    public function save(Group $group): Group
    {
        $structure = $this->groupSave->save($group);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->groupDelete->delete($id);
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments)
    {
        /** @var SequenceNumberData $sequenceNumberData */
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            /** @var Itemgroup $entity */
            $entity = $this->groupRepository->findOneBy(['id' => $sequenceNumberData->id]);

            if (is_object($entity)) {
                $entity->setSequencenumber($sequenceNumberData->sequencenumber);
                $this->groupRepository->save($entity, false);
            }
        }

        $this->groupRepository->flush();
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier', 'itemgrouptranslation.translation'];
    }
}
