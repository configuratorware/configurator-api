<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Group;

use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroup;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;

/**
 * @internal
 */
class GroupDelete
{
    /**
     * @var ItemgroupRepository
     */
    private $groupRepository;

    /**
     * GroupDelete constructor.
     *
     * @param ItemgroupRepository $groupRepository
     */
    public function __construct(ItemgroupRepository $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Itemgroup $entity */
            $entity = $this->groupRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            $this->deleteRelation($entity, 'Itemgrouptranslation');
            $this->deleteRelation($entity, 'ItemItemgroup');
            $this->deleteRelation($entity, 'ItemItemgroupentry');
            $this->deleteRelation($entity, 'OptionGroup');
            $this->deleteRelation($entity, 'OptionGroupEntry');

            $this->groupRepository->delete($entity);
        }
    }

    /**
     * @param Itemgroup $entity
     * @param $relation
     */
    private function deleteRelation(Itemgroup $entity, string $relation)
    {
        $relationGetter = 'get' . $relation;

        if (!empty($entity->$relationGetter()->toArray())) {
            foreach ($entity->$relationGetter() as $relationEntity) {
                $this->groupRepository->delete($relationEntity);
            }
        }
    }
}
