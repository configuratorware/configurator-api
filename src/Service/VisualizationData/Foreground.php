<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemGroupingWalker;
use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;
use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfiguration;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class Foreground
{
    /**
     * @var MediaHelper
     */
    private $mediaHelper;

    /**
     * @var FrontendItemGroupingWalker
     */
    private $frontendItemGroupingWalker;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * Layer constructor.
     *
     * @param MediaHelper $mediaHelper
     */
    public function __construct(
        MediaHelper $mediaHelper,
        FrontendItemGroupingWalker $frontendItemGroupingWalker,
        Filesystem $filesystem
    ) {
        $this->mediaHelper = $mediaHelper;
        $this->frontendItemGroupingWalker = $frontendItemGroupingWalker;
        $this->filesystem = $filesystem;
    }

    /**
     * Add the foreground images to visualization data.
     *
     * @param FrontendConfiguration|Configuration $configuration
     */
    public function addForegroundImages($configuration): void
    {
        $visualizationData = $configuration->visualizationData;

        $item = $configuration->item;
        $imageBasePath = rtrim($this->mediaHelper->getImageBasePath(), '/');
        $itemForegroundDir = $imageBasePath . '/item/foreground/' . $item->parentItemIdentifier;
        $itemForegroundRelativeDir = str_replace($this->mediaHelper->getBasePath(), '', $itemForegroundDir);
        if (!is_dir($itemForegroundDir)) {
            return;
        }

        // check if item has children
        $childIdentifiers = [];
        $children = $this->frontendItemGroupingWalker->walk($item);
        foreach ($children as $child) {
            $childIdentifiers[] = $child->identifier;
        }

        // no children, work with parent identifier as a shortcut
        if ([] === $childIdentifiers) {
            $childIdentifiers[] = $item->parentItemIdentifier;
        }

        $visualizationData->foregroundImages = [];

        $finder = new Finder();
        $finder->in($itemForegroundDir)
            ->depth(0)
            ->directories();

        foreach ($finder as $viewDir) {
            $viewIdentifier = $viewDir->getBasename();

            // set path to parent image
            $parentImagePath = $viewDir->getPathname() . '/' . $item->parentItemIdentifier . '.png';

            foreach ($childIdentifiers as $childIdentifier) {
                // work with child image
                $imagePath = $viewDir->getPathname() . '/' . $childIdentifier . '.png';

                if ($this->filesystem->exists($imagePath)) {
                    $visualizationData->foregroundImages[$viewIdentifier][$childIdentifier] =
                        $itemForegroundRelativeDir . '/' . $viewDir->getBasename() . '/' . $childIdentifier . '.png';

                    continue;
                }

                // there is no child image, work with parent image
                if ($this->filesystem->exists($parentImagePath)) {
                    $visualizationData->foregroundImages[$viewIdentifier][$childIdentifier] =
                        $itemForegroundRelativeDir . '/' . $viewDir->getBasename() . '/' . $item->parentItemIdentifier . '.png';
                }
            }
        }
    }
}
