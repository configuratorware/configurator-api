<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\ComponentCombination;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\OptionCombination;
use SplFileInfo;

interface VisualizationResourcesRepositoryInterface
{
    /** @return SplFileInfo[] */
    public function findBaseModelFor(string $itemIdentifier): array;

    /** @return ComponentCombination[] */
    public function findComponentCombinationsFor(string $itemIdentifier): array;

    /** @return OptionCombination[] */
    public function findOptionsFor(ComponentCombination $componentCombination, Configuration $configuration): array;

    /** @return SplFileInfo[] */
    public function findVisualizationFilesForComponent(ComponentCombination $componentCombination): array;

    /** @return SplFileInfo[] */
    public function findVisualizationFilesForOption(OptionCombination $optionCombination): array;
}
