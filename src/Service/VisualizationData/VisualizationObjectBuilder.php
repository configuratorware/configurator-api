<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\VisualizationObject;
use SplFileInfo;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @internal
 */
class VisualizationObjectBuilder
{
    /**
     * @var string
     */
    private $mediaBasePath;

    public function __construct(string $mediaBasePath)
    {
        $this->mediaBasePath = $mediaBasePath;
    }

    public function build(array $resources): VisualizationObject
    {
        $files = [];
        $model = $params = null;
        foreach ($resources as $file) {
            if (preg_match(VisualizationObject::MODEL_EXTENSION, $file->getExtension())) {
                $model = $this->toRelative($file->getPathname());
            } elseif (str_contains($file->getExtension(), VisualizationObject::PARAMS_EXTENSION)) {
                $content = (new File($file->getPathname()))->getContent();
                $params = json_decode($content);
            } elseif (preg_match(VisualizationObject::FILES_EXTENSION, $file->getExtension())) {
                $files[$file->getFilename()] = $this->toRelative($file->getPathname());
            }
        }

        return new VisualizationObject($files, $model, $params);
    }

    /** @return VisualizationObject[] */
    public function from(array $resources): array
    {
        $modelCollection = [];
        $models = $this->findModelsFor($resources);
        $otherFiles = $this->findResourceFilesFor($resources);
        foreach ($models as $model) {
            $params = $this->findMatchingParamsFor($model, $resources);
            if (!$params && 1 === count($models)) {
                $params = $this->findFirstParam($resources);
            }
            $files = array_filter(array_merge([$model], $otherFiles, [$params]));
            $modelCollection[] = $this->build($files);
        }

        if (empty($modelCollection)) { // no models are given, but we may have param-files and images
            $modelCollection[] = $this->build($resources);
        }

        return $modelCollection;
    }

    /**
     * @param array|SplFileInfo[] $resources
     *
     * @return array|SplFileInfo[]
     */
    private function findModelsFor(array $resources): array
    {
        $models = [];
        foreach ($resources as $file) {
            if (preg_match(VisualizationObject::MODEL_EXTENSION, $file->getExtension())) {
                $models[] = $file;
            }
        }

        return $models;
    }

    /**
     * @param SplFileInfo         $model
     * @param array|SplFileInfo[] $resources
     *
     * @return SplFileInfo|null
     */
    private function findMatchingParamsFor(SplFileInfo $model, array $resources): ?SplFileInfo
    {
        $modelName = $model->getBasename('.' . $model->getExtension());

        foreach ($resources as $file) {
            if (str_contains($file->getExtension(), VisualizationObject::PARAMS_EXTENSION)) {
                $filename = $file->getBasename('.' . $file->getExtension());
                if ($filename === $modelName && $file->getPathname() !== $model->getPathname()) {
                    return $file;
                }
            }
        }

        return null;
    }

    /**
     * @param array|SplFileInfo[] $resources
     *
     * @return SplFileInfo|null
     */
    private function findFirstParam(array $resources): ?SplFileInfo
    {
        foreach ($resources as $file) {
            if (str_contains($file->getExtension(), VisualizationObject::PARAMS_EXTENSION)) {
                return $file;
            }
        }

        return null;
    }

    /**
     * @param array|SplFileInfo[] $resources
     *
     * @return array|SplFileInfo[]
     */
    private function findResourceFilesFor(array $resources): array
    {
        $resourceFiles = [];
        foreach ($resources as $file) {
            if (preg_match(VisualizationObject::FILES_EXTENSION, $file->getExtension())) {
                $resourceFiles[] = $file;
            }
        }

        return $resourceFiles;
    }

    private function toRelative(string $pathName): string
    {
        return str_replace($this->mediaBasePath . '/', '', $pathName);
    }
}
