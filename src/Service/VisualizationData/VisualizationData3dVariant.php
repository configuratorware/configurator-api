<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\Default3dViewFactory;
use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\VisualizationData3dVariant as VisualizationData3dVariantStructure;
use stdClass;

/**
 * @internal
 */
class VisualizationData3dVariant
{
    /**
     * @var VisualizationData3d
     */
    private $visualizationData3d;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemgroupRepository
     */
    private $itemGroupRepository;

    /**
     * VisualizationData3dVariant constructor.
     *
     * @param VisualizationData3d $visualizationData3d
     * @param ItemRepository $itemRepository
     * @param ItemgroupRepository $itemGroupRepository
     */
    public function __construct(
        VisualizationData3d $visualizationData3d,
        ItemRepository $itemRepository,
        ItemgroupRepository $itemGroupRepository
    ) {
        $this->visualizationData3d = $visualizationData3d;
        $this->itemRepository = $itemRepository;
        $this->itemGroupRepository = $itemGroupRepository;
    }

    /**
     * @param FrontendConfiguration|Configuration $configuration
     *
     * @return VisualizationData3dVariantStructure
     */
    public function getData($configuration): VisualizationData3dVariantStructure
    {
        $files = $this->getModelMap($configuration);

        $visualizationData = new VisualizationData3dVariantStructure();

        $designView = new stdClass();
        $designView->identifier = Default3dViewFactory::DEFAULT_3D_VIEW_IDENTIFIER;

        $designView->models = $this->processFileData($files);
        $visualizationData->designViews[] = $designView;

        return $visualizationData;
    }

    /**
     * process list of found files and create sceneData objects from them.
     *
     * @param array $fileData
     *
     * @return stdClass
     */
    private function processFileData(array $fileData): stdClass
    {
        $models = new stdClass();

        foreach ($fileData as $itemIdentifier => $modelData) {
            foreach ($modelData as $modelMapEntry) {
                if (!empty($modelMapEntry->model)) {
                    $sceneDataEntry = $this->visualizationData3d->createSceneDataEntry($modelMapEntry);
                    $models->$itemIdentifier[] = $sceneDataEntry;
                }
            }
        }

        return $models;
    }

    /**
     * get all files from the items directory and filter them by the groups and group values per child in the item.
     *
     * additional map files like alpha maps, normal maps etc. are found by parsing references from the json files
     *
     * @param FrontendConfiguration|Configuration $configuration
     *
     * @return array
     */
    private function getModelMap($configuration): array
    {
        $dataDir = $this->visualizationData3d->getDataDir($configuration->item->identifier, VisualizationMode::IDENTIFIER_3D_VARIANT);
        $finder = $this->visualizationData3d->getFileList($dataDir);

        if (null === $finder) {
            return [];
        }

        $groupMap = $this->getGroupMap($configuration->item->identifier);

        $modelMap = [];
        // find the list of files for each variant
        foreach ($groupMap as $itemIdentifier => $mapping) {
            $filesFiltered[$itemIdentifier] = [];

            // expressions for files on the root level of the item
            $regex = [
                'model' => [
                    '\/' . $configuration->item->identifier . '\/([^\/]*)\.gltf',
                ],
                'params' => [
                    '\/' . $configuration->item->identifier . '\/([^\/]*)\.json',
                ],
                'texture' => [
                    '\/' . $configuration->item->identifier . '\/([^\/]*)\.(png|jpg|jpeg)',
                ],
            ];

            // each group / group entry pair of an item can lead to a deeper level in the directory structure
            // a regular expression for each path depth is created to find all relevant files within the tree
            // add the file grouped by their basename to the list of files per item (variant)
            $path = '';
            foreach ($mapping as $pathFragment) {
                $path .= '\/' . $pathFragment;

                $regex['model'][] = $path . '\/([^\/]*)\.gltf';
                $regex['params'][] = $path . '\/([^\/]*)\.json';
                $regex['texture'][] = $path . '\/([^\/]*)\.(png|jpg|jpeg)';
            }

            foreach ($finder as $fileInfo) {
                $baseName = $fileInfo->getBasename('.' . $fileInfo->getExtension());
                foreach ($regex as $type => $expressions) {
                    $expression = '/' . implode('|', $expressions) . '/i';

                    $fileValid = $this->visualizationData3d->checkFileIsValid($fileInfo, $expression);
                    if (true === $fileValid) {
                        $modelMap[$itemIdentifier][$baseName] = $this->visualizationData3d->mergeModelMapEntry($fileInfo, $type,
                            $modelMap[$itemIdentifier][$baseName] ?? null);
                        // this file was found to belong to one type of file no need to check if its another one too
                        // skip to the next file
                        continue 2;
                    }
                }
            }
        }

        return $modelMap;
    }

    /**
     * collect group/group entry pairs per item.
     *
     * @param string $itemIdentifier
     *
     * @return array
     */
    private function getGroupMap(string $itemIdentifier): array
    {
        $children = $this->itemRepository->getItemsByParent($itemIdentifier);
        $itemIdentifiers = [];
        foreach ($children as $child) {
            $itemIdentifiers[] = $child->getIdentifier();
        }
        $groupMapping = $this->itemGroupRepository->getItemGroupMapping($itemIdentifiers);

        $groupMap = [];
        foreach ($groupMapping as $mapping) {
            $groupMap[$mapping['item_identifier']][] = implode('\/', [$mapping['group_identifier'], $mapping['group_entry_identifier']]);
        }

        return $groupMap;
    }
}
