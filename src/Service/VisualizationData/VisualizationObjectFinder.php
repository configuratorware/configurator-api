<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\VisualizationObject;

/**
 * @internal
 */
class VisualizationObjectFinder
{
    public const SEPARATOR = '+';

    /**
     * @var VisualizationResourcesRepositoryInterface
     */
    private $repository;

    /**
     * @var VisualizationObjectBuilder
     */
    private $visualizationObjectBuilder;

    public function __construct(VisualizationResourcesRepositoryInterface $repository, string $mediaBasePath)
    {
        $this->repository = $repository;
        $this->visualizationObjectBuilder = new VisualizationObjectBuilder($mediaBasePath);
    }

    /** @return VisualizationObject[] */
    public function findBaseModelVisualizationObjectFor(Configuration $configuration): array
    {
        $resources = $this->repository->findBaseModelFor($configuration->itemIdentifier);

        return $this->visualizationObjectBuilder->from($resources);
    }

    /** @return VisualizationObject[] */
    public function findVisualizationObjectsFor(Configuration $configuration): array
    {
        $visualizationData = [];
        $componentCombinations = $this->repository->findComponentCombinationsFor($configuration->itemIdentifier);

        foreach ($componentCombinations as $componentCombination) {
            $modelFound = false;
            $optionsCombinations = $this->repository->findOptionsFor($componentCombination, $configuration);

            foreach ($optionsCombinations as $optionsCombination) {
                $resources = $this->repository->findVisualizationFilesForOption($optionsCombination);
                $optionVisualizationData = $this->visualizationObjectBuilder->from($resources);
                $visualizationData[] = $optionVisualizationData;

                foreach ($optionVisualizationData as $data) {
                    if ($data->model) {
                        $modelFound = true;
                    }
                }
            }

            if (!$modelFound) {
                $resources = $this->repository->findVisualizationFilesForComponent($componentCombination);
                $visualizationData[] = $this->visualizationObjectBuilder->from($resources);
            }
        }

        return array_merge(...$visualizationData);
    }
}
