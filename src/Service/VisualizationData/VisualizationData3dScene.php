<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO\Configuration as ConfigurationDTO;
use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\VisualizationData3dScene as VisualizationData3dSceneStructure;

/**
 * @internal
 */
class VisualizationData3dScene
{
    /**
     * @var VisualizationObjectFinder
     */
    private $visualizationDataFinder;

    /**
     * VisualizationData3dScene constructor.
     *
     * @param VisualizationObjectFinder $visualizationDataFinder
     */
    public function __construct(VisualizationObjectFinder $visualizationDataFinder)
    {
        $this->visualizationDataFinder = $visualizationDataFinder;
    }

    /**
     * @param FrontendConfiguration|Configuration $configuration
     *
     * @return VisualizationData3dSceneStructure
     */
    public function getData($configuration): VisualizationData3dSceneStructure
    {
        $visualizationData = [];

        $configurationDTO = ConfigurationDTO::from($configuration);
        $visualizationData[] = $this->visualizationDataFinder->findBaseModelVisualizationObjectFor($configurationDTO);
        $visualizationData[] = $this->visualizationDataFinder->findVisualizationObjectsFor($configurationDTO);

        $structure = new VisualizationData3dSceneStructure();
        $structure->sceneData = array_merge(...$visualizationData);

        return $structure;
    }
}
