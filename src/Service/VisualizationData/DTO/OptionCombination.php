<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO;

use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationObjectFinder;

class OptionCombination
{
    /**
     * @var false|string[]
     */
    private $options;

    /**
     * @var string
     */
    public $pathName;

    public function __construct(\SplFileInfo $fileInfo)
    {
        $this->options = explode(VisualizationObjectFinder::SEPARATOR, $fileInfo->getFilename());
        $this->pathName = $fileInfo->getPathname();
    }

    /** @param string[] $options */
    public function doesMatch(array $options): bool
    {
        return (is_array($this->options) && 1 === count($this->options)) ? in_array($this->options[0], $options, true) : $this->options === $options;
    }
}
