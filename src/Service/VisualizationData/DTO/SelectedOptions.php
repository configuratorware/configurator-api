<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO;

class SelectedOptions
{
    /** @param string[] $components */
    public static function inOrderForComponents(Configuration $configuration, array $components): array
    {
        $selectedOptions = [];

        foreach ($components as $component) {
            foreach ($configuration->optionclassifications as $configurationComponent) {
                if ($configurationComponent->identifier === $component) {
                    $configurationSelectedOptions = $configurationComponent->selectedoptions ?? [];
                    foreach ($configurationSelectedOptions as $configurationSelectedOption) {
                        $selectedOptions[] = $configurationSelectedOption->identifier;
                    }
                }
            }
        }

        return $selectedOptions;
    }
}
