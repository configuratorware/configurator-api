<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO;

use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationObjectFinder;

class ComponentCombination
{
    private const OPTIONS_FOLDER = '/options';

    /**
     * @var string
     */
    public $componentFolder;

    /**
     * @var string[]
     */
    public $components;

    /**
     * @var string
     */
    public $optionFolder;

    public function __construct(\SplFileInfo $fileInfo)
    {
        $this->componentFolder = $fileInfo->getPathname();
        $this->components = explode(VisualizationObjectFinder::SEPARATOR, $fileInfo->getFilename());
        $this->optionFolder = $fileInfo->getPathname() . self::OPTIONS_FOLDER;
    }
}
