<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\DTO;

use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration as ConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendStructure;

class Configuration
{
    /**
     * @var string
     */
    public $itemIdentifier;

    /**
     * @var array
     */
    public $optionclassifications;

    /**
     * @param ConfigurationStructure|FrontendStructure $configuration
     */
    public static function from($configuration): self
    {
        $dto = new self();
        $dto->itemIdentifier = $configuration->item->identifier;
        $dto->optionclassifications = $configuration->optionclassifications;

        return $dto;
    }
}
