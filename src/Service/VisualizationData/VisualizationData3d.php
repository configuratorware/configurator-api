<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValueNotAllowedException;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\Material;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\ModelMapEntry;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationData\SceneData;
use stdClass;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * @internal
 */
class VisualizationData3d
{
    /**
     * the data is stored in a directory called creator or designer because the user not necessarily the visualization mode when storing the data.
     */
    public const SUB_DIR_MAP = [
        VisualizationMode::IDENTIFIER_3D_SCENE => 'creator',
        VisualizationMode::IDENTIFIER_3D_VARIANT => 'designer',
    ];

    /**
     * @var string
     */
    private $publicDir;

    /**
     * @var string
     */
    private $visualizationComponentDataDir;

    /**
     * VisualizationData3d constructor.
     *
     * @param string $publicDir
     * @param string $visualizationComponentDataDir
     */
    public function __construct(string $publicDir, string $visualizationComponentDataDir)
    {
        $this->publicDir = $publicDir;
        $this->visualizationComponentDataDir = $visualizationComponentDataDir;
    }

    /**
     * @param string $itemIdentifier
     * @param string $visualizationMode
     *
     * @return string
     */
    public function getDataDir(string $itemIdentifier, string $visualizationMode): string
    {
        if (!array_key_exists($visualizationMode, self::SUB_DIR_MAP)) {
            throw ValueNotAllowedException::expected($visualizationMode, array_keys(self::SUB_DIR_MAP));
        }

        return str_replace(
            ['{configurationMode}', '{itemIdentifier}'],
            [self::SUB_DIR_MAP[$visualizationMode], $itemIdentifier],
            $this->visualizationComponentDataDir
        );
    }

    /**
     * @param string $dataDir
     *
     * @return Finder
     */
    public function getFileList(string $dataDir): ?Finder
    {
        $finder = null;
        if (is_dir($dataDir)) {
            $finder = new Finder();

            // sort by directory depth (we want root level files first to have more specific files "win" over higher level files)
            $finder->sort(static function (SplFileInfo $a, SplFileInfo $b) {
                return substr_count($a->getPathname(), '/') - substr_count($b->getPathname(), '/');
            });

            $finder->in($dataDir)->files()->name('/\.(gltf|json|png|jpg|jpeg)$/i');
        }

        return $finder;
    }

    /**
     * @param SplFileInfo $fileInfo
     * @param string $regex
     *
     * @return bool
     */
    public function checkFileIsValid(SplFileInfo $fileInfo, string $regex)
    {
        preg_match($regex, $fileInfo->getPathname(), $matches);

        return !empty($matches);
    }

    /**
     * @param SplFileInfo $fileInfo
     * @param string $type
     * @param ModelMapEntry|null $modelMapEntry
     *
     * @return ModelMapEntry
     */
    public function mergeModelMapEntry(SplFileInfo $fileInfo, string $type, ?ModelMapEntry $modelMapEntry): ModelMapEntry
    {
        if (null === $modelMapEntry) {
            $modelMapEntry = new ModelMapEntry();
        }

        // params are merged per selection
        if ('json' === $fileInfo->getExtension()) {
            // read params as assoc array to be able to use array_merge to combine them
            $params = json_decode($fileInfo->getContents(), true);
            if (!empty($params)) {
                $modelMapEntry->params = array_merge($modelMapEntry->params,
                    $this->resolveLinkedMaps($fileInfo, $params));
            }
        } // models and textures are overwritten
        else {
            $modelMapEntry->$type = $fileInfo->getPathname();
        }

        return $modelMapEntry;
    }

    /**
     * @param ModelMapEntry $modelMapEntry
     *
     * @return SceneData
     */
    public function createSceneDataEntry(ModelMapEntry $modelMapEntry): SceneData
    {
        $sceneDataEntry = new SceneData();
        $sceneDataEntry->src = $this->pathToUri($modelMapEntry->model);

        $material = new Material();
        $material->params = new stdClass();

        if (!empty($modelMapEntry->params)) {
            $material->params = (object)$modelMapEntry->params;
        }

        if (!empty($modelMapEntry->texture)) {
            $material->params->map = $this->pathToUri($modelMapEntry->texture);
        }

        if (!empty($modelMapEntry->params) || !empty($modelMapEntry->texture)) {
            $sceneDataEntry->material = $material;
        }

        return $sceneDataEntry;
    }

    /**
     * check map settings in params file, if the value of the setting points to an existing file
     * replace the value with the uri to the file so the frontend can load it.
     *
     * @param SplFileInfo $fileInfo
     * @param array $params
     *
     * @return array
     */
    private function resolveLinkedMaps(SplFileInfo $fileInfo, array $params): array
    {
        foreach ($params as $key => $param) {
            if (false !== strpos($key, 'Map')) {
                $mapFile = $fileInfo->getPath() . '/' . $param;
                if (is_file($mapFile)) {
                    $params[$key] = $this->pathToUri($mapFile);
                } else {
                    unset($params[$key]);
                }
            }
        }

        return $params;
    }

    /**
     * @param string $fileName
     *
     * @return string
     */
    private function pathToUri(string $fileName): string
    {
        return str_replace($this->publicDir, '', $fileName);
    }
}
