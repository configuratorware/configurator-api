<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LayerImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfiguration;
use stdClass;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class Layer
{
    private LayerImagePathsInterface $layerImagePaths;

    public function __construct(LayerImagePathsInterface $layerImagePaths)
    {
        $this->layerImagePaths = $layerImagePaths;
    }

    /**
     * Add the view images to visualization data.
     *
     * @param FrontendConfiguration|Configuration $configuration
     */
    public function addViewImages($configuration): void
    {
        $visualizationData = $configuration->visualizationData;

        // we do not need to search for images if the item has no construction pattern
        if (!empty($configuration->optionclassifications)) {
            $imageBasePath = rtrim($this->layerImagePaths->getLayerImagePath(), '/');
            $imagePath = $imageBasePath . DIRECTORY_SEPARATOR . $configuration->item->identifier;

            if (!is_dir($imagePath)) {
                return;
            }

            $finder = new Finder();
            $finder->sortByName(true)->in($imagePath)->files()->name('/\.(png|jpg|jpeg)$/i');

            $viewImages = new stdClass();
            $viewRegex = '/' . str_replace('/', '\/', $imagePath) . '\/([0-9_]*[^\/]*)/i';

            foreach ($finder as $fileInfo) {
                $image = $fileInfo->getPathname();

                preg_match($viewRegex, $image, $viewMatches);
                $viewIdentifier = $viewMatches[1];

                if (!isset($viewImages->$viewIdentifier)) {
                    $viewImages->$viewIdentifier = [];
                }

                $classificationRegex = '/' . $viewIdentifier . '\/[0-9_]*([^\/]*)/i';
                preg_match($classificationRegex, $image, $classificationMatches);

                if (isset($classificationMatches[1])) {
                    $classificationIdentifier = $classificationMatches[1];

                    // if the image is for one on the selected options add it
                    $optionIdentifiers = [];

                    // get all option identifiers from the current image name
                    $imageNameParts = explode('.', basename($image));
                    array_pop($imageNameParts);
                    $imageOptionIdentifiers = explode('+', implode('.', $imageNameParts));

                    foreach ($configuration->optionclassifications as $optionClassification) {
                        if (in_array((string)$optionClassification->identifier, explode('+', $classificationIdentifier), true)
                            && is_array($optionClassification->selectedoptions)
                        ) {
                            foreach ($optionClassification->selectedoptions as $selectedOption) {
                                if (in_array((string)$selectedOption->identifier, $imageOptionIdentifiers, true)) {
                                    $optionIdentifiers[] = $selectedOption->identifier;
                                }
                            }
                        }
                    }

                    // if for all identifier parts of the image name a selected option was found, the image is output
                    if (!empty($optionIdentifiers) && count($optionIdentifiers) === count($imageOptionIdentifiers)) {
                        $viewImages->$viewIdentifier[] = str_replace($imageBasePath, DIRECTORY_SEPARATOR . $this->layerImagePaths->getLayerImagePathRelative(), $image);
                    }
                }
            }

            // sort images within view
            foreach ($viewImages as $viewIdentifier => $images) {
                natsort($images);
                $viewImages->$viewIdentifier = array_values($images);
            }

            $visualizationData->viewImages = $viewImages;
        }
    }
}
