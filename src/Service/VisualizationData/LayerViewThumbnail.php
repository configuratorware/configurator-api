<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class LayerViewThumbnail
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $layerViewThumbnailPath;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * LayerViewThumbnail constructor.
     *
     * @param Filesystem $filesystem
     * @param string $layerViewThumbnailPath
     * @param string $mediaBasePath
     */
    public function __construct(
        Filesystem $filesystem,
        string $layerViewThumbnailPath,
        string $mediaBasePath
    ) {
        $this->filesystem = $filesystem;
        $this->layerViewThumbnailPath = trim($layerViewThumbnailPath, '/');
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
    }

    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    public function getThumbnails(string $itemIdentifier): array
    {
        $thumbnailsDir = $this->mediaBasePath . '/' . $this->layerViewThumbnailPath . '/' . $itemIdentifier . '/' . $itemIdentifier . '/';

        if (false === $this->filesystem->exists($thumbnailsDir)) {
            return [];
        }

        $finder = new Finder();
        $finder->sortByName()->in($thumbnailsDir)->files()->name('/\.(png|jpg|jpeg)$/i');

        $viewThumbnails = [];
        foreach ($finder as $fileInfo) {
            $viewName = $fileInfo->getBasename('.' . $fileInfo->getExtension());
            $viewThumbnails[$viewName] =
                str_replace($this->mediaBasePath, '', $fileInfo->getPath()) . '/' . $fileInfo->getFilename();
        }

        return $viewThumbnails;
    }
}
