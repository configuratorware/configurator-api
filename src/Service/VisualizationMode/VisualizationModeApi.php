<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationMode;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\VisualizationModeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationMode as VisualizationModeStructure;

class VisualizationModeApi
{
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var VisualizationModeList
     */
    private $visualizationModeList;

    /**
     * @var VisualizationModeRepository
     */
    private $visualizationModeRepository;

    /**
     * @param SettingRepository $settingRepository
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param VisualizationModeList $visualizationModeList
     * @param VisualizationModeRepository $visualizationModeRepository
     */
    public function __construct(
        SettingRepository $settingRepository,
        StructureFromEntityConverter $structureFromEntityConverter,
        VisualizationModeList $visualizationModeList,
        VisualizationModeRepository $visualizationModeRepository
    ) {
        $this->settingRepository = $settingRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->visualizationModeList = $visualizationModeList;
        $this->visualizationModeRepository = $visualizationModeRepository;
    }

    /**
     * @param ListRequestArguments $arguments
     * @param string $configurationMode
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments, string $configurationMode): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->visualizationModeRepository->fetchList($arguments, $this->getSearchableFields());

        $structures = $this->structureFromEntityConverter->convertMany($entities, VisualizationModeStructure::class);
        $structures = $this->visualizationModeList->filter($structures, $configurationMode);
        $structures = $this->visualizationModeList->prioritize($structures,
            (string)$this->settingRepository->find(1)->getDefaultVisualizationModeLabel());

        $paginationResult->data = $structures;
        $paginationResult->count = count($structures);

        return $paginationResult;
    }

    /**
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['identifier', 'label'];
    }
}
