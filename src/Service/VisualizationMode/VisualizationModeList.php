<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\VisualizationMode;

use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationMode;

/**
 * @internal
 */
class VisualizationModeList
{
    /**
     * @param VisualizationMode[] $structures
     * @param string $configurationMode
     *
     * @return VisualizationMode[]
     */
    public function filter(array $structures, string $configurationMode): array
    {
        $filtered = [];

        foreach ($structures as $structure) {
            if (in_array($configurationMode, $structure->validForConfigurationModes, true)) {
                $filtered[] = $structure;
            }
        }

        return $filtered;
    }

    /**
     * @param VisualizationMode[] $structures
     * @param string $priority
     *
     * @return VisualizationMode[]
     */
    public function prioritize(array $structures, string $priority): array
    {
        $sorted = [];

        foreach ($structures as $structure) {
            if ($priority === $structure->label) {
                array_unshift($sorted, $structure);
            } else {
                $sorted[] = $structure;
            }
        }

        return $sorted;
    }
}
