<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\RefreshBrowsercache;

use DOMDocument;

final class RefreshBrowsercacheHash
{
    private const VERSION = 'version=';
    private const VERSION_SEARCH_TERM = '/(version=).*/';
    private const SCRIPT_TAG = 'script';
    private const HEAD_TAG = 'head';
    private const LINK_TAG = 'link';
    private const HREF_ATTR = 'href';
    private const SRC_ATTR = 'src';

    /**
     * @param string $indexHtmlContent
     * @param string $hash
     *
     * @return string
     */
    public function refresh(string $indexHtmlContent, string $hash): string
    {
        $indexHtmlAsDomDoc = new DOMDocument();
        $indexHtmlAsDomDoc->loadHTML($indexHtmlContent);

        // find js files path
        foreach ($indexHtmlAsDomDoc->getElementsByTagName(self::SCRIPT_TAG) as $scriptTag) {
            $src = $scriptTag->getAttribute(self::SRC_ATTR);
            if (false !== strpos($src, self::VERSION)) {
                $this->updateIndexHtmlContent($indexHtmlContent, $hash, $src);
            }
        }

        // find css bundle path
        foreach ($indexHtmlAsDomDoc->getElementsByTagName(self::HEAD_TAG) as $head) {
            foreach ($head->getElementsByTagName(self::LINK_TAG) as $linkTag) {
                $href = $linkTag->getAttribute(self::HREF_ATTR);
                if (false !== strpos($href, self::VERSION)) {
                    $this->updateIndexHtmlContent($indexHtmlContent, $hash, $href);
                }
            }
        }

        return $indexHtmlContent;
    }

    /**
     * @param $indexHtmlContent
     * @param $hash
     * @param $src
     */
    private function updateIndexHtmlContent(&$indexHtmlContent, $hash, $src): void
    {
        $srcOrig = $src;

        $srcReplaced = preg_replace(self::VERSION_SEARCH_TERM, self::VERSION . $hash, $src);

        $indexHtmlContent = str_replace($srcOrig, $srcReplaced, $indexHtmlContent);
    }
}
