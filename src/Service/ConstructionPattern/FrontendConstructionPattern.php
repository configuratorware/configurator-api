<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification\FrontendOptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

/**
 * @internal
 */
class FrontendConstructionPattern
{
    /**
     * @var FrontendOptionClassification
     */
    private $frontendOptionClassification;

    /**
     * @var FrontendOptionStructureFromEntityConverter
     */
    private $frontendOptionStructureFromEntityConverter;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * FrontendConstructionPattern constructor.
     *
     * @param FrontendOptionClassification $frontendOptionClassification
     * @param FrontendOptionStructureFromEntityConverter $frontendOptionStructureFromEntityConverter
     * @param OptionRepository $optionRepository
     */
    public function __construct(
        FrontendOptionClassification $frontendOptionClassification,
        FrontendOptionStructureFromEntityConverter $frontendOptionStructureFromEntityConverter,
        OptionRepository $optionRepository
    ) {
        $this->frontendOptionClassification = $frontendOptionClassification;
        $this->frontendOptionStructureFromEntityConverter = $frontendOptionStructureFromEntityConverter;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param Configuration $configuration
     *
     * @return array
     */
    public function getConstructionPatternForConfiguration(Configuration $configuration)
    {
        $optionClassifications = $this->frontendOptionClassification->getByItemId($configuration->getItem()->getId());

        if (!empty($configuration->getSelectedoptions()) && !empty($optionClassifications)) {
            foreach ($configuration->getSelectedoptions() as $optionClassificationIdentifier => $options) {
                foreach ($optionClassifications as $optionClassification) {
                    if ($optionClassification->identifier == $optionClassificationIdentifier) {
                        $selectedOptions = [];
                        foreach ($options as $option) {
                            $optionIdentifier = $option && $option['identifier'] ? $option['identifier'] : $option;
                            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Option $optionEntity */
                            $optionEntity = $this->optionRepository->findOneBy(['identifier' => $optionIdentifier]);
                            if (!empty($optionEntity) && 1 != $optionEntity->getDeactivated()) {
                                $optionStructure = $this->frontendOptionStructureFromEntityConverter->convertOne(
                                    $optionEntity,
                                    Option::class,
                                    $configuration->getItem()->getIdentifier(),
                                    $optionClassification->identifier
                                );

                                // selected amount, default to 1
                                $optionStructure->amount = 1;

                                if (!empty($option['amount'])) {
                                    $optionStructure->amount = $option['amount'];
                                }
                                if (array_key_exists('inputText', $option)) {
                                    $optionStructure->inputText = $option['inputText'];
                                }

                                $selectedOptions[] = $optionStructure;
                            }
                        }

                        $optionClassification->selectedoptions = $selectedOptions;
                    }
                }
            }
        }

        return $optionClassifications;
    }
}
