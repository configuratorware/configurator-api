<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOptionDeltaprice;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConstructionPattern;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableComponent;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOption;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOptionDeltaPrice;

/**
 * @internal
 */
class ConstructionPatternSave
{
    private ChannelRepository $channelRepository;

    private ItemRepository $itemRepository;

    private OptionRepository $optionRepository;

    private OptionclassificationRepository $optionClassificationRepository;

    private CreatorViewRepository $creatorViewRepository;

    public function __construct(
        ChannelRepository $channelRepository,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository,
        OptionclassificationRepository $optionClassificationRepository,
        CreatorViewRepository $creatorViewRepository
    ) {
        $this->channelRepository = $channelRepository;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
        $this->optionClassificationRepository = $optionClassificationRepository;
        $this->creatorViewRepository = $creatorViewRepository;
    }

    public function save(ConstructionPattern $constructionPattern): void
    {
        $item = $this->itemRepository->find($constructionPattern->item->id);

        if (null === $item) {
            throw NotFoundException::entityWithId(Item::class, $constructionPattern->item->id);
        }

        $channelMap = $this->createChannelMap();
        $componentMap = $this->createExistingComponentRelationsMap($item->getItemOptionclassification());
        $optionMap = $this->createExistingOptionRelationsMap($item->getItemOptionclassification());
        $deltaPriceMap = $this->createDeltaPriceMap($item->getItemOptionclassification());

        $componentOptionCombinationsToKeep = [];
        $deltaPricesToKeep = [];

        foreach ($constructionPattern->selectableComponents as $optionClassificationToSave) {
            $itemOptionClassification = $componentMap[$optionClassificationToSave->id] ?? $this->createOptionClassificationFrom($optionClassificationToSave->id);

            if (null !== $itemOptionClassification->getId()) {
                $componentOptionCombinationsToKeep[$itemOptionClassification->getId()] = [];
            } else {
                $item->addItemOptionclassification($itemOptionClassification);
            }

            $this->updateItemOptionClassificationFromStructure($itemOptionClassification, $optionClassificationToSave);

            foreach ($optionClassificationToSave->selectableOptions as $selectableOption) {
                $itemOptionClassificationOption = $optionMap[$optionClassificationToSave->id][$selectableOption->id] ?? $this->createItemOptionClassificationOptionFrom($selectableOption->id);

                if (null !== $itemOptionClassificationOption->getId()) {
                    $componentOptionCombinationsToKeep[$itemOptionClassification->getId()][] = $itemOptionClassificationOption->getId();
                } else {
                    $itemOptionClassification->addItemOptionclassificationOption($itemOptionClassificationOption);
                }

                $this->updateItemOptionFromStructure($itemOptionClassificationOption, $selectableOption);

                foreach ($selectableOption->deltaPrices as $deltaPrice) {
                    if (null !== $deltaPrice->id && array_key_exists($deltaPrice->id, $deltaPriceMap)) {
                        $itemOptionClassificationOptionDeltaPrice = $deltaPriceMap[$deltaPrice->id];
                        $deltaPricesToKeep[] = $itemOptionClassificationOptionDeltaPrice->getId();
                    } else {
                        $itemOptionClassificationOptionDeltaPrice = new ItemOptionclassificationOptionDeltaprice();
                        $itemOptionClassificationOption->addItemOptionclassificationOptionDeltaprice($itemOptionClassificationOptionDeltaPrice);
                    }

                    $this->updateDeltaPriceFromStructure($itemOptionClassificationOptionDeltaPrice, $deltaPrice, $channelMap);
                }
            }
        }

        $this->delete($item, $componentOptionCombinationsToKeep, $deltaPricesToKeep);

        $this->itemRepository->save($item);
    }

    /**
     * @param Item $item
     * @param array $componentOptionCombinationsToKeep
     * @param array $deltaPricesToKeep
     */
    private function delete(Item $item, array $componentOptionCombinationsToKeep, array $deltaPricesToKeep): void
    {
        // if an entity has no id, it was just created -> can be skipped

        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($item->getItemOptionclassification() as $itemOptionClassification) {
            $itemOptionClassificationId = $itemOptionClassification->getId();
            if (null === $itemOptionClassificationId) {
                continue;
            }

            if (!isset($componentOptionCombinationsToKeep[$itemOptionClassificationId])) {
                $item->removeItemOptionclassification($itemOptionClassification);

                continue;
            }

            /** @var ItemOptionclassificationOption $itemOptionClassificationOption */
            foreach ($itemOptionClassification->getItemOptionclassificationOption() as $itemOptionClassificationOption) {
                $itemOptionClassificationOptionId = $itemOptionClassificationOption->getId();
                if (null === $itemOptionClassificationOptionId) {
                    continue;
                }

                $idsToKeep = $componentOptionCombinationsToKeep[$itemOptionClassificationId];

                if (!in_array($itemOptionClassificationOptionId, $idsToKeep, false)) {
                    $itemOptionClassification->removeItemOptionclassificationOption($itemOptionClassificationOption);

                    continue;
                }

                foreach ($itemOptionClassificationOption->getItemOptionclassificationOptionDeltaprice() as $deltaPrice) {
                    $deltaPriceId = $deltaPrice->getId();
                    if (null === $deltaPriceId) {
                        continue;
                    }

                    if (!in_array($deltaPriceId, $deltaPricesToKeep, false)) {
                        $itemOptionClassificationOption->removeItemOptionclassificationOptionDeltaprice($deltaPrice);
                    }
                }
            }
        }
    }

    private function createOptionClassificationFrom(int $optionClassificationId): ItemOptionclassification
    {
        $optionClassification = $this->optionClassificationRepository->find($optionClassificationId);

        return (new ItemOptionclassification())->setOptionclassification($optionClassification);
    }

    private function createItemOptionClassificationOptionFrom(int $selectableOptionId): ItemOptionclassificationOption
    {
        $option = $this->optionRepository->find($selectableOptionId);

        return (new ItemOptionclassificationOption())->setOption($option);
    }

    private function updateItemOptionClassificationFromStructure(ItemOptionclassification $itemOptionClassification, SelectableComponent $optionClassificationToSave): void
    {
        $itemOptionClassification->setIsMultiselect($optionClassificationToSave->isMultiselect ?? false);
        $itemOptionClassification->setIsMandatory($optionClassificationToSave->isMandatory ?? true);
        $itemOptionClassification->setSequenceNumber((int)$optionClassificationToSave->sequenceNumber);
        $creatorView = null;
        if (null !== $optionClassificationToSave->creatorViewId) {
            $creatorView = $this->creatorViewRepository->find($optionClassificationToSave->creatorViewId);
        }
        $itemOptionClassification->setCreatorView($creatorView);
    }

    private function updateItemOptionFromStructure(ItemOptionclassificationOption $itemOptionClassificationOption, SelectableOption $selectableOption): void
    {
        $itemOptionClassificationOption->setAmountisselectable($selectableOption->amountIsSelectable);
        $itemOptionClassificationOption->setSequenceNumber($selectableOption->sequenceNumber);
    }

    private function updateDeltaPriceFromStructure(ItemOptionclassificationOptionDeltaprice $itemOptionClassificationOptionDeltaPrice, SelectableOptionDeltaPrice $deltaPrice, array $channelMap): void
    {
        if (!isset($deltaPrice->channel) || !array_key_exists($deltaPrice->channel, $channelMap)) {
            throw NotFoundException::entity(Channel::class);
        }

        $itemOptionClassificationOptionDeltaPrice->setPrice((float)$deltaPrice->price);
        $itemOptionClassificationOptionDeltaPrice->setPriceNet((float)$deltaPrice->priceNet);
        $itemOptionClassificationOptionDeltaPrice->setChannel($channelMap[$deltaPrice->channel]);
    }

    /**
     * @return Channel[]
     */
    private function createChannelMap(): array
    {
        $all = $this->channelRepository->findAll();

        $map = [];
        foreach ($all as $channel) {
            $map[$channel->getId()] = $channel;
        }

        return $map;
    }

    public function createExistingComponentRelationsMap(Collection $itemOptionClassifications): array
    {
        $map = [];

        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($itemOptionClassifications as $itemOptionClassification) {
            $componentId = $itemOptionClassification->getOptionclassification()->getId();
            $map[$componentId] = $itemOptionClassification;
        }

        return $map;
    }

    public function createExistingOptionRelationsMap(Collection $itemOptionClassifications): array
    {
        $map = [];

        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($itemOptionClassifications->toArray() as $itemOptionClassification) {
            $componentId = $itemOptionClassification->getOptionclassification()->getId();

            /** @var ItemOptionclassificationOption $componentOption */
            foreach ($itemOptionClassification->getItemOptionclassificationOption() as $componentOption) {
                $optionId = $componentOption->getOption()->getId();
                $map[$componentId][$optionId] = $componentOption;
            }
        }

        return $map;
    }

    public function createDeltaPriceMap(Collection $itemOptionClassifications): array
    {
        $map = [];

        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($itemOptionClassifications->toArray() as $itemOptionClassification) {
            /** @var ItemOptionclassificationOption $componentOption */
            foreach ($itemOptionClassification->getItemOptionclassificationOption() as $componentOption) {
                foreach ($componentOption->getItemOptionclassificationOptionDeltaprice() as $deltaPrice) {
                    $deltaPriceId = $deltaPrice->getId();
                    if (null === $deltaPriceId) {
                        continue;
                    }

                    $map[$deltaPriceId] = $deltaPrice;
                }
            }
        }

        return $map;
    }
}
