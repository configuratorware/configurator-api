<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern;

use Doctrine\ORM\EntityManagerInterface;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConstructionPattern;

class ConstructionPatternApi
{
    /**
     * @var ConstructionPatternStructureGenerator
     */
    private $constructionPatternStructureGenerator;

    /**
     * @var ConstructionPatternSave
     */
    private $constructionPatternSave;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @param ConstructionPatternStructureGenerator $constructionPatternStructureGenerator
     * @param ConstructionPatternSave $constructionPatternSave
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        ConstructionPatternStructureGenerator $constructionPatternStructureGenerator,
        ConstructionPatternSave $constructionPatternSave,
        EntityManagerInterface $entityManager
    ) {
        $this->constructionPatternStructureGenerator = $constructionPatternStructureGenerator;
        $this->constructionPatternSave = $constructionPatternSave;
        $this->entityManager = $entityManager;
    }

    /**
     * @param int $itemId
     *
     * @return ConstructionPattern
     *
     * @throws NotFoundException
     */
    public function getOne(int $itemId): ConstructionPattern
    {
        return $this->constructionPatternStructureGenerator->getConstructionPatternByItemId($itemId);
    }

    /**
     * @param ConstructionPattern $constructionPattern
     *
     * @return ConstructionPattern
     */
    public function save(ConstructionPattern $constructionPattern): ConstructionPattern
    {
        $this->constructionPatternSave->save($constructionPattern);

        // to load afterwards the item from database with correct date deleted clear entity manager
        $this->entityManager->clear();

        return $this->constructionPatternStructureGenerator->getConstructionPatternByItemId($constructionPattern->item->id);
    }
}
