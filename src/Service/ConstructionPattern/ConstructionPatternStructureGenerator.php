<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\AdminAreaItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassification\ItemOptionClassificationSorter;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassificationOption\ItemOptionClassificationOptionSorter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConstructionPattern;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableComponent;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOption;
use Redhotmagma\ConfiguratorApiBundle\Structure\SelectableOptionDeltaPrice;

/**
 * @internal
 */
class ConstructionPatternStructureGenerator
{
    private ItemRepository $itemRepository;

    private AdminAreaItemStructureFromEntityConverter $itemStructureFromEntityConverter;

    private ItemOptionClassificationSorter $itemOptionClassificationSorter;

    private ItemOptionClassificationOptionSorter $itemOptionClassificationOptionSorter;

    private OptionThumbnailRepositoryInterface $optionImageRepository;

    public function __construct(
        ItemRepository $itemRepository,
        AdminAreaItemStructureFromEntityConverter $itemStructureFromEntityConverter,
        ItemOptionClassificationSorter $itemOptionClassificationSorter,
        ItemOptionClassificationOptionSorter $itemOptionClassificationOptionSorter,
        OptionThumbnailRepositoryInterface $optionImageRepository
    ) {
        $this->itemRepository = $itemRepository;
        $this->itemStructureFromEntityConverter = $itemStructureFromEntityConverter;
        $this->itemOptionClassificationSorter = $itemOptionClassificationSorter;
        $this->itemOptionClassificationOptionSorter = $itemOptionClassificationOptionSorter;
        $this->optionImageRepository = $optionImageRepository;
    }

    public function getConstructionPatternByItemId(int $itemId): ConstructionPattern
    {
        $itemEntity = $this->itemRepository->find($itemId);

        if (empty($itemEntity)) {
            throw new NotFoundException();
        }

        $itemStructure = $this->itemStructureFromEntityConverter->convertOne($itemEntity, Item::class);

        return $this->getConstructionPatternForItem($itemStructure);
    }

    protected function getConstructionPatternForItem(Item $itemStructure): ConstructionPattern
    {
        $constructionPattern = new ConstructionPattern();

        $constructionPattern->item = $itemStructure;

        $itemEntity = $this->itemRepository->find($itemStructure->id);

        /** @var ItemOptionclassification[] $itemOptionClassificationEntities */
        $itemOptionClassificationEntities = $itemEntity->getItemOptionclassification()->toArray();
        $this->itemOptionClassificationSorter->sortAsc($itemOptionClassificationEntities);

        $optionThumbnailUrlMap = $this->findOptionThumbnailUrlsMapped($itemOptionClassificationEntities);

        foreach ($itemOptionClassificationEntities as $itemOptionClassification) {
            $optionClassification = $itemOptionClassification->getOptionclassification();

            $selectableOptions = [];
            $itemOptionClassificationOptionEntities = $itemOptionClassification->getItemOptionclassificationOption()->toArray();
            $this->itemOptionClassificationOptionSorter->sortAsc($itemOptionClassificationOptionEntities);
            /** @var ItemOptionclassificationOption $itemOptionClassificationOption */
            foreach ($itemOptionClassificationOptionEntities as $itemOptionClassificationOption) {
                $selectableOptionDeltaPrices = [];
                foreach ($itemOptionClassificationOption->getItemOptionclassificationOptionDeltaprice() as $deltaPrice) {
                    $channel = $deltaPrice->getChannel();
                    $selectableOptionDeltaPrices[] = new SelectableOptionDeltaPrice($deltaPrice->getId(), $deltaPrice->getPrice(), $deltaPrice->getPriceNet(), $channel ? (int)$channel->getId() : null);
                }

                $option = $itemOptionClassificationOption->getOption();
                $thumbnail = $optionThumbnailUrlMap[$option->getIdentifier()] ?? null;
                $selectableOptions[] = new SelectableOption((int)$option->getId(), $option->getIdentifier(), $itemOptionClassificationOption->getSequenceNumber(), $option->getTranslatedTitle(), $itemOptionClassificationOption->getAmountisselectable(), $thumbnail, $selectableOptionDeltaPrices);
            }

            $creatorViewId = null;
            if (null !== $itemOptionClassification->getCreatorView()) {
                /** @psalm-suppress PossiblyNullReference */
                $creatorViewId = $itemOptionClassification->getCreatorView()->getId();
            }

            $constructionPattern->selectableComponents[] = new SelectableComponent(
                (int)$optionClassification->getId(),
                $optionClassification->getIdentifier(),
                $itemOptionClassification->getSequenceNumber(),
                $optionClassification->getTranslatedTitle(),
                $selectableOptions,
                $itemOptionClassification->getIsMandatory(),
                $itemOptionClassification->getIsMultiselect(),
                $creatorViewId
            );
        }

        return $constructionPattern;
    }

    /**
     * @param ItemOptionclassification[] $itemOptionClassificationEntities
     *
     * @return array<string, string|null>
     */
    private function findOptionThumbnailUrlsMapped(array $itemOptionClassificationEntities): array
    {
        $optionEntities = [];
        foreach ($itemOptionClassificationEntities as $itemOptionClassificationEntity) {
            foreach ($itemOptionClassificationEntity->getItemOptionclassificationOption() as $itemOptionclassificationOption) {
                $option = $itemOptionclassificationOption->getOption();
                $optionEntities[$option->getId()] = $option;
            }
        }

        $map = [];
        foreach ($this->optionImageRepository->findThumbnails($optionEntities) as $thumbnail) {
            $map += $thumbnail->toMap();
        }

        return $map;
    }
}
