<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Attribute;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Cache\StructureCache;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributetext;

/**
 * @internal
 */
class AttributeStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var StructureCache
     */
    private $cache;

    /**
     * AttributeStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param StructureCache               $cache
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter, StructureCache $cache)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->cache = $cache;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute $entity
     * @param string                                              $structureClassName
     *
     * @return Attribute
     */
    public function convertOne($entity, $structureClassName = Attribute::class)
    {
        if (null === $this->cache->findStructureForEntity($entity)) {
            /** @var Attribute $structure */
            $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
            $structure = $this->addAttributeTextsToStructure($structure, $entity);

            $this->cache->store($entity, $structure);

            return $structure;
        }

        return $this->cache->findStructureForEntity($entity);
    }

    /**
     * @param array  $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Attribute::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param Attribute                                           $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute $entity
     *
     * @return mixed
     */
    protected function addAttributeTextsToStructure($structure, $entity)
    {
        $texts = [];

        $attributeTexts = $entity->getAttributetext();

        if (!empty($attributeTexts)) {
            foreach ($attributeTexts as $attributeText) {
                $attributeTextStructure = $this->structureFromEntityConverter->convertOne($attributeText,
                    Attributetext::class);

                if (!empty($attributeTextStructure)) {
                    $attributeTextStructure->language = $attributeText->getLanguage()
                        ->getIso();
                    $texts[] = $attributeTextStructure;
                }
            }
        }

        $structure->texts = $texts;

        return $structure;
    }
}
