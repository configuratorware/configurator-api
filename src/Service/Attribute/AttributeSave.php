<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Attribute;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;

/**
 * @internal
 */
class AttributeSave
{
    private AttributeRepository $attributeRepository;

    private AttributeEntityFromStructureConverter $attributeEntityFromStructureConverter;

    private AttributeStructureFromEntityConverter $attributeStructureFromEntityConverter;

    public function __construct(
        AttributeRepository $optionPoolRepository,
        AttributeEntityFromStructureConverter $optionPoolEntityFromStructureConverter,
        AttributeStructureFromEntityConverter $optionPoolStructureFromEntityConverter
    ) {
        $this->attributeRepository = $optionPoolRepository;
        $this->attributeEntityFromStructureConverter = $optionPoolEntityFromStructureConverter;
        $this->attributeStructureFromEntityConverter = $optionPoolStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Attribute $attribute
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Attribute
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Attribute $attribute)
    {
        $entity = null;
        if (isset($attribute->id) && $attribute->id > 0) {
            $entity = $this->attributeRepository->findOneBy(['id' => $attribute->id]);
        }
        $entity = $this->attributeEntityFromStructureConverter->convertOne($attribute, $entity);
        $entity = $this->attributeRepository->save($entity);

        /** @var Attribute $entity */
        $entity = $this->attributeRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->attributeStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
