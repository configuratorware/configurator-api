<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Attribute;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemattributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionAttributeRepository;

/**
 * @internal
 */
class AttributeDelete
{
    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * @var ItemattributeRepository
     */
    private $itemAttributeRepository;

    /**
     * @var OptionAttributeRepository
     */
    private $optionAttributeRepository;

    /**
     * @param AttributeRepository $attributeRepository
     * @param ItemattributeRepository $itemAttributeRepository
     * @param OptionAttributeRepository $optionAttributeRepository
     */
    public function __construct(
        AttributeRepository $attributeRepository,
        ItemattributeRepository $itemAttributeRepository,
        OptionAttributeRepository $optionAttributeRepository
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->itemAttributeRepository = $itemAttributeRepository;
        $this->optionAttributeRepository = $optionAttributeRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Attribute $entity */
            $entity = $this->attributeRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            if (!empty($entity->getAttributetext()->toArray())) {
                foreach ($entity->getAttributetext() as $attributeText) {
                    $this->attributeRepository->delete($attributeText);
                }
            }

            $itemAttributes = $this->itemAttributeRepository->findBy(['attribute' => $entity]);
            if (!empty($itemAttributes)) {
                foreach ($itemAttributes as $itemAttribute) {
                    $this->itemAttributeRepository->delete($itemAttribute);
                }
            }

            $optionAttributes = $this->optionAttributeRepository->findBy(['attribute' => $entity]);
            if (!empty($optionAttributes)) {
                foreach ($optionAttributes as $optionAttribute) {
                    $this->optionAttributeRepository->delete($optionAttribute);
                }
            }

            $this->attributeRepository->delete($entity);
        }
    }
}
