<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Attribute;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attribute;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class AttributeApi
{
    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * @var AttributeStructureFromEntityConverter
     */
    private $attributeStructureFromEntityConverter;

    /**
     * @var AttributeSave
     */
    private $attributeSave;

    /**
     * @var AttributeDelete
     */
    private $attributeDelete;

    /**
     * AttributeApi constructor.
     *
     * @param AttributeRepository $attributeRepository
     * @param AttributeStructureFromEntityConverter $attributeStructureFromEntityConverter
     * @param AttributeSave $attributeSave
     * @param AttributeDelete $attributeDelete
     */
    public function __construct(
        AttributeRepository $attributeRepository,
        AttributeStructureFromEntityConverter $attributeStructureFromEntityConverter,
        AttributeSave $attributeSave,
        AttributeDelete $attributeDelete
    ) {
        $this->attributeRepository = $attributeRepository;
        $this->attributeStructureFromEntityConverter = $attributeStructureFromEntityConverter;
        $this->attributeSave = $attributeSave;
        $this->attributeDelete = $attributeDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->attributeRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->attributeStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->attributeRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Attribute
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Attribute
    {
        /** @var Attribute $entity */
        $entity = $this->attributeRepository->findOneBy(['id' => $id]);

        if (null === $entity) {
            throw new NotFoundException();
        }

        $structure = $this->attributeStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Attribute $attribute
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Attribute
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Attribute $attribute)
    {
        $structure = $this->attributeSave->save($attribute);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->attributeDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier', 'attributetext.title'];
    }
}
