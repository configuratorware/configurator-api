<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Attribute;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attribute;

/**
 * @internal
 */
class AttributeEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * AttributeEntityFromStructureConverter constructor.
     *
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(EntityFromStructureConverter $entityFromStructureConverter)
    {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param Attribute $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Attribute $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($localStructure, $entity, 'texts',
            'Attributetext');
        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations($localStructure, $entity, 'texts',
            'Attributetext');

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Attribute::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
