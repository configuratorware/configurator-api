<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalItemPrice;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationTypeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPrice;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPriceItem;

/**
 * @internal
 */
class DesignerGlobalItemPriceItemStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var DesignerGlobalCalculationTypeRepository
     */
    private $designerGlobalCalculationTypeRepository;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * DesignerGlobalItemPriceItemStructureFromEntityConverter constructor.
     *
     * @param DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->designerGlobalCalculationTypeRepository = $designerGlobalCalculationTypeRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Item $entity
     * @param string $structureClassName
     *
     * @return mixed|DesignerGlobalItemPriceItem
     */
    public function convertOne($entity, $structureClassName = DesignerGlobalItemPriceItem::class)
    {
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        if (!empty($structure)) {
            $structure = $this->addCalculationTypesToStructure($structure, $entity);
        }

        return $structure;
    }

    /**
     * @param $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = DesignerGlobalItemPriceItem::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    protected function addCalculationTypesToStructure(DesignerGlobalItemPriceItem $structure, Item $entity)
    {
        $calculationTypeRelations = [];

        $calculationTypes = $this->designerGlobalCalculationTypeRepository->findBy(['price_per_item' => true]);

        /** @var DesignerGlobalCalculationType $calculationType */
        foreach ($calculationTypes as $calculationType) {
            /** @var DesignerGlobalCalculationTypeRelation $calculationTypeRelation */
            $calculationTypeRelation = $this->structureFromEntityConverter->convertOne($calculationType,
                DesignerGlobalCalculationTypeRelation::class);
            $calculationTypeRelation->title = $calculationType->getTranslatedTitle();

            // get prices for the current calculation type
            $prices = $entity->getDesignerGlobalItemPrice()->filter(
                function ($entity) use ($calculationType) {
                    /* @var \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice $entity */
                    return $entity->getDesignerGlobalCalculationType()->getId() == $calculationType->getId();
                }
            );

            $calculationTypeRelation->prices = [];
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalItemPrice $price */
            foreach ($prices as $price) {
                $priceStructure = $this->structureFromEntityConverter->convertOne($price,
                    DesignerGlobalItemPrice::class);

                $priceStructure->channel = $price->getChannel()->getId();

                $calculationTypeRelation->prices[] = $priceStructure;
            }

            $calculationTypeRelations[] = $calculationTypeRelation;
        }

        $structure->calculationTypes = $calculationTypeRelations;

        return $structure;
    }
}
