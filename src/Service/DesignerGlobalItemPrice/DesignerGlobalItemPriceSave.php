<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalItemPrice;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalItemPriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationTypeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPriceItem;

/**
 * @internal
 */
class DesignerGlobalItemPriceSave
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var DesignerGlobalCalculationTypeRepository
     */
    private $designerGlobalCalculationTypeRepository;

    /**
     * @var DesignerGlobalItemPriceItemStructureFromEntityConverter
     */
    private $designerGlobalItemPriceItemStructureFromEntityConverter;

    /**
     * @var DesignerGlobalItemPriceRepository
     */
    private $designerGlobalItemPriceRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * DesignerGlobalItemPriceSave constructor.
     *
     * @param ChannelRepository $channelRepository
     * @param DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
     * @param DesignerGlobalItemPriceItemStructureFromEntityConverter $designerGlobalItemPriceItemStructureFromEntityConverter
     * @param DesignerGlobalItemPriceRepository $designerGlobalItemPriceRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        ChannelRepository $channelRepository,
        DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository,
        DesignerGlobalItemPriceItemStructureFromEntityConverter $designerGlobalItemPriceItemStructureFromEntityConverter,
        DesignerGlobalItemPriceRepository $designerGlobalItemPriceRepository,
        EntityFromStructureConverter $entityFromStructureConverter,
        ItemRepository $itemRepository
    ) {
        $this->channelRepository = $channelRepository;
        $this->designerGlobalCalculationTypeRepository = $designerGlobalCalculationTypeRepository;
        $this->designerGlobalItemPriceItemStructureFromEntityConverter = $designerGlobalItemPriceItemStructureFromEntityConverter;
        $this->designerGlobalItemPriceRepository = $designerGlobalItemPriceRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param DesignerGlobalItemPriceItem $structure
     *
     * @return DesignerGlobalItemPriceItem
     */
    public function save(DesignerGlobalItemPriceItem $structure): DesignerGlobalItemPriceItem
    {
        $entity = null;

        /* @var Item $entity */
        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->itemRepository->findOneBy(['id' => $structure->id]);
        }

        $this->savePrices($structure, $entity);

        $this->itemRepository->clear();
        $entity = $this->itemRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->designerGlobalItemPriceItemStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * collect prices from calculation types and update them in the database.
     *
     * @param DesignerGlobalItemPriceItem $structure
     * @param Item $entity
     */
    protected function savePrices(DesignerGlobalItemPriceItem $structure, Item $entity)
    {
        $priceStructures = [];
        /** @var DesignerGlobalCalculationTypeRelation $calculationType */
        foreach ($structure->calculationTypes as $calculationType) {
            foreach ($calculationType->prices as $price) {
                // add calculation type entity to the price structure to save the relation correctly later
                $price->designerGlobalCalculationType = $this->designerGlobalCalculationTypeRepository->findOneBy(['id' => $calculationType->id]);
                $price->_metadata->changedProperties[] = 'designerGlobalCalculationType';

                // add channel entity to the price structure to save the relation correctly later
                $price->channel = $this->channelRepository->findOneBy(['id' => $price->channel]);

                $priceStructures[] = $price;
            }
        }

        // create a helper class to be able to update all global calculation item prices at once
        $priceUpdateHelper = new \stdClass();
        $priceUpdateHelper->prices = $priceStructures;
        $this->entityFromStructureConverter->setManyToOneRelationsDeleted($priceUpdateHelper, $entity, 'prices',
            'DesignerGlobalItemPrice');

        $this->entityFromStructureConverter->addNewManyToOneRelations($priceUpdateHelper, $entity, 'prices',
            'DesignerGlobalItemPrice');

        foreach ($entity->getDesignerGlobalItemPrice()->toArray() as $i => $designerGlobalItemPrice) {
            $this->designerGlobalItemPriceRepository->save($designerGlobalItemPrice);
        }
    }
}
