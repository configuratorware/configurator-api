<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalItemPrice;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalItemPriceItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class DesignerGlobalItemPriceApi
{
    /**
     * @var DesignerGlobalItemPriceItemStructureFromEntityConverter
     */
    private $designerGlobalItemPriceItemStructureFromEntityConverter;

    /**
     * @var DesignerGlobalItemPriceSave
     */
    private $designerGlobalItemPriceSave;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * DesignerGlobalItemPriceApi constructor.
     *
     * @param DesignerGlobalItemPriceItemStructureFromEntityConverter $designerGlobalItemPriceItemStructureFromEntityConverter
     * @param DesignerGlobalItemPriceSave $designerGlobalItemPriceSave
     * @param ItemRepository $itemRepository
     */
    public function __construct(
        DesignerGlobalItemPriceItemStructureFromEntityConverter $designerGlobalItemPriceItemStructureFromEntityConverter,
        DesignerGlobalItemPriceSave $designerGlobalItemPriceSave,
        ItemRepository $itemRepository
    ) {
        $this->designerGlobalItemPriceItemStructureFromEntityConverter = $designerGlobalItemPriceItemStructureFromEntityConverter;
        $this->designerGlobalItemPriceSave = $designerGlobalItemPriceSave;
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(
        ListRequestArguments $arguments
    ): PaginationResult {
        $paginationResult = new PaginationResult();

        $entities = $this->itemRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult->data = $this->designerGlobalItemPriceItemStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->itemRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return DesignerGlobalItemPriceItem
     *
     * @throws NotFoundException
     */
    public function getOne(
        int $id
    ): DesignerGlobalItemPriceItem {
        /** @var Item $entity */
        $entity = $this->itemRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->designerGlobalItemPriceItemStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param DesignerGlobalItemPriceItem $structure
     *
     * @return DesignerGlobalItemPriceItem
     */
    public function save(
        DesignerGlobalItemPriceItem $structure
    ): DesignerGlobalItemPriceItem {
        $structure = $this->designerGlobalItemPriceSave->save($structure);

        return $structure;
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['identifier', 'itemtext.title'];
    }
}
