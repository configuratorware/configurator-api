<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\FrontendTranslationFile;

use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\CacheClear\CacheClearCommandRunner;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\TranslationsPaths;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FrontendTranslationFileUploadArguments;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

class FrontendTranslationFileApi
{
    private TranslationsPaths $translationsPaths;

    private CachedFrontendTranslationFileCreator $translationFileGenerator;

    private CacheClearCommandRunner $cacheClearCommandRunner;

    private SettingRepository $settingsRepository;

    public function __construct(TranslationsPaths $translationsPaths, CachedFrontendTranslationFileCreator $translationFileGenerator, SettingRepository $settingsRepository, CacheClearCommandRunner $cacheClearCommandRunner)
    {
        $this->translationsPaths = $translationsPaths;
        $this->translationFileGenerator = $translationFileGenerator;
        $this->settingsRepository = $settingsRepository;
        $this->cacheClearCommandRunner = $cacheClearCommandRunner;
    }

    public function download(string $iso): FileResult
    {
        $filePath = $this->translationsPaths->getCachedIsoFilePath($iso);
        if (!file_exists($filePath)) {
            throw new FileNotFoundException($filePath);
        }

        return new FileResult($filePath, $iso . '.json', 'application/json');
    }

    public function save(string $iso, FrontendTranslationFileUploadArguments $translationUploadArguments): void
    {
        $this->translationFileGenerator->writeUserTranslationDiff($translationUploadArguments->uploadedFile->getContent(), $iso);
        $this->translationFileGenerator->createCachedTranslationFiles();
        $setting = $this->settingsRepository->getSetting();
        if ($setting && true === $setting->getClearCacheOnTranslationsUpload()) {
            $this->cacheClearCommandRunner->clearCache();
        }
    }
}
