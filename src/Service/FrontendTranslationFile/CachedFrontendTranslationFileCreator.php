<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\FrontendTranslationFile;

use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\TranslationsPaths;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class CachedFrontendTranslationFileCreator
{
    private TranslationsPaths $translationsPaths;

    private LanguageRepository $languageRepository;

    private LoggerInterface $logger;

    public function __construct(TranslationsPaths $translationsPaths, LanguageRepository $languageRepository, LoggerInterface $logger)
    {
        $this->translationsPaths = $translationsPaths;
        $this->languageRepository = $languageRepository;
        $this->logger = $logger;
    }

    /**
     * Merge base, custom and user translation files into cached translation.
     */
    public function createCachedTranslationFiles(): void
    {
        $fileSystem = new Filesystem();
        $langCodes = $this->getAvailableLanguageCodes();

        // create dirs on first run
        $fileSystem->mkdir([$this->translationsPaths->getCachedPath(), $this->translationsPaths->getUserPath()]);

        // process available lang codes
        foreach ($langCodes as $iso) {
            // search for base translation files
            $translations = $this->decodeTranslationsFile($this->translationsPaths->getBaseIsoFilePath($iso));

            // search for base translation files on public folder and merge them
            $baseTranslations = $this->decodeTranslationsFile($this->translationsPaths->getPublicIsoFilePath($iso));
            $withBaseTranslations = array_replace_recursive($translations, $baseTranslations);

            // search for custom translation file counterpart for base files and merge them
            $customTranslations = $this->decodeTranslationsFile($this->translationsPaths->getCustomIsoFilePath($iso));
            $withCustomTranslations = array_replace_recursive($withBaseTranslations, $customTranslations);

            // merge translations changed by admin users
            $userTranslations = $this->decodeTranslationsFile($this->translationsPaths->getUserIsoFilePath($iso));
            $withUserTranslations = array_replace_recursive($withCustomTranslations, $userTranslations);

            // write cached translation file
            $cachedFilePath = $this->translationsPaths->getCachedIsoFilePath($iso);

            try {
                $fileSystem->dumpFile($cachedFilePath, json_encode($withUserTranslations, JSON_THROW_ON_ERROR));
            } catch (\JsonException $e) {
                $this->logger->error('Error encoding translation file', ['exception' => $e]);
            }
        }
    }

    public function writeUserTranslationDiff(string $uploadedTranslations, string $iso): void
    {
        $cachedTranslations = $this->decodeTranslationsFile($this->translationsPaths->getCachedIsoFilePath($iso));

        // remove BOM
        $cleanedUpload = str_replace("\xEF\xBB\xBF", '', $uploadedTranslations);
        // get translations changed by user in this upload
        $uploadedTranslationsContent = json_decode($cleanedUpload, true, 512, JSON_THROW_ON_ERROR);
        $withoutCachedTranslations = $this->arrayDiffAssocRecursive($uploadedTranslationsContent, $cachedTranslations);

        // merge with existing user changes from previous uploads
        $userFilePath = $this->translationsPaths->getUserIsoFilePath($iso);
        $existingUserTranslations = $this->decodeTranslationsFile($userFilePath);
        $mergedUserTranslations = array_replace_recursive($existingUserTranslations, $withoutCachedTranslations);

        try {
            $fileSystem = new Filesystem();
            $fileSystem->dumpFile($userFilePath, json_encode($mergedUserTranslations, JSON_THROW_ON_ERROR));
        } catch (\JsonException $e) {
            $this->logger->error('Error encoding translation file', ['exception' => $e]);
        }
    }

    private function arrayDiffAssocRecursive(array $array1, array $array2): array
    {
        $difference = [];
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key]) || !is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = $this->arrayDiffAssocRecursive($value, $array2[$key]);
                    if (!empty($new_diff)) {
                        $difference[$key] = $new_diff;
                    }
                }
            } elseif (!array_key_exists($key, $array2) || $array2[$key] !== $value) {
                $difference[$key] = $value;
            }
        }

        return $difference;
    }

    private function decodeTranslationsFile(string $filename): array
    {
        $fileSystem = new Filesystem();
        if (!$fileSystem->exists($filename)) {
            return [];
        }

        try {
            return json_decode(file_get_contents($filename), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            $this->logger->error('Error decoding translation file: ' . $filename);

            return [];
        }
    }

    /**
     * Get available languages' ISO codes from the system.
     *
     * @return string[]
     */
    private function getAvailableLanguageCodes(): array
    {
        $isos = [];
        $languages = $this->languageRepository->findAll();
        foreach ($languages as $lang) {
            $isos[] = $lang->getIso();
        }

        return $isos;
    }
}
