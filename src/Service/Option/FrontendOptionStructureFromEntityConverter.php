<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Events\Option\FrontendOptionStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class FrontendOptionStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * FrontendOptionStructureFromEntityConverter constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     * @param string $structureClassName
     * @param string $baseItemIdentifier
     * @param string $optionClassificationIdentifier
     *
     * @return Option
     */
    public function convertOne(
        $entity,
        $structureClassName = Option::class,
        $baseItemIdentifier = '',
        $optionClassificationIdentifier = ''
    ) {
        $event = new FrontendOptionStructureFromEntityEvent(
            $entity,
            $structureClassName,
            $baseItemIdentifier,
            $optionClassificationIdentifier
        );
        $this->eventDispatcher->dispatch($event, FrontendOptionStructureFromEntityEvent::NAME);
        $structure = $event->getOptionStructure();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     * @param string $baseItemIdentifier
     * @param string $optionClassificationIdentifier
     *
     * @return array
     */
    public function convertMany(
        $entities,
        $structureClassName = Option::class,
        $baseItemIdentifier = null,
        $optionClassificationIdentifier = null
    ) {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne(
                $entity,
                $structureClassName,
                $baseItemIdentifier,
                $optionClassificationIdentifier
            );
            $structures[] = $structure;
        }

        return $structures;
    }
}
