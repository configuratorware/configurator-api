<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionAttribute;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;

/**
 * @internal
 */
class OptionEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var AttributeRelationEntityFromStructureConverter
     */
    private $attributeRelationEntityFromStructureConverter;

    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * OptionEntityFromStructureConverter constructor.
     *
     * @param AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter
     * @param ChannelRepository $channelRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter,
        ChannelRepository $channelRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->attributeRelationEntityFromStructureConverter = $attributeRelationEntityFromStructureConverter;
        $this->channelRepository = $channelRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param Option $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Option
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Option::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->updateOptionTexts($entity, $localStructure);
        $entity = $this->updateOptionPrices($entity, $localStructure);
        $entity = $this->updateOptionStock($entity, $localStructure);
        $entity = $this->updateOptionAttributes($entity, $localStructure);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Option::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option
     * @param Option $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Option
     */
    protected function updateOptionTexts($option, $structure)
    {
        $option = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $option, 'texts',
            'OptionText');
        $option = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $option, 'texts',
            'OptionText');

        return $option;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option
     * @param Option $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Option
     */
    protected function updateOptionPrices($option, $structure)
    {
        if (!empty($structure->prices)) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Structure\OptionPrice $priceStructure */
            foreach ($structure->prices as $i => $priceStructure) {
                if (!empty($priceStructure->channel)) {
                    $structure->prices[$i]->channel = $this->channelRepository->findOneBy(['id' => $priceStructure->channel]);
                }

                // amountfrom has  to be a number bigger than 0
                if (empty((int)$priceStructure->amountfrom) || (int)$priceStructure->amountfrom < 1) {
                    $priceStructure->amountfrom = 1;
                }
            }
        }

        $option = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $option, 'prices',
            'OptionPrice');
        $option = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $option, 'prices',
            'OptionPrice');

        return $option;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option
     * @param Option $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Option
     */
    protected function updateOptionStock($option, $structure)
    {
        if (!empty($structure->stock)) {
            foreach ($structure->stock as $i => $stockStructure) {
                if (!empty($stockStructure->channel)) {
                    $structure->stock[$i]->channel = $this->channelRepository->findOneBy(['id' => $stockStructure->channel]);
                }
            }
        }

        $option = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $option, 'stock',
            'OptionStock');
        $option = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $option, 'stock',
            'OptionStock');

        return $option;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option
     * @param Option $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Option
     */
    protected function updateOptionAttributes($option, $structure)
    {
        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option */
        $option = $this->attributeRelationEntityFromStructureConverter
            ->setManyToManyRelationsDeleted(
                $structure,
                $option,
                'OptionAttribute',
                'attributes',
                'Attribute'
            );

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Option $option */
        $option = $this->attributeRelationEntityFromStructureConverter
            ->addNewManyToManyRelations(
                $structure,
                $option,
                OptionAttribute::class,
                'attributes'
            );

        return $option;
    }
}
