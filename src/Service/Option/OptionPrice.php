<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemOptionclassificationOptionDeltapriceRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\VatCalculationService;

/**
 * @internal
 */
class OptionPrice
{
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @var ItemOptionclassificationOptionDeltapriceRepository
     */
    private $deltaPriceRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var VatCalculationService
     */
    private $vatCalculationService;

    /**
     * OptionPrice constructor.
     *
     * @param SettingRepository $settingRepository
     * @param ItemOptionclassificationOptionDeltapriceRepository $deltaPriceRepository
     * @param OptionRepository $optionRepository
     * @param VatCalculationService $vatCalculationService
     */
    public function __construct(
        SettingRepository $settingRepository,
        ItemOptionclassificationOptionDeltapriceRepository $deltaPriceRepository,
        OptionRepository $optionRepository,
        VatCalculationService $vatCalculationService
    ) {
        $this->settingRepository = $settingRepository;
        $this->deltaPriceRepository = $deltaPriceRepository;
        $this->optionRepository = $optionRepository;
        $this->vatCalculationService = $vatCalculationService;
    }

    public function getOptionPrice(
        string $optionIdentifier,
        ?string $optionClassificationIdentifier = null,
        ?string $baseItemIdentifier = null
    ): ?float {
        $calculationMethod = $this->settingRepository->getCalculationMethod();

        $optionPrices = [];
        $optionPrices['price'] = 0;
        $optionPrices['netPrice'] = 0;

        if (SettingRepository::DELTA_PRICES === $calculationMethod) {
            $deltaPrice = $this->deltaPriceRepository->getDeltaPriceByIdentifiers($baseItemIdentifier,
                $optionClassificationIdentifier,
                $optionIdentifier, C_CHANNEL);

            if (!empty($deltaPrice)) {
                $optionPrices['price'] = $deltaPrice['price'];
                $optionPrices['netPrice'] = $deltaPrice['netPrice'];
            }
        } elseif (SettingRepository::SUM_OF_ALL === $calculationMethod) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Option $optionEntity */
            $optionEntity = $this->optionRepository->findOneBy(['identifier' => $optionIdentifier]);

            // add first price from entity to structure, default to 0
            if ($optionEntity->getCurrentChannelPrice()) {
                $optionPrices['price'] = $optionEntity->getCurrentChannelPrice()->getPrice();
                $optionPrices['netPrice'] = $optionEntity->getCurrentChannelPrice()->getPriceNet();
            }
        }

        $vatPrices = $this->vatCalculationService->getPrices($optionPrices['price'], $optionPrices['netPrice']);

        return CALCULATION_PRICES_NET === true ? $vatPrices->getPriceNet() : $vatPrices->getPrice();
    }
}
