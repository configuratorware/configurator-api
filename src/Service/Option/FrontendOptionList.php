<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationSwitchOption;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\RulesRetriever;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

/**
 * @internal
 */
class FrontendOptionList
{
    private ConfigurationSwitchOption $configurationSwitchOption;

    private RulesRetriever $rulesRetriever;

    public function __construct(ConfigurationSwitchOption $configurationSwitchOption, RulesRetriever $rulesRetriever)
    {
        $this->configurationSwitchOption = $configurationSwitchOption;
        $this->rulesRetriever = $rulesRetriever;
    }

    /**
     * @param Option[] $options
     *
     * @return Option[]
     */
    public function runChecksForOptionsList(array $options, Configuration $configuration, string $optionClassificationIdentifier): array
    {
        if (!$this->rulesRetriever->hasRulesFor($configuration)) {
            return $options;
        }

        $configurationClone = serialize($configuration);
        foreach ($options as $option) {
            // try to switch every option in list
            $switchOption = new \stdClass();
            $switchOption->identifier = $option->identifier;
            $switchOption->amount = 1;
            $switchOptions = [$optionClassificationIdentifier => $switchOption];

            $configurationOut = $this->configurationSwitchOption->switchOption(unserialize($configurationClone),
                $switchOptions, false);

            $option->check_results = $configurationOut->check_results;
        }

        return $options;
    }
}
