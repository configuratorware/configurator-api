<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;

/**
 * @internal
 */
class OptionPriceCheckResult
{
    /**
     * @var OptionPrice
     */
    private $optionPrice;

    /**
     * @var FormatterService
     */
    private $formatterService;

    /**
     * OptionPriceCheckResult constructor.
     *
     * @param OptionPrice $optionPrice
     * @param FormatterService $formatterService
     */
    public function __construct(OptionPrice $optionPrice, FormatterService $formatterService)
    {
        $this->optionPrice = $optionPrice;
        $this->formatterService = $formatterService;
    }

    public function addOptionPricesToCheckResults(
        $configuration
    ) {
        // cache option prices
        $optionPrices = [];

        $baseItemIdentifier = $configuration->item->identifier;

        if (false === $configuration->check_results->status) {
            $checkResults = $configuration->check_results;

            foreach ($checkResults->check_results as $checkResult) {
                // checked option
                if (isset($checkResult->checked_option) && is_object($checkResult->checked_option)) {
                    $optionIdentifier = $checkResult->checked_option->identifier;
                    $optionClassificationIdentifier = $checkResult->checked_option->optionclassification_identifiers[0] ?? null;

                    if (isset($optionPrices[$optionClassificationIdentifier][$optionIdentifier])) {
                        $price = $optionPrices[$optionClassificationIdentifier][$optionIdentifier];
                    } else {
                        $price = $this->optionPrice->getOptionPrice($optionIdentifier, $optionClassificationIdentifier,
                            $baseItemIdentifier);
                        $optionPrices[$optionClassificationIdentifier][$optionIdentifier] = $price;
                    }

                    $checkResult->checked_option->price = $price;
                    $checkResult->checked_option->priceFormatted = $this->formatterService->formatPrice((float)$price);
                }

                // conflicting options
                if (isset($checkResult->conflicting_options) && !empty($checkResult->conflicting_options)) {
                    foreach ($checkResult->conflicting_options as $conflictingOption) {
                        $optionIdentifier = $conflictingOption->identifier;
                        $optionClassificationIdentifier = $conflictingOption->optionclassification_identifiers[0] ?? null;

                        if (isset($optionPrices[$optionClassificationIdentifier][$optionIdentifier])) {
                            $price = $optionPrices[$optionClassificationIdentifier][$optionIdentifier];
                        } else {
                            $price = $this->optionPrice->getOptionPrice($optionIdentifier,
                                $optionClassificationIdentifier,
                                $baseItemIdentifier);
                            $optionPrices[$optionClassificationIdentifier][$optionIdentifier] = $price;
                        }

                        $conflictingOption->price = $price;
                        $conflictingOption->priceFormatted = $this->formatterService->formatPrice((float)$price);
                    }
                }

                // switchOptions
                if (isset($checkResult->data['switchOptions'])) {
                    foreach ($checkResult->data['switchOptions'] as $optionClassificationIdentifier => $switchOptions) {
                        foreach ($switchOptions as $i => $switchOption) {
                            $optionIdentifier = $switchOption['identifier'];

                            if (isset($optionPrices[$optionClassificationIdentifier][$optionIdentifier])) {
                                $price = $optionPrices[$optionClassificationIdentifier][$optionIdentifier];
                            } else {
                                $price = $this->optionPrice->getOptionPrice($optionIdentifier,
                                    $optionClassificationIdentifier,
                                    $baseItemIdentifier);
                                $optionPrices[$optionClassificationIdentifier][$optionIdentifier] = $price;
                            }

                            $checkResult->data['switchOptions'][$optionClassificationIdentifier][$i]['price'] = $price;
                            $checkResult->data['switchOptions'][$optionClassificationIdentifier][$i]['priceFormatted'] = $this->formatterService->formatPrice((float)$price);
                        }
                    }
                }
            }
        }

        return $configuration;
    }
}
