<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputValidation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option as OptionStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadOptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadOptionThumbnail;

class OptionApi
{
    /**
     * @var OptionImage
     */
    private $optionImage;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionStructureFromEntityConverter
     */
    private $optionStructureFromEntityConverter;

    /**
     * @var OptionSave
     */
    private $optionSave;

    /**
     * @var OptionDelete
     */
    private $optionDelete;

    /**
     * @var OptionThumbnailRepositoryInterface
     */
    private $optionThumbnailRepository;

    /**
     * @var OptionInputValidation
     */
    private $optionInputValidation;

    /**
     * @param OptionImage $optionImage
     * @param OptionRepository $optionRepository
     * @param OptionStructureFromEntityConverter $optionStructureFromEntityConverter
     * @param OptionSave $optionSave
     * @param OptionDelete $optionDelete
     * @param OptionThumbnailRepositoryInterface $optionThumbnailRepository
     * @param OptionInputValidation $optionInputValidation
     */
    public function __construct(
        OptionImage $optionImage,
        OptionRepository $optionRepository,
        OptionStructureFromEntityConverter $optionStructureFromEntityConverter,
        OptionSave $optionSave,
        OptionDelete $optionDelete,
        OptionThumbnailRepositoryInterface $optionThumbnailRepository,
        OptionInputValidation $optionInputValidation
    ) {
        $this->optionImage = $optionImage;
        $this->optionRepository = $optionRepository;
        $this->optionStructureFromEntityConverter = $optionStructureFromEntityConverter;
        $this->optionSave = $optionSave;
        $this->optionDelete = $optionDelete;
        $this->optionThumbnailRepository = $optionThumbnailRepository;
        $this->optionInputValidation = $optionInputValidation;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->optionRepository->fetchList($arguments, $this->getSearchableFields());
        $structures = $this->optionStructureFromEntityConverter->convertMany($entities);
        $this->addThumbnailsToOptions($structures, $entities);
        $paginationResult->data = $structures;

        $paginationResult->count = $this->optionRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return OptionStructure
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): OptionStructure
    {
        /** @var Option $entity */
        $entity = $this->optionRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->optionStructureFromEntityConverter->convertOne($entity);
        $this->addThumbnailToOption($structure, $entity);

        return $structure;
    }

    /**
     * @param OptionStructure $option
     *
     * @return OptionStructure
     */
    public function save(OptionStructure $option): OptionStructure
    {
        $structure = $this->optionSave->save($option);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $this->optionDelete->delete($id);
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments): void
    {
        /** @var SequenceNumberData $sequenceNumberData */
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            /** @var Option $entity */
            $entity = $this->optionRepository->findOneBy(['id' => $sequenceNumberData->id]);

            if (is_object($entity)) {
                $entity->setSequencenumber($sequenceNumberData->sequencenumber);
                $this->optionRepository->save($entity, false);
            }
        }

        $this->optionRepository->flush();
    }

    /**
     * @param UploadOptionThumbnail $uploadOptionThumbnail
     */
    public function uploadThumbnail(UploadOptionThumbnail $uploadOptionThumbnail): void
    {
        $this->optionImage->uploadThumbnail($uploadOptionThumbnail);
    }

    /**
     * @param int $id
     */
    public function deleteThumbnail(int $id): void
    {
        $this->optionImage->deleteThumbnail($id);
    }

    /**
     * @param UploadOptionDetailImage $uploadOptionDetailImage
     */
    public function uploadDetailImage(UploadOptionDetailImage $uploadOptionDetailImage): void
    {
        $this->optionImage->uploadDetailImage($uploadOptionDetailImage);
    }

    /**
     * @param int $id
     */
    public function deleteDetailImage(int $id): void
    {
        $this->optionImage->deleteDetailImage($id);
    }

    /**
     * @return PaginationResult
     */
    public function getOptionInputValidationTypes(): PaginationResult
    {
        $validationTypes = $this->optionInputValidation->getValidationTypes();

        return new PaginationResult($validationTypes, count($validationTypes));
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['identifier', 'optionText.title', 'optionText.abstract', 'optionText.description'];
    }

    /**
     * @param array $options
     * @param array $optionEntities
     */
    private function addThumbnailsToOptions(array &$options, array $optionEntities): void
    {
        $optionThumbnails = $this->map($this->optionThumbnailRepository->findThumbnails($optionEntities));
        foreach ($options as &$option) {
            $option->thumbnail = $optionThumbnails[$option->identifier] ?? null;
        }
    }

    /**
     * @param OptionStructure $option
     * @param Option $optionEntity
     */
    private function addThumbnailToOption(OptionStructure $option, Option $optionEntity): void
    {
        try {
            $option->thumbnail = $this->optionThumbnailRepository->findThumbnail($optionEntity)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }
    }

    /**
     * @param OptionThumbnail[] $images
     *
     * @return array
     */
    private function map(array $images): array
    {
        $map = [];
        foreach ($images as $image) {
            $map += $image->toMap();
        }

        return $map;
    }
}
