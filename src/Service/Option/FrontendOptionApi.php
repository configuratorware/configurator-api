<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option as OptionEntity;
use Redhotmagma\ConfiguratorApiBundle\Events\Option\FrontendOptionListBeforeReturnEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\OptionFilterArguments;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class FrontendOptionApi
{
    private EventDispatcherInterface $eventDispatcher;

    private FrontendOptionList $frontendOptionList;

    private FrontendOptionGrouping $frontendOptionGrouping;

    private OptionRepository $optionRepository;

    private OptionDetailImageRepositoryInterface $optionDetailImageRepository;

    private FrontendOptionStructureFromEntityConverter $optionStructureFromEntityConverter;

    private OptionThumbnailRepositoryInterface $optionThumbnailRepository;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        FrontendOptionList $frontendOptionList,
        FrontendOptionGrouping $frontendOptionGrouping,
        OptionRepository $optionRepository,
        OptionDetailImageRepositoryInterface $optionDetailImageRepository,
        FrontendOptionStructureFromEntityConverter $optionStructureFromEntityConverter,
        OptionThumbnailRepositoryInterface $optionThumbnailRepository
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->frontendOptionList = $frontendOptionList;
        $this->frontendOptionGrouping = $frontendOptionGrouping;
        $this->optionRepository = $optionRepository;
        $this->optionDetailImageRepository = $optionDetailImageRepository;
        $this->optionStructureFromEntityConverter = $optionStructureFromEntityConverter;
        $this->optionThumbnailRepository = $optionThumbnailRepository;
    }

    public function getList(
        Configuration $configuration,
        string $itemIdentifier,
        string $optionClassificationIdentifier,
        OptionFilterArguments $optionFilters
    ): array {
        $filters = $optionFilters->filters;
        $queryTitle = $optionFilters->queryTitle;

        /** @var OptionEntity[] $optionEntities */
        $optionEntities = $this->optionRepository->fetchOptionitemsForFrontendlist($itemIdentifier, $optionClassificationIdentifier, false, $filters, $queryTitle);
        $options = $this->optionStructureFromEntityConverter->convertMany($optionEntities, Option::class, $itemIdentifier, $optionClassificationIdentifier);
        $options = $this->frontendOptionList->runChecksForOptionsList($options, $configuration, $optionClassificationIdentifier);

        $this->addImagesToOptions($options, $optionEntities);

        // group options by their itemgroups/itemgroupentries
        $options = $this->frontendOptionGrouping->groupOptionsByItemGroups($options);

        $event = new FrontendOptionListBeforeReturnEvent($options, $configuration, $itemIdentifier, $optionClassificationIdentifier, $filters);
        $this->eventDispatcher->dispatch($event, FrontendOptionListBeforeReturnEvent::NAME);
        $options = $event->getOptions();

        return $options;
    }

    /**
     * @param Configuration $configuration
     * @param $optionIdentifier
     *
     * @return Option|null
     */
    public function getDetail(Configuration $configuration, $optionIdentifier)
    {
        /** @var OptionEntity $optionEntity */
        $optionEntity = $this->optionRepository->findOneBy(['identifier' => $optionIdentifier]);

        $option = null;
        if (null !== $optionEntity) {
            $option = $this->optionStructureFromEntityConverter->convertOne($optionEntity);
            $this->addImagesToOption($option, $optionEntity);
        }

        return $option;
    }

    /**
     * @param Configuration $configuration
     * @param string $optionClassificationIdentifier
     *
     * @return array
     */
    public function getMatchingOptions(Configuration $configuration, $optionClassificationIdentifier)
    {
        // get all options for the base item and the option classification
        /**
         * @var OptionEntity[] $availableOptions
         */
        $availableOptions = $this->optionRepository
            ->fetchOptionitemsForFrontendlist($configuration->item->identifier, $optionClassificationIdentifier);

        $options = $this->optionStructureFromEntityConverter->convertMany($availableOptions, Option::class,
            $configuration->item->identifier, $optionClassificationIdentifier);

        $options = $this->frontendOptionList->runChecksForOptionsList($options, $configuration,
            $optionClassificationIdentifier);

        $options = $this->filterMatchingOptions($options, $configuration);

        return $options;
    }

    /**
     * iterates over the check results and filters all aviailable options
     * to a reduced list of options that can be added to the current configuration.
     *
     * @param array $availableOptions
     * @param Configuration $configuration
     *
     * @return array
     */
    protected function filterMatchingOptions($availableOptions, $configuration)
    {
        $matchingOptions = [];
        foreach ($availableOptions as $k => $availableOption) {
            if (!isset($availableOption->check_results) || true == $availableOption->check_results->status) {
                $matchingOptions[] = $availableOption;
            } else {
                $canBeAdded = true;

                $checkResults = $availableOption->check_results->check_results;
                $checksPerOptionClassification = [];
                foreach ($checkResults as $i => $checkResult) {
                    // also add the options if the problem was an option dependency and there is no conflict in a non empty optionclassification
                    // if there is a conflict to another (not empty) option classification and no other valid check for that option classification
                    if ('optiondependency' == $checkResult->rule_type) {
                        if (true === $checkResult->status) {
                            $currentTargetOptionClassification = $checkResult->checked_option->target_optionclassification_identifiers[0];
                            $checksPerOptionClassification[$currentTargetOptionClassification] = true;
                        } else {
                            // if it conflicts a non empty option classification do not add it
                            if (isset($checkResult->conflicting_options) && isset($checkResult->conflicting_options[0]->optionclassification_identifiers)) {
                                $conflictingOptionClassificationIdentifier = $checkResult->conflicting_options[0]->optionclassification_identifiers[0];
                                foreach ($configuration->optionclassifications as $optionClassification) {
                                    if ($optionClassification->identifier == $conflictingOptionClassificationIdentifier
                                        && !empty($optionClassification->selectedoptions)) {
                                        if (!isset($checksPerOptionClassification[$conflictingOptionClassificationIdentifier])) {
                                            $checksPerOptionClassification[$conflictingOptionClassificationIdentifier] = false;
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        $canBeAdded = false;
                    }
                }

                if (true == $canBeAdded) {
                    // if there are results per optionclassifiaction all of them have to be true
                    if (!empty($checksPerOptionClassification)) {
                        foreach ($checksPerOptionClassification as $checkResult) {
                            if (false === $checkResult) {
                                $canBeAdded = false;
                            }
                        }
                    }

                    if (true == $canBeAdded) {
                        $matchingOptions[] = $availableOption;
                    }
                }
            }
        }

        return $matchingOptions;
    }

    /**
     * @param array $options
     * @param array $optionEntities
     */
    private function addImagesToOptions(array &$options, array $optionEntities): void
    {
        $optionDetailImages = $this->map($this->optionDetailImageRepository->findDetailImages($optionEntities));
        $optionThumbnails = $this->map($this->optionThumbnailRepository->findThumbnails($optionEntities));
        foreach ($options as &$option) {
            $option->detailImage = $optionDetailImages[$option->identifier] ?? null;
            $option->thumbnail = $optionThumbnails[$option->identifier] ?? null;
        }
    }

    /**
     * @param Option $option
     * @param OptionEntity $optionEntity
     */
    private function addImagesToOption(Option $option, OptionEntity $optionEntity): void
    {
        try {
            $option->detailImage = $this->optionDetailImageRepository->findDetailImage($optionEntity)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }

        try {
            $option->thumbnail = $this->optionThumbnailRepository->findThumbnail($optionEntity)->getUrl();
        } catch (FileException $e) {
            // file can be non existent
        }
    }

    /**
     * @param OptionDetailImage[]|OptionThumbnail[] $images
     *
     * @return array
     */
    private function map(array $images): array
    {
        $map = [];
        foreach ($images as $image) {
            $map += $image->toMap();
        }

        return $map;
    }
}
