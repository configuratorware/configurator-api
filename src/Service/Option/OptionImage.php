<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\OptionThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Image\OptionThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadOptionDetailImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadOptionThumbnail;

/**
 * @internal
 */
final class OptionImage
{
    /**
     * @var OptionDetailImageRepositoryInterface
     */
    private $optionDetailImageRepository;

    /**
     * @var OptionThumbnailRepositoryInterface
     */
    private $optionThumbnailRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @param OptionDetailImageRepositoryInterface $optionDetailImageRepository
     * @param OptionThumbnailRepositoryInterface $optionThumbnailRepository
     * @param OptionRepository $optionRepository
     */
    public function __construct(OptionDetailImageRepositoryInterface $optionDetailImageRepository, OptionThumbnailRepositoryInterface $optionThumbnailRepository, OptionRepository $optionRepository)
    {
        $this->optionDetailImageRepository = $optionDetailImageRepository;
        $this->optionThumbnailRepository = $optionThumbnailRepository;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param UploadOptionThumbnail $uploadOptionThumbnail
     */
    public function uploadThumbnail(UploadOptionThumbnail $uploadOptionThumbnail): void
    {
        $option = $this->optionRepository->find($uploadOptionThumbnail->optionId);

        $thumbnail = OptionThumbnail::from(null, $option->getIdentifier(), $uploadOptionThumbnail->realPath, $uploadOptionThumbnail->fileName);

        $this->optionThumbnailRepository->saveThumbnail($thumbnail, $option);
    }

    /**
     * @param int $id
     */
    public function deleteThumbnail(int $id): void
    {
        $option = $this->optionRepository->find($id);

        $thumbnail = $this->optionThumbnailRepository->findThumbnail($option);

        $this->optionThumbnailRepository->deleteThumbnail($thumbnail);
    }

    /**
     * @param UploadOptionDetailImage $uploadOptionDetailImage
     */
    public function uploadDetailImage(UploadOptionDetailImage $uploadOptionDetailImage): void
    {
        $option = $this->optionRepository->find($uploadOptionDetailImage->optionId);

        $detailImage = OptionDetailImage::from(null, $option->getIdentifier(), $uploadOptionDetailImage->realPath, $uploadOptionDetailImage->fileName);

        $this->optionDetailImageRepository->saveDetailImage($detailImage, $option);
    }

    /**
     * @param int $id
     */
    public function deleteDetailImage(int $id): void
    {
        $option = $this->optionRepository->find($id);

        $detailImage = $this->optionDetailImageRepository->findDetailImage($option);

        $this->optionDetailImageRepository->deleteDetailImage($detailImage);
    }
}
