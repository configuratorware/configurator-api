<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Doctrine\Common\Collections\ArrayCollection;
use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;

/**
 * @internal
 */
class OptionSave
{
    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionEntityFromStructureConverter
     */
    private $optionEntityFromStructureConverter;

    /**
     * @var OptionStructureFromEntityConverter
     */
    private $optionStructureFromEntityConverter;

    /**
     * @param OptionRepository $optionPoolRepository
     * @param OptionEntityFromStructureConverter $optionPoolEntityFromStructureConverter
     * @param OptionStructureFromEntityConverter $optionPoolStructureFromEntityConverter
     */
    public function __construct(
        OptionRepository $optionPoolRepository,
        OptionEntityFromStructureConverter $optionPoolEntityFromStructureConverter,
        OptionStructureFromEntityConverter $optionPoolStructureFromEntityConverter
    ) {
        $this->optionRepository = $optionPoolRepository;
        $this->optionEntityFromStructureConverter = $optionPoolEntityFromStructureConverter;
        $this->optionStructureFromEntityConverter = $optionPoolStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Option $option
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Option
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Option $option)
    {
        $entity = null;
        if (isset($option->id) && $option->id > 0) {
            $entity = $this->optionRepository->findOneBy(['id' => $option->id]);
        }
        $entity = $this->optionEntityFromStructureConverter->convertOne($option, $entity);

        $newPrices = [];
        $existingPrices = [];
        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\OptionPrice $price */
        foreach ($entity->getOptionPrice() as $price) {
            if ($price->getId() > 0) {
                $existingPrices[] = $price;
            } else {
                $newPrices[] = $price;
            }
        }
        if (count($newPrices) > 0) {
            $entity->setOptionPrice(new ArrayCollection($existingPrices));
            $entity = $this->optionRepository->save($entity);
            $entity->setOptionPrice(new ArrayCollection($newPrices));
        }

        $entity = $this->optionRepository->save($entity);
        $this->optionRepository->clear();
        /** @var Option $entity */
        $entity = $this->optionRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->optionStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
