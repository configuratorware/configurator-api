<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

class OptionInputNotEmptyValidator implements OptionInputValidatorInterface
{
    private const TYPE = 'optionInputNotEmpty';

    private const VALIDATION_MESSAGE = 'Invalid value. Value must not be empty.';

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function validate(Option $option, ?Configuration $configuration): ?string
    {
        $optionInput = $option->inputText;
        if (null !== $optionInput && strlen($optionInput) > 0) {
            return null;
        }

        return self::VALIDATION_MESSAGE;
    }
}
