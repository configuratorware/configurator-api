<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

interface OptionInputValidatorInterface
{
    public static function getType(): string;

    public function validate(Option $option, ?Configuration $configuration): ?string;
}
