<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation;

use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DTO\TextInputOption;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationComponentValidationError;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationComponentValidationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

class OptionInputValidation
{
    /**
     * @var OptionInputValidatorInterface[]
     */
    private array $validators;

    /**
     * @var OptionRepository
     */
    private OptionRepository $optionRepository;

    public function __construct(OptionRepository $optionRepository)
    {
        $this->optionRepository = $optionRepository;
        $this->validators = [];
    }

    public function addValidator(OptionInputValidatorInterface $validator): void
    {
        $this->validators[$validator->getType()] = $validator;
    }

    /**
     * @return array<string>
     */
    public function getValidationTypes(): array
    {
        return array_keys($this->validators);
    }

    /**
     * @param Configuration $configuration
     *
     * @return array<ConfigurationComponentValidationResult>|null
     */
    public function validateConfiguration(Configuration $configuration): ?array
    {
        $optionIdentifiers = [];
        foreach ($configuration->optionclassifications ?? [] as $component) {
            foreach ($component->selectedoptions ?? [] as $option) {
                $optionIdentifiers[] = $option->identifier;
            }
        }
        $optionsToValidate = $this->groupOptionsByIdentifier($this->optionRepository->findTextInputOptionsByIdentifiers(array_unique($optionIdentifiers)));
        if (empty($optionsToValidate)) {
            return null;
        }

        return $this->doValidation($configuration, $optionsToValidate);
    }

    /**
     * @param array $options
     *
     * @return array<TextInputOption>
     */
    private function groupOptionsByIdentifier(array $options): array
    {
        $groupedOptions = [];
        foreach ($options as $option) {
            $groupedOptions[$option['identifier']] = new TextInputOption($option['identifier'], $option['inputValidationType']);
        }

        return $groupedOptions;
    }

    /**
     * @param Configuration $configuration
     * @param array<TextInputOption> $optionsToValidate
     *
     * @return array<ConfigurationComponentValidationResult>|null
     */
    private function doValidation(Configuration $configuration, array $optionsToValidate): ?array
    {
        $validationResults = [];
        foreach ($configuration->optionclassifications ?? [] as $component) {
            foreach ($component->selectedoptions ?? [] as $option) {
                if (isset($optionsToValidate[$option->identifier])) {
                    $validator = $this->validators[$optionsToValidate[$option->identifier]->inputValidationType];
                    $validationResult = $validator->validate($option, $configuration);
                    if ($validationResult) {
                        $error = new ConfigurationComponentValidationError($optionsToValidate[$option->identifier]->inputValidationType, $validationResult, $option->inputText);
                        if (isset($validationResults[$option->identifier])) {
                            $validationResults[$option->identifier]->errors[] = $error;
                        } else {
                            $validationResults[$option->identifier] = new ConfigurationComponentValidationResult(
                                false, [$error], $component->title, $option->title
                            );
                        }
                    }
                }
            }
        }

        return empty($validationResults) ? null : $validationResults;
    }
}
