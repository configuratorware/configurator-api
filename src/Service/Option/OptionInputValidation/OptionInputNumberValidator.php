<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;

class OptionInputNumberValidator implements OptionInputValidatorInterface
{
    private const TYPE = 'optionInputNumber';

    private const VALIDATION_MESSAGE = 'Invalid value: %s is not a number';

    public static function getType(): string
    {
        return self::TYPE;
    }

    public function validate(Option $option, ?Configuration $configuration): ?string
    {
        $optionInput = $option->inputText;
        if (is_numeric($optionInput)) {
            return null;
        }

        return sprintf(self::VALIDATION_MESSAGE, $optionInput ?? 'null');
    }
}
