<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\AttributeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionPrice;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionText;
use Redhotmagma\ConfiguratorApiBundle\Structure\Stock;

/**
 * @internal
 */
class OptionStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var AttributeRelationStructureFromEntityConverter
     */
    private $attributeRelationStructureFromEntityConverter;

    /**
     * OptionStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     * @param string $structureClassName
     *
     * @return Option
     */
    public function convertOne($entity, $structureClassName = Option::class): Option
    {
        /** @var Option $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addOptionTextsToStructure($structure, $entity);
        $structure = $this->addOptionAttributesToStructure($structure, $entity);
        $structure = $this->addOptionPriceToStructure($structure, $entity);
        $structure = $this->addStockToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Option::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param Option $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     *
     * @return Option
     */
    protected function addOptionTextsToStructure($structure, $entity)
    {
        $texts = [];

        $optionTexts = $entity->getOptiontext();

        if (!empty($optionTexts)) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\OptionText $optionText */
            foreach ($optionTexts as $optionText) {
                $optionTextStructure = $this->structureFromEntityConverter->convertOne($optionText,
                    OptionText::class);

                if (!empty($optionTextStructure)) {
                    $optionTextStructure->language = $optionText->getLanguage()
                        ->getIso();
                    $texts[] = $optionTextStructure;
                }
            }
        }

        $structure->texts = $texts;

        return $structure;
    }

    /**
     * @param Option $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     *
     * @return Option
     */
    protected function addOptionAttributesToStructure($structure, $entity)
    {
        $optionAttributes = $this->attributeRelationStructureFromEntityConverter->convertMany($entity->getOptionAttribute(),
            AttributeRelation::class);

        $structure->attributes = $optionAttributes;

        return $structure;
    }

    /**
     * @param Option $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     *
     * @return Option
     */
    protected function addOptionPriceToStructure($structure, $entity)
    {
        $prices = [];

        $optionPrices = $entity->getOptionPrice();
        if (!empty($optionPrices)) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\OptionPrice $optionPrice */
            foreach ($optionPrices as $optionPrice) {
                $priceStructure = $this->structureFromEntityConverter->convertOne($optionPrice, OptionPrice::class);
                $priceStructure->channel = $optionPrice->getChannel()->getId();

                $prices[] = $priceStructure;
            }
        }

        $structure->prices = $prices;

        return $structure;
    }

    /**
     * @param Option $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Option $entity
     *
     * @return Option
     */
    protected function addStockToStructure($structure, $entity)
    {
        $stock = [];

        $stocks = $entity->getOptionStock();

        if (!empty($stocks)) {
            foreach ($stocks as $stockEntity) {
                $stockStructure = $this->structureFromEntityConverter->convertOne($stockEntity, Stock::class);

                $stockStructure->channel = $stockEntity->getChannel()->getId();

                $stock[] = $stockStructure;
            }
        }

        $structure->stock = $stock;

        return $structure;
    }
}
