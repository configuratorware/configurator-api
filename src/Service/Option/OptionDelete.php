<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\Entity\Option;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;

/**
 * @internal
 */
class OptionDelete
{
    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * OptionDelete constructor.
     *
     * @param OptionRepository $optionRepository
     */
    public function __construct(OptionRepository $optionRepository)
    {
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Option $entity */
            $entity = $this->optionRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            if (!empty($entity->getOptionText()->toArray())) {
                foreach ($entity->getOptionText() as $optionText) {
                    $this->optionRepository->delete($optionText);
                }
            }

            if (!empty($entity->getOptionPrice()->toArray())) {
                foreach ($entity->getOptionPrice() as $optionPrice) {
                    $this->optionRepository->delete($optionPrice);
                }
            }

            if (!empty($entity->getOptionAttribute()->toArray())) {
                foreach ($entity->getOptionAttribute() as $optionAttribute) {
                    $this->optionRepository->delete($optionAttribute);
                }
            }

            if (!empty($entity->getOptionStock()->toArray())) {
                foreach ($entity->getOptionStock() as $optionStock) {
                    $this->optionRepository->delete($optionStock);
                }
            }

            if (!empty($entity->getOptionGroup()->toArray())) {
                foreach ($entity->getOptionGroup() as $optionGroup) {
                    $this->optionRepository->delete($optionGroup);
                }
            }

            if (!empty($entity->getOptionGroupEntry()->toArray())) {
                foreach ($entity->getOptionGroupEntry() as $optionGroupEntry) {
                    $this->optionRepository->delete($optionGroupEntry);
                }
            }

            if (!empty($entity->getItemOptionclassificationOption()->toArray())) {
                foreach ($entity->getItemOptionclassificationOption() as $itemOptionClassificationOption) {
                    $this->optionRepository->delete($itemOptionClassificationOption);
                }
            }

            $this->optionRepository->delete($entity);
        }
    }
}
