<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Option;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;

/**
 * @internal
 */
class FrontendOptionGrouping
{
    /**
     * @var ItemgroupRepository
     */
    private $itemGroupRepository;

    /**
     * @var ItemgroupentryRepository
     */
    private $itemGroupEntryRepository;

    /**
     * FrontendOptionGrouping constructor.
     *
     * @param ItemgroupRepository $itemGroupRepository
     * @param ItemgroupentryRepository $itemGroupEntryRepository
     */
    public function __construct(
        ItemgroupRepository $itemGroupRepository,
        ItemgroupentryRepository $itemGroupEntryRepository
    ) {
        $this->itemGroupRepository = $itemGroupRepository;
        $this->itemGroupEntryRepository = $itemGroupEntryRepository;
    }

    /**
     * @param array $optionStructures
     *
     * @return array
     */
    public function groupOptionsByItemGroups($optionStructures)
    {
        $groupedOptionStructures = [];
        $indexedOptionStructures = [];

        $optionIdentifiers = [];
        foreach ($optionStructures as $optionStructure) {
            $optionIdentifiers[] = $optionStructure->identifier;
            $indexedOptionStructures[$optionStructure->identifier] = $optionStructure;
        }

        $result = $this->itemGroupRepository->getOptionGroupMapping($optionIdentifiers);

        // if mapping entries where found, build options tree from he mapping data
        // else return the options list as is
        if (!empty($result)) {
            $currentParent = null;
            $currentGroup = null;
            $currentEntry = null;
            $currentOption = null;
            $mappingKeys = [];

            $mapping = [];

            foreach ($result as $row) {
                if ($currentParent != $row['parent_identifier'] || $currentOption != $row['option_identifier']) {
                    $currentParent = $row['parent_identifier'];
                    $mappingKeys = [$currentParent];
                    $mappingKeys[] = $row['group_identifier'];
                    $mappingKeys[] = $row['group_entry_identifier'];

                    $currentOption = $row['option_identifier'];
                    $currentGroup = $row['group_identifier'];
                    $currentEntry = $row['group_entry_identifier'];
                } else {
                    if ($currentGroup != $row['group_identifier'] && $currentEntry != $row['group_entry_identifier']) {
                        $mappingKeys = array_slice($mappingKeys, 0, count($mappingKeys) - 1);

                        $mappingKeys[] = $currentEntry;
                        $mappingKeys[] = $row['group_identifier'];
                        $mappingKeys[] = $row['group_entry_identifier'];

                        $currentGroup = $row['group_identifier'];
                        $currentEntry = $row['group_entry_identifier'];
                    }
                }

                $check = $this->multiDimensionArrayValue($mapping, $mappingKeys);

                if (empty($check)) {
                    $this->multiDimensionArrayValue($mapping, $mappingKeys, $row['option_identifier']);
                }
            }

            // iterate over the mapping to build the options structure
            $distributedOptions = [];
            foreach ($mapping as $parentIdentifier => $optionMapping) {
                $groupedOptionStructures = array_merge($groupedOptionStructures,
                    $this->transformOptionsMapping($optionMapping, $indexedOptionStructures, $distributedOptions));
            }

            // add options that where not added by grouping
            foreach ($optionStructures as $option) {
                if (!in_array($option->identifier, $distributedOptions)) {
                    $groupedOptionStructures[] = $option;
                }
            }
        } else {
            $groupedOptionStructures = $optionStructures;
        }

        return $groupedOptionStructures;
    }

    /**
     * @param array $optionsMapping
     * @param array $indexedOptionStructures
     * @param array $distributedOptions
     *
     * @return array
     */
    protected function transformOptionsMapping($optionsMapping, $indexedOptionStructures, &$distributedOptions)
    {
        $options = [];
        foreach ($optionsMapping as $groupIdentifier => $childMappings) {
            if (is_array($childMappings)) {
                // find first leaf and get option structure
                $itemIdentifiers = new \ArrayObject();
                array_walk_recursive($childMappings, function ($value, $key, $itemIdentifiers) {
                    $itemIdentifiers[] = $value;
                }, $itemIdentifiers);
                $option = unserialize(serialize($indexedOptionStructures[$itemIdentifiers[0]]));

                $children = [];
                foreach ($childMappings as $groupEntryIdentifier => $childGroupMapping) {
                    if (is_array($childGroupMapping)) {
                        $groupChildren = $this->transformOptionsMapping($childGroupMapping,
                            $indexedOptionStructures, $distributedOptions);
                        $child = $groupChildren[0];
                    } else {
                        $child = $indexedOptionStructures[$childGroupMapping];
                    }
                    $child->itemgroupentry_identifier = $groupEntryIdentifier;

                    $itemGroupEntryEntity = $this->itemGroupEntryRepository->findOneBy(['identifier' => $groupEntryIdentifier]);
                    $child->itemgroupentry_title = $itemGroupEntryEntity->getTranslatedTitle();

                    $children[] = $child;
                    $distributedOptions[$child->identifier] = $child->identifier;
                }

                $itemGroupEntity = $this->itemGroupRepository->findOneBy(['identifier' => $groupIdentifier]);

                $optionGroup = new \stdClass();
                $optionGroup->identifier = $groupIdentifier;
                $optionGroup->title = $itemGroupEntity->getTranslatedTitle();
                $optionGroup->children = $children;

                $option->option_group = $optionGroup;

                $options[] = $option;
            }
        }

        return $options;
    }

    /**
     * sets or gets the value in an multidimensional array by an array of keys
     * if no value is given the current value is returned.
     * in either case the path is created.
     *
     * @param $array
     * @param array $path
     * @param null $value
     *
     * @return mixed
     */
    private function multiDimensionArrayValue(&$array, $path = [], &$value = null)
    {
        $args = func_get_args();
        $ref = &$array;
        foreach ($path as $key) {
            if (!is_array($ref)) {
                $ref = [];
            }
            $ref = &$ref[$key];
        }
        $prev = $ref;
        if (array_key_exists(2, $args)) {
            $ref = $value;
        }

        return $prev;
    }
}
