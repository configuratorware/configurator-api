<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassificationOption;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;

/**
 * @internal
 */
class ItemOptionClassificationOptionSorter
{
    /**
     * @param ItemOptionclassificationOption[] $itemOptionClassificationOptions
     */
    public function sortAsc(array &$itemOptionClassificationOptions): void
    {
        usort($itemOptionClassificationOptions, static function (ItemOptionclassificationOption $a, ItemOptionclassificationOption $b) {
            if (null === $b->getSequenceNumber()) {
                return -1;
            }

            return $a->getSequenceNumber() <=> $b->getSequenceNumber();
        });
    }
}
