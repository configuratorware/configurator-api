<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataTransfer;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\EntityDefaultsReset;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\DesignAreaDelete;

/**
 * @internal
 */
class DesignAreaTransfer
{
    /**
     * @var DesignAreaDelete
     */
    private $designAreaDelete;

    /**
     * @var EntityDefaultsReset
     */
    private $entityDefaultsReset;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * DesignAreaTransfer constructor.
     *
     * @param DesignAreaDelete $designAreaDelete
     * @param ItemRepository $itemRepository
     * @param EntityDefaultsReset $entityDefaultsReset
     */
    public function __construct(
        DesignAreaDelete $designAreaDelete,
        ItemRepository $itemRepository,
        EntityDefaultsReset $entityDefaultsReset
    ) {
        $this->designAreaDelete = $designAreaDelete;
        $this->itemRepository = $itemRepository;
        $this->entityDefaultsReset = $entityDefaultsReset;
    }

    /**
     * @param Item $sourceItem
     * @param Item $targetItem
     *
     * @throws \Exception
     */
    public function transfer(Item $sourceItem, Item $targetItem)
    {
        $this->itemRepository->beginTransaction();

        $this->delete($targetItem->getDesignArea());

        $this->copy($sourceItem, $targetItem);

        $this->itemRepository->commitTransaction();
    }

    /**
     * @param Collection $deleteAreas
     */
    private function delete(Collection $deleteAreas)
    {
        foreach ($deleteAreas as $area) {
            $this->designAreaDelete->delete($area->getId());
        }
    }

    /**
     * @param Item $sourceItem
     * @param Item $targetItem
     *
     * @throws \Exception
     */
    private function copy(Item $sourceItem, Item $targetItem): void
    {
        /** @var DesignArea $sourceArea */
        foreach ($sourceItem->getDesignArea() as $sourceArea) {
            $targetArea = clone $sourceArea;
            $targetArea->setItem($targetItem);
            $this->entityDefaultsReset->reset($targetArea);

            /** @var DesignAreaText $sourceText */
            foreach ($sourceArea->getDesignAreaText() as $sourceText) {
                $targetText = clone $sourceText;
                $targetText->setDesignArea($targetArea);
                $this->entityDefaultsReset->reset($targetText);
                $targetArea->addDesignAreaText($targetText);
            }

            /** @var DesignAreaDesignProductionMethod $sourceMethod */
            foreach ($sourceArea->getDesignAreaDesignProductionMethod() as $sourceMethod) {
                $targetMethod = clone $sourceMethod;
                $targetMethod->setDesignArea($targetArea);
                $this->entityDefaultsReset->reset($targetMethod);

                foreach ($sourceMethod->getDesignAreaDesignProductionMethodPrice() as $sourcePrice) {
                    $targetPrice = clone $sourcePrice;
                    $targetPrice->setDesignAreaDesignProductionMethod($targetMethod);
                    $this->entityDefaultsReset->reset($targetPrice);
                    $targetMethod->addDesignAreaDesignProductionMethodPrice($targetPrice);
                }

                $targetArea->addDesignAreaDesignProductionMethod($targetMethod);
            }

            $targetItem->addDesignArea($targetArea);
        }

        $this->itemRepository->save($targetItem);
    }
}
