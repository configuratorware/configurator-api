<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataTransfer;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;

/**
 * @internal
 */
class ItemStatusTransfer
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @param ItemRepository $itemRepository
     */
    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param Item $sourceItem
     * @param Item $targetItem
     */
    public function transfer(Item $sourceItem, Item $targetItem): void
    {
        $targetItem->setItemStatus($sourceItem->getItemStatus());

        $this->itemRepository->save($targetItem);
    }
}
