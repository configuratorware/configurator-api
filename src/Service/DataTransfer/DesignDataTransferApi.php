<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataTransfer;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignDataTransfer as DesignDataTransferStructure;

class DesignDataTransferApi
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var DesignAreaTransfer
     */
    private $designAreaTransfer;

    /**
     * @var VisualizationDataTransfer
     */
    private $visualizationDataTransfer;

    /**
     * @var ItemStatusTransfer
     */
    private $itemStatusTransfer;

    /**
     * DesignDataTransfer constructor.
     *
     * @param ItemRepository $itemRepository
     * @param DesignAreaTransfer $designAreaTransfer
     * @param VisualizationDataTransfer $visualizationDataTransfer
     * @param ItemStatusTransfer $itemStatusTransfer
     */
    public function __construct(
        ItemRepository $itemRepository,
        DesignAreaTransfer $designAreaTransfer,
        VisualizationDataTransfer $visualizationDataTransfer,
        ItemStatusTransfer $itemStatusTransfer
    ) {
        $this->itemRepository = $itemRepository;
        $this->designAreaTransfer = $designAreaTransfer;
        $this->visualizationDataTransfer = $visualizationDataTransfer;
        $this->itemStatusTransfer = $itemStatusTransfer;
    }

    /**
     * @param DesignDataTransferStructure $designDataTransferStructure
     *
     * @throws Exception
     */
    public function transfer(DesignDataTransferStructure $designDataTransferStructure)
    {
        $sourceItem = $this->itemRepository->findOneById($designDataTransferStructure->sourceItemId);
        $targetItem = $this->itemRepository->findOneById($designDataTransferStructure->targetItemId);

        if (!$sourceItem || !$targetItem) {
            throw new NotFoundException();
        }

        if (true === $designDataTransferStructure->designAreas) {
            $this->designAreaTransfer->transfer($sourceItem, $targetItem);
        }

        if (true === $designDataTransferStructure->visualizationData) {
            $this->visualizationDataTransfer->transfer($sourceItem, $targetItem);
        }

        if (true === $designDataTransferStructure->itemStatus) {
            $this->itemStatusTransfer->transfer($sourceItem, $targetItem);
        }
    }
}
