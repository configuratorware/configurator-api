<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataTransfer;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewText;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\EntityDefaultsReset;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewDelete;

/**
 * @internal
 */
class VisualizationDataTransfer
{
    /**
     * @var DesignViewDelete
     */
    private $designViewDelete;

    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var EntityDefaultsReset
     */
    private $entityDefaultsReset;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var CreatorViewDelete
     */
    private $creatorViewDelete;

    /**
     * DesignAreaTransfer constructor.
     *
     * @param DesignViewDelete $designViewDelete
     * @param DesignAreaRepository $designAreaRepository
     * @param ItemRepository $itemRepository
     * @param EntityDefaultsReset $entityDefaultsReset
     * @param CreatorViewDelete $creatorViewDelete
     */
    public function __construct(
        DesignViewDelete $designViewDelete,
        DesignAreaRepository $designAreaRepository,
        ItemRepository $itemRepository,
        EntityDefaultsReset $entityDefaultsReset,
        CreatorViewDelete $creatorViewDelete
    ) {
        $this->designViewDelete = $designViewDelete;
        $this->designAreaRepository = $designAreaRepository;
        $this->itemRepository = $itemRepository;
        $this->entityDefaultsReset = $entityDefaultsReset;
        $this->creatorViewDelete = $creatorViewDelete;
    }

    /**
     * @param Item $sourceItem
     * @param Item $targetItem
     *
     * @throws \Exception
     */
    public function transfer(Item $sourceItem, Item $targetItem)
    {
        $this->itemRepository->beginTransaction();

        $this->deleteDesignViews($targetItem->getDesignView());
        $this->deleteCreatorViews($targetItem->getCreatorView());

        $this->copy($sourceItem, $targetItem);

        $this->itemRepository->commitTransaction();
    }

    /**
     * @param Collection $deleteViews
     */
    private function deleteDesignViews(Collection $deleteViews)
    {
        foreach ($deleteViews as $view) {
            $this->designViewDelete->delete($view->getId());
        }
    }

    /**
     * @param Collection $deleteViews
     */
    private function deleteCreatorViews(Collection $deleteViews)
    {
        foreach ($deleteViews as $view) {
            $this->creatorViewDelete->delete($view->getId());
        }
    }

    /**
     * @param Item $sourceItem
     * @param Item $targetItem
     *
     * @throws \Exception
     */
    private function copy(Item $sourceItem, Item $targetItem): void
    {
        $sourceConfigurationMode = $sourceItem->getConfigurationMode();
        $targetConfigurationMode = $targetItem->getConfigurationMode();

        if (Item::CONFIGURATION_MODE_DESIGNER === $sourceConfigurationMode && Item::CONFIGURATION_MODE_DESIGNER === $targetConfigurationMode) {
            $this->copyDesignerSourceDesignerTarget($sourceItem, $targetItem);
        } elseif (Item::CONFIGURATION_MODE_DESIGNER === $sourceConfigurationMode && Item::CONFIGURATION_MODE_CREATOR_DESIGNER === $targetConfigurationMode) {
            $this->copyDesignerSourceCreatorTarget($sourceItem, $targetItem);
        } elseif (Item::CONFIGURATION_MODE_CREATOR_DESIGNER === $sourceConfigurationMode && Item::CONFIGURATION_MODE_DESIGNER === $targetConfigurationMode) {
            $this->copyCreatorSourceDesignerTarget($sourceItem, $targetItem);
        } elseif (Item::CONFIGURATION_MODE_CREATOR_DESIGNER === $sourceConfigurationMode && Item::CONFIGURATION_MODE_CREATOR_DESIGNER === $targetConfigurationMode) {
            $this->copyCreatorSourceCreatorTarget($sourceItem, $targetItem);
        }
    }

    private function copyDesignerSourceDesignerTarget(Item $sourceItem, Item $targetItem): void
    {
        /** @var DesignView $sourceView */
        foreach ($sourceItem->getDesignView() as $sourceView) {
            $targetView = clone $sourceView;
            $targetView->setItem($targetItem);
            $this->entityDefaultsReset->reset($targetView);

            /** @var DesignViewText $sourceText */
            foreach ($sourceView->getDesignViewText() as $sourceText) {
                $targetText = clone $sourceText;
                $targetText->setDesignView($targetView);
                $this->entityDefaultsReset->reset($targetText);
                $targetView->addDesignViewText($targetText);
            }

            /** @var DesignViewDesignArea $sourceAreaRelation */
            foreach ($sourceView->getDesignViewDesignArea() as $sourceAreaRelation) {
                $targetDesignArea = $this->designAreaRepository->findOneBy([
                    'identifier' => $sourceAreaRelation->getDesignArea()->getIdentifier(),
                    'item' => $targetItem->getId(),
                ]);

                if ($targetDesignArea) {
                    $targetAreaRelation = clone $sourceAreaRelation;
                    $targetAreaRelation->setDesignView($targetView);
                    $targetAreaRelation->setDesignArea($targetDesignArea);
                    $this->entityDefaultsReset->reset($targetAreaRelation);

                    $targetView->addDesignViewDesignArea($targetAreaRelation);
                }
            }

            $targetItem->addDesignView($targetView);
        }

        $this->itemRepository->save($targetItem);
    }

    private function copyDesignerSourceCreatorTarget(Item $sourceItem, Item $targetItem): void
    {
        /** @var DesignView $sourceView */
        foreach ($sourceItem->getDesignView() as $sourceView) {
            $targetView = CreatorView::initFromDesignView($sourceView, $targetItem);

            /** @var DesignViewText $sourceText */
            foreach ($sourceView->getDesignViewText() as $sourceText) {
                $targetText = CreatorViewText::fromDesignViewText($sourceText, $targetView);
                $targetView->addCreatorViewText($targetText);
            }

            foreach ($sourceView->getDesignViewDesignArea() as $sourceAreaRelation) {
                $targetDesignArea = $this->designAreaRepository->findOneBy([
                    'identifier' => $sourceAreaRelation->getDesignArea()->getIdentifier(),
                    'item' => $targetItem->getId(),
                ]);

                if ($targetDesignArea) {
                    $targetAreaRelation = CreatorViewDesignArea::fromDesignViewDesignArea($sourceAreaRelation, $targetView, $targetDesignArea);
                    $targetView->addCreatorViewDesignArea($targetAreaRelation);
                }
            }

            $targetItem->addCreatorView($targetView);
        }

        $this->itemRepository->save($targetItem);
    }

    private function copyCreatorSourceDesignerTarget(Item $sourceItem, Item $targetItem): void
    {
        /** @var CreatorView $sourceView */
        foreach ($sourceItem->getCreatorView() as $sourceView) {
            $targetView = DesignView::initFromCreatorView($sourceView, $targetItem);

            /** @var CreatorViewText $sourceText */
            foreach ($sourceView->getCreatorViewText() as $sourceText) {
                $targetText = DesignViewText::fromCreatorViewText($sourceText, $targetView);
                $targetView->addDesignViewText($targetText);
            }

            /** @var CreatorViewDesignArea $sourceAreaRelation */
            foreach ($sourceView->getCreatorViewDesignArea() as $sourceAreaRelation) {
                $targetDesignArea = $this->designAreaRepository->findOneBy([
                    'identifier' => $sourceAreaRelation->getDesignArea()->getIdentifier(),
                    'item' => $targetItem->getId(),
                ]);

                if ($targetDesignArea) {
                    $targetAreaRelation = DesignViewDesignArea::fromCreatorViewDesignArea($sourceAreaRelation, $targetView, $targetDesignArea);
                    $targetView->addDesignViewDesignArea($targetAreaRelation);
                }
            }

            $targetItem->addDesignView($targetView);
        }

        $this->itemRepository->save($targetItem);
    }

    private function copyCreatorSourceCreatorTarget(Item $sourceItem, Item $targetItem): void
    {
        /** @var CreatorView $sourceView */
        foreach ($sourceItem->getCreatorView() as $sourceView) {
            $targetView = clone $sourceView;
            $targetView->setItem($targetItem);
            $this->entityDefaultsReset->reset($targetView);

            if ($targetView->getDirectory()) {
                $targetView->setDirectory(str_replace($sourceItem->getIdentifier(), $targetItem->getIdentifier(), $targetView->getDirectory()));
            }

            /** @var CreatorViewText $sourceText */
            foreach ($sourceView->getCreatorViewText() as $sourceText) {
                $targetText = clone $sourceText;
                $targetText->setCreatorView($targetView);
                $this->entityDefaultsReset->reset($targetText);
                $targetView->addCreatorViewText($targetText);
            }

            /** @var CreatorViewDesignArea $sourceAreaRelation */
            foreach ($sourceView->getCreatorViewDesignArea() as $sourceAreaRelation) {
                $targetDesignArea = $this->designAreaRepository->findOneBy([
                    'identifier' => $sourceAreaRelation->getDesignArea()->getIdentifier(),
                    'item' => $targetItem->getId(),
                ]);

                if ($targetDesignArea) {
                    $targetAreaRelation = clone $sourceAreaRelation;
                    $targetAreaRelation->setCreatorView($targetView);
                    $targetAreaRelation->setDesignArea($targetDesignArea);
                    $this->entityDefaultsReset->reset($targetAreaRelation);
                    $targetView->addCreatorViewDesignArea($targetAreaRelation);
                }
            }

            $targetItem->addCreatorView($targetView);
        }

        $this->itemRepository->save($targetItem);
    }
}
