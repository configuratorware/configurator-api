<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\CalculationType;

use Redhotmagma\ApiBundle\Service\Converter\EntityHelper;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerProductionCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaDesignProductionMethodPrice as DesignAreaDesignProductionMethodPriceStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodRelation;

/**
 * Class CalculationTypeEntityFromDataConverter.
 *
 * Special Case:
 * The Structures are not aligned with Entities in this case:
 * Entity:
 * DesignArea
 * - DesignProductionMethod
 * -- DesignProductionMethodPrice
 * --- CalculationType
 *
 * Structure:
 * DesignArea
 * - DesignProductionMethod
 * -- CalculationType
 * --- DesignProductionMethodPrice
 *
 * To manage that, the convert function loops over the prices in structure and directly maps those to the
 * DesignProductionMethod
 *
 * @internal
 */
class CalculationTypeEntityFromDataConverter
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var DesignerProductionCalculationTypeRepository
     */
    private $designerProductionCalculationTypeRepository;

    /**
     * @var EntityHelper
     */
    private $entityHelper;

    /**
     * CalculationTypeEntityFromDataConverter constructor.
     *
     * @param ChannelRepository $channelRepository
     * @param DesignAreaRepository $designAreaRepository
     * @param DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository
     * @param EntityHelper $entityHelper
     */
    public function __construct(
        ChannelRepository $channelRepository,
        DesignAreaRepository $designAreaRepository,
        DesignerProductionCalculationTypeRepository $designerProductionCalculationTypeRepository,
        EntityHelper $entityHelper
    ) {
        $this->channelRepository = $channelRepository;
        $this->designAreaRepository = $designAreaRepository;
        $this->designerProductionCalculationTypeRepository = $designerProductionCalculationTypeRepository;
        $this->entityHelper = $entityHelper;
    }

    /**
     * @param DesignAreaDesignProductionMethod $designProductionMethodRelationEntity
     * @param DesignProductionMethodRelation $designProductionMethodRelationStructure
     *
     * @throws \Exception
     */
    public function convert(
        DesignAreaDesignProductionMethod $designProductionMethodRelationEntity,
        DesignProductionMethodRelation $designProductionMethodRelationStructure
    ): void {
        $priceIdsInStructure = [];
        $newPriceEntities = [];
        $calculationTypes = $designProductionMethodRelationStructure->calculationTypes ?? [];
        foreach ($calculationTypes as $calculationTypeStructure) {
            foreach ($calculationTypeStructure->prices as $priceStructure) {
                // Get the Entity with the same Id as in Structure
                $existingPrice = $designProductionMethodRelationEntity->getDesignAreaDesignProductionMethodPrice()->filter(
                    function ($entry) use ($priceStructure) {
                        /* @var DesignAreaDesignProductionMethodPrice $entry */
                        return $entry->getId() == $priceStructure->id;
                    }
                );

                // Choose if update or create
                if (false !== $existingPrice->first()) {
                    /** @var DesignAreaDesignProductionMethodPrice $pricesEntity */
                    $pricesEntity = $existingPrice->first();
                } else {
                    $pricesEntity = new DesignAreaDesignProductionMethodPrice();
                }

                // Update PriceModel Entity
                $this->updateProperties(
                    $pricesEntity,
                    $priceStructure,
                    $designProductionMethodRelationEntity,
                    $calculationTypeStructure
                );

                // Save Ids from Structure to know which to delete
                $priceIdsInStructure[] = $priceStructure->id;

                // Add new Relations to an Array of Entities
                if (false === $existingPrice->first()) {
                    $newPriceEntities[] = $pricesEntity;
                }
            }
        }

        // Add new Entities from Structure
        foreach ($newPriceEntities as $newEntity) {
            $designProductionMethodRelationEntity->addDesignAreaDesignProductionMethodPrice($newEntity);
        }

        // Remove Entities which are not in structure
        $entitiesMissingInStructure = $designProductionMethodRelationEntity->getDesignAreaDesignProductionMethodPrice()->filter(
            function ($entry) use ($priceIdsInStructure) {
                /* @var DesignAreaDesignProductionMethodPrice $entry */
                return !in_array($entry->getId(), $priceIdsInStructure);
            }
        );

        foreach ($entitiesMissingInStructure as $toRemove) {
            $toRemove->setDateDeleted(new \DateTime());
        }
    }

    /**
     * @param DesignAreaDesignProductionMethodPrice $pricesEntity
     * @param DesignAreaDesignProductionMethodPriceStructure $priceStructure
     * @param $designProductionMethodRelationEntity
     * @param $calculationTypeStructure
     *
     * @return DesignAreaDesignProductionMethodPrice
     */
    protected function updateProperties(
        DesignAreaDesignProductionMethodPrice $pricesEntity,
        DesignAreaDesignProductionMethodPriceStructure $priceStructure,
        $designProductionMethodRelationEntity,
        $calculationTypeStructure
    ): DesignAreaDesignProductionMethodPrice {
        $properties = array_keys((array)$priceStructure);

        foreach ($properties as $property) {
            if ('_metadata' !== $property) {
                $this->entityHelper->updateEntityProperty($pricesEntity, $property, $priceStructure->$property);
            }
        }

        /** @var Channel $channel */
        $channel = $this->channelRepository->findOneBy(['id' => $priceStructure->channel]);
        $pricesEntity->setChannel($channel);

        /**
         * @var DesignerProductionCalculationType $designProductionCalculationType
         * @var DesignAreaDesignProductionMethod $designProductionMethodRelationEntity
         */
        $designProductionCalculationType = $this->designerProductionCalculationTypeRepository->findOneBy(
            [
                'id' => $calculationTypeStructure->id,
                'designProductionMethod' => $designProductionMethodRelationEntity->getDesignProductionMethod()->getId(),
            ]
        );
        $pricesEntity->setDesignerProductionCalculationType($designProductionCalculationType);

        $pricesEntity->setDesignAreaDesignProductionMethod(
            $designProductionMethodRelationEntity
        );

        return $pricesEntity;
    }
}
