<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\CalculationType;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationTypeDesignAreaRelation as DesignerProductionCalculationTypeDesignAreaRelationStructure;

/**
 * @internal
 */
class CalculationTypeStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * DesignerProductionCalculationTypeDesignAreaRelationStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param DesignerProductionCalculationType $entity
     * @param string $structureClassName
     *
     * @return mixed|DesignerProductionCalculationTypeDesignAreaRelationStructure
     */
    public function convertOne(
        $entity,
        $structureClassName = DesignerProductionCalculationTypeDesignAreaRelationStructure::class
    ) {
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure->title = $entity->getTranslatedTitle();

        return $structure;
    }

    /**
     * @param Collection $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany(
        $entities,
        $structureClassName = DesignerProductionCalculationTypeDesignAreaRelationStructure::class
    ) {
        $structures = [];

        /** @var DesignerProductionCalculationType $entity */
        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }
}
