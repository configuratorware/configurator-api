<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DesignAreaMaskArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DesignAreaApi
{
    public const MASK_PATH = 'designer/masks/{itemIdentifier}/{designAreaIdentifier}.svg';
    public const DESIGN_PRODUCTION_METHOD_MASK_PATH = 'designer/masks/{itemIdentifier}/{designAreaIdentifier}_{designProductionMethodIdentifier}.svg';

    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var DesignAreaStructureFromEntityConverter
     */
    private $designAreaStructureFromEntityConverter;

    /**
     * @var DesignAreaSave
     */
    private $designAreaSave;

    /**
     * @var DesignAreaDelete
     */
    private $designAreaDelete;

    /**
     * @var UploadMask
     */
    private $uploadMask;

    /**
     * DesignAreaApi constructor.
     *
     * @param DesignAreaRepository $designAreaRepository
     * @param DesignAreaStructureFromEntityConverter $designAreaStructureFromEntityConverter
     * @param DesignAreaSave $designAreaSave
     * @param DesignAreaDelete $designAreaDelete
     * @param UploadMask $uploadMask
     */
    public function __construct(
        DesignAreaRepository $designAreaRepository,
        DesignAreaStructureFromEntityConverter $designAreaStructureFromEntityConverter,
        DesignAreaSave $designAreaSave,
        DesignAreaDelete $designAreaDelete,
        UploadMask $uploadMask
    ) {
        $this->designAreaRepository = $designAreaRepository;
        $this->designAreaStructureFromEntityConverter = $designAreaStructureFromEntityConverter;
        $this->designAreaSave = $designAreaSave;
        $this->designAreaDelete = $designAreaDelete;
        $this->uploadMask = $uploadMask;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(
        ListRequestArguments $arguments
    ): PaginationResult {
        $paginationResult = new PaginationResult();

        $entities = $this->designAreaRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult->data = $this->designAreaStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->designAreaRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return DesignAreaStructure
     *
     * @throws NotFoundException
     */
    public function getOne(
        int $id
    ): DesignAreaStructure {
        /** @var DesignArea $entity */
        $entity = $this->designAreaRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->designAreaStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param DesignAreaStructure $structure
     *
     * @return DesignAreaStructure
     */
    public function save(
        DesignAreaStructure $structure
    ): DesignAreaStructure {
        $structure = $this->designAreaSave->save($structure);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete(string $id): void
    {
        $ids = explode(',', $id);

        foreach ($ids as $id) {
            $this->designAreaDelete->delete((int)$id);
        }
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments): void
    {
        /** @var SequenceNumberData $sequenceNumberData */
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            /** @var DesignArea $entity */
            $entity = $this->designAreaRepository->findOneBy(['id' => $sequenceNumberData->id]);
            if (is_object($entity)) {
                $entity->setSequencenumber($sequenceNumberData->sequencenumber);
                $this->designAreaRepository->save($entity, false);
            }
        }

        $this->designAreaRepository->flush();
    }

    /**
     * @param DesignAreaMaskArguments $arguments
     *
     * @return bool
     */
    public function uploadMask(DesignAreaMaskArguments $arguments): bool
    {
        if (null !== $arguments->designProductionMethodId) {
            throw new BadRequestHttpException();
        }

        return $this->uploadMask->upload($arguments, self::MASK_PATH);
    }

    /**
     * @param DesignAreaMaskArguments $arguments
     *
     * @return bool
     */
    public function uploadDesignProductionMethodMask(DesignAreaMaskArguments $arguments): bool
    {
        if (null === $arguments->designProductionMethodId) {
            throw new BadRequestHttpException();
        }

        return $this->uploadMask->upload($arguments, self::DESIGN_PRODUCTION_METHOD_MASK_PATH);
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['designAreaText.title'];
    }
}
