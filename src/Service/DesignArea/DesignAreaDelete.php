<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;

/**
 * @internal
 */
class DesignAreaDelete
{
    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @param DesignAreaRepository $designAreaRepository
     */
    public function __construct(
        DesignAreaRepository $designAreaRepository
    ) {
        $this->designAreaRepository = $designAreaRepository;
    }

    /**
     * @param int $id
     */
    public function delete(int $id): void
    {
        $entity = $this->designAreaRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        // Delete Design Area Design Production Method Relation and its Prices
        $relations = $entity->getDesignAreaDesignProductionMethod();
        if (!$relations->isEmpty()) {
            foreach ($relations as $relation) {
                /** @var Collection $prices */
                $prices = $relation->getDesignAreaDesignProductionMethodPrice();
                if (!$prices->isEmpty()) {
                    foreach ($prices as $price) {
                        $this->designAreaRepository->delete($price);
                    }
                }
                $this->designAreaRepository->delete($relation);
            }
        }

        $this->removeRelation($entity, 'DesignAreaText');
        $this->removeRelation($entity, 'DesignViewDesignArea');
        $this->removeRelation($entity, 'CreatorViewDesignArea');

        $this->designAreaRepository->delete($entity);
    }

    /**
     * @param DesignArea $entity
     * @param string $relationName
     */
    protected function removeRelation(DesignArea $entity, string $relationName): void
    {
        $callEntity = 'get' . $relationName;
        /** @var mixed $relations */
        $relations = $entity->$callEntity();
        if (!$relations->isEmpty()) {
            foreach ($relations as $relationEntry) {
                $this->designAreaRepository->delete($relationEntry);
            }
        }
    }
}
