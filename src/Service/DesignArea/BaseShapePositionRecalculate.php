<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;

/**
 * @internal
 */
class BaseShapePositionRecalculate
{
    /**
     * @param DesignArea $designArea
     * @param string $visualizationMode
     * @param float $widthRatio
     * @param float $heightRatio
     */
    public function update(DesignArea $designArea, string $visualizationMode, float $widthRatio, float $heightRatio): void
    {
        /** @var DesignViewDesignArea $areaView */
        foreach ($designArea->getDesignViewDesignArea() as $areaView) {
            $baseShape = $areaView->getBaseShape();
            if ($baseShape instanceof \stdClass && isset($baseShape->type, $baseShape->width, $baseShape->height) && 'Plane' === $baseShape->type) {
                $baseShape->width = (int)($baseShape->width * $widthRatio);
                $baseShape->height = (int)($baseShape->height * $heightRatio);
                $areaView->setBaseShape($baseShape);
            }

            $position = $areaView->getPosition();
            if ($position instanceof \stdClass && isset($position->x, $position->y, $position->width, $position->height)) {
                if (VisualizationMode::is2dVariantVisualization($visualizationMode)) {
                    $position->x = (int)($position->x * $widthRatio);
                    $position->y = (int)($position->y * $heightRatio);
                }
                $position->width = (int)($position->width * $widthRatio);
                $position->height = (int)($position->height * $heightRatio);
                $areaView->setPosition($position);
            }
        }
    }
}
