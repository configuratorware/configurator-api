<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\CalculationType\CalculationTypeStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaDesignProductionMethodPrice as DesignAreaDesignProductionMethodPriceStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignAreaText as DesignAreaTextStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodRelation as DesignProductionMethodRelationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemRelation as ItemRelationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionRelation as OptionRelationStructure;

/**
 * @internal
 */
class DesignAreaStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var CalculationTypeStructureFromEntityConverter
     */
    private $calculationTypeStructureFromEntityConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * DesignAreaStructureFromEntityConverter constructor.
     *
     * @param CalculationTypeStructureFromEntityConverter $calculationTypeStructureFromEntityConverter
     * @param ItemRepository $itemRepository
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        CalculationTypeStructureFromEntityConverter $calculationTypeStructureFromEntityConverter,
        ItemRepository $itemRepository,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->calculationTypeStructureFromEntityConverter = $calculationTypeStructureFromEntityConverter;
        $this->itemRepository = $itemRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param DesignArea $entity
     * @param string $structureClassName
     *
     * @return DesignAreaStructure
     */
    public function convertOne($entity, $structureClassName = DesignAreaStructure::class)
    {
        /** @var DesignAreaStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        // Add Item or null to structure
        $structure = $this->addObjectToStructure(
            $structure,
            $entity,
            ItemRelationStructure::class,
            'item',
            'Item'
        );

        // Add Option or null to structure
        $structure = $this->addObjectToStructure(
            $structure,
            $entity,
            OptionRelationStructure::class,
            'option',
            'Option'
        );

        $structure = $this->addTextsToStructure(
            $structure,
            $entity,
            'DesignArea',
            DesignAreaTextStructure::class,
            'texts',
            'Text'
        );

        $structure = $this->addDesignProductionMethodsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = DesignAreaStructure::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param DesignAreaStructure $structure
     * @param DesignArea $entity
     * @param string $objectStructureClass
     * @param string $objectStructureName
     * @param string $objectEntityName
     *
     * @return DesignAreaStructure
     */
    protected function addObjectToStructure(
        DesignAreaStructure $structure,
        DesignArea $entity,
        string $objectStructureClass,
        string $objectStructureName,
        string $objectEntityName
    ): DesignAreaStructure {
        $callEntity = 'get' . $objectEntityName;

        /** @var mixed $object */
        $object = $entity->$callEntity();
        if ($object) {
            $structure->$objectStructureName = $this->structureFromEntityConverter->convertOne(
                $object,
                $objectStructureClass
            );

            if ($object->getTranslatedTitle()) {
                $structure->$objectStructureName->title = $object->getTranslatedTitle();
            }
        }

        return $structure;
    }

    /**
     * @param DesignAreaStructure $structure
     * @param DesignArea $entity
     * @param string $entityName
     * @param string $textStructureClass
     * @param string $textStructureName
     * @param string $textEntityName
     *
     * @return DesignAreaStructure
     */
    protected function addTextsToStructure(
        DesignAreaStructure $structure,
        DesignArea $entity,
        string $entityName,
        string $textStructureClass,
        string $textStructureName,
        string $textEntityName
    ): DesignAreaStructure {
        $callEntity = 'get' . $entityName . $textEntityName;

        /** @var DesignArea $entity */
        $texts = $entity->$callEntity();

        $result = [];
        if (!empty($texts)) {
            /** @var DesignAreaText $text */
            foreach ($texts as $text) {
                /** @var DesignAreaTextStructure $textStructure */
                $textStructure = $this->structureFromEntityConverter->convertOne(
                    $text,
                    $textStructureClass
                );

                if (!empty($textStructure)) {
                    $textStructure->language = $text->getLanguage()->getIso();
                    $result[] = $textStructure;
                }
            }
        }

        $structure->$textStructureName = $result;

        return $structure;
    }

    /**
     * @param DesignAreaStructure $structure
     * @param DesignArea $entity
     *
     * @return DesignAreaStructure
     */
    protected function addDesignProductionMethodsToStructure(
        DesignAreaStructure $structure,
        DesignArea $entity
    ): DesignAreaStructure {
        $result = [];

        $relations = $entity->getDesignAreaDesignProductionMethod();

        $itemIdentifiers = [];
        if (!empty($entity->getItem())) {
            $itemIdentifiers[] = $entity->getItem()->getIdentifier();
            $children = $this->itemRepository->getItemsByParent($entity->getItem()->getIdentifier());

            foreach ($children as $child) {
                $itemIdentifiers[] = $child->getIdentifier();
            }
        }

        if (!empty($relations)) {
            /** @var DesignAreaDesignProductionMethod $relation */
            foreach ($relations as $relation) {
                /** @var DesignAreaStructure $textStructure */
                $relationStructure = $this->structureFromEntityConverter->convertOne(
                    $relation->getDesignProductionMethod(),
                    DesignProductionMethodRelationStructure::class
                );

                /*
                 * Fields which cannot be populated by DesignProductionMethod
                 * receive data from relation: DesignAreaDesignProductionMethod
                 */
                foreach ((array)$relationStructure as $key => $data) {
                    $propertyGetter = 'get' . ucfirst($key);

                    /*
                     * Hint: if entity exists in DesignProductionMethod and DesignAreaDesignProductionMethod
                     * data will be retrieved from DesignAreaDesignProductionMethod if they are not excluded by LOCKED_ORIGIN_PROPERTIES
                     */
                    if (method_exists($relation, $propertyGetter) &&
                        null !== $relation->$propertyGetter() &&
                        !in_array($key, DesignProductionMethodRelationStructure::LOCKED_ORIGIN_PROPERTIES)) {
                        $relationStructure->$key = $relation->$propertyGetter();
                    }
                }

                $relationStructure->title = $relation->getDesignProductionMethod()->getTranslatedTitle();

                // remove entries from color mapping for no longer existing items
                if (!empty($itemIdentifiers)
                    && !empty($relationStructure->additionalData)
                    && isset($relationStructure->additionalData->engravingBackgroundColors)) {
                    foreach ($relationStructure->additionalData->engravingBackgroundColors as $identifier => $color) {
                        if (!in_array($identifier, $itemIdentifiers)) {
                            unset($relationStructure->additionalData->engravingBackgroundColors->$identifier);
                        }
                    }
                }

                $designProductionCalculationTypes =
                    $this->calculationTypeStructureFromEntityConverter->convertMany(
                        $relation->getDesignProductionMethod()->getDesignerProductionCalculationType()
                    );

                foreach ($designProductionCalculationTypes as $typeKey => $calculationType) {
                    if (true === $calculationType->itemAmountDependent) {
                        $calculationTypePrices = $relation->getDesignAreaDesignProductionMethodPrice()->filter(
                            function ($entry) use ($calculationType) {
                                /* @var DesignAreaDesignProductionMethodPrice $entry */
                                return $entry->getDesignerProductionCalculationType()->getId() == $calculationType->id;
                            }
                        );

                        if (false !== $calculationTypePrices->first()) {
                            foreach ($calculationTypePrices as $price) {
                                $priceStructure = $this->structureFromEntityConverter->convertOne(
                                    $price,
                                    DesignAreaDesignProductionMethodPriceStructure::class
                                );

                                if (!empty($priceStructure)) {
                                    $priceStructure->channel = $price->getChannel() ? $price->getChannel()->getId() : null;
                                    $calculationType->prices[] = $priceStructure;
                                }
                            }
                        }
                        $relationStructure->calculationTypes[] = $calculationType;
                    }
                }

                if (!empty($relationStructure)) {
                    $result[] = $relationStructure;
                }
            }
        }

        $structure->designProductionMethods = $result;

        return $structure;
    }
}
