<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @internal
 */
class MaskFileName
{
    /**
     * @param string $template
     * @param DesignArea $designArea
     * @param string|null $designProductionMethodId
     *
     * @psalm-suppress PossiblyFalseReference
     * @psalm-suppress PossiblyNullReference
     * @psalm-suppress InvalidArgument
     *
     * @return string
     */
    public function get(string $template, DesignArea $designArea, string $designProductionMethodId = null): string
    {
        $searchTerms = [
            '{itemIdentifier}',
            '{designAreaIdentifier}',
            '{designProductionMethodIdentifier}',
        ];

        $replaceTerms = [
            $designArea->getItem()->getIdentifier(),
            $designArea->getIdentifier(),
            '',
        ];

        if (null !== $designProductionMethodId) {
            $designProductionMethods = $designArea->getDesignAreaDesignProductionMethod()->filter(
                function (DesignAreaDesignProductionMethod $item) use ($designProductionMethodId) {
                    return $item->getDesignProductionMethod()->getId() == $designProductionMethodId;
                }
            );

            if (false !== $designProductionMethods->first()) {
                $replaceTerms[2] = $designProductionMethods->first()->getDesignProductionMethod()->getIdentifier();
            }
        }

        $result = str_replace($searchTerms, $replaceTerms, $template);

        // check if all placeholders got removed from string
        if (false !== strpos($result, '{')) {
            throw new BadRequestHttpException();
        }

        return $result;
    }
}
