<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DesignAreaMaskArguments;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @internal
 */
class UploadMask
{
    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var MaskFileName
     */
    private $maskFileName;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * UploadMask constructor.
     *
     * @param DesignAreaRepository $designAreaRepository
     * @param Filesystem $fileSystem
     * @param MaskFileName $maskFileName
     * @param string $mediaBasePath
     */
    public function __construct(
        DesignAreaRepository $designAreaRepository,
        Filesystem $fileSystem,
        MaskFileName $maskFileName,
        string $mediaBasePath
    ) {
        $this->designAreaRepository = $designAreaRepository;
        $this->fileSystem = $fileSystem;
        $this->maskFileName = $maskFileName;
        $this->mediaBasePath = $mediaBasePath;
    }

    /**
     * @param DesignAreaMaskArguments $arguments
     * @param string $template
     *
     * @return bool
     */
    public function upload(DesignAreaMaskArguments $arguments, string $template): bool
    {
        if (!isset($arguments->designAreaId) || !$arguments->uploadedFile instanceof UploadedFile) {
            throw new BadRequestHttpException();
        }

        $designArea = $this->designAreaRepository->find($arguments->designAreaId);

        if (!$designArea instanceof DesignArea) {
            throw new NotFoundException();
        }

        $fileName = $this->maskFileName->get($template, $designArea, $arguments->designProductionMethodId);

        $this->fileSystem->copy(
            $arguments->uploadedFile->getPathname(),
            $this->mediaBasePath . '/' . $fileName,
            true
        );

        return true;
    }
}
