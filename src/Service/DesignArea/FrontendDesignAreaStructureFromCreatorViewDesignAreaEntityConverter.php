<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\FrontendDesignProductionMethodStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignArea as DesignAreaStructure;

/**
 * @internal
 */
class FrontendDesignAreaStructureFromCreatorViewDesignAreaEntityConverter
{
    /**
     * @var FrontendDesignProductionMethodStructureFromEntityConverter
     */
    private $frontendDesignProductionMethodStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * FrontendDesignAreaStructureFromEntityConverter constructor.
     *
     * @param FrontendDesignProductionMethodStructureFromEntityConverter $frontendDesignProductionMethodStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        FrontendDesignProductionMethodStructureFromEntityConverter $frontendDesignProductionMethodStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->frontendDesignProductionMethodStructureFromEntityConverter = $frontendDesignProductionMethodStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param CreatorViewDesignArea $relationEntity
     *
     * @return DesignAreaStructure
     */
    public function convertOne(CreatorViewDesignArea $relationEntity): DesignAreaStructure
    {
        $relationTarget = $relationEntity->getDesignArea();

        $structure = $this->structureFromEntityConverter->convertOne($relationTarget, DesignAreaStructure::class);

        $structure = $this->structureFromEntityConverter->completeByRelationData($structure, $relationEntity);

        $structure->title = $relationTarget->getTranslatedTitle();

        $this->setDefaultViewIdentifierOnAreaStructure($structure, $relationTarget);

        $structure->designProductionMethods = $this->frontendDesignProductionMethodStructureFromEntityConverter->convertMany($relationTarget->getDesignAreaDesignProductionMethod()->toArray());

        return $structure;
    }

    /**
     * @param CreatorViewDesignArea[] $relationEntities
     *
     * @return array
     */
    public function convertMany(array $relationEntities): array
    {
        $structures = [];

        foreach ($relationEntities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    /**
     * Set the value fo default design view, if present.
     *
     * @param DesignAreaStructure $structure
     * @param DesignArea          $designAreaEntity
     *
     * @psalm-suppress PossiblyNullReference
     */
    private function setDefaultViewIdentifierOnAreaStructure(DesignAreaStructure $structure, DesignArea $designAreaEntity): void
    {
        /** @var DesignViewDesignArea $relation */
        foreach ($designAreaEntity->getDesignViewDesignArea() as $relation) {
            if ($relation->getIsDefault()) {
                $structure->defaultViewIdentifier = $relation->getDesignView()->getIdentifier();
            }
        }
    }
}
