<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\CalculationType\CalculationTypeEntityFromDataConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodRelation;

/**
 * @internal
 */
class DesignAreaEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var CalculationTypeEntityFromDataConverter
     */
    private $calculationTypeEntityFromDataConverter;

    /**
     * @var DesignProductionMethodRepository
     */
    private $designProductionMethodRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @param CalculationTypeEntityFromDataConverter $calculationTypeEntityFromDataConverter
     * @param DesignProductionMethodRepository $designProductionMethodRepository
     * @param ItemRepository $itemRepository
     * @param OptionRepository $optionRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        CalculationTypeEntityFromDataConverter $calculationTypeEntityFromDataConverter,
        DesignProductionMethodRepository $designProductionMethodRepository,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->calculationTypeEntityFromDataConverter = $calculationTypeEntityFromDataConverter;
        $this->designProductionMethodRepository = $designProductionMethodRepository;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param DesignAreaStructure $structure
     * @param DesignArea $entity
     * @param string $entityClassName
     *
     * @return DesignArea
     *
     * @throws \Exception
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = DesignArea::class
    ): DesignArea {
        if (isset($structure->item)) {
            $structure->item = $this->itemRepository->findOneBy(['id' => $structure->item->id]);
        }

        if (isset($structure->option)) {
            $structure->option = $this->optionRepository->findOneBy(['id' => $structure->option->id]);
        }

        /** @var DesignArea $entity */
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        $entity = $this->updateTexts(
            $entity,
            $structure,
            'texts',
            'DesignAreaText'
        );

        $entity = $this->updateDesignProductionMethods(
            $entity,
            $structure,
            'DesignAreaDesignProductionMethod',
            DesignAreaDesignProductionMethod::class,
            'designProductionMethods',
            'DesignProductionMethod',
            $this->designProductionMethodRepository
        );

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     *
     * @throws \Exception
     */
    public function convertMany($entities, $structureClassName = DesignArea::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param DesignArea $entity
     * @param DesignAreaStructure $structure
     * @param string $relationStructureName
     * @param string $relationEntityName
     *
     * @return DesignArea
     */
    protected function updateTexts(
        DesignArea $entity,
        DesignAreaStructure $structure,
        string $relationStructureName,
        string $relationEntityName
    ): DesignArea {
        /** @var DesignArea $entity */
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        return $entity;
    }

    /**
     * @param DesignArea $entity
     * @param DesignAreaStructure $structure
     * @param string $relationEntityName
     * @param string $relationEntityClass
     * @param string $relatedStructureName
     * @param string $relatedEntityName
     * @param DesignProductionMethodRepository $relatedRepository
     *
     * @return DesignArea
     *
     * @throws \Exception
     */
    protected function updateDesignProductionMethods(
        DesignArea $entity,
        DesignAreaStructure $structure,
        string $relationEntityName,
        string $relationEntityClass,
        string $relatedStructureName,
        string $relatedEntityName,
        DesignProductionMethodRepository $relatedRepository
    ): DesignArea {
        /** @var DesignArea $entity */
        $entity = $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
            $structure,
            $entity,
            $relationEntityName,
            $relatedStructureName,
            $relatedEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToManyRelations(
            $structure,
            $entity,
            $relationEntityClass,
            $relatedStructureName,
            $relatedEntityName,
            $relatedRepository
        );

        // Push DesignProductionMethods's id as key to array for easier access
        $designProductionMethodsById = null;
        foreach ($structure->designProductionMethods as $method) {
            $designProductionMethodsById[$method->id] = $method;
        }

        // If Json contains data for DesignAreaDesignProductionMethods, update entity
        $designAreaDesignProductionMethods = $entity->getDesignAreaDesignProductionMethod();
        $item = $entity->getItem();

        if (is_array($designProductionMethodsById) && is_countable($designAreaDesignProductionMethods)) {
            $amount = count($designAreaDesignProductionMethods);
            for ($i = 0; $i < $amount; ++$i) {
                // get the DesignAreaDesignProductionMethod Entity to change
                $areaProductionMethodEntity = $entity->getDesignAreaDesignProductionMethod()[$i];
                if (!$areaProductionMethodEntity instanceof DesignAreaDesignProductionMethod) {
                    continue;
                }

                // check if the DesignAreaDesignProductionMethod is in Structure to retrieve data
                $productionMethodEntity = $areaProductionMethodEntity->getDesignProductionMethod();
                if (null !== $productionMethodEntity
                    && !array_key_exists($productionMethodEntity->getId(), $designProductionMethodsById)) {
                    continue;
                }

                // check if Structure has correct type
                $areaProductionMethodStructure = $designProductionMethodsById[$productionMethodEntity->getId()];
                if (!$areaProductionMethodStructure instanceof DesignProductionMethodRelation) {
                    continue;
                }

                // reset all designAreaDesignProductionMethods related to the related item
                if (null !== $item && true === $areaProductionMethodStructure->isDefault) {
                    foreach ($item->getDesignArea() as $overrideAreaEntity) {
                        foreach ($overrideAreaEntity->getDesignAreaDesignProductionMethod() as $overrideAreaMethodEntity) {
                            $overrideAreaMethodEntity->setIsDefault(false);
                        }
                    }
                }

                $areaProductionMethodEntity->setIsDefault($areaProductionMethodStructure->isDefault ?? false);
                $areaProductionMethodEntity->setHeight($areaProductionMethodStructure->height);
                $areaProductionMethodEntity->setWidth($areaProductionMethodStructure->width);
                $areaProductionMethodEntity->setCustomData($areaProductionMethodStructure->customData);
                $areaProductionMethodEntity->setAdditionalData($areaProductionMethodStructure->additionalData);
                $areaProductionMethodEntity->setMask($areaProductionMethodStructure->mask);
                $areaProductionMethodEntity->setDefaultColors($areaProductionMethodStructure->defaultColors);
                $areaProductionMethodEntity->setMinimumOrderAmount($areaProductionMethodStructure->minimumOrderAmount);
                $areaProductionMethodEntity->setAllowBulkNames((bool)$areaProductionMethodStructure->allowBulkNames);
                $areaProductionMethodEntity->setDesignElementsLocked((bool)$areaProductionMethodStructure->designElementsLocked);
                $areaProductionMethodEntity->setMaxElements($areaProductionMethodStructure->maxElements);
                $areaProductionMethodEntity->setMaxImages($areaProductionMethodStructure->maxImages);
                $areaProductionMethodEntity->setMaxTexts($areaProductionMethodStructure->maxTexts);
                $areaProductionMethodEntity->setOneLineText($areaProductionMethodStructure->oneLineText);

                $this->calculationTypeEntityFromDataConverter->convert(
                    $areaProductionMethodEntity,
                    $areaProductionMethodStructure
                );
            }
        }

        return $entity;
    }
}
