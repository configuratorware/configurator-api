<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemSetting\ItemSettingFactory;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignArea as DesignAreaStructure;

/**
 * @internal
 */
class DesignAreaSave
{
    /**
     * @var BaseShapePositionRecalculate
     */
    private $baseShapePositionRecalculate;

    /**
     * @var DesignAreaRepository
     */
    private $designAreaRepository;

    /**
     * @var DesignAreaEntityFromStructureConverter
     */
    private $designAreaEntityFromStructureConverter;

    /**
     * @var DesignAreaStructureFromEntityConverter
     */
    private $designAreaStructureFromEntityConverter;

    /**
     * @var ItemSettingFactory
     */
    private $itemSettingFactory;

    /**
     * DesignAreaSave constructor.
     *
     * @param BaseShapePositionRecalculate $baseShapePositionRecalculate
     * @param DesignAreaRepository $designAreaRepository
     * @param DesignAreaEntityFromStructureConverter $designAreaEntityFromStructureConverter
     * @param DesignAreaStructureFromEntityConverter $designAreaStructureFromEntityConverter
     * @param ItemSettingFactory $itemSettingFactory
     */
    public function __construct(
        BaseShapePositionRecalculate $baseShapePositionRecalculate,
        DesignAreaRepository $designAreaRepository,
        DesignAreaEntityFromStructureConverter $designAreaEntityFromStructureConverter,
        DesignAreaStructureFromEntityConverter $designAreaStructureFromEntityConverter,
        ItemSettingFactory $itemSettingFactory
    ) {
        $this->baseShapePositionRecalculate = $baseShapePositionRecalculate;
        $this->designAreaRepository = $designAreaRepository;
        $this->designAreaEntityFromStructureConverter = $designAreaEntityFromStructureConverter;
        $this->designAreaStructureFromEntityConverter = $designAreaStructureFromEntityConverter;
        $this->itemSettingFactory = $itemSettingFactory;
    }

    /**
     * @param DesignAreaStructure $structure
     *
     * @return DesignAreaStructure
     *
     * @throws \Exception
     */
    public function save(DesignAreaStructure $structure): DesignAreaStructure
    {
        $entity = null;

        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->designAreaRepository->findOneBy(['id' => $structure->id]);
        }

        if (null !== $entity && $structure->width > 0 && $structure->height > 0
            && $entity->getWidth() > 0 && $entity->getHeight() > 0) {
            $widthRatio = $structure->width / $entity->getWidth();
            $heightRatio = $structure->height / $entity->getHeight();
            if ((1 !== $widthRatio || 1 !== $heightRatio) && null !== $entity->getItem()) {
                $itemSetting = $this->itemSettingFactory->createRelationStructure($entity->getItem());
                $this->baseShapePositionRecalculate->update(
                    $entity,
                    $itemSetting->visualizationDesigner,
                    $widthRatio,
                    $heightRatio
                );
            }
        }

        $entity = $this->designAreaEntityFromStructureConverter->convertOne($structure, $entity);
        $entity = $this->designAreaRepository->save($entity);

        $this->designAreaRepository->clear();

        /** @var DesignArea $entity */
        $entity = $this->designAreaRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->designAreaStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
