<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignArea;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignViewDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\FrontendDesignProductionMethodStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignViewDesignArea\BaseShape2dPositionCalculation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignArea as DesignAreaStructure;

/**
 * @internal
 */
class FrontendDesignAreaStructureFromDesignViewDesignAreaEntityConverter
{
    /**
     * @var FrontendDesignProductionMethodStructureFromEntityConverter
     */
    private $frontendDesignProductionMethodStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var BaseShape2dPositionCalculation
     */
    private $baseShape2dPositionCalculation;

    /**
     * FrontendDesignAreaStructureFromEntityConverter constructor.
     *
     * @param FrontendDesignProductionMethodStructureFromEntityConverter $frontendDesignProductionMethodStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param BaseShape2dPositionCalculation $baseShape2dPositionCalculation
     */
    public function __construct(
        FrontendDesignProductionMethodStructureFromEntityConverter $frontendDesignProductionMethodStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter,
        BaseShape2dPositionCalculation $baseShape2dPositionCalculation
    ) {
        $this->frontendDesignProductionMethodStructureFromEntityConverter = $frontendDesignProductionMethodStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->baseShape2dPositionCalculation = $baseShape2dPositionCalculation;
    }

    /**
     * @param DesignViewDesignArea $relationEntity
     *
     * @psalm-suppress PossiblyNullArgument, PossiblyNullReference $relationTarget
     *
     * @return DesignAreaStructure
     */
    public function convertOne(DesignViewDesignArea $relationEntity): DesignAreaStructure
    {
        $relationTarget = $relationEntity->getDesignArea();

        $relationEntity = $this->baseShape2dPositionCalculation->calculateBaseShapePosition($relationEntity);

        $structure = $this->structureFromEntityConverter->convertOne($relationTarget, DesignAreaStructure::class);

        $structure = $this->structureFromEntityConverter->completeByRelationData($structure, $relationEntity);

        $structure->title = $relationTarget->getTranslatedTitle();

        $this->setDefaultViewIdentifierOnAreaStructure($structure, $relationTarget);

        $structure->designProductionMethods = $this->frontendDesignProductionMethodStructureFromEntityConverter->convertMany($relationTarget->getDesignAreaDesignProductionMethod()->toArray());

        return $structure;
    }

    /**
     * @param DesignViewDesignArea[] $relationEntities
     *
     * @return array
     */
    public function convertMany(array $relationEntities): array
    {
        $structures = [];

        foreach ($relationEntities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    /**
     * Set the value fo default design view, if present.
     *
     * @param DesignAreaStructure $structure
     * @param DesignArea          $designAreaEntity
     *
     * @psalm-suppress PossiblyNullReference
     */
    private function setDefaultViewIdentifierOnAreaStructure(DesignAreaStructure $structure, DesignArea $designAreaEntity): void
    {
        /** @var DesignViewDesignArea $relation */
        foreach ($designAreaEntity->getDesignViewDesignArea() as $relation) {
            if ($relation->getIsDefault()) {
                $structure->defaultViewIdentifier = $relation->getDesignView()->getIdentifier();
            }
        }
    }
}
