<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Export;

use Redhotmagma\ConfiguratorApiBundle\Exporter\Repository\ExporterRepository;
use Symfony\Component\Filesystem\Filesystem;

class ExportItemService
{
    private ExporterRepository $exporterRepository;
    private Filesystem $filesystem;

    public function __construct(ExporterRepository $exporterRepository)
    {
        $this->exporterRepository = $exporterRepository;
        $this->filesystem = new Filesystem();
    }

    public function exportItems(array $itemIdentifiers, string $outputFolder, bool $combine): void
    {
        if (!is_dir($outputFolder)) {
            throw new \InvalidArgumentException('Output folder ' . $outputFolder . ' is invalid.');
        }

        $itemdata = $this->exporterRepository->loadItemdata($itemIdentifiers);

        foreach ($itemdata as $item) {
            $priceCollection = $this->exporterRepository->findPricesByComponentAndOption($item);
            $components = $this->exporterRepository->loadComponentsFor($item);
            $selectedOptionCollection = $this->exporterRepository->findDefaultOptionsFor($item);
            foreach ($components as $component) {
                $component->defaultSelections = $selectedOptionCollection->findDefaultSelectionFor($component->componentIdentifier);
                $component->updatePricesWith($priceCollection);
            }
            $item->components = $components;
        }

        if (!$combine) {
            foreach ($itemdata as $item) {
                $this->filesystem->dumpFile($outputFolder . DIRECTORY_SEPARATOR . $item->itemIdentifier . '.json', json_encode([$item]));
            }
        } else {
            $this->filesystem->dumpFile($outputFolder . DIRECTORY_SEPARATOR . time() . '.json', json_encode($itemdata));
        }
    }
}
