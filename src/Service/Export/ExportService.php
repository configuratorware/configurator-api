<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Export;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ExportService
{
    private Filesystem $filesystem;
    private string $exportTempDir;
    private string $projectDir;
    private ExportItemService $exportItemService;

    public function __construct(string $exportTempDir, ExportItemService $exportItemService)
    {
        $this->filesystem = new Filesystem();
        $this->exportTempDir = $exportTempDir;
        $this->exportItemService = $exportItemService;
    }

    public function export(array $itemIdentifiers, bool $combine): Response
    {
        $outputFolder = $this->exportTempDir . '/export';
        $this->filesystem->mkdir($outputFolder);

        try {
            $this->exportItemService->exportItems($itemIdentifiers, $outputFolder, $combine);
        } catch (\InvalidArgumentException $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $finder = new Finder();
        $finder->files()->in($outputFolder);
        if ($combine) {
            $response = $this->exportJsonFile($finder);
        } else {
            $response = $this->exportZipFile($finder, $outputFolder);
        }
        $this->filesystem->remove($outputFolder);

        return $response;
    }

    private function exportJsonFile(Finder $finder): Response
    {
        $iterator = $finder->getIterator();
        $iterator->rewind();
        if ($iterator->valid()) {
            $filePath = $iterator->current()->getPathname();
            $content = file_get_contents($filePath);

            return new JsonResponse(json_decode($content, true));
        } else {
            return new JsonResponse(['error' => 'No files found'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function exportZipFile(Finder $finder, string $outputFolder): Response
    {
        $zip = new \ZipArchive();
        $zipFile = $outputFolder . DIRECTORY_SEPARATOR . 'export.zip';
        if (true === $zip->open($zipFile, \ZipArchive::CREATE)) {
            foreach ($finder as $file) {
                $zip->addFile($file->getPathname(), $file->getFilename());
            }
            $zip->close();
        }

        return new Response(file_get_contents($zipFile), Response::HTTP_OK, [
            'Content-Type' => 'application/zip',
            'Content-Disposition' => 'attachment; filename="export.zip"',
        ]);
    }
}
