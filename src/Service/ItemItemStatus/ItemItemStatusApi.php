<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemItemStatus;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus\ItemStatusAvailability;
use Redhotmagma\ConfiguratorApiBundle\Structure\Connector\ItemStatus as ConnectorItemStatusStructure;

class ItemItemStatusApi
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemStatusAvailability
     */
    private $itemStatusAvailability;

    /**
     * ItemStatusApi constructor.
     *
     * @param ItemRepository $itemRepository
     * @param ItemStatusAvailability $itemStatusAvailability
     */
    public function __construct(
        ItemRepository $itemRepository,
        ItemStatusAvailability $itemStatusAvailability
    ) {
        $this->itemRepository = $itemRepository;
        $this->itemStatusAvailability = $itemStatusAvailability;
    }

    /**
     * @param string $itemIdentifiers
     *
     * @return ConnectorItemStatusStructure[]
     */
    public function getConnectorListResult(string $itemIdentifiers): array
    {
        $identifiers = array_filter(explode(',', $itemIdentifiers), 'strlen');

        $items = $this->itemRepository->findByIdentifiersIndexed($identifiers);

        $result = [];

        foreach ($identifiers as $identifier) {
            $status = new ConnectorItemStatusStructure();
            $status->itemIdentifier = $identifier;
            $status->configurable = false;

            if (isset($items[$identifier])) {
                $status->configurable = $this->itemStatusAvailability->checkItemAvailability($items[$identifier]);
                $result[] = $status;
            }
        }

        return $result;
    }
}
