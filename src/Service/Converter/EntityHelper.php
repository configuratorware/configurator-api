<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Converter;

use Doctrine\Common\Annotations\AnnotationReader;
use Redhotmagma\ApiBundle\Service\Converter\EntityHelperInterface;
use Redhotmagma\ApiBundle\Service\Helper\StringHelper;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;

/**
 * @internal
 */
class EntityHelper extends \Redhotmagma\ApiBundle\Service\Converter\EntityHelper implements EntityHelperInterface
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    public function __construct(
        StringHelper $stringHelper,
        LanguageRepository $languageRepository
    ) {
        parent::__construct($stringHelper);

        $this->languageRepository = $languageRepository;
    }

    /**
     * if the property is language, set the relation automatically.
     *
     * @param object $entity
     * @param string $property
     * @param mixed $value
     *
     * @return object
     *
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function updateEntityProperty($entity, $property, $value)
    {
        $setter = 'set' . ucfirst($this->stringHelper->underscoresToCamelCase($property));

        // Get the a targetEntity of Setter if property reflects a relation
        $targetEntity = null;
        $reflectionProperty = array_filter(
            (new \ReflectionClass(get_class($entity)))->getProperties(),
            function ($reflectionProperties) use ($property) {
                return $reflectionProperties->name === $property;
            }
        );
        if (!empty($reflectionProperty)) {
            $annotationReader = new AnnotationReader();
            $propertyAnnotation = $annotationReader->getPropertyAnnotations(reset($reflectionProperty));
            $targetEntity = $propertyAnnotation[0]->targetEntity ?? null;
        }

        // Set value to $entity
        if (method_exists($entity, $setter)) {
            if ('language' == $property) {
                $language = $this->languageRepository->findOneByIso($value);
                $entity->setLanguage($language);
            } elseif (null !== $targetEntity) {
                if ($value instanceof $targetEntity) {
                    $entity->$setter($value);
                }
            } else {
                $entity->$setter($value);
            }
        }

        return $entity;
    }
}
