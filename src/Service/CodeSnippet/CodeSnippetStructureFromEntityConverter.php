<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\CodeSnippet;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Channel\ChannelStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet;

/**
 * @internal
 */
class CodeSnippetStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var ChannelStructureFromEntityConverter
     */
    private $channelStructureFromEntityConverter;

    /**
     * @var ClientStructureFromEntityConverter
     */
    private $clientStructureFromEntityConverter;

    /**
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param ChannelStructureFromEntityConverter $channelStructureFromEntityConverter
     * @param ClientStructureFromEntityConverter $clientStructureFromEntityConverter
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        ChannelStructureFromEntityConverter $channelStructureFromEntityConverter,
        ClientStructureFromEntityConverter $clientStructureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->channelStructureFromEntityConverter = $channelStructureFromEntityConverter;
        $this->clientStructureFromEntityConverter = $clientStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\CodeSnippet $entity
     * @param string $structureClassName
     *
     * @return CodeSnippet
     */
    public function convertOne($entity, $structureClassName = CodeSnippet::class)
    {
        /** @var CodeSnippet $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        // add channel to structure
        $channel = $entity->getChannel();
        if (null !== $channel) {
            $structure->channel = $this->channelStructureFromEntityConverter->convertOne($channel);
        }

        $client = $entity->getClient();
        if (null !== $client) {
            $structure->client = $this->clientStructureFromEntityConverter->convertOne($client);
        }

        $language = $entity->getLanguage();
        if (null !== $language) {
            $structure->language = $language->getIso();
        }

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = CodeSnippet::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
