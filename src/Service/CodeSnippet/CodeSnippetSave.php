<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\CodeSnippet;

use Redhotmagma\ConfiguratorApiBundle\Entity\Codesnippet;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CodeSnippetRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientUserCheck;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

/**
 * @internal
 */
class CodeSnippetSave
{
    /**
     * @var ClientUserCheck
     */
    private $clientUserCheck;

    /**
     * @var CodeSnippetRepository
     */
    private $codeSnippetRepository;

    /**
     * @var CodeSnippetEntityFromStructureConverter
     */
    private $codeSnippetEntityFromStructureConverter;

    /**
     * @var CodeSnippetStructureFromEntityConverter
     */
    private $codeSnippetStructureFromEntityConverter;

    /**
     * @var Security
     */
    private $security;

    /**
     * @param ClientUserCheck $clientUserCheck
     * @param CodeSnippetRepository $codeSnippetRepository
     * @param CodeSnippetEntityFromStructureConverter $codeSnippetEntityFromStructureConverter
     * @param CodeSnippetStructureFromEntityConverter $codeSnippetStructureFromEntityConverter
     * @param Security $security
     */
    public function __construct(
        ClientUserCheck $clientUserCheck,
        CodeSnippetRepository $codeSnippetRepository,
        CodeSnippetEntityFromStructureConverter $codeSnippetEntityFromStructureConverter,
        CodeSnippetStructureFromEntityConverter $codeSnippetStructureFromEntityConverter,
        Security $security
    ) {
        $this->clientUserCheck = $clientUserCheck;
        $this->codeSnippetRepository = $codeSnippetRepository;
        $this->codeSnippetEntityFromStructureConverter = $codeSnippetEntityFromStructureConverter;
        $this->codeSnippetStructureFromEntityConverter = $codeSnippetStructureFromEntityConverter;
        $this->security = $security;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet $codeSnippet
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet $codeSnippet)
    {
        $entity = null;

        if (isset($codeSnippet->id) && $codeSnippet->id > 0) {
            $entity = $this->codeSnippetRepository->findOneBy(['id' => $codeSnippet->id]);

            // client user cannot change existing codeSnippets if not related to it's client
            if (null === $entity->getClient() && $this->clientUserCheck->currentUserHasClientRole()) {
                throw new NotFoundException();
            }
        }

        $entity = $this->codeSnippetEntityFromStructureConverter->convertOne($codeSnippet, $entity);

        if ($this->clientUserCheck->currentUserHasClientRole()) {
            if (null === $entity->getClient()) {
                $user = $this->security->getUser();
                if (!$user instanceof User) {
                    throw new AuthenticationException();
                }

                $firsClientUser = $user->getClientUser()->first();
                if (false === $firsClientUser) {
                    throw new NotFoundException();
                }

                $entity->setClient($firsClientUser->getClient());
            }
            $this->clientUserCheck->checkClient($entity->getClient());
        }

        $entity = $this->codeSnippetRepository->save($entity);

        /** @var Codesnippet $entity */
        $entity = $this->codeSnippetRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->codeSnippetStructureFromEntityConverter->convertOne(
            $entity,
            \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet::class
        );

        return $structure;
    }
}
