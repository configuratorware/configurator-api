<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\CodeSnippet;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Codesnippet;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Channel as ChannelStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Client as ClientStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet as CodeSnippetStructure;

/**
 * @internal
 */
class CodeSnippetEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param ChannelRepository $channelRepository
     * @param ClientRepository $clientRepository
     */
    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        ChannelRepository $channelRepository,
        ClientRepository $clientRepository
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->channelRepository = $channelRepository;
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param CodeSnippetStructure $structure
     * @param Codesnippet $entity
     * @param string $entityClassName
     *
     * @return Codesnippet
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = Codesnippet::class
    ): Codesnippet {
        /** @var CodeSnippet $entity */
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        if ($structure->channel instanceof ChannelStructure) {
            $channelEntity = $this->channelRepository->findOneBy(['id' => $structure->channel->id]);
        }
        $entity->setChannel($channelEntity ?? null);

        if ($structure->client instanceof ClientStructure) {
            $clientEntity = $this->clientRepository->findOneBy(['id' => $structure->client->id]);
        }
        $entity->setClient($clientEntity ?? null);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = CodeSnippetStructure::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }
}
