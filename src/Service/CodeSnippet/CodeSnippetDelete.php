<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\CodeSnippet;

use Redhotmagma\ConfiguratorApiBundle\Entity\CodeSnippet;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CodeSnippetRepository;

/**
 * @internal
 */
class CodeSnippetDelete
{
    /**
     * @var CodeSnippetRepository
     */
    private $codeSnippetRepository;

    /**
     * CodeSnippetDelete constructor.
     *
     * @param CodeSnippetRepository $codeSnippetRepository
     */
    public function __construct(CodeSnippetRepository $codeSnippetRepository)
    {
        $this->codeSnippetRepository = $codeSnippetRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var CodeSnippet $entity */
            $entity = $this->codeSnippetRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            $this->codeSnippetRepository->delete($entity);
        }
    }
}
