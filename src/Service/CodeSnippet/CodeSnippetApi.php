<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\CodeSnippet;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\CodeSnippet;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CodeSnippetRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientUserCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Bundle\SecurityBundle\Security;

class CodeSnippetApi
{
    /**
     * @var ClientUserCheck
     */
    private $clientUserCheck;

    /**
     * @var CodeSnippetRepository
     */
    private $codeSnippetRepository;

    /**
     * @var CodeSnippetStructureFromEntityConverter
     */
    private $codeSnippetStructureFromEntityConverter;

    /**
     * @var CodeSnippetSave
     */
    private $codeSnippetSave;

    /**
     * @var CodeSnippetDelete
     */
    private $codeSnippetDelete;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @param ClientUserCheck $clientUserCheck
     * @param CodeSnippetRepository $codeSnippetRepository
     * @param CodeSnippetStructureFromEntityConverter $codeSnippetStructureFromEntityConverter
     * @param CodeSnippetSave $codeSnippetSave
     * @param CodeSnippetDelete $codeSnippetDelete
     * @param Security $security
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        ClientUserCheck $clientUserCheck,
        CodeSnippetRepository $codeSnippetRepository,
        CodeSnippetStructureFromEntityConverter $codeSnippetStructureFromEntityConverter,
        CodeSnippetSave $codeSnippetSave,
        CodeSnippetDelete $codeSnippetDelete,
        Security $security,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->clientUserCheck = $clientUserCheck;
        $this->codeSnippetRepository = $codeSnippetRepository;
        $this->codeSnippetStructureFromEntityConverter = $codeSnippetStructureFromEntityConverter;
        $this->codeSnippetSave = $codeSnippetSave;
        $this->codeSnippetDelete = $codeSnippetDelete;
        $this->security = $security;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        if ($this->clientUserCheck->currentUserHasClientRole()) {
            $arguments->filters['_client_user_filter'] = $this->security->getUser();
        }

        $entities = $this->codeSnippetRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->codeSnippetStructureFromEntityConverter->convertMany(
            $entities,
            \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet::class
        );

        $paginationResult->count = $this->codeSnippetRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet
    {
        $entity = $this->codeSnippetRepository->findOneBy(['id' => $id]);

        if (!$entity instanceof CodeSnippet) {
            throw new NotFoundException();
        }

        $client = $entity->getClient();

        if (null === $client && $this->clientUserCheck->currentUserHasClientRole()) {
            throw new NotFoundException();
        }

        if ($client instanceof Client) {
            $this->clientUserCheck->checkClient($client);
        }

        $structure = $this->codeSnippetStructureFromEntityConverter->convertOne(
            $entity,
            \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet::class
        );

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet $codeSnippet
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\CodeSnippet $codeSnippet)
    {
        $structure = $this->codeSnippetSave->save($codeSnippet);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->codeSnippetDelete->delete($id);
    }

    /**
     * @return array
     */
    public function findCurrent()
    {
        $result = [];

        $codeSnippets = $this->codeSnippetRepository->findCurrent();

        if (!empty($codeSnippets)) {
            $result = $this->structureFromEntityConverter->convertMany(
                $codeSnippets,
                \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Codesnippet::class
            );
        }

        return $result;
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier'];
    }
}
