<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Exception\ChangeUserPasswordException;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChangeUserPassword;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @internal
 */
class ChangePassword
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    /**
     * ChangePassword constructor.
     *
     * @param UserRepository $userRepository
     * @param UserPasswordHasherInterface $passwordHasher
     */
    public function __construct(UserRepository $userRepository, UserPasswordHasherInterface $passwordHasher)
    {
        $this->userRepository = $userRepository;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @param ChangeUserPassword $userPassword
     * @param User $user
     *
     * @throws ChangeUserPasswordException
     */
    public function changePassword(ChangeUserPassword $userPassword, User $user)
    {
        if (empty($userPassword->newPassword)) {
            throw ChangeUserPasswordException::newPasswordEmpty();
        }

        if (!$this->passwordHasher->isPasswordValid($user, $userPassword->oldPassword)) {
            throw ChangeUserPasswordException::oldPasswordInvalid();
        }

        $user->setPassword($userPassword->newPassword, $this->passwordHasher);
        $this->userRepository->save($user);
    }
}
