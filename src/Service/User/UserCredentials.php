<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @internal
 */
class UserCredentials
{
    private const LICENCE_REQUIRED_FEATURES = ['clients'];

    /**
     * @var LicenseFileValidator
     */
    private $licenseFileValidator;

    /**
     * @param LicenseFileValidator $licenseFileValidator
     */
    public function __construct(LicenseFileValidator $licenseFileValidator)
    {
        $this->licenseFileValidator = $licenseFileValidator;
    }

    /**
     * @param UserInterface $user
     *
     * @return array
     */
    public function getByUser(UserInterface $user): array
    {
        $credentials = [];

        if (!method_exists($user, 'getUserRole')) {
            return [];
        }

        foreach ($user->getUserRole() as $userRole) {
            $role = $userRole->getRole();
            if (null === $role) {
                continue;
            }

            foreach ($role->getRoleCredential() as $roleCredential) {
                $credentials[] = $roleCredential->getCredential()->getArea();
            }
        }

        return $this->filterByLicenseFeature($credentials);
    }

    /**
     * @param array $credentials
     *
     * @return array
     */
    private function filterByLicenseFeature(array $credentials): array
    {
        $licencedFeatures = $this->licenseFileValidator->getFeatures();

        return array_values(array_diff($credentials, array_diff(self::LICENCE_REQUIRED_FEATURES, $licencedFeatures)));
    }
}
