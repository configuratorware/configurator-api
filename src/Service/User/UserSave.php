<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Role\RoleStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Role as RoleStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\User as UserStructure;

/**
 * @internal
 */
class UserSave
{
    public const RANDOM_PASSWORD_SIZE = 12;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var UserEntityFromStructureConverter
     */
    private $userEntityFromStructureConverter;

    /**
     * @var UserStructureFromEntityConverter
     */
    private $userStructureFromEntityConverter;

    /**
     * @var RoleStructureFromEntityConverter
     */
    private $roleStructureFromEntityConverter;

    /**
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     * @param UserEntityFromStructureConverter $userEntityFromStructureConverter
     * @param UserStructureFromEntityConverter $userStructureFromEntityConverter
     * @param RoleStructureFromEntityConverter $roleStructureFromEntityConverter
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository,
        UserEntityFromStructureConverter $userEntityFromStructureConverter,
        UserStructureFromEntityConverter $userStructureFromEntityConverter,
        RoleStructureFromEntityConverter $roleStructureFromEntityConverter
    ) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->userEntityFromStructureConverter = $userEntityFromStructureConverter;
        $this->userStructureFromEntityConverter = $userStructureFromEntityConverter;
        $this->roleStructureFromEntityConverter = $roleStructureFromEntityConverter;
    }

    /**
     * @param UserStructure $user
     *
     * @return UserStructure
     */
    public function save(UserStructure $user)
    {
        $entity = null;
        if (isset($user->id) && $user->id > 0) {
            $entity = $this->userRepository->findOneBy(['id' => $user->id]);
        }

        $entity = $this->userEntityFromStructureConverter->convertOne($user, $entity);
        $entity = $this->userRepository->save($entity);

        /** @var User $entity */
        $entity = $this->userRepository->findOneBy(['id' => $entity->getId()]);

        return $this->userStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @param UserStructure $user
     *
     * @return string
     *
     * @throws \Exception
     */
    public function saveAdminUser(UserStructure $user): string
    {
        $user->password = $user->password ?: $this->generateRandomPassword();
        $user->active = true;

        /** @var User $entity */
        $entity = $this->userRepository->findOneBy(['username' => $user->username]);

        if (null !== $entity) {
            foreach ($entity->getUserRole() as $userRole) {
                $role = $userRole->getRole();
                if (null === $role) {
                    continue;
                }

                $user->roles[] = $this->roleStructureFromEntityConverter->convertOne($role);
            }
        } else {
            $roleEntity = $this->roleRepository->findOneBy(['role' => Role::ADMIN_ROLE]);
            $roleStructure = new RoleStructure();
            $roleStructure->id = $roleEntity->getId();
            $user->roles[] = $roleStructure;
        }

        $entity = $this->userEntityFromStructureConverter->convertOne($user, $entity);

        $this->userRepository->save($entity);

        return $user->password;
    }

    /**
     * @return false|string
     */
    private function generateRandomPassword()
    {
        return substr(md5(uniqid(mt_rand(), true)), 0, self::RANDOM_PASSWORD_SIZE);
    }
}
