<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Role;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;

/**
 * @internal
 */
class UserStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\User $entity
     * @param string $structureClassName
     *
     * @return User
     */
    public function convertOne($entity, $structureClassName = User::class)
    {
        /** @var User $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure->password = null;

        $roles = [];
        foreach ($entity->getUserRole() as $userRole) {
            $roleEntity = $userRole->getRole();

            if (null === $roleEntity) {
                continue;
            }

            $role = new Role();
            $role->id = $roleEntity->getId();
            $role->name = $roleEntity->getName();

            $roles[] = $role;
        }

        $structure->roles = $roles;

        $structure->active = $entity->getIsActive();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = User::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
