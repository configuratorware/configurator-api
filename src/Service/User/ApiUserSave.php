<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Entity\UserRole;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ApiUser  as ApiUserStructure;

/**
 * @internal
 */
class ApiUserSave
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     */
    public function __construct(
        UserRepository $userRepository,
        RoleRepository $roleRepository
    ) {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param ApiUserStructure $apiUser
     *
     * @return ApiUserStructure
     *
     * @throws \Exception
     */
    public function saveApiUser(ApiUserStructure $apiUser): ApiUserStructure
    {
        if ($apiUser->id > 0) {
            $entity = $this->userRepository->find($apiUser->id);
        } else {
            $entity = (new User())
                ->setApiKey($this->generateApiKey())
                ->addUserRole($this->createConnectorUserRole());
        }

        $entity->setUsername($apiUser->username);
        $entity->setEmail($apiUser->email);
        $entity->setIsActive((bool)$apiUser->active);

        $this->userRepository->save($entity);

        $apiUser->id = $entity->getId();
        $apiUser->api_key = $entity->getApiKey();

        return $apiUser;
    }

    /**
     * @param int $id
     */
    public function refreshKey(int $id): void
    {
        $userEntity = $this->userRepository->find($id);
        if (null === $userEntity) {
            throw new NotFoundException();
        }

        // check for key collision
        $apiKey = $this->generateApiKey();
        while (null !== $this->userRepository->findOneBy(['api_key' => $apiKey])) {
            $apiKey = $this->generateApiKey();
        }

        $userEntity->setApiKey($apiKey);
        $this->userRepository->save($userEntity);
    }

    /**
     * @return string
     */
    private function generateApiKey(): string
    {
        $hash = '';
        for ($i = 0; $i < 3; ++$i) {
            $hash .= uniqid('');
        }

        $pos = rand(0, strlen($hash));

        return substr($hash, $pos) . substr($hash, 0, $pos);
    }

    /**
     * @return UserRole
     *
     * @throws \Exception
     */
    private function createConnectorUserRole(): UserRole
    {
        $role = $this->roleRepository->findOneByRole(Role::CONNECTOR_ROLE);

        if (null === $role) {
            throw new \Exception('Connector role is not found in database');
        }

        return UserRole::forRole($role);
    }
}
