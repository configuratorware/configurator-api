<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;

/**
 * @internal
 */
class UserDelete
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            $entity = $this->userRepository->find($id);

            if (null === $entity) {
                throw new NotFoundException();
            }

            foreach ($entity->getUserRole() as $userRole) {
                $entity->removeUserRole($userRole);
            }

            foreach ($entity->getClientUser() as $clientUser) {
                $entity->removeClientUser($clientUser);
            }

            $this->userRepository->delete($entity);
        }
    }
}
