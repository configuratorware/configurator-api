<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Entity\UserRole;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\User as UserStructure;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @internal
 */
class UserEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        UserPasswordHasherInterface $passwordHasher,
        RoleRepository $roleRepository
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->passwordHasher = $passwordHasher;
        $this->roleRepository = $roleRepository;
    }

    /**
     * @param UserStructure $structure
     * @param User|null $entity
     * @param string $entityClassName
     *
     * @return User
     */
    public function convertOne($structure, $entity = null, $entityClassName = User::class)
    {
        if (null === $entity) {
            $entity = new User();
        }

        $entity->setUsername($structure->username ?? $structure->email)
            ->setEmail($structure->email)
            ->setIsActive($structure->active)
            ->setFirstname($structure->firstname)
            ->setLastname($structure->lastname);

        // encode password if not empty
        if (!empty($structure->password)) {
            $entity->setPassword($structure->password, $this->passwordHasher);
        }

        $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
            $structure,
            $entity,
            'UserRole',
            'roles',
            'Role'
        );

        $this->entityFromStructureConverter->addNewManyToManyRelations(
            $structure,
            $entity,
            UserRole::class,
            'roles',
            'Role',
            $this->roleRepository
        );

        return $entity;
    }

    /**
     * @param User[] $entities
     * @param string $structureClassName
     *
     * @return User[]
     */
    public function convertMany($entities, $structureClassName = User::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }
}
