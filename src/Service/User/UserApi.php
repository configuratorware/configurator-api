<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\User;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ApiUser;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChangeUserPassword;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\User as UserStructure;

class UserApi
{
    /**
     * @var UserDelete
     */
    private $userDelete;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserSave
     */
    private $userSave;

    /**
     * @var UserStructureFromEntityConverter
     */
    private $userStructureFromEntityConverter;

    /**
     * @var ApiUserSave
     */
    private $apiUserSave;

    /**
     * @var ChangePassword
     */
    private $changePassword;

    /**
     * @param UserDelete $userDelete
     * @param UserRepository $userRepository
     * @param UserSave $userSave
     * @param UserStructureFromEntityConverter $userStructureFromEntityConverter
     * @param ApiUserSave $apiUserSave
     * @param ChangePassword $changePassword
     */
    public function __construct(
        UserDelete $userDelete,
        UserRepository $userRepository,
        UserSave $userSave,
        UserStructureFromEntityConverter $userStructureFromEntityConverter,
        ApiUserSave $apiUserSave,
        ChangePassword $changePassword
    ) {
        $this->userDelete = $userDelete;
        $this->userRepository = $userRepository;
        $this->userSave = $userSave;
        $this->userStructureFromEntityConverter = $userStructureFromEntityConverter;
        $this->apiUserSave = $apiUserSave;
        $this->changePassword = $changePassword;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->userRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->userStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->userRepository->fetchListCount($arguments, $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return UserStructure
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): UserStructure
    {
        /** @var User $entity */
        $entity = $this->userRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        return $this->userStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @param UserStructure $user
     *
     * @return UserStructure
     */
    public function save(UserStructure $user): UserStructure
    {
        return $this->userSave->save($user);
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $this->userDelete->delete($id);
    }

    /**
     * @param ChangeUserPassword $userPassword
     * @param User $user
     */
    public function changePassword(ChangeUserPassword $userPassword, User $user): void
    {
        $this->changePassword->changePassword($userPassword, $user);
    }

    /**
     * @param UserStructure $user
     *
     * @return string
     *
     * @throws Exception
     */
    public function createAdminUser(UserStructure $user): string
    {
        return $this->userSave->saveAdminUser($user);
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getApiUserListResult(ListRequestArguments $arguments): PaginationResult
    {
        $arguments->filters['_apiuser'] = true;

        $entities = $this->userRepository->fetchList($arguments, $this->getSearchableFields());
        $data = [];
        foreach ($entities as $entity) {
            $data[] = ApiUser::from($entity);
        }

        $count = $this->userRepository->fetchListCount($arguments, $this->getSearchableFields());

        return new PaginationResult($data, $count);
    }

    /**
     * @param int $id
     *
     * @return ApiUser
     */
    public function getApiUser(int $id): ApiUser
    {
        $entity = $this->userRepository->find($id);

        if (null === $entity) {
            throw NotFoundException::entityWithId(User::class, $id);
        }

        return ApiUser::from($entity);
    }

    /**
     * @param ApiUser $apiUser
     *
     * @return ApiUser
     *
     * @throws Exception
     */
    public function saveApiUser(ApiUser $apiUser): ApiUser
    {
        return $this->apiUserSave->saveApiUser($apiUser);
    }

    /**
     * @param int $id
     */
    public function refreshKey(int $id): void
    {
        $this->apiUserSave->refreshKey($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['username', 'email'];
    }
}
