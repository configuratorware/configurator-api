<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Setting;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting as SettingEntity;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;
use Redhotmagma\ConfiguratorApiBundle\Structure\Setting;

/**
 * @internal
 */
class SettingStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var LicenseFileValidator
     */
    private $licenseFileValidator;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter, LicenseFileValidator $licenseFileValidator)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->licenseFileValidator = $licenseFileValidator;
    }

    /**
     * @param SettingEntity $entity
     * @param string $structureclassname
     *
     * @return Setting
     */
    public function convertOne($entity, $structureclassname = Setting::class)
    {
        /** @var Setting $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureclassname);

        if (false === $this->licenseFileValidator->hasClientsFeature()) {
            unset($structure->configurationsClientRestricted);
        }

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureclassname
     *
     * @return array
     */
    public function convertMany($entities, $structureclassname = Setting::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureclassname);
            $structures[] = $structure;
        }

        return $structures;
    }
}
