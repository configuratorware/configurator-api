<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Setting;

use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;

/**
 * @internal
 */
class ShareUrlResolver
{
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * ShareUrlResolver constructor.
     *
     * @param SettingRepository $settingRepository
     */
    public function __construct(SettingRepository $settingRepository)
    {
        $this->settingRepository = $settingRepository;
    }

    /**
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     *
     * @throws NotFoundException
     *
     * @return string
     */
    public function getShareUrl(string $configurationCode, ControlParameters $controlParameters): string
    {
        $shareUrl = $controlParameters->getParameter(ControlParameters::SHARE_URL);
        if (null !== $shareUrl) {
            return $this->replaceConfigurationCode($configurationCode, $shareUrl);
        }
        $setting = $this->settingRepository->getSetting();
        if (null === $setting) {
            throw new NotFoundException(Setting::class);
        }
        $shareUrl = $setting->getShareurl();
        $shareUrl = $this->replaceConfigurationCode($configurationCode, $shareUrl);

        return $controlParameters->mergeGetParametersToUrl($shareUrl);
    }

    /**
     * @param string $configurationCode
     * @param string $shareUrl
     *
     * @return string
     */
    private function replaceConfigurationCode(string $configurationCode, string $shareUrl): string
    {
        return str_replace('{code}', $configurationCode, $shareUrl);
    }
}
