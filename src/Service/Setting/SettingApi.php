<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Setting;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Setting;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Setting as SettingStructure;

class SettingApi
{
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var SettingStructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * SettingApi constructor.
     *
     * @param SettingRepository $settingRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param SettingStructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        SettingRepository $settingRepository,
        EntityFromStructureConverter $entityFromStructureConverter,
        SettingStructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->settingRepository = $settingRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param int $id
     *
     * @return SettingStructure
     */
    public function getOne(int $id): SettingStructure
    {
        $entity = $this->settingRepository->findOneBy(['id' => $id]);

        if (null === $entity) {
            throw new NotFoundException();
        }

        $settings = $this->structureFromEntityConverter->convertOne($entity);

        // convert zoom factor to %
        $settings->maxZoom2d = $settings->maxZoom2d * 100;

        return $settings;
    }

    /**
     * @param SettingStructure $structure
     *
     * @return bool
     */
    public function save(SettingStructure $structure): bool
    {
        // convert % to zoom factor
        $structure->maxZoom2d = $structure->maxZoom2d / 100;
        $entity = $this->settingRepository->findOneBy(['id' => $structure->id]);

        $this->entityFromStructureConverter->convertOne($structure, $entity, Setting::class);

        $this->settingRepository->save($entity);

        return true;
    }

    public function frontendGetMaxZoom2d(): float
    {
        /** @var Setting $setting */
        $setting = current($this->settingRepository->findAll());

        return $setting->getMaxZoom2d();
    }
}
