<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\PrintFile;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationLoad;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\PrintFile;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * @internal
 */
class PrintFileGenerator
{
    /**
     * @var ConfigurationLoad
     */
    private $configurationLoad;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $printFileGenerator;

    /**
     * @var string
     */
    private $printFileLocation;

    /**
     * @var ProcessFactory
     */
    private $processFactory;

    /**
     * PrintFileGenerator constructor.
     *
     * @param ConfigurationLoad $configurationLoad
     * @param ConfigurationRepository $configurationRepository
     * @param Filesystem $fileSystem
     * @param string $printFileGenerator
     * @param string $printFileLocation
     * @param ProcessFactory $processFactory
     */
    public function __construct(
        ConfigurationLoad $configurationLoad,
        ConfigurationRepository $configurationRepository,
        Filesystem $fileSystem,
        string $printFileGenerator,
        string $printFileLocation,
        ProcessFactory $processFactory
    ) {
        $this->configurationLoad = $configurationLoad;
        $this->configurationRepository = $configurationRepository;
        $this->fileSystem = $fileSystem;
        $this->printFileGenerator = $printFileGenerator;
        $this->printFileLocation = $printFileLocation;
        $this->processFactory = $processFactory;
    }

    /**
     * @param string $configurationCode
     *
     * @return PrintFile[]
     */
    public function getList(string $configurationCode): array
    {
        $imagePaths = $this->generate($configurationCode);

        $response = $this->createResponse($imagePaths);

        return $response;
    }

    /**
     * @param string $configurationCode
     *
     * @return array
     */
    public function generate(string $configurationCode): array
    {
        $frontendConfiguration = $this->getFrontendConfiguration($configurationCode);

        $imagePaths = $this->createImagePaths($configurationCode, $frontendConfiguration);

        $isProcessed = $this->handleProcessRun($configurationCode, $imagePaths, $frontendConfiguration);

        if (!$isProcessed) {
            throw new Exception('Unable to process Print Images.');
        }

        return $imagePaths;
    }

    /**
     * @param string $configurationCode
     * @param FrontendConfigurationStructure $frontendConfiguration
     *
     * @return array
     */
    private function createImagePaths(
        string $configurationCode,
        FrontendConfigurationStructure $frontendConfiguration
    ): array {
        $imagePaths = [];
        $baseDir = sys_get_temp_dir();
        if ('' !== $this->printFileLocation) {
            $baseDir = $this->printFileLocation;
        }

        $dir = rtrim($baseDir, '/') . '/print_file_' . $configurationCode;

        if (!empty($frontendConfiguration->designdata)) {
            if (!is_dir($dir)) {
                $this->fileSystem->mkdir($dir);
            }

            foreach ((array)$frontendConfiguration->designdata as $designAreaIdentifier => $designArea) {
                $imagePaths[$designAreaIdentifier] = $dir . '/' . $designAreaIdentifier . '.png';
            }
        }

        return $imagePaths;
    }

    /**
     * @param string $configurationCode
     *
     * @return FrontendConfigurationStructure
     */
    private function getFrontendConfiguration(string $configurationCode): FrontendConfigurationStructure
    {
        $controlParameters = new ControlParameters([], true);

        return $this->configurationLoad->loadByCode($configurationCode, $controlParameters);
    }

    /**
     * @param string $configurationCode
     * @param array $imagePaths
     * @param FrontendConfigurationStructure $frontendConfiguration
     *
     * @return bool
     */
    private function handleProcessRun(
        string $configurationCode,
        array $imagePaths,
        FrontendConfigurationStructure $frontendConfiguration
    ): bool {
        $configuration = $this->configurationRepository->findOneBy(['code' => $configurationCode]);

        // figure out, which files do not exist or are not up to date
        $imagesToProcess = [];

        foreach ($imagePaths as $designAreaIdentifier => $imagePath) {
            if (!file_exists($imagePath) || $this->isImageOutdated($imagePath, $configuration)) {
                $imagesToProcess[$designAreaIdentifier] = $imagePath;
            }
        }

        // Run the Process for non existent images
        if (!empty($imagesToProcess)) {
            $this->runProcess($imagesToProcess, $frontendConfiguration);
        }

        // check, if all files were generated properly
        $allFilesExist = false;
        foreach ($imagePaths as $imagePath) {
            $allFilesExist = true;
            if (!file_exists($imagePath) || $this->isImageOutdated($imagePath, $configuration)) {
                $allFilesExist = false;

                break;
            }
        }

        // if at least one image was not generated, run the process again
        if (!$allFilesExist) {
            $this->runProcess($imagePaths, $frontendConfiguration);
            // check again
            foreach ($imagePaths as $imagePath) {
                if (!file_exists($imagePath) || $this->isImageOutdated($imagePath, $configuration)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param array $imagePaths
     * @param FrontendConfigurationStructure $frontendConfiguration
     */
    private function runProcess(
        array $imagePaths,
        FrontendConfigurationStructure $frontendConfiguration
    ) {
        $process = $this->processFactory::createWithDefaults([
            'node',
            $this->printFileGenerator,
            '--imagePaths="' . json_encode($imagePaths) . '"',
            '--configuration="' . json_encode($frontendConfiguration) . '"',
        ]);

        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }

    /**
     * @param array $imagePaths
     *
     * @return PrintFile[]
     */
    private function createResponse(array $imagePaths): array
    {
        $response = [];

        foreach ($imagePaths as $designAreaIdentifier => $imagePath) {
            $printFile = new PrintFile();
            $printFile->designAreaIdentifier = $designAreaIdentifier;
            $printFile->printFilePath = $imagePath;
            $response[] = $printFile;
        }

        return $response;
    }

    /**
     * @param string $imagePath
     * @param Configuration $configuration
     *
     * @return bool
     */
    private function isImageOutdated(string $imagePath, Configuration $configuration): bool
    {
        if (file_exists($imagePath) &&
            null !== $configuration->getDateUpdated() &&
            $configuration->getDateUpdated()->getTimestamp() > filemtime($imagePath)
        ) {
            return true;
        }

        return false;
    }
}
