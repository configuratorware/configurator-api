<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\PrintFile;

use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;

/**
 * @internal
 */
class PrintFileSave
{
    /**
     * @var MediaHelper
     */
    private $mediaHelper;

    /**
     * PrintFileSave constructor.
     *
     * @param MediaHelper $mediaHelper
     */
    public function __construct(MediaHelper $mediaHelper)
    {
        $this->mediaHelper = $mediaHelper;
    }

    /**
     * @param array $printFiles
     * @param string $configurationCode
     *
     * @return string[]
     */
    public function save($printFiles, $configurationCode)
    {
        $imageDir = '/printfiles';

        $imagePath = $this->mediaHelper->getImageBasePath() . $imageDir;

        if (!is_dir($imagePath)) {
            mkdir($imagePath);
        }

        $imageNames = [];
        foreach ($printFiles as $name => $printFile) {
            if ('default' === $name) {
                $imageName = $configurationCode . '.png';
            } else {
                $imageName = $configurationCode . '_' . $name . '.png';
            }

            $printFile = str_replace('data:image/png;base64,', '', $printFile);
            $printFile = str_replace(' ', '+', $printFile);
            $printFile = base64_decode($printFile);

            file_put_contents($imagePath . '/' . $imageName, $printFile);

            $imageNames[] = $imageName;
        }

        return $imageNames;
    }

    /**
     * @param $printFiles
     * @param $configurationCode
     *
     * @return array
     */
    public function savePrintDesignAreas($printFiles, $configurationCode)
    {
        $imageDir = '/printfiles';

        $imagePath = $this->mediaHelper->getImageBasePath() . $imageDir;

        if (!is_dir($imagePath)) {
            mkdir($imagePath);
        }

        $imageNames = [];
        foreach ($printFiles as $name => $printFile) {
            $imageName = $configurationCode . '_' . $name . '.svg';
            file_put_contents($imagePath . '/' . $imageName, $printFile);

            $imageNames[] = $imageName;
        }

        return $imageNames;
    }
}
