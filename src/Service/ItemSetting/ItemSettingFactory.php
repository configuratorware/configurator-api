<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemSetting;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemSettingRelation;

/**
 * @internal
 */
class ItemSettingFactory
{
    /**
     * @param Item $item
     *
     * @return ItemSettingRelation
     */
    public function createRelationStructure(Item $item): ItemSettingRelation
    {
        // children inherit the visualization mode from their parent
        if (null !== $item->getParent()) {
            $item = $item->getParent();
        }

        $visualizationMode = $item->getVisualizationMode();

        $relationStructure = new ItemSettingRelation();
        $relationStructure->visualizationCreator = null !== $visualizationMode ? $visualizationMode->getIdentifier() : '';
        $relationStructure->visualizationDesigner = null !== $visualizationMode ? $visualizationMode->getIdentifier() : '';

        return $relationStructure;
    }
}
