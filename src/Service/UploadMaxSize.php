<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service;

/**
 * Get max upload file size
 * (inspired by a Drupal implementation).
 *
 * @internal
 */
class UploadMaxSize
{
    /**
     * Returns a file size limit in bytes based on the PHP upload_max_filesize and post_max_size.
     *
     * @return int
     */
    public function getFileUploadMaxSize(): int
    {
        $maxSize = 0;

        // Start with post_max_size, use it if set
        $postMaxSize = $this->parseSize(ini_get('post_max_size'));
        if ($postMaxSize > 0) {
            $maxSize = $postMaxSize;
        }

        // If upload_max_filesize is less, then reduce. Except if $uploadMaxSize is zero, which indicates no limit.
        $uploadMaxFileSize = $this->parseSize(ini_get('upload_max_filesize'));
        if ($uploadMaxFileSize > 0 && $uploadMaxFileSize < $postMaxSize) {
            $maxSize = $uploadMaxFileSize;
        }

        return $maxSize;
    }

    /**
     * Parse string size to a numeric value.
     *
     * @param string $size
     *
     * @return int
     */
    private function parseSize(string $size): int
    {
        // Remove the non-unit characters from the size.
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);

        // Remove the non-numeric characters from the size.
        $numericSize = preg_replace('/[^0-9\.]/', '', $size);

        if (!empty($unit)) {
            // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
            $numericSize = $numericSize * pow(1024, stripos('bkmgtpezy', $unit[0]));
        }

        return (int)$numericSize;
    }
}
