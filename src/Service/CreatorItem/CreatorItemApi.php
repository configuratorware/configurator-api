<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\CreatorItem;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\AdminAreaItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class CreatorItemApi
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var AdminAreaItemStructureFromEntityConverter
     */
    private $adminAreaItemStructureFromEntityConverter;

    /**
     * CreatorItemApi constructor.
     *
     * @param ItemRepository $itemRepository
     * @param AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter
     */
    public function __construct(
        ItemRepository $itemRepository,
        AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter
    ) {
        $this->itemRepository = $itemRepository;
        $this->adminAreaItemStructureFromEntityConverter = $adminAreaItemStructureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $arguments->filters['_parent_filter'] = 'sub_level_only';
        $entities = $this->itemRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->adminAreaItemStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->itemRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier', 'itemtext.title', 'itemtext.abstract', 'itemtext.description'];
    }
}
