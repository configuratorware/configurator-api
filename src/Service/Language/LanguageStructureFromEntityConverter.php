<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Language;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Structure\Language as LanguageStructure;

/**
 * @internal
 */
class LanguageStructureFromEntityConverter
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Language $entity
     *
     * @return LanguageStructure
     */
    public function convertOne(
        $entity
    ): LanguageStructure {
        $structure = $this->structureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param array $entities
     *
     * @return array
     */
    public function convertMany($entities): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }
}
