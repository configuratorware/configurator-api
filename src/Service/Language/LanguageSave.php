<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Language;

use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Language as LanguageStructure;

/**
 * @internal
 */
class LanguageSave
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var LanguageEntityFromStructureConverter
     */
    private $languageEntityFromStructureConverter;

    /**
     * @var LanguageStructureFromEntityConverter
     */
    private $languageStructureFromEntityConverter;

    /**
     * DesignAreaSave constructor.
     *
     * @param LanguageRepository $languageRepository
     * @param LanguageEntityFromStructureConverter $languageEntityFromStructureConverter
     * @param LanguageStructureFromEntityConverter $languageStructureFromEntityConverter
     */
    public function __construct(
        LanguageRepository $languageRepository,
        LanguageEntityFromStructureConverter $languageEntityFromStructureConverter,
        LanguageStructureFromEntityConverter $languageStructureFromEntityConverter
    ) {
        $this->languageRepository = $languageRepository;
        $this->languageEntityFromStructureConverter = $languageEntityFromStructureConverter;
        $this->languageStructureFromEntityConverter = $languageStructureFromEntityConverter;
    }

    /**
     * @param LanguageStructure $structure
     *
     * @return LanguageStructure
     *
     * @throws \Exception
     */
    public function save(LanguageStructure $structure): LanguageStructure
    {
        $entity = null;

        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->languageRepository->findOneBy(['id' => $structure->id]);
        }

        if (true === $structure->isDefault && true !== $entity->getIsDefault()) {
            $this->resetIsDefaults();
        }

        $entity = $this->languageEntityFromStructureConverter->convertOne($structure, $entity);
        $entity = $this->languageRepository->save($entity);
        $this->languageRepository->clear();

        $entity = $this->languageRepository->findOneBy(['id' => $entity->getId()]);

        return $this->languageStructureFromEntityConverter->convertOne($entity);
    }

    private function resetIsDefaults(): void
    {
        foreach ($this->languageRepository->findAll() as $language) {
            $language->setIsDefault(false);
            $this->languageRepository->save($language);
        }
    }
}
