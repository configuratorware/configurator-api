<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Language;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Structure\Language as LanguageStructure;

/**
 * @internal
 */
class LanguageEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param LanguageStructure $structure
     * @param Language $entity
     * @param string $entityClassName
     *
     * @return Language
     *
     * @throws \Exception
     */
    public function convertOne($structure, $entity = null, $entityClassName = Language::class): Language
    {
        return $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     *
     * @throws \Exception
     */
    public function convertMany($entities, $structureClassName = Language::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }
}
