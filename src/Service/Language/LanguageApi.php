<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Language;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Language as LanguageStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class LanguageApi
{
    /**
     * @var array
     */
    private const SEARCHABLE_FIELDS = ['iso'];

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var FrontendLanguageStructureFromEntityConverter
     */
    private $frontendLanguageStructureFromEntityConverter;

    /**
     * @var LanguageStructureFromEntityConverter
     */
    private $languageStructureFromEntityConverter;

    /**
     * @var LanguageSave
     */
    private $languageSave;

    /**
     * LanguageApi constructor.
     *
     * @param LanguageRepository $languageRepository
     * @param FrontendLanguageStructureFromEntityConverter $frontendLanguageStructureFromEntityConverter
     * @param LanguageSave $languageSave
     * @param LanguageStructureFromEntityConverter $languageStructureFromEntityConverter
     */
    public function __construct(
        LanguageRepository $languageRepository,
        FrontendLanguageStructureFromEntityConverter $frontendLanguageStructureFromEntityConverter,
        LanguageSave $languageSave,
        LanguageStructureFromEntityConverter $languageStructureFromEntityConverter
    ) {
        $this->languageRepository = $languageRepository;
        $this->frontendLanguageStructureFromEntityConverter = $frontendLanguageStructureFromEntityConverter;
        $this->languageSave = $languageSave;
        $this->languageStructureFromEntityConverter = $languageStructureFromEntityConverter;
    }

    /**
     * @return array
     */
    public function getFrontendList(): array
    {
        $entities = $this->languageRepository->findAll();

        $result = $this->frontendLanguageStructureFromEntityConverter->convertMany($entities);

        return $result;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->languageRepository->fetchList(
            $arguments,
            self::SEARCHABLE_FIELDS
        );

        $paginationResult->data = $this->languageStructureFromEntityConverter->convertMany(
            $entities,
            LanguageStructure::class
        );

        $paginationResult->count = $this->languageRepository->fetchListCount(
            $arguments,
            self::SEARCHABLE_FIELDS
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return LanguageStructure
     */
    public function getOne(int $id): LanguageStructure
    {
        $entity = $this->languageRepository->findOneById($id);

        if (!$entity) {
            throw new NotFoundException();
        }

        $structure = $this->languageStructureFromEntityConverter->convertOne(
            $entity,
            LanguageStructure::class
        );

        return $structure;
    }

    /**
     * @param LanguageStructure $structure
     *
     * @return LanguageStructure
     *
     * @throws Exception
     */
    public function save(LanguageStructure $structure): LanguageStructure
    {
        return $this->languageSave->save($structure);
    }
}
