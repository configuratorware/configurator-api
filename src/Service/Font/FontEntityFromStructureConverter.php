<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;

/**
 * @internal
 */
class FontEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var FontRepository
     */
    private $fontRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * DesignAreaEntityFromStructureConverter constructor.
     *
     * @param FontRepository $fontRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        FontRepository $fontRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->fontRepository = $fontRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param FontStructure $structure
     * @param Font $entity
     * @param string $entityClassName
     *
     * @return Font
     *
     * @throws \Exception
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = Font::class
    ): Font {
        /** @var Font $entity */
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        return $entity;
    }
}
