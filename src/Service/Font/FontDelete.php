<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\FontPathsInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class FontDelete
{
    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var FontRepository
     */
    private $fontRepository;

    /**
     * @var FontPathsInterface
     */
    private $fontPaths;

    /**
     * @param Filesystem $fileSystem
     * @param FontRepository $fontRepository
     * @param FontPathsInterface $fontPaths
     */
    public function __construct(
        Filesystem $fileSystem,
        FontRepository $fontRepository,
        FontPathsInterface $fontPaths
    ) {
        $this->fileSystem = $fileSystem;
        $this->fontRepository = $fontRepository;
        $this->fontPaths = $fontPaths;
    }

    /**
     * @param string $idList
     */
    public function delete(string $idList): void
    {
        $ids = explode(',', $idList);
        foreach ($ids as $id) {
            /**
             * Get Entity for given Id.
             *
             * @var Font $entity
             */
            $entity = $this->fontRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            $paths = $this->getFilePaths($entity);

            $this->deleteFiles($paths);

            $this->fontRepository->delete($entity);
        }
    }

    /**
     * @param Font $entity
     *
     * @return array
     */
    private function getFilePaths(Font $entity): array
    {
        $paths = [];
        $dir = $this->fontPaths->getFontPath() . '/';

        if ($entity->getFileNameRegular()) {
            $paths[] = $dir . $entity->getFileNameRegular();
        }

        if ($entity->getFileNameBold()) {
            $paths[] = $dir . $entity->getFileNameBold();
        }

        if ($entity->getFileNameItalic()) {
            $paths[] = $dir . $entity->getFileNameItalic();
        }

        if ($entity->getFileNameBoldItalic()) {
            $paths[] = $dir . $entity->getFileNameBoldItalic();
        }

        return $paths;
    }

    /**
     * @param array $paths
     */
    private function deleteFiles(array $paths): void
    {
        if (!empty($paths)) {
            foreach ($paths as $path) {
                if (!is_dir($path)) {
                    $this->fileSystem->remove($path);
                }
            }
        }
    }
}
