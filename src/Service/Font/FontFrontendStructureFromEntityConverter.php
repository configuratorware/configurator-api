<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\FontPathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Font as FrontendFontStructure;

/**
 * @internal
 */
class FontFrontendStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var FontPathsInterface
     */
    private $fontPaths;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(FontPathsInterface $fontPaths, StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->fontPaths = $fontPaths;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Font $entity
     * @param string $structureClassName
     *
     * @return FrontendFontStructure
     */
    public function convertOne($entity, $structureClassName = FrontendFontStructure::class): FrontendFontStructure
    {
        /** @var FrontendFontStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $properties = FrontendFontStructure::FILENAME_PROPERTIES;

        $fontPathRelative = DIRECTORY_SEPARATOR . $this->fontPaths->getFontPathRelative();
        foreach ($properties as $property) {
            if (!empty($structure->$property)) {
                $structure->$property = $fontPathRelative . '/' . $structure->$property;
            }
        }

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = FrontendFontStructure::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }
}
