<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;

/**
 * @internal
 */
class FontStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Font $entity
     * @param string $structureClassName
     *
     * @return FontStructure
     */
    public function convertOne($entity, $structureClassName = FontStructure::class): FontStructure
    {
        /** @var FontStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = FontStructure::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }
}
