<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\FontPathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class FontSave
{
    private Filesystem $fileSystem;

    private FontRepository $fontRepository;

    private FontEntityFromStructureConverter $fontEntityFromStructureConverter;

    private FontStructureFromEntityConverter $fontStructureFromEntityConverter;

    private FontPathsInterface $fontPaths;

    public function __construct(
        Filesystem $fileSystem,
        FontRepository $fontRepository,
        FontEntityFromStructureConverter $fontEntityFromStructureConverter,
        FontStructureFromEntityConverter $fontStructureFromEntityConverter,
        FontPathsInterface $fontPaths
    ) {
        $this->fileSystem = $fileSystem;
        $this->fontRepository = $fontRepository;
        $this->fontEntityFromStructureConverter = $fontEntityFromStructureConverter;
        $this->fontStructureFromEntityConverter = $fontStructureFromEntityConverter;
        $this->fontPaths = $fontPaths;
    }

    /**
     * @param FontStructure $structure
     *
     * @return FontStructure
     *
     * @throws \Exception
     */
    public function save(FontStructure $structure): FontStructure
    {
        $entity = null;
        $previouslyLinkedFiles = null;
        $oldFontName = '';

        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->fontRepository->findOneBy(['id' => $structure->id]);
            $oldFontName = $entity->getName();
            $previouslyLinkedFiles = [
                $entity->getFileNameRegular(),
                $entity->getFileNameBold(),
                $entity->getFileNameItalic(),
                $entity->getFileNameBoldItalic(),
            ];
            $previouslyLinkedFiles = array_filter(array_unique($previouslyLinkedFiles));
        }

        $entity = $this->fontEntityFromStructureConverter->convertOne($structure, $entity);

        // only one font is allowed to be the default font
        if ($entity->getIsDefault()) {
            $fonts = $this->fontRepository->findAll();
            foreach ($fonts as $font) {
                if ($font->getId() != $entity->getId()) {
                    $font->setIsDefault(false);
                }
            }
        }

        $entity = $this->fontRepository->save($entity);

        if ($previouslyLinkedFiles) {
            $nowLinkedFiles = [
                $entity->getFileNameRegular(),
                $entity->getFileNameBold(),
                $entity->getFileNameItalic(),
                $entity->getFileNameBoldItalic(),
            ];
            $nowLinkedFiles = array_filter(array_unique($nowLinkedFiles));

            $this->renameLinkedFiles($structure->name, $oldFontName, $previouslyLinkedFiles);
            $this->deleteUnlinkedFiles($previouslyLinkedFiles, $nowLinkedFiles);
        }

        $this->fontRepository->clear();

        /** @var Font $entity */
        $entity = $this->fontRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->fontStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param array|string[] $previouslyLinkedFiles
     * @param array|string[] $nowLinkedFiles
     */
    private function deleteUnlinkedFiles(array $previouslyLinkedFiles, array $nowLinkedFiles): void
    {
        $fontPath = $this->fontPaths->getFontPath();

        if ($previouslyLinkedFiles !== $nowLinkedFiles) {
            foreach ($previouslyLinkedFiles as $file) {
                $path = $fontPath . '/' . $file;

                if (!in_array($file, $nowLinkedFiles, true) && is_file($path)) {
                    $this->fileSystem->remove($path);
                }
            }
        }
    }

    /**
     * @param array|string[] $linkedFiles
     */
    private function renameLinkedFiles(string $newName, string $oldName, array $linkedFiles): void
    {
        $fontPath = $this->fontPaths->getFontPath();
        if ($newName === $oldName) {
            return;
        }
        foreach ($linkedFiles as $file) {
            $oldFilePath = sprintf('%s/%s', $fontPath, $file);
            $newFilePath = str_replace($oldName, $newName, $oldFilePath);
            $this->fileSystem->rename($oldFilePath, $newFilePath);
        }
    }
}
