<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\FontPathsInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @internal
 */
class FontUpload
{
    /**
     * @var FontPathsInterface
     */
    private $fontPaths;

    /**
     * @param FontPathsInterface $fontPaths
     */
    public function __construct(FontPathsInterface $fontPaths)
    {
        $this->fontPaths = $fontPaths;
    }

    /**
     * @param UploadedFile $uploadedFile
     *
     * @return bool
     */
    public function save(UploadedFile $uploadedFile): bool
    {
        $path = $this->getPath($this->fontPaths->getFontPath());

        try {
            $uploadedFile->move($path, $uploadedFile->getClientOriginalName());
        } catch (FileException $exception) {
            throw new Exception($exception->getMessage());
        }

        return true;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    private function getPath(string $path): string
    {
        if (is_dir($path)) {
            return $path;
        }

        $success = mkdir($path, 0777, true);
        if ($success) {
            return $path;
        }

        throw new Exception(sprintf('Directory %s could not be created.', $path));
    }
}
