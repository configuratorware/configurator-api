<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Font;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Entity\Font;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\FontRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Font as FontStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Font as FrontendFontStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FontUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

class FontApi
{
    /**
     * @var FontDelete
     */
    private $fontDelete;

    /**
     * @var FontFrontendStructureFromEntityConverter
     */
    private $fontFrontendStructureFromEntityConverter;

    /**
     * @var FontRepository
     */
    private $fontRepository;

    /**
     * @var FontSave
     */
    private $fontSave;

    /**
     * @var FontStructureFromEntityConverter
     */
    private $fontStructureFromEntityConverter;

    /**
     * @var FontUpload
     */
    private $fontUpload;

    /**
     * FontApi constructor.
     *
     * @param FontDelete $fontDelete
     * @param FontFrontendStructureFromEntityConverter $fontFrontendStructureFromEntityConverter
     * @param FontRepository $fontRepository
     * @param FontSave $fontSave
     * @param FontStructureFromEntityConverter $fontStructureFromEntityConverter
     * @param FontUpload $fontUpload
     */
    public function __construct(
        FontDelete $fontDelete,
        FontFrontendStructureFromEntityConverter $fontFrontendStructureFromEntityConverter,
        FontRepository $fontRepository,
        FontSave $fontSave,
        FontStructureFromEntityConverter $fontStructureFromEntityConverter,
        FontUpload $fontUpload
    ) {
        $this->fontDelete = $fontDelete;
        $this->fontFrontendStructureFromEntityConverter = $fontFrontendStructureFromEntityConverter;
        $this->fontRepository = $fontRepository;
        $this->fontSave = $fontSave;
        $this->fontStructureFromEntityConverter = $fontStructureFromEntityConverter;
        $this->fontUpload = $fontUpload;
    }

    /**
     * @return FrontendFontStructure[]
     */
    public function getFrontendDesignerList(): array
    {
        $entities = $this->fontRepository->frontendList();

        $structures = $this->fontFrontendStructureFromEntityConverter->convertMany($entities);

        return $structures;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $arguments->orderBy = $arguments->orderBy ?? 'font.sequence_number';

        $entities = $this->fontRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult = new PaginationResult();
        $paginationResult->data = $this->fontStructureFromEntityConverter->convertMany($entities);
        $paginationResult->count = $this->fontRepository->fetchListCount($arguments, $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return FontStructure
     */
    public function getOne(int $id): FontStructure
    {
        $entity = $this->fontRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->fontStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param FontStructure $structure
     *
     * @return FontStructure
     *
     * @throws Exception
     */
    public function save(FontStructure $structure): FontStructure
    {
        $structure = $this->fontSave->save($structure);

        return $structure;
    }

    /**
     * @param FontUploadArguments $fontUpload
     *
     * @return bool
     */
    public function upload(FontUploadArguments $fontUpload): bool
    {
        $success = $this->fontUpload->save($fontUpload->uploadedFile);

        return $success;
    }

    /**
     * @param $id
     */
    public function delete($id): void
    {
        $this->fontDelete->delete($id);
    }

    public function updateSequence(SequenceNumberArguments $arguments): void
    {
        /* @var SequenceNumberData $sequenceNumberData */
        foreach ($arguments->data as $sequenceData) {
            /** @var Font $font */
            $font = $this->fontRepository->find($sequenceData->id);

            if ($font) {
                $font->setSequenceNumber($sequenceData->sequencenumber);
                $this->fontRepository->save($font);
            }
        }

        $this->fontRepository->flush();
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['name'];
    }
}
