<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\License;

use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LicensePathInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class LicenseFileValidator
{
    /**
     * @var LicenseChecker
     */
    private $licenseChecker;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $licenseFilePath;

    /**
     * LicenseFileValidator constructor.
     *
     * @param LicensePathInterface $licensePath
     * @param LicenseChecker $licenseChecker
     * @param Filesystem $filesystem
     */
    public function __construct(LicensePathInterface $licensePath, LicenseChecker $licenseChecker, Filesystem $filesystem)
    {
        $this->licenseFilePath = $licensePath->getLicenseFilePath();
        $this->licenseChecker = $licenseChecker;
        $this->filesystem = $filesystem;
    }

    /**
     * @return string|null
     */
    public function getValidProduct(): ?string
    {
        if (false === $this->filesystem->exists($this->licenseFilePath)) {
            return null;
        }

        return $this->licenseChecker->getLicensedProduct(file_get_contents($this->licenseFilePath));
    }

    /**
     * @return array
     */
    public function getFeatures(): array
    {
        if (false === $this->filesystem->exists($this->licenseFilePath)) {
            return [];
        }

        return $this->licenseChecker->getLicensedFeatures(file_get_contents($this->licenseFilePath));
    }

    /**
     * @return bool
     */
    public function hasClientsFeature(): bool
    {
        $features = $this->getFeatures();

        return in_array(LicenseChecker::FEATURE_CLIENTS, $features, true);
    }
}
