<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\License;

/**
 * @internal
 */
class LicenseChecker
{
    public const FEATURE_CLIENTS = 'clients';
    public const VALID_PRODUCT_PARTS = [
        'designer',
        'creator',
        'finder',
    ];

    public function isValidProductCombination(string $products): bool
    {
        $invalidProductParts = array_diff(explode('+', $products), self::VALID_PRODUCT_PARTS);

        return empty($invalidProductParts);
    }

    public function getLicensedProduct(string $qte3ba6c6ff941e): ?string
    {
        $dyw78f0805fa8ff = $this->getLicenseParts($qte3ba6c6ff941e);

        return $dyw78f0805fa8ff['products'] ?? null;
    }

    public function getLicensedFeatures(string $qte3ba6c6ff941e): array
    {
        $dyw78f0805fa8ff = $this->getLicenseParts($qte3ba6c6ff941e);
        if (!is_array($dyw78f0805fa8ff) || !array_key_exists('features', $dyw78f0805fa8ff)) {
            return [];
        }

        return array_values(array_filter((array)$dyw78f0805fa8ff['features'], function ($dos518a4861e5be) {return in_array($dos518a4861e5be, $this->getAvailableFeatures()); }));
    }

    private function getLicenseParts(string $qte3ba6c6ff941e)
    {
        /** @var list<string>|false $duq0a499853650a */
        $duq0a499853650a = explode(PHP_EOL, $qte3ba6c6ff941e);
        if (false === $duq0a499853650a || 2 !== count($duq0a499853650a)) {
            return null;
        }
        [$maa4ffc1a098e5d,$hdr2dbbe420c169] = $duq0a499853650a;
        $srhbbf9691bdc7c = explode($this->getDataDelimiter(), $maa4ffc1a098e5d);
        if (false === $srhbbf9691bdc7c || count($srhbbf9691bdc7c) < 2 || count($srhbbf9691bdc7c) > 3) {
            return null;
        }
        if ('' === $srhbbf9691bdc7c[1] || !$this->isValidProductCombination($srhbbf9691bdc7c[0])) {
            return null;
        }
        if (false === $this->isValidLicenseHash($srhbbf9691bdc7c, $this->getDataDelimiter(), $this->getSalt(), $hdr2dbbe420c169)) {
            return null;
        }

        return ['products' => $srhbbf9691bdc7c[0], 'domains' => $srhbbf9691bdc7c[1], 'features' => isset($srhbbf9691bdc7c[2]) ? explode($this->getListDelimiter(), $srhbbf9691bdc7c[2]) : []];
    }

    private function isValidLicenseHash(array $srhbbf9691bdc7c, string $bbo528d3a0db57c, string $aocceb20772e0c9, string $hdr2dbbe420c169): bool
    {
        return hash('gost', trim(implode($bbo528d3a0db57c, $srhbbf9691bdc7c)) . $aocceb20772e0c9) === $hdr2dbbe420c169;
    }

    private function getSalt(): string
    {
        return '#hOz7t4h8bv?p_dwaTHNYMRb';
    }

    private function getDataDelimiter(): string
    {
        return '###';
    }

    private function getListDelimiter(): string
    {
        return ';';
    }

    private function getAvailableFeatures(): array
    {
        return [self::FEATURE_CLIENTS];
    }
}
