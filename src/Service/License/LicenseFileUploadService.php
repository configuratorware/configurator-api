<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\License;

use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\LicensePathInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\LicenseFileUploadArguments;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * @internal
 */
class LicenseFileUploadService
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var LicensePathInterface
     */
    private $licensePath;

    public function __construct(Filesystem $filesystem, LicensePathInterface $licensePath)
    {
        $this->filesystem = $filesystem;
        $this->licensePath = $licensePath;
    }

    /**
     * @param LicenseFileUploadArguments $arguments
     */
    public function uploadFile(LicenseFileUploadArguments $arguments): void
    {
        try {
            $originalFilePath = $arguments->uploadedFile->getPath() . '/' . $arguments->uploadedFile->getFilename();
            $this->filesystem->copy($originalFilePath, $this->getLicensePath(), true);
        } catch (FileException $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @return string
     */
    private function getLicensePath(): string
    {
        $licensePath = $this->licensePath->getLicenseFilePath();
        $dirname = dirname($licensePath);

        if ($this->filesystem->exists($dirname)) {
            return $licensePath;
        }

        try {
            $this->filesystem->mkdir($dirname);
        } catch (\Exception $exception) {
            throw new Exception('License file directory could not be created');
        }

        return $licensePath;
    }
}
