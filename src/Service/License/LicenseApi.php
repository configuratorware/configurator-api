<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\License;

use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserCredentials;
use Redhotmagma\ConfiguratorApiBundle\Structure\LicenseFile;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\LicenseFileUploadArguments;

class LicenseApi
{
    /**
     * @var LicenseFileValidator
     */
    private $licenseFileValidator;

    /**
     * @var LicenseFileUploadService
     */
    private $licenseFileUploadService;

    /**
     * @var UserCredentials
     */
    private $userCredentials;

    public function __construct(
        LicenseFileUploadService $licenseFileUploadService,
        LicenseFileValidator $licenseFileValidator,
        UserCredentials $userCredentials
    ) {
        $this->licenseFileUploadService = $licenseFileUploadService;
        $this->licenseFileValidator = $licenseFileValidator;
        $this->userCredentials = $userCredentials;
    }

    /**
     * @param LicenseFileUploadArguments $arguments
     * @param User $user
     *
     * @return LicenseFile
     */
    public function uploadLicenseFile(LicenseFileUploadArguments $arguments, User $user): LicenseFile
    {
        $this->licenseFileUploadService->uploadFile($arguments);

        $licenseFile = new LicenseFile();
        $licenseFile->license = $this->licenseFileValidator->getValidProduct();
        $licenseFile->credentials = $this->userCredentials->getByUser($user);

        return $licenseFile;
    }

    /**
     * @return string|null
     */
    public function getValidProduct(): ?string
    {
        return $this->licenseFileValidator->getValidProduct();
    }
}
