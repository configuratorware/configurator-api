<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation;

use Danmichaelo\Coma\ColorDistance;
use Danmichaelo\Coma\sRGB;
use Imagick;
use MischiefCollective\ColorJizz\Formats\RGB;
use Redhotmagma\ConfiguratorApiBundle\Factory\ProcessFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageDataProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageModel;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class Vectorize implements OperationInterface
{
    /*
     * @var int
     */
    private const POTENTIAL_MAX_THRESHOLD = 50;

    /**
     * Merge color to the nearest color if it's presence is below this percent.
     *
     * @var float
     */
    private const COLOR_MERGE_THRESHOLD = 3.0;

    /**
     * Default color threshold for vector conversion.
     *
     * @var int
     */
    private const THRESHOLD = 10;

    /**
     * Allowed number of colors in image.
     *
     * @var int
     */
    private const QUANTIZE_FROM_THRESHOLD = 32;

    /**
     * Minimum allowed threshold of vectorizing - related to, but not matching the number of colors in result.
     *
     * @var int
     */
    private const MIN_ALLOWED_THRESHOLD = 1;

    /**
     * Transparency threshold for color removal - remove more transparent colors.
     *
     * @var float
     */
    private const COLOR_TRANSPARENCY_THRESHOLD = 0.8;

    /**
     * @var ColorDistance
     */
    private $colorDistance;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var ProcessFactory
     */
    private $processFactory;

    /**
     * Temporary image files.
     *
     * @var string[]
     */
    private $tmpFiles;

    /**
     * Temporary SVG files.
     *
     * @var string[]
     */
    private $svgFiles = [];

    /**
     * @var int
     */
    private $minRequiredColorPresence;

    /**
     * @param Filesystem $fileSystem
     * @param ProcessFactory $processFactory
     * @param ColorDistance $colorDistance
     */
    public function __construct(Filesystem $fileSystem, ProcessFactory $processFactory, ColorDistance $colorDistance)
    {
        $this->fileSystem = $fileSystem;
        $this->processFactory = $processFactory;
        $this->colorDistance = $colorDistance;
    }

    /**
     * @param \StdClass $config Operation configs as StdClass
     * @param array $sources Return sources
     *
     * @return ImageModel[]
     *
     * @throws \ImagickException
     * @throws \ImagickPixelException
     * @throws \MischiefCollective\ColorJizz\Exceptions\InvalidArgumentException
     */
    public function process(\stdClass $config, array $sources)
    {
        // skip vectorizing if already an SVG
        $pathArray = explode('.', $sources[ImageDataProvider::PREVIEW]->getPath());
        if ('svg' === strtolower(end($pathArray))) {
            return $sources;
        }

        // color number threshold
        $threshold = $config->threshold ?: self::THRESHOLD;
        if (isset($config->monochrome) && true === $config->monochrome) {
            $threshold = self::MIN_ALLOWED_THRESHOLD;
        }

        // pixel amount threshold
        $colorMergeThreshold = $config->colorMergeThreshold ?? self::COLOR_MERGE_THRESHOLD;

        // first vectorize one time with self::POTENTIAL_MAX_THRESHOLD + $removeColorsUnderThreshold = false to get the potential path amount
        $maxPathAmount = $this->calculateMaxPathAmount($sources[ImageDataProvider::PREVIEW], $colorMergeThreshold);

        // then vectorize again for the result
        $removeColorsUnderThreshold = (int)$config->threshold > 0 ? false : true;
        $this->vectorize($sources[ImageDataProvider::PREVIEW], $threshold, $removeColorsUnderThreshold || (isset($config->monochrome) && $config->monochrome), $colorMergeThreshold);

        // create SVG path from preview path
        $pathInfo = pathinfo($sources[ImageDataProvider::PREVIEW]->getPath());
        $svgPath = $pathInfo['dirname'] . '/' . $pathInfo['filename'] . '.svg';

        // write SVG file
        $this->fileSystem->dumpFile($svgPath, $this->getSvgContent());

        // override preview image
        /* @var $previewImage ImageModel */
        $previewImage = $sources[ImageDataProvider::PREVIEW];
        $sources[ImageDataProvider::PREVIEW] = ImageFactory::create($svgPath, 'SVG', 'image/svg+xml', $previewImage->getSize(), $previewImage->getWidth(), $previewImage->getHeight());
        $sources[ImageDataProvider::PREVIEW]->addAdditionalData(['maxPotentialColorAmount' => $maxPathAmount]);

        // remove temp files
        $this->cleanUpFiles();

        return $sources;
    }

    private function calculateMaxPathAmount($previewSource, $colorMergeThreshold)
    {
        $this->vectorize($previewSource, self::POTENTIAL_MAX_THRESHOLD, false, $colorMergeThreshold);

        $svgAsXML = simplexml_load_string($this->getSvgContent());

        $colors = [];
        foreach ($svgAsXML as $group) {
            $colors[] = (string)$group->attributes()['fill'];
        }

        return count(array_unique($colors));
    }

    /**
     * @param ImageModel $source
     * @param int $colorCount
     * @param bool $removeColorsUnderThreshold
     * @param float $colorMergeThreshold
     *
     * @return void
     *
     * @throws \ImagickException
     * @throws \ImagickPixelException
     * @throws \MischiefCollective\ColorJizz\Exceptions\InvalidArgumentException
     */
    private function vectorize(ImageModel $source, int $colorCount, bool $removeColorsUnderThreshold, float $colorMergeThreshold)
    {
        $image = $this->createImageObject($source->getPath());

        // get original image colors
        $presentColors = $this->getColorsAndOccurrence($image);

        $imageColorPixelCount = $this->getNumberOfColoredPixels($presentColors);
        $this->minRequiredColorPresence = (int)($imageColorPixelCount / 100 * $colorMergeThreshold);

        // if image has too many colors, convert it to less color variant
        if (is_countable($presentColors) && count($presentColors) > $colorCount) {
            $this->quantizeImage($image);
            $presentColors = $this->getColorsAndOccurrence($image);
        }

        $this->sortColorsByOccurrence($presentColors);

        // get a smaller amount of target colors from the remaining colors
        $targetColors = $this->getTargetColors($presentColors, $colorCount, $removeColorsUnderThreshold);

        // replace original colors to target colors
        if (!empty($targetColors)) {
            $this->mergeOriginalToTargetColors($image, $targetColors, $presentColors);
            $presentColors = $this->getColorsAndOccurrence($image);
            $this->sortColorsByOccurrence($presentColors);
        }

        $targetColors = $this->getTargetColors($presentColors, $colorCount, $removeColorsUnderThreshold);

        $this->traceImage($image, $targetColors);
    }

    /**
     * @param array $colors
     *
     * @return int
     */
    private function getNumberOfColoredPixels(array $colors): int
    {
        $filteredColors = array_filter(
            $colors,
            function (array $color) {
                return 0 != $color['color']['a'];
            }
        );
        $summary = array_reduce(
            $filteredColors,
            function ($count, array $color) {
                $count += $color['count'];

                return $count;
            }
        );

        return $summary;
    }

    /**
     * @param \Imagick $image
     *
     * @throws \ImagickException
     */
    private function quantizeImage(Imagick &$image)
    {
        $image->quantizeImage(
            self::QUANTIZE_FROM_THRESHOLD,
            $image->getImageColorspace() ?? \Imagick::COLORSPACE_SRGB,
            8,
            false,
            false
        );

        // need to write the file and read back again, because quantize messes up the colors in Imagick object
        $tmpFile = $this->fileSystem->tempnam(sys_get_temp_dir(), 'vectorize_') . '.png';
        $this->tmpFiles[] = $tmpFile;
        $image->writeImage($tmpFile);
        $image = new \Imagick($tmpFile);

        $image = $this->createImageObject($tmpFile);
    }

    /**
     * Get target colors by threshold number.
     *
     * @param array $colors Color array sorted by occurence number
     * @param int $threshold Color threshold
     * @param bool $removeColorsUnderThreshold
     *
     * @return array
     */
    private function getTargetColors(array $colors, int $threshold, bool $removeColorsUnderThreshold = false): array
    {
        // remove copletely transparent colors to not get empty pathes
        foreach ($colors as $i => $color) {
            if (0 == $color['color']['a']) {
                unset($colors[$i]);
            }

            if (true === $removeColorsUnderThreshold && $color['count'] < $this->minRequiredColorPresence) {
                unset($colors[$i]);
            }
        }

        return array_slice($colors, 0, $threshold);
    }

    /**
     * Change original colors to target colors.
     *
     * @param \Imagick $image
     * @param array $targetColors
     * @param array $originalColors
     *
     * @throws \Exception
     */
    private function mergeOriginalToTargetColors(Imagick &$image, array $targetColors, array $originalColors)
    {
        foreach ($originalColors as $originalColor) {
            if ($originalColor['color']['a'] <
                self::COLOR_TRANSPARENCY_THRESHOLD) { // remove colors below this transparency
                continue;
            }

            $target = ['originalColor' => $originalColor];

            // decide final color by nearest color distance
            foreach ($targetColors as $targetColor) {
                if (0 == $targetColor['color']['a']) {
                    continue;
                }

                $distance = $this->getColorDistance($originalColor, $targetColor);

                if (!isset($target['distance']) || $target['distance'] > $distance) {
                    // store current color with lower distance than previous
                    $target['distance'] = $distance;
                    $target['targetColor'] = $targetColor;
                }
            }

            // replace colors by final target colors
            if (isset($target['distance']) && 0 != $target['distance']) {
                $this->replaceColor($image, $target['originalColor'], $target['targetColor']);
            }
        }
    }

    /**
     * Trace image parts.
     *
     * @param \Imagick $image
     * @param array $targetColors
     *
     * @return void
     *
     * @throws \MischiefCollective\ColorJizz\Exceptions\InvalidArgumentException
     */
    private function traceImage(Imagick $image, array $targetColors)
    {
        $this->svgFiles = null;

        // create trace processes for all partial colors
        foreach ($targetColors as $targetColor) {
            if (0 == $targetColor['color']['a']) {
                continue;
            }

            $tmpFile = $this->fileSystem->tempnam(sys_get_temp_dir(), 'vectorize_');

            list($red, $green, $blue, $alpha) = array_values($targetColor['color']);

            $partialImage = $this->createTraceableImage($image, $red, $green, $blue, $alpha);

            // write to bitmap format readable by potrace
            $partialImage->writeImage($tmpFile . '.ppm');

            // create SVG convert processes by file parts
            $rgb = new RGB($red, $green, $blue);
            $hexColor = '#' . $rgb->toHex(); // color path to default color

            // process image with potrace
            $process = $this->processFactory->create([
                'potrace',
                '--svg',            // need SVG result
                $tmpFile . '.ppm',    // need an uncompressed pixel file to trace
                '--flat',           // single path after trace
                '-C' . $hexColor,    // add color to trace result
                '-o',
                $tmpFile . '.svg', // output to this file

                /* curve optimization tuning */
                '--turnpolicy',
                'black',
                '--alphamax',
                0.5,
                '--longcurve',
                '--turdsize',
                5,
            ]);
            $process->mustRun();

            $this->tmpFiles[] = $tmpFile . '.ppm';
            $this->tmpFiles[] = $tmpFile . '.svg';

            $alphaIndex = floor($alpha * 100);
            $this->svgFiles[$hexColor . $alphaIndex] = $tmpFile;
        }
    }

    /**
     * Create traceable image with black and white colors and opacity.
     *
     * @param \Imagick $image
     * @param int $red
     * @param int $green
     * @param int $blue
     * @param int $alpha
     *
     * @return \Imagick Color replaced image for trace
     */
    private function createTraceableImage(Imagick $image, $red, $green, $blue, $alpha): Imagick
    {
        $partialImage = clone $image;

        // make non target colors opaque (so keeping blacks)
        $partialImage->opaquePaintImage(
            new \ImagickPixel("rgba($red,$green,$blue,$alpha)"),
            new \ImagickPixel('rgba(255,255,255,0)'),
            0,
            true,
            \Imagick::CHANNEL_ALL
        );

        // replace object color with black
        $partialImage->opaquePaintImage(
            new \ImagickPixel("rgba($red,$green,$blue,$alpha)"),
            new \ImagickPixel('rgba(0,0,0,1)'),
            0,
            false,
            \Imagick::CHANNEL_ALL
        );

        return $partialImage;
    }

    /**
     * Extract SVG paths from parts.
     *
     * @return string
     */
    private function getSvgContent(): string
    {
        $svgContent = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" ';
        $svgContent .= 'xmlns:xlink="http://www.w3.org/1999/xlink">';
        foreach ($this->svgFiles as $fileName) {
            $svgContent .= $this->extractPathFromFile(file_get_contents($fileName . '.svg'));
        }
        $svgContent .= '</svg>';

        return $svgContent;
    }

    /**
     * @param string $svgString
     *
     * @return string SVG paths
     */
    private function extractPathFromFile(string $svgString): string
    {
        $dom = new \DOMDocument();
        $dom->loadXML($svgString);
        $elements = $dom->getElementsByTagName('g');
        /* @var $elements \DOMElement[] */

        $svgPaths = '';
        foreach ($elements as $element) {
            if ('' !== $element->getElementsByTagName('path')->item(0)->attributes['d']->nodeValue) {
                $svgPaths .= $dom->saveXML($element);
            }
        }

        return $svgPaths;
    }

    /**
     * Sort colors by occurrence.
     *
     * @param array $colorArray
     *
     * @return array
     */
    private function sortColorsByOccurrence(array &$colorArray): array
    {
        usort(
            $colorArray,
            function (array $itemA, array $itemB) {
                return -1 * ($itemA['count'] <=> $itemB['count']);
            }
        );

        return $colorArray;
    }

    /**
     * Get colors with occurrence, also keep transparency as a color and handle it later.
     *
     * @param \Imagick $image
     *
     * @return array
     *
     * @throws \ImagickPixelException
     */
    private function getColorsAndOccurrence(Imagick $image)
    {
        // get if the image has an alpha channel, otherwise the alpha channel gets random values
        $hasAlphaChannel = $image->getImageAlphaChannel();

        $colorArray = [];
        foreach ($image->getImageHistogram() as $pixel) {
            /* @var $pixel \ImagickPixel */

            $color = [
                'color' => $pixel->getColor(),
                'count' => $pixel->getColorCount(),
            ];

            if ($hasAlphaChannel) {
                $color['color']['a'] = $pixel->getColorValue(\Imagick::COLOR_ALPHA);
            } else {
                $color['color']['a'] = 1;
            }

            $colorArray[implode('#', $color['color'])] = $color;
        }

        return $colorArray;
    }

    /**
     * Clean up temporary files.
     */
    private function cleanUpFiles()
    {
        if (is_array($this->tmpFiles)) {
            foreach ($this->tmpFiles as $path) {
                $this->fileSystem->remove($path);
            }
        }
    }

    /**
     * Create new Imagick image.
     *
     * @param string $path
     *
     * @return Imagick
     *
     * @throws \ImagickException
     */
    private function createImageObject($path): Imagick
    {
        $path = preg_replace('/\.svg$/i', '.png', $path);
        $image = new \Imagick($path);

        // if alpha channel is not present, normalize image, otherwise alpha channel problems could be present
        if (!$image->getImageAlphaChannel()) {
            $tempPath = $this->fileSystem->tempnam(sys_get_temp_dir(), 'convert_tmp') . '.png';
            $imageHandle = imagecreatefrompng($path);
            imagepng($imageHandle, $tempPath);
            $image = new \Imagick($tempPath);
        }

        return $image;
    }

    /**
     * @param Imagick $image
     * @param array $colorToReplace
     * @param array $colorToFill
     */
    private function replaceColor(Imagick &$image, array $colorToReplace, array $colorToFill)
    {
        list($rR, $rG, $rB, $rA) = array_values($colorToReplace['color']);
        list($fR, $fG, $fB, $fA) = array_values($colorToFill['color']);

        $image->opaquePaintImage(
            new \ImagickPixel("rgba($rR,$rG,$rB,$rA)"),
            new \ImagickPixel("rgba($fR,$fG,$fB,$fA)"),
            0,
            false,
            \Imagick::CHANNEL_ALL
        );
    }

    /**
     * @param array $color1
     * @param array $color2
     *
     * @return float
     *
     * @throws \Exception
     */
    private function getColorDistance(array $color1, array $color2): float
    {
        list($c1R, $c1G, $c1B) = array_values($color1['color']);
        $compare1 = new sRGB($c1R, $c1G, $c1B);

        list($c2R, $c2G, $c2B) = array_values($color2['color']);
        $compare2 = new sRGB($c2R, $c2G, $c2B);

        return (float)$this->colorDistance->cie76($compare1, $compare2);
    }
}
