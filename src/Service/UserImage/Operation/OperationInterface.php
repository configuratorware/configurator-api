<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation;

/**
 * @internal
 */
interface OperationInterface
{
    public function process(\stdClass $config, array $sources);
}
