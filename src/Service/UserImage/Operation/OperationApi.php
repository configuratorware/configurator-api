<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequestOperation;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OperationApi
{
    public const CLIPPING = 'clipping';

    public const VECTORIZE = 'vectorize';

    public const OPERATIONS = [self::CLIPPING, self::VECTORIZE];

    /**
     * @var Clipping
     */
    private $clipping;

    /**
     * @var Vectorize
     */
    private $vectorize;

    public function __construct(
        Clipping $clipping,
        Vectorize $vectorize
    ) {
        $this->clipping = $clipping;
        $this->vectorize = $vectorize;
    }

    public function process(
        DesignerUserImageRequestOperation $operation,
        array $sources
    ): array {
        /*
         * this check on in_array is also for security reasons, to make code injections impossible.
         */
        if (!in_array(strtolower($operation->identifier), self::OPERATIONS)) {
            throw new BadRequestHttpException();
        }

        $class = lcfirst($operation->identifier);

        return $this->$class->process($operation->config, $sources);
    }
}
