<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation;

use Imagick;
use ImagickPixel;
use Redhotmagma\ConfiguratorApiBundle\Exception\ImageConversionException;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ColorChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageDataProvider;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @internal
 */
class Clipping implements OperationInterface
{
    private const FUZZ = 3000;

    /**
     * @var ColorChecker
     */
    private $colorChecker;

    public const COMMANDS = ['flood', 'replace'];
    public const SOURCES_ALLOWED = [ImageDataProvider::PREVIEW, ImageDataProvider::THUMBNAIL];

    /**
     * @param ColorChecker $colorChecker
     */
    public function __construct(ColorChecker $colorChecker)
    {
        $this->colorChecker = $colorChecker;
    }

    /**
     * @param \StdClass $config
     * @param array $sources
     *
     * @return array
     */
    public function process(
        \stdClass $config,
        array $sources
    ): array {
        /*
         * this check on in_array is also for security reasons, to make code injections impossible.
         */
        if (!in_array($config->command, self::COMMANDS, true)) {
            throw new BadRequestHttpException();
        }

        $command = $config->command;

        if ($this->colorChecker->isHexColor($config->backgroundColor)) {
            $backgroundColor = $this->colorChecker->formatHexColor($config->backgroundColor);
        }

        foreach ($sources as $key => $source) {
            if (in_array($key, self::SOURCES_ALLOWED, true)) {
                $this->$command(
                    $source->getPath(),
                    $backgroundColor ?? '#ffffff'
                );
            }
        }

        return $sources;
    }

    /**
     * @param string $source
     * @param string $backgroundColor
     * @param int $fuzz
     *
     * @throws ImageConversionException
     */
    public function flood(
        string $source,
        string $backgroundColor = '#ffffff',
        int $fuzz = self::FUZZ
    ) {
        try {
            $image = new Imagick($source);

            // Add Border to make sure, that right color gets catched by floodfillpaintimage
            $image->borderImage($backgroundColor, 1, 1);

            // flood fill to transparency, starting from the top left pixel
            $image->floodfillpaintimage(
                new ImagickPixel('rgba(255,255,255,0)'),
                $fuzz,
                $backgroundColor,
                0,
                0,
                false,
                Imagick::CHANNEL_ALL
            );

            // remove the border
            $image->shaveImage(1, 1);

            $image->writeImage();
        } catch (\Throwable $e) {
            throw ImageConversionException::conversionFailed($e);
        }
    }

    /**
     * @param string $source
     * @param string $backgroundColor
     * @param int $fuzz
     *
     * @throws ImageConversionException;
     */
    public function replace(
        string $source,
        string $backgroundColor = '#ffffff',
        int $fuzz = self::FUZZ
    ) {
        try {
            $image = new Imagick($source);

            $image->transparentPaintImage(
                $backgroundColor,
                0.0,
                $fuzz,
                false
            );

            $image->writeImage();
        } catch (\Throwable $e) {
            throw ImageConversionException::conversionFailed($e);
        }
    }
}
