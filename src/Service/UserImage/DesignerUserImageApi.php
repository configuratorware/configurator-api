<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageDataProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageModel;
use Redhotmagma\ConfiguratorApiBundle\Service\UserImage\Operation\OperationApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequest as DesignerUserImageRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageResponse as DesignerUserImageResponseStructure;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @internal
 */
class DesignerUserImageApi
{
    public const VECTOR_IMAGE_MIME_TYPES = [
        'application/pdf', // PDF
        'application/x-pdf',
        'application/x-bzpdf',
        'application/x-gzpdf',
        'image/svg+xml', // SVG
        'image/eps', // EPS
        'image/x-eps',
        'image/x-ept',
        'application/eps',
        'application/x-eps',
        'application/x-ept',
        'application/postscript', // AI, EPS, PS
    ];

    /**
     * @var DesignerUserImageResponseImageStructureFromDataConverter
     */
    private $designerUserImageResponseImageStructureFromDataConverter;

    /**
     * @var DesignerUserImageUpload
     */
    private $designerUserImageUpload;

    /**
     * @var ImageDataProvider
     */
    private $imageDataProvider;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var string
     */
    private $uploadPathPrivate;

    /**
     * @var OperationApi
     */
    private $operationApi;

    /**
     * @var SvgStructureChecker
     */
    private $svgStructureChecker;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var UserImageCopy
     */
    private $userImageCopy;

    /**
     * DesignerUserImageApi constructor.
     *
     * @param DesignerUserImageResponseImageStructureFromDataConverter $designerUserImageResponseImageStructureFromDataConverter
     * @param DesignerUserImageUpload $designerUserImageUpload
     * @param ImageDataProvider $imageDataProvider
     * @param string $mediaBasePath
     * @param string $uploadPathPrivate
     * @param OperationApi $operationApi
     * @param SvgStructureChecker $svgStructureChecker
     * @param Filesystem $filesystem
     * @param UserImageCopy $userImageCopy
     */
    public function __construct(
        DesignerUserImageResponseImageStructureFromDataConverter $designerUserImageResponseImageStructureFromDataConverter,
        DesignerUserImageUpload $designerUserImageUpload,
        ImageDataProvider $imageDataProvider,
        string $mediaBasePath,
        string $uploadPathPrivate,
        OperationApi $operationApi,
        SvgStructureChecker $svgStructureChecker,
        Filesystem $filesystem,
        UserImageCopy $userImageCopy
    ) {
        $this->designerUserImageResponseImageStructureFromDataConverter = $designerUserImageResponseImageStructureFromDataConverter;
        $this->designerUserImageUpload = $designerUserImageUpload;
        $this->imageDataProvider = $imageDataProvider;
        $this->mediaBasePath = $mediaBasePath;
        $this->uploadPathPrivate = $uploadPathPrivate;
        $this->operationApi = $operationApi;
        $this->svgStructureChecker = $svgStructureChecker;
        $this->filesystem = $filesystem;
        $this->userImageCopy = $userImageCopy;
    }

    /**
     * @param DesignerUserImageRequestStructure $designerUserImageRequestStructure
     *
     * @return DesignerUserImageResponseStructure
     */
    public function edit(DesignerUserImageRequestStructure $designerUserImageRequestStructure): DesignerUserImageResponseStructure
    {
        // create sources by upload
        $uploadedFile = $designerUserImageRequestStructure->uploadedFile;
        if ($uploadedFile instanceof UploadedFile) {
            $sources = $this->designerUserImageUpload->upload($uploadedFile);
        }

        $fileHash = $designerUserImageRequestStructure->fileName;
        if (true == $designerUserImageRequestStructure->createCopy) {
            $fileHash = $this->userImageCopy->createCopy($designerUserImageRequestStructure);
        }

        // create sources from a file, if present
        if (!isset($sources) && !empty($fileHash)) {
            $sources = $this->provideSources($fileHash);
        }

        // no sources created -> bad request
        if (!isset($sources)) {
            throw new BadRequestHttpException('A new file or a valid present file\'s name should be passed');
        }

        // run image operations
        foreach ((array)$designerUserImageRequestStructure->operations as $operation) {
            // vectorization and clipping requires resetting svg preview images and forcing raster format
            $previewExtension = pathinfo($sources[ImageDataProvider::PREVIEW]->getPath(), PATHINFO_EXTENSION);
            if ('svg' === strtolower($previewExtension)) {
                $sources[ImageDataProvider::PREVIEW] = $this->designerUserImageUpload->resetPreview($sources, true);
            }
            $sources = $this->operationApi->process($operation, $sources);
        }

        // reset preview image for empty operations (= frontend needs to reload original preview)
        if (!$designerUserImageRequestStructure->operations || empty((array)$designerUserImageRequestStructure->operations)) {
            $sources[ImageDataProvider::PREVIEW] = $this->designerUserImageUpload->resetPreview($sources);
            $sources = $this->designerUserImageUpload->overridePreviewDimensionsOriginal($sources);
        }

        return $this->createStructure($sources);
    }

    /**
     * @param string $fileName
     *
     * @return ImageModel[]
     */
    private function provideSources(string $fileName): array
    {
        $sources = [];
        $originalPath = $this->getOriginalFilePath($fileName);
        if ($this->filesystem->exists($originalPath)) {
            $paths = [];
            $original = $this->imageDataProvider->provide($originalPath);
            $sources[ImageDataProvider::ORIGINAL] = $original;

            $paths[ImageDataProvider::PREVIEW] = $this->mediaBasePath . DesignerUserImageUpload::PATH_PREVIEW . $fileName . '.svg';
            if (!$this->filesystem->exists($paths[ImageDataProvider::PREVIEW])) {
                $paths[ImageDataProvider::PREVIEW] = $this->mediaBasePath . DesignerUserImageUpload::PATH_PREVIEW . $fileName . '.png';
            }
            $paths[ImageDataProvider::THUMBNAIL] = $this->mediaBasePath . DesignerUserImageUpload::PATH_THUMBNAIL . $fileName . '.png';
            foreach ($paths as $key => $path) {
                if ($this->filesystem->exists($path)) {
                    $sources[$key] = $this->imageDataProvider->provide($path);
                }
            }

            $this->designerUserImageUpload->resetPreview($sources);
            $this->designerUserImageUpload->overrideOriginalSourceDimensionsWidthPreview($sources);
        }

        return $sources;
    }

    /**
     * @param ImageModel[] $sources
     *
     * @return DesignerUserImageResponseStructure
     */
    private function createStructure(array $sources): DesignerUserImageResponseStructure
    {
        $structure = new DesignerUserImageResponseStructure();

        $originalFilePath = explode('/', $sources[ImageDataProvider::ORIGINAL]->getPath());
        $originalFilename = end($originalFilePath);

        $structure->fileName = pathinfo($sources[ImageDataProvider::PREVIEW]->getPath())['filename'];
        $structure->originalFileName = $originalFilename;

        foreach ($sources as $key => $source) {
            if (property_exists($structure, $key)) {
                $structure->$key = $this->designerUserImageResponseImageStructureFromDataConverter->convert($source);
            }
        }

        $original = $sources[ImageDataProvider::ORIGINAL];
        if (in_array($original->getMimeType(), self::VECTOR_IMAGE_MIME_TYPES) && $this->svgStructureChecker->checkIfVectorFileContainsEmbeddedTextOrImage($original->getPath())) {
            $structure->message = 'vector_not_paths_only';
        }

        return $structure;
    }

    /**
     * Get the path of the original file uploaded.
     *
     * @param string $fileName
     *
     * @return string
     */
    private function getOriginalFilePath(string $fileName): string
    {
        $originalPath = glob($this->uploadPathPrivate . $fileName . '/*');

        return array_shift($originalPath);
    }
}
