<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageConverterService;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\UserImage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserImageApi
{
    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var ImageConverterService
     */
    private $imageConverterService;

    /**
     * @var array
     */
    private $userImageUploadThumbSize;

    /**
     * UserImageApi constructor.
     *
     * @param string $mediaBasePath
     * @param ImageConverterService $imageConverterService
     * @param array $userImageUploadThumbSize
     */
    public function __construct(
        string $mediaBasePath,
        ImageConverterService $imageConverterService,
        array $userImageUploadThumbSize
    ) {
        $this->mediaBasePath = $mediaBasePath;
        $this->imageConverterService = $imageConverterService;
        $this->userImageUploadThumbSize = $userImageUploadThumbSize;
    }

    /**
     * @param UploadedFile $imageFormData
     *
     * @return UserImage
     */
    public function upload(UploadedFile $imageFormData)
    {
        $imagePath = $this->getImagePath();

        $baseImageName = md5($imageFormData->getPathname() . microtime());
        $extension = $imageFormData->guessExtension();

        $imageNameOriginal = $baseImageName . '_original.' . $extension;
        $imageNamePreview = $baseImageName . '_preview.png';
        $imageNameThumb = $baseImageName . '_thumb.png';

        // create preview
        $this->imageConverterService->convert($imageFormData->getPathname(), $imagePath . $imageNamePreview);

        // create thumb
        $this->imageConverterService->contain(
            $imageFormData->getPathname(),
            $imagePath . $imageNameThumb,
            $this->userImageUploadThumbSize['width'],
            $this->userImageUploadThumbSize['height']
        );

        // move original file
        $imageFormData->move($imagePath, $imageNameOriginal);

        $userImage = new UserImage();
        $userImage->original = '/img/uploads/' . $imageNameOriginal;
        $userImage->preview = '/img/uploads/' . $imageNamePreview;
        $userImage->thumb = '/img/uploads/' . $imageNameThumb;

        return $userImage;
    }

    /**
     * get path for user image uploads, create it if it does not already exist.
     *
     * @return string
     */
    protected function getImagePath()
    {
        $imagePath = $this->mediaBasePath . '/img/uploads/';

        if (!is_dir($imagePath)) {
            mkdir($imagePath, 0755, true);
        }

        return $imagePath;
    }
}
