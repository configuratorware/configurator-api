<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageRequest;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class UserImageCopy
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $uploadPathPrivate;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * UserImageCopy constructor.
     *
     * @param Filesystem $filesystem
     * @param string $uploadPathPrivate
     * @param string $mediaBasePath
     */
    public function __construct(Filesystem $filesystem, string $uploadPathPrivate, string $mediaBasePath)
    {
        $this->filesystem = $filesystem;
        $this->uploadPathPrivate = $uploadPathPrivate;
        $this->mediaBasePath = $mediaBasePath;
    }

    /**
     * check if the file has to be copied
     * if yes create a copy of the original and return the hash of the newly generated file.
     *
     * @param DesignerUserImageRequest $designerUserImageRequest
     *
     * @return string
     */
    public function createCopy(DesignerUserImageRequest $designerUserImageRequest)
    {
        $finder = new Finder();
        $finder->files()->in($this->uploadPathPrivate . $designerUserImageRequest->fileName);

        foreach ($finder as $file) {
            $hash = md5($file->getFilename() . microtime() . $file->getSize());

            // copy original
            $this->filesystem->copy($file->getPathname(), $this->uploadPathPrivate . $hash . '/' . $file->getFilename());

            // copy preview
            $previewPath = $this->mediaBasePath . DesignerUserImageUpload::PATH_PREVIEW;
            $previewFile = $previewPath . $designerUserImageRequest->fileName . '.png';
            if (false === file_exists($previewFile)) {
                $previewFile = $previewPath . $designerUserImageRequest->fileName . '.svg';
            }
            $this->filesystem->copy($previewFile, $previewPath . $hash . '.' . pathinfo($previewFile, PATHINFO_EXTENSION));

            // copy thumbnail
            $thumbnailPath = $this->mediaBasePath . DesignerUserImageUpload::PATH_THUMBNAIL;
            $this->filesystem->copy($thumbnailPath . $designerUserImageRequest->fileName . '.png', $thumbnailPath . $hash . '.png');

            return $hash;
        }

        return $designerUserImageRequest->fileName;
    }
}
