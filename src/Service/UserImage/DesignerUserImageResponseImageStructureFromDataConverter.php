<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageModel;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerUserImageResponseImage as DesignerUserImageResponseImageStructure;

/**
 * @internal
 */
class DesignerUserImageResponseImageStructureFromDataConverter implements StructureFromDataConverterInterface
{
    /**
     * @var StructureFromDataConverter
     */
    private $structureFromDataConverter;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param StructureFromDataConverter $structureFromDataConverter
     * @param string $mediaBasePath
     */
    public function __construct(StructureFromDataConverter $structureFromDataConverter, string $mediaBasePath)
    {
        $this->structureFromDataConverter = $structureFromDataConverter;
        $this->mediaBasePath = $mediaBasePath;
    }

    /**
     * @param ImageModel $image
     * @param string $structureName
     *
     * @return DesignerUserImageResponseImageStructure
     */
    public function convert($image, $structureName = DesignerUserImageResponseImageStructure::class): DesignerUserImageResponseImageStructure
    {
        $data = $image->getData();

        $structure = $this->structureFromDataConverter->convert($data, $structureName);
        $structure->additionalData = $image->getAdditionalData();
        if (false !== strpos($image->getPath(), $this->mediaBasePath)) {
            $structure->url = str_replace($this->mediaBasePath, '', $image->getPath());
        }

        unset($structure->_metadata);

        return $structure;
    }
}
