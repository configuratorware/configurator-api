<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage;

use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageConverterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageDataProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageModel;
use Redhotmagma\ConfiguratorApiBundle\Vector\Svg;
use Redhotmagma\ConfiguratorApiBundle\Vector\SvgPngConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Vector\Vector;
use Redhotmagma\ConfiguratorApiBundle\Vector\VectorSvgConverterInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 *
 * @internal
 */
class DesignerUserImageUpload
{
    public const PATH_PREVIEW = '/img/uploads/designer/preview/';
    public const PATH_THUMBNAIL = '/img/uploads/designer/thumbnail/';
    private const ROTATION_DEGREE_MAPPING = [
        2 => 0,
        3 => 180,
        4 => 180,
        5 => -90,
        6 => -90,
        7 => 90,
        8 => 90,
    ];
    public const VECTOR_FORMATS = ['ai', 'pdf', 'svg', 'eps', 'ps', 'ept'];

    /**
     * @var string
     */
    private $uploadPathPrivate;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @var ImageConverterService
     */
    private $imageConverterService;

    /**
     * @var ImageDataProvider
     */
    private $imageDataProvider;

    /**
     * @var array
     */
    private $userImageUploadPreviewMax;

    /**
     * @var array
     */
    private $userImageUploadThumbSize;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var SvgStructureChecker
     */
    private $svgStructureChecker;

    /**
     * @var SvgPngConverterInterface
     */
    private $svgPngConverter;

    /**
     * @var VectorSvgConverterInterface
     */
    private $vectorSvgConverter;

    /**
     * @param string $uploadPathPrivate
     * @param string $mediaBasePath
     * @param ImageConverterService $imageConverterService
     * @param ImageDataProvider $imageDataProvider
     * @param array $userImageUploadPreviewMax
     * @param array $userImageUploadThumbSize
     * @param Filesystem $filesystem
     * @param SvgStructureChecker $svgStructureChecker
     * @param SvgPngConverterInterface $svgPngConverter
     * @param VectorSvgConverterInterface $vectorSvgConverter
     */
    public function __construct(string $uploadPathPrivate, string $mediaBasePath, ImageConverterService $imageConverterService, ImageDataProvider $imageDataProvider, array $userImageUploadPreviewMax, array $userImageUploadThumbSize, Filesystem $filesystem, SvgStructureChecker $svgStructureChecker, SvgPngConverterInterface $svgPngConverter, VectorSvgConverterInterface $vectorSvgConverter)
    {
        $this->uploadPathPrivate = $uploadPathPrivate;
        $this->mediaBasePath = $mediaBasePath;
        $this->imageConverterService = $imageConverterService;
        $this->imageDataProvider = $imageDataProvider;
        $this->userImageUploadPreviewMax = $userImageUploadPreviewMax;
        $this->userImageUploadThumbSize = $userImageUploadThumbSize;
        $this->filesystem = $filesystem;
        $this->svgStructureChecker = $svgStructureChecker;
        $this->svgPngConverter = $svgPngConverter;
        $this->vectorSvgConverter = $vectorSvgConverter;
    }

    /**
     * @param UploadedFile $file
     *
     * @return ImageModel[]
     */
    public function upload(UploadedFile $file): array
    {
        $hash = md5($file->getFilename() . microtime() . $file->getSize());

        $original = $this->uploadOriginal($file, $hash);

        $sources = [];
        $sources[ImageDataProvider::PREVIEW] = $this->uploadPreview($original, $hash);
        $sources[ImageDataProvider::THUMBNAIL] = $this->uploadThumbnail($original, $hash);
        $sources[ImageDataProvider::ORIGINAL] = $original;

        $this->overrideOriginalSourceDimensionsWidthPreview($sources);

        return $sources;
    }

    /**
     * @param ImageModel[] $sources
     * @param bool $forceRasterFormat
     *
     * @return ImageModel
     */
    public function resetPreview(array $sources, bool $forceRasterFormat = false): ImageModel
    {
        $originalPath = $sources[ImageDataProvider::ORIGINAL]->getPath();
        $previewPath = pathinfo($sources[ImageDataProvider::PREVIEW]->getPath(), PATHINFO_DIRNAME) . DIRECTORY_SEPARATOR . pathinfo($sources[ImageDataProvider::PREVIEW]->getPath(), PATHINFO_FILENAME);

        return $this->createPreviewImage($originalPath, $previewPath, $forceRasterFormat);
    }

    /**
     * @param ImageModel $original
     * @param string $hash
     *
     * @return ImageModel
     */
    private function uploadPreview(ImageModel $original, string $hash): ImageModel
    {
        $directory = $this->mediaBasePath . self::PATH_PREVIEW;
        $this->filesystem->mkdir($directory, 0755);
        $sourcePath = $original->getPath();
        $targetPath = $directory . $hash;

        return $this->createPreviewImage($sourcePath, $targetPath);
    }

    /**
     * Create an image preview by paths and extension.
     *
     * @param string $sourcePath
     * @param string $targetPath
     * @param bool $forceRasterFormat
     *
     * @return ImageModel
     */
    private function createPreviewImage(string $sourcePath, string $targetPath, bool $forceRasterFormat = false): ImageModel
    {
        $sourceExtension = strtolower((new \SplFileInfo($sourcePath))->getExtension());
        if ('' === $sourceExtension) {
            throw new \InvalidArgumentException('File extension is missing');
        }

        if (false === $forceRasterFormat) {
            $preview = $this->createVectorPreviewImage($sourcePath, $sourceExtension, $targetPath);
            if ($preview) {
                return $preview;
            }
        }

        $sourcePath = $this->preScaleVectorFile($sourcePath, $sourceExtension);

        // scale down to a PNG image
        return $this->imageConverterService->scaleDown($sourcePath, $targetPath . '.png', $this->userImageUploadPreviewMax['width'], $this->userImageUploadPreviewMax['height']);
    }

    /**
     * @param ImageModel $original
     * @param string $hash
     *
     * @return ImageModel
     */
    private function uploadThumbnail(ImageModel $original, string $hash): ImageModel
    {
        return $this->imageConverterService->contain(
            $original->getPath(),
            $this->mediaBasePath . self::PATH_THUMBNAIL . $hash . '.png',
            $this->userImageUploadThumbSize['width'],
            $this->userImageUploadThumbSize['height']
        );
    }

    /**
     * @param UploadedFile $file
     * @param string $hash
     *
     * @return ImageModel
     */
    private function uploadOriginal(UploadedFile $file, string $hash): ImageModel
    {
        $directory = $this->uploadPathPrivate . '/' . $hash;
        $this->filesystem->mkdir($directory, 0755);

        $fileName = $file->getClientOriginalName();
        $file->move($directory, $fileName);

        $filePath = $directory . '/' . $fileName;

        if (IMG_JPG === @exif_imagetype($filePath)) {
            $this->correctJpegOrientation($filePath);
        }

        return $this->imageDataProvider->provide($filePath);
    }

    /**
     * @param string $sourcePath
     * @param string $sourceExtension
     * @param string $targetPath
     *
     * @return ImageModel|null
     */
    private function createVectorPreviewImage(string $sourcePath, string $sourceExtension, string $targetPath): ?ImageModel
    {
        if (!in_array($sourceExtension, self::VECTOR_FORMATS)) {
            return null;
        }

        if ('svg' === $sourceExtension) {
            $svgContent = file_get_contents($sourcePath);
        } else {
            $vector = Vector::withPath($sourcePath);
            $svg = $this->vectorSvgConverter->vectorToSvg($vector);
            $svgContent = $svg->getContent();
        }

        // check if vector file contains embedded image, then skip prescaling because of bad image upscaling quality
        if ($this->svgStructureChecker->checkIfSvgContainsEmbeddedImage($svgContent)) {
            return null;
        }

        $targetPath = '' === pathinfo($targetPath, PATHINFO_EXTENSION) ? $targetPath . '.svg' : $targetPath;
        file_put_contents($targetPath, $svgContent);

        return $this->imageDataProvider->provide($targetPath);
    }

    /**
     * @param string $sourcePath
     * @param string $sourceExtension
     *
     * @return string
     */
    public function preScaleVectorFile(string $sourcePath, string $sourceExtension): string
    {
        $sourceExtension = strtolower($sourceExtension);
        // allowed to scale this format
        if (!in_array($sourceExtension, self::VECTOR_FORMATS)) {
            return $sourcePath;
        }

        // convert to SVG and get the SVG string
        if ('svg' === $sourceExtension) {
            $svgContent = file_get_contents($sourcePath);
        } else {
            $vector = Vector::withPath($sourcePath);
            $svg = $this->vectorSvgConverter->vectorToSvg($vector);
            $svgContent = $svg->getContent();
        }

        // check if vector file contains embedded image, then skip prescaling because of bad image upscaling quality
        if ($this->svgStructureChecker->checkIfSvgContainsEmbeddedImage($svgContent)) {
            return $sourcePath;
        }

        $svg = Svg::withContent($svgContent);
        $png = $this->svgPngConverter->svgToPng($svg, 600);

        return $png->getPath();
    }

    /**
     * Replace 'original' dimensions with 'preview' dimensions, if the latter is greater.
     *
     * @param ImageModel[] $sources
     */
    public function overrideOriginalSourceDimensionsWidthPreview(array &$sources): void
    {
        if (!($sources[ImageDataProvider::ORIGINAL] instanceof ImageModel) || !($sources[ImageDataProvider::PREVIEW] instanceof ImageModel)) {
            throw new \InvalidArgumentException('Sources array expected to contain ImageModel objects');
        }

        if ($sources[ImageDataProvider::ORIGINAL]->getWidth() < $sources[ImageDataProvider::PREVIEW]->getWidth() &&
            $sources[ImageDataProvider::ORIGINAL]->getHeight() < $sources[ImageDataProvider::PREVIEW]->getHeight()
        ) {
            $sources[ImageDataProvider::ORIGINAL]->setWidth($sources[ImageDataProvider::PREVIEW]->getWidth());
            $sources[ImageDataProvider::ORIGINAL]->setHeight($sources[ImageDataProvider::PREVIEW]->getHeight());
        }
    }

    /**
     * Fix svg scaling issues: Set Preview dimensions to original dimensions.
     *
     * @param ImageModel[] $sources
     *
     * @return ImageModel[]
     */
    public function overridePreviewDimensionsOriginal(array $sources): array
    {
        if (!($sources[ImageDataProvider::ORIGINAL] instanceof ImageModel) || !($sources[ImageDataProvider::PREVIEW] instanceof ImageModel)) {
            throw new \InvalidArgumentException('Sources array expected to contain ImageModel objects');
        }

        $originalExtension = pathinfo($sources[ImageDataProvider::ORIGINAL]->getPath(), PATHINFO_EXTENSION);
        $previewExtension = pathinfo($sources[ImageDataProvider::PREVIEW]->getPath(), PATHINFO_EXTENSION);
        if ('svg' !== strtolower($previewExtension) || !in_array(strtolower($originalExtension), self::VECTOR_FORMATS)) {
            return $sources;
        }

        $sources[ImageDataProvider::PREVIEW]->setWidth($sources[ImageDataProvider::ORIGINAL]->getWidth());
        $sources[ImageDataProvider::PREVIEW]->setHeight($sources[ImageDataProvider::ORIGINAL]->getHeight());

        return $sources;
    }

    /**
     * @param $filePath
     */
    private function correctJpegOrientation($filePath): void
    {
        $exif = exif_read_data($filePath);
        if (false === $exif || !array_key_exists('Orientation', $exif)) {
            return;
        }

        $orientation = $exif['Orientation'];
        if (!array_key_exists($orientation, self::ROTATION_DEGREE_MAPPING)) {
            return;
        }

        $resource = @imagecreatefromjpeg($filePath);
        if (false === $resource) {
            return;
        }

        $resource = imagerotate($resource, self::ROTATION_DEGREE_MAPPING[$orientation], 0);

        if (in_array($orientation, [2, 5, 7, 4])) {
            imageflip($resource, IMG_FLIP_HORIZONTAL);
        }

        imagejpeg($resource, $filePath, 100);
    }
}
