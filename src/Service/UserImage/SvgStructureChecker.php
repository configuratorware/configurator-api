<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\UserImage;

use Redhotmagma\ConfiguratorApiBundle\Vector\Vector;
use Redhotmagma\ConfiguratorApiBundle\Vector\VectorSvgConverterInterface;

/**
 * @internal
 */
class SvgStructureChecker
{
    /**
     * @var VectorSvgConverterInterface
     */
    private $vectorSvgConverter;

    /**
     * @param VectorSvgConverterInterface $vectorSvgConverter
     */
    public function __construct(VectorSvgConverterInterface $vectorSvgConverter)
    {
        $this->vectorSvgConverter = $vectorSvgConverter;
    }

    /**
     * Check if a vector file contains embedded image or text.
     *
     * @param string $sourcePath
     *
     * @return bool
     */
    public function checkIfVectorFileContainsEmbeddedTextOrImage(string $sourcePath): bool
    {
        $svgContent = $this->vectorSvgConverter->vectorToSvg(Vector::withPath($sourcePath))->getContent();

        return $this->checkIfSvgContainsEmbeddedText($svgContent) || $this->checkIfSvgContainsEmbeddedImage($svgContent);
    }

    /**
     * Check if a vector file contains embedded image.
     *
     * @param string $svgContent
     *
     * @return bool
     */
    public function checkIfSvgContainsEmbeddedText(string $svgContent): bool
    {
        return false !== strpos($svgContent, '<text');
    }

    /**
     * Check if a vector file contains embedded image.
     *
     * @param string $svgContent
     *
     * @return bool
     */
    public function checkIfSvgContainsEmbeddedImage(string $svgContent): bool
    {
        return false !== strpos($svgContent, '<image');
    }
}
