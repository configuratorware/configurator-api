<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ClientResourcePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FontUploadArguments;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class ClientFont
{
    /**
     * @var ClientResourcePathsInterface
     */
    private $clientResourcePaths;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param ClientResourcePathsInterface $clientResourcePaths
     * @param ClientRepository $clientRepository
     * @param Filesystem $filesystem
     * @param string $mediaBasePath
     */
    public function __construct(ClientResourcePathsInterface $clientResourcePaths, ClientRepository $clientRepository, FileSystem $filesystem, string $mediaBasePath)
    {
        $this->clientResourcePaths = $clientResourcePaths;
        $this->clientRepository = $clientRepository;
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
    }

    /**
     * Upload a client font.
     *
     * @param int                       $id
     * @param FontUploadArguments $fontUpload
     *
     * @return bool
     */
    public function upload(int $id, FontUploadArguments $fontUpload): bool
    {
        $entity = $this->clientRepository->find($id);
        if (null === $entity) {
            throw new NotFoundException();
        }

        $clientIdentifier = $entity->getIdentifier();
        if (null === $clientIdentifier) {
            throw new NotFoundException();
        }

        $theme = $entity->getTheme();
        if (null === $theme) {
            $theme = new \stdClass();
        }

        // remove current font if already present
        if (isset($theme->font)) {
            $fullFontPath = $this->mediaBasePath . $theme->font;
            $this->filesystem->remove($fullFontPath);
        }

        // save font to filesystem
        $originalFileName = $fontUpload->uploadedFile->getClientOriginalName();
        $absoluteFontDir = $this->clientResourcePaths->getClientFontPath($clientIdentifier);
        $relativeFontDir = $this->clientResourcePaths->getClientFontPathRelative($clientIdentifier);
        $fontUpload->uploadedFile->move($absoluteFontDir, $originalFileName);

        // update theme with new font
        $theme->font = DIRECTORY_SEPARATOR . $relativeFontDir . DIRECTORY_SEPARATOR . $originalFileName;
        $entity->setTheme($theme);

        $this->clientRepository->save($entity);

        return true;
    }
}
