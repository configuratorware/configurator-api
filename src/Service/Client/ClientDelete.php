<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;

/**
 * @internal
 */
class ClientDelete
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ClientUserCheck
     */
    private $clientUserCheck;

    /**
     * ClientDelete constructor.
     *
     * @param ClientRepository $clientRepository
     * @param ClientUserCheck $clientUserCheck
     */
    public function __construct(ClientRepository $clientRepository, ClientUserCheck $clientUserCheck)
    {
        $this->clientRepository = $clientRepository;
        $this->clientUserCheck = $clientUserCheck;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Client $entity */
            $entity = $this->clientRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            $this->clientUserCheck->checkClient($entity);

            $this->deleteRelation($entity, 'ClientUser');
            $this->deleteRelation($entity, 'ClientChannel');
            $this->deleteRelation($entity, 'ClientText');
            $this->deleteRelation($entity, 'ImageGalleryImage');

            $this->clientRepository->delete($entity);
        }
    }

    /**
     * @param Client $entity
     * @param $relation
     */
    private function deleteRelation(Client $entity, string $relation)
    {
        $relationGetter = 'get' . $relation;

        if (!empty($entity->$relationGetter()->toArray())) {
            foreach ($entity->$relationGetter() as $relationEntity) {
                $this->clientRepository->delete($relationEntity);
            }
        }
    }
}
