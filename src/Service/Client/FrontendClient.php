<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientTextRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Client as ClientStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ClientText;

/**
 * @internal
 */
class FrontendClient
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ClientTextRepository
     */
    private $clientTextRepository;

    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * FrontendClient constructor.
     *
     * @param ClientRepository $clientRepository
     * @param ClientTextRepository $clientTextRepository
     * @param ChannelRepository $channelRepository
     * @param LanguageRepository $languageRepository
     */
    public function __construct(
        ClientRepository $clientRepository,
        ClientTextRepository $clientTextRepository,
        ChannelRepository $channelRepository,
        LanguageRepository $languageRepository
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientTextRepository = $clientTextRepository;
        $this->channelRepository = $channelRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @param string $clientIdentifier
     * @param string $channelIdentifier
     *
     * @return ClientStructure
     */
    public function getClient(string $clientIdentifier, string $channelIdentifier): ClientStructure
    {
        $client = $this->clientRepository->findOneByIdentifier($clientIdentifier);
        $channel = $this->channelRepository->findOneByIdentifier($channelIdentifier);

        $this->validateCombination($client, $channel);

        $clientStructure = new ClientStructure();
        $clientStructure->identifier = $client->getIdentifier();
        $clientStructure->channel = $channel->getIdentifier();
        $clientStructure->texts = $this->getClientTexts($client, $channel);
        $clientStructure->theme = $client->getTheme();

        $hidePrices = false;
        $channelSettings = $channel->getSettings();
        if (isset($channelSettings['hidePrices']) && true === $channelSettings['hidePrices']) {
            $hidePrices = true;
        }
        $clientStructure->hidePrices = $hidePrices;

        return $clientStructure;
    }

    /**
     * @param ClientEntity $client
     * @param Channel $channel
     */
    private function validateCombination(?ClientEntity $client, ?Channel $channel): void
    {
        if (empty($client) || empty($channel)) {
            throw new ValidationException();
        }

        // validation is only needed if both are not the default entries
        // combining with a default is always allowed
        if (ClientRepository::DEFAULT_CLIENT_IDENTIFIER != $client->getIdentifier()
            && ChannelRepository::DEFAULT_CHANNEL_IDENTIFIER != $channel->getIdentifier()) {
            $isValid = false;
            foreach ($client->getClientChannel() as $clientChannel) {
                if ($clientChannel->getChannel()->getIdentifier() === $channel->getIdentifier()) {
                    $isValid = true;
                }
            }

            if (false === $isValid) {
                throw new ValidationException();
            }
        }
    }

    /**
     * @param Client $client
     * @param Channel $channel
     *
     * @return ClientText|null
     */
    private function getClientTexts(Client $client, Channel $channel): ?ClientText
    {
        if (!defined('C_LANGUAGE_ISO')) {
            return null;
        }

        $language = $this->languageRepository->findOneBy(['iso' => C_LANGUAGE_ISO]);

        if (null === $language) {
            return null;
        }

        $clientTextEntity = $this->clientTextRepository->findOneBy([
            'client' => $client,
            'channel' => $channel,
            'language' => $language,
        ]);

        if (null === $clientTextEntity) {
            $clientTextEntity = $this->clientTextRepository->findOneBy([
                'client' => $client,
                'language' => $language,
            ]);
        }

        if (null === $clientTextEntity) {
            return null;
        }

        $clientTextStructure = new ClientText();
        $clientTextStructure->termsAndConditionsLink = $clientTextEntity->getTermsAndConditionsLink();
        $clientTextStructure->dataPrivacyLink = $clientTextEntity->getDataPrivacyLink();

        return $clientTextStructure;
    }
}
