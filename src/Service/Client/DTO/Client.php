<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO;

use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;

final class Client
{
    public ?string $identifier;

    public ?string $callToAction;

    public ?string $fromEmailAddress;

    /**
     * @var string[]
     */
    public array $toEmailAddresses;

    /**
     * @var string[]
     */
    public array $ccEmailAddresses;

    /**
     * @var string[]
     */
    public array $bccEmailAddresses;

    public bool $sendOfferRequestToCustomer = false;

    /**
     * @param string[] $toEmailAddresses
     * @param string[] $ccEmailAddresses
     * @param string[] $bccEmailAddresses
     */
    private function __construct(
        ?string $identifier,
        ?string $callToAction,
        ?string $fromEmailAddress,
        array $toEmailAddresses,
        array $ccEmailAddresses,
        array $bccEmailAddresses,
        bool $sendOfferRequestToCustomer = false
    ) {
        $this->identifier = $identifier;
        $this->callToAction = $callToAction;
        $this->fromEmailAddress = $fromEmailAddress;
        $this->toEmailAddresses = $toEmailAddresses;
        $this->ccEmailAddresses = $ccEmailAddresses;
        $this->bccEmailAddresses = $bccEmailAddresses;
        $this->sendOfferRequestToCustomer = $sendOfferRequestToCustomer;
    }

    public static function fromEntity(ClientEntity $client): self
    {
        return new self(
            $client->getIdentifier(),
            $client->getCallToAction(),
            $client->getFromEmailAddress(),
            self::createArray($client->getToEmailAddresses()),
            self::createArray($client->getCcEmailAddresses()),
            self::createArray($client->getBccEmailAddresses()),
            $client->getSendOfferRequestMailToCustomer()
        );
    }

    /**
     * @return string[]
     */
    private static function createArray(?string $string): array
    {
        if (null === $string) {
            return [];
        }

        return explode(';', $string);
    }
}
