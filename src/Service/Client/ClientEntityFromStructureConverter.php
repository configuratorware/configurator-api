<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientText;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser;
use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\UserRole;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Client as ClientStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ClientText as ClientTextStructure;

/**
 * @internal
 */
class ClientEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var UserEntityFromStructureConverter
     */
    private $userEntityFromStructureConverter;

    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param UserEntityFromStructureConverter $userEntityFromStructureConverter
     * @param ChannelRepository $channelRepository
     * @param RoleRepository $roleRepository
     * @param UserRepository $userRepository
     */
    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        UserEntityFromStructureConverter $userEntityFromStructureConverter,
        ChannelRepository $channelRepository,
        RoleRepository $roleRepository,
        UserRepository $userRepository
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->userEntityFromStructureConverter = $userEntityFromStructureConverter;
        $this->channelRepository = $channelRepository;
        $this->roleRepository = $roleRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param ClientStructure $structure
     * @param Client $entity
     * @param string $entityClassName
     *
     * @return Client
     */
    public function convertOne($structure, $entity = null, $entityClassName = Client::class): Client
    {
        if (null === $entity) {
            $entity = new Client();
        }

        $toEmailAddress = $structure->email ?: $structure->toEmailAddresses;

        /* @var Client $entity */
        $entity->setIdentifier($structure->identifier)
            ->setTheme($structure->theme)
            ->setCallToAction($structure->callToAction)
            ->setContactName($structure->contactName)
            ->setContactStreet($structure->contactStreet)
            ->setContactPostCode($structure->contactPostCode)
            ->setContactCity($structure->contactCity)
            ->setContactPhone($structure->contactPhone)
            ->setContactEmail($structure->contactEmail)
            ->setFromEmailAddress($structure->fromEmailAddress)
            ->setToEmailAddresses($toEmailAddress)
            ->setCcEmailAddresses($structure->ccEmailAddresses)
            ->setBccEmailAddresses($structure->bccEmailAddresses)
            ->setSendOfferRequestMailToCustomer($structure->sendOfferRequestMailToCustomer)
            ->setPdfHeaderHtml($structure->pdfHeaderHtml)
            ->setPdfFooterHtml($structure->pdfFooterHtml)
            ->setPdfHeaderText($structure->pdfHeaderText)
            ->setPdfFooterText($structure->pdfFooterText)
            ->setCustomCss($structure->customCss);

        $this->updateTexts($structure, $entity);

        // update/create user
        $entity = $this->updateClientUser($structure, $entity);

        // update channel relations
        $entity = $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
            $structure,
            $entity,
            'ClientChannel',
            'channels',
            'Channel'
        );

        $entity = $this->entityFromStructureConverter->addNewManyToManyRelations(
            $structure,
            $entity,
            ClientChannel::class,
            'channels',
            'Channel',
            $this->channelRepository
        );

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Client::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }

    /**
     * @param ClientStructure $structure
     * @param Client $entity
     */
    private function updateTexts(ClientStructure $structure, Client $entity): void
    {
        if (!is_iterable($structure->texts)) {
            return;
        }

        /** @var ClientTextStructure $textStructure */
        foreach ($structure->texts as $textStructure) {
            if (null === $textStructure->id) {
                break;
            }

            $textEntity = $entity->getClientText()
                ->filter(static function (ClientText $clientText) use ($textStructure) {
                    return $clientText->getId() == $textStructure->id;
                });

            $matchingTextEntity = $textEntity->first();

            if (null === $matchingTextEntity) {
                break;
            }

            $matchingTextEntity->setTermsAndConditionsLink($textStructure->termsAndConditionsLink);
            $matchingTextEntity->setDataPrivacyLink($textStructure->dataPrivacyLink);
        }
    }

    /**
     * @param ClientStructure $structure
     * @param Client $entity
     *
     * @return Client
     */
    private function updateClientUser(ClientStructure $structure, Client $entity): Client
    {
        if (!empty($structure->users)) {
            // since the id is not always populated from the frontend we are getting the user by its email
            // users are saved first to create new ones or update the password
            // then the client relations are updated
            foreach ($structure->users as $userStructure) {
                $userEntity = $this->userRepository->findOneByEmail($userStructure->email);
                $userEntity = $this->userEntityFromStructureConverter->convertOne($userStructure, $userEntity);

                // users saved with a client get the role "client" attached automatically
                if (false === $userEntity->hasRole(Role::CLIENT_ROLE)) {
                    $role = $this->roleRepository->findOneByRole(Role::CLIENT_ROLE);
                    $userRole = new UserRole();
                    $userRole->setUser($userEntity);
                    $userRole->setRole($role);

                    $userEntity->addUserRole($userRole);
                }

                $userEntity->setIsActive(true);
                $this->userRepository->save($userEntity);

                // set back user id to the structure to ba able to update client user relations
                $userStructure->id = $userStructure->id ?? $userEntity->getId();
            }

            $entity = $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
                $structure,
                $entity,
                'ClientUser',
                'users',
                'User'
            );

            $entity = $this->entityFromStructureConverter->addNewManyToManyRelations(
                $structure,
                $entity,
                ClientUser::class,
                'users',
                'User',
                $this->userRepository
            );
        }

        return $entity;
    }
}
