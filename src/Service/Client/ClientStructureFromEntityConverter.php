<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientChannel;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientUser;
use Redhotmagma\ConfiguratorApiBundle\Service\Channel\ChannelStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\User\UserStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Channel;
use Redhotmagma\ConfiguratorApiBundle\Structure\Client as ClientStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ClientText as ClientTextStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\User;

/**
 * @internal
 */
class ClientStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var ChannelStructureFromEntityConverter
     */
    private $channelStructureFromEntityConverter;

    /**
     * @var UserStructureFromEntityConverter
     */
    private $userStructureFromEntityConverter;

    /**
     * ClientStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param ChannelStructureFromEntityConverter $channelStructureFromEntityConverter
     * @param UserStructureFromEntityConverter $userStructureFromEntityConverter
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        ChannelStructureFromEntityConverter $channelStructureFromEntityConverter,
        UserStructureFromEntityConverter $userStructureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->channelStructureFromEntityConverter = $channelStructureFromEntityConverter;
        $this->userStructureFromEntityConverter = $userStructureFromEntityConverter;
    }

    /**
     * @param Client $entity
     * @param string $structureClassName
     *
     * @return ClientStructure
     */
    public function convertOne($entity, $structureClassName = ClientStructure::class)
    {
        /** @var ClientStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $this->addTextsToStructure($structure, $entity);
        $this->addUsersToStructure($structure, $entity);
        $this->addChannelsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ClientStructure::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param ClientStructure $structure
     * @param Client $entity
     */
    protected function addTextsToStructure(
        ClientStructure $structure,
        Client $entity
    ): void {
        $texts = [];

        foreach ($entity->getClientText() as $text) {
            /** @var ClientTextStructure $textStructure */
            $textStructure = $this->structureFromEntityConverter->convertOne(
                $text,
                ClientTextStructure::class
            );

            if (!empty($textStructure)) {
                $textStructure->channel = $this->channelStructureFromEntityConverter->convertOne(
                    $text->getChannel(),
                    Channel::class
                );
                $textStructure->language = $text->getLanguage()->getIso();
                $texts[] = $textStructure;
            }
        }

        $structure->texts = $texts;
    }

    /**
     * @param ClientStructure $structure
     * @param Client $entity
     */
    private function addUsersToStructure(
        ClientStructure $structure,
        Client $entity
    ): void {
        $users = [];

        /** @var ClientUser $clientUser */
        foreach ($entity->getClientUser() as $clientUser) {
            /** @var User $user */
            $user = $this->userStructureFromEntityConverter->convertOne($clientUser->getUser(), User::class);
            $user->password = '';

            $users[] = $user;
        }

        $structure->users = $users;
    }

    /**
     * @param ClientStructure $structure
     * @param Client $entity
     */
    private function addChannelsToStructure(ClientStructure $structure, Client $entity): void
    {
        $channels = [];

        /** @var ClientChannel $clientChannel */
        foreach ($entity->getClientChannel() as $clientChannel) {
            $channel = $this->channelStructureFromEntityConverter->convertOne(
                $clientChannel->getChannel(),
                Channel::class
            );
            $channels[] = $channel;
        }

        $structure->channels = $channels;
    }
}
