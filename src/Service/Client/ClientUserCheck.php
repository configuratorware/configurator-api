<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\UserRepository;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * @internal
 */
class ClientUserCheck
{
    /**
     * @var Security
     */
    private $security;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * ClientUserCheck constructor.
     *
     * @param Security $security
     * @param ClientRepository $clientRepository
     * @param UserRepository $userRepository
     */
    public function __construct(Security $security, ClientRepository $clientRepository, UserRepository $userRepository)
    {
        $this->security = $security;
        $this->clientRepository = $clientRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * @param Client[] $clients
     *
     * @throws NotFoundException
     */
    public function checkClients(array $clients): void
    {
        if ($this->currentUserHasClientRole()) {
            foreach ($clients as $client) {
                if ($this->currentUserHasClient($client)) {
                    return;
                }
            }

            throw new NotFoundException();
        }
    }

    /**
     * if the current user has the client role, only clients that are related to that user may be accessed.
     *
     * @param Client $client
     *
     * @throws NotFoundException
     */
    public function checkClient(Client $client): void
    {
        $hasClientRole = $this->currentUserHasClientRole();
        if (true === $hasClientRole) {
            $userHasClient = $this->currentUserHasClient($client);
            if (false === $userHasClient) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * check if the current user has the role client.
     *
     * @return bool
     */
    public function currentUserHasClientRole(): bool
    {
        /** @var User $currentUser */
        $currentUser = $this->security->getUser();

        return $currentUser->hasRole(Role::CLIENT_ROLE);
    }

    /**
     * @param string $userEmail
     * @param int|null $clientId
     *
     * @return bool
     */
    public function isUserEmailValidForClientId(string $userEmail, ?int $clientId): bool
    {
        $user = $this->userRepository->findOneByEmail($userEmail);

        if (null === $user) {
            return true;
        }

        if (!empty($clientId)) {
            $client = $this->clientRepository->findOneById($clientId);

            if (null !== $client && $user->hasClient($client)) {
                return true;
            }
        }

        return 0 === $user->getClientUser()->count();
    }

    /**
     * @param Client $client
     *
     * @return bool
     */
    private function currentUserHasClient(Client $client): bool
    {
        $userHasClient = false;

        /** @var User $currentUser */
        $currentUser = $this->security->getUser();
        foreach ($client->getClientUser() as $clientUser) {
            if ($clientUser->getUser()->getId() == $currentUser->getId()) {
                $userHasClient = true;
            }
        }

        return $userHasClient;
    }
}
