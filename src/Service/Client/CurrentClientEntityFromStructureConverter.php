<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientText;
use Redhotmagma\ConfiguratorApiBundle\Structure\CurrentClient;

/**
 * @internal
 */
class CurrentClientEntityFromStructureConverter
{
    /**
     * @param CurrentClient $structure
     * @param Client $entity
´     *
     * @return Client
     */
    public function convertOne(CurrentClient $structure, Client $entity): Client
    {
        $entity->setTheme($structure->theme)
            ->setCallToAction($structure->callToAction)
            ->setContactName($structure->contactName)
            ->setContactStreet($structure->contactStreet)
            ->setContactPostCode($structure->contactPostCode)
            ->setContactCity($structure->contactCity)
            ->setContactPhone($structure->contactPhone)
            ->setContactEmail($structure->contactEmail)
            ->setFromEmailAddress($structure->fromEmailAddress)
            ->setToEmailAddresses($structure->toEmailAddresses)
            ->setCcEmailAddresses($structure->ccEmailAddresses)
            ->setBccEmailAddresses($structure->bccEmailAddresses)
            ->setPdfHeaderHtml($structure->pdfHeaderHtml)
            ->setPdfFooterHtml($structure->pdfFooterHtml)
            ->setPdfHeaderText($structure->pdfHeaderText)
            ->setPdfFooterText($structure->pdfFooterText);

        $this->updateTexts($structure, $entity);

        return $entity;
    }

    /**
     * @param CurrentClient $structure
     * @param Client $entity
     */
    private function updateTexts(CurrentClient $structure, Client $entity): void
    {
        if (!is_iterable($structure->texts)) {
            return;
        }

        foreach ($structure->texts as $textStructure) {
            if (null === $textStructure->id) {
                break;
            }

            $matchingTextEntity = $entity->getClientText()
                ->filter(static function (ClientText $clientText) use ($textStructure) {
                    return $clientText->getId() == $textStructure->id;
                })->first();

            if (null === $matchingTextEntity) {
                break;
            }

            $matchingTextEntity->setTermsAndConditionsLink($textStructure->termsAndConditionsLink);
        }
    }
}
