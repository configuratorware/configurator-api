<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ClientResourcePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ClientLogoUploadArguments;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class ClientLogo
{
    /**
     * @var ClientResourcePathsInterface
     */
    private $clientResourcePaths;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param ClientResourcePathsInterface $clientResourcePaths
     * @param ClientRepository $clientRepository
     * @param Filesystem $filesystem
     * @param string $mediaBasePath
     */
    public function __construct(ClientResourcePathsInterface $clientResourcePaths, ClientRepository $clientRepository, FileSystem $filesystem, string $mediaBasePath)
    {
        $this->clientResourcePaths = $clientResourcePaths;
        $this->clientRepository = $clientRepository;
        $this->filesystem = $filesystem;
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
    }

    /**
     * @param int $id
     * @param ClientLogoUploadArguments $logoUpload
     *
     * @return void
     */
    public function upload(int $id, ClientLogoUploadArguments $logoUpload): void
    {
        $entity = $this->clientRepository->find($id);
        if (null === $entity) {
            throw new NotFoundException();
        }

        $clientIdentifier = $entity->getIdentifier();
        if (null === $clientIdentifier) {
            throw new NotFoundException();
        }

        $theme = $entity->getTheme();
        if (null === $theme) {
            $theme = new \stdClass();
        }

        // remove current logo if already present
        if (isset($theme->logo)) {
            $fullFontPath = $this->mediaBasePath . $theme->logo;
            $this->filesystem->remove($fullFontPath);
        }

        // save logo to filesystem
        $originalFileName = $logoUpload->uploadedFile->getClientOriginalName();
        $absoluteLogoDir = $this->clientResourcePaths->getClientLogoPath($clientIdentifier);
        $relativeLogoDir = $this->clientResourcePaths->getClientLogoPathRelative($clientIdentifier);
        $logoUpload->uploadedFile->move($absoluteLogoDir, $originalFileName);

        // update theme with new logo
        $theme->logo = DIRECTORY_SEPARATOR . $relativeLogoDir . DIRECTORY_SEPARATOR . $originalFileName;
        $entity->setTheme($theme);

        $this->clientRepository->save($entity);
    }
}
