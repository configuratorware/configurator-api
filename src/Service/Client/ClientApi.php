<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Client;
use Redhotmagma\ConfiguratorApiBundle\Structure\CurrentClient;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ClientLogoUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\FontUploadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class ClientApi
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ClientStructureFromEntityConverter
     */
    private $clientStructureFromEntityConverter;

    /**
     * @var CurrentClientStructureFromEntityConverter
     */
    private $currentClientStructureFromEntityConverter;

    /**
     * @var ClientSave
     */
    private $clientSave;

    /**
     * @var CurrentClientSave
     */
    private $currentClientSave;

    /**
     * @var ClientDelete
     */
    private $clientDelete;

    /**
     * @var ClientTextGenerator
     */
    private $clientTextGenerator;

    /**
     * @var ClientFont
     */
    private $clientFont;

    /**
     * @var ClientLogo
     */
    private $clientLogo;

    /**
     * @var FrontendClient
     */
    private $frontendClient;

    /**
     * ClientApi constructor.
     *
     * @param ClientRepository $clientRepository
     * @param ClientStructureFromEntityConverter $clientStructureFromEntityConverter
     * @param ClientSave $clientSave
     * @param CurrentClientSave $currentClientSave
     * @param ClientDelete $clientDelete
     * @param ClientTextGenerator $clientTextGenerator
     * @param ClientFont $clientFont
     * @param ClientLogo $clientLogo
     * @param FrontendClient $frontendClient
     * @param CurrentClientStructureFromEntityConverter $currentClientStructureFromEntityConverter
     */
    public function __construct(
        ClientRepository $clientRepository,
        ClientStructureFromEntityConverter $clientStructureFromEntityConverter,
        ClientSave $clientSave,
        CurrentClientSave $currentClientSave,
        ClientDelete $clientDelete,
        ClientTextGenerator $clientTextGenerator,
        ClientFont $clientFont,
        ClientLogo $clientLogo,
        FrontendClient $frontendClient,
        CurrentClientStructureFromEntityConverter $currentClientStructureFromEntityConverter
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientStructureFromEntityConverter = $clientStructureFromEntityConverter;
        $this->clientSave = $clientSave;
        $this->currentClientSave = $currentClientSave;
        $this->clientDelete = $clientDelete;
        $this->clientTextGenerator = $clientTextGenerator;
        $this->clientFont = $clientFont;
        $this->clientLogo = $clientLogo;
        $this->frontendClient = $frontendClient;
        $this->currentClientStructureFromEntityConverter = $currentClientStructureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $arguments->filters['_no_default_filter'] = true;

        $entities = $this->clientRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->clientStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->clientRepository->fetchListCount($arguments, $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return Client
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): Client
    {
        $entity = $this->clientRepository->findOneById($id);
        if (null === $entity) {
            throw new NotFoundException();
        }

        $this->clientTextGenerator->generate($entity);
        $this->clientRepository->save($entity);

        return $this->clientStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @return Client
     */
    public function getDefault(): Client
    {
        $client = $this->clientRepository->findOneBy(['identifier' => ClientRepository::DEFAULT_CLIENT_IDENTIFIER]);
        $this->clientTextGenerator->generate($client);
        $this->clientRepository->save($client);

        return $this->clientStructureFromEntityConverter->convertOne($client);
    }

    /**
     * @param Client $client
     *
     * @return Client
     */
    public function save(Client $client): Client
    {
        return $this->clientSave->save($client);
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $this->clientDelete->delete($id);
    }

    /**
     * @param string $clientIdentifier
     * @param string $channelIdentifier
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Client
     */
    public function frontendClient(
        string $clientIdentifier,
        string $channelIdentifier
    ): \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Client {
        $client = $this->frontendClient->getClient($clientIdentifier, $channelIdentifier);

        return $client;
    }

    /**
     * @param int $id
     * @param FontUploadArguments $fontUpload
     *
     * @return bool
     */
    public function uploadFont(int $id, FontUploadArguments $fontUpload): bool
    {
        return $this->clientFont->upload($id, $fontUpload);
    }

    /**
     * @param int $id
     * @param ClientLogoUploadArguments $fontUpload
     *
     * @return void
     */
    public function uploadLogo(int $id, ClientLogoUploadArguments $fontUpload): void
    {
        $this->clientLogo->upload($id, $fontUpload);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier'];
    }

    /**
     * @param ClientEntity $client
     *
     * @return CurrentClient
     */
    public function getCurrent(ClientEntity $client): CurrentClient
    {
        $this->clientTextGenerator->generate($client);
        $this->clientRepository->save($client);

        return $this->currentClientStructureFromEntityConverter->convertOne($client);
    }

    /**
     * @param CurrentClient $client
     *
     * @return CurrentClient
     */
    public function saveCurrent(CurrentClient $client): CurrentClient
    {
        return $this->currentClientSave->save($client);
    }
}
