<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client as ClientDTO;

final class Client
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * Client constructor.
     *
     * @param ClientRepository $clientRepository
     */
    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function getClient(): ClientDTO
    {
        $clientIdentifier = ClientRepository::DEFAULT_CLIENT_IDENTIFIER;

        if (defined('C_CLIENT')) {
            $clientIdentifier = C_CLIENT;
        }

        return $this->loadByClientIdentifier($clientIdentifier);
    }

    private function loadByClientIdentifier(string $clientIdentifier): ClientDTO
    {
        return ClientDTO::fromEntity($this->clientRepository->findOneByIdentifier($clientIdentifier));
    }
}
