<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Exception\CurrentClientNotFound;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\CurrentClient;

/**
 * @internal
 */
class CurrentClientSave
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ClientEntityFromStructureConverter
     */
    private $currentClientEntityFromStructureConverter;

    /**
     * @var ClientStructureFromEntityConverter
     */
    private $currentClientStructureFromEntityConverter;

    /**
     * @param ClientRepository $clientRepository
     * @param CurrentClientEntityFromStructureConverter $currentClientEntityFromStructureConverter
     * @param CurrentClientStructureFromEntityConverter $currentClientStructureFromEntityConverter
     */
    public function __construct(ClientRepository $clientRepository, CurrentClientEntityFromStructureConverter $currentClientEntityFromStructureConverter, CurrentClientStructureFromEntityConverter $currentClientStructureFromEntityConverter)
    {
        $this->clientRepository = $clientRepository;
        $this->currentClientEntityFromStructureConverter = $currentClientEntityFromStructureConverter;
        $this->currentClientStructureFromEntityConverter = $currentClientStructureFromEntityConverter;
    }

    /**
     * @param CurrentClient $client
     *
     * @return CurrentClient
     */
    public function save(CurrentClient $client): CurrentClient
    {
        $entity = null;
        if (isset($client->id) && $client->id > 0) {
            $entity = $this->clientRepository->findOneById($client->id);
        }

        if (null === $entity) {
            throw CurrentClientNotFound::withId($client->id);
        }

        $entity = $this->currentClientEntityFromStructureConverter->convertOne($client, $entity);

        $this->clientRepository->save($entity);

        $entity = $this->clientRepository->findOneById($entity->getId());

        return $this->currentClientStructureFromEntityConverter->convertOne($entity);
    }
}
