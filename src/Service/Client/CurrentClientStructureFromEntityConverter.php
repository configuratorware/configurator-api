<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Structure\ClientText as ClientTextStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\CurrentClient as CurrentClientStructure;

/**
 * @internal
 */
class CurrentClientStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * ClientStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param Client $entity
     * @param string $structureClassName
     *
     * @return CurrentClientStructure
     */
    public function convertOne($entity, $structureClassName = CurrentClientStructure::class)
    {
        /** @var CurrentClientStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = CurrentClientStructure::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }

    /**
     * @param CurrentClientStructure $structure
     * @param Client $entity
     */
    protected function addTextsToStructure(
        CurrentClientStructure $structure,
        Client $entity
    ): void {
        $texts = [];

        foreach ($entity->getClientText() as $text) {
            /** @var ClientTextStructure $textStructure */
            $textStructure = $this->structureFromEntityConverter->convertOne(
                $text,
                ClientTextStructure::class
            );

            if (null !== $textStructure) {
                $textStructure->language = $text->getLanguage()->getIso();
                $texts[] = $textStructure;
            }
        }

        $structure->texts = $texts;
    }
}
