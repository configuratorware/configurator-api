<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Entity\ClientText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Language;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;

/**
 * @internal
 */
class ClientTextGenerator
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @param LanguageRepository $languageRepository
     * @param ChannelRepository $channelRepository
     */
    public function __construct(LanguageRepository $languageRepository, ChannelRepository $channelRepository)
    {
        $this->languageRepository = $languageRepository;
        $this->channelRepository = $channelRepository;
    }

    /**
     * @param Client $client
     */
    public function generate(Client $client): void
    {
        $languages = $this->languageRepository->findAll();

        if (null === $languages) {
            return;
        }

        $channelsForClient = array_map(function ($clientChannel) { return $clientChannel->getChannel(); }, $client->getClientChannel()->toArray());
        if (0 === count($channelsForClient)) {
            $channelsForClient = [$this->channelRepository->findOneByIdentifier(Channel::DEFAULT_IDENTIFIER)];
        }

        foreach ($channelsForClient as $channelForClient) {
            foreach ($languages as $language) {
                $languageId = $language->getId();

                if (!$this->isClientTextExisting($client->getClientText(), $languageId, $channelForClient->getId())) {
                    $client->addClientText($this->createClientText($client, $language, $channelForClient));
                }
            }
        }
    }

    /**
     * @param Collection $clientTexts
     * @param int $languageId
     * @param int $channelId
     *
     * @return bool
     */
    private function isClientTextExisting(Collection $clientTexts, int $languageId, int $channelId): bool
    {
        foreach ($clientTexts as $clientText) {
            if ($languageId == $clientText->getLanguage()->getId() && $channelId == $clientText->getChannel()->getId()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Client $client
     * @param Language $language
     *
     * @return ClientText
     */
    private function createClientText(Client $client, Language $language, Channel $channel): ClientText
    {
        $clientText = new ClientText();
        $clientText->setClient($client);
        $clientText->setChannel($channel);
        $clientText->setLanguage($language);

        return $clientText;
    }
}
