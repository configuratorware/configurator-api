<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Client;

use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Client;

/**
 * @internal
 */
class ClientSave
{
    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ClientEntityFromStructureConverter
     */
    private $clientEntityFromStructureConverter;

    /**
     * @var ClientStructureFromEntityConverter
     */
    private $clientStructureFromEntityConverter;

    /**
     * ClientSave constructor.
     *
     * @param ClientRepository $clientRepository
     * @param ClientEntityFromStructureConverter $clientEntityFromStructureConverter
     * @param ClientStructureFromEntityConverter $clientStructureFromEntityConverter
     */
    public function __construct(
        ClientRepository $clientRepository,
        ClientEntityFromStructureConverter $clientEntityFromStructureConverter,
        ClientStructureFromEntityConverter $clientStructureFromEntityConverter
    ) {
        $this->clientRepository = $clientRepository;
        $this->clientEntityFromStructureConverter = $clientEntityFromStructureConverter;
        $this->clientStructureFromEntityConverter = $clientStructureFromEntityConverter;
    }

    /**
     * @param Client $client
     *
     * @return Client
     */
    public function save(Client $client): Client
    {
        $entity = null;
        if (isset($client->id) && $client->id > 0) {
            $entity = $this->clientRepository->findOneById($client->id);
        }

        $entity = $this->clientEntityFromStructureConverter->convertOne($client, $entity);

        $entity = $this->clientRepository->save($entity);

        $entity = $this->clientRepository->findOneById($entity->getId());

        return $this->clientStructureFromEntityConverter->convertOne($entity);
    }
}
