<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion;

/**
 * @internal
 */
final class LatestApiVersionFinder
{
    /**
     * @var ReleasedApiVersionRepositoryInterface
     */
    private $releasedVersionRepository;

    /**
     * @param ReleasedApiVersionRepositoryInterface $releasedVersionRepository
     */
    public function __construct(ReleasedApiVersionRepositoryInterface $releasedVersionRepository)
    {
        $this->releasedVersionRepository = $releasedVersionRepository;
    }

    /**
     * @return ReleasedApiVersion
     */
    public function findStable(): ?ReleasedApiVersion
    {
        $releasedVersions = $this->releasedVersionRepository->findVersions();

        $stableVersions = [];
        foreach ($releasedVersions as $releasedVersion) {
            if ($releasedVersion->isStable()) {
                $stableVersions[$releasedVersion->getTag()] = $releasedVersion;
            }
        }

        krsort($stableVersions, SORT_NATURAL);

        $latestVersion = current($stableVersions);
        if (!$latestVersion instanceof ReleasedApiVersion) {
            return null;
        }

        return $latestVersion;
    }
}
