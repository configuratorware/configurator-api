<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion;

use Redhotmagma\ConfiguratorApiBundle\Exception\ExternalResourceException;

interface ReleasedApiVersionRepositoryInterface
{
    /**
     * @return ReleasedApiVersion[]
     *
     * @throws ExternalResourceException
     */
    public function findVersions(): array;
}
