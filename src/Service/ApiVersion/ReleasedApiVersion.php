<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion;

/**
 * @internal
 */
final class ReleasedApiVersion
{
    private const STABLE_PATTERN = '/^(\d+)\.(\d+)\.(\d+)$/';

    /**
     * @var string
     */
    private $tag;

    /**
     * @param string $tag
     */
    private function __construct(string $tag)
    {
        $this->tag = $tag;
    }

    /**
     * @param string $tag
     *
     * @return static
     */
    public static function fromState(string $tag): self
    {
        return new self($tag);
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @return bool
     */
    public function isStable(): bool
    {
        return 1 === preg_match(self::STABLE_PATTERN, $this->tag);
    }
}
