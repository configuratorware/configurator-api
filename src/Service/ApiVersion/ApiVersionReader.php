<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion;

/**
 * @internal
 */
class ApiVersionReader
{
    /**
     * @var string
     */
    public const PACKAGE_NAME = 'configuratorware/configurator-api';

    /**
     * @var string
     */
    public const PACKAGE_NAME_DEPRECATED = 'redhotmagma/configuratorapibundle';

    /**
     * @var string
     */
    private $projectDir;

    /**
     * @var array
     */
    private $apiPackageStore = [];

    public function __construct(string $projectDir)
    {
        $this->projectDir = rtrim($projectDir, '/');
    }

    /**
     * @return string
     */
    public function getApiVersion(): string
    {
        $config = $this->getPackageConfig();
        if (isset($config['version'])) {
            return $config['version'];
        }

        return 'unknown';
    }

    /**
     * @return string
     */
    public function getApireferenceHash(): string
    {
        $config = $this->getPackageConfig();
        if (isset($config['source']['reference'])) {
            return $config['source']['reference'];
        }

        return 'unknown';
    }

    /**
     * Get the package config array.
     *
     * @return array
     */
    private function getPackageConfig(): array
    {
        if (empty($this->apiPackageStore)) {
            $this->apiPackageStore = $this->parsePackageConfigFromFile();
        }

        return $this->apiPackageStore;
    }

    /**
     * Read package config from composer.lock file.
     *
     * @return array
     */
    private function parsePackageConfigFromFile(): array
    {
        // check for compose.lock file existence
        $composerLockPath = $this->getComposerFilePath();
        if (!file_exists($composerLockPath)) {
            return [];
        }

        // read and parse lock file, check required fields
        $composerLockArray = json_decode(file_get_contents($composerLockPath), true);
        if (!is_array($composerLockArray)
            || !isset($composerLockArray['packages'])
            || !is_array($composerLockArray['packages'])
        ) {
            return [];
        }

        // get required package data
        $apiPackage = array_filter(
            $composerLockArray['packages'],
            function ($package) {
                return isset($package['name']) && (self::PACKAGE_NAME === $package['name'] || self::PACKAGE_NAME_DEPRECATED === $package['name']);
            }
        );

        // check if required package is found
        if (empty($apiPackage)) {
            return [];
        }

        // return package data
        return reset($apiPackage);
    }

    /**
     * Get the path of the composer file.
     *
     * @return string
     */
    private function getComposerFilePath(): string
    {
        return $this->projectDir . '/composer.lock';
    }
}
