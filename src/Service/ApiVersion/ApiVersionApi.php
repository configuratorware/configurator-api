<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ApiVersion;

use Redhotmagma\ConfiguratorApiBundle\Structure\ApiVersion;

class ApiVersionApi
{
    /**
     * @var ApiVersionReader
     */
    private $apiVersionReader;

    /**
     * @var LatestApiVersionFinder
     */
    private $latestApiVersionFinder;

    /**
     * @param ApiVersionReader $apiVersionReader
     * @param LatestApiVersionFinder $latestApiVersionFinder
     */
    public function __construct(ApiVersionReader $apiVersionReader, LatestApiVersionFinder $latestApiVersionFinder)
    {
        $this->apiVersionReader = $apiVersionReader;
        $this->latestApiVersionFinder = $latestApiVersionFinder;
    }

    /**
     * @return ApiVersion
     */
    public function getVersionInfo(): ApiVersion
    {
        $releasedApiVersion = $this->latestApiVersionFinder->findStable();

        $structure = new ApiVersion();
        $structure->version = $this->apiVersionReader->getApiVersion();
        $structure->reference = $this->apiVersionReader->getApireferenceHash();
        $structure->latestReleasedVersion = null !== $releasedApiVersion ? $releasedApiVersion->getTag() : '';

        return $structure;
    }
}
