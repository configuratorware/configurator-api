<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataCheck;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheckPlaceholder;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item as ItemStructure;

/**
 * @internal
 */
class ItemCheck
{
    private const TITLE = 'item.title';
    private const VARIANTS = 'item.variants';
    private const CONFIGURATION_MODE = 'item.configuration_mode';

    /**
     * @param ItemStructure $item
     *
     * @return DataCheck[]
     */
    public function checkItem(ItemStructure $item): array
    {
        $checks = [];

        if (empty($item->title)) {
            $checks[] = new DataCheck(self::TITLE, DataCheck::TYPE_NOTICE);
        } else {
            $checks[] = new DataCheck(self::TITLE, DataCheck::TYPE_NEUTRAL);
        }

        if (isset($item->configurationModes)) {
            foreach ($item->configurationModes as $configurationMode) {
                $checks[] = new DataCheck(
                    self::CONFIGURATION_MODE . '.' . $configurationMode->identifier,
                    $configurationMode->itemStatus->item_available ? DataCheck::TYPE_NEUTRAL : DataCheck::TYPE_NOTICE,
                    [
                        new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AVAILABLE, $configurationMode->itemStatus->item_available),
                    ]
                );
            }
        }

        return $checks;
    }

    /**
     * @param Item[] $variants
     *
     * @return DataCheck[]
     */
    public function checkVariants(array $variants): array
    {
        return [
            new DataCheck(
                self::VARIANTS,
                DataCheck::TYPE_NEUTRAL,
                [
                    new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AMOUNT, count($variants)),
                ]
            ),
        ];
    }
}
