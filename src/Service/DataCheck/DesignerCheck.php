<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataCheck;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheckPlaceholder;

/**
 * @internal
 */
class DesignerCheck
{
    private const DESIGN_VIEWS = 'designer.views';
    private const DESIGN_AREAS = 'designer.design_areas';
    private const DESIGN_AREA = 'designer.design_area';
    private const PRODUCTION_METHOD = 'designer.design_area.production_method';

    /**
     * @param DesignView[] $designViews
     *
     * @return DataCheck[]
     */
    public function checkDesignViews(array $designViews): array
    {
        $amount = count($designViews);
        $type = $amount > 0 ? DataCheck::TYPE_NEUTRAL : DataCheck::TYPE_NOTICE;

        $placeHolders = [
            new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AMOUNT, $amount),
        ];
        foreach ($designViews as $designView) {
            $value = $designView->getTranslatedTitle();
            if ('' === $value) {
                $value = $designView->getIdentifier();
            }
            $placeHolders[] = new DataCheckPlaceholder(DataCheckPlaceholder::KEY_TITLE, $value);
        }

        return [new DataCheck(self::DESIGN_VIEWS, $type, $placeHolders)];
    }

    /**
     * @param DesignArea[] $designAreas
     *
     * @return DataCheck[]
     */
    public function checkDesignAreas(array $designAreas): array
    {
        $amount = count($designAreas);
        $type = $amount > 0 ? DataCheck::TYPE_NEUTRAL : DataCheck::TYPE_ERROR;
        $rootPlaceHolder = [new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AMOUNT, $amount)];

        $designAreaChildren = [];
        foreach ($designAreas as $designArea) {
            $productionMethodChildren = [];
            foreach ($designArea->getDesignAreaDesignProductionMethod() as $areaProductionMethod) {
                $value = $areaProductionMethod->getDesignProductionMethod()->getTranslatedTitle();
                if ('' === $value) {
                    $value = $areaProductionMethod->getDesignProductionMethod()->getIdentifier();
                }
                $productionMethodChildren[] = new DataCheck(
                    self::PRODUCTION_METHOD,
                    DataCheck::TYPE_NEUTRAL,
                    [
                        new DataCheckPlaceholder(DataCheckPlaceholder::KEY_TITLE,
                            $value),
                    ]
                );
            }

            $value = $designArea->getTranslatedTitle();
            if ('' === $value) {
                $value = $designArea->getIdentifier();
            }

            $designAreaChildren[] = new DataCheck(
                self::DESIGN_AREA,
                DataCheck::TYPE_NEUTRAL,
                [new DataCheckPlaceholder(DataCheckPlaceholder::KEY_TITLE, $value)],
                $productionMethodChildren
            );
        }

        return [new DataCheck(self::DESIGN_AREAS, $type, $rootPlaceHolder, $designAreaChildren)];
    }
}
