<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataCheck;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationApi;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;

class DataCheckApi
{
    /**
     * @var ConfigurationApi
     */
    private $configurationApi;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var FrontendOptionApi
     */
    private $frontendOptionApi;

    /**
     * @var CreatorCheck
     */
    private $creatorCheck;

    /**
     * @var DesignerCheck
     */
    private $designerCheck;

    /**
     * @var ItemCheck
     */
    private $itemCheck;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @param ConfigurationApi $configurationApi
     * @param ConfigurationRepository $configurationRepository
     * @param CreatorCheck $creatorCheck
     * @param DesignerCheck $designerCheck
     * @param FrontendOptionApi $frontendOptionApi
     * @param ItemCheck $itemCheck
     * @param ItemRepository $itemRepository
     * @param OptionRepository $optionRepository
     */
    public function __construct(
        ConfigurationApi $configurationApi,
        ConfigurationRepository $configurationRepository,
        CreatorCheck $creatorCheck,
        DesignerCheck $designerCheck,
        FrontendOptionApi $frontendOptionApi,
        ItemCheck $itemCheck,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository
    ) {
        $this->configurationApi = $configurationApi;
        $this->configurationRepository = $configurationRepository;
        $this->creatorCheck = $creatorCheck;
        $this->designerCheck = $designerCheck;
        $this->frontendOptionApi = $frontendOptionApi;
        $this->itemCheck = $itemCheck;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param string $itemIdentifier
     * @param ControlParameters $controlParameters
     *
     * @throws NotFoundException
     *
     * @return DataCheck[]
     */
    public function getList(string $itemIdentifier, ControlParameters $controlParameters): array
    {
        $configuration = $this->configurationApi->loadByItemIdentifier($itemIdentifier, $controlParameters);
        if (null === $configuration) {
            throw new NotFoundException();
        }
        $optionsByComponentIdentifier = [];
        $componentsByIdentifier = [];
        $optionsByIdentifier = [];
        $optionIdentifiers = [];
        foreach ($configuration->optionclassifications as $optionClassification) {
            $componentsByIdentifier[$optionClassification->identifier] = $optionClassification;
            $itemOptions = $this->frontendOptionApi->getMatchingOptions($configuration, $optionClassification->identifier);
            $optionsByComponentIdentifier[$optionClassification->identifier] = $itemOptions;
            foreach ($itemOptions as $option) {
                $optionIdentifiers[] = $option->identifier;
                $optionsByIdentifier[$option->identifier] = $option;
            }
        }

        $optionRules = [];
        $optionEntities = $this->optionRepository->findOptionsByIdentifiers($optionIdentifiers);
        foreach ($optionEntities as $option) {
            array_merge($optionRules, $option->getSortedRules($itemIdentifier));
        }

        $item = $this->itemRepository->findOneByIdentifier($itemIdentifier);

        $defaultConfiguration = $this->configurationRepository->getBaseconfigurationByItemIdentifier($itemIdentifier);

        return array_merge(
            $this->itemCheck->checkItem($configuration->item),
            $this->itemCheck->checkVariants($item->getItem()->toArray()),
            $this->creatorCheck->checkComponents($optionsByComponentIdentifier, $componentsByIdentifier),
            $this->creatorCheck->checkDefaultConfiguration($defaultConfiguration, $componentsByIdentifier, $optionsByIdentifier),
            $this->creatorCheck->checkRules($item->getSortedRules(), $optionRules),
            $this->designerCheck->checkDesignViews($item->getDesignView()->toArray()),
            $this->designerCheck->checkDesignAreas($item->getDesignArea()->toArray())
        );
    }
}
