<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DataCheck;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheck;
use Redhotmagma\ConfiguratorApiBundle\Structure\DataCheckPlaceholder;

/**
 * @internal
 */
class CreatorCheck
{
    private const COMPONENT = 'creator.component';
    private const COMPONENT_OPTION = 'creator.component.option';
    private const RULE = 'creator.rule';
    private const DEFAULT_CONFIGURATION = 'creator.default.configuration';
    private const DEFAULT_CONFIGURATION_OPTION = 'creator.default.option';

    /**
     * @param array $optionsByComponentIdentifier
     * @param array $componentsByIdentifier
     *
     * @return DataCheck[]
     */
    public function checkComponents(array $optionsByComponentIdentifier, array $componentsByIdentifier): array
    {
        $componentAmount = count($optionsByComponentIdentifier);

        if ($componentAmount > 0) {
            $rootType = DataCheck::TYPE_NEUTRAL;
            $rootPlaceHolders = [new DataCheckPlaceholder(DataCheckPlaceholder::KEY_COMPONENTS_AMOUNT, $componentAmount)];
        } else {
            $rootType = DataCheck::TYPE_ERROR;
            $rootPlaceHolders = [];
        }

        $children = [];
        foreach ($optionsByComponentIdentifier as $componentIdentifier => $options) {
            $optionsAmount = count($options);
            $componentName = $componentsByIdentifier[$componentIdentifier]->title ?? '';

            $children[] = new DataCheck(
                'creator.component.title',
                0 === $optionsAmount ? DataCheck::TYPE_ERROR : DataCheck::TYPE_NEUTRAL,
                [
                    new DataCheckPlaceholder(DataCheckPlaceholder::KEY_COMPONENT_NAME, $componentName),
                    new DataCheckPlaceholder(DataCheckPlaceholder::KEY_OPTIONS_AMOUNT, $optionsAmount),
                ],
                $this->checkOptionsForComponent($componentName, $options)
            );
        }

        return [new DataCheck(self::COMPONENT, $rootType, $rootPlaceHolders, $children)];
    }

    /**
     * @param Rule[] $itemRules
     * @param Rule[] $optionRules
     *
     * @return DataCheck[]
     */
    public function checkRules(array $itemRules, array $optionRules): array
    {
        $count = count($itemRules) + count($optionRules);

        return [
            new DataCheck(
                self::RULE,
                DataCheck::TYPE_NEUTRAL,
                [
                    new DataCheckPlaceholder(DataCheckPlaceholder::KEY_AMOUNT, $count),
                ]
            ),
        ];
    }

    /**
     * @param Configuration|null $defaultConfiguration
     * @param array $componentsByIdentifier
     * @param array $optionsByIdentifier
     *
     * @return DataCheck[]
     */
    public function checkDefaultConfiguration(
        ?Configuration $defaultConfiguration,
        array $componentsByIdentifier,
        array $optionsByIdentifier
    ): array {
        if (null === $defaultConfiguration || empty($defaultConfiguration->getSelectedoptions())) {
            return [
                new DataCheck(
                    self::DEFAULT_CONFIGURATION,
                    DataCheck::TYPE_ERROR
                ),
            ];
        }
        $children = [];
        foreach ($defaultConfiguration->getSelectedoptions() as $componentIdentifier => $optionsSet) {
            $componentName = $componentsByIdentifier[$componentIdentifier]->title ?? $componentIdentifier;
            foreach ($optionsSet as $option) {
                $optionTitle = isset($optionsByIdentifier[$option['identifier']]) ?
                    $optionsByIdentifier[$option['identifier']]->title : $option['identifier'];
                $children[] = new DataCheck(
                    self::DEFAULT_CONFIGURATION_OPTION,
                    empty($option['identifier']) ? DataCheck::TYPE_NOTICE : DataCheck::TYPE_NEUTRAL,
                    [
                        new DataCheckPlaceholder(DataCheckPlaceholder::KEY_COMPONENT_NAME, $componentName),
                        new DataCheckPlaceholder(DataCheckPlaceholder::KEY_OPTION, $optionTitle),
                    ]
                );
            }
        }

        return [new DataCheck(self::DEFAULT_CONFIGURATION, DataCheck::TYPE_NEUTRAL, [], $children)];
    }

    /**
     * @param string $componentName
     * @param array $options
     *
     * @return DataCheck[]
     */
    private function checkOptionsForComponent(string $componentName, array $options): array
    {
        $placeHolders = [
            new DataCheckPlaceholder(DataCheckPlaceholder::KEY_COMPONENT_NAME, $componentName),
            new DataCheckPlaceholder(DataCheckPlaceholder::KEY_OPTIONS_AMOUNT, count($options)),
        ];

        if ([] !== $options) {
            $placeHolders[] = new DataCheckPlaceholder(DataCheckPlaceholder::KEY_OPTIONS, implode(', ', array_column($options, 'title')));
        } else {
            $placeHolders[] = new DataCheckPlaceholder(DataCheckPlaceholder::KEY_OPTIONS, 'n/a');
        }

        return [new DataCheck(self::COMPONENT_OPTION, DataCheck::TYPE_NEUTRAL, $placeHolders)];
    }
}
