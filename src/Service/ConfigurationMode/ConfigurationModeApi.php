<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationMode\FrontendDesignerDataEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as ConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerData;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ConfigurationModeApi
{
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getData(ConfigurationStructure $configuration): DesignerData
    {
        $designerDataEvent = new FrontendDesignerDataEvent($configuration);
        $this->eventDispatcher->dispatch($designerDataEvent, FrontendDesignerDataEvent::NAME);

        return $designerDataEvent->getDesignerData();
    }
}
