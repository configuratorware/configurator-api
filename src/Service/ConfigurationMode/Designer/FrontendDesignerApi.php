<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\Designer;

use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\FrontendDesignerData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as ConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerData;

/**
 * @deprecated use \Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode\ConfigurationModeApi
 */
class FrontendDesignerApi
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var FrontendDesignerData
     */
    private $frontendDesignerData;

    /**
     * @param ItemRepository $itemRepository
     * @param FrontendDesignerData $frontendDesignerData
     */
    public function __construct(ItemRepository $itemRepository, FrontendDesignerData $frontendDesignerData)
    {
        $this->itemRepository = $itemRepository;
        $this->frontendDesignerData = $frontendDesignerData;
    }

    public function getData(ConfigurationStructure $configuration): DesignerData
    {
        $itemIdentifier = $configuration->item->identifier;

        $item = $this->itemRepository->findOneByIdentifier($itemIdentifier);

        if (null === $item) {
            throw new NotFoundException();
        }

        if (null !== $item->getParent()) {
            $item = $item->getParent();
        }

        return $this->frontendDesignerData->provideForItem($item);
    }
}
