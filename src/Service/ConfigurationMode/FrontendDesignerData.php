<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewDefaultFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewImage;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\FrontendDesignViewStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\SettingApi;
use Redhotmagma\ConfiguratorApiBundle\Service\UploadMaxSize;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignView as DesignViewStructure;

/**
 * @internal
 */
final class FrontendDesignerData
{
    /**
     * @var DesignViewRepository
     */
    private $designViewRepository;

    /**
     * @var DesignViewDefaultFactory
     */
    private $designViewDefaultFactory;

    /**
     * @var FrontendDesignViewStructureFromEntityConverter
     */
    private $frontendDesignViewStructureFromEntityConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var SettingApi
     */
    private $settingApi;

    /**
     * @var UploadMaxSize
     */
    private $uploadMaxSize;

    /**
     * @var DesignViewImage
     */
    private $designViewImage;

    /**
     * @var DesignViewThumbnail
     */
    private $designViewThumbnail;

    /**
     * @param DesignViewRepository $designViewRepository
     * @param DesignViewDefaultFactory $designViewDefaultFactory
     * @param FrontendDesignViewStructureFromEntityConverter $frontendDesignViewStructureFromEntityConverter
     * @param ItemRepository $itemRepository
     * @param SettingApi $settingApi
     * @param UploadMaxSize $uploadMaxSize
     * @param DesignViewImage $designViewImage
     * @param DesignViewThumbnail $designViewThumbnail
     */
    public function __construct(
        DesignViewRepository $designViewRepository,
        DesignViewDefaultFactory $designViewDefaultFactory,
        FrontendDesignViewStructureFromEntityConverter $frontendDesignViewStructureFromEntityConverter,
        ItemRepository $itemRepository,
        SettingApi $settingApi,
        UploadMaxSize $uploadMaxSize,
        DesignViewImage $designViewImage,
        DesignViewThumbnail $designViewThumbnail
    ) {
        $this->designViewRepository = $designViewRepository;
        $this->designViewDefaultFactory = $designViewDefaultFactory;
        $this->frontendDesignViewStructureFromEntityConverter = $frontendDesignViewStructureFromEntityConverter;
        $this->itemRepository = $itemRepository;
        $this->settingApi = $settingApi;
        $this->uploadMaxSize = $uploadMaxSize;
        $this->designViewImage = $designViewImage;
        $this->designViewThumbnail = $designViewThumbnail;
    }

    /**
     * @param Item $item
     *
     * @return DesignerData
     */
    public function provideForItem(Item $item): DesignerData
    {
        $designViews = $this->getViewsByItem($item);

        $designViewsStructures = $this->frontendDesignViewStructureFromEntityConverter->convertMany($designViews);

        $this->addImagesToStructure($designViewsStructures, $item->getIdentifier(), $this->getChildIdentifiers($item->getIdentifier()));

        return new DesignerData($designViewsStructures, $this->uploadMaxSize->getFileUploadMaxSize(), $this->settingApi->frontendGetMaxZoom2d());
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    private function getViewsByItem(Item $item): array
    {
        $designViews = [];

        try {
            $designViews = $this->designViewRepository->findByItemAndHasDesignAreaRelation($item);
        } catch (\Exception $e) {
            try {
                $designViews[] = $this->designViewDefaultFactory->create($item);
            } catch (Exception $e) {
                throw new NotFoundException();
            }
        }

        return $designViews;
    }

    /**
     * @param array $designViewsStructures
     * @param string $itemIdentifier
     * @param Item[] $childIdentifiers
     */
    private function addImagesToStructure(array $designViewsStructures, string $itemIdentifier, array $childIdentifiers): void
    {
        foreach ($designViewsStructures as $designView) {
            if ($designView instanceof DesignViewStructure && '' !== $designView->identifier) {
                $designView->images = $this->designViewImage->getImages($designView->identifier, $itemIdentifier, $childIdentifiers);
                $designView->thumbnails = $this->designViewThumbnail->getImages($designView->identifier, $itemIdentifier, $childIdentifiers);
            }
        }
    }

    /**
     * @param string $parentIdentifier
     *
     * @return array
     */
    private function getChildIdentifiers(string $parentIdentifier): array
    {
        $childIdentifiers = [];

        $children = $this->itemRepository->getItemsByParent($parentIdentifier);

        foreach ($children as $child) {
            $childIdentifiers[] = $child->getIdentifier();
        }

        return $childIdentifiers;
    }
}
