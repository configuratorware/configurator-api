<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationMode;

use Exception;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDefaultFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\FrontendCreatorViewStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\SettingApi;
use Redhotmagma\ConfiguratorApiBundle\Service\UploadMaxSize;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignerData;

/**
 * @internal
 */
final class FrontendCreatorDesignerData
{
    /**
     * @var FrontendCreatorViewStructureFromEntityConverter
     */
    private $frontendCreatorViewStructureFromEntityConverter;

    /**
     * @var SettingApi
     */
    private $settingApi;

    /**
     * @var UploadMaxSize
     */
    private $uploadMaxSize;
    /**
     * @var CreatorViewDefaultFactory
     */
    private $creatorViewDefaultFactory;

    /**
     * @param FrontendCreatorViewStructureFromEntityConverter $frontendCreatorViewStructureFromEntityConverter
     * @param SettingApi $settingApi
     * @param UploadMaxSize $uploadMaxSize
     * @param CreatorViewDefaultFactory $creatorViewDefaultFactory
     */
    public function __construct(
        FrontendCreatorViewStructureFromEntityConverter $frontendCreatorViewStructureFromEntityConverter,
        SettingApi $settingApi,
        UploadMaxSize $uploadMaxSize,
        CreatorViewDefaultFactory $creatorViewDefaultFactory
    ) {
        $this->frontendCreatorViewStructureFromEntityConverter = $frontendCreatorViewStructureFromEntityConverter;
        $this->settingApi = $settingApi;
        $this->uploadMaxSize = $uploadMaxSize;
        $this->creatorViewDefaultFactory = $creatorViewDefaultFactory;
    }

    /**
     * @param Item $item
     *
     * @return DesignerData
     *
     * @throws NotFoundException
     */
    public function provideForItem(Item $item): DesignerData
    {
        $designViewsStructures = $this->frontendCreatorViewStructureFromEntityConverter->convertMany($this->getViewsByItem($item));

        return new DesignerData($designViewsStructures, $this->uploadMaxSize->getFileUploadMaxSize(), $this->settingApi->frontendGetMaxZoom2d());
    }

    /**
     * @param Item $item
     *
     * @return array
     *
     * @throws NotFoundException
     */
    private function getViewsByItem(Item $item): array
    {
        $creatorViews = $item->getCreatorView()->toArray();
        if (empty($creatorViews)) {
            try {
                $creatorViews[] = $this->creatorViewDefaultFactory->create($item);
            } catch (Exception $e) {
                throw new NotFoundException();
            }
        }

        return $creatorViews;
    }
}
