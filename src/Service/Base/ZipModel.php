<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

/**
 * @internal
 */
class ZipModel
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $name;

    /**
     * @param string $path
     * @param string $name
     *
     * @deprecated will be private in future versions, use named constructor instead
     */
    public function __construct(
        string $path,
        string $name
    ) {
        $this->path = $path;
        $this->name = $name;
    }

    /**
     * @param string $path
     * @param string $name
     *
     * @return static
     */
    public static function new(string $path, string $name): self
    {
        return new self($path, $name);
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
