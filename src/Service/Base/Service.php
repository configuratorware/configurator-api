<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

use Redhotmagma\ApiBundle\Service\BaseService;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityHelperInterface;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromDataConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ApiBundle\Service\Converter\StructureHelper;
use Redhotmagma\ApiBundle\Service\ServiceInterface;
use Redhotmagma\ApiBundle\Service\Validator\StructureValidator;

/**
 * Class Service.
 *
 * parent class for services
 *
 * @internal
 */
class Service extends BaseService implements ServiceInterface
{
    public function __construct(
        StructureFromEntityConverterInterface $structureFromEntityConverter,
        StructureHelper $structureHelper,
        EntityHelperInterface $entityHelper,
        EntityFromStructureConverter $entityFromStructureConverter,
        StructureFromDataConverter $structureFromDataConverter,
        StructureValidator $structureValidator
    ) {
        parent::__construct(
            $structureFromEntityConverter,
            $structureHelper,
            $entityHelper,
            $entityFromStructureConverter,
            $structureFromDataConverter,
            $structureValidator
        );
    }

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @return LanguageRepository
     */
    public function getLanguageRepository()
    {
        return $this->languageRepository;
    }

    /**
     * @param LanguageRepository $languageRepository
     */
    public function setLanguageRepository($languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }
}
