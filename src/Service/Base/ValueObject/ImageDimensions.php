<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject;

use Redhotmagma\ConfiguratorApiBundle\Vector\Svg;

class ImageDimensions
{
    /**
     * @return int[]
     */
    public static function fromPath(string $path, int $defaultWidth = 512, int $defaultHeight = 512): array
    {
        $imageSize = getimagesize($path);
        if (false !== $imageSize) {
            [$width, $height] = $imageSize;
        } elseif (str_ends_with($path, Svg::EXTENSION)) {
            $values = self::getDimensionsFromSVGViewBox($path);
            if (empty($values)) {
                $values = [$defaultWidth, $defaultHeight];
            }
            [$width, $height] = $values;
        } else {
            $width = $defaultWidth;
            $height = $defaultHeight;
        }

        return [$width, $height];
    }

    private static function getDimensionsFromSVGViewBox(string $path)
    {
        try {
            $xml = simplexml_load_string(file_get_contents($path));
            if ($viewBoxValue = $xml->attributes()['viewBox'] ?? null) {
                $stringValue = (string)$viewBoxValue[0];
                $values = explode(' ', $stringValue);
                if (4 === count($values)) {
                    return [$values[2], $values[3]];
                }
            }
        } catch (\Throwable $t) {
            // not handling available
        }

        return [];
    }
}
