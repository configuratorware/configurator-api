<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject;

class ImageFinder
{
    public const EXTENSION_JPG = '.jpg';
    public const EXTENSION_JPEG = '.jpeg';
    public const EXTENSION_PNG = '.png';
    private string $folderPath;

    public function __construct(string $folderPath)
    {
        $this->folderPath = $folderPath;
    }

    public static function for(string $folderPath): self
    {
        return new self($folderPath);
    }

    public function findImagePathBy(string $name): ?\SplFileInfo
    {
        $jpg = $this->folderPath . DIRECTORY_SEPARATOR . $name . self::EXTENSION_JPG;
        if (file_exists($jpg)) {
            return new \SplFileInfo($jpg);
        }
        $jpeg = $this->folderPath . DIRECTORY_SEPARATOR . $name . self::EXTENSION_JPEG;
        if (file_exists($jpeg)) {
            return new \SplFileInfo($jpeg);
        }
        $png = $this->folderPath . DIRECTORY_SEPARATOR . $name . self::EXTENSION_PNG;
        if (file_exists($png)) {
            return new \SplFileInfo($png);
        }

        return null;
    }
}
