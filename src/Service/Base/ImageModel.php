<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

/**
 * @internal
 */
class ImageModel
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $format;

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var float
     */
    private $size;

    /**
     * @var int
     */
    private $width;

    /**
     * @var int
     */
    private $height;

    /**
     * @var \StdClass
     */
    private $additionalData;

    /**
     * ImageModel constructor.
     *
     * @param string $path
     * @param string $format
     * @param string $mimeType
     * @param float $size
     * @param int $width
     * @param int $height
     */
    public function __construct(
        string $path,
        string $format,
        string $mimeType,
        float $size,
        int $width,
        int $height
    ) {
        $this->path = $path;
        $this->format = $format;
        $this->mimeType = $mimeType;
        $this->size = $size;
        $this->width = $width;
        $this->height = $height;
        $this->additionalData = new \stdClass();
    }

    /**
     * @return \StdClass
     */
    public function getData(): \stdClass
    {
        $image = new \stdClass();
        $image->path = $this->path;
        $image->format = $this->format;
        $image->mimeType = $this->mimeType;
        $image->size = $this->size;
        $image->width = $this->width;
        $image->height = $this->height;

        return $image;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * @return float
     */
    public function getSize(): float
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    /**
     * @param array<string, string> $items
     */
    public function addAdditionalData(array $items): void
    {
        foreach ($items as $key => $value) {
            $this->additionalData->$key = $value;
        }
    }

    /**
     * @return \StdClass
     */
    public function getAdditionalData(): \stdClass
    {
        return $this->additionalData;
    }
}
