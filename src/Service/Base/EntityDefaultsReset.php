<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @internal
 */
class EntityDefaultsReset
{
    /**
     * @var int
     */
    private $currentUserId;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param mixed $entity
     *
     * @throws \Exception
     */
    public function reset($entity): void
    {
        $entity->setDateCreated(new \DateTimeImmutable('now'));
        $entity->setDateUpdated(null);
        $entity->setUserCreatedId($this->getCurrentUserId());
        $entity->setUserUpdatedId(null);
    }

    /**
     * @return int|null
     */
    private function getCurrentUserId(): ?int
    {
        if (!$this->currentUserId) {
            if ($this->tokenStorage->getToken() instanceof TokenInterface) {
                $objUser = $this->tokenStorage->getToken()->getUser();
            }

            if (!empty($objUser) && is_object($objUser)) {
                $this->currentUserId = (int)$objUser->getId();
            }
        }

        return $this->currentUserId;
    }
}
