<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

/**
 * @internal
 */
class ColorChecker
{
    /**
     * Check if color is a hex color
     * * error during parsing or not matching format -> FALSE
     * * otherwise -> TRUE.
     *
     * @param string $colorString
     *
     * @return bool
     */
    public function isHexColor(string $colorString): bool
    {
        return (bool) preg_match('/^#?[[:xdigit:]]{3,6}$/i', $colorString);
    }

    /**
     * Validate and format hex color, hashtag is optional.
     *
     * @param string $colorString
     * @param bool   $hashmarkPrefix
     *
     * @return string|null
     */
    public function formatHexColor(string $colorString, bool $hashmarkPrefix = true): ?string
    {
        if ($this->isHexColor($colorString)) {
            $hexColor = strtolower(ltrim($colorString, '#'));
            if (3 === strlen($hexColor)) {
                $hexColor = $hexColor[0] . $hexColor[0] . $hexColor[1] . $hexColor[1] . $hexColor[2] . $hexColor[2];
            }

            return ($hashmarkPrefix ? '#' : '') . $hexColor;
        }

        return null;
    }
}
