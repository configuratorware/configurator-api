<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

/**
 * @internal
 */
class ZipFactory
{
    /**
     * @param string $path
     * @param string $name
     *
     * @return ZipModel
     *
     * @deprecated use ZipModel::new() instead
     */
    public static function create(string $path, string $name): ZipModel
    {
        return ZipModel::new($path, $name);
    }
}
