<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

/**
 * @internal
 */
class ImageFactory
{
    /**
     * @param string $path
     * @param string $format
     * @param string $mimeType
     * @param float $size
     * @param int $width
     * @param int $height
     *
     * @return ImageModel
     */
    public static function create(
        string $path,
        string $format,
        string $mimeType,
        float $size,
        int $width,
        int $height
    ) {
        $imageModel = new ImageModel(
            $path,
            $format,
            $mimeType,
            $size,
            $width,
            $height
        );

        return $imageModel;
    }
}
