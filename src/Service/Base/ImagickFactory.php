<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

use Redhotmagma\ConfiguratorApiBundle\Exception\ImageConversionException;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject\ImageDimensions;
use Redhotmagma\ConfiguratorApiBundle\Vector\Vector;
use Redhotmagma\ConfiguratorApiBundle\Vector\VectorPngConverterInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * @internal
 */
class ImagickFactory
{
    private const SCALABLE_FORMATS = ['ai', 'pdf', 'svg', 'eps', 'ps'];

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var VectorPngConverterInterface
     */
    private $vectorConversion;

    public function __construct(Filesystem $filesystem, VectorPngConverterInterface $vectorConversion)
    {
        $this->fileSystem = $filesystem;
        $this->vectorConversion = $vectorConversion;
    }

    /**
     * Create an Imagick image object using error handling.
     *
     * @param string $sourceImagePath
     *
     * @return \Imagick
     *
     * @throws \ImagickException
     */
    public function createFromSource(string $sourceImagePath): \Imagick
    {
        $image = $this->readImageFile($sourceImagePath);

        $colorspace = $image->getImageColorspace();

        // handle non RGB colorspaces (usually CMYK)
        if (\Imagick::COLORSPACE_SRGB !== $colorspace && \Imagick::COLORSPACE_RGB !== $colorspace) {
            $tempImgPath = $this->convertToRGBColorspace($sourceImagePath, $image);
            $image = $this->readImageFile($tempImgPath);
            $this->fileSystem->remove($tempImgPath);
        }

        // force 8bit colors channels - otherwise image color manipulations won't work
        if ($image->getImageDepth() > 8) {
            $image->setImageDepth(8);
        }

        return $image;
    }

    /**
     * Create a new empty image object.
     *
     * @param int    $width
     * @param int    $height
     * @param string $backgroundColor
     *
     * @return \Imagick
     *
     * @throws \ImagickException
     */
    public function createFromDimensions(
        int $width,
        int $height,
        string $backgroundColor = 'transparent'
    ): \Imagick {
        $image = new \Imagick();
        $image->newImage($width, $height, new \ImagickPixel($backgroundColor));

        return $image;
    }

    /**
     * Convert an image to RGB colorspace using console **convert** command of Imagick.
     *
     * @param string $sourceImagePath
     * @param \Imagick $image
     *
     * @return string New image path of converted image
     */
    private function convertToRGBColorspace(string $sourceImagePath, \Imagick $image): string
    {
        $tempImgPath = preg_replace('/\.[^.]+$/i', '', $sourceImagePath) . '.png';

        if ($image->getNumberImages() > 1) {
            // try composite image conversion for multi layered / multi page images
            $command = 'convert -colorspace sRGB -composite "' . $sourceImagePath . '" PNG32:"' . $tempImgPath . '"';
        } else {
            // try simple image conversion
            $command = 'convert -colorspace sRGB "' . $sourceImagePath . '" PNG32:"' . $tempImgPath . '"';
        }
        $process = Process::fromShellCommandline($command);
        $process->mustRun();

        if (!$this->fileSystem->exists($tempImgPath)) {
            // if no result, throw exception
            throw ImageConversionException::cannotConvertToRGBColorspace();
        }

        return $tempImgPath;
    }

    /**
     * @param string $sourceImagePath
     *
     * @return \Imagick
     *
     * @throws \ImagickException
     */
    private function readImageFile(string $sourceImagePath): \Imagick
    {
        $sourceImagePath = $this->preConvertVectorFormatsWithPossibleColorProblems($sourceImagePath);

        $image = new \Imagick();

        try {
            [$width, $height] = ImageDimensions::fromPath($sourceImagePath);
            $image->setSize($width, $height);

            // fix for SVG with transparent bg getting a white bg in PNG format
            $image->setBackgroundColor(new \ImagickPixel('transparent'));
            // this is failing with PDF test image ("white_logo_transparent_bg_CMYK.pdf") with new imagick version (6.9.10-23)
            $image->readImageBlob(file_get_contents($sourceImagePath));
        } catch (\Exception $e) {
            $image = new \Imagick($sourceImagePath);
        }

        return $image;
    }

    /**
     * This function was added to overcome a problem in newer Imagick libraries, where an .eps vector file conversion was failing.
     *
     * @param string $sourceImagePath
     *
     * @return string Source image path of the converted file or original path if no conversion was needed
     */
    private function preConvertVectorFormatsWithPossibleColorProblems(string $sourceImagePath): string
    {
        $sourceExtension = (new \SplFileInfo($sourceImagePath))->getExtension();
        if ('' === $sourceExtension) {
            throw new \InvalidArgumentException('File extension is missing');
        }

        if (in_array(strtolower($sourceExtension), self::SCALABLE_FORMATS)) {
            $png = $this->vectorConversion->vectorToPng(Vector::withPath($sourceImagePath));

            return $png->getPath();
        }

        return $sourceImagePath;
    }
}
