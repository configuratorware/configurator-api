<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

/**
 * @internal
 */
class FormatterService
{
    /**
     * Formats a price based on the current channel and language.
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param float $price
     *
     * @return string
     */
    public function formatPrice($price)
    {
        $channelSettings = C_CHANNEL_SETTINGS;
        $languageSettings = C_LANGUAGE_SETTINGS;
        $priceFormatted = $price;

        $priceDecimals = 2;
        if (isset($languageSettings['pricedecimals'])) {
            $priceDecimals = $languageSettings['pricedecimals'];
        }

        $priceFormatted = $this->formatNumber($priceFormatted, $priceDecimals);

        if ('left' === $languageSettings['currencysymbolposition']) {
            $priceFormatted = $channelSettings['currencySymbol'] . ' ' . $priceFormatted;
        } else {
            $priceFormatted = $priceFormatted . ' ' . $channelSettings['currencySymbol'];
        }

        return $priceFormatted;
    }

    /**
     * Formats a number based on the current language.
     *
     * @author  Christian Schilling <schilling@redhotmagma.de>
     *
     * @since   1.0
     *
     * @version 1.0
     *
     * @param float $number
     * @param int|null $decimals
     *
     * @return string
     */
    public function formatNumber($number, $decimals = null)
    {
        $languageSettings = C_LANGUAGE_SETTINGS;

        if (null === $decimals) {
            $decimals = $languageSettings['numberdecimals'];
        }

        return number_format($number, $decimals,
            $languageSettings['decimalpoint'], $languageSettings['thousandsseparator']);
    }

    /**
     * replace the price in a formatted price.
     *
     * @author  Michael Aichee <aichele@redhotmagma.de>
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @param   string $price
     * @param   string $targetPrice (with decimal point!)
     *
     * @return  string  mixed
     */
    public function replaceFormattedPrice($price, $targetPrice)
    {
        preg_match('/[0-9]*([\.,])[0-9]*/', $price, $matches);

        $price = str_replace($matches[0], str_replace('.', $matches[1], $targetPrice), $price);

        return $price;
    }
}
