<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

use ZipArchive;

/**
 * @internal
 */
class ZipCreator
{
    /**
     * @param ZipModel[] $files
     *
     * @return string
     */
    public function createArchive(array $files): string
    {
        $zipPath = sprintf('%s/%s.zip', sys_get_temp_dir(), uniqid('zip_'));

        $zip = new ZipArchive();
        $zip->open($zipPath, ZipArchive::CREATE);

        foreach ($files as $file) {
            $fileName = $file->getName();

            if (!$fileName) {
                $fileName = pathinfo($file->getPath(), PATHINFO_BASENAME);
            }
            if (file_exists($file->getPath())) {
                $zip->addFile($file->getPath(), $fileName);
            }
        }

        $zip->close();

        return $zipPath;
    }
}
