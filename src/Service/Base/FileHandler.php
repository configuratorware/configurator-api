<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class FileHandler
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var ZipCreator
     */
    private $zipCreator;

    /**
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem, ZipCreator $zipCreator)
    {
        $this->filesystem = $filesystem;
        $this->zipCreator = $zipCreator;
    }

    /**
     * @param string $url
     *
     * @return false|string|null
     */
    public function downloadFile(string $url)
    {
        $url = trim($url);

        $headers = @get_headers($url);
        if (!$headers || false === strpos($headers[0], '200')) {
            return null;
        }

        $fileContent = @file_get_contents($url);
        if (false === $fileContent) {
            return null;
        }

        return $fileContent;
    }

    /**
     * @param string $path
     * @param string $fileName
     * @param bool $overwrite
     * @param string|null $unwrapPath
     */
    public function uncompress(string $path, string $fileName, bool $overwrite = false, string $unwrapPath = ''): void
    {
        $path = rtrim($path, DIRECTORY_SEPARATOR);

        $tmpFiles = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid('uncompress_tar_');

        $pharExtract = new \PharData($path . DIRECTORY_SEPARATOR . $fileName);
        $pharExtract->extractTo($tmpFiles, null, true);

        // copy file by file to not overwrite files not contained in the archive (e.g. images under /images)
        $finder = new Finder();
        $finder->files()->in($tmpFiles . '/' . trim($unwrapPath, '/'));
        foreach ($finder as $file) {
            $this->filesystem->copy($file->getPathname(), $path . '/' . $file->getRelativePath() . '/' . $file->getBasename(), $overwrite);
        }

        $this->filesystem->remove($tmpFiles);
    }

    /**
     * @param string $basePath
     * @param array $relativeFilePaths
     * @param string $targetFileName
     */
    public function backupFiles(string $basePath, array $relativeFilePaths, string $targetFileName): void
    {
        $zipModels = [];

        foreach ($relativeFilePaths as $filePath) {
            $zipModels[] = ZipModel::new($basePath . DIRECTORY_SEPARATOR . $filePath, $filePath);
        }

        $zipPath = $this->zipCreator->createArchive($zipModels);

        if ($this->filesystem->exists($zipPath)) {
            $this->filesystem->copy($zipPath, $targetFileName);
        }
    }
}
