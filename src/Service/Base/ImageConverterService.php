<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

use Redhotmagma\ConfiguratorApiBundle\Exception\ImageConversionException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class ImageConverterService
{
    private ImagickFactory $imagickFactory;

    private Filesystem $filesystem;

    public function __construct(
        ImagickFactory $imagickFactory,
        Filesystem $filesystem
    ) {
        $this->imagickFactory = $imagickFactory;
        $this->filesystem = $filesystem;
    }

    /**
     * contain an image into a specific size.
     *
     * @author  Michael Aichele <aichlele@redhotmagma.de>
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @param $sourceImagePath
     * @param $targetImagePath
     * @param $targetWidth
     * @param $targetHeight
     * @param string $targetFormat
     * @param string $backgroundColor
     *
     * @return ImageModel
     */
    public function contain(
        $sourceImagePath,
        $targetImagePath,
        $targetWidth,
        $targetHeight,
        $targetFormat = 'png',
        $backgroundColor = 'transparent'
    ): ImageModel {
        try {
            $sourceImage = $this->imagickFactory->createFromSource($sourceImagePath);
            $targetImage = $this->imagickFactory->createFromDimensions($targetWidth, $targetHeight, $backgroundColor);

            // resize source image
            $sourceImage->scaleImage($targetWidth, $targetHeight, true);
            $sourceSize = $sourceImage->getImageGeometry();
            $sourceWidth = $sourceSize['width'];
            $sourceHeight = $sourceSize['height'];

            // place source in target
            $targetX = floor(($targetWidth - $sourceWidth) / 2);
            $targetY = floor(($targetHeight - $sourceHeight) / 2);
            $targetImage->compositeImage($sourceImage, \Imagick::COMPOSITE_DEFAULT, $targetX, $targetY);

            // save target
            $this->filesystem->mkdir(dirname($targetImagePath), 0755);
            $imageModel = $this->saveImage($targetImage, $targetImagePath, $targetFormat);
        } catch (\Throwable $exception) {
            throw ImageConversionException::conversionFailed($exception);
        }

        return $imageModel;
    }

    /**
     * Scales Images by the given $width OR $height if at least one of them is exceeded.
     *
     * @param string $sourceImagePath
     * @param string $targetImagePath
     * @param int    $maxWidth
     * @param int    $maxHeight
     * @param string $targetFormat
     * @param string $sourceExtension
     *
     * @return ImageModel
     */
    public function scaleDown(
        string $sourceImagePath,
        string $targetImagePath,
        int $maxWidth,
        int $maxHeight,
        string $targetFormat = 'png'
    ): ImageModel {
        try {
            $image = $this->imagickFactory->createFromSource($sourceImagePath);

            $imageGeo = $image->getImageGeometry();
            $targetWidth = $imageGeo['width'];
            $targetHeight = $imageGeo['height'];

            if ($imageGeo['width'] >= $maxWidth || $imageGeo['height'] >= $maxHeight) {
                if ($maxWidth / $imageGeo['width'] < $maxHeight / $imageGeo['height']) {
                    // Case 1: width exceeds more than height
                    $targetWidth = $maxWidth;
                    $targetHeight = ($imageGeo['height'] * $maxWidth) / $imageGeo['width'];
                } else {
                    // Case 2: height exceeds more than width
                    $targetHeight = $maxHeight;
                    $targetWidth = ($imageGeo['width'] * $maxHeight) / $imageGeo['height'];
                }
            }

            // resize source image
            $image->scaleImage($targetWidth, $targetHeight, true);

            // save target
            $imageModel = $this->saveImage($image, $targetImagePath, $targetFormat);
        } catch (\Throwable $exception) {
            throw ImageConversionException::conversionFailed($exception);
        }

        return $imageModel;
    }

    /**
     * convert an image to target format.
     *
     * @author  Michael Aichele <aichlele@redhotmagma.de>
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @param $sourceImagePath
     * @param $targetImagePath
     * @param string $targetFormat
     *
     * @return ImageModel
     */
    public function convert($sourceImagePath, $targetImagePath, $targetFormat = 'png'): ImageModel
    {
        try {
            $sourceImage = $this->imagickFactory->createFromSource($sourceImagePath);

            $imageModel = $this->saveImage($sourceImage, $targetImagePath, $targetFormat);
        } catch (\Throwable $exception) {
            throw ImageConversionException::conversionFailed($exception);
        }

        return $imageModel;
    }

    /**
     * saves an image.
     *
     * @author  Michael Aichele <aichlele@redhotmagma.de>
     *
     * @since   1.0.20
     *
     * @version 1.0.20
     *
     * @param   \Imagick $targetImage
     * @param   string $targetImagePath
     * @param   string $targetFormat
     * @param   int $quality
     *
     * @return ImageModel
     *
     * @throws \ImagickException
     */
    protected function saveImage($targetImage, $targetImagePath, $targetFormat = 'png', $quality = 90): ImageModel
    {
        $targetImage->stripImage();
        $targetImage->mergeImageLayers(\Imagick::LAYERMETHOD_FLATTEN);

        $targetImage->setImageFormat($targetFormat);
        $targetImage->setCompressionQuality($quality);

        $targetImage->writeimage($targetImagePath);

        $imageModel = ImageFactory::create(
            $targetImage->getImageFilename(),
            $targetImage->getImageFormat(),
            $targetImage->getImageMimeType(),
            strlen($targetImage->getImageBlob()),
            $targetImage->getImageGeometry()['width'],
            $targetImage->getImageGeometry()['height']
        );

        return $imageModel;
    }
}
