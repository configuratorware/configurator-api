<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Base;

use Redhotmagma\ConfiguratorApiBundle\Exception\ImageConversionException;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject\ImageDimensions;

/**
 * @internal
 */
class ImageDataProvider
{
    public const ORIGINAL = 'original';

    public const PREVIEW = 'preview';

    public const THUMBNAIL = 'thumbnail';

    /**
     * @param $path
     *
     * @return ImageModel
     */
    public function provide($path): ImageModel
    {
        try {
            [$width, $height] = ImageDimensions::fromPath($path);

            $imagick = new \Imagick();
            $imagick->setSize($width, $height);
            $imagick->readImageBlob(file_get_contents($path));
        } catch (\Throwable $exception) {
            throw ImageConversionException::cannotCreateImageFromPath($exception);
        }

        return ImageFactory::create(
            $path,
            $imagick->getImageFormat(),
            $imagick->getImageMimeType(),
            $imagick->getImageLength(),
            $imagick->getImageGeometry()['width'],
            $imagick->getImageGeometry()['height']
        );
    }
}
