<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service;

/**
 * @internal
 */
class StringUtilizer
{
    /**
     * @param string $identifier
     *
     * @return string
     */
    public function normalizeIdentifier(string $identifier): string
    {
        $normalizedIdentifier = strtolower($identifier);
        $normalizedIdentifier = preg_replace('/[^a-z0-9_-]/', '', $normalizedIdentifier);

        return $normalizedIdentifier;
    }
}
