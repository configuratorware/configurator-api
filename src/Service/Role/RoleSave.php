<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Role;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;

/**
 * @internal
 */
class RoleSave
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var RoleEntityFromStructureConverter
     */
    private $roleEntityFromStructureConverter;

    /**
     * @var RoleStructureFromEntityConverter
     */
    private $roleStructureFromEntityConverter;

    /**
     * @param RoleRepository $optionPoolRepository
     * @param RoleEntityFromStructureConverter $optionPoolEntityFromStructureConverter
     * @param RoleStructureFromEntityConverter $optionPoolStructureFromEntityConverter
     */
    public function __construct(
        RoleRepository $optionPoolRepository,
        RoleEntityFromStructureConverter $optionPoolEntityFromStructureConverter,
        RoleStructureFromEntityConverter $optionPoolStructureFromEntityConverter
    ) {
        $this->roleRepository = $optionPoolRepository;
        $this->roleEntityFromStructureConverter = $optionPoolEntityFromStructureConverter;
        $this->roleStructureFromEntityConverter = $optionPoolStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Role $role
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Role
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Role $role)
    {
        $entity = null;
        if (isset($role->id) && $role->id > 0) {
            $entity = $this->roleRepository->findOneBy(['id' => $role->id]);
        }

        $entity = $this->roleEntityFromStructureConverter->convertOne($role, $entity);
        $entity = $this->roleRepository->save($entity);

        /** @var Role $entity */
        $entity = $this->roleRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->roleStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
