<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Role;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential;
use Redhotmagma\ConfiguratorApiBundle\Structure\Credential;
use Redhotmagma\ConfiguratorApiBundle\Structure\Role;

/**
 * @internal
 */
class RoleStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Role $entity
     * @param string $structureClassName
     *
     * @return Role
     */
    public function convertOne($entity, $structureClassName = Role::class)
    {
        /** @var Role $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addCredentialsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Role::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param Role $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Role $entity
     *
     * @return mixed
     */
    protected function addCredentialsToStructure($structure, $entity)
    {
        $credentials = [];

        $roleCredentials = $entity->getRoleCredential();

        if (!empty($roleCredentials)) {
            /** @var RoleCredential $roleCredential */
            foreach ($roleCredentials as $roleCredential) {
                $credential = $roleCredential->getCredential();
                $credentialStructure = $this->structureFromEntityConverter->convertOne($credential,
                    Credential::class);

                $credentials[] = $credentialStructure;
            }
        }

        $structure->credentials = $credentials;

        return $structure;
    }
}
