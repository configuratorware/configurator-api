<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Role;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Entity\User;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Symfony\Bundle\SecurityBundle\Security;

/**
 * @internal
 */
class RoleDelete
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var Security
     */
    private $security;

    /**
     * RoleDelete constructor.
     *
     * @param RoleRepository $roleRepository
     * @param Security $security
     */
    public function __construct(RoleRepository $roleRepository, Security $security)
    {
        $this->roleRepository = $roleRepository;
        $this->security = $security;
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $ids = explode(',', $id);

        $roles = $this->validateAndFilterRolesToDelete($ids);

        foreach ($roles as $role) {
            foreach ($role->getRoleCredential()->toArray() as $roleCredential) {
                $this->roleRepository->delete($roleCredential);
            }

            $this->roleRepository->delete($role);
        }
    }

    /**
     * @param array $ids
     *
     * @return array
     */
    private function validateAndFilterRolesToDelete($ids): array
    {
        $roles = [];
        $violations = [];

        // validate and filter ids
        foreach ($ids as $id) {
            /** @var Role $entity */
            $entity = $this->roleRepository->findOneBy(['id' => $id]);
            $roles[] = $entity;

            if (!empty($entity)) {
                // do not allow to delete the admin role
                if ('ROLE_ADMIN' === $entity->getRole()) {
                    $violations[] = [
                        'property' => 'id',
                        'invalidvalue' => $entity->getName(),
                        'message' => 'not_allowed_to_delete',
                    ];
                }

                // do not allow to delete the last role of logged in user
                /** @var User $user */
                $user = $this->security->getUser();
                $userRoles = $user->getUserRole();

                if (1 === $userRoles->count()
                    && $userRoles->first()->getRole()->getId() === $id) {
                    $violations[] = [
                        'property' => 'id',
                        'invalidvalue' => $entity->getName(),
                        'message' => 'not_allowed_to_delete',
                    ];
                }
            }
        }

        if (!empty($violations)) {
            throw new ValidationException($violations);
        }

        return $roles;
    }
}
