<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Role;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\RoleCredential;
use Redhotmagma\ConfiguratorApiBundle\Repository\CredentialRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Role;

/**
 * @internal
 */
class RoleEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var CredentialRepository
     */
    private $credentialRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * RoleEntityFromStructureConverter constructor.
     *
     * @param CredentialRepository $credentialRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        CredentialRepository $credentialRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->credentialRepository = $credentialRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param Role $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Role $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Role
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Role::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Role $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->entityFromStructureConverter->setManyToManyRelationsDeleted($localStructure, $entity,
            'RoleCredential', 'credentials', 'Credential');

        $entity = $this->entityFromStructureConverter->addNewManyToManyRelations(
            $localStructure,
            $entity,
            RoleCredential::class,
            'credentials',
            'Credential',
            $this->credentialRepository
        );

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Role::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
