<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Role;

use Redhotmagma\ConfiguratorApiBundle\Entity\Role;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\RoleRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class RoleApi
{
    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var RoleStructureFromEntityConverter
     */
    private $roleStructureFromEntityConverter;

    /**
     * @var RoleSave
     */
    private $roleSave;

    /**
     * @var RoleDelete
     */
    private $roleDelete;

    /**
     * RoleApi constructor.
     *
     * @param RoleRepository $roleRepository
     * @param RoleStructureFromEntityConverter $roleStructureFromEntityConverter
     * @param RoleSave $roleSave
     * @param RoleDelete $roleDelete
     */
    public function __construct(
        RoleRepository $roleRepository,
        RoleStructureFromEntityConverter $roleStructureFromEntityConverter,
        RoleSave $roleSave,
        RoleDelete $roleDelete
    ) {
        $this->roleRepository = $roleRepository;
        $this->roleStructureFromEntityConverter = $roleStructureFromEntityConverter;
        $this->roleSave = $roleSave;
        $this->roleDelete = $roleDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->roleRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->roleStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->roleRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Role
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Role
    {
        /** @var Role $entity */
        $entity = $this->roleRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->roleStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Role $role
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Role
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Role $role)
    {
        $structure = $this->roleSave->save($role);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->roleDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['name'];
    }
}
