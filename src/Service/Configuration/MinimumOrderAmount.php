<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;

/**
 * @internal
 */
class MinimumOrderAmount
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * MinimumOrderAmount constructor.
     *
     * @param ItemRepository $itemRepository
     */
    public function __construct(ItemRepository $itemRepository)
    {
        $this->itemRepository = $itemRepository;
    }

    /**
     * @param Configuration $configuration
     * @param string $selectedItemIdentifier
     *
     * @return Configuration
     */
    public function addSelectedAmountsToStructure(
        Configuration $configuration,
        string $selectedItemIdentifier
    ): Configuration {
        $item = $this->itemRepository->findOneByIdentifier($selectedItemIdentifier);

        // by default select the minimum order amount of the item given in the configuration
        $minimumOrderAmount = $item->getMinimumOrderAmount() ?? 1;

        // if item has a parent use the minimum order amount from the parent
        if (null !== $item->getParent() && $item->getParent()->getMinimumOrderAmount() > $minimumOrderAmount) {
            $minimumOrderAmount = $item->getParent()->getMinimumOrderAmount();
        }

        // for parent items use the first child
        if (null === $item->getParent()
            && !empty($configuration->item->itemGroup)
            && !empty($configuration->item->itemGroup->children)) {
            $selectedItemIdentifier = $configuration->item->itemGroup->children[0]->identifier;
            $child = $this->itemRepository->findOneByIdentifier($selectedItemIdentifier);
            if ((!$item->getAccumulateAmounts() || null === $item->getMinimumOrderAmount()) && $child->getMinimumOrderAmount() > $minimumOrderAmount) {
                $minimumOrderAmount = $child->getMinimumOrderAmount();
            }
        }

        if (!isset($configuration->customdata)) {
            $configuration->customdata = new \stdClass();
        }

        $selectedAmounts = new \stdClass();
        $selectedAmounts->$selectedItemIdentifier = $minimumOrderAmount;

        $configuration->customdata->selectedAmounts = $selectedAmounts;

        return $configuration;
    }
}
