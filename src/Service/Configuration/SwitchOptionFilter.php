<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

/**
 * @internal
 */
class SwitchOptionFilter
{
    /**
     * @param $switchOptions
     *
     * @return mixed
     */
    public function filterOptionToSwitch($switchOptions)
    {
        foreach ($switchOptions as $optionClassification => $switchOption) {
            if ($switchOption instanceof \stdClass) {
                $switchOption = [$switchOption];
            }

            foreach ($switchOption as $index => $switchOptionItem) {
                if (empty($switchOptionItem->identifier)) {
                    unset($switchOption->$index);
                }
            }
        }

        return $switchOptions;
    }
}
