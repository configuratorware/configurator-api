<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use League\Csv\Writer;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;

/**
 * @internal
 */
class CsvGenerator
{
    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @param ConfigurationRepository $configurationRepository
     */
    public function __construct(ConfigurationRepository $configurationRepository)
    {
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * Generate CSV containing the bulk names from the configuration customData.
     *
     * @param string $configurationCode
     *
     * @return FileResult|null
     *
     * @throws \League\Csv\CannotInsertRecord
     */
    public function generateBulkNameCsv(string $configurationCode): ?FileResult
    {
        $configuration = $this->configurationRepository->findOneBy(['code' => $configurationCode]);

        if (!($configuration instanceof Configuration)) {
            // no configuration found
            throw new NotFoundException();
        }

        $customData = $configuration->getCustomdata();

        /* @var \stdClass $customData */
        if (empty($customData->bulkNames)
        ) {
            // no custom data or no bulk names found
            return null;
        }

        $fileResult = $this->createCsvFileResult(
            $configuration->getItem()->getTranslatedTitle(),
            sys_get_temp_dir() . '/' . uniqid('bulk_names_csv_') . '.csv',
            $configurationCode
        );

        $csvWriter = Writer::createFromPath($fileResult->filePath, 'w');
        $csvWriter->setDelimiter(';');
        foreach ($customData->bulkNames as $itemIdentifier => $bulkNames) {
            if (is_array($bulkNames)) {
                foreach ($bulkNames as $bulkName) {
                    $csvWriter->insertOne([$itemIdentifier, $bulkName]);
                }
            } // fallback to old format where item identifier was not given in bulk name data
            elseif (is_string($bulkNames)) {
                $csvWriter->insertOne([$configuration->getItem()->getIdentifier(), $bulkNames]);
            }
        }

        return $fileResult;
    }

    /**
     * @param string $itemName
     * @param string $csvPath
     * @param string $configurationCode
     *
     * @return FileResult
     */
    private function createCsvFileResult(string $itemName, string $csvPath, string $configurationCode): FileResult
    {
        return new FileResult($csvPath, $itemName . '_' . $configurationCode . '.csv');
    }
}
