<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;

/**
 * @internal
 */
class ConfigurationImages
{
    private const FILE_PATH_PATTERN = [
        'preview' => [
            '/default.png',
            '/{configurationCode}.png',
            '/*.png',
            '/*/*.png',
        ],
        'configurations' => [
            '/{configurationCode}/*.png',
            '/{configurationCode}/*/*.png',
        ],
        'printfiles' => [
            '/{configurationCode}*.png',
            '/{configurationCode}*.svg',
        ],
    ];

    /**
     * @var MediaHelper
     */
    private $mediaHelper;

    /**
     * @param MediaHelper $mediaHelper
     */
    public function __construct(MediaHelper $mediaHelper)
    {
        $this->mediaHelper = $mediaHelper;
    }

    /**
     * @param string $configurationCode
     *
     * @return string
     */
    public function getPreviewImage(string $configurationCode): ?string
    {
        $imageDir = '/images/configurations/' . $configurationCode;
        $imagePath = $this->mediaHelper->getImageBasePath() . '/configurations/' . $configurationCode . '/';

        $patternAmount = count(self::FILE_PATH_PATTERN['preview']);

        $i = 0;
        $imageName = '';
        while ('' === $imageName && $i < $patternAmount) {
            $pattern = str_replace(
                '{configurationCode}',
                $configurationCode,
                self::FILE_PATH_PATTERN['preview'][$i]
            );

            $images = glob($imagePath . $pattern);

            if (is_array($images) && count($images) > 0) {
                $imageName = str_replace($imagePath, '', current($images));
            }

            ++$i;
        }

        if ('' !== $imageName) {
            return $this->mediaHelper->getBaseUrl() . $imageDir . $imageName;
        }

        return null;
    }

    /**
     * @param string $code
     * @param string $imageType
     *
     * @return array
     */
    public function getImagesForConfiguration($code, $imageType = 'configurations'): array
    {
        $imageUrls = [];

        $imageDir = '/images/' . $imageType;

        $imagePath = $this->mediaHelper->getImageBasePath() . '/' . $imageType . '/';

        $basePatterns = self::FILE_PATH_PATTERN[$imageType] ?? [];

        foreach ($basePatterns as $basePattern) {
            $codePattern = str_replace(
                '{configurationCode}',
                $code,
                $basePattern
            );

            foreach (glob($imagePath . $codePattern) as $image) {
                $imageUrl = str_replace($imagePath, $imageDir, $image);

                if (!in_array($imageUrl, $imageUrls, true)) {
                    $imageUrls[] = $this->mediaHelper->getImageBaseUrl() . $imageUrl;
                }
            }
        }

        return $imageUrls;
    }
}
