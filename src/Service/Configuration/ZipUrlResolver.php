<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ZipUrlResolver
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @param UrlGeneratorInterface $router
     * @param LanguageRepository $languageRepository
     */
    public function __construct(UrlGeneratorInterface $router, LanguageRepository $languageRepository)
    {
        $this->router = $router;
        $this->languageRepository = $languageRepository;
    }

    public function getProductionZipUrl(string $configurationCode, ControlParameters $controlParameters): string
    {
        $parameters = $controlParameters->getAllParameters();
        $parameters = $this->addDefaultLanguageToParameters($parameters);
        $parameters['configurationCode'] = $configurationCode;

        return $this->router->generate('redhotmagma_configurator_frontend_configurations_zip', $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
    }

    private function addDefaultLanguageToParameters(array $parameters): array
    {
        $defaultLanguageIso = $this->languageRepository->findDefault()->getIso();
        $parameters[ControlParameters::LANGUAGE] = $defaultLanguageIso;

        return $parameters;
    }
}
