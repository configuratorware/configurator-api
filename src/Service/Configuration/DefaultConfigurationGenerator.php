<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;

/**
 * @internal
 */
class DefaultConfigurationGenerator
{
    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var ConfigurationCodeGenerator
     */
    private $configurationCodeGenerator;

    /**
     * @var ConfigurationtypeRepository
     */
    private $configurationTypeRepository;

    /**
     * DefaultConfigurationGenerator constructor.
     *
     * @param ConfigurationRepository $configurationRepository
     * @param ConfigurationCodeGenerator $configurationCodeGenerator
     * @param ConfigurationtypeRepository $configurationTypeRepository
     */
    public function __construct(
        ConfigurationRepository $configurationRepository,
        ConfigurationCodeGenerator $configurationCodeGenerator,
        ConfigurationtypeRepository $configurationTypeRepository
    ) {
        $this->configurationRepository = $configurationRepository;
        $this->configurationCodeGenerator = $configurationCodeGenerator;
        $this->configurationTypeRepository = $configurationTypeRepository;
    }

    /**
     * @param Item $item
     *
     * @return Configuration
     */
    public function getDefaultConfigurationForItem(Item $item)
    {
        $baseConfiguration = $this->configurationRepository->getBaseconfigurationByItemIdentifier($item->getIdentifier());

        if (empty($baseConfiguration)) {
            $baseConfiguration = $this->createEmptyDefaultConfigurationForItem($item);
        }

        return $baseConfiguration;
    }

    /**
     * @param Item $item
     *
     * @return Configuration
     */
    protected function createEmptyDefaultConfigurationForItem(Item $item)
    {
        $configurationType = $this->configurationTypeRepository->findOneBy(['identifier' => ConfigurationtypeRepository::DEFAULT_OPTIONS]);

        $defaultConfiguration = new Configuration();
        $defaultConfiguration->setItem($item);
        $defaultConfiguration->setConfigurationtype($configurationType);
        $defaultConfiguration->setSelectedoptions([]);
        $defaultConfiguration->setDateUpdated(new \DateTime());

        $codeLength = $this->configurationCodeGenerator->getConfigurationCodeLengthByConfigurationType(ConfigurationtypeRepository::DEFAULT_OPTIONS);

        $defaultConfiguration->setCode($this->configurationCodeGenerator->generateCode($codeLength));

        /** @var Configuration $configuration */
        $configuration = $this->configurationRepository->save($defaultConfiguration);

        return $configuration;
    }
}
