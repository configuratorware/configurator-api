<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Exception;
use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ZipModel;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignAreaPrintfilePathInterface;
use Redhotmagma\ConfiguratorApiBundle\Vector\Pdf;
use Redhotmagma\ConfiguratorApiBundle\Vector\Svg;
use Redhotmagma\ConfiguratorApiBundle\Vector\SvgPdfConverterInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class DesignAreaPdfGenerator
{
    /**
     * @var DesignAreaPrintfilePathInterface
     */
    private $designAreaPrintfilePathSetting;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var SvgPdfConverterInterface
     */
    private $svgPdfConverter;

    /**
     * @var PrintDesignDataProvider
     */
    private $printDesignDataProvider;

    /**
     * @param DesignAreaPrintfilePathInterface $designAreaPrintfilePathSetting
     * @param Filesystem $filesystem
     * @param LoggerInterface $logger
     * @param SvgPdfConverterInterface $svgPdfConverter
     * @param PrintDesignDataProvider $printDesignDataProvider
     */
    public function __construct(DesignAreaPrintfilePathInterface $designAreaPrintfilePathSetting, Filesystem $filesystem, LoggerInterface $logger, SvgPdfConverterInterface $svgPdfConverter, PrintDesignDataProvider $printDesignDataProvider)
    {
        $this->designAreaPrintfilePathSetting = $designAreaPrintfilePathSetting;
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->svgPdfConverter = $svgPdfConverter;
        $this->printDesignDataProvider = $printDesignDataProvider;
    }

    /**
     * @param Configuration $configuration
     *
     * @return ZipModel[]|array
     */
    public function generateFiles(Configuration $configuration): array
    {
        $designAreaPdfFiles = [];
        foreach ($configuration->getItem()->getDesignArea() as $designArea) {
            $identifier = $designArea->getIdentifier();
            $svgPath = $this->designAreaPrintfilePathSetting->getDesignAreaPrintfilePath() . '/' . $configuration->getCode() . '_' . $identifier . '.svg';

            if (!$this->filesystem->exists($svgPath)) {
                continue;
            }

            $designData = $configuration->getDesigndata()->$identifier ?? null;

            try {
                $designAreaPdfFile = $this->createAreaPdf($svgPath, $designArea, $designData);
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());

                continue;
            }

            $designAreaPdfFiles[] = ZipModel::new($designAreaPdfFile->getPath(), $designArea->getIdentifier() . '.pdf');
        }

        return $designAreaPdfFiles;
    }

    /**
     * @param string $svgPath
     * @param DesignArea $designArea
     * @param $designData
     *
     * @return Pdf
     */
    private function createAreaPdf(string $svgPath, DesignArea $designArea, $designData): Pdf
    {
        $svgWithCorrectDimensions = $this->manipulateSvgString(
            $this->printDesignDataProvider->provide($svgPath),
            $designArea->getMethodWidth($designData->designProductionMethodIdentifier ?? ''),
            $designArea->getMethodHeight($designData->designProductionMethodIdentifier ?? '')
        );

        return $this->svgPdfConverter->svgToPdf(Svg::withContent($svgWithCorrectDimensions));
    }

    /**
     * @param string $svgString
     * @param $designAreaWidth
     * @param $designAreaHeight
     *
     * @return string
     */
    private function manipulateSvgString(string $svgString, $designAreaWidth, $designAreaHeight): string
    {
        $svgAsXML = simplexml_load_string($svgString);

        $svgAsXML->attributes()->width = $designAreaWidth . 'mm';
        $svgAsXML->attributes()->height = $designAreaHeight . 'mm';

        return $svgAsXML->asXML();
    }
}
