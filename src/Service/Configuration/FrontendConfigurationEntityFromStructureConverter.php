<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidConfiguration;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;

/**
 * @internal
 */
class FrontendConfigurationEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ConfigurationtypeRepository
     */
    private $configurationTypeRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @param ChannelRepository $channelRepository
     * @param ClientRepository $clientRepository
     * @param ConfigurationtypeRepository $configurationTypeRepository
     * @param ItemRepository $itemRepository
     * @param LanguageRepository $languageRepository
     */
    public function __construct(
        ChannelRepository $channelRepository,
        ClientRepository $clientRepository,
        ConfigurationtypeRepository $configurationTypeRepository,
        ItemRepository $itemRepository,
        LanguageRepository $languageRepository
    ) {
        $this->channelRepository = $channelRepository;
        $this->clientRepository = $clientRepository;
        $this->configurationTypeRepository = $configurationTypeRepository;
        $this->itemRepository = $itemRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @param FrontendConfigurationStructure $structure
     * @param Configuration $entity
     * @param string $entityclassname
     *
     * @psalm-suppress PossiblyNullPropertyAssignmentValue
     * @psalm-suppress InvalidPropertyAssignmentValue
     *
     * @return Configuration
     */
    public function convertOne($structure, $entity = null, $entityclassname = Configuration::class)
    {
        $configurationTypeIdentifier = $structure->configurationType;

        if (empty($structure->code)) {
            InvalidConfiguration::missingCode();
        }

        if (null === $entity) {
            $entity = new Configuration();
        }

        $entity->setCode($structure->code)
            ->setName($structure->name)
            ->setDesigndata($structure->designdata)
            ->setCustomdata($structure->customdata)
            ->setItem($this->itemRepository->findOneByIdentifier($structure->item->identifier))
            ->setConfigurationtype($this->configurationTypeRepository->findOneByIdentifier($configurationTypeIdentifier))
            ->setSelectedoptions($this->getSelectedOptionsFromOptionClassification($structure))
            ->setPartslisthash($this->generatePartsListHash($structure))
            ->setFinderResult($structure->finderResult);

        if (ConfigurationtypeRepository::DEFAULT_OPTIONS !== $configurationTypeIdentifier) {
            if (null !== $structure->client && null === $entity->getClient()) {
                $entity->setClient($this->clientRepository->findOneByIdentifier($structure->client));
            }

            if (null !== $structure->channel && null === $entity->getChannel()) {
                $entity->setChannel($this->channelRepository->findOneByIdentifier($structure->channel));
            }

            if (null !== $structure->language && null === $entity->getLanguage()) {
                $entity->setLanguage($this->languageRepository->findOneByIso($structure->language));
            }
        }

        return $entity;
    }

    /**
     * get selected options from optionclassifications as array.
     *
     * @param   $configurationStructure \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration|\Redhotmagma\ConfiguratorApiBundle\Structure\Configuration
     *
     * @return  array
     */
    protected function getSelectedOptionsFromOptionClassification($configurationStructure)
    {
        $selectedOptions = [];
        if (!empty($configurationStructure->optionclassifications)) {
            foreach ($configurationStructure->optionclassifications as $optionClassification) {
                if (!empty($optionClassification->selectedoptions)) {
                    foreach ($optionClassification->selectedoptions as $option) {
                        if (!isset($selectedOptions[$optionClassification->identifier])) {
                            $selectedOptions[$optionClassification->identifier] = [];
                        }

                        // save only relevant properties
                        $selectedOption = [];
                        $selectedOption['identifier'] = $option->identifier;

                        if (!empty($option->amount)) {
                            $selectedOption['amount'] = $option->amount;
                        }
                        if (!empty($option->inputText)) {
                            $selectedOption['inputText'] = $option->inputText;
                        }

                        $selectedOptions[$optionClassification->identifier][] = $selectedOption;
                    }
                }
            }
        }

        return $selectedOptions;
    }

    /**
     * @param FrontendConfigurationStructure $configuration
     *
     * @return string
     */
    private function generatePartsListHash(FrontendConfigurationStructure $configuration): string
    {
        $partsList = [];
        foreach ($configuration->optionclassifications as $optionClassification) {
            $partsList[] = $optionClassification->identifier;
        }

        // sort the part list to get identical codes even if the order differs
        natsort($partsList);
        $partsList = array_flip($partsList);

        foreach ($configuration->optionclassifications as $optionClassification) {
            $selectedOptions = [];
            if (!empty($optionClassification->selectedoptions)) {
                foreach ($optionClassification->selectedoptions as $selectedOption) {
                    $selectedOptions[] = $selectedOption->amount . '_' . $selectedOption->identifier;
                }
            }

            natsort($selectedOptions);

            $partsList[$optionClassification->identifier] = $selectedOptions;
        }

        $selectedOptionsString = json_encode($partsList);

        return sha1($selectedOptionsString);
    }
}
