<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

/**
 * @internal
 */
class ConfigurationCodeLength
{
    private const DEFAULT = 6;
    /**
     * @var array
     */
    private $configurationCodeLengths;

    /**
     * @param array $configurationCodeLengths
     */
    public function __construct(array $configurationCodeLengths)
    {
        $this->configurationCodeLengths = $configurationCodeLengths;
    }

    /**
     * @param $configurationType
     *
     * @return int
     */
    public function getCodeLengthByConfigurationType($configurationType): int
    {
        return $this->configurationCodeLengths[$configurationType] ?? self::DEFAULT;
    }
}
