<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ComponentThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ItemDetailImageRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ItemThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\ComponentThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Image\ItemImage;

/**
 * @internal
 */
final class ConfigurationImageProvider
{
    /**
     * @var ComponentThumbnailRepositoryInterface
     */
    private $componentThumbnailRepository;

    /**
     * @var ItemDetailImageRepositoryInterface
     */
    private $itemDetailImageRepository;

    /**
     * @var ItemThumbnailRepositoryInterface
     */
    private $itemThumbnailRepository;

    /**
     * @param ComponentThumbnailRepositoryInterface $componentThumbnailRepository
     * @param ItemDetailImageRepositoryInterface $itemDetailImageRepository
     * @param ItemThumbnailRepositoryInterface $itemThumbnailRepository
     */
    public function __construct(ComponentThumbnailRepositoryInterface $componentThumbnailRepository, ItemDetailImageRepositoryInterface $itemDetailImageRepository, ItemThumbnailRepositoryInterface $itemThumbnailRepository)
    {
        $this->componentThumbnailRepository = $componentThumbnailRepository;
        $this->itemDetailImageRepository = $itemDetailImageRepository;
        $this->itemThumbnailRepository = $itemThumbnailRepository;
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    public function provideDetailImageUrlMap(Item $item): array
    {
        $resultMap = [];
        $accessMap = $this->map($this->itemDetailImageRepository->findDetailImages($item));

        $defaultImage = $accessMap[$item->getIdentifier()] ?? '';

        $resultMap[$item->getIdentifier()] = $defaultImage;
        foreach ($item->getItem() as $child) {
            $childIdentifier = $child->getIdentifier();
            $resultMap[$childIdentifier] = $accessMap[$childIdentifier] ?? $defaultImage;
        }

        return $resultMap;
    }

    /**
     * @param Item $item
     *
     * @return array
     */
    public function provideThumbnailUrlMap(Item $item): array
    {
        $resultMap = [];
        $accessMap = $this->map($this->itemThumbnailRepository->findThumbnails($item));

        $defaultImage = $accessMap[$item->getIdentifier()] ?? '';

        $resultMap[$item->getIdentifier()] = $defaultImage;
        foreach ($item->getItem() as $child) {
            $childIdentifier = $child->getIdentifier();
            $resultMap[$childIdentifier] = $accessMap[$childIdentifier] ?? $defaultImage;
        }

        return $resultMap;
    }

    /**
     * @param Optionclassification[] $components
     *
     * @return array
     */
    public function provideComponentThumbnailUrlMap(array $components): array
    {
        return $this->map($this->componentThumbnailRepository->findThumbnails($components));
    }

    /**
     * @param ComponentThumbnail[]|ItemImage[] $images
     *
     * @return array
     */
    private function map(array $images): array
    {
        $map = [];
        foreach ($images as $image) {
            $map += $image->toMap();
        }

        return $map;
    }
}
