<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\FrontendConfigurationSaveEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\LoadByConfigurationCodeEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\LoadByItemIdentifierEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\ZipGenerationEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ZipCreator;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\DocumentGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\PrintFile\PrintFileGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\PrintFile\PrintFileSave;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationPrintFileSaveData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSaveData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSwitchOptionData;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\PrintFile;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ConfigurationLoadArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ConfigurationApi
{
    /**
     * @var AdminAreaConfigurationStructureFromEntityConverter
     */
    private $adminAreaConfigurationStructureFromEntityConverter;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ConfigurationDelete
     */
    private $configurationDelete;

    /**
     * @var ConfigurationSwitchOption
     */
    private $configurationSwitchOption;

    /**
     * @var ConfigurationSwitchOptionNoCheck
     */
    private $configurationSwitchOptionNoCheck;

    /**
     * @var ConfigurationValidator
     */
    private $configurationValidator;

    /**
     * @var DocumentGenerator
     */
    private $documentGenerator;

    /**
     * @var PrintFileGenerator
     */
    private $printFileGenerator;

    /**
     * @var PrintFileSave
     */
    private $printFileSave;

    /**
     * @var ZipCreator
     */
    private $zipCreator;

    /**
     * @var Security
     */
    private $security;

    /**
     * @param AdminAreaConfigurationStructureFromEntityConverter $adminAreaConfigurationStructureFromEntityConverter
     * @param ConfigurationRepository $configurationRepository
     * @param EventDispatcherInterface $eventDispatcher
     * @param ConfigurationDelete $configurationDelete
     * @param ConfigurationSwitchOption $configurationSwitchOption
     * @param ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck
     * @param ConfigurationValidator $configurationValidator
     * @param DocumentGenerator $documentGenerator
     * @param PrintFileGenerator $printFileGenerator
     * @param PrintFileSave $printFileSave
     * @param ZipCreator $zipCreator
     * @param Security $security
     */
    public function __construct(
        AdminAreaConfigurationStructureFromEntityConverter $adminAreaConfigurationStructureFromEntityConverter,
        ConfigurationRepository $configurationRepository,
        EventDispatcherInterface $eventDispatcher,
        ConfigurationDelete $configurationDelete,
        ConfigurationSwitchOption $configurationSwitchOption,
        ConfigurationSwitchOptionNoCheck $configurationSwitchOptionNoCheck,
        ConfigurationValidator $configurationValidator,
        DocumentGenerator $documentGenerator,
        PrintFileGenerator $printFileGenerator,
        PrintFileSave $printFileSave,
        ZipCreator $zipCreator,
        Security $security
    ) {
        $this->adminAreaConfigurationStructureFromEntityConverter = $adminAreaConfigurationStructureFromEntityConverter;
        $this->configurationRepository = $configurationRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->configurationDelete = $configurationDelete;
        $this->configurationSwitchOption = $configurationSwitchOption;
        $this->configurationSwitchOptionNoCheck = $configurationSwitchOptionNoCheck;
        $this->configurationValidator = $configurationValidator;
        $this->documentGenerator = $documentGenerator;
        $this->printFileGenerator = $printFileGenerator;
        $this->printFileSave = $printFileSave;
        $this->zipCreator = $zipCreator;
        $this->security = $security;
    }

    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $client = $this->security->getUser()->getMainClient();

        if (null !== $client) {
            $arguments->filters['_client_config_filter'] = $client;
        }

        $entities = $this->configurationRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->adminAreaConfigurationStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->configurationRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Configuration
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Configuration
    {
        /** @var Configuration $entity */
        $entity = $this->configurationRepository->findOneBy(['id' => $id]);

        if (!$entity) {
            throw new NotFoundException();
        }

        return $this->adminAreaConfigurationStructureFromEntityConverter->convertOne($entity);
    }

    public function delete(string $id): void
    {
        $this->configurationDelete->delete($id);
    }

    public function frontendSave(ConfigurationSaveData $configurationSaveData, ControlParameters $controlParameters = null): FrontendConfigurationStructure
    {
        $controlParameters = $controlParameters ?? new ControlParameters([]);

        $calculationEvent = new CalculateEvent($configurationSaveData->configuration);
        $this->eventDispatcher->dispatch($calculationEvent, CalculateEvent::NAME);
        $calculationResult = $calculationEvent->getCalculationResult();

        $saveEvent = new FrontendConfigurationSaveEvent($configurationSaveData, $controlParameters, $calculationResult);
        $this->eventDispatcher->dispatch($saveEvent, FrontendConfigurationSaveEvent::NAME);
        $configuration = $saveEvent->getConfiguration();

        $loadEvent = new LoadByConfigurationCodeEvent($configuration->getCode(), $controlParameters);
        $this->eventDispatcher->dispatch($loadEvent, LoadByConfigurationCodeEvent::NAME);

        return $loadEvent->getConfiguration();
    }

    /**
     * @return PrintFile[]
     */
    public function getPrintFileListResults(string $configurationCode): array
    {
        return $this->printFileGenerator->getList($configurationCode);
    }

    /**
     * @return string[]
     */
    public function uploadPrintData(ConfigurationPrintFileSaveData $configurationPrintFileSaveData): array
    {
        // save print file(s) if sent
        $imageNames = [];
        if (!empty($configurationPrintFileSaveData->printfiles) && !empty($configurationPrintFileSaveData->code)) {
            $imageNames = $this->printFileSave->save(
                $configurationPrintFileSaveData->printfiles,
                $configurationPrintFileSaveData->code
            );
        }

        return $imageNames;
    }

    public function loadByItemIdentifier(string $itemIdentifier, ControlParameters $controlParameters = null, ConfigurationLoadArguments $configurationLoadArguments = null): ?FrontendConfigurationStructure
    {
        $event = new LoadByItemIdentifierEvent($itemIdentifier);
        if (null !== $controlParameters) {
            $event->setControlParameters($controlParameters);
        }
        $this->eventDispatcher->dispatch($event, LoadByItemIdentifierEvent::NAME);
        $configuration = $event->getConfiguration();
        if ($configuration && $configurationLoadArguments && !empty($configurationLoadArguments->switchedOptions)) {
            $configuration = $this->switchOption(ConfigurationSwitchOptionData::from($configuration, $configurationLoadArguments->switchedOptions, true));
        }

        return $configuration;
    }

    public function loadByConfigurationCode(string $configurationCode, ControlParameters $controlParameters = null): ?FrontendConfigurationStructure
    {
        $controlParameters = $controlParameters ?? new ControlParameters([]);

        $loadEvent = new LoadByConfigurationCodeEvent($configurationCode, $controlParameters);
        $this->eventDispatcher->dispatch($loadEvent, LoadByConfigurationCodeEvent::NAME);

        return $loadEvent->getConfiguration();
    }

    public function switchOption(ConfigurationSwitchOptionData $configurationSwitchOptionData): ?FrontendConfigurationStructure
    {
        if ($configurationSwitchOptionData->noCheck) {
            $configuration = $this->configurationSwitchOptionNoCheck->switchOption(
                $configurationSwitchOptionData->configuration,
                $configurationSwitchOptionData->switchedOptions
            );
        } else {
            $configuration = $this->configurationSwitchOption->switchOption(
                $configurationSwitchOptionData->configuration,
                $configurationSwitchOptionData->switchedOptions
            );
        }

        return $configuration;
    }

    public function validateConfiguration(FrontendConfigurationStructure $configuration): array
    {
        return $this->configurationValidator->validate($configuration);
    }

    public function generateZip(string $configurationCode, ControlParameters $controlParameters = null): FileResult
    {
        if (null === $controlParameters) {
            $controlParameters = new ControlParameters();
        }

        $event = new ZipGenerationEvent($configurationCode, $controlParameters);

        $this->eventDispatcher->dispatch($event, ZipGenerationEvent::NAME);

        $zipPath = $this->zipCreator->createArchive($event->getFiles());

        return new FileResult($zipPath, $configurationCode . FileResult::EXT_ZIP);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['code', 'name', 'customdata'];
    }
}
