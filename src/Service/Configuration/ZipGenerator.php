<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use League\Csv\CannotInsertRecord;
use Redhotmagma\ConfiguratorApiBundle\Exception\InvalidAppConfig;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ZipModel;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\ConfigurationDocumentApi;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use SplFileInfo;
use Symfony\Component\Finder\Finder;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @internal
 */
class ZipGenerator
{
    private ConfigurationRepository $configurationRepository;
    private ConfigurationDocumentApi $configurationDocumentApi;
    private CsvGenerator $csvGenerator;
    private string $uploadPathPrivate;
    /** @var array<string, string> */
    private array $imageGalleryConfig;
    private string $webDocumentRoot;

    /**
     * @param array<string, string> $imageGalleryConfig
     */
    public function __construct(
        ConfigurationRepository $configurationRepository,
        ConfigurationDocumentApi $configurationDocumentApi,
        CsvGenerator $csvGenerator,
        string $uploadPathPrivate,
        array $imageGalleryConfig,
        string $webDocumentRoot
    ) {
        $this->configurationRepository = $configurationRepository;
        $this->configurationDocumentApi = $configurationDocumentApi;
        $this->csvGenerator = $csvGenerator;
        $this->uploadPathPrivate = $uploadPathPrivate;
        $this->imageGalleryConfig = $imageGalleryConfig;
        $this->webDocumentRoot = rtrim($webDocumentRoot, '/');
    }

    /**
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     *
     * @return ZipModel[]
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getPdfs(string $configurationCode, ControlParameters $controlParameters): array
    {
        $fileResult = $this->configurationDocumentApi->generatePdf('production', $configurationCode, $controlParameters);

        $productionPdf = ZipModel::new($fileResult->filePath, $fileResult->fileName);

        return [$productionPdf];
    }

    /**
     * @param string $configurationCode
     *
     * @return ZipModel[]
     */
    public function getUserImages(string $configurationCode): array
    {
        $configuration = $this->configurationRepository->findOneByCode($configurationCode);

        if (!$configuration) {
            throw new NotFoundException();
        }

        if (!array_key_exists('url_path', $this->imageGalleryConfig)) {
            throw InvalidAppConfig::missing('image_gallery.url_path');
        }

        $imageGalleryUrlPath = trim($this->imageGalleryConfig['url_path'], '/');

        $images = [];

        foreach ((array)$configuration->getDesigndata() as $designAreaIdentifier => $designArea) {
            if (empty($designArea->canvasData->objects)) {
                continue;
            }

            foreach ($designArea->canvasData->objects as $i => $object) {
                if ('Image' !== $object->type || empty($object->src)) {
                    continue;
                }

                // collect originally uploaded images for each used image in the design data
                $fileHash = explode('.', basename($object->src))[0];
                if ($file = $this->findUploadedFile($fileHash)) {
                    $images[] = ZipModel::new($file->getPathname(), $designAreaIdentifier . '_' . $i . '_' . $file->getFilename());
                }

                if (!isset($object->imageData, $object->imageData->originalFileName)) {
                    continue;
                }

                // collect imageGallery images
                $imageGalleryImagePath = $this->webDocumentRoot . '/' . trim($object->imageData->originalFileName, '/');
                if (false !== strpos($object->src, $imageGalleryUrlPath) && file_exists($imageGalleryImagePath)) {
                    $images[] = ZipModel::new($imageGalleryImagePath, $designAreaIdentifier . '_' . $i . '_' . basename($imageGalleryImagePath));
                }
            }
        }

        return $images;
    }

    /**
     * @param string $configurationCode
     *
     * @return ZipModel[]
     *
     * @throws CannotInsertRecord
     */
    public function getCsv(string $configurationCode): array
    {
        $fileResult = $this->csvGenerator->generateBulkNameCsv($configurationCode);

        if (null === $fileResult) {
            return [];
        }

        $csv = ZipModel::new($fileResult->filePath, $fileResult->fileName);

        return [$csv];
    }

    /**
     * @param string $fileHash
     *
     * @return SplFileInfo|null
     */
    private function findUploadedFile(string $fileHash): ?SplFileInfo
    {
        if (is_dir($this->uploadPathPrivate . $fileHash)) {
            $foundFiles = new Finder();
            $foundFiles->in($this->uploadPathPrivate . $fileHash);
            /** @var SplFileInfo $file */
            foreach ($foundFiles->getIterator() as $file) {
                return $file;
            }
        }

        return null;
    }
}
