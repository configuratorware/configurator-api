<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Exception\FileException;

/**
 * @internal
 */
class PrintDesignDataProvider
{
    private const HREF = 'xlink:href="';

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param string $mediaBasePath
     */
    public function __construct(string $mediaBasePath)
    {
        $this->mediaBasePath = rtrim($mediaBasePath, DIRECTORY_SEPARATOR);
    }

    /**
     * @param string $svgPath
     *
     * @return string
     */
    public function provide(string $svgPath): string
    {
        $svg = @file_get_contents($svgPath);

        if (empty($svg)) {
            throw FileException::notFound($svgPath);
        }

        if (false === strpos($svg, '<svg')) {
            throw FileException::wrongMimeType($svgPath, 'image/svg+xml');
        }

        // this is for:
        // 1. embedded images because colors have been changed
        // 2. backwards compatibility: if svgs already stored with embedded raster images
        if ($this->hasEmbeddedImage($svg)) {
            return $svg;
        }

        $imageLinks = $this->findImageLinks($svg);

        foreach ($imageLinks as $imageLink) {
            $withoutCacheHash = $this->removeCacheHash($imageLink);
            $svg = str_replace($imageLink, $this->mediaBasePath . $withoutCacheHash, $svg);
        }

        return $svg;
    }

    /**
     * @param string $svg
     *
     * @return bool
     */
    private function hasEmbeddedImage(string $svg): bool
    {
        return false !== strpos($svg, 'xlink:href="data:image');
    }

    /**
     * @param string $svg
     * @param string[] $imageLinks
     * @param int $offset
     *
     * @return string[]
     */
    private function findImageLinks(string $svg, array $imageLinks = [], int $offset = 0): array
    {
        $marker = mb_strpos($svg, self::HREF, $offset);

        if (false === $marker) {
            return $imageLinks;
        }

        $start = $marker + strlen(self::HREF);
        $length = mb_strpos(substr($svg, $start, strlen($svg)), '"');
        $imageLinks[] = mb_substr($svg, $start, $length);
        $offset = $start + (int)$length;

        return $this->findImageLinks($svg, $imageLinks, $offset);
    }

    /**
     * @param string $imageLink
     *
     * @return string
     */
    private function removeCacheHash(string $imageLink): string
    {
        $marker = mb_strpos($imageLink, '.png?');

        if (false === $marker) {
            return $imageLink;
        }

        return mb_substr($imageLink, 0, $marker + 4);
    }
}
