<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\AddVisualizationDataToStructureEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\ClientStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\CodeSnippet\CodeSnippetApi;
use Redhotmagma\ConfiguratorApiBundle\Service\ConstructionPattern\FrontendConstructionPattern;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration as ConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as ConfigurationFrontendStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class ConfigurationStructureFromEntityConverter
{
    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @var FrontendItemStructureFromEntityConverter
     */
    private $frontendItemStructureFromEntityConverter;

    /**
     * @var FrontendConstructionPattern
     */
    private $frontendConstructionPattern;

    /**
     * @var CodeSnippetApi
     */
    private $codeSnippetApi;

    /**
     * @var ConfigurationImages
     */
    private $configurationImages;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ClientStructureFromEntityConverter
     */
    private $clientStructureFromEntityConverter;

    /**
     * @param SettingRepository $settingRepository
     * @param FrontendItemStructureFromEntityConverter $frontendItemStructureFromEntityConverter
     * @param FrontendConstructionPattern $frontendConstructionPattern
     * @param CodeSnippetApi $codeSnippetApi
     * @param ConfigurationImages $configurationImages
     * @param EventDispatcherInterface $eventDispatcher
     * @param ClientStructureFromEntityConverter $clientStructureFromEntityConverter
     */
    public function __construct(
        SettingRepository $settingRepository,
        FrontendItemStructureFromEntityConverter $frontendItemStructureFromEntityConverter,
        FrontendConstructionPattern $frontendConstructionPattern,
        CodeSnippetApi $codeSnippetApi,
        ConfigurationImages $configurationImages,
        EventDispatcherInterface $eventDispatcher,
        ClientStructureFromEntityConverter $clientStructureFromEntityConverter
    ) {
        $this->settingRepository = $settingRepository;
        $this->frontendItemStructureFromEntityConverter = $frontendItemStructureFromEntityConverter;
        $this->frontendConstructionPattern = $frontendConstructionPattern;
        $this->codeSnippetApi = $codeSnippetApi;
        $this->configurationImages = $configurationImages;
        $this->eventDispatcher = $eventDispatcher;
        $this->clientStructureFromEntityConverter = $clientStructureFromEntityConverter;
    }

    /**
     * always use frontend item, its is only used for information, no editing.
     *
     * @param $configurationStructure
     * @param Configuration $configurationEntity
     * @param ControlParameters|null $controlParameters
     *
     * @return ConfigurationStructure|ConfigurationFrontendStructure
     */
    public function addItemToStructure($configurationStructure, Configuration $configurationEntity, ?ControlParameters $controlParameters = null)
    {
        $this->frontendItemStructureFromEntityConverter->setControlParameters($controlParameters);
        $itemEntity = $configurationEntity->getItem();
        if (null !== $itemEntity) {
            $configurationStructure->item = $this->frontendItemStructureFromEntityConverter->convertOne(
                $configurationEntity->getItem()
            );
        }

        return $configurationStructure;
    }

    /**
     * always use frontend item, its is only used for information, no edition.
     *
     * @param $configurationStructure
     * @param Configuration $configurationEntity
     */
    public function addClientToStructure($configurationStructure, $configurationEntity): void
    {
        $configurationStructure->client = $this->clientStructureFromEntityConverter->convertOne(
            $configurationEntity->getClient()
        );
    }

    /**
     * the option classifications also contain the selected options.
     *
     * @param $configurationStructure
     * @param Configuration $configurationEntity
     *
     * @return ConfigurationStructure|ConfigurationFrontendStructure
     */
    public function addOptionClassificationsToStructure($configurationStructure, $configurationEntity)
    {
        $constructionPattern = $this->frontendConstructionPattern->getConstructionPatternForConfiguration($configurationEntity);

        $configurationStructure->optionclassifications = $constructionPattern;

        return $configurationStructure;
    }

    /**
     * the option classifications also contain the selected options.
     *
     * @param $configurationStructure
     *
     * @return ConfigurationStructure|ConfigurationFrontendStructure
     */
    public function addInformationToStructure($configurationStructure)
    {
        $configurationStructure->information = new \stdClass();

        $configurationStructure->information->channelSettings = C_CHANNEL_SETTINGS;
        $configurationStructure->information->languageSettings = C_LANGUAGE_SETTINGS;

        $settings = $this->settingRepository->getSettingAsArray();

        if (!empty($settings)) {
            $configurationStructure->information->settings = $settings;
        }

        // add share link
        if (!empty($configurationStructure->information->settings['shareurl'])) {
            $configurationStructure->shareUrl = str_replace(
                '{code}',
                $configurationStructure->code,
                $configurationStructure->information->settings['shareurl']
            );
        }

        // add code snippets
        $codeSnippets = $this->codeSnippetApi->findCurrent();
        $configurationStructure->information->codeSnippets = $codeSnippets;

        return $configurationStructure;
    }

    /**
     * @param ConfigurationStructure|ConfigurationFrontendStructure $configurationStructure
     *
     * @return ConfigurationStructure|ConfigurationFrontendStructure
     */
    public function addPreviewImageURLToStructure($configurationStructure)
    {
        $configurationStructure->previewImageURL = $this->configurationImages->getPreviewImage($configurationStructure->code);

        return $configurationStructure;
    }

    /**
     * @param $configurationStructure
     *
     * @return ConfigurationStructure|ConfigurationFrontendStructure
     */
    public function addImagesToStructure($configurationStructure)
    {
        $configurationStructure->images = $this->configurationImages->getImagesForConfiguration($configurationStructure->code);

        return $configurationStructure;
    }

    /**
     * @param $configurationStructure
     *
     * @return ConfigurationStructure|ConfigurationFrontendStructure
     */
    public function addVisualizationDataToStructure($configurationStructure)
    {
        $event = new AddVisualizationDataToStructureEvent($configurationStructure);
        $this->eventDispatcher->dispatch($event, AddVisualizationDataToStructureEvent::NAME);
        $configurationStructure = $event->getConfiguration();

        return $configurationStructure;
    }
}
