<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\Foreground;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\Layer;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\LayerViewThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationData3dScene;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationData3dVariant;
use stdClass;

/**
 * @internal
 */
class VisualizationData
{
    /**
     * @var Layer
     */
    private $visualizationDataLayer;

    /**
     * @var Foreground
     */
    private $visualizationDataForeground;

    /**
     * @var LayerViewThumbnail
     */
    private $layerViewThumbnail;

    /**
     * @var VisualizationData3dScene
     */
    private $visualizationData3dScene;

    /**
     * @var VisualizationData3dVariant
     */
    private $visualizationData3dVariant;

    /**
     * VisualizationData constructor.
     *
     * @param Layer $visualizationDataLayer
     * @param Foreground $visualizationDataForeground
     * @param LayerViewThumbnail $layerViewThumbnail
     * @param VisualizationData3dScene $visualizationData3dScene
     * @param VisualizationData3dVariant $visualizationData3dVariant
     */
    public function __construct(
        Layer $visualizationDataLayer,
        Foreground $visualizationDataForeground,
        LayerViewThumbnail $layerViewThumbnail,
        VisualizationData3dScene $visualizationData3dScene,
        VisualizationData3dVariant $visualizationData3dVariant
    ) {
        $this->visualizationDataLayer = $visualizationDataLayer;
        $this->visualizationDataForeground = $visualizationDataForeground;
        $this->layerViewThumbnail = $layerViewThumbnail;
        $this->visualizationData3dScene = $visualizationData3dScene;
        $this->visualizationData3dVariant = $visualizationData3dVariant;
    }

    /**
     * @param $configuration
     */
    public function addToConfiguration($configuration): void
    {
        // init visualization data
        if (empty($configuration->visualizationData)) {
            $configuration->visualizationData = new stdClass();
        }

        if (!empty($configuration->item->settings)) {
            $visualizationMode = $configuration->item->settings->visualizationCreator;

            if (VisualizationMode::IDENTIFIER_2D_LAYER === $visualizationMode) {
                $this->visualizationDataLayer->addViewImages($configuration);
                $configuration->visualizationData->viewThumbImages = $this->layerViewThumbnail->getThumbnails($configuration->item->identifier);
            }

            if (VisualizationMode::is2dVisualization($visualizationMode)) {
                $this->visualizationDataForeground->addForegroundImages($configuration);
            }

            if (VisualizationMode::IDENTIFIER_3D_SCENE === $visualizationMode) {
                $configuration->visualizationData = $this->visualizationData3dScene->getData($configuration);
            }

            if (VisualizationMode::IDENTIFIER_3D_VARIANT === $visualizationMode) {
                $configuration->visualizationData = $this->visualizationData3dVariant->getData($configuration);
            }
        }
    }

    /**
     * @param $configuration
     *
     * @deprecated use $this->addToConfiguration instead
     */
    public function getVisualizationData($configuration): void
    {
        $this->addToConfiguration($configuration);
    }
}
