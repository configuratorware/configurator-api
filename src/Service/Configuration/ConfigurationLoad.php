<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration as ConfigurationEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\SettingRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\ItemAvailability;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification as OptionClassificationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;

/**
 * @internal
 */
class ConfigurationLoad
{
    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var ConfigurationValidator
     */
    private $configurationValidator;

    /**
     * @var DefaultConfigurationGenerator
     */
    private $defaultConfigurationGenerator;

    /**
     * @var FrontendConfigurationStructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var ItemAvailability
     */
    private $itemAvailability;

    /**
     * @var ConfigurationImageProvider
     */
    private $configurationImageProvider;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var MinimumOrderAmount
     */
    private $minimumOrderAmount;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var SettingRepository
     */
    private $settingRepository;

    /**
     * @param ConfigurationRepository $configurationRepository
     * @param ConfigurationValidator $configurationValidator
     * @param DefaultConfigurationGenerator $defaultConfigurationGenerator
     * @param FrontendConfigurationStructureFromEntityConverter $structureFromEntityConverter
     * @param ItemAvailability $itemAvailability
     * @param ConfigurationImageProvider $configurationImageProvider
     * @param ItemRepository $itemRepository
     * @param MinimumOrderAmount $minimumOrderAmount
     * @param SettingRepository $settingRepository
     * @param Client $client
     */
    public function __construct(
        ConfigurationRepository $configurationRepository,
        ConfigurationValidator $configurationValidator,
        DefaultConfigurationGenerator $defaultConfigurationGenerator,
        FrontendConfigurationStructureFromEntityConverter $structureFromEntityConverter,
        ItemAvailability $itemAvailability,
        ConfigurationImageProvider $configurationImageProvider,
        ItemRepository $itemRepository,
        MinimumOrderAmount $minimumOrderAmount,
        SettingRepository $settingRepository,
        Client $client
    ) {
        $this->configurationRepository = $configurationRepository;
        $this->configurationValidator = $configurationValidator;
        $this->defaultConfigurationGenerator = $defaultConfigurationGenerator;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->itemAvailability = $itemAvailability;
        $this->configurationImageProvider = $configurationImageProvider;
        $this->itemRepository = $itemRepository;
        $this->minimumOrderAmount = $minimumOrderAmount;
        $this->settingRepository = $settingRepository;
        $this->client = $client;
    }

    /**
     * @param string $itemIdentifier
     * @param ControlParameters|null $controlParameters
     *
     * @return Configuration
     */
    public function loadByItemIdentifier(string $itemIdentifier, ControlParameters $controlParameters = null): Configuration
    {
        $item = $this->itemRepository->findOneByIdentifier($itemIdentifier);
        if (null === $item) {
            throw new NotFoundException();
        }
        // if the item has a parent get the data from the parent
        if (null !== $item->getParent()) {
            $item = $item->getParent();
        }

        $configuration = $this->defaultConfigurationGenerator->getDefaultConfigurationForItem($item);

        $configurationStructure = $this->loadByCode($configuration->getCode(), $controlParameters);

        if (null === $configurationStructure) {
            throw NotFoundException::entity(Item::class);
        }

        $configurationStructure = $this->minimumOrderAmount->addSelectedAmountsToStructure($configurationStructure, $itemIdentifier);

        return $configurationStructure;
    }

    /**
     * @param string $configurationCode
     * @param ControlParameters|null $controlParameters
     *
     * @return Configuration|null
     */
    public function loadByCode(string $configurationCode, ControlParameters $controlParameters = null): ?Configuration
    {
        if (empty($configurationCode)) {
            return null;
        }

        $configurationEntity = $this->configurationRepository->findOneByCode($configurationCode);

        if (null === $configurationEntity || null === $configurationEntity->getItem()) {
            return null;
        }

        $item = $configurationEntity->getItem();

        $itemIsAvailable = $this->itemAvailability->checkAvailability($item, $controlParameters);

        if (false === $itemIsAvailable) {
            return null;
        }

        if ($this->isConfigurationClientRestricted($configurationEntity)) {
            return null;
        }

        $this->structureFromEntityConverter->setControlParameters($controlParameters);
        $configurationStructure = $this->structureFromEntityConverter->convertOne($configurationEntity);

        $configurationStructure = $this->configurationValidator->validateStructure(
            $configurationStructure,
            $configurationEntity->getSelectedoptions(),
            false,
            false
        );

        $this->addImages($configurationStructure, $item);

        return $configurationStructure;
    }

    /**
     * @param ConfigurationEntity $configuration
     *
     * @return bool
     */
    private function isConfigurationClientRestricted(ConfigurationEntity $configuration): bool
    {
        $clientIdentifier = $this->client->getClient()->identifier;
        $configurationClientIdentifier = $configuration->getClientIdentifier();

        if (null !== $configurationClientIdentifier && $clientIdentifier !== $configurationClientIdentifier) {
            $setting = current($this->settingRepository->findAll());

            return $setting->getConfigurationsClientRestricted();
        }

        return false;
    }

    /**
     * @param Configuration $configurationStructure
     * @param Item $item
     */
    private function addImages(Configuration $configurationStructure, Item $item): void
    {
        $detailImageExternalUrlMap = $this->configurationImageProvider->provideDetailImageUrlMap($item);
        $thumbnailExternalUrlMap = $this->configurationImageProvider->provideThumbnailUrlMap($item);

        $configurationStructure->item->detailImage = $detailImageExternalUrlMap[$item->getIdentifier()];
        $configurationStructure->item->thumbnail = $thumbnailExternalUrlMap[$item->getIdentifier()];

        $this->addItemImages($configurationStructure->item->itemGroup, $detailImageExternalUrlMap, $thumbnailExternalUrlMap);

        $optionClassifications = array_map(static function (ItemOptionclassification $itemOptionClassification) {
            return $itemOptionClassification->getOptionclassification();
        }, $item->getItemOptionclassification()->toArray());

        $componentThumbnailImageMap = $this->configurationImageProvider->provideComponentThumbnailUrlMap($optionClassifications);

        $this->addComponentImages($configurationStructure->optionclassifications, $componentThumbnailImageMap);
    }

    /**
     * @param \stdClass|null $group
     * @param array $detailImageMap
     * @param array $thumbnailMap
     */
    private function addItemImages(?\stdClass $group, array $detailImageMap, array $thumbnailMap): void
    {
        if (is_object($group) && property_exists($group, 'children')) {
            foreach ($group->children as $child) {
                if (isset($child->identifier)) {
                    $child->detailImage = $detailImageMap[$child->identifier] ?? '';
                    $child->thumbnail = $thumbnailMap[$child->identifier] ?? '';
                }

                if (property_exists($child, 'itemGroup')) {
                    $this->addItemImages($child->itemGroup, $detailImageMap, $thumbnailMap);
                }
            }
        }
    }

    /**
     * @param OptionclassificationStructure[] $componentStructures
     * @param array <string, ComponentThumbnail> $componentThumbnailImageMap
     */
    private function addComponentImages(array $componentStructures, array $componentThumbnailImageMap): void
    {
        foreach ($componentStructures as $componentStructure) {
            if (array_key_exists($componentStructure->identifier, $componentThumbnailImageMap)) {
                $componentStructure->thumbnail = $componentThumbnailImageMap[$componentStructure->identifier];
            }
        }
    }
}
