<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\FrontendConfigurationStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class FrontendConfigurationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var ControlParameters|null
     */
    private $controlParameters;

    /**
     * AdminAreaConfigurationStructureFromEntityConverter constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration $entity
     * @param string $structureClassName
     *
     * @return Configuration
     */
    public function convertOne($entity, $structureClassName = Configuration::class)
    {
        $event = new FrontendConfigurationStructureFromEntityEvent($entity, $structureClassName, $this->controlParameters);
        $this->eventDispatcher->dispatch($event, FrontendConfigurationStructureFromEntityEvent::NAME);
        $structure = $event->getConfigurationStructure();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Configuration::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param ControlParameters|null $controlParameters
     */
    public function setControlParameters(?ControlParameters $controlParameters): void
    {
        $this->controlParameters = $controlParameters;
    }
}
