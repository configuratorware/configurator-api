<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\AdminAreaConfigurationStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\Configuration;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class AdminAreaConfigurationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * AdminAreaConfigurationStructureFromEntityConverter constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Configuration $entity
     * @param string $structureClassName
     *
     * @return Configuration
     */
    public function convertOne($entity, $structureClassName = Configuration::class)
    {
        $event = new AdminAreaConfigurationStructureFromEntityEvent($entity, $structureClassName);
        $this->eventDispatcher->dispatch($event, AdminAreaConfigurationStructureFromEntityEvent::NAME);
        $structure = $event->getConfigurationStructure();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Configuration::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
