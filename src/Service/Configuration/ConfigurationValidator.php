<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\ConfigurationValidationEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionInputValidation\OptionInputValidation;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationComponentValidationError;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationComponentValidationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\ConfigurationValidationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class ConfigurationValidator
{
    /**
     * @var CheckResolver
     */
    private $checkResolver;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var OptionInputValidation
     */
    private $optionInputValidation;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public const VALIDATION_RESULT_NO_SELECTED_OPTION_IN_MANDATORY_COMPONENT = 'no_selected_option_in_mandatory_component';
    public const VALIDATION_RESULT_SELECTED_OPTION_NO_LONGER_AVAILABLE = 'selected_option_no_longer_available';
    public const VALIDATION_RESULT_MINAMOUNT_NOT_MET = 'minamount_not_met';

    /**
     * ConfigurationValidator constructor.
     *
     * @param CheckResolver $checkResolver
     * @param ItemRepository $itemRepository
     * @param OptionRepository $optionRepository
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        CheckResolver $checkResolver,
        ItemRepository $itemRepository,
        OptionRepository $optionRepository,
        EventDispatcherInterface $eventDispatcher,
        OptionInputValidation $optionInputValidation
    ) {
        $this->checkResolver = $checkResolver;
        $this->itemRepository = $itemRepository;
        $this->optionRepository = $optionRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->optionInputValidation = $optionInputValidation;
    }

    /**
     * @param Configuration $configuration
     *
     * @return array
     */
    public function validate(Configuration $configuration)
    {
        $configuration = $this->validateStructure($configuration, false, true);

        $validationResult = $configuration->information->validation_result;

        if (true !== $validationResult->valid) {
            throw new ValidationException($validationResult->validationErrors);
        }

        return ['success' => true];
    }

    /**
     * @param Configuration $configuration
     * @param bool $checkMinAmounts
     *
     * @return Configuration
     */
    public function validateStructure(
        Configuration $configuration,
        $selectedOptionsFromSavedConfiguration,
        bool $checkMinAmounts = false,
        bool $validateOptionInputTexts = true
    ) {
        $validationResult = new ConfigurationValidationResult();

        // we start optimistically
        $validationResult->valid = true;

        // get the item to know which components need to be checked
        /** @var Item $baseItem */
        $baseItem = $this->itemRepository->findOneBy(['identifier' => $configuration->item->identifier]);

        if (!empty($baseItem)) {
            // iterate over all components and do the checks
            /** @var OptionClassification $component */
            foreach ($configuration->optionclassifications as $component) {
                $componentCheckResult = $this->validateComponent(
                    $component,
                    $baseItem,
                    $configuration,
                    $checkMinAmounts
                );

                if (false === $componentCheckResult->valid) {
                    $validationResult->valid = false;
                    $validationResult->validationErrors[$component->identifier] = $componentCheckResult;
                }
            }

            // also check if options are no longer available - simply compare the saved selected options to what we
            // have in the configuration now (deactivated or deleted options are not part of the configuration anymore)
            // but only do this if we are at load check ($checkMinAmounts === false)
            if (false === $checkMinAmounts) {
                $validationResult = $this->areOptionsNoLongerAvailable(
                    $configuration,
                    $selectedOptionsFromSavedConfiguration,
                    $validationResult
                );
            }

            if (true === $validateOptionInputTexts) {
                $optionValidationResult = $this->optionInputValidation->validateConfiguration($configuration);
                if ($optionValidationResult) {
                    $validationResult->valid = false;
                    $validationResult->validationErrors = array_merge($validationResult->validationErrors, $optionValidationResult);
                }
            }
        }

        // hook for additional validation in client code
        $event = new ConfigurationValidationEvent($configuration, $validationResult);
        $this->eventDispatcher->dispatch($event, ConfigurationValidationEvent::NAME);
        $validationResult = $event->getValidationResult();

        $configuration->information->validation_result = $validationResult;

        return $configuration;
    }

    /**
     * @param Configuration $configuration
     * @param array|bool $selectedOptionsFromSavedConfiguration
     * @param ConfigurationValidationResult $validationResult
     *
     * @return ConfigurationValidationResult
     */
    private function areOptionsNoLongerAvailable(
        Configuration $configuration,
        $selectedOptionsFromSavedConfiguration,
        ConfigurationValidationResult $validationResult
    ) {
        if (!empty($selectedOptionsFromSavedConfiguration)) {
            foreach ($configuration->optionclassifications as $componentCurrently) {
                // first we collect both the option identifiers from the saved and current configuration to diff them
                $optionIdentifiersSaved = [];
                $optionIdentifiersCurrent = [];

                if (isset($selectedOptionsFromSavedConfiguration[$componentCurrently->identifier])) {
                    foreach ($selectedOptionsFromSavedConfiguration[$componentCurrently->identifier] as $savedOption) {
                        $optionIdentifiersSaved[] = $savedOption['identifier'];
                    }
                }

                foreach ((array)$componentCurrently->selectedoptions as $currentOption) {
                    $optionIdentifiersCurrent[] = $currentOption->identifier;
                }

                $diff = array_diff($optionIdentifiersSaved, $optionIdentifiersCurrent);

                // saved and current are different: this is an error
                if (!empty($diff)) {
                    $error = new ConfigurationComponentValidationError(self::VALIDATION_RESULT_SELECTED_OPTION_NO_LONGER_AVAILABLE, '', null, $diff);
                    if (!isset($validationResult->validationErrors[$componentCurrently->identifier])) {
                        $validationResult->validationErrors[$componentCurrently->identifier]
                            = new ConfigurationComponentValidationResult(false, [], $componentCurrently->title);
                    }
                    $validationResult->validationErrors[$componentCurrently->identifier]->errors[] = $error;
                }
            }
        }

        return $validationResult;
    }

    /**
     * @param OptionClassification $component
     * @param Item $baseItem
     * @param Configuration $configuration
     * @param bool $checkMinAmounts
     *
     * @return ConfigurationComponentValidationResult
     */
    private function validateComponent(
        OptionClassification $component,
        Item $baseItem,
        Configuration $configuration,
        bool $checkMinAmounts = false
    ) {
        $validateComponentResult = new ConfigurationComponentValidationResult();
        $validateComponentResult->componentTitle = $component->title;

        /* we check for
         * - no_selected_option_in_mandatory_component
         * - minamount_not_met
         */
        if ($component->is_mandatory && empty($component->selectedoptions)) {
            $validateComponentResult->valid = false;
            $error = new ConfigurationComponentValidationError(self::VALIDATION_RESULT_NO_SELECTED_OPTION_IN_MANDATORY_COMPONENT);
            $validateComponentResult->errors[] = $error;
        }

        if (true === $checkMinAmounts) {
            $minAmountResult = $this->checkResolver->checkOptionClassificationMinAmount(
                $component->identifier,
                $baseItem->getIdentifier(),
                $configuration
            );

            if (!empty($minAmountResult) && false === $minAmountResult->status) {
                $validateComponentResult->valid = false;
                $error = new ConfigurationComponentValidationError(self::VALIDATION_RESULT_MINAMOUNT_NOT_MET);

                // Get the amounts from the first result. For component min amount checks only one result is expected
                $error->amounts = $minAmountResult->check_results[0]->data;
                $validateComponentResult->errors[] = $error;
            }
        }

        return $validateComponentResult;
    }
}
