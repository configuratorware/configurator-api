<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\SwitchOptionEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Structure\Rules\CheckResults;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class ConfigurationSwitchOptionNoCheck
{
    /**
     * @var CheckResolver
     */
    private $checkResolver;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var SwitchOptionFilter
     */
    private $switchOptionFilter;

    /**
     * ConfigurationSwitchOptionNoCheck constructor.
     *
     * @param CheckResolver $checkResolver
     * @param EventDispatcherInterface $eventDispatcher
     * @param SwitchOptionFilter $switchOptionFilter
     */
    public function __construct(
        CheckResolver $checkResolver,
        EventDispatcherInterface $eventDispatcher,
        SwitchOptionFilter $switchOptionFilter
    ) {
        $this->checkResolver = $checkResolver;
        $this->eventDispatcher = $eventDispatcher;
        $this->switchOptionFilter = $switchOptionFilter;
    }

    /**
     * @param Configuration $configuration
     * @param $switchedOptions
     *
     * @return Configuration
     */
    public function switchOption(Configuration $configuration, $switchedOptions)
    {
        // filter out invalid switch option objects (empty identifier)
        $switchedOptions = $this->switchOptionFilter->filterOptionToSwitch($switchedOptions);
        $checkResults = new CheckResults();
        $checkResults->status = true;
        $configuration->check_results = $checkResults;

        $event = new SwitchOptionEvent($configuration, $switchedOptions);
        $this->eventDispatcher->dispatch($event, SwitchOptionEvent::NAME);
        $configuration = $event->getConfiguration();

        $removedOptions = $event->getRemovedOptions();

        // reverse check additional options
        $configuration = $this->checkResolver->handleAdditionalOptionRules($configuration, $removedOptions);

        return $configuration;
    }
}
