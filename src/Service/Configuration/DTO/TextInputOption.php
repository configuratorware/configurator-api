<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DTO;

/**
 * @internal
 */
final class TextInputOption
{
    /**
     * @var string
     *
     * @psalm-readonly
     */
    public $identifier;

    /**
     * @var string
     *
     * @psalm-readonly
     */
    public $inputValidationType;

    /**
     * @param string $identifier
     * @param string $inputValidationType
     */
    public function __construct(string $identifier, string $inputValidationType)
    {
        $this->identifier = $identifier;
        $this->inputValidationType = $inputValidationType;
    }
}
