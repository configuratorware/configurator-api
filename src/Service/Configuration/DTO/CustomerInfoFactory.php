<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DTO;

use Redhotmagma\ConfiguratorApiBundle\Entity\Channel;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client;
use Redhotmagma\ConfiguratorApiBundle\Repository\LanguageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;

/**
 * @internal
 */
final class CustomerInfoFactory
{
    /**
     * @var LanguageRepository
     */
    private $languageRepository;

    /**
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageRepository $languageRepository)
    {
        $this->languageRepository = $languageRepository;
    }

    public function create(ControlParameters $controlParameters): CustomerInfo
    {
        $clientIdentifier = $controlParameters->getParameter(ControlParameters::CLIENT, Client::DEFAULT_IDENTIFIER);
        $channelIdentifier = $controlParameters->getParameter(ControlParameters::CHANNEL, Channel::DEFAULT_IDENTIFIER);

        $languageIso = $controlParameters->getParameter(ControlParameters::LANGUAGE);
        if (null === $languageIso) {
            $language = $this->languageRepository->findDefault();
            $languageIso = $language->getIso();
        }

        return new CustomerInfo($clientIdentifier, $channelIdentifier, $languageIso);
    }
}
