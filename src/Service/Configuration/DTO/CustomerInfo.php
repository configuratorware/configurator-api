<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DTO;

/**
 * @internal
 */
final class CustomerInfo
{
    /**
     * @var string
     *
     * @psalm-readonly
     */
    public $clientIdentifier;

    /**
     * @var string
     *
     * @psalm-readonly
     */
    public $channelIdentifier;

    /**
     * @var string
     *
     * @psalm-readonly
     */
    public $languageIso;

    /**
     * @param string $clientIdentifier
     * @param string $channelIdentifier
     * @param string $languageIso
     */
    public function __construct(string $clientIdentifier, string $channelIdentifier, string $languageIso)
    {
        $this->clientIdentifier = $clientIdentifier;
        $this->channelIdentifier = $channelIdentifier;
        $this->languageIso = $languageIso;
    }
}
