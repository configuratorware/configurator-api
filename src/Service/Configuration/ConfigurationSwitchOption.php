<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\AddVisualizationDataToStructureEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Configuration\SwitchOptionEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\Rules\CheckResultsPostprocessingEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\OptionPriceCheckResult;
use Redhotmagma\ConfiguratorApiBundle\Service\Rules\CheckResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class ConfigurationSwitchOption
{
    /**
     * @var CheckResolver
     */
    private $checkResolver;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionPriceCheckResult
     */
    private $optionPriceCheckResult;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var FrontendOptionStructureFromEntityConverter
     */
    private $frontendOptionStructureFromEntityConverter;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var SwitchOptionFilter
     */
    private $switchOptionFilter;

    /**
     * ConfigurationSwitchOption constructor.
     *
     * @param CheckResolver $checkResolver
     * @param ItemRepository $itemRepository
     * @param OptionPriceCheckResult $optionPriceCheckResult
     * @param OptionRepository $optionRepository
     * @param FrontendOptionStructureFromEntityConverter $frontendOptionStructureFromEntityConverter
     * @param EventDispatcherInterface $eventDispatcher
     * @param SwitchOptionFilter $switchOptionFilter
     */
    public function __construct(
        CheckResolver $checkResolver,
        ItemRepository $itemRepository,
        OptionPriceCheckResult $optionPriceCheckResult,
        OptionRepository $optionRepository,
        FrontendOptionStructureFromEntityConverter $frontendOptionStructureFromEntityConverter,
        EventDispatcherInterface $eventDispatcher,
        SwitchOptionFilter $switchOptionFilter
    ) {
        $this->checkResolver = $checkResolver;
        $this->itemRepository = $itemRepository;
        $this->optionPriceCheckResult = $optionPriceCheckResult;
        $this->optionRepository = $optionRepository;
        $this->frontendOptionStructureFromEntityConverter = $frontendOptionStructureFromEntityConverter;
        $this->eventDispatcher = $eventDispatcher;
        $this->switchOptionFilter = $switchOptionFilter;
    }

    /**
     * @param Configuration $configuration
     * @param $switchedOptions
     * @param bool $addVisualizationDataAtSwitch
     *
     * @return mixed|Configuration
     */
    public function switchOption(Configuration $configuration, $switchedOptions, bool $addVisualizationDataAtSwitch = true)
    {
        // filter out invalid switch option objects (empty identifier)
        $switchedOptions = $this->switchOptionFilter->filterOptionToSwitch($switchedOptions);

        // backup configuration, it has to be reset to this state if tests fail later
        // create clone by encode/decode json
        $backupConfiguration = unserialize(serialize($configuration));

        // run checks for all options that are switched, perform the switch only if all checks pass
        // use configuration from check result for further operations, it could have been changed through check actions
        $checkResults = $this->checkResolver->runChecksForSwitchOptions($switchedOptions, $configuration);

        if (!empty($checkResults->configuration)) {
            $configuration = $checkResults->configuration;
        }
        // clear configuration data from check results to prevent redundancy
        $checkResults->configuration = null;
        $configuration->check_results = $checkResults;

        $event = new SwitchOptionEvent($configuration, $switchedOptions, $addVisualizationDataAtSwitch);
        $this->eventDispatcher->dispatch($event, SwitchOptionEvent::NAME);
        $configuration = $event->getConfiguration();
        $removedOptions = $event->getRemovedOptions();

        // run checks for removed options
        // they must not be required by a selected option
        // -> reverse option dependency check
        if (!empty($removedOptions)) {
            foreach ($removedOptions as $removedOptionIdentifier) {
                $reverseCheckResults = $this->checkResolver->reverseOptionDependencyCheck(
                    $configuration,
                    $removedOptionIdentifier
                );

                if (false == $reverseCheckResults->status) {
                    $reverseCheckResults->check_results = array_merge(
                        $reverseCheckResults->check_results,
                        $configuration->check_results->check_results
                    );
                    unset($reverseCheckResults->configuration);

                    $configuration->check_results = $reverseCheckResults;
                }
            }
        }

        // reverse check additional options
        $configuration = $this->checkResolver->handleAdditionalOptionRules($configuration, $removedOptions);

        // run checks on base item
        /** @var Item $baseItem */
        $baseItem = $this->itemRepository->findOneByIdentifier($configuration->item->identifier);

        if ($baseItem) {
            $checkResults = $this->checkResolver->executeItemChecks(
                $baseItem,
                $configuration,
                $switchedOptions
            );

            // merge test results and add them to the backup configuration that should be returned here
            if (!empty($configuration->check_results->check_results)) {
                $checkResults->check_results = array_merge(
                    $checkResults->check_results,
                    $configuration->check_results->check_results
                );

                // if the results already have failed take the failed status to the current checks even if they would have passed
                if (false == $configuration->check_results->status) {
                    $checkResults->status = false;
                }
            }

            unset($checkResults->configuration);
            $configuration->check_results = $checkResults;
        }

        // check results postprocessing
        $event = new CheckResultsPostprocessingEvent($configuration);
        $this->eventDispatcher->dispatch($event, CheckResultsPostprocessingEvent::NAME);
        $configuration = $event->getConfiguration();

        // add prices to options in check results
        $configuration = $this->optionPriceCheckResult->addOptionPricesToCheckResults($configuration);

        // return the backup configuration if the checks failed
        if (false === $configuration->check_results->status) {
            $backupConfiguration->check_results = $configuration->check_results;
            $configuration = $backupConfiguration;
        }

        if (empty($configuration->check_results->check_results)) {
            $configuration->check_results->status = true;
        }

        if (true === $addVisualizationDataAtSwitch) {
            $event = new AddVisualizationDataToStructureEvent($configuration);
            $this->eventDispatcher->dispatch($event, AddVisualizationDataToStructureEvent::NAME);
            $configuration = $event->getConfiguration();
        }

        return $configuration;
    }
}
