<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class ConfigurationScreenshot
{
    /**
     * @var MediaHelper
     */
    private $mediaHelper;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * ConfigurationScreenshot constructor.
     *
     * @param MediaHelper $mediaHelper
     * @param Filesystem $fileSystem
     */
    public function __construct(MediaHelper $mediaHelper, Filesystem $fileSystem)
    {
        $this->mediaHelper = $mediaHelper;
        $this->fileSystem = $fileSystem;
    }

    /**
     * @param object|null $screenShots
     * @param string $code
     */
    public function save($screenShots, string $code): void
    {
        if (!is_object($screenShots)) {
            return;
        }

        $imagePath = $this->mediaHelper->getImageBasePath() . '/configurations/' . $code;

        foreach ($screenShots as $itemIdentifier => $item) {
            if (is_object($item)) {
                foreach ($item as $screenShotIdentifier => $screenShot) {
                    $this->decodeAndDumpFile(
                        $imagePath . '/' . $itemIdentifier . '/' . $screenShotIdentifier . '.png',
                        $screenShot
                    );
                }
            } else {
                // this case is for backwards compatibility
                $this->decodeAndDumpFile(
                    $imagePath . '/' . $itemIdentifier . '.png',
                    $item
                );
            }
        }
    }

    /**
     * @param array $designAreaSnapshots
     * @param string $code
     */
    public function saveDesignAreas($designAreaSnapshots, $code)
    {
        if (!empty($designAreaSnapshots)) {
            $imageDir = '/configurations/' . $code . '/designAreas';

            $imagePath = $this->mediaHelper->getImageBasePath() . $imageDir;

            if (!$this->fileSystem->exists($imagePath)) {
                $this->fileSystem->mkdir($imagePath);
            }

            foreach ($designAreaSnapshots as $designAreaIdentifier => $screenshots) {
                foreach ($screenshots as $i => $screenshot) {
                    $imageName = $designAreaIdentifier . '_' . $i . '.svg';
                    file_put_contents($imagePath . '/' . $imageName, $screenshot);
                }
            }
        }
    }

    /**
     * @param string $fileName
     * @param string|null $screenShot
     */
    private function decodeAndDumpFile(string $fileName, ?string $screenShot): void
    {
        if (is_string($screenShot)) {
            $encodedImage = str_replace('data:image/png;base64,', '', $screenShot);
            $encodedImage = str_replace(' ', '+', $encodedImage);

            $this->fileSystem->dumpFile(
                $fileName,
                base64_decode($encodedImage)
            );
        }
    }
}
