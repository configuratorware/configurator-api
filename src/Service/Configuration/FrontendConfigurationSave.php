<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\PrintFile\PrintFileSave;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationSaveData;

/**
 * @internal
 */
class FrontendConfigurationSave
{
    /**
     * @var ConfigurationCodeLength
     */
    private $configurationCodeLength;

    /**
     * @var ConfigurationCodeGenerator
     */
    private $configurationCodeGenerator;

    /**
     * @var FrontendConfigurationEntityFromStructureConverter
     */
    private $frontendConfigurationEntityFromStructureConverter;

    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var ConfigurationScreenshot
     */
    private $configurationScreenshot;

    /**
     * @var ConfigurationtypeRepository
     */
    private $configurationTypeRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var PrintFileSave
     */
    private $printFileSave;

    /**
     * @param ConfigurationCodeLength $configurationCodeLength
     * @param ConfigurationCodeGenerator $configurationCodeGenerator
     * @param ConfigurationRepository $configurationRepository
     * @param ConfigurationScreenshot $configurationScreenshot
     * @param ConfigurationtypeRepository $configurationTypeRepository
     * @param FrontendConfigurationEntityFromStructureConverter $frontendConfigurationEntityFromStructureConverter
     * @param ItemRepository $itemRepository
     * @param PrintFileSave $printFileSave
     */
    public function __construct(
        ConfigurationCodeLength $configurationCodeLength,
        ConfigurationCodeGenerator $configurationCodeGenerator,
        ConfigurationRepository $configurationRepository,
        ConfigurationScreenshot $configurationScreenshot,
        ConfigurationtypeRepository $configurationTypeRepository,
        FrontendConfigurationEntityFromStructureConverter $frontendConfigurationEntityFromStructureConverter,
        ItemRepository $itemRepository,
        PrintFileSave $printFileSave
    ) {
        $this->configurationCodeLength = $configurationCodeLength;
        $this->configurationCodeGenerator = $configurationCodeGenerator;
        $this->configurationRepository = $configurationRepository;
        $this->configurationScreenshot = $configurationScreenshot;
        $this->configurationTypeRepository = $configurationTypeRepository;
        $this->frontendConfigurationEntityFromStructureConverter = $frontendConfigurationEntityFromStructureConverter;
        $this->itemRepository = $itemRepository;
        $this->printFileSave = $printFileSave;
    }

    /**
     * @param ConfigurationSaveData $configurationSaveData
     * @param CalculationResult|null $calculationResult
     *
     * @return Configuration
     */
    public function save(ConfigurationSaveData $configurationSaveData, CalculationResult $calculationResult = null): Configuration
    {
        // align duplicated configurationTypes to be BC compatible
        $configurationSaveData->configuration->configurationType = $configurationSaveData->configurationType ?? ConfigurationtypeRepository::USER;

        $entity = $this->createEntity($configurationSaveData->configuration, $calculationResult);

        // save screenshots if sent
        $this->configurationScreenshot->save($configurationSaveData->screenshots, $entity->getCode());

        // save designer data (svg)
        $this->configurationScreenshot->saveDesignAreas($configurationSaveData->designDataSnapshots, $entity->getCode());
        $this->printFileSave->savePrintDesignAreas($configurationSaveData->printDesignData, $entity->getCode());

        // save print file(s) if sent
        if (!empty($configurationSaveData->printFiles)) {
            $this->printFileSave->save($configurationSaveData->printFiles, $entity->getCode());
        }

        $this->configurationRepository->save($entity);

        return $entity;
    }

    /**
     * @param FrontendConfigurationStructure $configuration
     * @param CalculationResult|null $calculationResult
     *
     * @return Configuration
     */
    public function createEntity(FrontendConfigurationStructure $configuration, CalculationResult $calculationResult = null): Configuration
    {
        $configurationCode = $configuration->code;
        $configurationTypeIdentifier = $configuration->configurationType;

        // decide if a new configuration should be generated
        if (empty($configurationCode)) {
            $entity = null;
            $configuration->code = $this->configurationCodeGenerator->generateCode($this->configurationCodeLength->getCodeLengthByConfigurationType($configurationTypeIdentifier));
            $configuration->_metadata->changedProperties[] = 'code';
        } else {
            $entity = $this->configurationRepository->findOneByCode($configurationCode);
        }

        // DEFAULT_OPTIONS are always retrieved by item
        $configurationTypeEntity = $this->configurationTypeRepository->findOneByIdentifier($configurationTypeIdentifier);
        if (ConfigurationtypeRepository::DEFAULT_OPTIONS === $configurationTypeIdentifier) {
            $item = $this->itemRepository->findOneByIdentifier($configuration->item->identifier);
            if (null !== $item) {
                $entity = $this->configurationRepository->findOneBy(['configurationtype' => $configurationTypeEntity, 'item' => $item]);
            }
        }

        $entity = $this->frontendConfigurationEntityFromStructureConverter->convertOne($configuration, $entity);

        if (ConfigurationtypeRepository::DEFAULT_OPTIONS !== $configurationTypeIdentifier && null !== $calculationResult) {
            $entity->setCalculationResult(json_encode($calculationResult));
        }

        return $entity;
    }
}
