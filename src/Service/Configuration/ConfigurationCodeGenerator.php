<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Configuration;

use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;

/**
 * @internal
 */
class ConfigurationCodeGenerator
{
    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var int
     */
    private $codeGenerateRecursionLoops = 0;

    /**
     * @var ConfigurationCodeLength
     */
    private $configurationCodeLength;

    /**
     * @param ConfigurationCodeLength $configurationCodeLength
     * @param ConfigurationRepository $configurationRepository
     */
    public function __construct(
        ConfigurationCodeLength $configurationCodeLength,
        ConfigurationRepository $configurationRepository
    ) {
        $this->configurationCodeLength = $configurationCodeLength;
        $this->configurationRepository = $configurationRepository;
    }

    /**
     * @param int $length
     * @param bool $uppercase
     *
     * @return string
     */
    public function generateCode($length = 8, $uppercase = true)
    {
        $this->codeGenerateRecursionLoops = 0;

        $code = $this->generateCodeRecursively($length, $uppercase);

        return $code;
    }

    /**
     * get code length for configuration type, default to 8.
     *
     * @param $configurationType
     *
     * @return int
     */
    public function getConfigurationCodeLengthByConfigurationType($configurationType): int
    {
        return $this->configurationCodeLength->getCodeLengthByConfigurationType($configurationType);
    }

    /**
     * recursion to retry code generation if a non unique code was generated, abort after 5 tries.
     *
     * @param int $length
     * @param bool $uppercase
     *
     * @return string
     */
    protected function generateCodeRecursively($length = 8, $uppercase = true)
    {
        if ($this->codeGenerateRecursionLoops > 5) {
            $violations = [];
            $violations[] = [
                'property' => 'configuration_code',
                'invalidvalue' => '',
                'message' => 'code_generation_failed',
            ];

            throw new ValidationException($violations);
        }

        $chars = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $charLength = strlen($chars);

        $i = 0;
        $code = '';

        while ($i < $length && $i < 100) {
            $randomNumber = rand(1, $charLength) - 1;
            $randomChar = substr($chars, $randomNumber, 1);

            $code .= $randomChar;
            ++$i;
        }

        if (true === $uppercase) {
            $code = strtoupper($code);
        }

        ++$this->codeGenerateRecursionLoops;

        // check for duplicates, enter recursion if duplicate found
        $entityWithCode = $this->configurationRepository->findOneBy(['code' => $code]);

        if (null !== $entityWithCode) {
            $code = $this->generateCodeRecursively($length, $uppercase);
        }

        return $code;
    }
}
