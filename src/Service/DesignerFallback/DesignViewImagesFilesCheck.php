<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemGroupingWalker;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class DesignViewImagesFilesCheck implements DesignerFallbackCheckInterface
{
    /**
     * @var FrontendItemGroupingWalker
     */
    private $frontendItemGroupingWalker;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param FrontendItemGroupingWalker $frontendItemGroupingWalker
     * @param string $mediaBasePath
     */
    public function __construct(
        FrontendItemGroupingWalker $frontendItemGroupingWalker,
        string $mediaBasePath
    ) {
        $this->frontendItemGroupingWalker = $frontendItemGroupingWalker;
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
    }

    /**
     * @param Item $item
     * @param array $viewDesignAreaRelations<ViewRelationInterface>
     *
     * @return bool
     */
    public function isSatisfied(Item $item, array $viewDesignAreaRelations): bool
    {
        $children = $this->frontendItemGroupingWalker->walk($item);

        $path = $this->mediaBasePath . '/images/item/view/' . $item->parentItemIdentifier;

        foreach ($viewDesignAreaRelations as $viewArea) {
            // if the directory of item and/or view does not exist no image can exist
            if (!is_dir($path) || !is_dir($path . '/' . $viewArea->getView()->getIdentifier())) {
                return false;
            }

            if (!empty($children)) {
                foreach ($children as $child) {
                    $finder = new Finder();
                    $imageExists = $finder->files()->in($path . '/' . $viewArea->getView()->getIdentifier())->name($child->identifier . '.*');
                    if (!$imageExists->count()) {
                        return false;
                    }
                }
            } else {
                $finder = new Finder();
                $imageExists = $finder->files()->in($path . '/' . $viewArea->getView()->getIdentifier())->name($item->identifier . '.*');
                if (!$imageExists->count()) {
                    return false;
                }
            }
        }

        return true;
    }
}
