<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use Redhotmagma\ConfiguratorApiBundle\Service\DesignViewDesignArea\BaseShape2dPositionCalculation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;

/**
 * @internal
 */
class Database2DCheck implements DesignerFallbackCheckInterface
{
    public const BASE_SHAPE_MANDATORY_FIELDS = [
        'type' => 'string',
        'x' => 'numeric',
        'y' => 'numeric',
        'width' => 'numeric',
        'height' => 'numeric',
    ];

    /**
     * @var BaseShape2dPositionCalculation
     */
    private $baseShape2dPositionCalculation;

    /**
     * Database2DCheck constructor.
     *
     * @param BaseShape2dPositionCalculation $baseShape2dPositionCalculation
     */
    public function __construct(BaseShape2dPositionCalculation $baseShape2dPositionCalculation)
    {
        $this->baseShape2dPositionCalculation = $baseShape2dPositionCalculation;
    }

    /**
     * @param Item $item
     * @param array $viewDesignAreaRelations<ViewRelationInterface>
     *
     * @return bool
     */
    public function isSatisfied(Item $item, array $viewDesignAreaRelations): bool
    {
        foreach ($viewDesignAreaRelations as $viewArea) {
            $viewArea = $this->baseShape2dPositionCalculation->calculateBaseShapePosition($viewArea);

            $baseShape = $viewArea->getBaseShape();

            if (empty($baseShape)) {
                return false;
            }

            foreach (self::BASE_SHAPE_MANDATORY_FIELDS as $fieldKey => $fieldValue) {
                $isType = 'is_' . $fieldValue;
                if (!isset($baseShape->$fieldKey) || (function_exists($isType) && !$isType($baseShape->$fieldKey))) {
                    return false;
                }
            }
        }

        return true;
    }
}
