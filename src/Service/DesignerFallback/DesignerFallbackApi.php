<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewDesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignViewDesignAreaRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\VisualizationData\VisualizationData3d;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemSettingRelation;

class DesignerFallbackApi
{
    public const FALLBACK_VALUE = 'fallback';

    /**
     * @var Database2DCheck
     */
    private $database2DCheck;

    /**
     * @var Database3DCheck
     */
    private $database3DCheck;

    /**
     * @var DesignViewImagesFilesCheck
     */
    private $designViewImagesFilesCheck;

    /**
     * @var DesignViewDesignAreaRepository
     */
    private $designViewDesignAreaRepository;

    /**
     * @var Setup3DFilesCheck
     */
    private $setup3DFilesCheck;

    /**
     * @var TextureFilesCheck
     */
    private $textureFilesCheck;

    /**
     * @var VisualizationData3d
     */
    private $visualizationData3d;

    /**
     * @var CreatorViewDesignAreaRepository
     */
    private $creatorViewDesignAreaRepository;

    /**
     * @param Database2DCheck $database2DCheck
     * @param Database3DCheck $database3DCheck
     * @param DesignViewImagesFilesCheck $designViewImagesFilesCheck
     * @param DesignViewDesignAreaRepository $designViewDesignAreaRepository
     * @param Setup3DFilesCheck $setup3DFilesCheck
     * @param TextureFilesCheck $textureFilesCheck
     * @param VisualizationData3d $visualizationData3d
     * @param CreatorViewDesignAreaRepository $creatorViewDesignAreaRepository
     */
    public function __construct(
        Database2DCheck $database2DCheck,
        Database3DCheck $database3DCheck,
        DesignViewImagesFilesCheck $designViewImagesFilesCheck,
        DesignViewDesignAreaRepository $designViewDesignAreaRepository,
        Setup3DFilesCheck $setup3DFilesCheck,
        TextureFilesCheck $textureFilesCheck,
        VisualizationData3d $visualizationData3d,
        CreatorViewDesignAreaRepository $creatorViewDesignAreaRepository
    ) {
        $this->database2DCheck = $database2DCheck;
        $this->database3DCheck = $database3DCheck;
        $this->designViewImagesFilesCheck = $designViewImagesFilesCheck;
        $this->designViewDesignAreaRepository = $designViewDesignAreaRepository;
        $this->setup3DFilesCheck = $setup3DFilesCheck;
        $this->textureFilesCheck = $textureFilesCheck;
        $this->visualizationData3d = $visualizationData3d;
        $this->creatorViewDesignAreaRepository = $creatorViewDesignAreaRepository;
    }

    /**
     * @param Item $item
     *
     * @return ItemSettingRelation
     */
    public function getCheckedItemSetting(Item $item): ItemSettingRelation
    {
        $visualizationMode = $item->settings->visualizationDesigner;

        if (empty($visualizationMode)) {
            return $item->settings;
        }

        if (array_key_exists($visualizationMode, VisualizationData3d::SUB_DIR_MAP)
            && $this->isExcludedForChecks($item->identifier, $visualizationMode)) {
            return $item->settings;
        }

        $designAreaViewRelations = array_merge(
            $this->designViewDesignAreaRepository->getByItemIdentifier($item->parentItemIdentifier),
            $designAreaViewRelations = $this->creatorViewDesignAreaRepository->findByItemIdentifier($item->parentItemIdentifier)
        );

        if (VisualizationMode::IDENTIFIER_3D_VARIANT === $visualizationMode) {
            return $item->settings;
        }

        if (VisualizationMode::is2dVariantVisualization($visualizationMode) && $this->isSatisfied2d($item, $designAreaViewRelations)) {
            return $item->settings;
        }

        if (VisualizationMode::IDENTIFIER_2D_LAYER === $visualizationMode && $this->isSatisfiedLayer($item, $designAreaViewRelations)) {
            return $item->settings;
        }

        $item->settings->visualizationDesigner = self::FALLBACK_VALUE;

        return $item->settings;
    }

    /**
     * @param string $itemIdentifier
     * @param string $visualizationMode
     *
     * @return bool
     */
    private function isExcludedForChecks(string $itemIdentifier, string $visualizationMode): bool
    {
        return is_dir($this->visualizationData3d->getDataDir($itemIdentifier, $visualizationMode));
    }

    /**
     * @param Item $item
     * @param array $designAreaDesignViews
     *
     * @return bool
     */
    private function isSatisfied2d(Item $item, array $designAreaDesignViews): bool
    {
        if ([] === $designAreaDesignViews) {
            return false;
        }

        $checks = [
            $this->database2DCheck,
            $this->designViewImagesFilesCheck,
        ];

        foreach ($checks as $check) {
            $isSatisfied = $check->isSatisfied($item, $designAreaDesignViews);
            if (!$isSatisfied) {
                break;
            }
        }

        return $isSatisfied;
    }

    /**
     * @param Item $item
     * @param array $designAreaDesignViews
     *
     * @return bool
     */
    private function isSatisfiedLayer(Item $item, array $designAreaDesignViews): bool
    {
        if ([] === $designAreaDesignViews) {
            return false;
        }

        return $this->database2DCheck->isSatisfied($item, $designAreaDesignViews);
    }
}
