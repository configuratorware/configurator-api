<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class TextureFilesCheck implements DesignerFallbackCheckInterface
{
    public const MATERIALS_FILE_PATH_PATTERN = 'content/3D/{parentItemIdentifier}/materials/{fileIdentifier}/';
    public const MATERIALS_FILE_NAME = 'materials.mtl';

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * TextureFilesCheck constructor.
     *
     * @param Filesystem $fileSystem
     * @param string $mediaBasePath
     */
    public function __construct(Filesystem $fileSystem, string $mediaBasePath)
    {
        $this->fileSystem = $fileSystem;
        $this->mediaBasePath = $mediaBasePath;
    }

    /**
     * @param Item $item
     * @param array $viewDesignAreaRelations<ViewRelationInterface>
     *
     * @return bool
     */
    public function isSatisfied(Item $item, array $viewDesignAreaRelations = []): bool
    {
        $isSatisfied = true;

        if (!empty($item->itemGroup->children)) {
            foreach ($item->itemGroup->children as $child) {
                $isSatisfied = $this->check($item->parentItemIdentifier, $child->identifier);
            }
        } else {
            $isSatisfied = $this->check($item->parentItemIdentifier, $item->identifier);
        }

        return $isSatisfied;
    }

    /**
     * @param string $parentItemIdentifier
     * @param string $fileIdentifier
     *
     * @return bool
     */
    private function check(string $parentItemIdentifier, string $fileIdentifier): bool
    {
        $isSatisfied = false;

        $materialsFile = $this->getMaterialsFile($parentItemIdentifier, $fileIdentifier);

        if ($materialsFile) {
            $textureFiles = $this->parseMaterialsFile($materialsFile);

            foreach ($textureFiles as $texture) {
                $isSatisfied = $this->fileSystem->exists(
                    $this->mediaBasePath . '/' . $this->getMaterialPathPart(
                        $parentItemIdentifier,
                        $fileIdentifier
                    ) . $texture
                );

                if (!$isSatisfied) {
                    break;
                }
            }
        }

        return $isSatisfied;
    }

    /**
     * @param string $parentItemIdentifier
     * @param string $fileIdentifier
     *
     * @return string|null
     */
    private function getMaterialsFile(string $parentItemIdentifier, string $fileIdentifier): ?string
    {
        $file = null;

        $materialsFilePath = $this->mediaBasePath . '/' . $this->getMaterialPathPart(
            $parentItemIdentifier,
            $fileIdentifier
        ) . self::MATERIALS_FILE_NAME;

        if ($this->fileSystem->exists($materialsFilePath)) {
            $file = file_get_contents($materialsFilePath);
        }

        return $file;
    }

    /**
     * @param string $parentItemIdentifier
     * @param string $fileIdentifier
     *
     * @return string
     */
    private function getMaterialPathPart(string $parentItemIdentifier, string $fileIdentifier): string
    {
        $replaceable = [
            '{parentItemIdentifier}' => $parentItemIdentifier,
            '{fileIdentifier}' => $fileIdentifier,
        ];

        $materialPathPart = self::MATERIALS_FILE_PATH_PATTERN;

        foreach ($replaceable as $search => $toReplace) {
            $materialPathPart = str_replace($search, $toReplace, $materialPathPart);
        }

        return $materialPathPart;
    }

    /**
     * @param string $materialsFile
     *
     * @return array
     */
    private function parseMaterialsFile(string $materialsFile): array
    {
        preg_match_all('/map_.+?\s(.+\S)\s/', $materialsFile, $result, PREG_SET_ORDER);

        return array_unique(array_column($result, 1));
    }
}
