<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;

/**
 * @internal
 */
class Database3DCheck implements DesignerFallbackCheckInterface
{
    public const BASE_SHAPE_MANDATORY_FIELDS = ['type' => 'string'];
    public const POSITION_MANDATORY_FIELDS = ['x' => 'int', 'y' => 'int', 'width' => 'int', 'height' => 'int'];

    /**
     * @param Item $item
     * @param array $viewDesignAreaRelations<ViewRelationInterface>
     *
     * @return bool
     */
    public function isSatisfied(Item $item, array $viewDesignAreaRelations): bool
    {
        foreach ($viewDesignAreaRelations as $viewArea) {
            $baseShape = $viewArea->getBaseShape();
            $position = $viewArea->getPosition();

            if (empty($baseShape) || empty($position)) {
                return false;
            }

            foreach (self::BASE_SHAPE_MANDATORY_FIELDS as $fieldKey => $fieldValue) {
                $isType = 'is_' . $fieldValue;
                if (!isset($baseShape->$fieldKey) || (function_exists($isType) && !$isType($baseShape->$fieldKey))) {
                    return false;
                }
            }

            foreach (self::POSITION_MANDATORY_FIELDS as $fieldKey => $fieldValue) {
                $isType = 'is_' . $fieldValue;
                if (!isset($position->$fieldKey) || (function_exists($isType) && !$isType($position->$fieldKey))) {
                    return false;
                }
            }
        }

        return true;
    }
}
