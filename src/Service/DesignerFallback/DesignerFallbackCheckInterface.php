<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use Redhotmagma\ConfiguratorApiBundle\Entity\ViewRelationInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;

interface DesignerFallbackCheckInterface
{
    /**
     * @param Item $item
     * @param array $viewDesignAreaRelations<ViewRelationInterface>
     *
     * @return bool
     */
    public function isSatisfied(Item $item, array $viewDesignAreaRelations): bool;
}
