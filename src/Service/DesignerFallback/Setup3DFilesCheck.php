<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerFallback;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class Setup3DFilesCheck implements DesignerFallbackCheckInterface
{
    public const MODEL_FILE_NAME = 'model.obj';
    public const CAMERA_VIEWS_FILE_NAME = 'cameraviews.dae';

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var string
     */
    private $mediaBasePath;

    /**
     * @param Filesystem $fileSystem
     * @param string $mediaBasePath
     */
    public function __construct(
        Filesystem $fileSystem,
        string $mediaBasePath
    ) {
        $this->fileSystem = $fileSystem;
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
    }

    /**
     * @param Item $item
     * @param array $viewDesignAreaRelations<ViewRelationInterface>
     *
     * @return bool
     */
    public function isSatisfied(Item $item, array $viewDesignAreaRelations = []): bool
    {
        $files = [
            $this->mediaBasePath . '/content/3D/' . $item->parentItemIdentifier . '/' . self::MODEL_FILE_NAME,
            $this->mediaBasePath . '/content/3D/' . $item->parentItemIdentifier . '/' . self::CAMERA_VIEWS_FILE_NAME,
        ];

        $isSatisfied = $this->fileSystem->exists($files);

        return $isSatisfied;
    }
}
