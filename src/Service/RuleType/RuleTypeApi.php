<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\RuleType;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Repository\RuletypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RuleType;

class RuleTypeApi
{
    /**
     * @var RuletypeRepository
     */
    private $ruleTypeRepository;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * RuleTypeApi constructor.
     *
     * @param RuletypeRepository $ruleTypeRepository
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        RuletypeRepository $ruleTypeRepository,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->ruleTypeRepository = $ruleTypeRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->ruleTypeRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->structureFromEntityConverter->convertMany($entities, RuleType::class);

        $paginationResult->count = $this->ruleTypeRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return [];
    }
}
