<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use DateTime;
use Doctrine\DBAL\ConnectionException;
use Exception;
use InvalidArgumentException;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;

class ImportApi
{
    /**
     * @var ImporterFileReader
     */
    protected $importerFileReader;

    /**
     * @var ImportLegacyTransformer
     */
    private $importLegacyTransformer;

    /**
     * @var ImportWorker
     */
    protected $importWorker;

    /**
     * @var ImportXmlConverter
     */
    protected $importXmlConverter;

    /**
     * @var ImportFileStorage
     */
    protected $importFileStorage;

    /**
     * ImportController constructor.
     *
     * @param ImporterFileReader $importerFileReader
     * @param ImportLegacyTransformer $importLegacyTransformer
     * @param ImportWorker $importWorker
     * @param ImportXmlConverter $importXmlConverter
     * @param ImportFileStorage $importFileStorage
     */
    public function __construct(
        ImporterFileReader $importerFileReader,
        ImportLegacyTransformer $importLegacyTransformer,
        ImportWorker $importWorker,
        ImportXmlConverter $importXmlConverter,
        ImportFileStorage $importFileStorage
    ) {
        $this->importerFileReader = $importerFileReader;
        $this->importLegacyTransformer = $importLegacyTransformer;
        $this->importWorker = $importWorker;
        $this->importXmlConverter = $importXmlConverter;
        $this->importFileStorage = $importFileStorage;
    }

    /**
     * @param string $source
     *
     * @return string
     */
    public function readFromSource(string $source, ImportOptions $importOptions)
    {
        $content = $this->importerFileReader->readSource($source, $importOptions);

        return $content;
    }

    /**
     * @param string $raw
     *
     * @return array
     */
    public function prepareImportArray(string $raw): array
    {
        if ($this->importerFileReader->isJson($raw)) {
            $importArray = json_decode($raw, true);
        } elseif ($this->importerFileReader->isXML($raw)) {
            $importArray = $this->importXmlConverter->convert($raw);
        } else {
            throw new InvalidArgumentException('Wrong Input format. Supported: Json or XML');
        }

        if (!is_array($importArray)) {
            throw new InvalidArgumentException('Input could not be prepared for processing.');
        }

        return $importArray;
    }

    /**
     * @param ImportOptions $options
     * @param array $importArray
     *
     * @return bool
     *
     * @throws ConnectionException|\Doctrine\DBAL\Exception
     */
    public function executeImport(ImportOptions $options, array $importArray): bool
    {
        return $this->importWorker->import($options, $this->importLegacyTransformer->transform($importArray));
    }

    /**
     * @param DateTime|null $beforeDate
     *
     * @throws Exception
     */
    public function removeOldImportFiles(?DateTime $beforeDate = null): void
    {
        $this->importFileStorage->removeOldImportFiles($beforeDate);
    }
}
