<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Exception as DBALException;
use Exception;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Logger\SqlConsoleLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\AttributeHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DefaultConfigurationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DeleteHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\DesignHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ImportHelper;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\ItemHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\MediaHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers\OptionClassificationHandler;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 *
 * @internal
 */
class ImportWorker
{
    private Connection $connection;

    private ImportValidator $validator;

    private LoggerInterface $logger;

    private MediaHandler $mediaHandler;

    private DeleteHandler $deleteHandler;

    private ItemHandler $itemHandler;

    private AttributeHandler $attributeHandler;

    private DesignHandler $designHandler;

    private OptionClassificationHandler $componentHandler;

    private DefaultConfigurationHandler $defaultConfigurationHandler;

    private ItemClassificationHandler $itemClassificationHandler;

    private array $itemDetailImageFoundCollection = [];

    private array $itemThumbnailFoundCollection = [];

    private ImportHelper $importHelper;

    public function __construct(
        Connection $connection,
        AttributeHandler $attributeHandler,
        OptionClassificationHandler $componentHandler,
        DefaultConfigurationHandler $defaultConfigurationHandler,
        DeleteHandler $deleteHandler,
        DesignHandler $designHandler,
        ImportLogger $logger,
        MediaHandler $mediaHandler,
        ImportHelper $importHelper,
        ItemHandler $itemHandler,
        ImportValidator $validator,
        ItemClassificationHandler $itemClassificationHandler
    ) {
        $this->connection = $connection;
        $this->attributeHandler = $attributeHandler;
        $this->componentHandler = $componentHandler;
        $this->defaultConfigurationHandler = $defaultConfigurationHandler;
        $this->deleteHandler = $deleteHandler;
        $this->designHandler = $designHandler;
        $this->logger = $logger;
        $this->mediaHandler = $mediaHandler;
        $this->importHelper = $importHelper;
        $this->itemHandler = $itemHandler;
        $this->validator = $validator;
        $this->itemClassificationHandler = $itemClassificationHandler;
    }

    /**
     * Do the actual import.
     *
     * @param ImportOptions $importOptions
     * @param array $data
     *
     * @return bool
     *
     * @throws ConnectionException
     * @throws \Doctrine\DBAL\Exception
     */
    public function import(ImportOptions $importOptions, array $data): bool
    {
        if (!empty($importOptions->priceChannel)) {
            $priceChannelId = $this->importHelper->getChannelByIdentifier($importOptions->priceChannel);
            if (0 === $priceChannelId) {
                throw new InvalidArgumentException('PriceChannel parameter "' . $importOptions->priceChannel . '" does not exist!');
            }
        }

        if (true === $importOptions->dryRun || true === $importOptions->logSql) {
            $sqlLogger = new SqlConsoleLogger($this->connection);
            $this->connection->getConfiguration()->setSQLLogger($sqlLogger);
        }

        $this->connection->beginTransaction();

        $startDate = new DateTime();
        $itemIdentifiers = array_map(static fn (array $dataSet) => $dataSet['itemIdentifier'], $data);

        try {
            $this->doImports($data, $importOptions);
            $this->doDeletes($startDate, $importOptions, $itemIdentifiers);
        } catch (Exception $e) {
            // error
            $this->logger->error('Import failed: ', ['message' => $e->getMessage(), 'trace' => $e->getTrace()]);
            $this->connection->rollBack();

            return false;
        }

        // success
        if (false === $importOptions->dryRun) {
            $this->connection->commit();

            return true;
        }

        // dry run
        $this->connection->rollBack();
        if (true === $importOptions->logSql) {
            $sqlLogger->printSummary();
        }

        return true;
    }

    /**
     * Do the import & insert jobs.
     *
     * @param array $data
     * @param ImportOptions $importOptions
     *
     * @return bool
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function doImports(array $data, ImportOptions $importOptions): bool
    {
        if (!is_array($data)) {
            $this->logger->error('Import data is not an array!');

            return false;
        }

        // if should not overwrite all images, collect images already present, to later skip them
        if (!$importOptions->overwriteItemImages) {
            $this->itemDetailImageFoundCollection = $this->mediaHandler->collectItemIdentifiersForPresentDetailImages();
            $this->itemThumbnailFoundCollection = $this->mediaHandler->collectItemIdentifiersForPresentItemThumbnails();
        }
        foreach ($data as $itemData) {
            if (!is_array($itemData)) {
                $this->logger->error('Item is not an array!');

                continue;
            }
            if ($this->validator->validateItem($itemData)) {
                if (!isset($itemData['configurationMode'])) {
                    $itemData['configurationMode'] = $this->getConfigurationMode($itemData['itemIdentifier']);
                }
                $itemId = $this->itemHandler->processItem($itemData);
                $this->processItemProperties($itemData, $itemId, $itemData['configurationMode'], $importOptions);
                if (isset($itemData['children']) && is_array($itemData['children'])) {
                    foreach ($itemData['children'] as $child) {
                        $childId = $this->itemHandler->processItem($child, $itemId);
                        $this->processItemProperties($child, $childId, $itemData['configurationMode'], $importOptions);
                    }
                }
            } else {
                $this->logger->error('Item is invalid!', $itemData);
            }
        }

        return true;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return string|null
     */
    private function getConfigurationMode(string $itemIdentifier): ?string
    {
        $item = $this->itemHandler->findItem($itemIdentifier);

        if (null !== $item && isset($item['configuration_mode'])) {
            return $item['configuration_mode'];
        }

        $this->logger->error('Item has no configuration mode!', [$itemIdentifier]);

        return null;
    }

    /**
     * Process an item's properties.
     *
     * @param array $itemData
     * @param int $itemId
     * @param string|null $configurationMode
     * @param ImportOptions $importOptions
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private function processItemProperties(array $itemData, int $itemId, ?string $configurationMode, ImportOptions $importOptions)
    {
        $itemIdentifier = (string)$itemData['itemIdentifier'];

        if (!is_array($itemData['texts'])) {
            $this->logger->notice('"texts" is not an array, no data to import!');
        } else {
            foreach ($itemData['texts'] as $text) {
                $this->itemHandler->processItemText($text, $itemId);
            }
        }

        if (!is_array($itemData['prices'])) {
            $this->logger->notice('"prices" is not an array, no data to import!');
        } else {
            foreach ($itemData['prices'] as $price) {
                // if import is run with "priceChannel" parameter, skip if this isn't the right channel
                if (!empty($importOptions->priceChannel) && $importOptions->priceChannel !== $price['channelIdentifier']) {
                    continue;
                }

                $this->itemHandler->processItemPrice($price, $itemId);
            }
        }

        if (!is_array($itemData['attributes'])) {
            $this->logger->notice('"attributes" is not an array, no data to import!');
        } else {
            foreach ($itemData['attributes'] as $attribute) {
                $this->attributeHandler->processItemAttribute($attribute, $itemId);
            }
        }
        if (isset($itemData['itemClassifications']) && !empty($itemData['itemClassifications'])) {
            foreach ($itemData['itemClassifications'] as $itemClassification) {
                $this->itemClassificationHandler->processItemClassification($itemClassification, $itemId);
            }
        }

        $detailImageExists = false;
        $thumbnailImageExists = false;
        if (!empty($itemData['images'])) {
            // check item images
            if (isset($itemData['images']['detailImage']) && ($importOptions->overwriteItemImages || !in_array($itemIdentifier, $this->itemDetailImageFoundCollection))) {
                $detailImageExists = $this->mediaHandler->uploadItemDetailImage($itemIdentifier, $itemData['images']['detailImage']);
            }

            // check item thumbnails
            if (isset($itemData['images']['thumbnail']) && ($importOptions->overwriteItemImages || !in_array($itemIdentifier, $this->itemThumbnailFoundCollection))) {
                $thumbnailImageExists = $this->mediaHandler->uploadItemThumbnail($itemIdentifier, $itemData['images']['thumbnail']);
            }
        }

        if ($importOptions->deleteNonExistingItemImages) {
            if (!$detailImageExists) {
                $this->mediaHandler->deleteItemDetailImage($itemIdentifier);
            }
            if (!$thumbnailImageExists) {
                $this->mediaHandler->deleteItemThumbnailImage($itemIdentifier);
            }
        }

        $createdViewThumbnails = [];
        if (isset($itemData['designAreas']) &&
            !empty($itemData['designAreas']) &&
            is_array($itemData['designAreas'])
        ) {
            foreach ($itemData['designAreas'] as $designArea) {
                $areaId = $this->designHandler->processDesignArea($designArea, $itemId);

                if (!$areaId) {
                    continue;
                }

                if (!is_array($designArea['texts'])) {
                    $this->logger->notice('Design area "texts" is not an array, no data to import!');
                } else {
                    foreach ($designArea['texts'] as $text) {
                        $this->designHandler->processDesignAreaText($text, $areaId);
                    }
                }

                if (!is_array($designArea['visualData'])) {
                    $this->logger->notice('Design area "visualData" is not an array, no data to import!');
                } elseif ('creator+designer' === $configurationMode) {
                    foreach ($designArea['visualData'] as $visualData) {
                        $this->designHandler->processDesignAreaCreatorViewData($visualData, $areaId, $itemId);
                    }
                } else {
                    foreach ($designArea['visualData'] as $visualData) {
                        $viewId = $this->designHandler->processDesignAreaViewData($visualData, $itemId);

                        $created = $this->mediaHandler->uploadViewThumbnails(
                            $itemIdentifier,
                            (string)$visualData['designViewIdentifier'],
                            $visualData['viewThumbnails'] ?? []
                        );

                        $createdViewThumbnails = array_merge($createdViewThumbnails, $created);

                        $this->mediaHandler->uploadVisualDataImage(
                            $itemIdentifier,
                            (string)$visualData['designViewIdentifier'],
                            $visualData['mediaURLs'] ?? []
                        );

                        foreach ($visualData['texts'] as $text) {
                            $this->designHandler->processDesignViewText($text, $viewId);
                        }

                        $this->designHandler->processDesignViewDesignArea($visualData, $areaId, $viewId);
                    }
                }

                if (!is_array($designArea['designProductionMethods'])) {
                    $this->logger
                        ->notice('Design area "designProductionMethods" is not an array, no data to import!');
                } else {
                    foreach ($designArea['designProductionMethods'] as $productionMethod) {
                        $this->designHandler->processDesignProductionMethods($productionMethod, $areaId, $itemId, $importOptions->priceChannel);
                    }
                }

                // add visualization background colors to DesignAreaDesignProductionMethods
                $this->designHandler->processVisualizationBackgroundColors($areaId, $designArea);
            }
        }

        if (array_key_exists('designAreas', $itemData) && $importOptions->deleteNonExistingDesignViewThumbnails) {
            $this->mediaHandler->deleteViewThumbnails($itemIdentifier, $createdViewThumbnails);
        }

        if (isset($itemData['components']) && is_array($itemData['components'])) {
            foreach ($itemData['components'] as $component) {
                $this->componentHandler->processItemOptionClassification($component, $itemId, $importOptions);
            }
            $this->defaultConfigurationHandler->processDefaultConfigurationData($itemData['components'], $itemId);
        }
    }

    /**
     * Do the delete jobs.
     *
     * @param DateTime $startDate
     * @param ImportOptions $importOptions
     * @param string [] $itemIdentifiersToImport
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function doDeletes(DateTime $startDate, ImportOptions $importOptions, array $itemIdentifiersToImport)
    {
        if ($importOptions->deleteNonExistingItems) {
            $this->deleteHandler->deleteNonExistingItems($startDate, $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingItemPrices) {
            $this->deleteHandler->deleteNonExistingItemPrices($startDate, $importOptions->priceChannel, $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingAttributeRelations) {
            $this->deleteHandler->deleteNonExistingAttributeRelations($startDate,
                $importOptions->deleteWriteProtectedAttributes,
                $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingItemClassificationRelations) {
            $this->deleteHandler->deleteNonExistingItemClassificationRelations($startDate, $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingDesignAreas) {
            $this->deleteHandler->deleteNonExistingDesignAreas($startDate, $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingDesignAreaVisualData) {
            $this->deleteHandler->deleteNonExistingDesignAreaVisualData($startDate, $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingDesignAreaDesignProductionMethodPrices) {
            $this->deleteHandler->deleteNonExistingDesignAreaDesignProductionMethodPrices($startDate,
                $importOptions->priceChannel, $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingDesignAreaDesignProductionMethod) {
            $this->deleteHandler->deleteNonExistingDesignAreaDesignProductionMethod($startDate, $itemIdentifiersToImport);
        }
        if ($importOptions->deleteNonExistingComponentRelations) {
            // delete components and options together
            $this->deleteHandler->deleteNonExistingComponentRelations($startDate, $itemIdentifiersToImport);

            // delete options only
            $this->deleteHandler->deleteNonExistingOptionRelations($startDate, $itemIdentifiersToImport);

            // delete option delta prices only
            $this->deleteHandler->deleteNonExistingOptionDeltaPrices($startDate, $itemIdentifiersToImport);
        }
    }
}
