<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * @internal
 */
class ImporterFileReader
{
    /**
     * @var bool
     */
    private $isDir;

    /**
     * @var bool
     */
    private $isUrl;

    /**
     * @var bool
     */
    private $isFile;

    /**
     * @var ImportFileStorage
     */
    private $importFileStorage;

    /**
     * ImporterFileReader constructor.
     *
     * @param ImportFileStorage $importFileStorage
     */
    public function __construct(ImportFileStorage $importFileStorage)
    {
        $this->importFileStorage = $importFileStorage;
    }

    /**
     * Reads a file from a source, returns false if no file found.
     *
     * @param string $path
     *
     * @return string
     */
    public function readSource(string $path, ImportOptions $importOptions)
    {
        $content = null;

        // handel URLS
        if (filter_var($path, FILTER_VALIDATE_URL)) {
            $this->setIsUrl(true);
            $content = file_get_contents($path);
            if ($importOptions->moveProcessedFile) {
                $this->importFileStorage->saveContentToLogDir($content);
            }
        }

        // handel directories
        if (is_dir($path)) {
            $this->setIsDir(true);
            $file = $this->getSingleFileFromDir($path);
            if ($file instanceof SplFileInfo) {
                $filePath = $file->getPath() . '/' . $file->getBasename();
                $content = file_get_contents($filePath);
                if ($importOptions->moveProcessedFile) {
                    $this->importFileStorage->moveFileToLogDir($filePath);
                }
            }
        }

        // handle single files
        if (file_exists($path) && is_file($path)) {
            $this->setIsFile(true);
            $content = file_get_contents($path);
        }

        if (null === $content) {
            throw new \InvalidArgumentException('The path is not valid!');
        }

        return $content;
    }

    /**
     * Read one file from a directory.
     *
     * @param string $path
     *
     * @return \SplFileInfo|false
     */
    protected function getSingleFileFromDir(string $path): ?\SplFileInfo
    {
        $finder = new Finder();
        $finder->files()
            ->name('/.+\.(xml|json)/i')
            ->sortByModifiedTime()
            ->ignoreUnreadableDirs();

        return $finder->in($path)->getIterator()->current();
    }

    /**
     * Is this a valid JSON?
     *
     * @param string $content
     *
     * @return bool
     */
    public function isJson(string $content)
    {
        return null !== json_decode($content);
    }

    /**
     * Is this a valid XML?
     *
     * @param string $content
     *
     * @return bool
     */
    public function isXML(string $content)
    {
        libxml_use_internal_errors(true);
        simplexml_load_string(trim($content));
        $result = 0 === count(libxml_get_errors());
        libxml_clear_errors(); // clear errors or they will cumulate

        return $result;
    }

    /**
     * @return bool
     */
    public function isDir(): bool
    {
        return (bool) $this->isDir;
    }

    /**
     * @param bool $isDir
     */
    protected function setIsDir(bool $isDir): void
    {
        $this->isDir = $isDir;
    }

    /**
     * @return bool
     */
    public function isUrl(): bool
    {
        return (bool) $this->isUrl;
    }

    /**
     * @param bool $isUrl
     */
    protected function setIsUrl(bool $isUrl): void
    {
        $this->isUrl = $isUrl;
    }

    /**
     * @return bool
     */
    public function isFile(): bool
    {
        return (bool) $this->isFile;
    }

    /**
     * @param bool $isFile
     */
    protected function setIsFile(bool $isFile): void
    {
        $this->isFile = $isFile;
    }
}
