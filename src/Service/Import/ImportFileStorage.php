<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class ImportFileStorage
{
    /**
     * @var string
     */
    private $importLogPath;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * ImportFileStorage to handle storing of imported files to a log dir and removing old import files from there.
     *
     * @param string     $importLogPath
     * @param Filesystem $filesystem
     */
    public function __construct(
        string $importLogPath,
        Filesystem $filesystem
    ) {
        $this->importLogPath = rtrim($importLogPath, '/');
        $this->filesystem = $filesystem;
    }

    /**
     * Remove files from importlog directory
     * By default, remove files older than 2 weeks.
     *
     * @param \DateTime|null $beforeDate
     *
     * @throws \Exception
     */
    public function removeOldImportFiles(?\DateTime $beforeDate = null)
    {
        if (null === $beforeDate) {
            $beforeDate = new \DateTime('-2 weeks');
        }

        if (!$this->filesystem->exists($this->importLogPath)) {
            return;
        }

        $finder = new Finder();
        $finder->in($this->importLogPath)->depth('== 0')->files()->date('<= ' . $beforeDate->format('c'));
        foreach ($finder as $file) {
            $this->filesystem->remove($file->getPath() . '/' . $file->getFilename());
        }
    }

    /**
     * Move a file to log dir.
     *
     * @param string $filePath
     */
    public function moveFileToLogDir(string $filePath)
    {
        if (!$this->filesystem->exists($filePath)) {
            return;
        }

        $fileInfo = new \SplFileInfo($filePath);
        $targetFileName = $fileInfo->getBasename();

        // check if file with same name already exists
        $iteration = 0;
        while ($this->filesystem->exists($this->importLogPath . '/' . $targetFileName) && $iteration < 10) {
            $targetFileName = $fileInfo->getBasename('.' . $fileInfo->getExtension())
                . '_' . ++$iteration . '.' . $fileInfo->getExtension();
        }

        $this->filesystem->copy($filePath, $this->importLogPath . '/' . $targetFileName);
        $this->filesystem->remove($filePath);
    }

    /**
     * Save a string content to log dir.
     *
     * @param string $content
     */
    public function saveContentToLogDir(string $content)
    {
        $dateFormat = 'YmdHis';

        $filename = 'url_import_' . date($dateFormat) . '.log';
        $filePath = $this->importLogPath . '/' . $filename;

        // check if file with same name already exists
        $iteration = 0;
        while ($this->filesystem->exists($filePath) && $iteration < 10) {
            $filename = 'url_import_' . date($dateFormat) . '_' . ++$iteration . '.log';
            $filePath = $this->importLogPath . '/' . $filename;
        }

        $this->filesystem->dumpFile($filePath, $content);
    }
}
