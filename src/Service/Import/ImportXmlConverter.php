<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

/**
 * @internal
 */
class ImportXmlConverter
{
    /**
     * @var string
     */
    protected $sourceString;

    /**
     * Array map for primitive types conversion.
     *
     * @var array
     */
    protected static $primitivesMap = [
        'true' => true,
        'false' => false,
        'null' => null,
    ];

    /**
     * Convert an XML import document to array.
     *
     * @param string $xmlString
     *
     * @return array
     */
    public function convert(string $xmlString): array
    {
        $xmlItem = simplexml_load_string($xmlString);
        $result = [];
        $this->parseRecursive($xmlItem, $result);
        if (empty($result['item'])) {
            throw new \InvalidArgumentException('main Item element is missing in converted XML');
        }

        return $result['item'];
    }

    /**
     * Parse an XML import  recursively
     * <ol>
     *  <li>walks XML structure recursively, searching for array structure nodes or value nodes</li>
     *  <li>converts array structure nodes containing multiple items to array, e.g. pirces->price[] to prices[]<li>
     *  <li>converts value nodes to they intended type, e.g. (string) "true" => (bool) true<li>
     * </ol>.
     *
     * @param \SimpleXMLElement $xmlItem
     * @param array|mixed       $target
     *
     * @return void
     */
    protected function parseRecursive(\SimpleXMLElement &$xmlItem, &$target)
    {
        if (0 != $xmlItem->children()->count()) { // parse children, if any
            foreach ($xmlItem->children() as $child) { /* @var $child \SimpleXMLElement */
                $key = $child->getName();

                // handling keys fol plural field names
                $subKey1 = substr($key, 0, -1);
                $subKey2 = substr($key, 0, -2);

                if ($key != $subKey1 && isset($child->{$subKey1})) { // e.g. prices -> price[]
                    foreach ($child->{$subKey1} as $subChild) {
                        $this->parseChildrenAsArray($key, $subChild, $target);
                    }
                } elseif ($key != $subKey2 && isset($child->{$subKey2})) { // e.g. statuses -> status[]
                    foreach ($child->{$subKey2} as $subChild) {
                        $this->parseChildrenAsArray($key, $subChild, $target);
                    }
                } elseif ('visualData' == $key && isset($child->designView)) { // visualData -> designView[]
                    foreach ($child->designView as $subChild) {
                        $this->parseChildrenAsArray($key, $subChild, $target);
                    }
                } elseif ('children' == $key && $child->item) { // children -> item[]
                    foreach ($child->item as $subChild) {
                        $this->parseChildrenAsArray($key, $subChild, $target);
                    }
                } else { // simple value nodes
                    $newTargetArray = [];
                    $this->parseRecursive($child, $newTargetArray);
                    if ('item' == $key) { // exception for the very first item
                        $target[$key][] = $newTargetArray;
                    } else {
                        $target[$key] = $newTargetArray;
                    }
                }
            }
        } else { // if no children, this is a value node
            $value = (string) $xmlItem; // casting SimpleXMLElement to string with __toString()

            /* converting value nodes to primitive values */
            if (false != preg_match('/^[0-9\.]+$/', $value)) {
                if ($value == (int) $value) {
                    $value = (int) $value;
                } elseif ($value == (float) $value) {
                    $value = (float) $value;
                }
            } elseif (isset(self::$primitivesMap[strtolower($value)])) {
                // convert strings of "true", "false", "null" to true, false, null
                $value = self::$primitivesMap[strtolower($value)];
            }

            if ('customData' == $xmlItem->getname()) {
                if (!$value) {
                    $value = [];
                }
                $value = json_encode($value);
            }

            $target = $value; // set the final value
        }
    }

    /**
     * Parse children items as array.
     *
     * @param string            $key
     * @param \SimpleXMLElement $subChild
     * @param array             $targetArray
     *
     * @return void
     */
    protected function parseChildrenAsArray(string $key, \SimpleXMLElement &$subChild, array &$targetArray)
    {
        $newTargetArray = [];
        $this->parseRecursive($subChild, $newTargetArray);
        $targetArray[$key][] = $newTargetArray;
    }
}
