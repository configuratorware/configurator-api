<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;

/**
 * @internal
 */
final class ImportLegacyTransformer
{
    private const CONFIGURATION_CODE_MAP = [
        'creator' => 'creator',
        'designer' => 'designer',
        'creatordesigner' => 'creator+designer',
        'designercreator' => 'creator+designer',
    ];

    /**
     * @var ItemStatusRepository
     */
    private $itemStatusRepository;

    /**
     * @param ItemStatusRepository $itemStatusRepository
     */
    public function __construct(ItemStatusRepository $itemStatusRepository)
    {
        $this->itemStatusRepository = $itemStatusRepository;
    }

    /**
     * @param array $importArray
     *
     * @return array
     */
    public function transform(array $importArray): array
    {
        foreach ($importArray as $parentKey => $item) {
            if (!is_array($item)) {
                continue;
            }

            if (isset($item['itemStatuses'])) {
                if (!isset($item['configurationMode'])) {
                    $importArray[$parentKey]['configurationMode'] = $this->getConfigurationMode($item['itemStatuses']);
                }
                if (!isset($item['itemStatusIdentifier'])) {
                    $importArray[$parentKey]['itemStatusIdentifier'] = $this->getItemStatusIdentifier($item['itemStatuses']);
                }
                unset($importArray[$parentKey]['itemStatuses']);
            }

            if (array_key_exists('images', $item) && is_array($item['images'])) {
                $this->modifyImages($importArray[$parentKey]['images']);
            }

            foreach ($item['children'] ?? [] as $childKey => $child) {
                if (array_key_exists('images', $child) && is_array($child['images'])) {
                    $this->modifyImages($importArray[$parentKey]['children'][$childKey]['images']);
                }
            }

            if (array_key_exists('components', $item) && is_array($item['components'])) {
                foreach ($item['components'] as $componentKey => $component) {
                    if (array_key_exists('options', $component) && is_array($component['options'])) {
                        foreach ($component['options'] as $optionKey => $option) {
                            if (array_key_exists('prices', $option) && is_array($option['prices'])) {
                                $this->modifyOptionPrices($importArray[$parentKey]['components'][$componentKey]['options'][$optionKey]['prices']);
                            }
                        }
                    }
                }
            }
        }

        return $importArray;
    }

    /**
     * @param array $itemStatuses
     *
     * @return string|null
     */
    private function getConfigurationMode(array $itemStatuses): ?string
    {
        $collectedModes = null;

        foreach ($itemStatuses as $itemStatus) {
            if (isset($itemStatus['available']) && true === $itemStatus['available']) {
                $collectedModes .= $itemStatus['configurationModeIdentifier'] ?? '';
            }
        }

        return self::CONFIGURATION_CODE_MAP[$collectedModes] ?? null;
    }

    /**
     * @param array $itemStatuses
     *
     * @return string|null
     */
    private function getItemStatusIdentifier(array $itemStatuses): ?string
    {
        $availableStatuses = $this->itemStatusRepository->findBy(['item_available' => true]);
        $availableStatusIdentifier = isset($availableStatuses[0]) ? $availableStatuses[0]->getIdentifier() : ItemStatus::DEFAULT_IDENTIFIER;
        $identifier = ItemStatus::DEFAULT_IDENTIFIER;

        foreach ($itemStatuses as $itemStatus) {
            if (isset($itemStatus['available']) && true === $itemStatus['available']) {
                $identifier = $availableStatusIdentifier;
            }
        }

        return $identifier;
    }

    /**
     * @param array $images
     */
    private function modifyImages(array &$images): void
    {
        if (!array_key_exists('detailImage', $images) && array_key_exists('itemImage', $images)) {
            $images['detailImage'] = $images['itemImage'];
            unset($images['itemImage']);
        }
    }

    private function modifyOptionPrices(array &$optionPrices): void
    {
        foreach ($optionPrices as $optionPriceKey => $optionPrice) {
            if (!array_key_exists('bulkPrices', $optionPrice) && array_key_exists('prices', $optionPrice)) {
                $optionPrices[$optionPriceKey]['bulkPrices'] = $optionPrice['prices'];
                unset($optionPrices[$optionPriceKey]['prices']);
            }
        }
    }
}
