<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Models;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 *
 * @internal
 */
class ImportOptions
{
    public bool $deleteNonExistingItems = false;

    public bool $deleteNonExistingAttributeRelations = true;

    public bool $deleteWriteProtectedAttributes = false;

    public bool $deleteNonExistingItemClassificationRelations = true;

    public bool $deleteNonExistingItemPrices = true;

    public bool $deleteNonExistingDesignAreas = true;

    public bool $deleteNonExistingDesignAreaVisualData = false;

    public bool $deleteNonExistingDesignAreaDesignProductionMethodPrices = true;

    public bool $deleteNonExistingDesignAreaDesignProductionMethod = true;

    public bool $deleteNonExistingDesignViewThumbnails = false;

    public bool $deleteNonExistingItemImages = false;

    public bool $deleteNonExistingComponentThumbnails = false;

    public bool $deleteNonExistingOptionThumbnails = false;

    public bool $deleteNonExistingComponentRelations = false;

    public string $priceChannel = '';

    /**
     * Dry run import, rollback at the end.
     */
    public bool $dryRun = false;

    /**
     * Log SQL queries while running.
     */
    public bool $logSql = false;

    /**
     * Move a processed file to log dir.
     */
    public bool $moveProcessedFile = true;

    /**
     * Overwrite item images, if they are already present.
     */
    public bool $overwriteItemImages = false;
}
