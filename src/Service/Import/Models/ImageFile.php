<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Models;

/**
 * @internal
 */
class ImageFile
{
    /**
     * ImportImageFile constructor.
     *
     * @param string $imageFileContent
     * @param string $fileExtension
     */
    public function __construct(string $imageFileContent, string $fileExtension)
    {
        $this->content = $imageFileContent;
        $this->extension = $fileExtension;
    }

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $extension;
}
