<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Models;

class ImageUrl
{
    public ?string $imageUrl;

    private function __construct(?string $imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    public static function fromString(string $imageUrl): self
    {
        $imageUrl = urldecode(trim($imageUrl));
        if (empty($imageUrl)) {
            return new self(null);
        }

        return new self($imageUrl);
    }
}
