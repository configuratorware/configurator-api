<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Models;

/**
 * @internal
 */
class ErrorLocation
{
    /**
     * @var string
     */
    private $errorLogIdentifierName;

    /**
     * @var string
     */
    private $errorLogIdentifierValue;

    /**
     * ErrorReport constructor.
     *
     * @param string $errorLogIdentifierName
     * @param string $errorLogIdentifierValue
     */
    private function __construct(string $errorLogIdentifierName, string $errorLogIdentifierValue)
    {
        $this->errorLogIdentifierName = $errorLogIdentifierName;
        $this->errorLogIdentifierValue = $errorLogIdentifierValue;
    }

    /**
     * @param string $errorLogIdentifierName
     * @param string $errorLogIdentifierValue
     *
     * @return ErrorLocation
     */
    public static function create(string $errorLogIdentifierName, string $errorLogIdentifierValue): ErrorLocation
    {
        return new ErrorLocation($errorLogIdentifierName, $errorLogIdentifierValue);
    }

    /**
     * @return array|string[]
     */
    public function getErrorLocation(): array
    {
        return [$this->errorLogIdentifierName => $this->errorLogIdentifierValue];
    }
}
