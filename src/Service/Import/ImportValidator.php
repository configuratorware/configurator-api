<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import;

use Psr\Log\LoggerInterface;

/**
 * @internal
 */
class ImportValidator
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(ImportLogger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Check required fields and if they contain data.
     *
     * @param array $item Import array item
     * @param bool $isChild Is this a child item
     *
     * @return bool
     */
    public function validateItem(array $item, bool $isChild = false): bool
    {
        if (!is_array($item)) {
            $this->log('Item is not an array', $item);

            return true;
        }

        if (!isset($item['itemIdentifier'])) {
            $this->log('Item identifier is missing', $item);

            return false;
        }

        if ($isChild && isset($item['children']) && is_countable($item['children']) && count($item['children']) > 0) {
            $this->log('The child item should not have children!', $item);

            return false;
        }

        if (!empty($item['texts'])) {
            if (!is_array($item['texts'])) {
                $this->log('Item texts field is not an array!', $item);

                return false;
            }
            foreach ($item['texts'] as $text) {
                if (!is_array($text)) {
                    $this->log('One of item\'s texts is not an array!', $item);

                    return false;
                }
                if (!$this->validateText($text, $item)) {
                    return false;
                }
            }
        }

        if (isset($item['components'])) {
            foreach ((array)$item['components'] as $component) {
                if (!$this->validateComponent($component)) {
                    return false;
                }
                if (isset($component['options'])) {
                    foreach ((array)$component['options'] as $option) {
                        if (!$this->validateOption($option)) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * @param array $child
     * @param array $item
     *
     * @return bool
     */
    public function validateChildren(array $child, array $item): bool
    {
        if (!isset($child['childGroups']) || !is_array($child['childGroups']) || 0 === count($child['childGroups'])) {
            $this->log('The child item should have "childGroups"', $item);

            return false;
        }

        return $this->validateItem($child, true);
    }

    /**
     * @param array $attribute
     * @param array $item
     *
     * @return bool
     */
    public function validateAttributes(array $attribute, array $item): bool
    {
        if (!isset($attribute['attributeIdentifier'])) {
            $this->log('The attribute identifier is missing!', $item);

            return false;
        }
        if (!isset($attribute['type'])) {
            $this->log('The attribute type is missing!', $item);

            return false;
        }
        if (isset($attribute['texts']) || !empty($attribute['texts'])) {
            foreach ($attribute['texts'] as $text) {
                if (!$this->validateText($text, $item)) {
                    return false;
                }
            }
        }
        if (!isset($attribute['attributeValues']) || !is_array($attribute['attributeValues']) || 0 === count($attribute['attributeValues'])) {
            $this->log('The values of the attribute are missing!', $item);

            return false;
        }

        return true;
    }

    public function validateImages(array $image, array $item): bool
    {
        if (!isset($image['thumbnail'])) {
            $this->log('Item thumbnail is not set!', $item);

            return false;
        }
        if (!isset($image['detailImage'])) {
            $this->log('Item image is not set!', $item);

            return false;
        }

        return true;
    }

    /**
     * @param array $price
     * @param array $item
     *
     * @return bool
     */
    public function validatePrices(array $price, array $item): bool
    {
        if (!isset($price['channelIdentifier'])) {
            $this->log('The price is not valid!', $item);

            return false;
        }
        if (!isset($price['bulkPrices']) || !is_array($price['bulkPrices']) || 0 === count($price['bulkPrices'])) {
            $this->log('Bulk prices are missing!', $item);

            return false;
        }
        foreach ($price['bulkPrices'] as $bulkPrice) {
            if (empty($bulkPrice['itemAmountFrom'])) {
                $this->log('Bulk price itemAmountFrom is incorrect!', $item);

                return false;
            }
            if (empty($bulkPrice['price'])) {
                $this->log('Bulk price is incorrect!', $item);

                return false;
            }
        }

        return true;
    }

    /**
     * @param array $status
     * @param array $item
     *
     * @return bool
     */
    public function validateItemStatuses(array $status, array $item): bool
    {
        if (!isset($status['configurationModeIdentifier']) || empty(isset($status['configurationModeIdentifier']))) {
            $this->log('Status config mode identifier is missing!', $item);

            return false;
        }
        if (!isset($status['available']) || !is_bool(isset($status['available']))) {
            $this->log('Status availability is incorrect', $item);

            return false;
        }

        return true;
    }

    /**
     * @param array $area
     * @param array $item
     *
     * @return bool
     */
    public function validateDesignAreas(array $area, array $item): bool
    {
        if (empty($area['designAreaIdentifier'])) {
            $this->log('Design area identifier is missing!', $item);

            return false;
        }
        if (isset($area['texts'])) {
            foreach ($area['texts'] as $text) {
                if (!$this->validateText($text, $item)) {
                    return false;
                }
            }
        }
        if (!empty($area['designProductionMethods'])) {
            foreach ($area['designProductionMethods'] as $visualData) {
                if (!$this->validateProdMethod($visualData, $item)) {
                    return false;
                }
            }
        }
        if (!empty($area['visualData'])) {
            foreach ($area['visualData'] as $visualData) {
                if (!$this->validateVisualData($visualData, $item)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param array $method
     * @param array $item
     *
     * @return bool
     */
    protected function validateProdMethod(array $method, array $item): bool
    {
        if (empty($method['designProductionMethodIdentifier'])) {
            $this->log('Production method identifier is missing!', $item);

            return false;
        }
        if (empty($method['heightMillimeter'])) {
            $this->log('Production method height is missing!', $item);

            return false;
        }
        if (empty($method['widthMillimeter'])) {
            $this->log('Production method width is missing!', $item);

            return false;
        }

        return true;
    }

    /**
     * @param array $data
     * @param array $item
     *
     * @return bool
     */
    protected function validateVisualData(array $data, array $item): bool
    {
        if (empty($data['designViewIdentifier'])) {
            $this->log('Visual data identifier is missing!', $item);

            return false;
        }
        if (empty($data['widthPixels'])) {
            $this->log('Visual data height is missing!', $item);

            return false;
        }
        if (empty($data['widthPixels'])) {
            $this->log('Visual data width is missing!', $item);

            return false;
        }

        return true;
    }

    /**
     * @param array $text
     * @param array $item
     *
     * @return bool
     */
    public function validateText(array $text, array $item): bool
    {
        if (!isset($text['iso'])) {
            $this->log('Text without ISO code!', $item);

            return false;
        }

        return true;
    }

    /**
     * @param array $component
     *
     * @return bool
     */
    public function validateComponent($component): bool
    {
        if (!is_array($component)) {
            $this->log('Component is not an array', $component);

            return false;
        }
        if (!isset($component['componentIdentifier'])) {
            $this->log('Component identifier is missing', $component);

            return false;
        }
        if (!isset($component['options']) || empty($component['options'])) {
            $this->log('Component has no options', $component);

            return false;
        }

        return true;
    }

    /**
     * @param array $option
     *
     * @return bool
     */
    public function validateOption($option): bool
    {
        if (!is_array($option)) {
            $this->log('Option is not an array', $option);

            return false;
        }
        if (!isset($option['optionIdentifier'])) {
            $this->log('Option identifier is missing', $option);

            return false;
        }

        return true;
    }

    /**
     * Log an error during validation.
     *
     * @param string $message
     * @param array $item Root item, that is being checked
     */
    protected function log(string $message, array $item): void
    {
        $this->logger->error(
            $message,
            ['item' => $item['itemIdentifier'] ?? json_encode($item)]
        );
    }
}
