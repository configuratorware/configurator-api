<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;

/**
 * @internal
 */
class GroupHandler
{
    /**
     * @var Connection
     */
    private $conn;

    /**
     * Storage for prepared statements.
     *
     * @var Statement[]
     */
    private $statements = [];

    /**
     * @var ImportHelper
     */
    private $helper;

    /**
     * @param Connection $conn
     * @param ImportHelper $helper
     */
    public function __construct(Connection $conn, ImportHelper $helper)
    {
        $this->conn = $conn;
        $this->helper = $helper;
    }

    /**
     * @param array $childGroup
     * @param int   $itemId
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processChildGroup(array $childGroup, int $itemId)
    {
        $groupId = $this->processGroup($childGroup['group']);
        $valueId = $this->processGroupValue($childGroup['groupValue']);

        if ($groupId) {
            $this->processItemsItemGroup($itemId, $groupId);
        }
        if ($groupId && $valueId) {
            $this->processItemsItemGroupEntry($itemId, $valueId, $groupId);
        }
    }

    /**
     * @param $itemId
     * @param $groupId
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemsItemGroup($itemId, $groupId)
    {
        if (!isset($this->statements['item_itemgroup'])) {
            $this->statements['item_itemgroup'] =
                $this->conn->prepare('insert into
                    item_itemgroup (item_id, itemgroup_id, date_created, date_updated)
                    values (:itemId, :groupId, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['item_itemgroup'];

        $stmt->bindValue(':itemId', $itemId);
        $stmt->bindValue(':groupId', $groupId);
        $this->helper->bindDate($stmt);

        $stmt->execute();
    }

    /**
     * @param int $itemId
     * @param int $entryId
     * @param int $groupId
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemsItemGroupEntry(int $itemId, int $entryId, int $groupId)
    {
        if (!isset($this->statements['item_itemgroupentry'])) {
            $this->statements['item_itemgroupentry'] =
                $this->conn->prepare('insert into
                    item_itemgroupentry (item_id, itemgroupentry_id, itemgroup_id, date_created, date_updated)
                    values (:itemId, :entryId, :groupId, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['item_itemgroupentry'];

        $stmt->bindValue(':itemId', $itemId);
        $stmt->bindValue(':entryId', $entryId);
        $stmt->bindValue(':groupId', $groupId);
        $this->helper->bindDate($stmt);

        $stmt->execute();
    }

    /**
     * @param array $group
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processGroup(array $group)
    {
        if (!isset($this->statements['itemgroup'])) {
            $this->statements['itemgroup'] =
                $this->conn->prepare('insert into
                    itemgroup (identifier, sequencenumber, date_created, date_updated)
                    values (:identifier, :sequenceNumber, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['itemgroup'];

        $stmt->bindValue(':identifier', trim((string)$group['groupIdentifier']));
        $stmt->bindValue(':sequenceNumber', $this->getNextItemGroupSequenceNumber());
        $this->helper->bindDate($stmt);

        $stmt->execute();

        $groupId = (int) $this->conn->lastInsertId();

        foreach ($group['texts'] as $text) {
            $this->processItemGroupText($text, $groupId);
        }

        return $groupId;
    }

    /**
     * @param array $groupValue
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processGroupValue(array $groupValue): int
    {
        if (!isset($this->statements['itemgroupentry'])) {
            $this->statements['itemgroupentry'] =
                $this->conn->prepare('insert into
                    itemgroupentry (identifier, sequencenumber, date_created, date_updated)
                    values (:identifier, :sequenceNumber, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['itemgroupentry'];

        $stmt->bindValue(':identifier', trim((string)$groupValue['groupValueIdentifier']));
        $stmt->bindValue(':sequenceNumber', $this->getNextItemGroupEntrySequenceNumber());
        $this->helper->bindDate($stmt);

        $stmt->execute();

        $entryId = (int) $this->conn->lastInsertId();

        foreach ($groupValue['texts'] as $text) {
            $this->processItemGroupEntryText($text, $entryId);
        }

        return $entryId;
    }

    /**
     * @param array $text
     * @param int   $groupEntryId
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemGroupEntryText(array $text, int $groupEntryId): int
    {
        if (!isset($this->statements['groupentrytext'])) {
            $this->statements['groupentrytext'] =
                $this->conn->prepare('insert into
                    itemgroupentrytranslation (translation, itemgroupentry_id, language_id, date_created, date_updated)
                    values (:translation,:itemGroupEntryId,:languageId,:dateCreated,:dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['groupentrytext'];

        $languageId = $this->helper->getLanguageIdByTextItem($text);
        if (!$languageId) {
            return 0;
        }

        $stmt->bindValue(':translation', $text['title']);
        $stmt->bindValue(':itemGroupEntryId', $groupEntryId);
        $stmt->bindValue(':languageId', $languageId);
        $this->helper->bindDate($stmt);

        $stmt->execute();

        return (int) $this->conn->lastInsertId();
    }

    /**
     * @param array $text
     * @param int   $groupId
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemGroupText(array $text, int $groupId): int
    {
        if (!isset($this->statements['grouptext'])) {
            $this->statements['grouptext'] =
                $this->conn->prepare('insert into
                    itemgrouptranslation (translation, itemgroup_id, language_id, date_created, date_updated)
                    values (:translation,:itemgroupId,:languageId,:dateCreated,:dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['grouptext'];

        $languageId = $this->helper->getLanguageIdByTextItem($text);
        if (!$languageId) {
            return 0;
        }

        $stmt->bindValue(':translation', (string)$text['title']);
        $stmt->bindValue(':itemgroupId', $groupId);
        $stmt->bindValue(':languageId', $languageId);
        $this->helper->bindDate($stmt);

        $stmt->execute();

        $groupId = (int) $this->conn->lastInsertId();

        return $groupId;
    }

    /**
     * Next ItemGroup sequence number.
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getNextItemGroupSequenceNumber(): int
    {
        if (!isset($this->statements['itemgroup_seq'])) {
            $this->statements['itemgroup_seq'] =
                $this->conn->prepare('select max(sequencenumber) as sequence_number from itemgroup');
        }
        $stmt = $this->statements['itemgroup_seq'];
        $result = $stmt->executeQuery();

        $seqNumber = $result->fetchFirstColumn();
        $seqNumber = $seqNumber[0] ?? 0;

        return (int)$seqNumber + 1;
    }

    /**
     * Next ItemGroupEntry sequence number.
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getNextItemGroupEntrySequenceNumber(): int
    {
        if (!isset($this->statements['itemgroupentry_seq'])) {
            $this->statements['itemgroupentry_seq'] =
                $this->conn->prepare('select max(sequencenumber) as sequence_number from itemgroupentry');
        }
        $stmt = $this->statements['itemgroupentry_seq'];
        $result = $stmt->executeQuery();

        $seqNumber = $result->fetchFirstColumn();
        $seqNumber = $seqNumber[0] ?? 0;

        return (int)$seqNumber + 1;
    }
}
