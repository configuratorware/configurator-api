<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Statement;
use Psr\Log\LoggerInterface;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;

/**
 * @internal
 */
class ItemHandler
{
    /**
     * @var Connection
     */
    private $conn;

    /**
     * Storage for prepared statements.
     *
     * @var Statement[]
     */
    private $statements = [];

    /**
     * @var ImportHelper
     */
    private $helper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var GroupHandler
     */
    private $groupHandler;

    /**
     * @param Connection $conn
     * @param GroupHandler $groupHandler
     * @param ImportHelper $helper
     * @param ImportLogger $logger
     */
    public function __construct(Connection $conn, GroupHandler $groupHandler, ImportHelper $helper, ImportLogger $logger)
    {
        $this->conn = $conn;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->groupHandler = $groupHandler;
    }

    /**
     * Insert new Item.
     *
     * @param array $itemData
     * @param int $parentItemId
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception|\Doctrine\DBAL\Driver\Exception
     */
    public function processItem(array $itemData, int $parentItemId = null): int
    {
        if (!isset($this->statements['item'])) {
            $this->statements['item'] =
                $this->conn->prepare('INSERT INTO 
                  item (identifier, minimum_order_amount, accumulate_amounts, parent_id, configuration_mode, item_status_id, visualization_mode_id, overwrite_component_order, override_option_order, date_created, date_updated)
                  VALUES (:identifier, :minimumOrderAmount, :accumulateAmounts, :parentId, :configurationMode, :itemStatusId, :visualizationModeId, IFNULL(:overwriteComponentOrder, 0), :overrideOptionOrder, :dateCreated, :dateUpdated)
                  ON DUPLICATE KEY UPDATE
                        minimum_order_amount = :minimumOrderAmount,
                        accumulate_amounts = :accumulateAmounts,
                        parent_id = :parentId,
                        configuration_mode = IFNULL(:configurationMode, configuration_mode),
                        item_status_id = IFNULL(:itemStatusId, item_status_id),
                        visualization_mode_id = IFNULL(:visualizationModeId, visualization_mode_id),
                        overwrite_component_order = IFNULL(:overwriteComponentOrder, overwrite_component_order),
                        override_option_order = :overrideOptionOrder,
                        id = LAST_INSERT_ID(id),
                        date_updated = :dateUpdated;
                  ');
        }
        $stmt = &$this->statements['item'];

        $itemStatusId = isset($itemData['itemStatusIdentifier']) ? $this->helper->getItemStatusByIdentifier((string)$itemData['itemStatusIdentifier']) : 0;
        $visualizationModeId = 0;
        if (null === $parentItemId) {
            $visualizationModeId = $this->helper->getVisualizationModeId($itemData);
        }
        $overwriteComponentOrder = isset($itemData['overwriteComponentOrder']) ? (int)$itemData['overwriteComponentOrder'] : null;

        $stmt->bindValue(':identifier', trim((string)$itemData['itemIdentifier']));
        $stmt->bindValue(':minimumOrderAmount', (int)($itemData['minimumOrderAmount'] ?? 1)); // default to 1
        $stmt->bindValue(':accumulateAmounts', (int)($itemData['accumulateAmounts'] ?? true)); // default to true
        $stmt->bindValue(':parentId', $parentItemId);
        $stmt->bindValue(':configurationMode', isset($itemData['configurationMode']) ? (string)$itemData['configurationMode'] : null);
        $stmt->bindValue(':itemStatusId', $itemStatusId > 0 ? $itemStatusId : null);
        $stmt->bindValue(':visualizationModeId', $visualizationModeId > 0 ? $visualizationModeId : null);
        $stmt->bindValue(':overwriteComponentOrder', $overwriteComponentOrder);
        $stmt->bindValue(':overrideOptionOrder', $this->overrideOptionOrder($itemData), ParameterType::BOOLEAN);

        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        $itemId = (int)$this->conn->lastInsertId();

        if (isset($itemData['childGroups'])) {
            foreach ($itemData['childGroups'] as $childGroup) {
                $this->groupHandler->processChildGroup($childGroup, $itemId);
            }
        }

        return $itemId;
    }

    /**
     * Insert new item text.
     *
     * @param array $textData
     * @param int $itemId
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemText(array $textData, int $itemId): int
    {
        if (!isset($this->statements['itemtext'])) {
            $this->statements['itemtext'] =
                $this->conn->prepare('INSERT INTO
                    itemtext (title, description, item_id, language_id, date_created, date_updated)
                    VALUES (:title, :description, :itemId, :languageId, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE 
                        id = LAST_INSERT_ID(id), title = :title, description = :description, date_updated = :dateUpdated
                ');
        }
        $stmt = &$this->statements['itemtext'];

        $langId = $this->helper->getLanguageIdByTextItem($textData);
        if (!$langId) {
            $this->logger->error('Language is invalid', $textData);

            return 0;
        }

        $stmt->bindValue(':title', $textData['title']);
        $stmt->bindValue(':description', $textData['description']);
        $stmt->bindValue(':itemId', $itemId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * Insert new item price.
     *
     * @param array $price
     * @param int $itemId
     *
     * @return bool success
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemPrice(array $price, int $itemId): bool
    {
        if (!isset($this->statements['itemprice'])) {
            $this->statements['itemprice'] =
                $this->conn->prepare('INSERT INTO
                    itemprice (price, price_net, amountfrom, date_created, date_updated, channel_id, item_id)
                    VALUES (:price, :priceNet, :amountFrom, :dateCreated, :dateUpdated, :channelId, :itemId)
                    ON DUPLICATE KEY UPDATE price = :price, price_net = :priceNet, id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['itemprice'];

        $channelId = $this->helper->getChannelByIdentifier($price['channelIdentifier']);

        if (!$channelId) {
            $this->logger->error('Channel is invalid', $price);

            return false;
        }

        foreach ($price['bulkPrices'] as $bulkPrice) {
            // amountfrom has  to be a number bigger than 0
            if (!isset($bulkPrice['itemAmountFrom'])
                || empty((int)$bulkPrice['itemAmountFrom'])
                || (int)$bulkPrice['itemAmountFrom'] < 1) {
                $bulkPrice['itemAmountFrom'] = 1;
            }

            $stmt->bindValue(':itemId', $itemId);
            $stmt->bindValue(':channelId', $channelId);
            $stmt->bindValue(':price', $bulkPrice['price']);
            $stmt->bindValue(':priceNet', $bulkPrice['priceNet']);
            $stmt->bindValue(':amountFrom', $bulkPrice['itemAmountFrom']);
            $this->helper->bindDate($stmt);
            $stmt->executeStatement();
        }

        return true;
    }

    /**
     * @param string $itemIdentifier
     *
     * @return array|null
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function findItem(string $itemIdentifier): ?array
    {
        $statement = $this->conn->prepare('SELECT * FROM item WHERE identifier = :itemIdentifier AND date_deleted = :dateDeletedDefault');
        $statement->bindValue(':itemIdentifier', $itemIdentifier);
        $statement->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $result = $statement->executeQuery();

        if ($result->rowCount()) {
            $result = $result->fetchAssociative();
            if ($result) {
                return $result;
            }
        }

        $this->logger->error('Item not in database!', [$itemIdentifier]);

        return null;
    }

    private function overrideOptionOrder(array $itemData): bool
    {
        if (!array_key_exists('components', $itemData)) {
            return false;
        }

        foreach ($itemData['components'] as $component) {
            if (array_key_exists('optionsOrder', $component) && count($component['optionsOrder']) > 0) {
                return true;
            }
        }

        return false;
    }
}
