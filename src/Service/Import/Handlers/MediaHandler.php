<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ValueObject\ImageFinder;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ErrorLocation;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImageFile;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImageUrl;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ComponentImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\DesignViewImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ItemImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\OptionImagePathsInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @internal
 */
final class MediaHandler
{
    /**
     * @var array
     */
    private const IMAGE_TYPES = ['image/jpeg' => 'jpg', 'image/png' => 'png'];

    /**
     * RegEx pattern for image names with identifier.
     *
     * @var string
     */
    private const ITEM_IMAGE_NAME_PATTERN = '/.+\.(jpeg|jpg|png)/i';

    /**
     * @var DesignViewImagePathsInterface
     */
    private DesignViewImagePathsInterface $designViewImagePaths;

    /**
     * @var Filesystem
     */
    private Filesystem $filesystem;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var ItemImagePathsInterface
     */
    private ItemImagePathsInterface $itemImagePaths;

    /**
     * @var ComponentImagePathsInterface
     */
    private ComponentImagePathsInterface $componentImagePaths;

    /**
     * @var OptionImagePathsInterface
     */
    private OptionImagePathsInterface $optionImagePaths;

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    /**
     * @param ComponentImagePathsInterface $componentImagePaths
     * @param DesignViewImagePathsInterface $designViewImagePaths
     * @param Filesystem $filesystem
     * @param ItemImagePathsInterface $itemImagePaths
     * @param ImportLogger $logger
     * @param OptionImagePathsInterface $optionImagePaths
     */
    public function __construct(
        ComponentImagePathsInterface $componentImagePaths,
        DesignViewImagePathsInterface $designViewImagePaths,
        Filesystem $filesystem,
        ItemImagePathsInterface $itemImagePaths,
        ImportLogger $logger,
        OptionImagePathsInterface $optionImagePaths
    ) {
        $this->componentImagePaths = $componentImagePaths;
        $this->designViewImagePaths = $designViewImagePaths;
        $this->filesystem = $filesystem;
        $this->itemImagePaths = $itemImagePaths;
        $this->logger = $logger;
        $this->optionImagePaths = $optionImagePaths;
        $this->client = HttpClient::create();
    }

    /**
     * Item identifiers from detail images.
     *
     * @return array
     */
    public function collectItemIdentifiersForPresentDetailImages(): array
    {
        return $this->collectItemIdentifiersFromImages($this->itemImagePaths->getItemDetailImagePath());
    }

    /**
     * Item identifiers from thumbnail images.
     *
     * @return array
     */
    public function collectItemIdentifiersForPresentItemThumbnails(): array
    {
        return $this->collectItemIdentifiersFromImages($this->itemImagePaths->getItemThumbnailPath());
    }

    /**
     * @param string $searchDir
     *
     * @return array
     */
    private function collectItemIdentifiersFromImages(string $searchDir): array
    {
        if (!$this->filesystem->exists($searchDir)) {
            return [];
        }

        $finder = new Finder();
        $finder->depth(0)->files()->in($searchDir)->name(self::ITEM_IMAGE_NAME_PATTERN);

        $identifiers = [];
        foreach ($finder as $imageFile) {
            $identifiers[] = $imageFile->getBasename('.' . $imageFile->getExtension());
        }

        return $identifiers;
    }

    /**
     * @param string $itemIdentifier
     * @param string $imageUrl
     *
     * @return bool
     */
    public function uploadItemThumbnail(string $itemIdentifier, string $imageUrl): bool
    {
        $imageFile = $this->loadImageFile(
            $imageUrl,
            ErrorLocation::create('itemIdentifier', $itemIdentifier)
        );

        if (null === $imageFile) {
            return false;
        }

        $this->filesystem->dumpFile(
            $this->itemImagePaths->getItemThumbnailPath() . '/' . $itemIdentifier . '.' . $imageFile->extension,
            $imageFile->content
        );

        return true;
    }

    /**
     * @param string $itemIdentifier
     * @param string $imageUrl
     *
     * @return bool
     */
    public function uploadItemDetailImage(string $itemIdentifier, string $imageUrl): bool
    {
        $imageFile = $this->loadImageFile(
            $imageUrl,
            ErrorLocation::create('itemIdentifier', $itemIdentifier)
        );

        if (null === $imageFile) {
            return false;
        }

        $this->filesystem->dumpFile(
            $this->itemImagePaths->getItemDetailImagePath() . '/' . $itemIdentifier . '.' . $imageFile->extension,
            $imageFile->content
        );

        return true;
    }

    /**
     * @param string $itemIdentifier
     * @param string $viewIdentifier
     * @param array $viewThumbnails
     *
     * @return array
     */
    public function uploadViewThumbnails(
        string $itemIdentifier,
        string $viewIdentifier,
        array $viewThumbnails = []
    ): array {
        if ('' === $itemIdentifier || '' === $viewIdentifier) {
            return [];
        }

        $attemptedImagePathChunks = [];

        // upload or override images
        foreach ($viewThumbnails as $viewThumbnail) {
            $imageFile = $this->loadImageFile(
                (string)$viewThumbnail['viewThumbnailURL'],
                ErrorLocation::create('itemIdentifier', $itemIdentifier)
            );
            if (null === $imageFile) {
                continue;
            }

            // create image path
            $imageItemIdentifier = trim((string)$viewThumbnail['itemIdentifier']);
            $path = $this->designViewImagePaths->getDesignViewThumbnailPath() . '/' . $itemIdentifier;
            if ($itemIdentifier !== $imageItemIdentifier) {
                $path .= '/' . $imageItemIdentifier;
            }
            $path .= '/' . $viewIdentifier;

            // save image path without extension. This is later used, if images get deleted to make sure,
            // that the ones with errors do NOT get deleted.
            $attemptedImagePathChunks[] = $path;

            // write file
            $this->filesystem->dumpFile($path . '.' . $imageFile->extension, $imageFile->content);
        }

        return $attemptedImagePathChunks;
    }

    /**
     * @param string $itemIdentifier
     * @param array $recentlyCreatedImagesPathChunks
     */
    public function deleteViewThumbnails(string $itemIdentifier, array $recentlyCreatedImagesPathChunks): void
    {
        $searchDir = $this->designViewImagePaths->getDesignViewThumbnailPath() . '/' . $itemIdentifier;

        if (!$this->filesystem->exists($searchDir)) {
            return;
        }

        try {
            $finder = new Finder();
            $finder->files()->in($searchDir)->depth('<2');

            $imagesToRemove = [];
            foreach ($finder as $file) {
                $path = $file->getPath() . '/' . $file->getFilename();
                if (!$this->isRecentlyCreated($path, $recentlyCreatedImagesPathChunks)) {
                    $imagesToRemove[] = $path;
                }
            }

            $this->filesystem->remove($imagesToRemove);
        } catch (\Throwable $exception) {
            $this->logger->error(
                'An Error occurred when deleting images of view thumbnails.',
                ['itemIdentifier' => $itemIdentifier]
            );
        }
    }

    /**
     * @param string $itemIdentifier
     *
     * @return void
     */
    public function deleteItemDetailImage(string $itemIdentifier): void
    {
        $this->deleteImageByIdentifier($itemIdentifier, $this->itemImagePaths->getItemDetailImagePath());
    }

    /**
     * @param string $itemIdentifier
     *
     * @return void
     */
    public function deleteItemThumbnailImage(string $itemIdentifier): void
    {
        $this->deleteImageByIdentifier($itemIdentifier, $this->itemImagePaths->getItemThumbnailPath());
    }

    /**
     * @param string $componentIdentifier
     *
     * @return void
     */
    public function deleteComponentThumbnail(string $componentIdentifier): void
    {
        $this->deleteImageByIdentifier($componentIdentifier, $this->componentImagePaths->getComponentThumbnailPath());
    }

    /**
     * @param string $optionIdentifier
     *
     * @return void
     */
    public function deleteOptionThumbnail(string $optionIdentifier): void
    {
        $this->deleteImageByIdentifier($optionIdentifier, $this->optionImagePaths->getOptionThumbnailPath());
    }

    private function deleteImageByIdentifier(string $identifier, string $directory): void
    {
        while ($file = ImageFinder::for($directory)->findImagePathBy($identifier)) {
            $this->filesystem->remove($file->getPathname());
        }
    }

    /**
     * Upload image for visual data.
     *
     * @param string $itemIdentifier
     * @param string $viewIdentifier
     * @param array $mediaUrls
     */
    public function uploadVisualDataImage(string $itemIdentifier, string $viewIdentifier, array $mediaUrls): void
    {
        $designViewImagePath = $this->designViewImagePaths->getDesignViewImagePath();

        foreach ($mediaUrls as $mediaUrl) {
            $mediaIdentifier = trim((string)$mediaUrl['itemIdentifier']);

            $imageFile = $this->loadImageFile(
                (string)$mediaUrl['mediaURL'],
                ErrorLocation::create('itemIdentifier', $itemIdentifier)
            );
            if (null === $imageFile) {
                return;
            }

            $imagePath = $designViewImagePath . '/' . $itemIdentifier . '/' . $viewIdentifier . '/';
            $imagePath .= $mediaIdentifier . '.' . $imageFile->extension;
            $this->filesystem->dumpFile($imagePath, $imageFile->content);

            // if a file with another extension as the currently imported exists delete it
            $replaces = ['jpg' => 'png', 'png' => 'jpg'];
            $imagePath = strtr($imagePath, $replaces);
            $this->filesystem->remove($imagePath);
        }
    }

    /**
     * @param string $componentIdentifier
     * @param string $mediaUrl
     *
     * @return bool
     */
    public function uploadOptionClassificationThumbnail(string $componentIdentifier, string $mediaUrl): bool
    {
        $imageFile = $this->loadImageFile(
            $mediaUrl,
            ErrorLocation::create('componentIdentifier', $componentIdentifier)
        );

        if (null === $imageFile) {
            return false;
        }

        $imagePath = $this->componentImagePaths->getComponentThumbnailPath() . '/';
        $imagePath .= $componentIdentifier . '.' . $imageFile->extension;
        $this->filesystem->dumpFile($imagePath, $imageFile->content);

        return true;
    }

    /**
     * @param string $optionIdentifier
     * @param string $mediaUrl
     *
     * @return bool
     */
    public function uploadOptionThumbnail(string $optionIdentifier, string $mediaUrl): bool
    {
        $imageFile = $this->loadImageFile(
            $mediaUrl,
            ErrorLocation::create('optionIdentifier', $optionIdentifier)
        );
        if (null === $imageFile) {
            return false;
        }

        $imagePath = $this->optionImagePaths->getOptionThumbnailPath() . '/' . $optionIdentifier . '.' . $imageFile->extension;
        $this->filesystem->dumpFile($imagePath, $imageFile->content);

        return true;
    }

    /**
     * @param string $path
     * @param array $recentlyCreatedImagesPathChunks
     *
     * @return bool
     */
    private function isRecentlyCreated(string $path, array $recentlyCreatedImagesPathChunks): bool
    {
        foreach ($recentlyCreatedImagesPathChunks as $chunk) {
            if (false !== strpos($path, $chunk)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $url
     * @param ErrorLocation $errorLocation
     *
     * @return ImageFile|null
     */
    private function loadImageFile(string $url, ErrorLocation $errorLocation): ?ImageFile
    {
        // check url and its image
        $url = ImageUrl::fromString($url)->imageUrl;
        if (null === $url) {
            $this->logger->error(
                'image url is empty',
                $errorLocation->getErrorLocation()
            );

            return null;
        }

        $response = $this->client->request('GET', $url);
        $statusCode = $response->getStatusCode();
        $content = $response->getContent(false);

        if (Response::HTTP_OK !== $statusCode || empty($content)) {
            $this->logger->error(
                'File could not be downloaded: "' . $url . '"',
                $errorLocation->getErrorLocation()
            );

            return null;
        }

        $contentType = $response->getHeaders()['content-type'][0];

        if (!$contentType || !array_key_exists($contentType, self::IMAGE_TYPES)) {
            $this->logger->error(
                'File type not allowed: "' . $url . '"',
                $errorLocation->getErrorLocation()
            );

            return null;
        }

        return new ImageFile($content, self::IMAGE_TYPES[$contentType]);
    }
}
