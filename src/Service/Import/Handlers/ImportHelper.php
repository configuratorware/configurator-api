<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;
use Psr\Log\LoggerInterface;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\License\LicenseFileValidator;

/**
 * @internal
 */
class ImportHelper
{
    public const DATE_FORMAT_DEFAULT = 'Y-m-d H:i:s';
    /**
     * @var Connection
     */
    private $conn;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * Store language data.
     *
     * @var array
     */
    private $languages = [];

    /**
     * Store channel data.
     *
     * @var array
     */
    private $channels = [];

    /**
     * @var array
     */
    private $itemStatuses;

    /**
     * @var array
     */
    private $visualizationModes;

    /**
     * @var array
     */
    private $productionMethods = [];

    /**
     * @var array
     */
    private $designerProdCalcTypes = [];

    /**
     * @var int
     */
    private $defaultVisualizationModeId;

    /**
     * @var LicenseFileValidator
     */
    private $licenseFileValidator;

    /**
     * @param Connection $conn
     * @param ImportLogger $logger
     * @param LicenseFileValidator $licenseFileValidator
     */
    public function __construct(Connection $conn, ImportLogger $logger, LicenseFileValidator $licenseFileValidator)
    {
        $this->conn = $conn;
        $this->logger = $logger;
        $this->licenseFileValidator = $licenseFileValidator;
    }

    /**
     * Get language ID by ISO code.
     *
     * @param string $code
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getLanguageIdByCode(string $code): int
    {
        if (empty($this->languages)) {
            $this->languages = $this->conn->executeQuery(
                'SELECT id, iso FROM language'
            )->fetchAllAssociative();
        }
        $lang = array_filter(
            $this->languages,
            function (array $langItem) use ($code) {
                return $langItem['iso'] == $code;
            }
        );
        if (empty($lang)) {
            return 0; // language doesn't exist
        }

        return (int)array_values($lang)[0]['id']; // reset keys
    }

    /**
     * Get channel ID by identifier.
     *
     * @param string $identifier
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getChannelByIdentifier(string $identifier): int
    {
        if (empty($this->channels)) {
            $this->channels = $this->conn->executeQuery(
                'SELECT id, identifier FROM channel'
            )->fetchAllAssociative();
        }
        $channel = array_filter(
            $this->channels,
            function (array $channel) use ($identifier) {
                return $channel['identifier'] == $identifier;
            }
        );
        if (empty($channel)) {
            return 0; // channel doesn't exist
        }

        return (int)array_values($channel)[0]['id']; // reset keys
    }

    /**
     * Bind custom data value to a statement.
     *
     * @param Statement $stmt
     * @param null $customData
     *
     * @return bool Was there custom data to save
     */
    public function bindCustomData(Statement &$stmt, $customData = null): bool
    {
        // no custom data
        if (!$customData || empty($customData) || false === json_decode($customData)) {
            $stmt->bindValue(':customData', null);

            return false;
        }

        // custom data is present, test if it's json already
        try {
            $customData = json_decode($customData);
            if (empty($customData)) { // json but empty
                $stmt->bindValue(':customData', null);

                return false;
            }
        } catch (\Exception $e) {
            // it's not json
        }

        $stmt->bindValue(':customData', json_encode($customData));

        return true;
    }

    /**
     * Bind current date to the DBAL prepared statement.
     *
     * @param Statement $stmt
     *
     * @throws \Exception
     */
    public function bindDate(Statement &$stmt)
    {
        $stmt->bindValue(':dateCreated', (new \DateTime())->format('Y-m-d H:i:s'));
        $stmt->bindValue(':dateUpdated', (new \DateTime())->format('Y-m-d H:i:s'));
    }

    /**
     * Get item status ID by identifier.
     *
     * @param string $identifier
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getItemStatusByIdentifier(string $identifier): int
    {
        if (empty($this->itemStatuses)) {
            $stmt = $this->conn
                ->prepare('SELECT id, identifier, item_available FROM item_status WHERE date_deleted = :dateDeleted');
            $stmt->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
            $result = $stmt->executeQuery();
            $this->itemStatuses = $result->fetchAllAssociative();
        }

        // filter itemStatuses by bool availability and return the record for available(s)
        $statuses = array_filter(
            $this->itemStatuses,
            static function (array $status) use ($identifier) {
                return $status['identifier'] == $identifier;
            }
        );

        // get first result of array filter or false
        $status = array_shift($statuses);
        if (!$status) {
            return 0; // no matching status in DB
        }

        return (int)$status['id'];
    }

    /**
     * @param array $itemData
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function getVisualizationModeId(array $itemData): int
    {
        if (isset($itemData['visualizationModeIdentifier'])) {
            $visualizationModeId = $this->getVisualizationModeByIdentifier((string)$itemData['visualizationModeIdentifier']);
        } else {
            $itemIdentifier = trim((string)$itemData['itemIdentifier']);
            $visualizationModeId = $this->getItemVisualizationModeId($itemIdentifier);
            if (0 === $visualizationModeId) {
                $visualizationModeId = $this->getDefaultVisualizationModeId();
            }
        }

        return $visualizationModeId;
    }

    /**
     * @param string $identifier
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception|\Doctrine\DBAL\Driver\Exception
     */
    private function getVisualizationModeByIdentifier(string $identifier): int
    {
        if (empty($this->visualizationModes)) {
            $stmt = $this->conn
                ->prepare('SELECT id, identifier FROM visualization_mode WHERE date_deleted = :dateDeleted');
            $stmt->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
            $result = $stmt->executeQuery();
            $this->visualizationModes = $result->fetchAllAssociative();
        }

        // filter itemStatuses by bool availability and return the record for available(s)
        $visualizationModes = array_filter(
            $this->visualizationModes,
            static function (array $mode) use ($identifier) {
                return $mode['identifier'] == $identifier;
            }
        );

        // get first result of array filter or false
        $visualizationMode = array_shift($visualizationModes);
        if (!$visualizationMode) {
            return 0; // no matching status in DB
        }

        return (int)$visualizationMode['id'];
    }

    /**
     * @param string $itemIdentifier
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function getItemVisualizationModeId(string $itemIdentifier): int
    {
        $stmt = $this->conn
            ->prepare('SELECT visualization_mode_id FROM item WHERE identifier = :itemIdentifier AND date_deleted = :dateDeleted');
        $stmt->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
        $stmt->bindValue(':itemIdentifier', $itemIdentifier);
        $result = $stmt->executeQuery();
        $visualizationModeId = $result->fetchOne();

        return (int)$visualizationModeId;
    }

    /**
     * @return int
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function getDefaultVisualizationModeId(): int
    {
        if (null === $this->defaultVisualizationModeId) {
            $license = $this->licenseFileValidator->getValidProduct();
            $visualizationModeIdentifier = 'designer' === $license ? VisualizationMode::IDENTIFIER_2D_VARIANT : VisualizationMode::IDENTIFIER_2D_LAYER;
            $stmt = $this->conn
                ->prepare('SELECT id FROM visualization_mode WHERE identifier = :visualiztaionModeIdentifier AND date_deleted = :dateDeleted');
            $stmt->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
            $stmt->bindValue(':visualiztaionModeIdentifier', $visualizationModeIdentifier);
            $result = $stmt->executeQuery();
            $this->defaultVisualizationModeId = (int)$result->fetchOne();
        }

        return $this->defaultVisualizationModeId;
    }

    /**
     * @param string $identifier
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getProductionMethodByIdentifier(string $identifier): int
    {
        if (empty($this->productionMethods)) {
            $stmt = $this->conn
                ->prepare('SELECT id, identifier FROM design_production_method 
                            WHERE date_deleted = :dateDeleted');
            $stmt->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
            $result = $stmt->executeQuery();
            $this->productionMethods = $result->fetchAllAssociative();
        }

        // filter by method desginProductionMethodIdentifier, will return one or null result
        $methods = array_filter(
            $this->productionMethods,
            function (array $item) use ($identifier) {
                return $item['identifier'] == $identifier;
            }
        );

        // get first result of array filter or false
        $method = array_shift($methods);
        if (!$method) {
            return 0; // doesn't exist
        }

        return (int)$method['id'];
    }

    /**
     * @param string $identifier
     * @param int $methodId
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getDesignerProdCalculationTypeByIdentifier(string $identifier, int $methodId): int
    {
        if (empty($this->designerProdCalcTypes)) {
            $stmt = $this->conn
                ->prepare('SELECT id, identifier, design_production_method_id FROM designer_production_calculation_type 
                            WHERE date_deleted = :dateDeleted');
            $stmt->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
            $result = $stmt->executeQuery();
            $this->designerProdCalcTypes = $result->fetchAllAssociative();
        }

        // filter by calculationTypeIdentifier, will return one or null result
        $calcTypes = array_filter(
            $this->designerProdCalcTypes,
            function (array $item) use ($identifier, $methodId) {
                return $item['identifier'] == $identifier && $item['design_production_method_id'] == $methodId;
            }
        );

        // get first result of array filter or false
        $calcType = array_shift($calcTypes);
        if (!$calcType) {
            return 0; // doesn't exist
        }

        return (int)$calcType['id'];
    }

    /**
     * Get language by text array while also validating it.
     *
     * @param array $text
     *
     * @return int
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function getLanguageIdByTextItem(array $text): int
    {
        if (!isset($text['iso']) || !is_string($text['iso'])) {
            $this->logger->notice('Unavailable iso for text', $text);

            return 0;
        }
        if ((!isset($text['title']) || !is_string($text['title'])) &&
            (!isset($text['translation']) || !is_string($text['translation']))
        ) {
            $this->logger->notice('Title is not set in text', $text);

            return 0;
        }
        $languageId = $this->getLanguageIdByCode($text['iso']);
        if (!$languageId) {
            $this->logger->notice('Unavailable language for text', $text);

            return 0;
        }

        return $languageId;
    }
}
