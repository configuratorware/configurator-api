<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\DBAL\Statement;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeGenerator;

/**
 * @internal
 */
class DefaultConfigurationHandler
{
    /**
     * @var Connection
     */
    private $conn;

    /**
     * @var ImportHelper
     */
    private $helper;

    /**
     * @var ConfigurationCodeGenerator
     */
    private $configurationCodeGenerator;

    /**
     * Storage for prepared statements.
     *
     * @var Statement[]
     */
    private $statements = [];

    /**
     * @var int|null
     */
    private $getDefaultConfigurationTypeId = null;

    /**
     * DefaultConfigurationHandler constructor.
     *
     * @param Connection $conn
     * @param ImportHelper $helper
     * @param ConfigurationCodeGenerator $configurationCodeGenerator
     */
    public function __construct(
        Connection $conn,
        ImportHelper $helper,
        ConfigurationCodeGenerator $configurationCodeGenerator
    ) {
        $this->conn = $conn;
        $this->helper = $helper;
        $this->configurationCodeGenerator = $configurationCodeGenerator;
    }

    /**
     * @param array $componentData
     * @param int $itemId
     *
     * @throws DBALException
     */
    public function processDefaultConfigurationData(array $componentData, int $itemId): void
    {
        $selectedOptions = $this->collectSelectedOptions($componentData);
        if ([] !== $selectedOptions) {
            $this->saveDefaultConfiguration($selectedOptions, $itemId);
        }
    }

    /**
     * @param array $componentData
     *
     * @return array Array of default options
     */
    private function collectSelectedOptions(array $componentData): array
    {
        $selectedOptions = [];
        foreach ($componentData as $component) {
            if (!isset($component['defaultSelections']) || [] === (array)$component['defaultSelections']) {
                continue;
            }
            foreach ($component['defaultSelections'] as $defaultSelection) {
                $optionClassificationIdentifier = $component['componentIdentifier'];
                if (!isset($defaultSelection['optionIdentifier'])) {
                    continue;
                }
                if (!isset($selectedOptions[$optionClassificationIdentifier])) {
                    $selectedOptions[$optionClassificationIdentifier] = [];
                }

                $selectedOption = [
                    'identifier' => $defaultSelection['optionIdentifier'],
                    'amount' => isset($defaultSelection['amount']) ? (int)$defaultSelection['amount'] : 1,
                ];

                if (isset($defaultSelection['inputText'])) {
                    $selectedOption['inputText'] = $defaultSelection['inputText'];
                }

                $selectedOptions[$optionClassificationIdentifier][] = $selectedOption;
            }
        }

        return $selectedOptions;
    }

    /**
     * @param array $selectedOptions
     * @param int $itemId
     *
     * @return int
     *
     * @throws DBALException
     */
    private function saveDefaultConfiguration(array $selectedOptions, int $itemId): int
    {
        if (!isset($this->statements['save_default_configuration'])) {
            $this->statements['save_default_configuration'] =
                $this->conn->prepare('insert into
                    configuration  (item_id, code, selectedoptions, configurationtype_id, date_created, date_updated)
                    values (:itemId, :code, :selectedOptions, :configurationType, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), selectedoptions = :selectedOptions,
                                            date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['save_default_configuration'];

        $configurationCodeLength = $this->configurationCodeGenerator
            ->getConfigurationCodeLengthByConfigurationType(ConfigurationtypeRepository::DEFAULT_OPTIONS);
        $configurationCode = $this->configurationCodeGenerator->generateCode($configurationCodeLength);

        $stmt->bindValue(':itemId', $itemId);
        $stmt->bindValue(':code', $configurationCode);
        $stmt->bindValue(':configurationType', $this->getDefaultConfigurationTypeId());
        $stmt->bindValue(':selectedOptions', json_encode($selectedOptions));
        $stmt->bindValue(':defaultClientId', ClientRepository::DEFAULT_CLIENT_IDENTIFIER);
        $this->helper->bindDate($stmt);
        $stmt->executeStatement();

        // there is no key check on default configurations, so we need to delete the old ones manually
        $this->deletePreviousDefaultConfiguration($itemId);

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param int $itemId
     *
     * @throws DBALException
     */
    private function deletePreviousDefaultConfiguration(int $itemId): void
    {
        if (!isset($this->statements['delete_default_configuration'])) {
            $this->statements['delete_default_configuration'] =
                $this->conn->prepare('update configuration
                    set date_deleted = :dateUpdated
                    where item_id = :itemId AND configurationtype_id = :configurationType AND 
                          date_updated < :dateUpdated;');
        }
        $stmt = &$this->statements['delete_default_configuration'];

        $stmt->bindValue(':itemId', $itemId);
        $stmt->bindValue(':configurationType', $this->getDefaultConfigurationTypeId());
        $stmt->bindValue(':dateUpdated', (new \DateTime())->format('Y-m-d H:i:s'));

        $stmt->executeStatement();
    }

    /**
     * @return int|null
     *
     * @throws DBALException
     */
    public function getDefaultConfigurationTypeId(): ?int
    {
        if (null === $this->getDefaultConfigurationTypeId) {
            $stmt = $this->conn->prepare('select id from configurationtype where identifier = :identifier;');
            $stmt->bindValue(':identifier', ConfigurationtypeRepository::DEFAULT_OPTIONS);
            $result = $stmt->executeQuery();
            $this->getDefaultConfigurationTypeId = $result->fetchFirstColumn()[0];
        }

        return $this->getDefaultConfigurationTypeId;
    }
}
