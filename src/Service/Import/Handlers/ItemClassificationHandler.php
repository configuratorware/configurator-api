<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement; /**
 * @internal
 */
class ItemClassificationHandler
{
    private const STATEMENT_ITEMCLASSIFICATION = 'itemclassification';

    private const STATEMENT_ITEM_ITEM_CLASSIFICATION = 'itemitemclassification';

    private const STATEMENT_SEQUENCENUMBER = 'sequencenumber';

    private const STATEMENT_ITEMCLASSIFICATION_TEXT = 'itemclassificationtext';

    private Connection $conn;

    /**
     * @var Statement[]
     */
    private array $statements = [];

    private ImportHelper $helper;

    public function __construct(Connection $conn, ImportHelper $helper)
    {
        $this->conn = $conn;
        $this->helper = $helper;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemClassification(array $itemClassification, int $itemId): int
    {
        if (!isset($this->statements[self::STATEMENT_ITEMCLASSIFICATION])) {
            $this->statements[self::STATEMENT_ITEMCLASSIFICATION] =
                $this->conn->prepare('insert into
                    itemclassification (identifier, sequencenumber, date_created, date_updated)
                    values (:identifier, :sequenceNumber, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements[self::STATEMENT_ITEMCLASSIFICATION];

        $stmt->bindValue(':identifier', trim((string)$itemClassification['identifier']));
        $stmt->bindValue(':sequenceNumber', $this->getNextSequenceNumber());
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        $itemClassificationId = (int) $this->conn->lastInsertId();

        $this->processItemItemClassification($itemClassificationId, $itemId);

        if (isset($itemClassification['texts'])) {
            foreach ($itemClassification['texts'] as $text) {
                $this->processItemClassificationText($text, $itemClassificationId);
            }
        }

        return $itemClassificationId;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    private function processItemItemClassification(int $itemClassificationId, int $itemId): void
    {
        if (!isset($this->statements[self::STATEMENT_ITEM_ITEM_CLASSIFICATION])) {
            $this->statements[self::STATEMENT_ITEM_ITEM_CLASSIFICATION] =
                    $this->conn->prepare('insert into
                        item_itemclassification (item_id, itemclassification_id, date_created, date_updated)
                        values (:itemId, :itemClassificationId, :dateCreated, :dateUpdated)
                        ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements[self::STATEMENT_ITEM_ITEM_CLASSIFICATION];

        $stmt->bindValue(':itemId', $itemId);
        $stmt->bindValue(':itemClassificationId', $itemClassificationId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    private function processItemClassificationText(array $text, int $itemClassificationId): void
    {
        if (!isset($this->statements[self::STATEMENT_ITEMCLASSIFICATION_TEXT])) {
            $this->statements[self::STATEMENT_ITEMCLASSIFICATION_TEXT] =
                $this->conn->prepare('insert into
                    itemclassificationtext (title, description, itemclassification_id, language_id, date_created, date_updated)
                    values (:title, :description, :itemClassificationId, :languageId, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements[self::STATEMENT_ITEMCLASSIFICATION_TEXT];

        $languageId = $this->helper->getLanguageIdByTextItem($text);
        if (!$languageId) {
            return;
        }

        $stmt->bindValue(':title', (string)$text['title']);
        $stmt->bindValue(':description', isset($text['description']) ? (string)$text['description'] : '');
        $stmt->bindValue(':itemClassificationId', $itemClassificationId);
        $stmt->bindValue(':languageId', $languageId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    public function getNextSequenceNumber(): int
    {
        if (!isset($this->statements[self::STATEMENT_SEQUENCENUMBER])) {
            $this->statements[self::STATEMENT_SEQUENCENUMBER] =
                $this->conn->prepare('select max(sequencenumber) as sequence_number from itemclassification');
        }
        $stmt = $this->statements[self::STATEMENT_SEQUENCENUMBER];
        $result = $stmt->executeQuery();

        $seqNumber = $result->fetchFirstColumn();
        $seqNumber = $seqNumber[0] ?? 0;

        return (int)$seqNumber + 1;
    }
}
