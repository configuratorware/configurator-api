<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\ParameterType;
use Doctrine\DBAL\Statement;

/**
 * @internal
 */
class AttributeHandler
{
    private const STATEMENT_ATTRIBUTEVALUE = 'attributevalue';

    private const FIELD_VALUE = 'value';

    private const FIELD_TRANSLATIONS = 'translations';

    private Connection $conn;

    /**
     * Storage for prepared statements.
     *
     * @var Statement[]
     */
    private array $statements = [];

    private ImportHelper $helper;

    public function __construct(Connection $conn, ImportHelper $helper)
    {
        $this->conn = $conn;
        $this->helper = $helper;
    }

    /**
     * Insert new item attribute.
     *
     * @param array $itemAttr
     * @param int $itemId
     *
     * @return bool success
     *
     * @throws Exception
     */
    public function processItemAttribute(array $itemAttr, int $itemId): bool
    {
        if (!isset($this->statements['item_attribute'])) {
            $this->statements['item_attribute'] =
                $this->conn->prepare('insert into
                    item_attribute (date_created, date_updated, attribute_id, attributevalue_id, item_id)
                    values (:dateCreated, :dateUpdated, :attributeId, :attributeValueId, :itemId)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['item_attribute'];

        $attrId = $this->processAttribute($itemAttr);

        foreach ($itemAttr['texts'] as $text) {
            $this->processAttributeText($text, $attrId);
        }

        foreach ($itemAttr['attributeValues'] as $attrValue) {
            $valueId = $this->processAttributeValue($attrValue);
            if ($valueId) {
                $stmt->bindValue(':itemId', $itemId);
                $stmt->bindValue(':attributeId', $attrId);
                $stmt->bindValue(':attributeValueId', $valueId);
                $this->helper->bindDate($stmt);
                $stmt->executeQuery();
            }
        }

        return true;
    }

    /**
     * @param array $optionClassificationAttribute
     * @param int $optionClassificationId
     *
     * @return bool Is save successful
     *
     * @throws Exception
     */
    public function processOptionClassificationAttribute(array $optionClassificationAttribute, int $optionClassificationId): bool
    {
        if (!isset($this->statements['optionclassification_attribute'])) {
            $this->statements['optionclassification_attribute'] =
                $this->conn->prepare('insert into
                    option_classification_attribute (date_created, date_updated, attribute_id, attributevalue_id,
                                                     optionclassification_id)
                    values (:dateCreated, :dateUpdated, :attributeId, :attributeValueId, :optionClassificationId)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['optionclassification_attribute'];

        $attrId = $this->processAttribute($optionClassificationAttribute);

        foreach ($optionClassificationAttribute['texts'] as $text) {
            $this->processAttributeText($text, $attrId);
        }

        foreach ($optionClassificationAttribute['attributeValues'] as $attrValue) {
            $valueId = $this->processAttributeValue($attrValue);
            if ($valueId) {
                $stmt->bindValue(':optionClassificationId', $optionClassificationId);
                $stmt->bindValue(':attributeId', $attrId);
                $stmt->bindValue(':attributeValueId', $valueId);
                $this->helper->bindDate($stmt);
                $stmt->executeQuery();
            }
        }

        return true;
    }

    /**
     * @param array $optionAttribute
     * @param int $optionId
     *
     * @return bool Is save successful
     *
     * @throws Exception
     */
    public function processOptionAttribute(array $optionAttribute, int $optionId): bool
    {
        if (!isset($this->statements['option_attribute'])) {
            $this->statements['option_attribute'] =
                $this->conn->prepare('insert into
                    option_attribute (date_created, date_updated, attribute_id, attributevalue_id, option_id)
                    values (:dateCreated, :dateUpdated, :attributeId, :attributeValueId, :optionId)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['option_attribute'];

        $attrId = $this->processAttribute($optionAttribute);

        foreach ($optionAttribute['texts'] as $text) {
            $this->processAttributeText($text, $attrId);
        }

        foreach ($optionAttribute['attributeValues'] as $attrValue) {
            $valueId = $this->processAttributeValue($attrValue);
            if ($valueId) {
                $stmt->bindValue(':optionId', $optionId);
                $stmt->bindValue(':attributeId', $attrId);
                $stmt->bindValue(':attributeValueId', $valueId);
                $this->helper->bindDate($stmt);
                $stmt->executeQuery();
            }
        }

        return true;
    }

    /**
     * Process a single item attribute.
     *
     * @param array $attr
     *
     * @return int New attribute's ID
     *
     * @throws Exception
     */
    public function processAttribute(array $attr): int
    {
        if (!isset($this->statements['attribute'])) {
            $this->statements['attribute'] =
                $this->conn->prepare('insert into
                    attribute (identifier, attributedatatype, date_created, date_updated)
                    values (:identifier, :attributeDataType, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id),
                                            attributedatatype = :attributeDataType, date_updated = :dateUpdated');
        }
        $stmt = &$this->statements['attribute'];

        $stmt->bindValue(':identifier', trim((string)$attr['attributeIdentifier']));
        $stmt->bindValue(':attributeDataType', (string)$attr['type']);
        $this->helper->bindDate($stmt);
        $stmt->executeQuery();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $attrText
     * @param int $attributeId
     *
     * @return int New attribute text's ID
     *
     * @throws Exception
     */
    public function processAttributeText(array $attrText, int $attributeId): int
    {
        if (!isset($this->statements['attributetext'])) {
            $this->statements['attributetext'] =
                $this->conn->prepare('insert into
                    attributetext (title, date_created, date_updated, attribute_id, language_id)
                    values (:title, :dateCreated, :dateUpdated, :attributeId, :languageId)
                    ON DUPLICATE KEY UPDATE title = :title, id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['attributetext'];

        $langId = $this->helper->getLanguageIdByTextItem($attrText);
        if (!$langId) {
            return 0;
        }

        $stmt->bindValue(':title', $attrText['title']);
        $stmt->bindValue(':attributeId', $attributeId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeQuery();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $attrValue
     *
     * @return int New attribute value's ID
     *
     * @throws Exception
     */
    public function processAttributeValue(array $attrValue): int
    {
        if (!isset($this->statements[self::STATEMENT_ATTRIBUTEVALUE])) {
            $this->statements[self::STATEMENT_ATTRIBUTEVALUE] =
                $this->conn->prepare('insert into
                    attributevalue (value, date_created, date_updated)
                    values (:value, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }

        $stmt = &$this->statements[self::STATEMENT_ATTRIBUTEVALUE];

        $parameterType = false === $attrValue[self::FIELD_VALUE] ? ParameterType::BOOLEAN : ParameterType::STRING;
        $stmt->bindValue(':value', $attrValue[self::FIELD_VALUE], $parameterType);
        $this->helper->bindDate($stmt);

        $stmt->executeQuery();

        $attrValueId = (int)$this->conn->lastInsertId();

        foreach ($attrValue[self::FIELD_TRANSLATIONS] as $trans) {
            $this->processAttributeValueTranslations($trans, $attrValueId);
        }

        return $attrValueId;
    }

    /**
     * @param array $attrTranslation
     * @param int $attributeValueId Translations belong to this attribute
     *
     * @return int New value translation's ID
     *
     * @throws Exception
     */
    public function processAttributeValueTranslations(array $attrTranslation, int $attributeValueId): int
    {
        if (!isset($this->statements['attributevaluetranslation'])) {
            $this->statements['attributevaluetranslation'] =
                $this->conn->prepare('insert into
                    attributevaluetranslation (translation, date_created, date_updated, attributevalue_id, language_id)
                    values (:translation, :dateCreated, :dateUpdated, :attributeValueId, :languageId)
                    ON DUPLICATE KEY UPDATE translation = :translation, id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['attributevaluetranslation'];

        $langId = $this->helper->getLanguageIdByTextItem($attrTranslation);
        if (!$langId) {
            return 0;
        }

        $stmt->bindValue(':translation', $attrTranslation['translation']);
        $stmt->bindValue(':attributeValueId', $attributeValueId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeQuery();

        return (int)$this->conn->lastInsertId();
    }
}
