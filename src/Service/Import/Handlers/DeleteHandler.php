<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Exception as DBALException;
use Redhotmagma\ApiBundle\EventListener\EntityListener;

/**
 * @internal
 */
class DeleteHandler
{
    private Connection $conn;

    private ImportHelper $helper;

    public function __construct(Connection $conn, ImportHelper $helper)
    {
        $this->conn = $conn;
        $this->helper = $helper;
    }

    /**
     * Delete items that were not updated.
     *
     * @param \DateTime $date
     *
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function deleteNonExistingItems(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $stmt = $this->conn->prepare('
          UPDATE item
            LEFT JOIN item_attribute ON item.id = item_attribute.item_id
            LEFT JOIN item_itemgroup ON item.id = item_itemgroup.item_id
            LEFT JOIN item_itemgroupentry ON item.id = item_itemgroupentry.item_id
            LEFT JOIN itemprice ON item.id = itemprice.item_id
            LEFT JOIN itemtext ON item.id = itemtext.item_id
            LEFT JOIN item parent on parent.id = item.parent_id
            SET 
                item.date_deleted = :currentDate,
                item_attribute.date_deleted = :currentDate,
                item_itemgroup.date_deleted = :currentDate,
                item_itemgroupentry.date_deleted = :currentDate,
                itemprice.date_deleted = :currentDate,
                itemtext.date_deleted = :currentDate
            WHERE ( item.identifier NOT IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier NOT IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
               item.date_deleted = :dateDeletedDefault AND 
               (item_attribute.date_deleted = :dateDeletedDefault OR item_attribute.date_deleted  IS NULL) AND
               (item_itemgroup.date_deleted = :dateDeletedDefault OR item_itemgroup.date_deleted IS NULL) AND
               (item_itemgroupentry.date_deleted = :dateDeletedDefault OR item_itemgroupentry.date_deleted IS NULL) AND
               (itemprice.date_deleted = :dateDeletedDefault OR itemprice.date_deleted IS NULL) AND
               (itemtext.date_deleted = :dateDeletedDefault OR itemtext.date_deleted IS NULL)
             ');
        $stmt->bindValue(':currentDate', $date->format('Y-m-d H:i:s'));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);

        $stmt->executeStatement();
    }

    /**
     * Delete item prices that were not updated.
     *
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function deleteNonExistingAttributeRelations(
        \DateTime $date,
        bool $deleteWriteProtectedAttributes = false,
        array $itemIdentifiersToImport = []
    ): void {
        $this->deleteNonExistingItemAttributeRelations($date, $deleteWriteProtectedAttributes, $itemIdentifiersToImport);
        $this->deleteNonExistingComponentAttributeRelations($date, $deleteWriteProtectedAttributes, $itemIdentifiersToImport);
        $this->deleteNonExistingOptionAttributeRelations($date, $deleteWriteProtectedAttributes, $itemIdentifiersToImport);
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function deleteNonExistingItemAttributeRelations(
        \DateTime $date,
        bool $deleteWriteProtectedAttributes = false,
        array $itemIdentifiersToImport = []
    ): void {
        $sql = '
            UPDATE item_attribute
                JOIN item ON item.id = item_attribute.item_id
                JOIN attribute ON item_attribute.attribute_id = attribute.id
                LEFT JOIN item parent on parent.id = item.parent_id
                SET 
                    item_attribute.date_deleted = :currentDate 
                WHERE 
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
                   item.date_updated >= :currentDate AND
                   (
                       item_attribute.date_updated < :currentDate OR
                       (
                           item_attribute.date_updated IS NULL AND
                           item_attribute.date_created < :currentDate
                       )
                   ) AND
                   item_attribute.date_deleted = :dateDeletedDefault
            ';
        if (!$deleteWriteProtectedAttributes) {
            $sql .= 'AND attribute.write_protected = false';
        }

        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $stmt->executeStatement();
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function deleteNonExistingComponentAttributeRelations(
        \DateTime $date,
        bool $deleteWriteProtectedAttributes = false,
        array $itemIdentifiersToImport = []
    ): void {
        $sql = '
            UPDATE option_classification_attribute
                JOIN optionclassification ON optionclassification.id = option_classification_attribute.optionclassification_id
                JOIN attribute ON option_classification_attribute.attribute_id = attribute.id
                JOIN item_optionclassification ON item_optionclassification.optionclassification_id = optionclassification.id
                JOIN item on item.id = item_optionclassification.item_id
                            LEFT JOIN item parent on parent.id = item.parent_id
                SET 
                    option_classification_attribute.date_deleted = :currentDate 
                WHERE 
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
                   optionclassification.date_updated >= :currentDate AND
                   (
                       option_classification_attribute.date_updated < :currentDate OR
                       (
                           option_classification_attribute.date_updated IS NULL AND
                           option_classification_attribute.date_created < :currentDate
                       )
                   ) AND
                   option_classification_attribute.date_deleted = :dateDeletedDefault
            ';

        if (!$deleteWriteProtectedAttributes) {
            $sql .= 'AND attribute.write_protected = false';
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $stmt->executeStatement();
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function deleteNonExistingOptionAttributeRelations(
        \DateTime $date,
        bool $deleteWriteProtectedAttributes = false,
        array $itemIdentifiersToImport = []
    ): void {
        $sql = '
            UPDATE option_attribute
                JOIN `option` ON `option`.id = option_attribute.option_id
                JOIN attribute ON option_attribute.attribute_id = attribute.id
                JOIN item_optionclassification_option ON item_optionclassification_option.option_id = `option`.id
                JOIN item_optionclassification ON item_optionclassification.id = item_optionclassification_option.item_optionclassification_id
                JOIN item on item.id = item_optionclassification.item_id                
                LEFT JOIN item parent on parent.id = item.parent_id
            SET 
                    option_attribute.date_deleted = :currentDate 
                WHERE 
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
                   `option`.date_updated >= :currentDate AND
                   (
                       option_attribute.date_updated < :currentDate OR
                       (
                           option_attribute.date_updated IS NULL AND
                           option_attribute.date_created < :currentDate
                       )
                   ) AND
                   option_attribute.date_deleted = :dateDeletedDefault
            ';
        if (!$deleteWriteProtectedAttributes) {
            $sql .= 'AND attribute.write_protected = false';
        }
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $stmt->executeStatement();
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function deleteNonExistingItemClassificationRelations(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $stmt = $this->conn->prepare('
            UPDATE item_itemclassification
                JOIN `item` ON `item`.id = item_itemclassification.item_id
                LEFT JOIN item parent on parent.id = item.parent_id
                SET 
                    item_itemclassification.date_deleted = :currentDate 
                WHERE 
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
                   (
                       item_itemclassification.date_updated < :currentDate OR
                       (
                           item_itemclassification.date_updated IS NULL AND
                           item_itemclassification.date_created < :currentDate
                       )
                   ) AND
                   item_itemclassification.date_deleted = :dateDeletedDefault
             ');
        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $stmt->executeStatement();
    }

    /**
     * Delete item design areas that were not in the current import.
     *
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function deleteNonExistingDesignAreas(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $stmt = $this->conn->prepare('
                UPDATE design_area
                  JOIN item ON design_area.item_id = item.id
                  LEFT JOIN design_area_text ON design_area.id = design_area_text.design_area_id
                  LEFT JOIN design_view_design_area ON design_area.id = design_view_design_area.design_area_id
                  LEFT JOIN creator_view_design_area ON design_area.id = creator_view_design_area.design_area_id    
                  LEFT JOIN design_area_design_production_method ON
                    design_area.id = design_area_design_production_method.design_area_id
                  LEFT JOIN design_area_design_production_method_price ON
                    design_area_design_production_method.id =
                        design_area_design_production_method_price.design_area_design_production_method_id
                LEFT JOIN item parent on parent.id = item.parent_id
                SET
                    design_area.date_deleted = :date,
                    design_area_text.date_deleted = :date,
                    design_view_design_area.date_deleted = :date,
                    creator_view_design_area.date_deleted = :date,
                    design_area_design_production_method.date_deleted = :date,
                    design_area_design_production_method_price.date_deleted = :date
                WHERE
                   item.date_updated >= :date AND
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
                   (
                       design_area.date_updated < :date OR
                       (
                            design_area.date_updated IS NULL AND
                            design_area.date_created < :date
                       )
                   ) 
                  AND design_area.date_deleted = :dateDeleted
                  AND (design_area_text.date_deleted = :dateDeleted OR design_area_text.date_deleted IS NULL)
                  AND (
                    (design_view_design_area.date_deleted = :dateDeleted OR design_view_design_area.date_deleted IS NULL)
                    OR
                    (creator_view_design_area.date_deleted = :dateDeleted OR creator_view_design_area.date_deleted IS NULL)
                  )
                  AND (design_area_design_production_method.date_deleted = :dateDeleted OR design_area_design_production_method.date_deleted IS NULL)
                  AND (design_area_design_production_method_price.date_deleted = :dateDeleted OR design_area_design_production_method_price.date_deleted IS NULL)
             ');

        $stmt->bindValue(':date', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
        $stmt->executeStatement();
    }

    /**
     * Delete design area relations.
     *
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function deleteNonExistingDesignAreaVisualData(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $stmtDesignView = $this->conn->prepare('
        UPDATE design_view
            JOIN item ON item.id = design_view.item_id    
            LEFT JOIN design_view_design_area ON (design_view.id = design_view_design_area.design_view_id)
            LEFT JOIN design_view_text ON (design_view.id = design_view_text.design_view_id)
            LEFT JOIN item parent on parent.id = item.parent_id
            SET 
                design_view_design_area.date_deleted = :currentDate
            WHERE
               item.date_updated >= :currentDate AND
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
               (
                   design_view.date_updated < :currentDate OR
                   (
                       design_view.date_updated IS NULL AND
                       design_view.date_created < :currentDate
                   )
               ) 
              AND design_view.date_deleted = :dateDeletedDefault
              AND (design_view_design_area.date_deleted = :dateDeletedDefault OR design_view_design_area.date_deleted IS NULL)
              AND (design_view_text.date_deleted = :dateDeletedDefault OR design_view_text.date_deleted  IS NULL)
         ');
        $stmtDesignView->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmtDesignView->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $stmtDesignView->executeStatement();

        $stmtCreatorView = $this->conn->prepare('
        UPDATE creator_view
            JOIN item ON item.id = creator_view.item_id    
            LEFT JOIN creator_view_design_area ON (creator_view.id = creator_view_design_area.creator_view_id)
            LEFT JOIN creator_view_text ON (creator_view.id = creator_view_text.creator_view_id)
            LEFT JOIN item parent on parent.id = item.parent_id
            SET 
                creator_view_design_area.date_deleted = :currentDate
            WHERE
               item.date_updated >= :currentDate AND
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
               (
                   creator_view.date_updated < :currentDate OR
                   (
                       creator_view.date_updated IS NULL AND
                       creator_view.date_created < :currentDate
                   )
               ) 
              AND creator_view.date_deleted = :dateDeletedDefault
              AND (creator_view_design_area.date_deleted = :dateDeletedDefault OR creator_view_design_area.date_deleted IS NULL)
              AND (creator_view_text.date_deleted = :dateDeletedDefault OR creator_view_text.date_deleted  IS NULL)
         ');
        $stmtCreatorView->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmtCreatorView->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $stmtCreatorView->executeStatement();
    }

    /**
     * Delete design are prod method prices if not updated.
     *
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function deleteNonExistingDesignAreaDesignProductionMethodPrices(
        \DateTime $date,
        string $importPriceChannel,
        array $itemIdentifiersToImport
    ): void {
        $deleteQueryStr = '
            UPDATE design_area_design_production_method_price AS method_price
            JOIN design_area_design_production_method AS method 
                ON method.id = method_price.design_area_design_production_method_id
            JOIN design_area ON design_area.id = method.design_area_id
            JOIN item ON item.id = design_area.item_id
            LEFT JOIN item parent on parent.id = item.parent_id
            SET 
                method_price.date_deleted = :currentDate
            WHERE 
               item.date_updated >= :currentDate AND
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
               (    
                   method_price.date_updated < :currentDate OR
                   (
                       method_price.date_updated IS NULL AND
                       method_price.date_created < :currentDate
                   )
               ) 
              AND method_price.date_deleted = :dateDeletedDefault
         ';

        // if import is run with "priceChannel" parameter, skip if this isn't the right channel
        $channelId = false;
        if ('' !== $importPriceChannel) {
            $deleteQueryStr .= ' and method_price.channel_id = :channelId';
            $channelId = $this->helper->getChannelByIdentifier($importPriceChannel);
        }

        $stmt = $this->conn->prepare($deleteQueryStr);

        if (false !== $channelId) {
            $stmt->bindValue(':channelId', $channelId);
        }

        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
        $stmt->executeStatement();
    }

    /**
     * Delete design are prod method prices if not updated.
     *
     * @throws DBALException
     * @throws Exception
     */
    public function deleteNonExistingDesignAreaDesignProductionMethod(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $stmt = $this->conn->prepare('
                    UPDATE design_area_design_production_method
                        JOIN design_area ON design_area.id = design_area_design_production_method.design_area_id
                        JOIN item ON item.id = design_area.item_id
                        LEFT JOIN item parent on parent.id = item.parent_id
                        LEFT JOIN design_area_design_production_method_price
                            ON design_area_design_production_method.id =
                               design_area_design_production_method_price.design_area_design_production_method_id
                    SET
                        design_area_design_production_method.date_deleted = :currentDate,
                        design_area_design_production_method_price.date_deleted = :currentDate
                    WHERE
                       item.date_updated >= :currentDate AND
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
                                       design_area.date_updated >= :currentDate AND
                       (
                           design_area_design_production_method.date_updated < :currentDate OR
                           (
                               design_area_design_production_method.date_updated IS NULL AND
                               design_area_design_production_method.date_created < :currentDate
                           )
                       ) 
                      AND design_area_design_production_method.date_deleted = :dateDeletedDefault
                      AND (design_area_design_production_method_price.date_deleted = :dateDeletedDefault OR design_area_design_production_method_price.date_deleted IS NULL)
                     ');

        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);

        $stmt->executeStatement();
    }

    /**
     * Delete item prices that were not updated.
     *
     * @throws DBALException
     * @throws Exception
     */
    public function deleteNonExistingItemPrices(\DateTime $date, string $importPriceChannel, array $itemIdentifiersToImport): void
    {
        $deleteQueryStr = '
            UPDATE itemprice
            JOIN item ON item.id = itemprice.item_id 
            LEFT JOIN item parent on parent.id = item.parent_id
            SET
                itemprice.date_deleted = :currentDate
            WHERE 
                           ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND
               item.date_updated >= :currentDate AND
               (
                   itemprice.date_updated < :currentDate OR 
                   (
                       itemprice.date_updated IS NULL AND
                       itemprice.date_created < :currentDate
                   )
               ) AND
               itemprice.date_deleted = :dateDeletedDefault
             ';

        // if import is run with "priceChannel" parameter, delete only for that channel
        if ('' !== $importPriceChannel) {
            $deleteQueryStr .= ' and itemprice.channel_id = :channelId';
        }

        $stmt = $this->conn->prepare($deleteQueryStr);

        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);

        // if import is run with "priceChannel" parameter, delete only for that channel
        if ('' !== $importPriceChannel) {
            $stmt->bindValue(':channelId', $this->helper->getChannelByIdentifier($importPriceChannel));
        }

        $stmt->executeStatement();
    }

    /**
     * Delete component relations to items.
     *
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function deleteNonExistingComponentRelations(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $deleteQueryStr = '
            UPDATE item_optionclassification
            JOIN item ON item.id = item_optionclassification.item_id 
            LEFT JOIN item parent ON parent.id = item.parent_id
            JOIN item_optionclassification_option 
                ON item_optionclassification.id = item_optionclassification_option.item_optionclassification_id
            LEFT JOIN itemoptionclassificationoptiondeltaprice
                ON item_optionclassification_option.id = itemoptionclassificationoptiondeltaprice.item_optionclassification_option_id
            SET
                item_optionclassification.date_deleted = :currentDate,
                item_optionclassification_option.date_deleted = :currentDate,
                itemoptionclassificationoptiondeltaprice.date_deleted = :currentDate
            WHERE 
               item.date_updated >= :currentDate AND
                ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '"))
                AND
               (
                   item_optionclassification.date_updated < :currentDate OR
                   (
                       item_optionclassification.date_updated IS NULL AND
                       item_optionclassification.date_created < :currentDate
                   )
               ) 
                AND item_optionclassification.date_deleted = :dateDeletedDefault
                AND item_optionclassification_option.date_deleted = :dateDeletedDefault
                AND (itemoptionclassificationoptiondeltaprice.date_deleted = :dateDeletedDefault OR itemoptionclassificationoptiondeltaprice.date_deleted IS NULL)
             ';
        $stmt = $this->conn->prepare($deleteQueryStr);

        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);

        $stmt->executeStatement();
    }

    /**
     * Delete component relations to items.
     *
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function deleteNonExistingOptionRelations(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $deleteQueryStr = '
            UPDATE item_optionclassification_option
                JOIN item_optionclassification ON item_optionclassification_option.item_optionclassification_id = item_optionclassification.id
                JOIN item ON item.id = item_optionclassification.item_id 
                LEFT JOIN item parent ON parent.id = item.parent_id
                LEFT JOIN itemoptionclassificationoptiondeltaprice ON item_optionclassification_option.id = itemoptionclassificationoptiondeltaprice.item_optionclassification_option_id
            SET
                item_optionclassification_option.date_deleted = :currentDate,
                itemoptionclassificationoptiondeltaprice.date_deleted = :currentDate
            WHERE 
               item.date_updated >= :currentDate AND
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND            
                   (
                   item_optionclassification_option.date_updated < :currentDate OR 
                   (
                       item_optionclassification_option.date_updated IS NULL AND 
                       item_optionclassification_option.date_created < :currentDate
                   )
               ) 
               AND item_optionclassification_option.date_deleted = :dateDeletedDefault
               AND (itemoptionclassificationoptiondeltaprice.date_deleted = :dateDeletedDefault OR itemoptionclassificationoptiondeltaprice.date_deleted IS NULL)
             ';

        $stmt = $this->conn->prepare($deleteQueryStr);

        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);

        $stmt->executeStatement();
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     * @throws Exception
     */
    public function deleteNonExistingOptionDeltaPrices(\DateTime $date, array $itemIdentifiersToImport): void
    {
        $deleteQueryStr = '
            UPDATE itemoptionclassificationoptiondeltaprice
                JOIN item_optionclassification_option ON itemoptionclassificationoptiondeltaprice.item_optionclassification_option_id = item_optionclassification_option.id
                JOIN item_optionclassification ON item_optionclassification_option.item_optionclassification_id = item_optionclassification.id
                JOIN item ON item.id = item_optionclassification.item_id 
                LEFT JOIN item parent ON parent.id = item.parent_id
            SET
                itemoptionclassificationoptiondeltaprice.date_deleted = :currentDate
            WHERE 
               item.date_updated >= :currentDate AND
                  ( item.identifier IN("' . implode('","', $itemIdentifiersToImport) . '") 
                OR parent.identifier IN("' . implode('","', $itemIdentifiersToImport) . '")) AND 
               (
                   itemoptionclassificationoptiondeltaprice.date_updated < :currentDate OR 
                   (
                       itemoptionclassificationoptiondeltaprice.date_updated IS NULL AND 
                       itemoptionclassificationoptiondeltaprice.date_created < :currentDate
                   )
               ) 
               AND itemoptionclassificationoptiondeltaprice.date_deleted = :dateDeletedDefault
             ';

        $stmt = $this->conn->prepare($deleteQueryStr);

        $stmt->bindValue(':currentDate', $date->format(ImportHelper::DATE_FORMAT_DEFAULT));
        $stmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);

        $stmt->executeStatement();
    }
}
