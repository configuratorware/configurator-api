<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\DBAL\Statement;
use Psr\Log\LoggerInterface;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ColorChecker;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;

/**
 * @internal
 */
class DesignHandler
{
    /**
     * @var Connection
     */
    private $conn;

    /**
     * Storage for prepared statements.
     *
     * @var Statement[]
     */
    private $statements = [];

    /**
     * @var ImportHelper
     */
    private $helper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ColorChecker
     */
    private $colorChecker;

    /**
     * Store sequences here for re-using them.
     *
     * @var array
     */
    private $sequences = [];

    public function __construct(
        Connection $conn,
        ImportHelper $helper,
        ImportLogger $logger,
        ColorChecker $colorChecker
    ) {
        $this->conn = $conn;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->colorChecker = $colorChecker;
    }

    /**
     * @param array $designArea
     * @param int $itemId
     *
     * @return int
     *
     * @throws DBALException
     */
    public function processDesignArea(array $designArea, int $itemId): int
    {
        $this->prepareDesignAreaStatements();
        $this->prepareColorSelectionFunctions();

        $stmtSelect = $this->statements['designarea_select'];

        $stmtSelect->bindValue(':identifier', trim((string)$designArea['designAreaIdentifier']));
        $stmtSelect->bindValue(':itemId', $itemId);
        $stmtSelect->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);

        $result = $stmtSelect->executeQuery();

        if ($result->rowCount()) {
            $row = $result->fetchAssociative();
            if ($row) {
                $areaId = (int)$row['id'];
                $sequenceNumber = (int)$row['sequence_number'];

                $stmtUpdate = $this->statements['designarea_update'];
                $stmtUpdate->bindValue(':id', $areaId);
                $stmtUpdate->bindValue(':height', (int)$designArea['heightMillimeter']);
                $stmtUpdate->bindValue(':width', (int)$designArea['widthMillimeter']);
                $stmtUpdate->bindValue(
                    ':sequenceNumber',
                    isset($designArea['sequenceNumber']) ? (int)$designArea['sequenceNumber'] : $sequenceNumber
                );
                $stmtUpdate->bindValue(':dateUpdated', (new \DateTime())->format('Y-m-d H:i:s'));
                $this->helper->bindCustomData($stmtUpdate, $designArea['customData']);
                $stmtUpdate->executeStatement();

                return $areaId;
            }
        }

        $stmtInsert = $this->statements['designarea_insert'];
        $stmtInsert->bindValue(':identifier', trim((string)$designArea['designAreaIdentifier']));
        $stmtInsert->bindValue(':height', (int)$designArea['heightMillimeter']);
        $stmtInsert->bindValue(':width', (int)$designArea['widthMillimeter']);
        $stmtInsert->bindValue(
            ':sequenceNumber',
            isset($designArea['sequenceNumber']) ? (int)$designArea['sequenceNumber'] : $this->getNextDesignAreaSequenceByItemId($itemId)
        );
        $stmtInsert->bindValue(':itemId', $itemId);
        $this->helper->bindCustomData($stmtInsert, $designArea['customData']);
        $this->helper->bindDate($stmtInsert);
        $stmtInsert->executeStatement();

        $areaId = (int)$this->conn->lastInsertId();

        return $areaId;
    }

    /**
     * @param int $itemId
     *
     * @return int
     *
     * @throws DBALException
     */
    public function getNextDesignAreaSequenceByItemId(int $itemId): int
    {
        if (!isset($this->statements['designarea_seq'])) {
            $this->statements['designarea_seq'] =
                $this->conn->prepare('SELECT max(sequence_number) AS sequence_number
                  FROM design_area
                  WHERE item_id = :itemId
                  GROUP BY item_id');
        }

        // return next sequence, if already stored
        if (isset($this->sequences['designarea_seq'][$itemId])) {
            return ++$this->sequences['designarea_seq'][$itemId];
        }

        // get next sequence, if not stored yet
        $stmt = $this->statements['designarea_seq'];
        $stmt->bindValue(':itemId', $itemId);
        $result = $stmt->executeQuery();
        $seqNumber = $result->fetchFirstColumn();
        $seqNumber = $seqNumber[0] ?? 0;

        return $this->sequences['designarea_seq'][$itemId] = (int)$seqNumber + 1;
    }

    /**
     * @param array $areaText
     * @param int $areaId
     *
     * @return int
     *
     * @throws DBALException
     */
    public function processDesignAreaText(array $areaText, int $areaId): int
    {
        if (!isset($this->statements['designareatext'])) {
            $this->statements['designareatext'] =
                $this->conn->prepare('INSERT INTO
                    design_area_text (title, date_created, date_updated, design_area_id, language_id)
                    VALUES (:title, :dateCreated, :dateUpdated, :areaId, :languageId)
                    ON DUPLICATE KEY UPDATE title = :title, id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['designareatext'];

        $langId = $this->helper->getLanguageIdByTextItem($areaText);
        if (!$langId) {
            return 0;
        }

        $stmt->bindValue(':title', $areaText['title']);
        $stmt->bindValue(':areaId', $areaId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $visualData
     * @param int $itemId
     *
     * @return int
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    public function processDesignAreaViewData(array $visualData, int $itemId): int
    {
        $this->prepareDesignViewStatements();
        $stmtSelect = &$this->statements['designareavisualdata_select'];
        $stmtInsert = &$this->statements['designareavisualdata_insert'];
        $stmtUpdate = &$this->statements['designareavisualdata_update'];

        $stmtSelect->bindValue(':identifier', trim((string)$visualData['designViewIdentifier']));
        $stmtSelect->bindValue(':itemId', $itemId);
        $stmtSelect->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
        $result = $stmtSelect->executeQuery();

        if ($result->rowCount()) {
            $viewId = $result->fetchFirstColumn();
            $viewId = $viewId[0] ?? 0;
            $stmtUpdate->bindValue(':id', $viewId);
            $stmtUpdate->bindValue(':dateUpdated', (new \DateTime())->format('Y-m-d H:i:s'));
            $this->helper->bindCustomData($stmtUpdate, $visualData['customData']);
            $stmtUpdate->executeStatement();

            return $viewId;
        }

        $stmtInsert->bindValue(':identifier', trim((string)$visualData['designViewIdentifier']));
        $stmtInsert->bindValue(':sequenceNumber', $this->getNextDesignViewSequenceByItemId($itemId));
        $stmtInsert->bindValue(':itemId', $itemId);
        $this->helper->bindCustomData($stmtInsert, $visualData['customData']);
        $this->helper->bindDate($stmtInsert);
        $stmtInsert->executeStatement();
        $viewId = (int)$this->conn->lastInsertId();

        return $viewId;
    }

    /**
     * @param int $itemId
     *
     * @return int
     *
     * @throws DBALException
     */
    public function getNextDesignViewSequenceByItemId(int $itemId): int
    {
        if (!isset($this->statements['designview_seq'])) {
            $this->statements['designview_seq'] =
                $this->conn->prepare('SELECT max(sequence_number) AS sequence_number
                  FROM design_view
                  WHERE item_id = :itemId
                  GROUP BY item_id');
        }

        // return next sequence, if already stored
        if (!isset($this->sequences['designarea'][$itemId])) {
            // get next sequence, if not stored yet
            $stmt = $this->statements['designview_seq'];
            $stmt->bindValue(':itemId', $itemId);
            $result = $stmt->executeQuery();
            $this->sequences['designarea'][$itemId] = $result->fetchFirstColumn()[0] ?? 0;
        }

        return ++$this->sequences['designarea'][$itemId];
    }

    /**
     * @param array $visualData
     * @param int $areaId
     * @param int $viewId
     *
     * @return int New ID
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function processDesignViewDesignArea(array $visualData, int $areaId, int $viewId): int
    {
        if (!isset($this->statements['designviewdesignarea'])) {
            $this->statements['designviewdesignarea'] =
                $this->conn->prepare('INSERT INTO
                    design_view_design_area (position, custom_data, design_view_id, design_area_id, 
                                             date_created, date_updated, base_shape, is_default)
                    VALUES (:position, :customData, :designViewId, :designAreaId,
                            :dateCreated, :dateUpdated, :baseShape, :isDefault)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated,
                                            base_shape = IFNULL(:baseShape, base_shape), 
                                            position = IFNULL(:position, position),
                                            is_default = IFNULL(:isDefault, is_default);');
        }
        $stmt = &$this->statements['designviewdesignarea'];

        $positionData = $this->collectPositionData($visualData);

        $isDefault = (isset($visualData['default']) && true === $visualData['default']);
        if ($isDefault) {
            // if this will be a default, reset all sibling views (including this if present) before running this
            $this->resetDefaultDesignViewDesignArea($areaId);
        }

        $stmt->bindValue(':designViewId', $viewId);
        $stmt->bindValue(':designAreaId', $areaId);
        $stmt->bindValue(':isDefault', (int)$isDefault);
        $stmt->bindValue(':position', empty($positionData) ? null : json_encode($positionData));
        $stmt->bindValue(':baseShape', $this->createBaseShapeData($visualData, $positionData));
        $this->helper->bindCustomData($stmt, $visualData['customData']);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $productionMethod
     * @param int $areaId
     * @param int $methodId
     *
     * @return int
     *
     * @throws DBALException
     */
    public function processDesignAreaDesignProductionMethods(array $productionMethod, int $areaId, int $methodId): int
    {
        if (!isset($this->statements['designareaproductionmethods'])) {
            $this->statements['designareaproductionmethods'] =
                $this->conn->prepare('INSERT INTO
                    design_area_design_production_method (
                          width, height, allow_bulk_names, minimum_order_amount,
                          design_area_id, design_production_method_id, default_colors,
                          design_elements_locked, max_elements, max_images, max_texts,
                          one_line_text, date_created, date_updated)
                    VALUES (:width, :height, :allowBulkNames, :minimumOrderAmount,
                            :designAreaId, :designProductionMethodId, :defaultColors,
                            :designElementsLocked, :maxElements, :maxImages, :maxTexts,
                            :oneLineText, :dateUpdated, :dateCreated)
                    ON DUPLICATE KEY UPDATE 
                        id = LAST_INSERT_ID(id),
                        date_updated = :dateUpdated,
                        default_colors = :defaultColors,
                        width = :width,
                        height = :height,
                        design_elements_locked = :designElementsLocked,
                        max_elements = :maxElements,
                        max_images = :maxImages,
                        max_texts = :maxTexts,
                        minimum_order_amount = :minimumOrderAmount
                ;');
        }
        $stmt = &$this->statements['designareaproductionmethods'];
        // parse data and search default color (if present) and convert to json
        $defaultColors = $this->parseDefaultColors($productionMethod, $methodId);
        $defaultColors = !empty($defaultColors) ? json_encode($defaultColors) : null;

        $stmt->bindValue(':width', $productionMethod['widthMillimeter']);
        $stmt->bindValue(':height', $productionMethod['heightMillimeter']);
        $stmt->bindValue(
            ':allowBulkNames',
            isset($productionMethod['allowBulkNames']) && true == $productionMethod['allowBulkNames'] ? 1 : 0
        );
        $stmt->bindValue(':designProductionMethodId', $methodId);
        $stmt->bindValue(':designAreaId', $areaId);
        $stmt->bindValue(':defaultColors', $defaultColors);
        $stmt->bindValue(':minimumOrderAmount', $productionMethod['minimumOrderAmount'] ?? null);
        $stmt->bindValue(
            ':designElementsLocked',
            isset($productionMethod['designElementsLocked']) && true == $productionMethod['designElementsLocked'] ? 1 : 0
        );
        $stmt->bindValue(':maxElements', $productionMethod['maxElements'] ?? null);
        $stmt->bindValue(':maxImages', $productionMethod['maxImages'] ?? null);
        $stmt->bindValue(':maxTexts', $productionMethod['maxTexts'] ?? null);
        $stmt->bindValue(
            ':oneLineText',
            isset($productionMethod['oneLineText']) && true == $productionMethod['oneLineText'] ? 1 : 0
        );
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $viewText
     * @param int $viewId
     *
     * @return int
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function processDesignViewText(array $viewText, int $viewId): int
    {
        if (!isset($this->statements['designviewtext'])) {
            $this->statements['designviewtext'] =
                $this->conn->prepare('INSERT INTO
                    design_view_text (title, date_created, date_updated, design_view_id, language_id)
                    VALUES (:title, :dateCreated, :dateUpdated, :viewId, :languageId)
                    ON DUPLICATE KEY UPDATE title = :title, id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['designviewtext'];

        $langId = $this->helper->getLanguageIdByTextItem($viewText);
        if (!$langId) {
            return 0;
        }

        $stmt->bindValue(':title', $viewText['title']);
        $stmt->bindValue(':viewId', $viewId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $visualData
     * @param int $areaId
     * @param int $itemId
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function processDesignAreaCreatorViewData(array $visualData, int $areaId, int $itemId)
    {
        $creatorViewId = $this->processCreatorView($visualData, $itemId);

        foreach ($visualData['texts'] as $text) {
            $this->processCreatorViewText($text, $creatorViewId);
        }

        $this->processCreatorViewDesignArea($visualData, $areaId, $creatorViewId);
    }

    /**
     * @param array $visualData
     * @param int $itemId
     *
     * @return int
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function processCreatorView(array $visualData, int $itemId): int
    {
        if (!isset($this->statements['creatorviewdesignareavisualdata_select'])) {
            $this->statements['creatorviewdesignareavisualdata_select'] =
                $this->conn->prepare('SELECT id FROM creator_view WHERE
                              identifier = :identifier AND item_id = :itemId AND date_deleted = :dateDeleted;');
        }
        if (!isset($this->statements['creatorviewdesignareavisualdata_insert'])) {
            $this->statements['creatorviewdesignareavisualdata_insert'] =
                $this->conn->prepare('INSERT INTO
                    creator_view (identifier, sequence_number, custom_data, date_created, date_updated, item_id)
                    VALUES (:identifier, :sequenceNumber, :customData, :dateCreated, :dateUpdated, :itemId)');
        }
        if (!isset($this->statements['creatorviewdesignareavisualdata_update'])) {
            $this->statements['creatorviewdesignareavisualdata_update'] =
                $this->conn->prepare('UPDATE creator_view SET
                    custom_data = :customData, date_updated = :dateUpdated
                    WHERE id = :id;');
        }
        $stmtSelect = &$this->statements['creatorviewdesignareavisualdata_select'];
        $stmtInsert = &$this->statements['creatorviewdesignareavisualdata_insert'];
        $stmtUpdate = &$this->statements['creatorviewdesignareavisualdata_update'];

        $stmtSelect->bindValue(':identifier', trim((string)$visualData['designViewIdentifier']));
        $stmtSelect->bindValue(':itemId', $itemId);
        $stmtSelect->bindValue(':dateDeleted', EntityListener::DATE_DELETED_DEFAULT);
        $result = $stmtSelect->executeQuery();
        if ($result->rowCount()) {
            $viewId = (int)$result->fetchFirstColumn()[0];

            $stmtUpdate->bindValue(':id', $viewId);
            $stmtUpdate->bindValue(':dateUpdated', (new \DateTime())->format('Y-m-d H:i:s'));
            $this->helper->bindCustomData($stmtUpdate, $visualData['customData']);
            $stmtUpdate->executeStatement();

            return $viewId;
        }

        $stmtInsert->bindValue(':identifier', trim((string)$visualData['designViewIdentifier']));
        $stmtInsert->bindValue(':sequenceNumber', $this->getNextCreatorViewSequenceByItemId($itemId));
        $stmtInsert->bindValue(':itemId', $itemId);
        $this->helper->bindCustomData($stmtInsert, $visualData['customData']);
        $this->helper->bindDate($stmtInsert);

        $stmtInsert->executeStatement();
        $viewId = (int)$this->conn->lastInsertId();

        return $viewId;
    }

    /**
     * @param array $viewText
     * @param int $creatorViewId
     *
     * @return int
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function processCreatorViewText(array $viewText, int $creatorViewId): int
    {
        if (!isset($this->statements['creatorviewtext'])) {
            $this->statements['creatorviewtext'] =
                $this->conn->prepare('INSERT INTO
                    creator_view_text (title, date_created, date_updated, creator_view_id, language_id)
                    VALUES (:title, :dateCreated, :dateUpdated, :viewId, :languageId)
                    ON DUPLICATE KEY UPDATE title = :title, id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['creatorviewtext'];

        $langId = $this->helper->getLanguageIdByTextItem($viewText);
        if (!$langId) {
            return 0;
        }

        $stmt->bindValue(':title', $viewText['title']);
        $stmt->bindValue(':viewId', $creatorViewId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $visualData
     * @param int $areaId
     * @param int $creatorViewId
     *
     * @return int New ID
     *
     * @throws DBALException
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function processCreatorViewDesignArea(array $visualData, int $areaId, int $creatorViewId): int
    {
        if (!isset($this->statements['creatorviewdesignarea'])) {
            $this->statements['creatorviewdesignarea'] =
                $this->conn->prepare('INSERT INTO
                    creator_view_design_area (position, custom_data, creator_view_id, design_area_id, 
                                             date_created, date_updated, base_shape, is_default)
                    VALUES (:position, :customData, :creatorViewId, :designAreaId,
                            :dateCreated, :dateUpdated, :baseShape, :isDefault)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), date_updated = :dateUpdated,
                                            base_shape = IFNULL(:baseShape, base_shape), 
                                            position = IFNULL(:position, position),
                                            is_default = IFNULL(:isDefault, is_default);');
        }
        $stmt = &$this->statements['creatorviewdesignarea'];

        $positionData = $this->collectPositionData($visualData);

        $isDefault = (isset($visualData['default']) && true === $visualData['default']);
        if ($isDefault) {
            // if this will be a default, reset all sibling views (including this if present) before running this
            $this->resetDefaultCreatorViewDesignArea($areaId);
        }

        $stmt->bindValue(':creatorViewId', $creatorViewId);
        $stmt->bindValue(':designAreaId', $areaId);
        $stmt->bindValue(':isDefault', (int)$isDefault);
        $stmt->bindValue(':position', empty($positionData) ? null : json_encode($positionData));
        $stmt->bindValue(':baseShape', $this->createBaseShapeData($visualData, $positionData));
        $this->helper->bindCustomData($stmt, $visualData['customData']);
        $this->helper->bindDate($stmt);
        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param int $itemId
     *
     * @return string
     *
     * @throws DBALException
     */
    private function getNextCreatorViewSequenceByItemId(int $itemId): string
    {
        if (!isset($this->statements['creatorview_seq'])) {
            $this->statements['creatorview_seq'] =
                $this->conn->prepare('SELECT MAX(CAST(sequence_number as UNSIGNED)) AS sequence_number
                  FROM creator_view
                  WHERE item_id = :itemId
                  GROUP BY item_id');
        }

        // return next sequence, if already stored
        if (!isset($this->sequences['creatorviewdesignarea'][$itemId])) {
            // get next sequence, if not stored yet
            $stmt = $this->statements['creatorview_seq'];
            $stmt->bindValue(':itemId', $itemId);
            $result = $stmt->executeQuery();
            $seqNumber = $result->fetchFirstColumn();
            $seqNumber = $seqNumber[0] ?? 0;
            $this->sequences['creatorviewdesignarea'][$itemId] = (int)$seqNumber;
        }

        return (string) ++$this->sequences['creatorviewdesignarea'][$itemId];
    }

    /**
     * Reset this area's design view relations to non-default.
     *
     * @param int $designAreaId
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function resetDefaultCreatorViewDesignArea(int $designAreaId): void
    {
        if (!isset($this->statements['defaultcreatorviewdesignarea'])) {
            $this->statements['defaultcreatorviewdesignarea'] =
                $this->conn->prepare('
                    UPDATE creator_view_design_area
                    SET is_default = 0
                    WHERE design_area_id = :designAreaId;'
                );
        }

        $stmt = &$this->statements['defaultcreatorviewdesignarea'];
        $stmt->bindValue(':designAreaId', $designAreaId);

        $stmt->executeStatement();
    }

    /**
     * @param array $productionMethod
     * @param int $areaId
     * @param int $itemId
     * @param string $importPriceChannel
     *
     * @throws DBALException
     * @SuppressWarnings(PHPMD.LongVariable)
     */
    public function processDesignProductionMethods(array $productionMethod, int $areaId, int $itemId, string $importPriceChannel)
    {
        $designProdMethodId = $this->helper
            ->getProductionMethodByIdentifier($productionMethod['designProductionMethodIdentifier']);

        if ($designProdMethodId) {
            $designAreaDesignProdMethodId = $this->processDesignAreaDesignProductionMethods($productionMethod, $areaId, $designProdMethodId);

            // store if this method is a default one for this area/item (overwrite if already stored)
            if (isset($productionMethod['default']) && true == $productionMethod['default']) {
                $this->storeDefaultDesignAreDesignProductionMethod(
                    $itemId,
                    $designAreaDesignProdMethodId
                );
            }

            if (isset($productionMethod['prices']) && is_iterable($productionMethod['prices'])) {
                foreach ($productionMethod['prices'] as $price) {
                    $this->processDesignAreaDesignProductionMethodPrice(
                        $price,
                        $designProdMethodId,
                        $designAreaDesignProdMethodId,
                        $importPriceChannel
                    );
                }
            } else {
                $this->logger->notice('Production method "' .
                    $productionMethod['designProductionMethodIdentifier'] . '" prices are empty');
            }
        } else {
            $this->logger->error('Production method "' .
                $productionMethod['designProductionMethodIdentifier'] . '" does not exist');
        }
    }

    /**
     * Process the whole price attribute of the import data.
     *
     * @param array $price
     * @param int $designProdMethodId
     * @param int $designAreaDesignProdMethodId
     * @param string $importPriceChannel
     *
     * @throws DBALException
     * @SuppressWarnings(PHPMD.LongVariable)
     */
    private function processDesignAreaDesignProductionMethodPrice(array $price, int $designProdMethodId, int $designAreaDesignProdMethodId, string $importPriceChannel)
    {
        // if import is run with "priceChannel" parameter, skip if this isn't the right channel
        if ('' !== $importPriceChannel && $importPriceChannel !== $price['channelIdentifier']) {
            return;
        }

        $channelId = $this->helper->getChannelByIdentifier($price['channelIdentifier']);
        if (!$channelId) {
            // do not import this price if the channel was not found
            return;
        }

        foreach ($price['calculationTypes'] as $calcType) {
            $calcTypeId = $this->helper->getDesignerProdCalculationTypeByIdentifier((string)$calcType['typeIdentifier'], $designProdMethodId);

            if (!$calcTypeId) {
                $this->logger->error('Calculation type "' . $calcType['typeIdentifier'] . '" not found for prod method price.');

                continue;
            }

            foreach ($calcType['bulkPrices'] as $bulkPrice) {
                $this->processDesignAreaDesignProdMethodBulkPrice(
                    $bulkPrice,
                    $designAreaDesignProdMethodId,
                    $calcTypeId,
                    $channelId
                );
            }
        }
    }

    /**
     * Process the single price items of the import data.
     *
     * @param array $bulkPrice
     * @param int $designAreaDesignMethodId
     * @param int $calcTypeId
     * @param int $channelId
     *
     * @return int
     *
     * @throws DBALException
     *
     * @SuppressWarnings(PHPMD.LongVariable)
     */
    public function processDesignAreaDesignProdMethodBulkPrice(
        array $bulkPrice,
        int $designAreaDesignMethodId,
        int $calcTypeId,
        int $channelId
    ) {
        if (!isset($this->statements['designproductionmethodprice'])) {
            $this->statements['designproductionmethodprice'] =
                $this->conn->prepare('INSERT INTO
                    design_area_design_production_method_price (
                        price, price_net, channel_id, designer_production_calculation_type_id,
                        amount_from, color_amount_from, 
                        design_area_design_production_method_id, date_created, date_updated)
                    VALUES (:price, :priceNet, :channelId, :prodCalcTypeId, :amountFrom, :colorAmountFrom,
                            :methodId, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE  price = :price, price_net = :priceNet, id = LAST_INSERT_ID(id),
                        date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['designproductionmethodprice'];

        $stmt->bindValue(':price', $bulkPrice['price']);
        $stmt->bindValue(':priceNet', $bulkPrice['priceNet']);
        $stmt->bindValue(':amountFrom', $bulkPrice['itemAmountFrom'] ?? 1);
        $stmt->bindValue(':colorAmountFrom', $bulkPrice['colorAmountFrom'] ?? 1);
        $stmt->bindValue(':channelId', $channelId);
        $stmt->bindValue(':prodCalcTypeId', $calcTypeId);
        $stmt->bindValue(':methodId', $designAreaDesignMethodId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        $priceId = (int)$this->conn->lastInsertId();

        return $priceId;
    }

    /**
     * @param int $itemId
     * @param int $designAreaDesignProductionMethodId
     *
     * @throws DBALException
     */
    public function storeDefaultDesignAreDesignProductionMethod(int $itemId, int $designAreaDesignProductionMethodId)
    {
        if (!isset($this->statements['defaultdesignareadesignproductionmethod'])) {
            $this->statements['defaultdesignareadesignproductionmethod'] =
                $this->conn->prepare('
                    UPDATE design_area_design_production_method
                    INNER JOIN design_area ON design_area.id = design_area_design_production_method.design_area_id
                    SET is_default = 
                        (CASE WHEN design_area_design_production_method.id = :designAreaDesignProductionMethodId 
                            THEN 1 ELSE 0 END)
                    WHERE design_area.item_id = :itemId;'
                );
        }

        $stmt = &$this->statements['defaultdesignareadesignproductionmethod'];
        $stmt->bindValue(':designAreaDesignProductionMethodId', $designAreaDesignProductionMethodId);
        $stmt->bindValue(':itemId', $itemId);

        $stmt->executeStatement();
    }

    /**
     * Create and store prepares statements for design area.
     *
     * @throws DBALException
     */
    private function prepareDesignAreaStatements()
    {
        if (!isset($this->statements['designarea_select'])) {
            $this->statements['designarea_select'] =
                $this->conn->prepare('SELECT id, sequence_number FROM design_area 
                  WHERE identifier = :identifier AND item_id = :itemId AND date_deleted = :dateDeleted');
        }
        if (!isset($this->statements['designarea_insert'])) {
            $this->statements['designarea_insert'] =
                $this->conn->prepare('INSERT INTO
                    design_area 
                      (identifier, height, width, sequence_number, custom_data, item_id, date_created, date_updated)
                    VALUES 
                      (:identifier, :height, :width, :sequenceNumber, :customData, :itemId, :dateCreated, :dateUpdated)'
                );
        }
        if (!isset($this->statements['designarea_update'])) {
            $this->statements['designarea_update'] =
                $this->conn->prepare('
                  UPDATE design_area
                    SET height          = :height,
                        width           = :width, 
                        sequence_number = :sequenceNumber, 
                        custom_data     = :customData,
                        date_updated    = :dateUpdated
                    WHERE id = :id'
                );
        }
        if (!isset($this->statements['visualizationBackgroundColorsSelect'])) {
            $this->statements['visualizationBackgroundColorsSelect'] =
                $this->conn->prepare('
                    SELECT additional_data, design_production_method_id FROM design_area_design_production_method
                    WHERE design_area_id = :designAreaId;'
                );
        }
        if (!isset($this->statements['visualizationBackgroundColorsUpdate'])) {
            $this->statements['visualizationBackgroundColorsUpdate'] =
                $this->conn->prepare('
                    UPDATE design_area_design_production_method
                    SET additional_data = :additionalData
                    WHERE design_area_id = :designAreaId AND design_production_method_id = :methodId;'
                );
        }
    }

    /**
     * Create and store prepared statements for DesignViewes.
     *
     * @param string $viewTable
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function prepareDesignViewStatements()
    {
        if (!isset($this->statements['designareavisualdata_select'])) {
            $this->statements['designareavisualdata_select'] =
                $this->conn->prepare('SELECT id FROM design_view WHERE
                              identifier = :identifier AND item_id = :itemId AND date_deleted = :dateDeleted;');
        }
        if (!isset($this->statements['designareavisualdata_insert'])) {
            $this->statements['designareavisualdata_insert'] =
                $this->conn->prepare('INSERT INTO
                    design_view (identifier, sequence_number, custom_data, date_created, date_updated, item_id)
                    VALUES (:identifier, :sequenceNumber, :customData, :dateCreated, :dateUpdated, :itemId)');
        }
        if (!isset($this->statements['designareavisualdata_update'])) {
            $this->statements['designareavisualdata_update'] =
                $this->conn->prepare('UPDATE design_view SET
                    custom_data = :customData, date_updated = :dateUpdated
                    WHERE id = :id;');
        }
    }

    /**
     * Get valid baseShape data from import.
     *
     * @param array $visualData
     * @param array $positionData
     *
     * @return string
     */
    private function createBaseShapeData(array $visualData, array $positionData): ?string
    {
        // no base shape required if no position data present
        if (empty($positionData)) {
            return null;
        }

        // set default base shape data and overwrite if present in import
        $baseShape = ['type' => 'Plane'];
        if (!empty($visualData['baseShape'])) {
            $jsonArray = json_decode($visualData['baseShape'], true);
            if (is_array($jsonArray)) {
                $baseShape = array_merge($baseShape, $jsonArray);
            }
        }

        return json_encode($baseShape);
    }

    /**
     * @param array $visualData
     *
     * @return array
     */
    private function collectPositionData(array $visualData): array
    {
        $positionData = [];

        // check and extract position data
        if (isset($visualData['position'])) {
            if (!is_array($visualData['position'])) { // this should be an array of x, y coordinates
                $this->logger->error('Position data is not an array!', [$visualData['position']]);
            }
            if (isset($visualData['position']['x'])) {
                $positionData['x'] = $visualData['position']['x'];
            }
            if (isset($visualData['position']['y'])) {
                $positionData['y'] = $visualData['position']['y'];
            }
        }

        // check and extract width and height data
        if (isset($visualData['widthPixels'])) {
            $positionData['width'] = $visualData['widthPixels'];
        }
        if (isset($visualData['heightPixels'])) {
            $positionData['height'] = $visualData['heightPixels'];
        }

        // raise an error if some position data is missing
        if (!empty($positionData) && array_keys($positionData) !== ['x', 'y', 'width', 'height']) {
            $this->logger->error('Position data has missing elements!', $positionData);
        }

        return $positionData;
    }

    /**
     * Pare default color field and build color info array.
     *
     * @param array $productionMethod
     * @param int $methodId
     *
     * @return array
     *
     * @throws DBALException
     */
    private function parseDefaultColors(array $productionMethod, int $methodId): array
    {
        $defaultColorsData = [];
        if (isset($productionMethod['defaultColors']) && is_array($productionMethod['defaultColors'])) {
            foreach ($productionMethod['defaultColors'] as $defaultColor) {
                if ($this->isDefaultColorValidFormat($defaultColor)) {
                    $foundColor = $this->findColor(
                        (string)$defaultColor['color'],
                        $methodId,
                        isset($defaultColor['paletteIdentifier']) ?
                            (string)$defaultColor['paletteIdentifier'] : null
                    );
                    if ($foundColor) {
                        $defaultColorsData[$defaultColor['itemIdentifier']] = [
                            $foundColor['paletteIdentifier'] => $foundColor['colorIdentifier'],
                        ];
                    }
                }
            }
        }

        return $defaultColorsData;
    }

    /**
     * Is default color in valid format?
     *
     * @param $defaultColor
     *
     * @return bool
     */
    private function isDefaultColorValidFormat($defaultColor): bool
    {
        if (!is_array($defaultColor) || !isset($defaultColor['itemIdentifier']) || !isset($defaultColor['color'])) {
            $this->logger->notice('Default color format is invalid');

            return false;
        }

        return true;
    }

    /**
     * Process color info and find matching color.
     *
     * @param string $colorString
     * @param int $methodId
     * @param string|null $paletteIdentifier
     *
     * @return array|null
     *
     * @throws DBALException
     */
    private function findColor(string $colorString, int $methodId, ?string $paletteIdentifier): ?array
    {
        if ($paletteIdentifier) {
            // use palette identifier and color name to search for colors
            $colorSelectStmt = $this->statements['selectcolorbyidentifierpalette'];
            $colorSelectStmt->bindValue(':paletteIdentifier', $paletteIdentifier);
            $colorSelectStmt->bindValue(':colorIdentifier', $colorString);
            $colorSelectStmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
            $colorSelectStmt->bindValue(':methodId', $methodId);
            $result = $colorSelectStmt->executeQuery();

            return $result->fetchAssociative() ?: null;
        }

        if ($this->colorChecker->isHexColor($colorString)) {
            // use hex value to search for colors
            $hexSelectStmt = $this->statements['selectcolorbyhex'];
            $hexSelectStmt->bindValue(
                ':hexValue',
                $this->colorChecker->formatHexColor($colorString, false)
            );
            $hexSelectStmt->bindValue(':dateDeletedDefault', EntityListener::DATE_DELETED_DEFAULT);
            $hexSelectStmt->bindValue(':methodId', $methodId);
            $result = $hexSelectStmt->executeQuery();

            return $result->fetchAssociative() ?: null;
        }

        return null;
    }

    /**
     * Prepare SQL statements for color selection.
     */
    private function prepareColorSelectionFunctions()
    {
        // select by hex color value
        if (!isset($this->statements['selectcolorbyhex'])) {
            $this->statements['selectcolorbyhex'] =
                $this->conn->prepare('
                    SELECT color_palette.identifier AS paletteIdentifier, color_palette.id AS paletteId,
                           color.identifier AS colorIdentifier FROM color 
                    JOIN color_palette ON color.color_palette_id = color_palette.id
                    JOIN design_production_method_color_palette 
                        ON color_palette.id = design_production_method_color_palette.color_palette_id 
                    WHERE 
                          color.date_deleted = :dateDeletedDefault AND 
                          color_palette.date_deleted = :dateDeletedDefault AND 
                          design_production_method_color_palette.date_deleted = :dateDeletedDefault AND
                          color.hex_value = :hexValue AND
                          design_production_method_color_palette.design_production_method_id = :methodId 
                    LIMIT 1;
                ');
        }

        // select by color and palette identifier
        if (!isset($this->statements['selectcolorbyidentifierpalette'])) {
            $this->statements['selectcolorbyidentifierpalette'] =
                $this->conn->prepare('
                    SELECT color_palette.identifier AS paletteIdentifier, color_palette.id AS paletteId,
                           color.identifier AS colorIdentifier FROM color 
                    JOIN color_palette ON color.color_palette_id = color_palette.id
                    JOIN design_production_method_color_palette 
                        ON color_palette.id = design_production_method_color_palette.color_palette_id 
                    WHERE 
                          color_palette.date_deleted = :dateDeletedDefault AND 
                          color.date_deleted = :dateDeletedDefault AND 
                          design_production_method_color_palette.date_deleted = :dateDeletedDefault AND
                          color_palette.identifier = :paletteIdentifier AND 
                          color.identifier = :colorIdentifier AND
                          design_production_method_color_palette.design_production_method_id = :methodId 
                    LIMIT 1;
                ');
        }
    }

    /**
     * Reset this area's design view relations to non-default.
     *
     * @param int $designAreaId
     *
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function resetDefaultDesignViewDesignArea(int $designAreaId): void
    {
        if (!isset($this->statements['defaultdesignviewdesignarea'])) {
            $this->statements['defaultdesignviewdesignarea'] =
                $this->conn->prepare('
                    UPDATE design_view_design_area
                    SET is_default = 0
                    WHERE design_area_id = :designAreaId;'
                );
        }

        $stmt = &$this->statements['defaultdesignviewdesignarea'];
        $stmt->bindValue(':designAreaId', $designAreaId);

        $stmt->executeStatement();
    }

    /**
     * @param int $designAreaId
     * @param array $designArea
     *
     * @throws DBALException
     */
    public function processVisualizationBackgroundColors(int $designAreaId, array $designArea): void
    {
        if (!$this->designAreaHasValidVisualizationBackgroundColors($designArea)) {
            return;
        }

        $backgroundColorMap = $this->convertVisualizationBackgroundColorMap(
            $designArea['visualizationBackgroundColors']
        );

        $selectStmt = $this->statements['visualizationBackgroundColorsSelect'];
        $selectStmt->bindValue('designAreaId', $designAreaId);

        $updateStmt = $this->statements['visualizationBackgroundColorsUpdate'];

        $result = $selectStmt->executeQuery();
        while ($designAreaDesignProductionMethod = $result->fetchAssociative()) {
            $methodId = $designAreaDesignProductionMethod['design_production_method_id'] ?? null;

            // get additional data from DB
            $additionalData = $designAreaDesignProductionMethod['additional_data'] ?
                json_decode($designAreaDesignProductionMethod['additional_data'], true) :
                [];

            // replace visualization background colors in additionalData
            $additionalData['engravingBackgroundColors'] = $backgroundColorMap;
            $additionalData = json_encode($additionalData);
            $updateStmt->bindValue('designAreaId', $designAreaId);
            $updateStmt->bindValue('methodId', $methodId);
            $updateStmt->bindValue('additionalData', $additionalData);
            $updateStmt->executeStatement();
        }
    }

    /**
     * @param array $designArea
     *
     * @return bool
     */
    private function designAreaHasValidVisualizationBackgroundColors(array $designArea): bool
    {
        return isset($designArea['visualizationBackgroundColors']) &&
            is_array($designArea['visualizationBackgroundColors']) &&
            [] !== $designArea['visualizationBackgroundColors'];
    }

    /**
     * Converting visualization background color from import to DB format.
     *
     * @param array $visualizationBackgroundColorMap
     *
     * @return array
     */
    private function convertVisualizationBackgroundColorMap(array $visualizationBackgroundColorMap): array
    {
        return array_map(
            static function (array $backgroundColor) {
                $backgroundColor['colorHex'] = $backgroundColor['color'];
                unset($backgroundColor['color']);

                return $backgroundColor;
            },
            $visualizationBackgroundColorMap
        );
    }
}
