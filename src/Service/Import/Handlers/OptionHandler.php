<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;
use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;

/**
 * @internal
 */
class OptionHandler
{
    /**
     * @var Connection
     */
    private Connection $conn;

    /**
     * Storage for prepared statements.
     *
     * @var Statement[]
     */
    private $statements = [];

    /**
     * @var ImportHelper
     */
    private $helper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AttributeHandler
     */
    private $attributeHandler;

    /**
     * @var MediaHandler
     */
    private $mediaHandler;

    /**
     * @param Connection $conn
     * @param ImportHelper $helper
     * @param AttributeHandler $attributeHandler
     * @param MediaHandler $mediaHandler
     * @param ImportLogger $logger
     */
    public function __construct(Connection $conn, ImportHelper $helper, AttributeHandler $attributeHandler, MediaHandler $mediaHandler, ImportLogger $logger)
    {
        $this->conn = $conn;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->attributeHandler = $attributeHandler;
        $this->mediaHandler = $mediaHandler;
    }

    /**
     * Process whole Option data array.
     *
     * @param array $optionData
     * @param int $itemOptionClassificationId
     * @param bool $deleteNonExistingOptionThumbnails
     * @param string[] $overrideOptionsOrder
     *
     * @return int Option ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processOption(
        array $optionData,
        int $itemOptionClassificationId,
        bool $deleteNonExistingOptionThumbnails,
        array $overrideOptionsOrder = []
    ): int {
        $optionId = $this->processOptionData($optionData);
        $itemOptionClassificationOptionId = $this->processItemOptionClassificationOptionRelation(
            $optionData,
            $optionId,
            $itemOptionClassificationId,
            $overrideOptionsOrder
        );

        if (isset($optionData['texts'])) {
            foreach ((array)$optionData['texts'] as $attributeData) {
                $this->processOptionText($attributeData, $optionId);
            }
        }

        if (isset($optionData['attributes'])) {
            foreach ((array)$optionData['attributes'] as $attributeData) {
                $this->attributeHandler->processOptionAttribute($attributeData, $optionId);
            }
        }

        if (isset($optionData['prices'])) {
            foreach ((array)$optionData['prices'] as $priceData) {
                $this->processOptionPrice($priceData, $optionId);
            }
        }

        if (isset($optionData['deltaPrices'])) {
            foreach ((array)$optionData['deltaPrices'] as $deltaPriceData) {
                $this->processOptionDeltaPrice($deltaPriceData, $itemOptionClassificationOptionId);
            }
        }

        $hasThumbnail = false;
        if (isset($optionData['images']['thumbnail'])) {
            $hasThumbnail = $this->mediaHandler->uploadOptionThumbnail(
                $optionData['optionIdentifier'],
                $optionData['images']['thumbnail']
            );
        }
        if (!$hasThumbnail && $deleteNonExistingOptionThumbnails) {
            $this->mediaHandler->deleteOptionThumbnail($optionData['optionIdentifier']);
        }

        return $optionId;
    }

    /**
     * Process an Option data.
     *
     * @param array $optionData
     *
     * @return int New Option ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function processOptionData(array $optionData): int
    {
        if (!isset($this->statements['option'])) {
            $this->statements['option'] =
                $this->conn->prepare('insert into
                    `option` (identifier, sequencenumber, has_textinput, input_validation_type, date_created, date_updated)
                    values (:identifier, :sequenceNumber, :hasTextinput, :inputValidationType, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), sequencenumber = :sequenceNumber, has_textinput = :hasTextinput, input_validation_type = :inputValidationType,
                                            date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['option'];

        $stmt->bindValue(':identifier', $optionData['optionIdentifier']);
        $stmt->bindValue(
            ':sequenceNumber',
            $optionData['sequencenumber'] ?? $this->getNextOptionSequenceNumber()
        );
        $stmt->bindValue(
            ':hasTextinput',
            isset($optionData['hasTextinput']) ? (int)$optionData['hasTextinput'] : 0
        );
        $stmt->bindValue(
            ':inputValidationType',
            $optionData['inputValidationType'] ?? null
        );
        $this->helper->bindDate($stmt);
        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * @param array $optionData
     * @param int $optionId
     * @param int $itemOptionClassificationId
     * @param string[] $overrideOptionsOrder
     *
     * @return int ItemOptionClassification ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function processItemOptionClassificationOptionRelation(
        array $optionData,
        int $optionId,
        int $itemOptionClassificationId,
        array $overrideOptionsOrder = []
    ): int {
        if (!isset($this->statements['item_optionclassification_option'])) {
            $this->statements['item_optionclassification_option'] =
                $this->conn->prepare('insert into
                    item_optionclassification_option (item_optionclassification_id, option_id, amountisselectable, 
                                                      sequence_number, date_created, date_updated)
                    values (:itemOptionClassificationId, :optionId, :amountIsSelectable, :sequenceNumber, :dateCreated, 
                            :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), amountisselectable = :amountIsSelectable,
                                            sequence_number = :sequenceNumber, date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['item_optionclassification_option'];

        $stmt->bindValue(':optionId', $optionId);
        $stmt->bindValue(':itemOptionClassificationId', $itemOptionClassificationId);
        $stmt->bindValue(
            ':amountIsSelectable',
            isset($optionData['amountIsSelectable']) ? (int)$optionData['amountIsSelectable'] : 0
        );
        $stmt->bindValue(':sequenceNumber', $overrideOptionsOrder[$optionData['optionIdentifier']] ?? null);
        $this->helper->bindDate($stmt);
        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * Insert new item text.
     *
     * @param array $textData
     * @param int $optionId
     *
     * @return int OptionText ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processOptionText(array $textData, int $optionId): int
    {
        if (!isset($this->statements['option_text'])) {
            $this->statements['option_text'] =
                $this->conn->prepare('insert into
                    option_text (title, description, abstract, option_id, language_id, date_created, date_updated)
                    values (:title, :description, :abstract, :optionId, :languageId, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE 
                        id = LAST_INSERT_ID(id), title = :title, abstract = :abstract, description = :description, date_updated = :dateUpdated
                ');
        }
        $stmt = &$this->statements['option_text'];

        $langId = $this->helper->getLanguageIdByTextItem($textData);
        if (!$langId) {
            $this->logger->error('Language is invalid', $textData);

            return 0;
        }

        $stmt->bindValue(':title', $textData['title']);
        $stmt->bindValue(':description', $textData['description'] ?? null);
        $stmt->bindValue(':abstract', $textData['abstract'] ?? null);
        $stmt->bindValue(':optionId', $optionId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * Get next Option sequence number.
     *
     * @return int Next Option sequnce number
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function getNextOptionSequenceNumber(): int
    {
        if (!isset($this->statements['option_seq'])) {
            $this->statements['option_seq'] =
                $this->conn->prepare('select max(sequencenumber) as sequence_number from `option`');
        }
        $stmt = $this->statements['option_seq'];
        $result = $stmt->executeQuery();

        $seqNumber = $result->fetchFirstColumn();
        $seqNumber = $seqNumber[0] ?? 0;

        return (int)$seqNumber + 1;
    }

    /**
     * @param array $price
     * @param int $optionId
     *
     * @return bool Is save successful
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function processOptionPrice(array $price, int $optionId): bool
    {
        if (!isset($this->statements['option_price'])) {
            $this->statements['option_price'] =
                $this->conn->prepare('insert into
                    option_price (price, price_net, amountfrom, date_created, date_updated, channel_id, option_id)
                    values (:price, :priceNet, :amountFrom, :dateCreated, :dateUpdated, :channelId, :optionId)
                    ON DUPLICATE KEY UPDATE price = :price, price_net = :priceNet, amountfrom = :amountFrom,
                                            id = LAST_INSERT_ID(id), date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['option_price'];

        $channelId = $this->helper->getChannelByIdentifier($price['channelIdentifier']);

        if (!$channelId) {
            $this->logger->error('Channel is invalid', $price);

            return false;
        }

        foreach ($price['bulkPrices'] as $optionPrice) {
            // amountfrom has  to be a number bigger than 0
            if (!isset($optionPrice['itemAmountFrom'])
                || empty((int)$optionPrice['itemAmountFrom'])
                || (int)$optionPrice['itemAmountFrom'] < 1) {
                $optionPrice['itemAmountFrom'] = 1;
            }

            $stmt->bindValue(':optionId', $optionId);
            $stmt->bindValue(':channelId', $channelId);
            $stmt->bindValue(':price', $optionPrice['price']);
            $stmt->bindValue(':priceNet', $optionPrice['priceNet']);
            $stmt->bindValue(':amountFrom', $optionPrice['itemAmountFrom']);
            $this->helper->bindDate($stmt);
            $stmt->executeStatement();
        }

        return true;
    }

    /**
     * @param array $deltaPrice
     * @param int $itemOptionClassificationOptionId
     *
     * @return bool Is save successful
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function processOptionDeltaPrice(array $deltaPrice, int $itemOptionClassificationOptionId): bool
    {
        if (!isset($this->statements['itemoptionclassificationoptiondeltaprice'])) {
            $this->statements['itemoptionclassificationoptiondeltaprice'] =
                $this->conn->prepare('insert into
                    itemoptionclassificationoptiondeltaprice (price, price_net, date_created, date_updated, channel_id,
                                                              item_optionclassification_option_id)
                    values (:price, :priceNet, :dateCreated, :dateUpdated, :channelId, :itemOptionClassificationOtionId)
                    ON DUPLICATE KEY UPDATE price = :price, price_net = :priceNet, id = LAST_INSERT_ID(id),
                                            date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['itemoptionclassificationoptiondeltaprice'];

        $channelId = $this->helper->getChannelByIdentifier($deltaPrice['channelIdentifier']);

        if (!$channelId) {
            $this->logger->error('Channel is invalid', $deltaPrice);

            return false;
        }

        foreach ($deltaPrice['prices'] as $optionPrice) {
            $stmt->bindValue(':itemOptionClassificationOtionId', $itemOptionClassificationOptionId);
            $stmt->bindValue(':channelId', $channelId);
            $stmt->bindValue(':price', $optionPrice['price']);
            $stmt->bindValue(':priceNet', $optionPrice['priceNet']);
            $stmt->bindValue(':amountFrom', $optionPrice['itemAmountFrom'] ?? 1);
            $this->helper->bindDate($stmt);
            $stmt->executeStatement();
        }

        return true;
    }
}
