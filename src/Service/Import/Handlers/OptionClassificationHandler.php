<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Import\Handlers;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;
use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\ImportLogger;
use Redhotmagma\ConfiguratorApiBundle\Service\Import\Models\ImportOptions;

/**
 * @internal
 */
class OptionClassificationHandler
{
    /**
     * @var Connection
     */
    private $conn;

    /**
     * Storage for prepared statements.
     *
     * @var Statement[]
     */
    private $statements = [];

    /**
     * @var ImportHelper
     */
    private $helper;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AttributeHandler
     */
    private $attributeHandler;

    /**
     * @var OptionHandler
     */
    private $optionHandler;

    /**
     * @var MediaHandler
     */
    private $mediaHandler;

    /**
     * OptionClassificationHandler constructor.
     *
     * @param Connection $conn
     * @param ImportHelper $helper
     * @param AttributeHandler $attributeHandler
     * @param OptionHandler $optionHandler
     * @param MediaHandler $mediaHandler
     * @param ImportLogger $logger
     */
    public function __construct(Connection $conn, ImportHelper $helper, AttributeHandler $attributeHandler, OptionHandler $optionHandler, MediaHandler $mediaHandler, ImportLogger $logger)
    {
        $this->conn = $conn;
        $this->helper = $helper;
        $this->logger = $logger;
        $this->attributeHandler = $attributeHandler;
        $this->optionHandler = $optionHandler;
        $this->mediaHandler = $mediaHandler;
    }

    /**
     * Process whole OptionClassification (Component) data array.
     *
     * @param array $optionClassificationData
     * @param int $itemId
     * @param ImportOptions $importOptions
     *
     * @return int OptionClassification ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processItemOptionClassification(array $optionClassificationData, int $itemId, ImportOptions $importOptions): int
    {
        $optionClassificationId = $this->processOptionClassificationData($optionClassificationData);
        $itemOptionClassificationId = $this->processOptionClassificationItemRelation(
            $optionClassificationData,
            $itemId,
            $optionClassificationId
        );

        if (isset($optionClassificationData['texts'])) {
            foreach ((array)$optionClassificationData['texts'] as $textData) {
                $this->processOptionClassificationText($textData, $optionClassificationId);
            }
        }

        if (isset($optionClassificationData['attributes'])) {
            foreach ((array)$optionClassificationData['attributes'] as $attributeData) {
                $this->attributeHandler->processOptionClassificationAttribute($attributeData, $optionClassificationId);
            }
        }

        if (isset($optionClassificationData['options'])) {
            $overrideOptionsOrder = array_key_exists('optionsOrder', $optionClassificationData)
                ? array_flip($optionClassificationData['optionsOrder'])
                : [];

            foreach ((array)$optionClassificationData['options'] as $optionData) {
                $this->optionHandler->processOption($optionData, $itemOptionClassificationId, $importOptions->deleteNonExistingOptionThumbnails, $overrideOptionsOrder);
            }
        }

        $hasComponentThumbnail = false;
        if (
            isset($optionClassificationData['images']['thumbnail'])) {
            $hasComponentThumbnail = $this->mediaHandler->uploadOptionClassificationThumbnail(
                $optionClassificationData['componentIdentifier'],
                $optionClassificationData['images']['thumbnail']
            );
        }
        if (!$hasComponentThumbnail && $importOptions->deleteNonExistingComponentThumbnails) {
            $this->mediaHandler->deleteComponentThumbnail($optionClassificationData['componentIdentifier']);
        }

        return $optionClassificationId;
    }

    /**
     * Process one OptionClassification (Component).
     *
     * @param array $optionClassificationData
     *
     * @return int OptionClassification ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function processOptionClassificationData(array $optionClassificationData): int
    {
        if (!isset($this->statements['optionclassification'])) {
            $this->statements['optionclassification'] =
                $this->conn->prepare('INSERT INTO
                    optionclassification (identifier, sequencenumber, hidden_in_frontend, hidden_in_pdf, date_created, date_updated)
                    VALUES (:identifier, :sequenceNumber, :hiddenInFrontend, :hiddenInPdf, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), sequencenumber = :sequenceNumber, hidden_in_frontend = :hiddenInFrontend, hidden_in_pdf = :hiddenInPdf, date_updated = :dateUpdated;');
        }
        $stmt = &$this->statements['optionclassification'];

        $stmt->bindValue(':identifier', $optionClassificationData['componentIdentifier']);

        $hiddenInFrontend = isset($optionClassificationData['hiddenInFrontend']) ? (int)$optionClassificationData['hiddenInFrontend'] : 0;
        $stmt->bindValue(':hiddenInFrontend', $hiddenInFrontend);

        $hiddenInPdf = isset($optionClassificationData['hiddenInPdf']) ? (int)$optionClassificationData['hiddenInPdf'] : 0;
        $stmt->bindValue(':hiddenInPdf', $hiddenInPdf);

        $stmt->bindValue(':sequenceNumber', $optionClassificationData['sequencenumber'] ?? $this->getNextOptionClassificationSequenceNumber());
        $this->helper->bindDate($stmt);
        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * Add Item relation to OptionClassification.
     *
     * @param array $optionClassificationData
     * @param int $itemId
     * @param int $optionClassificationId
     *
     * @return int ItemOptionClassification ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function processOptionClassificationItemRelation(
        array $optionClassificationData,
        int $itemId,
        int $optionClassificationId
    ): int {
        if (!isset($this->statements['item_optionclassification'])) {
            $this->statements['item_optionclassification'] =
                $this->conn->prepare('INSERT INTO
                    item_optionclassification (item_id, optionclassification_id, is_multiselect, sequence_number, minamount, maxamount, is_mandatory, date_created, date_updated)
                    VALUES (:itemId, :optionClassificationId, :isMultiSelect, :sequenceNumber, :minamount, :maxamount, :isMandatory, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), is_multiselect = :isMultiSelect, sequence_number = :sequenceNumber, date_updated = :dateUpdated;');
        }

        $stmt = &$this->statements['item_optionclassification'];

        $stmt->bindValue(':itemId', $itemId);
        $stmt->bindValue(':optionClassificationId', $optionClassificationId);
        $stmt->bindValue(':isMultiSelect', isset($optionClassificationData['isMultiselect']) ? (int)$optionClassificationData['isMultiselect'] : 0);
        $stmt->bindValue(':sequenceNumber', $optionClassificationData['sequencenumber'] ?? null);
        $stmt->bindValue(':minamount', isset($optionClassificationData['minamount']) ? (int)$optionClassificationData['minamount'] : null);
        $stmt->bindValue(':maxamount', isset($optionClassificationData['maxamount']) ? (int)$optionClassificationData['maxamount'] : null);
        $stmt->bindValue(':isMandatory', isset($optionClassificationData['isMandatory']) ? (int)$optionClassificationData['isMandatory'] : 1);
        $this->helper->bindDate($stmt);
        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * Insert new item text.
     *
     * @param array $textData
     * @param int $optionClassificationId
     *
     * @return int New OptionClassificationText ID
     *
     * @throws \Doctrine\DBAL\Exception
     */
    public function processOptionClassificationText(array $textData, int $optionClassificationId): int
    {
        if (!isset($this->statements['option_classification_text'])) {
            $this->statements['option_classification_text'] =
                $this->conn->prepare('INSERT INTO
                    option_classification_text (title, description, option_classification_id, language_id, date_created, date_updated)
                    VALUES (:title, :description, :optionClassificationId, :languageId, :dateCreated, :dateUpdated)
                    ON DUPLICATE KEY UPDATE id = LAST_INSERT_ID(id), title = :title, description = :description, date_updated = :dateUpdated
                ');
        }

        $stmt = &$this->statements['option_classification_text'];

        $langId = $this->helper->getLanguageIdByTextItem($textData);
        if (!$langId) {
            $this->logger->error('Language is invalid', $textData);

            return 0;
        }

        $stmt->bindValue(':title', $textData['title']);
        $stmt->bindValue(':description', $textData['description'] ?? null);
        $stmt->bindValue(':optionClassificationId', $optionClassificationId);
        $stmt->bindValue(':languageId', $langId);
        $this->helper->bindDate($stmt);

        $stmt->executeStatement();

        return (int)$this->conn->lastInsertId();
    }

    /**
     * Get the next OptionClassification sequence number.
     *
     * @return int Next sequence number
     *
     * @throws \Doctrine\DBAL\Exception
     */
    private function getNextOptionClassificationSequenceNumber(): int
    {
        if (!isset($this->statements['optionclassification_seq'])) {
            $this->statements['optionclassification_seq'] = $this->conn->prepare('SELECT max(sequencenumber) AS sequence_number FROM optionclassification');
        }

        $stmt = $this->statements['optionclassification_seq'];
        $result = $stmt->executeQuery();

        $seqNumber = $result->fetchFirstColumn();
        $seqNumber = $seqNumber[0] ?? 0;

        return (int)$seqNumber + 1;
    }
}
