<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Currency;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Currency;
use Redhotmagma\ConfiguratorApiBundle\Repository\CurrencyRepository;

/**
 * @internal
 */
class CurrencySave
{
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * CurrencySave constructor.
     *
     * @param CurrencyRepository $currencyRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        CurrencyRepository $currencyRepository,
        EntityFromStructureConverter $entityFromStructureConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->currencyRepository = $currencyRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Currency $currency
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Currency
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Currency $currency)
    {
        $entity = null;
        if (isset($currency->id) && $currency->id > 0) {
            $entity = $this->currencyRepository->findOneBy(['id' => $currency->id]);
        }
        $entity = $this->entityFromStructureConverter->convertOne($currency, $entity);
        $entity = $this->currencyRepository->save($entity);

        /** @var Currency $entity */
        $entity = $this->currencyRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->structureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
