<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Currency;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Currency;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CurrencyRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class CurrencyApi
{
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var CurrencySave
     */
    private $currencySave;

    /**
     * @var CurrencyDelete
     */
    private $currencyDelete;

    /**
     * CurrencyApi constructor.
     *
     * @param CurrencyRepository $currencyRepository
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param CurrencySave $currencySave
     * @param CurrencyDelete $currencyDelete
     */
    public function __construct(
        CurrencyRepository $currencyRepository,
        StructureFromEntityConverter $structureFromEntityConverter,
        CurrencySave $currencySave,
        CurrencyDelete $currencyDelete
    ) {
        $this->currencyRepository = $currencyRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->currencySave = $currencySave;
        $this->currencyDelete = $currencyDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->currencyRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->structureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->currencyRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Currency
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Currency
    {
        /** @var Currency $entity */
        $entity = $this->currencyRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->structureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Currency $currency
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Currency
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Currency $currency)
    {
        $structure = $this->currencySave->save($currency);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->currencyDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['iso'];
    }
}
