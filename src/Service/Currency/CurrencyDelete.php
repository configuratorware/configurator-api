<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Currency;

use Redhotmagma\ConfiguratorApiBundle\Entity\Currency;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\CurrencyRepository;

/**
 * @internal
 */
class CurrencyDelete
{
    /**
     * @var CurrencyRepository
     */
    private $currencyRepository;

    /**
     * @param CurrencyRepository $currencyRepository
     */
    public function __construct(
        CurrencyRepository $currencyRepository
    ) {
        $this->currencyRepository = $currencyRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Currency $entity */
            $entity = $this->currencyRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            $this->currencyRepository->delete($entity);
        }
    }
}
