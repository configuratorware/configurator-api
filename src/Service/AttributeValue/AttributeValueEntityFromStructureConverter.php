<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue;

/**
 * @internal
 */
class AttributeValueEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param Attributevalue $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($localStructure, $entity,
            'translations', 'Attributevaluetranslation');
        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations($localStructure, $entity,
            'translations', 'Attributevaluetranslation');

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Attributevalue::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }
}
