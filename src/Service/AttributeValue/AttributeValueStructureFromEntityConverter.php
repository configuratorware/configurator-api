<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Cache\StructureCache;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Structure\Attributevaluetranslation;

/**
 * @internal
 */
class AttributeValueStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var StructureCache
     */
    private $cache;

    /**
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param StructureCache               $cache
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter, StructureCache $cache)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->cache = $cache;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue $entity
     * @param string                                                   $structureClassName
     *
     * @return Attributevalue
     */
    public function convertOne($entity, $structureClassName = Attributevalue::class)
    {
        if (null === $this->cache->findStructureForEntity($entity)) {
            /** @var Attributevalue $structure */
            $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
            $structure = $this->addAttributeValueTranslationsToStructure($structure, $entity);

            $this->cache->store($entity, $structure);

            return $structure;
        }

        return $this->cache->findStructureForEntity($entity);
    }

    /**
     * @param array  $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Attributevalue::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param Attributevalue                                           $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue $entity
     *
     * @return mixed
     */
    protected function addAttributeValueTranslationsToStructure($structure, $entity)
    {
        $translations = [];

        $attributeValueTranslations = $entity->getAttributevaluetranslation();

        if (!empty($attributeValueTranslations)) {
            foreach ($attributeValueTranslations as $attributeValueTranslation) {
                $translationStructure = $this->structureFromEntityConverter->convertOne(
                    $attributeValueTranslation,
                    Attributevaluetranslation::class
                );

                if (!empty($translationStructure)) {
                    $translationStructure->language = $attributeValueTranslation->getLanguage()
                        ->getIso();
                    $translations[] = $translationStructure;
                }
            }
        }

        $structure->translations = $translations;

        return $structure;
    }
}
