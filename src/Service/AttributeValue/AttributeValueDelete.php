<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemattributeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionAttributeRepository;

/**
 * @internal
 */
class AttributeValueDelete
{
    /**
     * @var AttributevalueRepository
     */
    private $attributeValueRepository;

    /**
     * @var ItemattributeRepository
     */
    private $itemAttributeRepository;

    /**
     * @var OptionAttributeRepository
     */
    private $optionAttributeRepository;

    /**
     * AttributeValueDelete constructor.
     *
     * @param AttributevalueRepository $attributeValueRepository
     * @param ItemattributeRepository $itemAttributeRepository
     * @param OptionAttributeRepository $optionAttributeRepository
     */
    public function __construct(
        AttributevalueRepository $attributeValueRepository,
        ItemattributeRepository $itemAttributeRepository,
        OptionAttributeRepository $optionAttributeRepository
    ) {
        $this->attributeValueRepository = $attributeValueRepository;
        $this->itemAttributeRepository = $itemAttributeRepository;
        $this->optionAttributeRepository = $optionAttributeRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Attributevalue $entity */
            $entity = $this->attributeValueRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            if (!empty($entity->getAttributevaluetranslation()->toArray())) {
                foreach ($entity->getAttributevaluetranslation() as $attributeValueTranslation) {
                    $this->attributeValueRepository->delete($attributeValueTranslation);
                }
            }

            $itemAttributes = $this->itemAttributeRepository->findBy(['attributevalue' => $entity]);
            if (!empty($itemAttributes)) {
                foreach ($itemAttributes as $itemAttribute) {
                    $this->itemAttributeRepository->delete($itemAttribute);
                }
            }

            $optionAttributes = $this->optionAttributeRepository->findBy(['attributevalue' => $entity]);
            if (!empty($optionAttributes)) {
                foreach ($optionAttributes as $optionAttribute) {
                    $this->optionAttributeRepository->delete($optionAttribute);
                }
            }

            $this->attributeValueRepository->delete($entity);
        }
    }
}
