<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository;

/**
 * @internal
 */
class AttributeValueSave
{
    /**
     * @var AttributevalueRepository
     */
    private $attributeValueRepository;

    /**
     * @var AttributeValueEntityFromStructureConverter
     */
    private $attributeValueEntityFromStructureConverter;

    /**
     * @var AttributeValueStructureFromEntityConverter
     */
    private $attributeValueStructureFromEntityConverter;

    /**
     * AttributeValueSave constructor.
     *
     * @param AttributevalueRepository $attributeValueRepository
     * @param AttributeValueEntityFromStructureConverter $attributeValueEntityFromStructureConverter
     * @param AttributeValueStructureFromEntityConverter $attributeValueStructureFromEntityConverter
     */
    public function __construct(
        AttributevalueRepository $attributeValueRepository,
        AttributeValueEntityFromStructureConverter $attributeValueEntityFromStructureConverter,
        AttributeValueStructureFromEntityConverter $attributeValueStructureFromEntityConverter
    ) {
        $this->attributeValueRepository = $attributeValueRepository;
        $this->attributeValueEntityFromStructureConverter = $attributeValueEntityFromStructureConverter;
        $this->attributeValueStructureFromEntityConverter = $attributeValueStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue $attributeValue
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue $attributeValue)
    {
        $entity = null;
        if (isset($attributeValue->id) && $attributeValue->id > 0) {
            $entity = $this->attributeValueRepository->findOneBy(['id' => $attributeValue->id]);
        }
        $entity = $this->attributeValueEntityFromStructureConverter->convertOne($attributeValue, $entity);
        $entity = $this->attributeValueRepository->save($entity);

        /** @var Attributevalue $entity */
        $entity = $this->attributeValueRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->attributeValueStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
