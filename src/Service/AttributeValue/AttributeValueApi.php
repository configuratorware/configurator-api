<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\AttributeValue;

use Redhotmagma\ConfiguratorApiBundle\Entity\Attributevalue;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\AttributevalueRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class AttributeValueApi
{
    /**
     * @var AttributevalueRepository
     */
    private $attributeValueRepository;

    /**
     * @var AttributeValueStructureFromEntityConverter
     */
    private $attributeValueStructureFromEntityConverter;

    /**
     * @var AttributeValueSave
     */
    private $attributeValueSave;

    /**
     * @var AttributeValueDelete
     */
    private $attributeValueDelete;

    /**
     * AttributeValueApi constructor.
     *
     * @param AttributeValueRepository $attributeValueRepository
     * @param AttributeValueStructureFromEntityConverter $attributeValueStructureFromEntityConverter
     * @param AttributeValueSave $attributeValueSave
     * @param AttributeValueDelete $attributeValueDelete
     */
    public function __construct(
        AttributeValueRepository $attributeValueRepository,
        AttributeValueStructureFromEntityConverter $attributeValueStructureFromEntityConverter,
        AttributeValueSave $attributeValueSave,
        AttributeValueDelete $attributeValueDelete
    ) {
        $this->attributeValueRepository = $attributeValueRepository;
        $this->attributeValueStructureFromEntityConverter = $attributeValueStructureFromEntityConverter;
        $this->attributeValueSave = $attributeValueSave;
        $this->attributeValueDelete = $attributeValueDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->attributeValueRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->attributeValueStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->attributeValueRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue
    {
        /** @var Attributevalue $entity */
        $entity = $this->attributeValueRepository->findOneBy(['id' => $id]);

        if (null === $entity) {
            throw new NotFoundException();
        }

        $structure = $this->attributeValueStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue $attributeValue
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Attributevalue $attributeValue)
    {
        $structure = $this->attributeValueSave->save($attributeValue);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->attributeValueDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['value', 'attributevaluetranslation.translation'];
    }
}
