<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;

/**
 * @internal
 */
class DesignProductionMethodDelete
{
    /**
     * @var DesignProductionMethodRepository
     */
    private $designProductionMethodRepository;

    /**
     * @param DesignProductionMethodRepository $designProductionMethodRepository
     */
    public function __construct(
        DesignProductionMethodRepository $designProductionMethodRepository
    ) {
        $this->designProductionMethodRepository = $designProductionMethodRepository;
    }

    /**
     * @param string $id
     */
    public function delete(string $id): void
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /**
             * Get Entity for given Id.
             *
             * @var DesignProductionMethod $entity
             */
            $entity = $this->designProductionMethodRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            // Remove Color Palette Relation
            $this->removeRelation($entity, 'DesignProductionMethodColorPalette');

            // Remove Design Production Method's Texts
            $this->removeRelation($entity, 'DesignProductionMethodText');

            // Remove DesignArea connection
            $this->removeRelation($entity, 'DesignAreaDesignProductionMethod');

            $this->removeTypeRelations($entity);

            $this->designProductionMethodRepository->delete($entity);
        }
    }

    /**
     * @param DesignProductionMethod $entity
     * @param string $relationName
     */
    protected function removeRelation(DesignProductionMethod $entity, string $relationName): void
    {
        $callEntity = 'get' . $relationName;
        /** @var mixed $relations */
        $relations = $entity->$callEntity();
        if (!$relations->isEmpty()) {
            foreach ($relations as $relationEntry) {
                $this->designProductionMethodRepository->delete($relationEntry);
            }
        }
    }

    /**
     * @param DesignProductionMethod $entity
     */
    protected function removeTypeRelations(DesignProductionMethod $entity): void
    {
        /** @var DesignerProductionCalculationType $type */
        foreach ($entity->getDesignerProductionCalculationType() as $type) {
            foreach ($type->getDesignerProductionCalculationTypeText() as $text) {
                $this->designProductionMethodRepository->delete($text);
            }
            foreach ($type->getDesignProductionMethodPrice() as $price) {
                $this->designProductionMethodRepository->delete($price);
            }

            $this->designProductionMethodRepository->delete($type);
        }
    }
}
