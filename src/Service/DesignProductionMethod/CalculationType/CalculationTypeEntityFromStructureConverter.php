<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\CalculationType;

use Doctrine\Common\Collections\Collection;
use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationType as DesignerProductionCalculationTypeStructure;

/**
 * @internal
 */
class CalculationTypeEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * DesignerProductionCalculationTypeEntityFromStructureConverter constructor.
     *
     * @param ChannelRepository $channelRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        ChannelRepository $channelRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->channelRepository = $channelRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param DesignerProductionCalculationTypeStructure $structure
     * @param DesignerProductionCalculationType|null $entity
     * @param string $entityClassName
     *
     * @return mixed
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = DesignerProductionCalculationType::class
    ) {
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        /** @var DesignerProductionCalculationType $entity */
        $entity = $this->updateManyToOneRelations(
            $entity,
            $structure,
            'texts',
            'DesignerProductionCalculationTypeText'
        );

        $entity = $this->updatePrices(
            $entity,
            $structure
        );

        return $entity;
    }

    /**
     * @param array $structures
     * @param Collection $entities
     */
    public function convertMany(
        array $structures,
        Collection $entities
    ): void {
        foreach ($structures as $structure) {
            $entityCollection = $entities->filter(
                function ($entry) use ($structure) {
                    /** @var DesignerProductionCalculationType $entry */
                    if ($entry->getId()) {
                        return $entry->getId() == $structure->id;
                    } else {
                        return $entry->getIdentifier() == $structure->identifier;
                    }
                }
            );

            $this->convertOne($structure, $entityCollection->first());
        }
    }

    /**
     * @param Collection $designerProductionCalculationTypes
     *
     * @throws \Exception
     */
    public function setSubEntitiesDeleted(
        Collection $designerProductionCalculationTypes
    ): void {
        /** @var DesignerProductionCalculationType $type */
        foreach ($designerProductionCalculationTypes as $type) {
            if (EntityListener::DATE_DELETED_DEFAULT !== $type->getDateDeleted()->format('Y-m-d H:i:s')) {
                foreach ($type->getDesignerProductionCalculationTypeText() as $text) {
                    $text->setDateDeleted(new \DateTime());
                }

                foreach ($type->getDesignProductionMethodPrice() as $price) {
                    $price->setDateDeleted(new \DateTime());
                }
            }
        }
    }

    /**
     * @param DesignerProductionCalculationType $entity
     * @param DesignerProductionCalculationTypeStructure $structure
     * @param string $relationStructureName
     * @param string $relationEntityName
     *
     * @return DesignerProductionCalculationType
     */
    protected function updateManyToOneRelations(
        DesignerProductionCalculationType $entity,
        DesignerProductionCalculationTypeStructure $structure,
        string $relationStructureName,
        string $relationEntityName
    ): DesignerProductionCalculationType {
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        return $entity;
    }

    /**
     * @param DesignerProductionCalculationType $entity
     * @param DesignerProductionCalculationTypeStructure $structure
     *
     * @return DesignerProductionCalculationType
     */
    protected function updatePrices(
        DesignerProductionCalculationType $entity,
        DesignerProductionCalculationTypeStructure $structure
    ): DesignerProductionCalculationType {
        // add channel relation objects
        if (!empty($structure->prices)) {
            foreach ($structure->prices as $key => $priceStructure) {
                if (!empty($priceStructure->channel)) {
                    $structure->prices[$key]->channel = $this->channelRepository->findOneBy(['id' => $priceStructure->channel]);
                }

                if (!isset($priceStructure->amountFrom) || $priceStructure->amountFrom < 1) {
                    $structure->prices[$key]->amountFrom = 1;
                }
            }
        }

        $entity = $this->updateManyToOneRelations(
            $entity,
            $structure,
            'prices',
            'DesignProductionMethodPrice'
        );

        if (false === $entity->getColorAmountDependent()) {
            /** @var DesignProductionMethodPrice $price */
            foreach ($entity->getDesignProductionMethodPrice() as $price) {
                $price->setColorAmountFrom(1);
            }
        }

        return $entity;
    }
}
