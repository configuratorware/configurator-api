<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\CalculationType;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerProductionCalculationTypeText;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationType as DesignerProductionCalculationTypeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerProductionCalculationTypeText as DesignerProductionCalculationTypeTextStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodPrice as DesignProductionMethodPriceStructure;

/**
 * @internal
 */
class CalculationTypeStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * DesignerProductionCalculationTypeStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param DesignerProductionCalculationType $entity
     * @param string $structureClassName
     *
     * @return mixed|DesignerProductionCalculationTypeStructure
     */
    public function convertOne($entity, $structureClassName = DesignerProductionCalculationTypeStructure::class)
    {
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addTextsToStructure(
            $structure,
            $entity,
            'DesignerProductionCalculationType',
            DesignerProductionCalculationTypeTextStructure::class
        );

        $structure = $this->addPricesToStructure(
            $structure,
            $entity,
            'DesignProductionMethod',
            DesignProductionMethodPriceStructure::class
        );

        return $structure;
    }

    /**
     * @param $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = DesignerProductionCalculationTypeStructure::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    /**
     * @param DesignerProductionCalculationTypeStructure $structure
     * @param DesignerProductionCalculationType $entity
     * @param string $entityName
     * @param string $textStructureClass
     *
     * @return DesignerProductionCalculationTypeStructure
     */
    protected function addTextsToStructure(
        DesignerProductionCalculationTypeStructure $structure,
        DesignerProductionCalculationType $entity,
        string $entityName,
        string $textStructureClass
    ): DesignerProductionCalculationTypeStructure {
        $callEntity = 'get' . $entityName . 'Text';

        $texts = $entity->$callEntity();

        if (!empty($texts)) {
            /** @var DesignerProductionCalculationTypeText $text */
            foreach ($texts as $text) {
                /** @var DesignerProductionCalculationTypeTextStructure $textStructure */
                $textStructure = $this->structureFromEntityConverter->convertOne(
                    $text,
                    $textStructureClass
                );

                if (!empty($textStructure)) {
                    $textStructure->language = $text->getLanguage()->getIso();
                    $structure->texts[] = $textStructure;
                }
            }
        }

        return $structure;
    }

    /**
     * @param DesignerProductionCalculationTypeStructure $structure
     * @param DesignerProductionCalculationType $entity
     * @param string $entityName
     * @param string $priceStructureClass
     *
     * @return DesignerProductionCalculationTypeStructure
     */
    protected function addPricesToStructure(
        DesignerProductionCalculationTypeStructure $structure,
        DesignerProductionCalculationType $entity,
        string $entityName,
        string $priceStructureClass
    ): DesignerProductionCalculationTypeStructure {
        $callEntity = 'get' . $entityName . 'Price';

        $prices = $entity->$callEntity();

        if (!empty($prices)) {
            /** @var DesignProductionMethodPrice $price */
            foreach ($prices as $price) {
                /** @var DesignProductionMethodPriceStructure $priceStructure */
                $priceStructure = $this->structureFromEntityConverter->convertOne(
                    $price,
                    $priceStructureClass
                );

                if (!empty($priceStructure)) {
                    $priceStructure->channel = $price->getChannel()->getId();
                    $structure->prices[] = $priceStructure;
                }
            }
        }

        return $structure;
    }
}
