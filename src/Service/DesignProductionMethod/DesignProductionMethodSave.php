<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod as DesignProductionMethodStructure;

/**
 * @internal
 */
class DesignProductionMethodSave
{
    /**
     * @var DesignProductionMethodRepository
     */
    private $designProductionMethodRepository;

    /**
     * @var DesignProductionMethodEntityFromStructureConverter
     */
    private $designProductionMethodEntityFromStructureConverter;

    /**
     * @var DesignProductionMethodStructureFromEntityConverter
     */
    private $designProductionMethodStructureFromEntityConverter;

    /**
     * @param DesignProductionMethodRepository $designProductionMethodRepository
     * @param DesignProductionMethodEntityFromStructureConverter $designProductionMethodEntityFromStructureConverter
     * @param DesignProductionMethodStructureFromEntityConverter $designProductionMethodStructureFromEntityConverter
     */
    public function __construct(
        DesignProductionMethodRepository $designProductionMethodRepository,
        DesignProductionMethodEntityFromStructureConverter $designProductionMethodEntityFromStructureConverter,
        DesignProductionMethodStructureFromEntityConverter $designProductionMethodStructureFromEntityConverter
    ) {
        $this->designProductionMethodRepository = $designProductionMethodRepository;
        $this->designProductionMethodEntityFromStructureConverter = $designProductionMethodEntityFromStructureConverter;
        $this->designProductionMethodStructureFromEntityConverter = $designProductionMethodStructureFromEntityConverter;
    }

    /**
     * @param DesignProductionMethodStructure $structure
     *
     * @return DesignProductionMethodStructure
     */
    public function save(DesignProductionMethodStructure $structure): DesignProductionMethodStructure
    {
        $entity = null;

        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->designProductionMethodRepository->findOneBy(['id' => $structure->id]);
        }

        $entity = $this->designProductionMethodEntityFromStructureConverter->convertOne($structure, $entity);
        $entity = $this->designProductionMethodRepository->save($entity);

        $this->designProductionMethodRepository->clear();

        /** @var DesignProductionMethod $entity */
        $entity = $this->designProductionMethodRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->designProductionMethodStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
