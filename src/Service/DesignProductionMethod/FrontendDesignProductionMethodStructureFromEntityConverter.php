<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Service\ColorPalette\FrontendColorPaletteStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\DesignProductionMethod as DesignProductionMethodStructure;

/**
 * @internal
 */
class FrontendDesignProductionMethodStructureFromEntityConverter
{
    /**
     * @var FrontendColorPaletteStructureFromEntityConverter
     */
    private $frontendColorPaletteStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @param FrontendColorPaletteStructureFromEntityConverter $frontendColorPaletteStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(FrontendColorPaletteStructureFromEntityConverter $frontendColorPaletteStructureFromEntityConverter, StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->frontendColorPaletteStructureFromEntityConverter = $frontendColorPaletteStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param DesignAreaDesignProductionMethod $relationEntity
     *
     * @return DesignProductionMethodStructure
     */
    public function convertOne(DesignAreaDesignProductionMethod $relationEntity): DesignProductionMethodStructure
    {
        /** @var DesignAreaDesignProductionMethod $relationEntity */
        $relationTarget = $relationEntity->getDesignProductionMethod();

        /** @var DesignProductionMethodStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($relationTarget, DesignProductionMethodStructure::class);

        $structure = $this->structureFromEntityConverter->completeByRelationData($structure, $relationEntity);

        /*
         * Special Case: DesignAreaDesignProductionMethod can have height/width as well as DesignArea.
         * For easier access in Frontend we add data from DesignArea to DesignProductionMethod if
         * nothing is set in DesignAreaDesignProductionMethod
         */
        if (empty($structure->height) || empty($structure->width)) {
            $structure->height = $relationEntity->getDesignArea()->getHeight();
            $structure->width = $relationEntity->getDesignArea()->getWidth();
        }

        $structure->title = $relationTarget->getTranslatedTitle();

        $structure->engravingBackgroundColors = $this->extractFromAdditionalData(
            $relationEntity->getAdditionalData(),
            'engravingBackgroundColors'
        );

        $structure->colorPalettes = $this->addColorPalettesToDesignProductionMethod($relationTarget);

        $structure->designElementsLocked = $relationEntity->getDesignElementsLocked();
        $structure->maxImages = $relationEntity->getMaxImages();
        $structure->maxTexts = $relationEntity->getMaxTexts();
        $structure->maxElements = $relationEntity->getMaxElements();
        $structure->oneLineText = $relationEntity->getOneLineText();

        return $structure;
    }

    /**
     * @param DesignAreaDesignProductionMethod[] $relationEntities
     *
     * @return DesignProductionMethodStructure[]
     */
    public function convertMany(array $relationEntities): array
    {
        $structures = [];

        foreach ($relationEntities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    /**
     * @param DesignProductionMethod $entity
     *
     * @return array
     */
    protected function addColorPalettesToDesignProductionMethod(DesignProductionMethod $entity): array
    {
        $colorPalettes = [];

        /** @var DesignProductionMethodColorPalette $productionMethodColorPalette */
        foreach ($entity->getDesignProductionMethodColorPalette() as $productionMethodColorPalette) {
            $colorPalette = $productionMethodColorPalette->getColorPalette();
            if (null === $colorPalette) {
                continue;
            }

            $colorPalettes[] = $this->frontendColorPaletteStructureFromEntityConverter->convertOne($colorPalette);
        }

        return $colorPalettes;
    }

    /**
     * @param $additionalData
     * @param string $propertyName
     *
     * @return mixed
     */
    private function extractFromAdditionalData($additionalData, string $propertyName)
    {
        if (!$additionalData || !isset($additionalData->$propertyName)) {
            return null;
        }

        return $additionalData->$propertyName;
    }
}
