<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignProductionMethodRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod as DesignProductionMethodStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class DesignProductionMethodApi
{
    /**
     * @var DesignProductionMethodRepository
     */
    private $designProductionMethodRepository;

    /**
     * @var DesignProductionMethodStructureFromEntityConverter
     */
    private $designProductionMethodStructureFromEntityConverter;

    /**
     * DesignProductionMethodSave $designProductionMethodSave.
     */
    private $designProductionMethodSave;

    /**
     * DesignProductionMethodDelete $designProductionMethodDelete.
     */
    private $designProductionMethodDelete;

    public function __construct(
        DesignProductionMethodRepository $designProductionMethodRepository,
        DesignProductionMethodStructureFromEntityConverter $designProductionMethodStructureFromEntityConverter,
        DesignProductionMethodSave $designProductionMethodSave,
        DesignProductionMethodDelete $designProductionMethodDelete
    ) {
        $this->designProductionMethodRepository = $designProductionMethodRepository;
        $this->designProductionMethodStructureFromEntityConverter = $designProductionMethodStructureFromEntityConverter;
        $this->designProductionMethodSave = $designProductionMethodSave;
        $this->designProductionMethodDelete = $designProductionMethodDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(
        ListRequestArguments $arguments
    ): PaginationResult {
        $paginationResult = new PaginationResult();

        $entities = $this->designProductionMethodRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult->data = $this->designProductionMethodStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->designProductionMethodRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return DesignProductionMethodStructure
     *
     * @throws NotFoundException
     */
    public function getOne(
        int $id
    ): DesignProductionMethodStructure {
        /** @var DesignProductionMethod $entity */
        $entity = $this->designProductionMethodRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->designProductionMethodStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param DesignProductionMethodStructure $structure
     *
     * @return DesignProductionMethodStructure
     */
    public function save(
        DesignProductionMethodStructure $structure
    ): DesignProductionMethodStructure {
        $structure = $this->designProductionMethodSave->save($structure);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $this->designProductionMethodDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['designProductionMethodText.title', 'identifier'];
    }
}
