<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodColorPalette;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorPaletteRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\CalculationType\CalculationTypeEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod as DesignProductionMethodStructure;

/**
 * @internal
 */
class DesignProductionMethodEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var CalculationTypeEntityFromStructureConverter
     */
    private $calculationTypeEntityFromStructureConverter;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var ColorPaletteRepository
     */
    private $colorPaletteRepository;

    /**
     * DesignProductionMethodEntityFromStructureConverter constructor.
     *
     * @param CalculationTypeEntityFromStructureConverter $calculationTypeEntityFromStructureConverter
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param ColorPaletteRepository $colorPaletteRepository
     */
    public function __construct(
        CalculationTypeEntityFromStructureConverter $calculationTypeEntityFromStructureConverter,
        EntityFromStructureConverter $entityFromStructureConverter,
        ColorPaletteRepository $colorPaletteRepository
    ) {
        $this->calculationTypeEntityFromStructureConverter = $calculationTypeEntityFromStructureConverter;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->colorPaletteRepository = $colorPaletteRepository;
    }

    /**
     * @param DesignProductionMethodStructure $structure
     * @param DesignProductionMethod|null $entity
     * @param string $entityClassName
     *
     * @return DesignProductionMethod
     *
     * @throws \Exception
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = DesignProductionMethod::class
    ): DesignProductionMethod {
        /** @var DesignProductionMethod $entity */
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        $entity = $this->updateTexts(
            $entity,
            $structure,
            'texts',
            'DesignProductionMethodText'
        );

        $entity = $this->updateManyToManyRelations(
            $entity,
            $structure,
            'DesignProductionMethodColorPalette',
            DesignProductionMethodColorPalette::class,
            'colorPalettes',
            'ColorPalette',
            $this->colorPaletteRepository
        );

        $entity = $this->updateCalculationTypes(
            $entity,
            $structure
        );

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     *
     * @throws \Exception
     */
    public function convertMany($entities, $structureClassName = DesignProductionMethod::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param DesignProductionMethod $entity
     * @param DesignProductionMethodStructure $structure
     * @param string $relationStructureName
     * @param string $relationEntityName
     *
     * @return DesignProductionMethod
     */
    protected function updateTexts(
        DesignProductionMethod $entity,
        DesignProductionMethodStructure $structure,
        string $relationStructureName,
        string $relationEntityName
    ): DesignProductionMethod {
        /** @var DesignProductionMethod $entity */
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        return $entity;
    }

    /**
     * @param DesignProductionMethod $entity
     * @param DesignProductionMethodStructure $structure
     * @param string $relationEntityName
     * @param string $relationEntityClass
     * @param string $relatedStructureName
     * @param string $relatedEntityName
     * @param ColorPaletteRepository $relatedRepository
     *
     * @return DesignProductionMethod
     */
    protected function updateManyToManyRelations(
        DesignProductionMethod $entity,
        DesignProductionMethodStructure $structure,
        string $relationEntityName,
        string $relationEntityClass,
        string $relatedStructureName,
        string $relatedEntityName,
        ColorPaletteRepository $relatedRepository
    ): DesignProductionMethod {
        /** @var DesignProductionMethod $entity */
        $entity = $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
            $structure,
            $entity,
            $relationEntityName,
            $relatedStructureName,
            $relatedEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToManyRelations(
            $structure,
            $entity,
            $relationEntityClass,
            $relatedStructureName,
            $relatedEntityName,
            $relatedRepository
        );

        return $entity;
    }

    /**
     * @param DesignProductionMethod $entity
     * @param DesignProductionMethodStructure $structure
     *
     * @return DesignProductionMethod
     *
     * @throws \Exception
     */
    protected function updateCalculationTypes(
        DesignProductionMethod $entity,
        DesignProductionMethodStructure $structure
    ): DesignProductionMethod {
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            'calculationTypes',
            'DesignerProductionCalculationType'
        );

        $this->calculationTypeEntityFromStructureConverter->setSubEntitiesDeleted(
            $entity->getDesignerProductionCalculationType()
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            'calculationTypes',
            'DesignerProductionCalculationType'
        );

        $this->calculationTypeEntityFromStructureConverter->convertMany(
            $structure->calculationTypes,
            $entity->getDesignerProductionCalculationType()
        );

        return $entity;
    }
}
