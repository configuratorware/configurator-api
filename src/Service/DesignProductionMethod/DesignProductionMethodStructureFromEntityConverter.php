<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethodText;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignProductionMethod\CalculationType\CalculationTypeStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ColorPaletteRelation as ColorPaletteRelationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethod as DesignProductionMethodStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignProductionMethodText as DesignProductionMethodTextStructure;

/**
 * @internal
 */
class DesignProductionMethodStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var CalculationTypeStructureFromEntityConverter
     */
    private $calculationTypeStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(
        CalculationTypeStructureFromEntityConverter $calculationTypeStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->calculationTypeStructureFromEntityConverter = $calculationTypeStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param DesignProductionMethod $entity
     * @param string $structureClassName
     *
     * @return DesignProductionMethodStructure
     */
    public function convertOne(
        $entity,
        $structureClassName = DesignProductionMethodStructure::class
    ): DesignProductionMethodStructure {
        /** @var DesignProductionMethodStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addTextsToStructure(
            $structure,
            $entity,
            'DesignProductionMethod',
            DesignProductionMethodTextStructure::class,
            'texts',
            'Text'
        );

        $structure = $this->addColorPalettesToStructure($structure, $entity);

        $structure->calculationTypes = $this->calculationTypeStructureFromEntityConverter->convertMany(
            $entity->getDesignerProductionCalculationType()
        );

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = DesignProductionMethodStructure::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param DesignProductionMethodStructure $structure
     * @param DesignProductionMethod $entity
     * @param string $entityName
     * @param string $textStructureClass
     * @param string $textStructureName
     * @param string $textEntityName
     *
     * @return DesignProductionMethodStructure
     */
    protected function addTextsToStructure(
        DesignProductionMethodStructure $structure,
        DesignProductionMethod $entity,
        string $entityName,
        string $textStructureClass,
        string $textStructureName,
        string $textEntityName
    ): DesignProductionMethodStructure {
        $callEntity = 'get' . $entityName . $textEntityName;

        /** @var DesignProductionMethod $entity */
        $texts = $entity->$callEntity();

        $result = [];
        if (!empty($texts)) {
            /** @var DesignProductionMethodText $text */
            foreach ($texts as $text) {
                /** @var DesignProductionMethodTextStructure $textStructure */
                $textStructure = $this->structureFromEntityConverter->convertOne(
                    $text,
                    $textStructureClass
                );

                if (!empty($textStructure)) {
                    $textStructure->language = $text->getLanguage()->getIso();
                    $result[] = $textStructure;
                }
            }
        }

        $structure->$textStructureName = $result;

        return $structure;
    }

    /**
     * @param DesignProductionMethodStructure $structure
     * @param DesignProductionMethod $entity
     *
     * @return DesignProductionMethodStructure
     */
    protected function addColorPalettesToStructure($structure, $entity): DesignProductionMethodStructure
    {
        $result = [];

        $colorPalettes = $entity->getDesignProductionMethodColorPalette();

        if (!empty($colorPalettes)) {
            /* @var DesignProductionMethodStructure $textStructure */
            foreach ($colorPalettes as $colorPalette) {
                $relationStructure = $this->structureFromEntityConverter->convertOne(
                    $colorPalette->getColorPalette(),
                    ColorPaletteRelationStructure::class
                );

                $relationStructure->title = $colorPalette->getColorPalette()->getTranslatedTitle();

                if (!empty($relationStructure)) {
                    $result[] = $relationStructure;
                }
            }
        }
        $structure->colorPalettes = $result;

        return $structure;
    }
}
