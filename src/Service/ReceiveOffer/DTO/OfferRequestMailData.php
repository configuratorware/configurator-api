<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO;

use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;

class OfferRequestMailData
{
    private Configuration $configuration;

    private string $shareUrl;

    private ?string $productionZipUrl;

    public function __construct(Configuration $configuration, string $shareUrl, ?string $productionZipUrl)
    {
        $this->configuration = $configuration;
        $this->shareUrl = $shareUrl;
        $this->productionZipUrl = $productionZipUrl;
    }

    public function setConfiguration(Configuration $configuration): void
    {
        $this->configuration = $configuration;
    }

    public function getConfiguration(): Configuration
    {
        return $this->configuration;
    }

    public function setShareUrl(string $shareUrl): void
    {
        $this->shareUrl = $shareUrl;
    }

    public function getShareUrl(): string
    {
        return $this->shareUrl;
    }

    public function setProductionZipUrl(?string $productionZipUrl): void
    {
        $this->productionZipUrl = $productionZipUrl;
    }

    public function getProductionZipUrl(): ?string
    {
        return $this->productionZipUrl;
    }
}
