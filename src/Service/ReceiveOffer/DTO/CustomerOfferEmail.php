<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO;

use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client as ClientDTO;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;

class CustomerOfferEmail
{
    protected const SUBJECT = 'offerRequestCustomerEmail.subject';

    public string $subject;

    public ?string $fromEmailAddress;

    public string $customerEmailAddress;

    public string $configurationCode;

    public string $name;

    public string $shareUrl;

    public \stdClass $additional;

    public function __construct(
        string $subject,
        ?string $fromEmailAddress,
        string $configurationCode,
        string $customerEmailAddress,
        string $name,
        string $shareUrl,
        ?\stdClass $additional = null
    ) {
        $this->subject = $subject;
        $this->fromEmailAddress = $fromEmailAddress;
        $this->configurationCode = $configurationCode;
        $this->customerEmailAddress = $customerEmailAddress;
        $this->name = $name;
        $this->shareUrl = $shareUrl;
        $this->additional = $additional ?? new \stdClass();
    }

    public static function from(
        ClientDTO $clientDTO,
        ReceiveOfferRequestStructure $receiveOfferRequestStructure,
        OfferRequestMailData $offerRequestMailData
    ): self {
        return new self(
            self::SUBJECT,
            $clientDTO->fromEmailAddress,
            $offerRequestMailData->getConfiguration()->getCode(),
            $receiveOfferRequestStructure->emailAddress,
            $receiveOfferRequestStructure->name,
            $offerRequestMailData->getShareUrl(),
            $receiveOfferRequestStructure->additional
        );
    }
}
