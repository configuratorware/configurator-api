<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO;

use Redhotmagma\ConfiguratorApiBundle\Service\Client\DTO\Client as ClientDTO;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;

class ReceiveOfferEmail
{
    protected const SUBJECT = 'receiveOffersRequestMail.subject';

    public string $subject;

    public ?string $fromEmailAddress;

    /**
     * @var string[]
     */
    public array $toEmailAddresses;

    /**
     * @var string[]
     */
    public array $ccEmailAddresses;

    /**
     * @var string[]
     */
    public array $bccEmailAddresses;

    public string $configurationCode;

    public string $customerEmailAddress;

    public string $name;

    public string $company;

    public string $phoneNumber;

    public string $zip;

    public string $city;

    public string $street;

    public string $country;

    public string $comment;

    public string $shareUrl;

    public ?string $productionZipUrl;

    public \stdClass $additional;

    /**
     * @param string[] $toEmailAddresses
     * @param string[] $ccEmailAddresses
     * @param string[] $bccEmailAddresses
     */
    public function __construct(
        string $subject,
        ?string $fromEmailAddress,
        array $toEmailAddresses,
        array $ccEmailAddresses,
        array $bccEmailAddresses,
        string $configurationCode,
        string $customerEmailAddress,
        string $name,
        string $company,
        string $phoneNumber,
        string $zip,
        string $city,
        string $street,
        string $country,
        string $comment,
        string $shareUrl,
        ?string $productionZipUrl,
        ?\stdClass $additional = null
    ) {
        $this->subject = $subject;
        $this->fromEmailAddress = $fromEmailAddress;
        $this->toEmailAddresses = $toEmailAddresses;
        $this->ccEmailAddresses = array_filter($ccEmailAddresses);
        $this->bccEmailAddresses = array_filter($bccEmailAddresses);
        $this->configurationCode = $configurationCode;
        $this->customerEmailAddress = $customerEmailAddress;
        $this->name = $name;
        $this->company = $company;
        $this->phoneNumber = $phoneNumber;
        $this->zip = $zip;
        $this->city = $city;
        $this->street = $street;
        $this->country = $country;
        $this->comment = $comment;
        $this->shareUrl = $shareUrl;
        $this->productionZipUrl = $productionZipUrl;
        $this->additional = $additional ?? new \stdClass();
    }

    public static function from(
        ClientDTO $clientDTO,
        ReceiveOfferRequestStructure $receiveOfferRequestStructure,
        OfferRequestMailData $offerRequestMailData
    ): self {
        $customData = $offerRequestMailData->getConfiguration()->getCustomdata();

        return new self(
            self::SUBJECT,
            $clientDTO->fromEmailAddress,
            $clientDTO->toEmailAddresses,
            $clientDTO->ccEmailAddresses,
            $clientDTO->bccEmailAddresses,
            $offerRequestMailData->getConfiguration()->getCode(),
            $receiveOfferRequestStructure->emailAddress,
            $receiveOfferRequestStructure->name,
            $receiveOfferRequestStructure->company,
            $receiveOfferRequestStructure->phoneNumber,
            $receiveOfferRequestStructure->zip,
            $receiveOfferRequestStructure->city,
            $receiveOfferRequestStructure->street,
            $receiveOfferRequestStructure->country,
            $customData->comment ?? '',
            $offerRequestMailData->getShareUrl(),
            $offerRequestMailData->getProductionZipUrl(),
            $receiveOfferRequestStructure->additional
        );
    }
}
