<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer;

use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\CustomerEmailDataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\DTO\MailAttachment;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\EmailDataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\EmailNotSendException;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\CustomerOfferEmail;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\ReceiveOfferEmail;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @internal
 */
class ReceiveOfferSender
{
    private ?string $emailFromSetting;

    private MailerInterface $mailer;

    private TranslatorInterface $translator;

    public function __construct(string $emailFrom, MailerInterface $mailer, TranslatorInterface $translator)
    {
        $this->emailFromSetting = '' === $emailFrom ? null : $emailFrom;
        $this->mailer = $mailer;
        $this->translator = $translator;
    }

    /**
     * @param MailAttachment[] $mailAttachments
     */
    public function send(ReceiveOfferEmail $receiveOfferEmail, string $htmlTemplate = EmailDataCollectionEvent::DEFAULT_HTML_TEMPLATE, string $textTemplate = EmailDataCollectionEvent::DEFAULT_TEXT_TEMPLATE, array $mailAttachments = []): void
    {
        $to = null !== $this->emailFromSetting ? [$this->emailFromSetting] : $receiveOfferEmail->toEmailAddresses;
        if ([] === $to) {
            throw EmailNotSendException::missingRequiredFields();
        }

        $from = $this->emailFromSetting ?? $receiveOfferEmail->fromEmailAddress;
        if (null === $from) {
            $from = current($to);
        }

        $email = (new TemplatedEmail())
            ->from($from)
            ->to(...$to)
            ->cc(...$receiveOfferEmail->ccEmailAddresses)
            ->bcc(...$receiveOfferEmail->bccEmailAddresses)
            ->subject($this->translator->trans($receiveOfferEmail->subject, ['{{code}}' => $receiveOfferEmail->configurationCode]))
            ->textTemplate($textTemplate)
            ->htmlTemplate($htmlTemplate)
            ->context(['dto' => $receiveOfferEmail]);

        foreach ($mailAttachments as $mailAttachment) {
            $email->attachFromPath($mailAttachment->getFilePath(), $mailAttachment->getFileName(), $mailAttachment->getContentType());
        }

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $exception) {
            throw EmailNotSendException::receiveOffer();
        }
    }

    public function sendCustomerEmail(CustomerOfferEmail $customerEmail, string $htmlTemplate = CustomerEmailDataCollectionEvent::DEFAULT_HTML_TEMPLATE, string $textTemplate = CustomerEmailDataCollectionEvent::DEFAULT_TEXT_TEMPLATE, array $mailAttachments = []): void
    {
        $to = [$customerEmail->customerEmailAddress];

        $from = $this->emailFromSetting ?? $customerEmail->fromEmailAddress;
        if (null === $from) {
            $from = current($to);
        }

        $email = (new TemplatedEmail())
            ->from($from)
            ->to(...$to)
            ->subject($this->translator->trans($customerEmail->subject, ['%{code}' => $customerEmail->configurationCode]))
            ->textTemplate($textTemplate)
            ->htmlTemplate($htmlTemplate)
            ->context(['dto' => $customerEmail]);

        foreach ($mailAttachments as $mailAttachment) {
            $email->attachFromPath($mailAttachment->getFilePath(), $mailAttachment->getFileName(), $mailAttachment->getContentType());
        }

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $exception) {
            throw EmailNotSendException::customerOffer();
        }
    }
}
