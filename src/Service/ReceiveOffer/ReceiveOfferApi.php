<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer;

use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\OfferRequestCustomerEvent;
use Redhotmagma\ConfiguratorApiBundle\Events\ReceiveOffer\ReceiveOfferRequestEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ZipUrlResolver;
use Redhotmagma\ConfiguratorApiBundle\Service\ReceiveOffer\DTO\OfferRequestMailData;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\ShareUrlResolver;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ReceiveOfferRequest as ReceiveOfferRequestStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ReceiveOfferApi
{
    private Client $client;

    private ConfigurationRepository $configurationRepository;

    private EventDispatcherInterface $eventDispatcher;

    private LoggerInterface $logger;

    private ShareUrlResolver $shareUrl;

    private ZipUrlResolver $productionZip;

    public function __construct(
        Client $client,
        ConfigurationRepository $configurationRepository,
        EventDispatcherInterface $eventDispatcher,
        ShareUrlResolver $shareUrl,
        LoggerInterface $logger,
        ZipUrlResolver $productionZip
    ) {
        $this->client = $client;
        $this->configurationRepository = $configurationRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
        $this->shareUrl = $shareUrl;
        $this->productionZip = $productionZip;
    }

    public function request(ReceiveOfferRequestStructure $structure, ControlParameters $controlParameters): bool
    {
        $clientDTO = $this->client->getClient();
        $mailData = $this->collectMailData($structure, $controlParameters);

        $event = new ReceiveOfferRequestEvent($structure, $controlParameters, $clientDTO, $mailData);
        $this->eventDispatcher->dispatch($event, ReceiveOfferRequestEvent::NAME);

        $customerEvent = new OfferRequestCustomerEvent(
            $clientDTO,
            $structure,
            $controlParameters,
            $mailData
        );
        $this->eventDispatcher->dispatch($customerEvent, OfferRequestCustomerEvent::NAME);

        return $event->isMailSent();
    }

    private function collectMailData(ReceiveOfferRequestStructure $structure, ControlParameters $controlParameters): OfferRequestMailData
    {
        $configuration = $this->configurationRepository->findOneByCode($structure->configurationCode);
        $shareUrl = '';

        try {
            $shareUrl = $this->shareUrl->getShareUrl($structure->configurationCode, $controlParameters);
        } catch (NotFoundException $exception) {
            $this->logger->error('Settings not found', [$exception->getTrace()]);
        }

        $productionZipUrl = $configuration->getItem()->isDesigner()
            ? $this->productionZip->getProductionZipUrl($structure->configurationCode, $controlParameters)
            : null;

        return new OfferRequestMailData($configuration, $shareUrl, $productionZipUrl);
    }
}
