<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;

/**
 * @internal
 */
class ItemOptionClassificationSorter
{
    /**
     * @param ItemOptionclassification[] $itemOptionClassifications
     */
    public function sortAsc(array &$itemOptionClassifications): void
    {
        usort($itemOptionClassifications, static function (ItemOptionclassification $a, ItemOptionclassification $b) {
            if (null === $b->getSequenceNumber()) {
                return -1;
            }

            return $a->getSequenceNumber() <=> $b->getSequenceNumber();
        });
    }
}
