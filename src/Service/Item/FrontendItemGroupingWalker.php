<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;

/**
 * Returns the Children of all Item Groups in a 1 Dimensional array.
 *
 * @internal
 */
class FrontendItemGroupingWalker
{
    /**
     * @param Item $item
     *
     * @return array|Item[]
     */
    public function walk(Item $item): array
    {
        $childrenFlat = [];

        $children = $this->getItemGroupChildren($item->itemGroup);

        if (!is_array($children)) {
            return [];
        }

        array_walk_recursive(
            $children,
            function ($item) use (&$childrenFlat) {
                $childrenFlat[] = $item;
            }
        );

        return $childrenFlat;
    }

    /**
     * @param mixed $upper
     *
     * @return mixed
     */
    private function getItemGroupChildren($upper)
    {
        $children = [];

        if (empty($upper->itemGroup) && !isset($upper->children)) {
            return $upper;
        }

        if (!empty($upper->children)) {
            foreach ($upper->children as $child) {
                $children[] = $this->getItemGroupChildren($child);
            }
        } elseif (!empty($upper->itemGroup)) {
            $children[] = $this->getItemGroupChildren($upper->itemGroup);
        }

        return $children;
    }
}
