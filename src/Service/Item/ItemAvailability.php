<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Events\Item\CheckItemAvailabilityEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class ItemAvailability
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * ItemAvailability constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Item $item
     * @param ControlParameters|null $controlParameters
     *
     * @return bool
     */
    public function checkAvailability(Item $item, ControlParameters $controlParameters = null): bool
    {
        $event = new CheckItemAvailabilityEvent($item, $controlParameters);

        $this->eventDispatcher->dispatch($event, CheckItemAvailabilityEvent::NAME);

        return $event->isAvailable();
    }
}
