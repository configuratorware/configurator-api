<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\FrontendAttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\AttributeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ChildItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item as FrontendItemStructure;

/**
 * @internal
 */
class FrontendItemGrouping
{
    /**
     * @var FrontendAttributeRelationStructureFromEntityConverter
     */
    private $attributeRelationStructureFromEntityConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemgroupRepository
     */
    private $itemGroupRepository;

    /**
     * @var ItemgroupentryRepository
     */
    private $itemGroupEntryRepository;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * FrontendItemGrouping constructor.
     *
     * @param FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
     * @param ItemRepository $itemRepository
     * @param ItemgroupRepository $itemGroupRepository
     * @param ItemgroupentryRepository $itemGroupEntryRepository
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter,
        ItemRepository $itemRepository,
        ItemgroupRepository $itemGroupRepository,
        ItemgroupentryRepository $itemGroupEntryRepository,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
        $this->itemRepository = $itemRepository;
        $this->itemGroupRepository = $itemGroupRepository;
        $this->itemGroupEntryRepository = $itemGroupEntryRepository;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param FrontendItemStructure $structure
     * @param Item $entity
     *
     * @return FrontendItemStructure
     */
    public function addItemGroupsToStructure(FrontendItemStructure $structure, Item $entity): FrontendItemStructure
    {
        $parent = $entity;

        if (null !== $entity->getParent()) {
            $parent = $entity->getParent();
        }

        if (!$parent) {
            return $structure;
        }

        $children = $this->itemRepository->findActiveByParent($parent->getIdentifier());

        $childStructures = [];
        /** @var Item $childItem */
        foreach ($children as $childItem) {
            /** @var ChildItem $childStructure */
            $childStructure = $this->structureFromEntityConverter->convertOne($childItem, ChildItem::class);
            $childStructure->title = $childItem->getTranslatedTitle();
            $childStructure->attributes = $this->attributeRelationStructureFromEntityConverter->convertMany(
                $childItem->getItemattribute(),
                AttributeRelation::class
            );
            $childStructures[] = $childStructure;
        }

        $structure->itemGroup = $this->groupItemsByGroups($childStructures);

        return $structure;
    }

    /**
     * @param array $itemStructures
     *
     * @return array
     */
    public function groupItemsByGroups($itemStructures)
    {
        $groupedItemStructures = [];
        $indexedItemStructures = [];

        $itemIdentifiers = [];
        foreach ($itemStructures as $itemStructure) {
            $itemIdentifiers[] = $itemStructure->identifier;
            $indexedItemStructures[$itemStructure->identifier] = $itemStructure;
        }

        $result = $this->itemGroupRepository->getItemGroupMapping($itemIdentifiers);

        // if mapping entries where found, build items tree from he mapping data
        // else return the items list as is
        if (!empty($result)) {
            $currentParent = null;
            $currentGroup = null;
            $currentEntry = null;
            $currentOption = null;
            $mappingKeys = [];

            $mapping = [];

            foreach ($result as $row) {
                if ($currentParent != $row['parent_identifier'] || $currentOption != $row['item_identifier']) {
                    $currentParent = $row['parent_identifier'];
                    $mappingKeys = [$currentParent];
                    $mappingKeys[] = $row['group_identifier'];
                    $mappingKeys[] = $row['group_entry_identifier'];

                    $currentOption = $row['item_identifier'];
                    $currentGroup = $row['group_identifier'];
                    $currentEntry = $row['group_entry_identifier'];
                } else {
                    if ($currentGroup != $row['group_identifier'] && $currentEntry != $row['group_entry_identifier']) {
                        $mappingKeys = array_slice($mappingKeys, 0, count($mappingKeys) - 1);

                        $mappingKeys[] = $currentEntry;
                        $mappingKeys[] = $row['group_identifier'];
                        $mappingKeys[] = $row['group_entry_identifier'];

                        $currentGroup = $row['group_identifier'];
                        $currentEntry = $row['group_entry_identifier'];
                    }
                }

                $check = $this->multiDimensionArrayValue($mapping, $mappingKeys);

                if (empty($check)) {
                    $this->multiDimensionArrayValue($mapping, $mappingKeys, $row['item_identifier']);
                }
            }

            // iterate over the mapping to build the items structure
            $distributedItems = [];
            foreach ($mapping as $parentIdentifier => $itemMapping) {
                $groupedItemStructures = array_merge($groupedItemStructures,
                    $this->transformItemMapping($itemMapping, $indexedItemStructures, $distributedItems));
            }

            // add items that where not added by grouping
            foreach ($itemStructures as $item) {
                if (!in_array($item->identifier, $distributedItems)) {
                    $groupedItemStructures[] = $item;
                }
            }
        } else {
            $groupedItemStructures = $itemStructures;
        }

        $group = null;
        if (!empty($groupedItemStructures) && isset($groupedItemStructures[0]->itemGroup)) {
            $group = $groupedItemStructures[0]->itemGroup;
        }

        return $group;
    }

    /**
     * @param array $itemsMapping
     * @param array $indexedItemStructures
     * @param array $distributedItems
     *
     * @return array
     */
    protected function transformItemMapping($itemsMapping, $indexedItemStructures, &$distributedItems)
    {
        $items = [];
        foreach ($itemsMapping as $groupIdentifier => $childMappings) {
            if (is_array($childMappings)) {
                // find first leaf and get item structure
                $itemIdentifiers = new \ArrayObject();
                array_walk_recursive($childMappings, function ($value, $key, $itemIdentifiers) {
                    $itemIdentifiers[] = $value;
                }, $itemIdentifiers);
                $item = unserialize(serialize($indexedItemStructures[$itemIdentifiers[0]]));

                $children = [];
                foreach ($childMappings as $groupEntryIdentifier => $childGroupMapping) {
                    if (is_array($childGroupMapping)) {
                        $groupChildren = $this->transformItemMapping($childGroupMapping,
                            $indexedItemStructures, $distributedItems);
                        $child = $groupChildren[0];
                    } else {
                        $child = $indexedItemStructures[$childGroupMapping];
                    }
                    $child->itemgroupentryIdentifier = $groupEntryIdentifier;

                    $itemGroupEntryEntity = $this->itemGroupEntryRepository->findOneBy(['identifier' => $groupEntryIdentifier]);
                    $child->itemgroupentryTitle = $itemGroupEntryEntity->getTranslatedTitle();

                    $children[] = $child;
                    $distributedItems[$child->identifier] = $child->identifier;
                }

                $itemGroupEntity = $this->itemGroupRepository->findOneBy(['identifier' => $groupIdentifier]);
                if (null !== $itemGroupEntity) {
                    $itemGroup = new \stdClass();
                    $itemGroup->identifier = $groupIdentifier;
                    $itemGroup->title = $itemGroupEntity->getTranslatedTitle();
                    $itemGroup->children = $children;

                    $item->itemGroup = $itemGroup;
                }

                $items[] = $item;
            }
        }

        return $items;
    }

    /**
     * sets or gets the value in an multidimensional array by an array of keys
     * if no value is given the current value is returned.
     * in either case the path is created.
     *
     * @param $array
     * @param array $path
     * @param null $value
     *
     * @return mixed
     */
    private function multiDimensionArrayValue(&$array, $path = [], &$value = null)
    {
        $args = func_get_args();
        $ref = &$array;
        foreach ($path as $key) {
            if (!is_array($ref)) {
                $ref = [];
            }
            $ref = &$ref[$key];
        }
        $prev = $ref;
        if (array_key_exists(2, $args)) {
            $ref = $value;
        }

        return $prev;
    }
}
