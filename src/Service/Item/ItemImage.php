<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ConfiguratorApiBundle\FileRepository\LayerImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Image\LayerImage;
use Redhotmagma\ConfiguratorApiBundle\Repository\CreatorViewRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DeleteLayerImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadLayerImage;

/**
 * @internal
 */
final class ItemImage
{
    /**
     * @var CreatorViewRepository
     */
    private $creatorViewRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var OptionclassificationRepository
     */
    private $optionClassificationRepository;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    /**
     * @var LayerImageRepository
     */
    private $layerImageRepository;

    /**
     * @param CreatorViewRepository $creatorViewRepository
     * @param ItemRepository $itemRepository
     * @param LayerImageRepository $layerImageRepository
     * @param OptionclassificationRepository $optionClassificationRepository
     * @param OptionRepository $optionRepository
     */
    public function __construct(CreatorViewRepository $creatorViewRepository, ItemRepository $itemRepository, LayerImageRepository $layerImageRepository, OptionclassificationRepository $optionClassificationRepository, OptionRepository $optionRepository)
    {
        $this->creatorViewRepository = $creatorViewRepository;
        $this->itemRepository = $itemRepository;
        $this->optionClassificationRepository = $optionClassificationRepository;
        $this->optionRepository = $optionRepository;
        $this->layerImageRepository = $layerImageRepository;
    }

    /**
     * @param UploadLayerImage $uploadLayerImage
     */
    public function uploadLayerImage(UploadLayerImage $uploadLayerImage): void
    {
        $item = $this->itemRepository->find($uploadLayerImage->itemId);
        $component = $this->optionClassificationRepository->find($uploadLayerImage->componentId);
        $option = $this->optionRepository->find($uploadLayerImage->optionId);
        $creatorView = $this->creatorViewRepository->find($uploadLayerImage->creatorViewId);

        $layerImage = LayerImage::from(null, $component->getIdentifier(), $option->getIdentifier(), $creatorView->getIdentifier(), $uploadLayerImage->realPath, $uploadLayerImage->fileName, (int)$component->getSequencenumber(), $creatorView->getSequenceNumber());

        $this->layerImageRepository->saveLayerImage($layerImage, $item);
    }

    /**
     * @param DeleteLayerImage $deleteLayerImage
     */
    public function deleteLayerImage(DeleteLayerImage $deleteLayerImage): void
    {
        $item = $this->itemRepository->find($deleteLayerImage->itemId);
        $component = $this->optionClassificationRepository->find($deleteLayerImage->componentId);
        $option = $this->optionRepository->find($deleteLayerImage->optionId);
        $creatorView = $this->creatorViewRepository->find($deleteLayerImage->creatorViewId);

        $layerImage = $this->layerImageRepository->findLayerImage($item, $component, $option, $creatorView);

        $this->layerImageRepository->deleteLayerImage($layerImage, $item);
    }
}
