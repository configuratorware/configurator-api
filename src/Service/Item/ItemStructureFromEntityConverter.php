<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemClassification\ItemClassificationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemSetting\ItemSettingFactory;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Itemclassification as FrontendItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemList;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Structure\Itemtext;
use Redhotmagma\ConfiguratorApiBundle\Structure\Stock;

/**
 * @internal
 */
class ItemStructureFromEntityConverter
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var ItemClassificationStructureFromEntityConverter
     */
    private $itemClassificationStructureFromEntityConverter;

    /**
     * @var ItemSettingFactory
     */
    private $itemSettingFactory;

    /**
     * ItemStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter
     * @param ItemSettingFactory $itemSettingFactory
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        ItemClassificationStructureFromEntityConverter $itemClassificationStructureFromEntityConverter,
        ItemSettingFactory $itemSettingFactory
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->itemClassificationStructureFromEntityConverter = $itemClassificationStructureFromEntityConverter;
        $this->itemSettingFactory = $itemSettingFactory;
    }

    /**
     * @param Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     *
     * @return Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item $structure
     */
    public function addItemSettingToStructure($structure, \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity)
    {
        $structure->settings = $this->itemSettingFactory->createRelationStructure($entity);

        return $structure;
    }

    /**
     * @param Item $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     *
     * @return Item
     */
    public function addItemTextsToStructure($structure, $entity)
    {
        $texts = [];

        $itemTexts = $entity->getItemtext();

        if (!empty($itemTexts)) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext $itemText */
            foreach ($itemTexts as $itemText) {
                $itemTextStructure = $this->structureFromEntityConverter->convertOne(
                    $itemText,
                    ItemText::class
                );

                if (!empty($itemTextStructure)) {
                    $itemTextStructure->language = $itemText->getLanguage()
                        ->getIso();
                    $texts[] = $itemTextStructure;
                }
            }
        }

        $structure->texts = $texts;

        return $structure;
    }

    /**
     * @param Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     *
     * @return Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item
     */
    public function addItemPriceToStructure($structure, $entity)
    {
        $prices = [];

        $itemPrices = $entity->getItemPrice();
        if (!$itemPrices->isEmpty()) {
            foreach ($itemPrices as $itemPrice) {
                $priceStructure = $this->structureFromEntityConverter->convertOne($itemPrice, Itemprice::class);
                $priceStructure->channel = $itemPrice->getChannel()->getId();

                $prices[] = $priceStructure;
            }
        }

        $structure->prices = $prices;

        return $structure;
    }

    /**
     * @param Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     *
     * @return Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item
     */
    public function addStockToStructure($structure, $entity)
    {
        $stock = [];

        $stocks = $entity->getStock();

        if (!empty($stocks)) {
            foreach ($stocks as $stockEntity) {
                $stockStructure = $this->structureFromEntityConverter->convertOne($stockEntity, Stock::class);

                $stockStructure->channel = $stockEntity->getChannel()->getId();

                $stock[] = $stockStructure;
            }
        }

        $structure->stock = $stock;

        return $structure;
    }

    /**
     * @param Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item|ItemList $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     *
     * @return Item|\Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item|ItemList
     */
    public function addItemClassificationsToStructure($structure, $entity)
    {
        /**
         * @var FrontendItemClassification[] $itemClassifications
         */
        $itemClassifications = [];

        $itemItemClassifications = $entity->getItemItemclassification();
        if (!empty($itemItemClassifications)) {
            /** @var ItemItemclassification $itemItemClassification */
            foreach ($itemItemClassifications as $itemItemClassification) {
                $itemClassification = $itemItemClassification->getItemclassification();

                /**
                 * @var FrontendItemClassification $itemClassificationStructure
                 */
                $itemClassificationStructure =
                    $this->itemClassificationStructureFromEntityConverter->convertOne(
                        $itemClassification,
                        ItemClassification::class
                    );
                $itemClassifications[] = $itemClassificationStructure;
            }
        }

        $structure->itemclassifications = $itemClassifications;

        return $structure;
    }
}
