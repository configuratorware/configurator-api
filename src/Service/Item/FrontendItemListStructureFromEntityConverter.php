<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Events\Item\FrontendItemListStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\FrontendAttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\FormatterService;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\VatCalculationService;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationImageProvider;
use Redhotmagma\ConfiguratorApiBundle\Structure\AttributeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemList;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class FrontendItemListStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    private StructureFromEntityConverter $structureFromEntityConverter;

    private FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter;

    private ItemStructureFromEntityConverter $itemStructureFromEntityConverter;

    private EventDispatcherInterface $eventDispatcher;

    private ConfigurationImageProvider $configurationImageProvider;

    private VatCalculationService $vatCalculationService;

    private FormatterService $formatterService;

    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter,
        ItemStructureFromEntityConverter $itemStructureFromEntityConverter,
        EventDispatcherInterface $eventDispatcher,
        ConfigurationImageProvider $configurationImageProvider,
        VatCalculationService $vatCalculationService,
        FormatterService $formatterService
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
        $this->itemStructureFromEntityConverter = $itemStructureFromEntityConverter;
        $this->eventDispatcher = $eventDispatcher;
        $this->configurationImageProvider = $configurationImageProvider;
        $this->vatCalculationService = $vatCalculationService;
        $this->formatterService = $formatterService;
    }

    /**
     * @param Item $entity
     * @param class-string $structureClassName
     *
     * @return ItemList
     */
    public function convertOne($entity, $structureClassName = ItemList::class): ItemList
    {
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $this->addItemAttributesToStructure($structure, $entity);
        $this->addItemImagesToStructure($structure, $entity);
        $this->addItemPriceToStructure($structure, $entity);
        $this->itemStructureFromEntityConverter->addItemClassificationsToStructure($structure, $entity);

        $structure->title = $entity->getTranslatedTitle();
        $structure->abstract = $entity->getTranslatedAbstract();
        $structure->description = $entity->getTranslatedDescription();

        $event = new FrontendItemListStructureFromEntityEvent($entity, $structure);
        $this->eventDispatcher->dispatch($event, FrontendItemListStructureFromEntityEvent::NAME);

        return $event->getStructure();
    }

    /**
     * @param Item[] $entities
     * @param class-string $structureClassName
     *
     * @return ItemList[]
     */
    public function convertMany($entities, $structureClassName = ItemList::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }

    /**
     * @param ItemList $structure
     * @param Item $entity
     */
    protected function addItemAttributesToStructure(ItemList $structure, Item $entity): void
    {
        $itemAttributes = $entity->getItemattribute()->toArray();
        usort($itemAttributes, static function (ItemAttribute $a, ItemAttribute $b) {
            if (null === $a->getSequencenumber()) {
                return 1;
            }
            if (null === $b->getSequencenumber()) {
                return -1;
            }

            return (int)$a->getSequencenumber() - (int)$b->getSequencenumber();
        });

        $attributesRelationStructures = $this->attributeRelationStructureFromEntityConverter->convertMany(
            $itemAttributes,
            AttributeRelation::class
        );

        $structure->attributes = $attributesRelationStructures;
    }

    private function addItemImagesToStructure(ItemList $structure, Item $entity): void
    {
        $detailImageUrlMap = $this->configurationImageProvider->provideDetailImageUrlMap($entity);
        $thumbnailUrlMap = $this->configurationImageProvider->provideThumbnailUrlMap($entity);

        $structure->detailImage = $detailImageUrlMap[$entity->getIdentifier()];
        $structure->thumbnail = $thumbnailUrlMap[$entity->getIdentifier()];
    }

    protected function addItemPriceToStructure(ItemList $structure, Item $entity): void
    {
        $itemPrice = null;
        $itemPriceNet = null;
        if (null !== $entity->getCurrentChannelItemprice()) {
            $itemPrice = $entity->getCurrentChannelItemprice()->getPrice();
            $itemPriceNet = $entity->getCurrentChannelItemprice()->getPriceNet();
        }
        if (null !== $itemPrice) {
            $this->vatCalculationService->addPricesToStructure($structure, (float)$itemPrice, (float)$itemPriceNet);
        }
    }
}
