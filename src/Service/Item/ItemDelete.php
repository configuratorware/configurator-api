<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignArea\DesignAreaDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewDelete;

/**
 * @internal
 */
class ItemDelete
{
    /**
     * @var ConfigurationRepository
     */
    private $configurationRepository;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var DesignViewDelete
     */
    private $designViewDelete;

    /**
     * @var DesignAreaDelete
     */
    private $designAreaDelete;

    /**
     * @var CreatorViewDelete
     */
    private $creatorViewDelete;

    /**
     * ItemDelete constructor.
     *
     * @param ConfigurationRepository $configurationRepository
     * @param ItemRepository $itemRepository
     * @param DesignViewDelete $designViewDelete
     * @param DesignAreaDelete $designAreaDelete
     * @param CreatorViewDelete $creatorViewDelete
     */
    public function __construct(
        ConfigurationRepository $configurationRepository,
        ItemRepository $itemRepository,
        DesignViewDelete $designViewDelete,
        DesignAreaDelete $designAreaDelete,
        CreatorViewDelete $creatorViewDelete
    ) {
        $this->configurationRepository = $configurationRepository;
        $this->itemRepository = $itemRepository;
        $this->designViewDelete = $designViewDelete;
        $this->designAreaDelete = $designAreaDelete;
        $this->creatorViewDelete = $creatorViewDelete;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var Item $entity */
            $entity = $this->itemRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            // delete item text relations
            if (!empty($entity->getItemtext()->toArray())) {
                foreach ($entity->getItemtext() as $itemText) {
                    $this->itemRepository->delete($itemText);
                }
            }

            // delete item stock
            if (!empty($entity->getStock()->toArray())) {
                foreach ($entity->getStock()->toArray() as $stock) {
                    $this->itemRepository->delete($stock);
                }
            }

            // delete item attribute relations
            if (!empty($entity->getItemAttribute()->toArray())) {
                foreach ($entity->getItemAttribute()->toArray() as $itemAttribute) {
                    $this->itemRepository->delete($itemAttribute);
                }
            }

            // delete item classifications relations
            if (!empty($entity->getItemItemclassification()->toArray())) {
                foreach ($entity->getItemItemclassification()->toArray() as $itemItemClassification) {
                    $this->itemRepository->delete($itemItemClassification);
                }
            }

            // delete item prices
            if (!empty($entity->getItemprice()->toArray())) {
                foreach ($entity->getItemprice()->toArray() as $itemPrice) {
                    $this->itemRepository->delete($itemPrice);
                }
            }

            // delete construction pattern
            if (!empty($entity->getItemOptionclassification()->toArray())) {
                /** @var ItemOptionclassification $itemOptionClassification */
                foreach ($entity->getItemOptionclassification() as $itemOptionClassification) {
                    if (!empty($itemOptionClassification->getItemOptionclassificationOption()->toArray())) {
                        /** @var ItemOptionclassificationOption $itemOptionClassificationOption */
                        foreach ($itemOptionClassification->getItemOptionclassificationOption() as $itemOptionClassificationOption) {
                            if (!empty($itemOptionClassificationOption->getItemOptionclassificationOptionDeltaprice()->toArray())) {
                                foreach ($itemOptionClassificationOption->getItemOptionclassificationOptionDeltaprice() as $deltaPrice) {
                                    $this->itemRepository->delete($deltaPrice);
                                }
                            }

                            $this->itemRepository->delete($itemOptionClassificationOption);
                        }
                    }

                    $this->itemRepository->delete($itemOptionClassification);
                }
            }

            // delete configurations
            $configurations = $this->configurationRepository->findBy(['item' => $entity]);
            if (!empty($configurations)) {
                foreach ($configurations as $configuration) {
                    $this->configurationRepository->delete($configuration);
                }
            }

            // delete design views
            if ($entity->getDesignView()->count()) {
                foreach ($entity->getDesignView() as $designView) {
                    $this->designViewDelete->delete($designView->getId());
                }
            }

            // delete design areas
            if ($entity->getDesignArea()->count()) {
                foreach ($entity->getDesignArea() as $designArea) {
                    $this->designAreaDelete->delete($designArea->getId());
                }
            }

            // delete designer global item prices
            if ($entity->getDesignerGlobalItemPrice()->count()) {
                foreach ($entity->getDesignerGlobalItemPrice() as $price) {
                    $this->itemRepository->delete($price);
                }
            }

            // delete item pool entries
            foreach ($entity->getItemItemPool() as $itemPool) {
                $this->itemRepository->delete($itemPool);
            }

            // delete item's creator views
            foreach ($entity->getCreatorView() as $creatorView) {
                $this->creatorViewDelete->deleteEntity($creatorView);
            }
            $this->itemRepository->delete($entity);
        }
    }
}
