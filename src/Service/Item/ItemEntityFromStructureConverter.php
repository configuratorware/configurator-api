<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\VisualizationModeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Itemprice;

/**
 * @internal
 */
class ItemEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var AttributeRelationEntityFromStructureConverter
     */
    private $attributeRelationEntityFromStructureConverter;

    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var ItemclassificationRepository
     */
    private $itemClassificationRepository;

    /**
     * @var ItemStatusRepository
     */
    private $itemStatusRepository;

    /**
     * @var VisualizationModeRepository
     */
    private $visualizationModeRepository;

    /**
     * ItemEntityFromStructureConverter constructor.
     *
     * @param AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter
     * @param ChannelRepository $channelRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     * @param ItemclassificationRepository $itemClassificationRepository
     * @param ItemStatusRepository $itemStatusRepository
     * @param VisualizationModeRepository $visualizationModeRepository
     */
    public function __construct(
        AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter,
        ChannelRepository $channelRepository,
        EntityFromStructureConverter $entityFromStructureConverter,
        ItemclassificationRepository $itemClassificationRepository,
        ItemStatusRepository $itemStatusRepository,
        VisualizationModeRepository $visualizationModeRepository
    ) {
        $this->attributeRelationEntityFromStructureConverter = $attributeRelationEntityFromStructureConverter;
        $this->channelRepository = $channelRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->itemClassificationRepository = $itemClassificationRepository;
        $this->itemStatusRepository = $itemStatusRepository;
        $this->visualizationModeRepository = $visualizationModeRepository;
    }

    /**
     * @param Item $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\Item::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->updateItemTexts($entity, $localStructure);
        $entity = $this->updateItemPrices($entity, $localStructure);
        $entity = $this->updateItemStock($entity, $localStructure);
        $entity = $this->updateItemClassifications($entity, $localStructure);
        $entity = $this->updateItemAttributes($entity, $localStructure);

        if (isset($structure->itemStatus->id)) {
            $entity->setItemStatus($this->itemStatusRepository->findOneById($structure->itemStatus->id));
        }

        if (isset($structure->visualizationMode->id)) {
            $entity->setVisualizationMode($this->visualizationModeRepository->findOneById($structure->visualizationMode->id));
        }

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Item::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     * @param Item $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     */
    protected function updateItemTexts($item, $structure)
    {
        $item = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $item, 'texts',
            'Itemtext');
        $item = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $item, 'texts',
            'Itemtext');

        return $item;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     * @param Item $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     */
    protected function updateItemPrices($item, $structure)
    {
        // update itemprices, add channel relation objects
        if (!empty($structure->prices)) {
            /** @var Itemprice $priceStructure */
            foreach ($structure->prices as $i => $priceStructure) {
                if (!empty($priceStructure->channel)) {
                    $structure->prices[$i]->channel = $this->channelRepository->findOneBy(['id' => $priceStructure->channel]);
                }

                // amountfrom has  to be a number bigger than 0
                if (empty((int)$priceStructure->amountfrom) || (int)$priceStructure->amountfrom < 1) {
                    $priceStructure->amountfrom = 1;
                }
            }
        }

        $item = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $item, 'prices',
            'Itemprice');
        $item = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $item, 'prices',
            'Itemprice');

        return $item;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     * @param Item $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     */
    protected function updateItemStock($item, $structure)
    {
        // update stock, add channel relation objects
        if (!empty($structure->stock)) {
            foreach ($structure->stock as $i => $stockStructure) {
                if (!empty($stockStructure->channel)) {
                    $structure->stock[$i]->channel = $this->channelRepository->findOneBy(['id' => $stockStructure->channel]);
                }
            }
        }

        $item = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $item, 'stock',
            'Stock');
        $item = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $item, 'stock', 'Stock');

        return $item;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     * @param Item $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     */
    protected function updateItemClassifications($item, $structure)
    {
        // update itemclassifications
        $item = $this->entityFromStructureConverter->setManyToManyRelationsDeleted(
            $structure,
            $item,
            'ItemItemclassification',
            'itemclassifications',
            'Itemclassification'
        );

        $item =
            $this->entityFromStructureConverter->addNewManyToManyRelations(
                $structure,
                $item,
                ItemItemclassification::class,
                'itemclassifications',
                'Itemclassification',
                $this->itemClassificationRepository
            );

        return $item;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     * @param Item $structure
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item
     */
    protected function updateItemAttributes($item, $structure)
    {
        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item */
        $item = $this->attributeRelationEntityFromStructureConverter
            ->setManyToManyRelationsDeleted(
                $structure,
                $item,
                'ItemAttribute',
                'attributes',
                'Attribute'
            );

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Item $item */
        $item = $this->attributeRelationEntityFromStructureConverter
            ->addNewManyToManyRelations(
                $structure,
                $item,
                ItemAttribute::class,
                'attributes'
            );

        return $item;
    }
}
