<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChildItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupEntry;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupRelation;

/**
 * @internal
 */
class ChildItemStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * ChildItemStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     * @param string $structureClassName
     *
     * @return ChildItem
     */
    public function convertOne($entity, $structureClassName = ChildItem::class): ChildItem
    {
        /** @var ChildItem $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $structure->title = $entity->getTranslatedTitle();

        $structure = $this->addGroupRelationsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ChildItem::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param ChildItem $structure
     * @param Item $entity
     *
     * @return ChildItem
     */
    protected function addGroupRelationsToStructure(ChildItem $structure, Item $entity)
    {
        $itemGroupEntries = $entity->getItemItemgroupentry();

        $groupRelations = [];
        /** @var ItemItemgroupentry $itemGroupEntry */
        foreach ($itemGroupEntries as $itemGroupEntry) {
            /** @var GroupRelation $groupRelation */
            $groupRelation = $this->structureFromEntityConverter->convertOne($itemGroupEntry->getItemgroup(),
                GroupRelation::class);
            $groupRelation->translation = $itemGroupEntry->getItemgroup()->getTranslatedTitle();

            $groupRelation->groupEntry = $this->structureFromEntityConverter->convertOne($itemGroupEntry->getItemgroupentry(),
                GroupEntry::class);
            $groupRelation->groupEntry->translation = $itemGroupEntry->getItemgroupentry()->getTranslatedTitle();

            $groupRelations[] = $groupRelation;
        }

        $structure->groups = $groupRelations;

        return $structure;
    }
}
