<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;
use Redhotmagma\ConfiguratorApiBundle\Structure\AttributeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item as ItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus as ItemStatusStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\VisualizationMode as VisualizationComponentStructure;

/**
 * @internal
 */
class AdminAreaItemStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var AttributeRelationStructureFromEntityConverter
     */
    private $attributeRelationStructureFromEntityConverter;

    /**
     * @var ChildItemStructureFromEntityConverter
     */
    private $childItemStructureFromEntityConverter;

    /**
     * @var ItemStructureFromEntityConverter
     */
    private $itemStructureFromEntityConverter;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var Client
     */
    private $client;

    /**
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
     * @param ChildItemStructureFromEntityConverter $childItemStructureFromEntityConverter
     * @param ItemStructureFromEntityConverter $itemStructureFromEntityConverter
     * @param ItemRepository $itemRepository
     * @param Client $client
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter,
        ChildItemStructureFromEntityConverter $childItemStructureFromEntityConverter,
        ItemStructureFromEntityConverter $itemStructureFromEntityConverter,
        ItemRepository $itemRepository,
        Client $client
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
        $this->childItemStructureFromEntityConverter = $childItemStructureFromEntityConverter;
        $this->itemStructureFromEntityConverter = $itemStructureFromEntityConverter;
        $this->itemRepository = $itemRepository;
        $this->client = $client;
    }

    /**
     * @param Item $entity
     * @param string $structureClassName
     *
     * @return ItemStructure
     */
    public function convertOne($entity, $structureClassName = ItemStructure::class): ItemStructure
    {
        /** @var ItemStructure $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);
        $structure = $this->itemStructureFromEntityConverter->addItemTextsToStructure($structure, $entity);
        $structure = $this->itemStructureFromEntityConverter->addItemPriceToStructure($structure, $entity);
        $structure = $this->itemStructureFromEntityConverter->addStockToStructure($structure, $entity);
        $structure = $this->itemStructureFromEntityConverter->addItemClassificationsToStructure($structure, $entity);
        $structure = $this->addChildrenToStructure($structure, $entity);

        $structure->attributes = $this->attributeRelationStructureFromEntityConverter->convertMany(
            $entity->getItemAttribute()->toArray(),
            AttributeRelation::class
        );

        if (null !== $entity->getItemStatus()) {
            $structure->itemStatus = $this->structureFromEntityConverter->convertOne(
                $entity->getItemStatus(),
                ItemStatusStructure::class
            );
        }

        if (null !== $entity->getVisualizationMode()) {
            $structure->visualizationMode = $this->structureFromEntityConverter->convertOne(
                $entity->getVisualizationMode(),
                VisualizationComponentStructure::class
            );
        }

        return $structure;
    }

    /**
     * @param Item[] $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ItemStructure::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }

    /**
     * @param ItemStructure $structure
     * @param Item $entity
     *
     * @return ItemStructure
     */
    protected function addChildrenToStructure(ItemStructure $structure, Item $entity): ItemStructure
    {
        $childItemStructures = [];

        $children = $this->itemRepository->findBy(['parent' => $entity]);
        if (!empty($children)) {
            $childItemStructures = $this->childItemStructureFromEntityConverter->convertMany($children);
        }
        $structure->children = $childItemStructures;

        return $structure;
    }
}
