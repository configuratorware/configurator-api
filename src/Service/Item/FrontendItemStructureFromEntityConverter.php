<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\Client as ClientEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item as ItemEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Events\Item\FrontendItemStructureFromEntityEvent;
use Redhotmagma\ConfiguratorApiBundle\Repository\ClientRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationModeRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\FrontendAttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\VatCalculationService;
use Redhotmagma\ConfiguratorApiBundle\Service\Client\Client;
use Redhotmagma\ConfiguratorApiBundle\Structure\AttributeRelation;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ConfigurationMode as ConfigurationModeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Item;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ItemStatus as ItemStatusFrontendStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class FrontendItemStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var ConfigurationModeRepository
     */
    private $configurationModeRepository;

    /**
     * @var FrontendAttributeRelationStructureFromEntityConverter
     */
    private $attributeRelationStructureFromEntityConverter;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var FrontendItemGrouping
     */
    private $frontendItemGrouping;

    /**
     * @var ItemStructureFromEntityConverter
     */
    private $itemStructureFromEntityConverter;

    /**
     * @var VatCalculationService
     */
    private $vatCalculationService;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var ClientRepository
     */
    private $clientRepository;

    /**
     * @var ControlParameters|null
     */
    private $controlParameters;

    /**
     * FrontendItemStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param ConfigurationModeRepository $configurationModeRepository
     * @param FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
     * @param EventDispatcherInterface $eventDispatcher
     * @param FrontendItemGrouping $frontendItemGrouping
     * @param ItemStructureFromEntityConverter $itemStructureFromEntityConverter
     * @param VatCalculationService $vatCalculationService
     * @param Client $client
     * @param ClientRepository $clientRepository
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        ConfigurationModeRepository $configurationModeRepository,
        FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter,
        EventDispatcherInterface $eventDispatcher,
        FrontendItemGrouping $frontendItemGrouping,
        ItemStructureFromEntityConverter $itemStructureFromEntityConverter,
        VatCalculationService $vatCalculationService,
        Client $client,
        ClientRepository $clientRepository
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->configurationModeRepository = $configurationModeRepository;
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
        $this->eventDispatcher = $eventDispatcher;
        $this->frontendItemGrouping = $frontendItemGrouping;
        $this->itemStructureFromEntityConverter = $itemStructureFromEntityConverter;
        $this->vatCalculationService = $vatCalculationService;
        $this->client = $client;
        $this->clientRepository = $clientRepository;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     * @param string $structureClassName
     *
     * @return Item
     */
    public function convertOne($entity, $structureClassName = Item::class)
    {
        /** @var Item $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->frontendItemGrouping->addItemGroupsToStructure($structure, $entity);
        $structure = $this->addConfigurationModesToStructure($structure, $entity);
        $structure = $this->addItemAttributesToStructure($structure, $entity);
        $structure = $this->itemStructureFromEntityConverter->addStockToStructure($structure, $entity);
        $structure = $this->itemStructureFromEntityConverter->addItemClassificationsToStructure($structure, $entity);
        $structure = $this->itemStructureFromEntityConverter->addItemSettingToStructure($structure, $entity);
        $structure = $this->addItemPriceToStructure($structure, $entity);

        $structure->title = $entity->getTranslatedTitle();
        $structure->abstract = $entity->getTranslatedAbstract();
        $structure->description = $entity->getTranslatedDescription();

        $structure->parentItemIdentifier = null !== $entity->getParent() ? $entity->getParent()->getIdentifier() : $entity->getIdentifier();

        $structure->callToAction = $this->getCallToAction($structure);

        $event = new FrontendItemStructureFromEntityEvent($entity, $structure);
        $this->eventDispatcher->dispatch($event, FrontendItemStructureFromEntityEvent::NAME);

        return $event->getStructure();
    }

    private function getCallToAction(Item $structure): ?string
    {
        // default client call to action acts as a fallback: use if nothing is set at item level
        $client = $this->client->getClient();
        if (ClientEntity::DEFAULT_IDENTIFIER === $client->identifier) {
            if (empty($structure->callToAction)) {
                return $client->callToAction;
            }
        }
        // for all other clients, call to action acts as an override: if it's set, always use over items' call to action
        // this is needed in order to be able to have a different call to action in a different client. we can (currently) not edit the items properties for the client
        else {
            if (!empty($client->callToAction)) {
                return $client->callToAction;
            }
            if (empty($structure->callToAction)) {
                $defaultClient = $this->clientRepository->findOneByIdentifier(ClientEntity::DEFAULT_IDENTIFIER);

                return $defaultClient->getCallToAction();
            }
        }

        return $structure->callToAction;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = Item::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }

    /**
     * @param $structure
     *
     * @return mixed
     */
    protected function addConfigurationModesToStructure($structure, ItemEntity $entity)
    {
        // children inherit the item status from their parent
        $item = $entity->getParent() ?? $entity;

        $defaultItemStatus = new ItemStatusFrontendStructure();
        $defaultItemStatus->identifier = ItemStatus::DEFAULT_IDENTIFIER;
        $defaultItemStatus->item_available = false;

        $itemStatus = $defaultItemStatus;
        if (null !== $item->getItemStatus()) {
            $itemStatus = $this->structureFromEntityConverter->convertOne(
                $item->getItemStatus(),
                ItemStatusFrontendStructure::class
            );
        }

        if (null !== $this->controlParameters && true === $this->controlParameters->isAdminMode()) {
            $itemStatus->item_available = true;
            $itemStatus->identifier = ItemStatus::AVAILABLE_IDENTIFIER;
        }

        $itemConfigurationModes = explode('+', (string)$item->getConfigurationMode());

        foreach (['creator', 'designer'] as $configurationMode) {
            $configurationModeStructure = new ConfigurationModeStructure();
            $configurationModeStructure->identifier = $configurationMode;
            $configurationModeStructure->itemStatus = in_array($configurationMode, $itemConfigurationModes, true)
                ? $itemStatus : $defaultItemStatus;

            $structure->configurationModes[] = $configurationModeStructure;
        }

        return $structure;
    }

    /**
     * @param Item $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     *
     * @return Item
     */
    protected function addItemAttributesToStructure($structure, $entity)
    {
        $itemAttributes = $entity->getItemattribute()->toArray();
        usort($itemAttributes, static function ($a, $b) {
            /* @var $a ItemAttribute */
            /* @var $b ItemAttribute */
            return $a->getSequencenumber() > $b->getSequencenumber() ? 1 : -1;
        });

        $attributesRelationStructures = $this->attributeRelationStructureFromEntityConverter->convertMany(
            $itemAttributes,
            AttributeRelation::class
        );

        $structure->attributes = $attributesRelationStructures;

        return $structure;
    }

    /**
     * @param Item $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Item $entity
     *
     * @return Item
     */
    protected function addItemPriceToStructure($structure, $entity)
    {
        $itemPrice = null;
        $itemPriceNet = null;
        if ($entity->getCurrentChannelItemprice()) {
            $itemPrice = $entity->getCurrentChannelItemprice()->getPrice();
            $itemPriceNet = $entity->getCurrentChannelItemprice()->getPriceNet();
        }

        if (null !== $itemPrice) {
            $this->vatCalculationService->addPricesToStructure($structure, $itemPrice, $itemPriceNet);
        }

        return $structure;
    }

    /**
     * @param ControlParameters|null $controlParameters
     */
    public function setControlParameters(?ControlParameters $controlParameters): void
    {
        $this->controlParameters = $controlParameters;
    }
}
