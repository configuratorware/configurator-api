<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignView;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Entity\VisualizationMode;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\DefaultConfigurationGenerator;
use Redhotmagma\ConfiguratorApiBundle\Service\CreatorView\CreatorViewDelete;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\Default3dViewFactory;
use Redhotmagma\ConfiguratorApiBundle\Service\DesignView\DesignViewDelete;

/**
 * @internal
 */
class ItemSave
{
    /**
     * @var ChildItemSave
     */
    private $childItemSave;

    /**
     * @var Default3dViewFactory
     */
    private $default3dViewFactory;

    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemStatusRepository
     */
    private $itemStatusRepository;

    /**
     * @var ItemEntityFromStructureConverter
     */
    private $itemEntityFromStructureConverter;

    /**
     * @var AdminAreaItemStructureFromEntityConverter
     */
    private $adminAreaItemStructureFromEntityConverter;

    /**
     * @var DefaultConfigurationGenerator
     */
    private $defaultConfigurationGenerator;

    /**
     * @var DesignViewDelete
     */
    private $designViewDelete;
    /**
     * @var CreatorViewDelete
     */
    private $creatorViewDelete;

    /**
     * @param ChildItemSave $childItemSave
     * @param CreatorViewDelete $creatorViewDelete
     * @param Default3dViewFactory $default3dViewFactory
     * @param DesignViewDelete $designViewDelete
     * @param ItemRepository $itemRepository
     * @param ItemStatusRepository $itemStatusRepository
     * @param ItemEntityFromStructureConverter $itemEntityFromStructureConverter
     * @param AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter
     * @param DefaultConfigurationGenerator $defaultConfigurationGenerator
     */
    public function __construct(
        ChildItemSave $childItemSave,
        CreatorViewDelete $creatorViewDelete,
        Default3dViewFactory $default3dViewFactory,
        DesignViewDelete $designViewDelete,
        ItemRepository $itemRepository,
        ItemStatusRepository $itemStatusRepository,
        ItemEntityFromStructureConverter $itemEntityFromStructureConverter,
        AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter,
        DefaultConfigurationGenerator $defaultConfigurationGenerator
    ) {
        $this->childItemSave = $childItemSave;
        $this->creatorViewDelete = $creatorViewDelete;
        $this->default3dViewFactory = $default3dViewFactory;
        $this->designViewDelete = $designViewDelete;
        $this->itemRepository = $itemRepository;
        $this->itemStatusRepository = $itemStatusRepository;
        $this->itemEntityFromStructureConverter = $itemEntityFromStructureConverter;
        $this->adminAreaItemStructureFromEntityConverter = $adminAreaItemStructureFromEntityConverter;
        $this->defaultConfigurationGenerator = $defaultConfigurationGenerator;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\Item $item
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Item
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\Item $item)
    {
        $entity = null;
        if (isset($item->id) && $item->id > 0) {
            $entity = $this->itemRepository->findOneBy(['id' => $item->id]);
        }

        $previousVisualizationMode = $this->getVisualizationModeIdentifier($entity);

        $entity = $this->itemEntityFromStructureConverter->convertOne($item, $entity);

        $newVisualizationMode = $this->getVisualizationModeIdentifier($entity);

        if ($this->isViewUpdateRequired($previousVisualizationMode, $newVisualizationMode)) {
            $this->updateViews($entity);
        }

        if (null === $entity->getItemStatus()) {
            $entity->setItemStatus($this->itemStatusRepository->findOneByIdentifier(ItemStatus::DEFAULT_IDENTIFIER));
        }

        $entity = $this->itemRepository->save($entity);

        // save child items (grouping information)
        $this->childItemSave->save($entity, $item->children);

        // if there is no default configuration -> create an empty one
        $this->defaultConfigurationGenerator->getDefaultConfigurationForItem($entity);

        $this->itemRepository->clear();

        $entity = $this->itemRepository->find($entity->getId());

        return $this->adminAreaItemStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @param string|null $previousVisualizationMode
     * @param string|null $newVisualizationMode
     *
     * @return bool
     */
    private function isViewUpdateRequired(?string $previousVisualizationMode, ?string $newVisualizationMode): bool
    {
        return null !== $newVisualizationMode && $previousVisualizationMode !== $newVisualizationMode && !(VisualizationMode::is2dVariantVisualization($previousVisualizationMode) && VisualizationMode::is2dVariantVisualization($newVisualizationMode));
    }

    /**
     * @param Item $item
     */
    private function updateViews(Item $item): void
    {
        $visualizationMode = $item->getVisualizationMode();

        if (null === $visualizationMode) {
            return;
        }

        $visualizationModeIdentifier = $visualizationMode->getIdentifier();

        if (VisualizationMode::IDENTIFIER_2D_LAYER === $visualizationModeIdentifier) {
            $designViewBluePrints = $item->getDesignView()->toArray();
            $this->removeAllViews($item);

            foreach ($designViewBluePrints as $designView) {
                $item->addCreatorView(CreatorView::copyFromDesignView($designView));
            }

            return;
        }

        if (VisualizationMode::IDENTIFIER_3D_SCENE === $visualizationModeIdentifier) {
            $this->removeAllViews($item);

            $item->addCreatorView($this->default3dViewFactory->create3dDefaultCreatorView($item));

            return;
        }

        if (VisualizationMode::is2dVariantVisualization($visualizationModeIdentifier)) {
            $creatorViewBluePrints = $item->getCreatorView()->toArray();
            $this->removeAllViews($item);

            foreach ($creatorViewBluePrints as $creatorView) {
                $item->addDesignView(DesignView::copyFromCreatorView($creatorView));
            }

            return;
        }

        if (VisualizationMode::IDENTIFIER_3D_VARIANT === $visualizationModeIdentifier) {
            $this->removeAllViews($item);

            $item->addDesignView($this->default3dViewFactory->create3dDefaultDesignView($item));

            return;
        }
    }

    /**
     * @param Item $item
     */
    private function removeAllViews(Item $item): void
    {
        foreach ($item->getDesignView() as $designView) {
            $this->designViewDelete->delete($designView->getId());
            $item->removeDesignView($designView);
        }

        foreach ($item->getCreatorView() as $creatorView) {
            $this->creatorViewDelete->delete($creatorView->getId());
            $item->removeCreatorView($creatorView);
        }
    }

    /**
     * @param Item|null $item
     *
     * @return string|null
     */
    private function getVisualizationModeIdentifier(?Item $item): ?string
    {
        return null !== $item && null !== $item->getVisualizationMode() ? $item->getVisualizationMode()->getIdentifier() : null;
    }
}
