<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ApiBundle\EventListener\EntityListener;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupentryRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemgroupRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ChildItem;
use Redhotmagma\ConfiguratorApiBundle\Structure\GroupRelation;

/**
 * @internal
 */
class ChildItemSave
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemgroupRepository
     */
    private $groupRepository;

    /**
     * @var ItemgroupentryRepository
     */
    private $groupEntryRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * ChildItemSave constructor.
     *
     * @param ItemRepository               $itemRepository
     * @param ItemgroupRepository          $groupRepository
     * @param ItemgroupentryRepository     $groupEntryRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        ItemRepository $itemRepository,
        ItemgroupRepository $groupRepository,
        ItemgroupentryRepository $groupEntryRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->itemRepository = $itemRepository;
        $this->groupRepository = $groupRepository;
        $this->groupEntryRepository = $groupEntryRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param Item  $item
     * @param array $children
     */
    public function save(Item $item, $children)
    {
        $childItemIds = [];
        if (!empty($children)) {
            /** @var ChildItem $child */
            foreach ($children as $child) {
                $this->saveItemGroups($item, $child);
                $childItemIds[] = $child->id;
            }
        }

        $this->updateParentRelations($item, $childItemIds);
    }

    /**
     * @param Item      $item
     * @param ChildItem $child
     */
    protected function saveItemGroups(Item $item, ChildItem $child)
    {
        /** @var Item $childItem */
        $childItem = $this->itemRepository->find($child->id);
        $childItem->setParent($item);

        /** @var GroupRelation $group */
        $itemGroups = [];
        $itemGroupEntries = [];
        foreach ($child->groups as $group) {
            $itemGroups[] = $group;
            $groupEntry = $group->groupEntry;
            $groupEntry->itemgroup = $this->groupRepository->find($group->id);
            $itemGroupEntries[] = $group->groupEntry;
        }

        $relationUpdateHelper = new \stdClass();
        $relationUpdateHelper->id = $item->getId();
        $relationUpdateHelper->itemGroups = $itemGroups;
        $relationUpdateHelper->itemGroupEntries = $itemGroupEntries;

        $this->entityFromStructureConverter->setManyToManyRelationsDeleted($relationUpdateHelper, $childItem,
            'ItemItemgroup', 'itemGroups', 'Itemgroup');

        $this->entityFromStructureConverter->addNewManyToManyRelations($relationUpdateHelper, $childItem,
            ItemItemgroup::class, 'itemGroups', 'Itemgroup',
            $this->groupRepository);

        $this->entityFromStructureConverter->setManyToManyRelationsDeleted($relationUpdateHelper, $childItem,
            'ItemItemgroupentry', 'itemGroupEntries', 'Itemgroupentry');

        foreach ($child->groups as $group) {
            /** @var Itemgroupentry $groupEntry */
            $groupEntry = $this->groupEntryRepository->find($group->groupEntry->id);

            $itemGroupEntryFound = false;
            /** @var ItemItemgroupentry $itemGroupEntry */
            foreach ($childItem->getItemItemgroupentry() as $itemGroupEntry) {
                if ($itemGroupEntry->getItemgroup()->getId() == $group->id) {
                    $itemGroupEntry->setItemgroupentry($groupEntry);
                    // reset date deleted because we are updating an existing entry that might have been deleted above
                    $itemGroupEntry->setDateDeleted(new \DateTime(EntityListener::DATE_DELETED_DEFAULT));

                    $itemGroupEntryFound = true;
                }
            }

            if (false === $itemGroupEntryFound) {
                $group = $this->groupRepository->find($group->id);

                $itemGroupEntry = new ItemItemgroupentry();
                $itemGroupEntry->setItem($childItem);
                $itemGroupEntry->setItemgroupentry($groupEntry);
                $itemGroupEntry->setItemgroup($group);

                $childItem->addItemItemgroupentry($itemGroupEntry);
            }
        }

        $this->itemRepository->save($childItem);
    }

    /**
     * update parent relations
     * remove the ones that are no longer related.
     *
     * @param Item  $item
     * @param array $childItemIds
     */
    private function updateParentRelations(Item $item, array $childItemIds)
    {
        $children = $this->itemRepository->findBy(['parent' => $item]);

        /** @var Item $child */
        foreach ($children as $child) {
            if (!in_array($child->getId(), $childItemIds)) {
                $child->setParent(null);
                $this->itemRepository->save($child);
            }
        }
    }
}
