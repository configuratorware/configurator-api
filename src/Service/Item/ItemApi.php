<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Events\Item\GetConfigurableItemsByParentEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Exception\ValidationException;
use Redhotmagma\ConfiguratorApiBundle\Repository\InspirationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item as ItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemDuplicate as ItemDuplicateStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemInspiration;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus as ItemStatusStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\DeleteLayerImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ItemClassificationFilter;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestNoDefaultArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadLayerImage;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ItemApi
{
    private AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter;

    private FrontendItemListStructureFromEntityConverter $frontendItemListStructureFromEntityConverter;

    private EventDispatcherInterface $eventDispatcher;

    private InspirationRepository $inspirationRepository;

    private ItemDelete $itemDelete;

    private ItemImage $itemImage;

    private ItemRepository $itemRepository;

    private ItemSave $itemSave;

    private ItemStatusUpdate $itemStatusUpdate;

    private ValidatorInterface $validator;

    private ItemDuplicate $itemDuplicate;

    /**
     * @param AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter
     * @param FrontendItemListStructureFromEntityConverter $frontendItemListStructureFromEntityConverter
     * @param EventDispatcherInterface $eventDispatcher
     * @param InspirationRepository $inspirationRepository
     * @param ItemDelete $itemDelete
     * @param ItemImage $itemImage
     * @param ItemRepository $itemRepository
     * @param ItemSave $itemSave
     * @param ItemStatusUpdate $itemStatusUpdate
     * @param ValidatorInterface $validator
     * @param ItemDuplicate $itemDuplicate
     */
    public function __construct(
        AdminAreaItemStructureFromEntityConverter $adminAreaItemStructureFromEntityConverter,
        FrontendItemListStructureFromEntityConverter $frontendItemListStructureFromEntityConverter,
        EventDispatcherInterface $eventDispatcher,
        InspirationRepository $inspirationRepository,
        ItemDelete $itemDelete,
        ItemImage $itemImage,
        ItemRepository $itemRepository,
        ItemSave $itemSave,
        ItemStatusUpdate $itemStatusUpdate,
        ValidatorInterface $validator,
        ItemDuplicate $itemDuplicate
    ) {
        $this->adminAreaItemStructureFromEntityConverter = $adminAreaItemStructureFromEntityConverter;
        $this->frontendItemListStructureFromEntityConverter = $frontendItemListStructureFromEntityConverter;
        $this->eventDispatcher = $eventDispatcher;
        $this->inspirationRepository = $inspirationRepository;
        $this->itemDelete = $itemDelete;
        $this->itemImage = $itemImage;
        $this->itemRepository = $itemRepository;
        $this->itemSave = $itemSave;
        $this->itemStatusUpdate = $itemStatusUpdate;
        $this->validator = $validator;
        $this->itemDuplicate = $itemDuplicate;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $arguments->filters['_parent_filter'] = 'top_level_only';
        $entities = $this->itemRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->adminAreaItemStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->itemRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param string               $configurationMode
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResultBy(string $configurationMode, ListRequestArguments $arguments): PaginationResult
    {
        $configurationModeFilter = ['item.configuration_mode' => $configurationMode];

        $arguments->filters = array_replace($arguments->filters, $configurationModeFilter);

        return $this->getListResult($arguments);
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getInspirationListResult(ListRequestArguments $arguments): PaginationResult
    {
        $arguments->filters['_parent_filter'] = 'top_level_only';

        $count = (int)$this->itemRepository->fetchListCount($arguments, $this->getSearchableFields());
        $entities = $this->itemRepository->fetchList($arguments, $this->getSearchableFields());

        $inspirationCountMap = $this->inspirationRepository->countPerItem(array_map(static function (Item $item) {
            return $item->getId();
        }, $entities));

        $data = array_map(static function (Item $item) use ($inspirationCountMap) {
            return ItemInspiration::fromEntity($item, $inspirationCountMap);
        }, $entities);

        return new PaginationResult($data, $count);
    }

    /**
     * @param int $id
     *
     * @return ItemStructure
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): ItemStructure
    {
        /** @var Item $entity */
        $entity = $this->itemRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        return $this->adminAreaItemStructureFromEntityConverter->convertOne($entity);
    }

    /**
     * @param ItemStructure $item
     *
     * @return ItemStructure
     */
    public function save(ItemStructure $item): ItemStructure
    {
        return $this->itemSave->save($item);
    }

    /**
     * @param string $id
     */
    public function delete($id): void
    {
        $this->itemDelete->delete($id);
    }

    public function duplicate(int $id, ItemDuplicateStructure $itemDuplicate): ItemStructure
    {
        $itemToDuplicate = $this->itemRepository->find($id);
        if (null === $itemToDuplicate) {
            throw new NotFoundException();
        }

        $errors = $this->validator->validate($itemDuplicate);
        if (count($errors) > 0) {
            throw new ValidationException(iterator_to_array($errors));
        }

        $item = $this->itemDuplicate->duplicate($itemToDuplicate, $itemDuplicate);

        return $this->adminAreaItemStructureFromEntityConverter->convertOne($item);
    }

    /**
     * @param string $parentIdentifier
     *
     * @return array
     */
    public function getConfigurableItemsByParent(string $parentIdentifier = ''): array
    {
        $event = new GetConfigurableItemsByParentEvent($parentIdentifier);
        $this->eventDispatcher->dispatch($event, GetConfigurableItemsByParentEvent::NAME);

        return $event->getItemStructures();
    }

    /**
     * @param UploadLayerImage $uploadLayerImage
     */
    public function uploadLayerImage(UploadLayerImage $uploadLayerImage): void
    {
        $this->itemImage->uploadLayerImage($uploadLayerImage);
    }

    /**
     * @param DeleteLayerImage $deleteLayerImage
     */
    public function deleteLayerImage(DeleteLayerImage $deleteLayerImage): void
    {
        $this->itemImage->deleteLayerImage($deleteLayerImage);
    }

    /**
     * @param ItemStructure       $item
     * @param ItemStatusStructure $itemStatus
     */
    public function updateItemStatus(ItemStructure $item, ItemStatusStructure $itemStatus): void
    {
        $this->itemStatusUpdate->updateItemStatus($item, $itemStatus);
    }

    public function getFrontendList(
        ItemClassificationFilter $itemClassificationFilter,
        ListRequestNoDefaultArguments $arguments
    ): PaginationResult {
        $entities = $this->itemRepository->getFrontendList($itemClassificationFilter, $arguments);

        return new PaginationResult(
            $this->frontendItemListStructureFromEntityConverter->convertMany($entities),
            $this->itemRepository->fetchFrontendListCount($itemClassificationFilter)
        );
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return string[]
     */
    protected function getSearchableFields(): array
    {
        return ['identifier', 'itemtext.title', 'itemtext.abstract', 'itemtext.description'];
    }
}
