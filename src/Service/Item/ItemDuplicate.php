<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Doctrine\ORM\EntityManagerInterface;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Entity\CreatorViewText;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethodPrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemAttribute;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroup;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemItemgroupentry;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOptionDeltaprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemprice;
use Redhotmagma\ConfiguratorApiBundle\Entity\Itemtext;
use Redhotmagma\ConfiguratorApiBundle\Entity\Rule;
use Redhotmagma\ConfiguratorApiBundle\Entity\RuleText;
use Redhotmagma\ConfiguratorApiBundle\Entity\Stock;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ItemImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ConfigurationtypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\ConfigurationCodeGenerator;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\Paths;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemDuplicate as ItemDuplicateStructure;
use Symfony\Component\Filesystem\Filesystem;

class ItemDuplicate
{
    private ItemRepository $itemRepository;
    private EntityFromStructureConverter $entityFromStructureConverter;
    private Paths $paths;
    private ItemImageRepository $itemImageRepository;
    private string $mediaBasePath;
    private string $visualizationComponentDataDir;
    private EntityManagerInterface $entityManager;
    private ConfigurationRepository $configurationRepository;
    private ConfigurationCodeGenerator $configurationCodeGenerator;
    private ConfigurationtypeRepository $configurationTypeRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        ItemRepository $itemRepository,
        EntityFromStructureConverter $entityFromStructureConverter,
        Paths $paths,
        ItemImageRepository $itemImageRepository,
        string $mediaBasePath,
        string $visualizationComponentDataDir,
        ConfigurationRepository $configurationRepository,
        ConfigurationCodeGenerator $configurationCodeGenerator,
        ConfigurationtypeRepository $configurationTypeRepository
    ) {
        $this->itemRepository = $itemRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->paths = $paths;
        $this->itemImageRepository = $itemImageRepository;
        $this->mediaBasePath = $mediaBasePath;
        $this->visualizationComponentDataDir = $visualizationComponentDataDir;
        $this->entityManager = $entityManager;
        $this->configurationRepository = $configurationRepository;
        $this->configurationCodeGenerator = $configurationCodeGenerator;
        $this->configurationTypeRepository = $configurationTypeRepository;
    }

    public function duplicate(Item $itemToDuplicate, ItemDuplicateStructure $itemDuplicate): Item
    {
        $duplicatedItem = $this->itemRepository->findOneByIdentifier($itemDuplicate->identifier);
        if (null === $duplicatedItem) {
            $duplicatedItem = new Item();
            $duplicatedItem->setIdentifier($itemDuplicate->identifier);
        } else {
            $this->clearAssociatedEntities($duplicatedItem);
        }

        $this->updateItem($duplicatedItem, $itemToDuplicate);
        $this->updateItemTexts($duplicatedItem, $itemDuplicate);
        $this->updateItemPrices($duplicatedItem, $itemToDuplicate);
        $this->updateItemAttributes($duplicatedItem, $itemToDuplicate);
        $this->updateItemItemclassification($duplicatedItem, $itemToDuplicate);
        $this->updateItemOptionclassification($duplicatedItem, $itemToDuplicate);
        $duplicatedItem->setParent($itemToDuplicate->getParent());
        $duplicatedItem->setVisualizationMode($itemToDuplicate->getVisualizationMode());
        $this->updateStock($duplicatedItem, $itemToDuplicate);
        $this->updateRule($duplicatedItem, $itemToDuplicate);
        $this->updateItemItemgroup($duplicatedItem, $itemToDuplicate);
        $this->updateItemItemgroupentry($duplicatedItem, $itemToDuplicate);
        $this->updateDesignArea($duplicatedItem, $itemToDuplicate);
        $this->updateCreatorView($duplicatedItem, $itemToDuplicate);
        $duplicatedItem->setItemStatus($itemToDuplicate->getItemStatus());
        $this->updateFiles($duplicatedItem, $itemToDuplicate);

        $this->itemRepository->save($duplicatedItem);
        // Refresh entity in order to only get the non-deleted linked entities
        $this->entityManager->refresh($duplicatedItem);

        $this->updateDefaultConfiguration($duplicatedItem, $itemToDuplicate);

        return $duplicatedItem;
    }

    private function clearAssociatedEntities(Item $duplicatedItem): void
    {
        $duplicatedItem->clearItemtext();
        $duplicatedItem->clearItemprice();
        $duplicatedItem->clearItemattribute();
        $duplicatedItem->clearItemItemclassification();
        $duplicatedItem->clearItemOptionclassification();
        $duplicatedItem->clearStock();
        $duplicatedItem->clearRule();
        $duplicatedItem->clearItemItemgroup();
        $duplicatedItem->clearItemItemgroupentry();
        $duplicatedItem->clearDesignArea();
        $duplicatedItem->clearCreatorView();

        $this->itemRepository->save($duplicatedItem);
    }

    private function updateItem(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        $duplicatedItem->setExternalid($itemToDuplicate->getExternalid());
        $duplicatedItem->setConfigurable($itemToDuplicate->getConfigurable());
        $duplicatedItem->setDeactivated($itemToDuplicate->getDeactivated());
        $duplicatedItem->setMinimumOrderAmount($itemToDuplicate->getMinimumOrderAmount());
        $duplicatedItem->setAccumulateAmounts($itemToDuplicate->getAccumulateAmounts());
        $duplicatedItem->setConfigurationMode($itemToDuplicate->getConfigurationMode());
        $duplicatedItem->setCallToAction($itemToDuplicate->getCallToAction());
        $duplicatedItem->setOverwriteComponentOrder($itemToDuplicate->getOverwriteComponentOrder());
    }

    private function updateItemTexts(Item $duplicatedItem, ItemDuplicateStructure $itemDuplicate): void
    {
        foreach ($itemDuplicate->itemTexts as $itemTextDuplicate) {
            $itemText = $this->entityFromStructureConverter->convertOne($itemTextDuplicate, null, Itemtext::class);
            $itemText->setItem($duplicatedItem);

            $duplicatedItem->addItemtext($itemText);
        }
    }

    private function updateDefaultConfiguration(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        $defaultConfigurationToDuplicate = $this->configurationRepository->findDefaultConfigurationForItem($itemToDuplicate);

        if (null === $defaultConfigurationToDuplicate) {
            return;
        }

        $duplicatedDefaultConfiguration = $this->configurationRepository->findDefaultConfigurationForItem($duplicatedItem);
        if (null === $duplicatedDefaultConfiguration) {
            $duplicatedDefaultConfiguration = new Configuration();
        }

        $defaultOptionsConfigurationType = $this->configurationTypeRepository->getDefaultType();
        $duplicatedDefaultConfiguration->setConfigurationtype($defaultOptionsConfigurationType);

        $codeLength = $this->configurationCodeGenerator->getConfigurationCodeLengthByConfigurationType(ConfigurationtypeRepository::DEFAULT_OPTIONS);
        $duplicatedDefaultConfiguration->setCode($this->configurationCodeGenerator->generateCode($codeLength));

        $duplicatedDefaultConfiguration->setItem($duplicatedItem);
        $duplicatedDefaultConfiguration->setCustomdata($defaultConfigurationToDuplicate->getCustomdata());
        $duplicatedDefaultConfiguration->setChannel($defaultConfigurationToDuplicate->getChannel());
        $duplicatedDefaultConfiguration->setClient($defaultConfigurationToDuplicate->getClient());
        $duplicatedDefaultConfiguration->setLanguage($defaultConfigurationToDuplicate->getLanguage());
        $duplicatedDefaultConfiguration->setName($defaultConfigurationToDuplicate->getName());
        $duplicatedDefaultConfiguration->setPartslisthash($defaultConfigurationToDuplicate->getPartslisthash());
        $duplicatedDefaultConfiguration->setSelectedoptions($defaultConfigurationToDuplicate->getSelectedoptions());

        $this->configurationRepository->save($duplicatedDefaultConfiguration);
    }

    private function updateItemPrices(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getItemprice()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getItemprice() as $itemPriceToDuplicate) {
            $itemPrice = new Itemprice();
            $itemPrice->setPrice($itemPriceToDuplicate->getPrice());
            $itemPrice->setPriceNet($itemPriceToDuplicate->getPriceNet());
            $itemPrice->setAmountfrom($itemPriceToDuplicate->getAmountfrom());
            $itemPrice->setChannel($itemPriceToDuplicate->getChannel());
            $itemPrice->setItem($duplicatedItem);

            $duplicatedItem->addItemprice($itemPrice);
        }
    }

    private function updateItemAttributes(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getItemattribute()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getItemattribute() as $itemAttributeToDuplicate) {
            $itemAttribute = new ItemAttribute();
            $itemAttribute->setSequencenumber($itemAttributeToDuplicate->getSequencenumber());
            $itemAttribute->setAttribute($itemAttributeToDuplicate->getAttribute());
            $itemAttribute->setAttributevalue($itemAttributeToDuplicate->getAttributevalue());
            $itemAttribute->setItem($duplicatedItem);

            $duplicatedItem->addItemattribute($itemAttribute);
        }
    }

    private function updateItemItemclassification(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getItemItemclassification()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getItemItemclassification() as $itemItemclassificationToDuplicate) {
            $itemItemclassification = new ItemItemclassification();
            $itemItemclassification->setItemclassification($itemItemclassificationToDuplicate->getItemclassification());
            $itemItemclassification->setItem($duplicatedItem);

            $duplicatedItem->addItemItemclassification($itemItemclassification);
        }
    }

    private function updateItemOptionclassification(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getItemOptionclassification()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getItemOptionclassification() as $itemOptionclassificationToDuplicate) {
            $itemOptionclassification = new ItemOptionclassification();
            $itemOptionclassification->setIsMultiselect($itemOptionclassificationToDuplicate->getIsMultiselect());
            $itemOptionclassification->setMinamount($itemOptionclassificationToDuplicate->getMinamount());
            $itemOptionclassification->setMaxamount($itemOptionclassificationToDuplicate->getMaxamount());
            $itemOptionclassification->setIsMandatory($itemOptionclassificationToDuplicate->getIsMandatory());
            $itemOptionclassification->setSequencenumber($itemOptionclassificationToDuplicate->getSequencenumber());
            $itemOptionclassification->setCreatorView($itemOptionclassificationToDuplicate->getCreatorView());
            $itemOptionclassification->setOptionclassification($itemOptionclassificationToDuplicate->getOptionclassification());
            $itemOptionclassification->setItem($duplicatedItem);

            foreach ($itemOptionclassificationToDuplicate->getItemOptionclassificationOption() as $itemOptionclassificationOptionToDuplicate) {
                $itemOptionclassificationOption = new ItemOptionclassificationOption();
                $itemOptionclassificationOption->setAmountisselectable($itemOptionclassificationOptionToDuplicate->getAmountisselectable());
                $itemOptionclassificationOption->setOption($itemOptionclassificationOptionToDuplicate->getOption());
                $itemOptionclassificationOption->setItemOptionclassification($itemOptionclassification);

                foreach ($itemOptionclassificationOptionToDuplicate->getItemOptionclassificationOptionDeltaprice() as $itemOptionclassificationOptionDeltapriceToDuplicate) {
                    $itemOptionclassificationOptionDeltaprice = new ItemOptionclassificationOptionDeltaprice();
                    $itemOptionclassificationOptionDeltaprice->setPrice($itemOptionclassificationOptionDeltapriceToDuplicate->getPrice());
                    $itemOptionclassificationOptionDeltaprice->setPriceNet($itemOptionclassificationOptionDeltapriceToDuplicate->getPriceNet());
                    $itemOptionclassificationOptionDeltaprice->setChannel($itemOptionclassificationOptionDeltapriceToDuplicate->getChannel());
                    $itemOptionclassificationOptionDeltaprice->setItemOptionclassificationOption($itemOptionclassificationOption);

                    $itemOptionclassificationOption->addItemOptionclassificationOptionDeltaprice($itemOptionclassificationOptionDeltaprice);
                }

                $itemOptionclassification->addItemOptionclassificationOption($itemOptionclassificationOption);
            }

            $duplicatedItem->addItemOptionclassification($itemOptionclassification);
        }
    }

    private function updateStock(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getStock()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getStock() as $stockToDuplicate) {
            $stock = new Stock();
            $stock->setDeliverydate($stockToDuplicate->getDeliverydate());
            $stock->setDeliverytime($stockToDuplicate->getDeliverytime());
            $stock->setAmount($stockToDuplicate->getAmount());
            $stock->setChannel($stockToDuplicate->getChannel());
            $stock->setStockstatus($stockToDuplicate->getStockstatus());
            $stock->setItem($duplicatedItem);

            $duplicatedItem->addStock($stock);
        }
    }

    private function updateRule(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getRule()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getRule() as $ruleToDuplicate) {
            $rule = new Rule();
            $rule->setData($ruleToDuplicate->getData());
            $rule->setAction($ruleToDuplicate->getAction());
            $rule->setOrderable($ruleToDuplicate->getOrderable());

            foreach ($ruleToDuplicate->getRuleText() as $ruleTextToDuplicate) {
                $ruleText = new RuleText();
                $ruleText->setTitle($ruleTextToDuplicate->getTitle());
                $ruleText->setLanguage($ruleTextToDuplicate->getLanguage());
                $ruleText->setRule($rule);

                $rule->addRuleText($ruleText);
            }

            $rule->setRuletype($ruleToDuplicate->getRuletype());
            $rule->setRulefeedback($ruleToDuplicate->getRulefeedback());
            $rule->setOption($ruleToDuplicate->getOption());
            $rule->setItem($duplicatedItem);
            // TODO: Update reverse rule

            $duplicatedItem->addRule($rule);
        }
    }

    private function updateItemItemgroup(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getItemItemgroup()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getItemItemgroup() as $itemItemgroupToDuplicate) {
            $itemItemgroup = new ItemItemgroup();
            $itemItemgroup->setItemgroup($itemItemgroupToDuplicate->getItemgroup());
            $itemItemgroup->setItem($duplicatedItem);

            $duplicatedItem->addItemItemgroup($itemItemgroup);
        }
    }

    private function updateItemItemgroupentry(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getItemItemgroupentry()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getItemItemgroupentry() as $itemItemgroupentryToDuplicate) {
            $itemItemgroupentry = new ItemItemgroupentry();
            $itemItemgroupentry->setItemgroupentry($itemItemgroupentryToDuplicate->getItemgroupentry());
            $itemItemgroupentry->setItemgroup($itemItemgroupentryToDuplicate->getItemgroup());
            $itemItemgroupentry->setItem($duplicatedItem);

            $duplicatedItem->addItemItemgroupentry($itemItemgroupentry);
        }
    }

    private function updateDesignArea(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getDesignArea()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getDesignArea() as $designAreaToDuplicate) {
            $designArea = new DesignArea();
            $designArea->setIdentifier($designAreaToDuplicate->getIdentifier());
            $designArea->setSequenceNumber($designAreaToDuplicate->getSequenceNumber());
            $designArea->setHeight($designAreaToDuplicate->getHeight());
            $designArea->setWidth($designAreaToDuplicate->getWidth());
            $designArea->setMask($designAreaToDuplicate->getMask());
            $designArea->setCustomData($designAreaToDuplicate->getCustomData());
            $designArea->setOption($designAreaToDuplicate->getOption());
            $designArea->setItem($duplicatedItem);
            $this->duplicateDesignAreaDesignProductionMethods($designArea, $designAreaToDuplicate);

            foreach ($designAreaToDuplicate->getDesignAreaText() as $designAreaTextToDuplicate) {
                $designAreaText = new DesignAreaText();
                $designAreaText->setTitle($designAreaTextToDuplicate->getTitle());
                $designAreaText->setLanguage($designAreaTextToDuplicate->getLanguage());
                $designAreaText->setDesignArea($designArea);

                $designArea->addDesignAreaText($designAreaText);
            }

            // TODO: Duplicate CreatorViewDesignArea

            $duplicatedItem->addDesignArea($designArea);
        }
    }

    private function duplicateDesignAreaDesignProductionMethods(
        DesignArea $duplicatedDesignArea,
        DesignArea $designAreaToDuplicate
    ): void {
        foreach ($designAreaToDuplicate->getDesignAreaDesignProductionMethod() as $designAreaDesignProductionMethodToDuplicate) {
            $designAreaDesignProductionMethod = new DesignAreaDesignProductionMethod();
            $designAreaDesignProductionMethod->setWidth($designAreaDesignProductionMethodToDuplicate->getWidth());
            $designAreaDesignProductionMethod->setHeight($designAreaDesignProductionMethodToDuplicate->getHeight());
            $designAreaDesignProductionMethod->setMask($designAreaDesignProductionMethodToDuplicate->getMask());
            $designAreaDesignProductionMethod->setAdditionalData($designAreaDesignProductionMethodToDuplicate->getAdditionalData());
            $designAreaDesignProductionMethod->setCustomData($designAreaDesignProductionMethodToDuplicate->getCustomData());
            $designAreaDesignProductionMethod->setAllowBulkNames($designAreaDesignProductionMethodToDuplicate->getAllowBulkNames());
            $designAreaDesignProductionMethod->setIsDefault($designAreaDesignProductionMethodToDuplicate->getIsDefault());
            $designAreaDesignProductionMethod->setMinimumOrderAmount($designAreaDesignProductionMethodToDuplicate->getMinimumOrderAmount());
            $designAreaDesignProductionMethod->setDefaultColors($designAreaDesignProductionMethodToDuplicate->getDefaultColors());
            $designAreaDesignProductionMethod->setMaxElements($designAreaDesignProductionMethodToDuplicate->getMaxElements());
            $designAreaDesignProductionMethod->setMaxTexts($designAreaDesignProductionMethodToDuplicate->getMaxTexts());
            $designAreaDesignProductionMethod->setMaxImages($designAreaDesignProductionMethodToDuplicate->getMaxImages());
            $designAreaDesignProductionMethod->setOneLineText($designAreaDesignProductionMethodToDuplicate->getOneLineText());
            $designAreaDesignProductionMethod->setDesignElementsLocked($designAreaDesignProductionMethodToDuplicate->getDesignElementsLocked());
            $designAreaDesignProductionMethod->setDesignProductionMethod($designAreaDesignProductionMethodToDuplicate->getDesignProductionMethod());

            foreach ($designAreaDesignProductionMethodToDuplicate->getDesignAreaDesignProductionMethodPrice() as $designAreaDesignProductionMethodPriceToDuplicate) {
                $designAreaDesignProductionMethodPrice = new DesignAreaDesignProductionMethodPrice();
                $designAreaDesignProductionMethodPrice->setAmountFrom($designAreaDesignProductionMethodPriceToDuplicate->getAmountFrom());
                $designAreaDesignProductionMethodPrice->setColorAmountFrom($designAreaDesignProductionMethodPriceToDuplicate->getColorAmountFrom());
                $designAreaDesignProductionMethodPrice->setPrice($designAreaDesignProductionMethodPriceToDuplicate->getPrice());
                $designAreaDesignProductionMethodPrice->setPriceNet($designAreaDesignProductionMethodPriceToDuplicate->getPriceNet());
                $designAreaDesignProductionMethodPrice->setChannel($designAreaDesignProductionMethodPriceToDuplicate->getChannel());
                $designAreaDesignProductionMethodPrice->setDesignerProductionCalculationType($designAreaDesignProductionMethodPriceToDuplicate->getDesignerProductionCalculationType());
                $designAreaDesignProductionMethodPrice->setDesignAreaDesignProductionMethod($designAreaDesignProductionMethod);

                $designAreaDesignProductionMethod->addDesignAreaDesignProductionMethodPrice($designAreaDesignProductionMethodPrice);
            }

            $designAreaDesignProductionMethod->setDesignArea($duplicatedDesignArea);

            $duplicatedDesignArea->addDesignAreaDesignProductionMethod($designAreaDesignProductionMethod);
        }
    }

    private function updateCreatorView(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        if ($itemToDuplicate->getCreatorView()->isEmpty()) {
            return;
        }

        foreach ($itemToDuplicate->getCreatorView() as $creatorViewToDuplicate) {
            $creatorView = CreatorView::from(
                $creatorViewToDuplicate->getDirectory(),
                $creatorViewToDuplicate->getIdentifier(),
                $duplicatedItem,
                $creatorViewToDuplicate->getSequenceNumber()
            );

            foreach ($creatorViewToDuplicate->getCreatorViewText() as $creatorViewTextToDuplicate) {
                $creatorViewText = CreatorViewText::from(
                    $creatorViewTextToDuplicate->getTitle(),
                    $creatorView,
                    $creatorViewTextToDuplicate->getLanguage(),
                );

                $creatorView->addCreatorViewText($creatorViewText);
            }

            // TODO: Duplicate CreatorViewDesignArea

            $duplicatedItem->addCreatorView($creatorView);
        }
    }

    private function updateFiles(Item $duplicatedItem, Item $itemToDuplicate): void
    {
        $filesystem = new Filesystem();

        /**
         * 3D Creator Files.
         */
        $visualizationComponentDataDirToDuplicate = str_replace(
            ['{configurationMode}', '{itemIdentifier}'],
            ['creator', $itemToDuplicate->getIdentifier()],
            $this->visualizationComponentDataDir
        );
        $visualizationComponentDataDir = str_replace(
            ['{configurationMode}', '{itemIdentifier}'],
            ['creator', $duplicatedItem->getIdentifier()],
            $this->visualizationComponentDataDir
        );

        if (file_exists($visualizationComponentDataDir)) {
            $filesystem->remove($visualizationComponentDataDir);
        }
        if (file_exists($visualizationComponentDataDirToDuplicate)) {
            $filesystem->mirror($visualizationComponentDataDirToDuplicate, $visualizationComponentDataDir);
        }

        /**
         * 2D Creator files.
         */
        $configuratorImagesDir = $this->paths->getLayerImagePath() . DIRECTORY_SEPARATOR . $duplicatedItem->getIdentifier();
        $configuratorImagesDirToDuplicate = $this->paths->getLayerImagePath() . DIRECTORY_SEPARATOR . $itemToDuplicate->getIdentifier();

        if (file_exists($configuratorImagesDir)) {
            $filesystem->remove($configuratorImagesDir);
        }
        if (file_exists($configuratorImagesDirToDuplicate)) {
            $filesystem->mirror($configuratorImagesDirToDuplicate, $configuratorImagesDir);
        }

        /**
         * Item images.
         */
        $itemImages = $this->itemImageRepository->findDetailImages($duplicatedItem);
        $itemImagesToDuplicate = $this->itemImageRepository->findDetailImages($itemToDuplicate);
        foreach ($itemImages as $itemImage) {
            $filesystem->remove($this->mediaBasePath . $itemImage->getUrl());
        }
        foreach ($itemImagesToDuplicate as $itemImageToDuplicate) {
            $extension = pathinfo($itemImageToDuplicate->getUrl(), PATHINFO_EXTENSION);
            $filesystem->copy(
                $this->mediaBasePath . $itemImageToDuplicate->getUrl(),
                $this->paths->getItemDetailImagePath() . DIRECTORY_SEPARATOR . $duplicatedItem->getIdentifier() . '.' . $extension
            );
        }

        $itemThumbnails = $this->itemImageRepository->findThumbnails($duplicatedItem);
        $itemThumbnailsToDuplicate = $this->itemImageRepository->findThumbnails($itemToDuplicate);
        foreach ($itemThumbnails as $itemThumbnail) {
            $filesystem->remove($this->mediaBasePath . $itemThumbnail->getUrl());
        }
        foreach ($itemThumbnailsToDuplicate as $itemThumbnailToDuplicate) {
            $extension = pathinfo($itemThumbnailToDuplicate->getUrl(), PATHINFO_EXTENSION);
            $filesystem->copy(
                $this->mediaBasePath . $itemThumbnailToDuplicate->getUrl(),
                $this->paths->getItemThumbnailPath() . DIRECTORY_SEPARATOR . $duplicatedItem->getIdentifier() . '.' . $extension
            );
        }
    }
}
