<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\Item;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Item as ItemStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus as ItemStatusStructure;

/**
 * @internal
 */
class ItemStatusUpdate
{
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var ItemStatusRepository
     */
    private $itemStatusRepository;

    /**
     * ItemStatusUpdate constructor.
     *
     * @param ItemRepository       $itemRepository
     * @param ItemStatusRepository $itemStatusRepository
     */
    public function __construct(ItemRepository $itemRepository, ItemStatusRepository $itemStatusRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->itemStatusRepository = $itemStatusRepository;
    }

    /**
     * @param ItemStructure       $item
     * @param ItemStatusStructure $itemStatus
     */
    public function updateItemStatus(ItemStructure $item, ItemStatusStructure $itemStatus): void
    {
        $itemEntity = $this->itemRepository->find($item->id);
        if (null === $itemEntity) {
            throw NotFoundException::entity(Item::class);
        }

        $itemStatusEntity = $this->itemStatusRepository->find($itemStatus->id);
        if (null === $itemStatusEntity) {
            throw NotFoundException::entity(ItemStatus::class);
        }

        $itemEntity->setItemStatus($itemStatusEntity);
        $this->itemRepository->save($itemEntity);
    }
}
