<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassificationAttribute;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationEntityFromStructureConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification;

/**
 * @internal
 */
class OptionClassificationEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var AttributeRelationEntityFromStructureConverter
     */
    private $attributeRelationEntityFromStructureConverter;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * OptionClassificationEntityFromStructureConverter constructor.
     *
     * @param AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        AttributeRelationEntityFromStructureConverter $attributeRelationEntityFromStructureConverter,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->attributeRelationEntityFromStructureConverter = $attributeRelationEntityFromStructureConverter;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param OptionClassification $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->updateOptionClassificationTexts($localStructure, $entity);
        $entity = $this->updateOptionClassificationAttributes($localStructure, $entity);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = OptionClassification::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param OptionClassification $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification $entity
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification
     */
    protected function updateOptionClassificationTexts($structure, $entity)
    {
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            'texts',
            'OptionClassificationText');

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            'texts',
            'OptionClassificationText');

        return $entity;
    }

    /**
     * @param OptionClassification $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification $entity
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification
     */
    protected function updateOptionClassificationAttributes($structure, $entity)
    {
        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification $entity */
        $entity = $this->attributeRelationEntityFromStructureConverter
            ->setManyToManyRelationsDeleted(
                $structure,
                $entity,
                'OptionClassificationAttribute',
                'attributes',
                'Attribute'
            );

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\Optionclassification $entity */
        $entity = $this->attributeRelationEntityFromStructureConverter
            ->addNewManyToManyRelations(
                $structure,
                $entity,
                OptionClassificationAttribute::class,
                'attributes'
            );

        return $entity;
    }
}
