<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Events\OptionClassification\FrontendOptionClassificationsWithOptionsEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ItemRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ItemOptionClassification\ItemOptionClassificationSorter;
use Redhotmagma\ConfiguratorApiBundle\Service\Option\FrontendOptionStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CreatorView;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification as OptionClassificationStructure;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * @internal
 */
class FrontendOptionClassification
{
    private ItemRepository $itemRepository;

    private ItemOptionClassificationSorter $itemOptionClassificationSorter;

    private FrontendOptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter;

    private FrontendOptionStructureFromEntityConverter $optionStructureFromEntityConverter;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        ItemRepository $itemRepository,
        ItemOptionClassificationSorter $itemOptionClassificationSorter,
        FrontendOptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter,
        FrontendOptionStructureFromEntityConverter $optionStructureFromEntityConverter,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->itemRepository = $itemRepository;
        $this->itemOptionClassificationSorter = $itemOptionClassificationSorter;
        $this->optionClassificationStructureFromEntityConverter = $optionClassificationStructureFromEntityConverter;
        $this->optionStructureFromEntityConverter = $optionStructureFromEntityConverter;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return OptionClassificationStructure[]
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByItemId(int $id): array
    {
        $item = $this->itemRepository->findItemByIdWithOptionClassifications($id);
        if (null === $item) {
            throw NotFoundException::entity(Item::class);
        }

        return $this->getOptionClassificationStructureForItem($item);
    }

    /**
     * @return OptionClassificationStructure[]
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getByItemIdentifier(string $itemIdentifier): array
    {
        $item = $this->itemRepository->findItemByIdentifierWithOptionClassificationsAndOptions($itemIdentifier);

        if (null === $item) {
            throw NotFoundException::entity(Item::class);
        }

        $event = new FrontendOptionClassificationsWithOptionsEvent($item);
        $this->eventDispatcher->dispatch($event, FrontendOptionClassificationsWithOptionsEvent::NAME);

        return $event->getOptionClassifications();
    }

    public function getOptionClassificationStructureForItem(Item $item, bool $withOptions = false): array
    {
        $itemOptionClassifications = $item->getItemOptionclassification()->toArray();

        if ($item->getOverwriteComponentOrder()) {
            $this->itemOptionClassificationSorter->sortAsc($itemOptionClassifications);
        }

        $optionClassificationStructures = [];

        /** @var ItemOptionclassification $itemOptionClassification */
        foreach ($itemOptionClassifications as $itemOptionClassification) {
            $optionClassificationStructure = $this->optionClassificationStructureFromEntityConverter->convertOne($itemOptionClassification->getOptionclassification());

            $optionClassificationStructure->is_multiselect = $itemOptionClassification->getIsMultiselect();
            $optionClassificationStructure->is_mandatory = $itemOptionClassification->getIsMandatory();
            $optionClassificationStructure->optionsCount = $itemOptionClassification->getItemOptionclassificationOption()->count();
            $creatorView = $itemOptionClassification->getCreatorView();
            if (null !== $creatorView) {
                $optionClassificationStructure->creatorView = new CreatorView($creatorView->getIdentifier(), $creatorView->getSequenceNumber());
            }
            if (true === $withOptions) {
                $this->addOptionsToStructure($optionClassificationStructure, $itemOptionClassification);
            }

            $optionClassificationStructures[] = $optionClassificationStructure;
        }

        return $optionClassificationStructures;
    }

    protected function addOptionsToStructure(OptionClassificationStructure $optionClassification, ItemOptionclassification $itemOptionClassification): void
    {
        $optionClassification->options = [];
        /** @var ItemOptionclassificationOption $itemOptionClassificationOption */
        foreach ($itemOptionClassification->getItemOptionclassificationOption() as $itemOptionClassificationOption) {
            $option = $this->optionStructureFromEntityConverter->convertOne($itemOptionClassificationOption->getOption());
            $option->amount_is_selectable = $itemOptionClassificationOption->getAmountisselectable();
            $optionClassification->options[] = $option;
        }
    }
}
