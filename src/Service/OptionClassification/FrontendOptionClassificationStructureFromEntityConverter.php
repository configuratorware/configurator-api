<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\FrontendAttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;

/**
 * @internal
 */
class FrontendOptionClassificationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var FrontendAttributeRelationStructureFromEntityConverter
     */
    private $attributeRelationStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * FrontendOptionClassificationStructureFromEntityConverter constructor.
     *
     * @param FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        FrontendAttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification $entity
     * @param string $structureClassName
     *
     * @return OptionClassification
     */
    public function convertOne($entity, $structureClassName = OptionClassification::class)
    {
        /** @var OptionClassification $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure->title = $entity->getTranslatedTitle();
        $structure->description = $entity->getTranslatedDescription();

        $structure = $this->addOptionClassificationAttributesToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = OptionClassification::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * add attribute relations to structure.
     *
     * @param   OptionClassification $structure
     * @param   \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification $entity
     *
     * @return  OptionClassification
     */
    protected function addOptionClassificationAttributesToStructure($structure, $entity)
    {
        $optionClassificationAttributes = $entity->getOptionClassificationAttribute();

        $attributeStructures = [];
        if (!empty($optionClassificationAttributes->toArray())) {
            $attributeStructures = $this->attributeRelationStructureFromEntityConverter->convertMany($optionClassificationAttributes->toArray());
        }
        $structure->attributes = $attributeStructures;

        return $structure;
    }
}
