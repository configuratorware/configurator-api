<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;

/**
 * @internal
 */
class OptionClassificationSave
{
    /**
     * @var OptionclassificationRepository
     */
    private $optionClassificationRepository;

    /**
     * @var OptionClassificationEntityFromStructureConverter
     */
    private $optionClassificationEntityFromStructureConverter;

    /**
     * @var OptionClassificationStructureFromEntityConverter
     */
    private $optionClassificationStructureFromEntityConverter;

    /**
     * OptionClassificationSave constructor.
     *
     * @param OptionclassificationRepository $optionClassificationRepository
     * @param OptionClassificationEntityFromStructureConverter $optionClassificationEntityFromStructureConverter
     * @param OptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter
     */
    public function __construct(
        OptionclassificationRepository $optionClassificationRepository,
        OptionClassificationEntityFromStructureConverter $optionClassificationEntityFromStructureConverter,
        OptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter
    ) {
        $this->optionClassificationRepository = $optionClassificationRepository;
        $this->optionClassificationEntityFromStructureConverter = $optionClassificationEntityFromStructureConverter;
        $this->optionClassificationStructureFromEntityConverter = $optionClassificationStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification $optionClassification
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification $optionClassification)
    {
        $entity = null;
        if (isset($optionClassification->id) && $optionClassification->id > 0) {
            $entity = $this->optionClassificationRepository->findOneBy(['id' => $optionClassification->id]);
        }
        $entity = $this->optionClassificationEntityFromStructureConverter->convertOne($optionClassification, $entity);
        $currentSequenceNumber = $entity->getSequencenumber();
        if (null === $currentSequenceNumber || 0 === $currentSequenceNumber) {
            $entity->setSequencenumber($this->optionClassificationRepository->getNextSequenceNumber());
        }
        $entity = $this->optionClassificationRepository->save($entity);

        /** @var OptionClassification $entity */
        $entity = $this->optionClassificationRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->optionClassificationStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
