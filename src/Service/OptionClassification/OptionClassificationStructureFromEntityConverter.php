<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\AttributeRelation\AttributeRelationStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassificationText;

/**
 * @internal
 */
class OptionClassificationStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var AttributeRelationStructureFromEntityConverter
     */
    private $attributeRelationStructureFromEntityConverter;

    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * OptionClassificationStructureFromEntityConverter constructor.
     *
     * @param AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        AttributeRelationStructureFromEntityConverter $attributeRelationStructureFromEntityConverter,
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->attributeRelationStructureFromEntityConverter = $attributeRelationStructureFromEntityConverter;
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification $entity
     * @param string $structureClassName
     *
     * @return OptionClassification
     */
    public function convertOne($entity, $structureClassName = OptionClassification::class)
    {
        /** @var OptionClassification $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addOptionClassificationTextsToStructure($structure, $entity);
        $structure = $this->addOptionClassificationAttributesToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = OptionClassification::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param OptionClassification $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification $entity
     *
     * @return mixed
     */
    protected function addOptionClassificationTextsToStructure($structure, $entity)
    {
        $texts = [];

        $optionClassificationTexts = $entity->getOptionClassificationText();

        if (!empty($optionClassificationTexts)) {
            foreach ($optionClassificationTexts as $optionClassificationText) {
                $optionClassificationTextStructure = $this->structureFromEntityConverter->convertOne($optionClassificationText,
                    OptionClassificationText::class);

                if (!empty($optionClassificationTextStructure)) {
                    $optionClassificationTextStructure->language = $optionClassificationText->getLanguage()
                        ->getIso();
                    $texts[] = $optionClassificationTextStructure;
                }
            }
        }

        $structure->texts = $texts;

        return $structure;
    }

    /**
     * add attribute relations to structure.
     *
     * @param   OptionClassification $structure
     * @param   \Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification $entity
     *
     * @return  OptionClassification
     */
    protected function addOptionClassificationAttributesToStructure($structure, $entity)
    {
        $optionClassificationAttributes = $entity->getOptionClassificationAttribute();

        $attributeStructures = [];
        if (!empty($optionClassificationAttributes->toArray())) {
            $attributeStructures = $this->attributeRelationStructureFromEntityConverter->convertMany($optionClassificationAttributes->toArray());
        }
        $structure->attributes = $attributeStructures;

        return $structure;
    }
}
