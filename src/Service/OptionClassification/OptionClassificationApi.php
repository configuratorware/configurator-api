<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\FileRepository\ComponentThumbnailRepositoryInterface;
use Redhotmagma\ConfiguratorApiBundle\Image\ComponentThumbnail;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\SequenceNumberData;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionClassification as OptionClassificationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\UploadComponentThumbnail;

class OptionClassificationApi
{
    private ComponentThumbnailRepositoryInterface $componentThumbnailRepository;

    private OptionclassificationRepository $optionClassificationRepository;

    private OptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter;

    private OptionClassificationSave $optionClassificationSave;

    private OptionClassificationDelete $optionClassificationDelete;

    private OptionClassificationTreeBuilder $optionClassificationTreeBuilder;

    public function __construct(
        ComponentThumbnailRepositoryInterface $componentThumbnailRepository,
        OptionclassificationRepository $optionClassificationRepository,
        OptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter,
        OptionClassificationSave $optionClassificationSave,
        OptionClassificationDelete $optionClassificationDelete,
        OptionClassificationTreeBuilder $optionClassificationTreeBuilder
    ) {
        $this->componentThumbnailRepository = $componentThumbnailRepository;
        $this->optionClassificationRepository = $optionClassificationRepository;
        $this->optionClassificationStructureFromEntityConverter = $optionClassificationStructureFromEntityConverter;
        $this->optionClassificationSave = $optionClassificationSave;
        $this->optionClassificationDelete = $optionClassificationDelete;
        $this->optionClassificationTreeBuilder = $optionClassificationTreeBuilder;
    }

    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $paginationResult->data = $this->optionClassificationTreeBuilder->buildTree($arguments, null,
            $this->getSearchableFields());

        $arguments->filters['_parent_id'] = null;
        $paginationResult->count = $this->optionClassificationRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @throws NotFoundException
     */
    public function getOne(int $id): OptionClassificationStructure
    {
        /** @var OptionClassification $entity */
        $entity = $this->optionClassificationRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->optionClassificationStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    public function save(OptionClassificationStructure $optionClassification): OptionClassificationStructure
    {
        $structure = $this->optionClassificationSave->save($optionClassification);

        return $structure;
    }

    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments): void
    {
        /** @var SequenceNumberData $sequenceNumberData */
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            /** @var Optionclassification $entity */
            $entity = $this->optionClassificationRepository->findOneBy(['id' => $sequenceNumberData->id]);

            if (is_object($entity)) {
                $entity->setSequencenumber($sequenceNumberData->sequencenumber);
                $this->optionClassificationRepository->save($entity, false);
            }
        }

        $this->optionClassificationRepository->flush();
    }

    public function delete(string $id): void
    {
        $this->optionClassificationDelete->delete($id);
    }

    public function uploadThumbnail(UploadComponentThumbnail $uploadComponentThumbnail): void
    {
        $component = $this->optionClassificationRepository->find($uploadComponentThumbnail->componentId);

        $thumbnail = ComponentThumbnail::from(null, $component->getIdentifier(), $uploadComponentThumbnail->realPath, $uploadComponentThumbnail->fileName);

        $this->componentThumbnailRepository->saveThumbnail($thumbnail, $component);
    }

    public function deleteThumbnail(int $id): void
    {
        $component = $this->optionClassificationRepository->find($id);

        $thumbnail = $this->componentThumbnailRepository->findThumbnail($component);

        $this->componentThumbnailRepository->deleteThumbnail($thumbnail);
    }

    /**
     * @return string[]
     */
    protected function getSearchableFields()
    {
        return ['identifier', 'optionClassificationText.title'];
    }
}
