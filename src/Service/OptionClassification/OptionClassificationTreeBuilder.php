<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

/**
 * @internal
 */
class OptionClassificationTreeBuilder
{
    /**
     * @var OptionclassificationRepository
     */
    private $optionClassificationRepository;

    /**
     * @var OptionClassificationStructureFromEntityConverter
     */
    private $optionClassificationStructureFromEntityConverter;

    /**
     * OptionClassificationTreeBuilder constructor.
     *
     * @param OptionclassificationRepository $optionClassificationRepository
     * @param OptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter
     */
    public function __construct(
        OptionclassificationRepository $optionClassificationRepository,
        OptionClassificationStructureFromEntityConverter $optionClassificationStructureFromEntityConverter
    ) {
        $this->optionClassificationRepository = $optionClassificationRepository;
        $this->optionClassificationStructureFromEntityConverter = $optionClassificationStructureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $parameters
     * @param int $parentId
     * @param array $searchableFields
     *
     * @return array
     */
    public function buildTree($parameters, $parentId = null, $searchableFields = [])
    {
        $structures = [];
        $parameters->filters['_parent_id'] = $parentId;

        $data = $this->optionClassificationRepository->fetchList($parameters, $searchableFields);

        if (!empty($data)) {
            $structures = $this->optionClassificationStructureFromEntityConverter->convertMany($data);

            foreach ($structures as $structure) {
                $structure->children = $this->buildTree($parameters, $structure->id, $searchableFields);
            }
        }

        return $structures;
    }
}
