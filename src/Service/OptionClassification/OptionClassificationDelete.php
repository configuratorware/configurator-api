<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionClassification;

use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassification;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemOptionclassificationOption;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionclassificationRepository;

/**
 * @internal
 */
class OptionClassificationDelete
{
    private OptionclassificationRepository $optionClassificationRepository;

    public function __construct(OptionclassificationRepository $optionClassificationRepository)
    {
        $this->optionClassificationRepository = $optionClassificationRepository;
    }

    public function delete(string $id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var OptionClassification $entity */
            $entity = $this->optionClassificationRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            // delete texts
            if (!empty($entity->getOptionClassificationtext()->toArray())) {
                foreach ($entity->getOptionClassificationtext() as $optionClassificationText) {
                    $this->optionClassificationRepository->delete($optionClassificationText);
                }
            }

            // delete attribute relations
            if (!empty($entity->getOptionClassificationAttribute()->toArray())) {
                foreach ($entity->getOptionClassificationAttribute() as $optionClassificationAttribute) {
                    $this->optionClassificationRepository->delete($optionClassificationAttribute);
                }
            }

            // delete construction pattern relations
            if (!empty($entity->getItemOptionclassification()->toArray())) {
                /** @var ItemOptionclassification $itemOptionClassification */
                foreach ($entity->getItemOptionclassification() as $itemOptionClassification) {
                    if (!empty($itemOptionClassification->getItemOptionclassificationOption()->toArray())) {
                        /** @var ItemOptionclassificationOption $itemOptionClassificationOption */
                        foreach ($itemOptionClassification->getItemOptionclassificationOption() as $itemOptionClassificationOption) {
                            if (!empty($itemOptionClassificationOption->getItemOptionclassificationOptionDeltaprice()->toArray())) {
                                foreach ($itemOptionClassificationOption->getItemOptionclassificationOptionDeltaprice() as $deltaPrice) {
                                    $this->optionClassificationRepository->delete($deltaPrice);
                                }
                            }

                            $this->optionClassificationRepository->delete($itemOptionClassificationOption);
                        }
                    }

                    $this->optionClassificationRepository->delete($itemOptionClassification);
                }
            }

            $this->optionClassificationRepository->delete($entity);
        }
    }
}
