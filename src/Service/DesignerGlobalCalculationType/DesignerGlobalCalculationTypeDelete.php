<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository;

/**
 * @internal
 */
class DesignerGlobalCalculationTypeDelete
{
    /**
     * @var DesignerGlobalCalculationTypeRepository
     */
    private $designerGlobalCalculationTypeRepository;

    /**
     * @param DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
     */
    public function __construct(
        DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
    ) {
        $this->designerGlobalCalculationTypeRepository = $designerGlobalCalculationTypeRepository;
    }

    /**
     * @param string $ids
     */
    public function delete($ids): void
    {
        $ids = explode(',', $ids);
        foreach ($ids as $id) {
            /**
             * Get Entity for given Id.
             *
             * @var DesignerGlobalCalculationType $entity
             */
            $entity = $this->designerGlobalCalculationTypeRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            $this->removeRelation($entity, 'DesignerGlobalCalculationTypeText');
            $this->removeRelation($entity, 'DesignerGlobalCalculationTypePrice');

            $this->designerGlobalCalculationTypeRepository->delete($entity);
        }
    }

    /**
     * @param DesignerGlobalCalculationType $entity
     * @param string $relationName
     */
    protected function removeRelation(DesignerGlobalCalculationType $entity, string $relationName): void
    {
        $callEntity = 'get' . $relationName;
        /** @var mixed $relations */
        $relations = $entity->$callEntity();
        if (!$relations->isEmpty()) {
            foreach ($relations as $relationEntry) {
                $this->designerGlobalCalculationTypeRepository->delete($relationEntry);
            }
        }
    }
}
