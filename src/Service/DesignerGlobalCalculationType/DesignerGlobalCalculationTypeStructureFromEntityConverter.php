<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalCalculationType;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypePrice;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationTypeText;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationTypePrice as DesignerGlobalCalculationTypePriceStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationTypeText as DesignerGlobalCalculationTypeTextStructure;

/**
 * @internal
 */
class DesignerGlobalCalculationTypeStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * DesignerGlobalCalculationTypeStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param DesignerGlobalCalculationType $entity
     * @param string $structureClassName
     *
     * @return mixed|DesignerGlobalCalculationTypeStructure
     */
    public function convertOne($entity, $structureClassName = DesignerGlobalCalculationTypeStructure::class)
    {
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addTextsToStructure(
            $structure,
            $entity,
            'DesignerGlobalCalculationType',
            DesignerGlobalCalculationTypeTextStructure::class
        );

        $structure = $this->addPricesToStructure(
            $structure,
            $entity,
            'DesignerGlobalCalculationType',
            DesignerGlobalCalculationTypePriceStructure::class
        );

        return $structure;
    }

    /**
     * @param $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = DesignerGlobalCalculationTypeStructure::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structures[] = $this->convertOne($entity);
        }

        return $structures;
    }

    /**
     * @param DesignerGlobalCalculationTypeStructure $structure
     * @param DesignerGlobalCalculationType $entity
     * @param string $entityName
     * @param string $textStructureClass
     *
     * @return DesignerGlobalCalculationTypeStructure
     */
    protected function addTextsToStructure(
        DesignerGlobalCalculationTypeStructure $structure,
        DesignerGlobalCalculationType $entity,
        string $entityName,
        string $textStructureClass
    ): DesignerGlobalCalculationTypeStructure {
        $callEntity = 'get' . $entityName . 'Text';

        $texts = $entity->$callEntity();

        if (!empty($texts)) {
            /** @var DesignerGlobalCalculationTypeText $text */
            foreach ($texts as $text) {
                /** @var DesignerGlobalCalculationTypeTextStructure $textStructure */
                $textStructure = $this->structureFromEntityConverter->convertOne(
                    $text,
                    $textStructureClass
                );

                if (!empty($textStructure)) {
                    $textStructure->language = $text->getLanguage()->getIso();
                    $structure->texts[] = $textStructure;
                }
            }
        }

        return $structure;
    }

    /**
     * @param DesignerGlobalCalculationTypeStructure $structure
     * @param DesignerGlobalCalculationType $entity
     * @param string $entityName
     * @param string $priceStructureClass
     *
     * @return DesignerGlobalCalculationTypeStructure
     */
    protected function addPricesToStructure(
        DesignerGlobalCalculationTypeStructure $structure,
        DesignerGlobalCalculationType $entity,
        string $entityName,
        string $priceStructureClass
    ): DesignerGlobalCalculationTypeStructure {
        $callEntity = 'get' . $entityName . 'Price';

        $prices = $entity->$callEntity();

        if (!empty($prices)) {
            /** @var DesignerGlobalCalculationTypePrice $price */
            foreach ($prices as $price) {
                /** @var DesignerGlobalCalculationTypePriceStructure $priceStructure */
                $priceStructure = $this->structureFromEntityConverter->convertOne(
                    $price,
                    $priceStructureClass
                );

                if (!empty($priceStructure)) {
                    $priceStructure->channel = $price->getChannel()->getId();
                    $structure->prices[] = $priceStructure;
                }
            }
        }

        return $structure;
    }
}
