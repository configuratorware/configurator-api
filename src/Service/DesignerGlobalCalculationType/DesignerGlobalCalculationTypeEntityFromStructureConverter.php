<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalCalculationType;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeStructure;

/**
 * @internal
 */
class DesignerGlobalCalculationTypeEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var ChannelRepository
     */
    private $channelRepository;

    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * DesignerGlobalCalculationTypeEntityFromStructureConverter constructor.
     *
     * @param ChannelRepository $channelRepository
     * @param EntityFromStructureConverter $entityFromStructureConverter
     */
    public function __construct(
        ChannelRepository $channelRepository,
        EntityFromStructureConverter $entityFromStructureConverter
    ) {
        $this->channelRepository = $channelRepository;
        $this->entityFromStructureConverter = $entityFromStructureConverter;
    }

    /**
     * @param DesignerGlobalCalculationTypeStructure $structure
     * @param DesignerGlobalCalculationType|null $entity
     * @param string $entityClassName
     *
     * @return mixed
     */
    public function convertOne(
        $structure,
        $entity,
        $entityClassName = DesignerGlobalCalculationType::class
    ) {
        $entity = $this->entityFromStructureConverter->convertOne($structure, $entity, $entityClassName);

        /** @var DesignerGlobalCalculationType $entity */
        $entity = $this->updateManyToOneRelations(
            $entity,
            $structure,
            'texts',
            'DesignerGlobalCalculationTypeText'
        );

        $entity = $this->updatePrices(
            $entity,
            $structure
        );

        return $entity;
    }

    /**
     * @param DesignerGlobalCalculationType $entity
     * @param DesignerGlobalCalculationTypeStructure $structure
     * @param string $relationStructureName
     * @param string $relationEntityName
     *
     * @return DesignerGlobalCalculationType
     */
    protected function updateManyToOneRelations(
        DesignerGlobalCalculationType $entity,
        DesignerGlobalCalculationTypeStructure $structure,
        string $relationStructureName,
        string $relationEntityName
    ): DesignerGlobalCalculationType {
        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations(
            $structure,
            $entity,
            $relationStructureName,
            $relationEntityName
        );

        return $entity;
    }

    /**
     * @param DesignerGlobalCalculationType $entity
     * @param DesignerGlobalCalculationTypeStructure $structure
     *
     * @return DesignerGlobalCalculationType
     */
    protected function updatePrices(
        DesignerGlobalCalculationType $entity,
        DesignerGlobalCalculationTypeStructure $structure
    ): DesignerGlobalCalculationType {
        // add channel relation objects
        if (!empty($structure->prices)) {
            foreach ($structure->prices as $key => $priceStructure) {
                if (!empty($priceStructure->channel)) {
                    $structure->prices[$key]->channel = $this->channelRepository->findOneBy(['id' => $priceStructure->channel]);
                }
            }
        }

        $entity = $this->updateManyToOneRelations(
            $entity,
            $structure,
            'prices',
            'DesignerGlobalCalculationTypePrice'
        );

        return $entity;
    }
}
