<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class DesignerGlobalCalculationTypeApi
{
    /**
     * @var DesignerGlobalCalculationTypeRepository
     */
    private $designerGlobalCalculationTypeRepository;

    /**
     * @var DesignerGlobalCalculationTypeStructureFromEntityConverter
     */
    private $designerGlobalCalculationTypeStructureFromEntityConverter;

    /**
     * DesignerGlobalCalculationTypeSave $$designerGlobalCalculationTypeSave.
     */
    private $designerGlobalCalculationTypeSave;

    /**
     * DesignerGlobalCalculationTypeDelete $designerGlobalCalculationTypeDelete.
     */
    private $designerGlobalCalculationTypeDelete;

    /**
     * DesignerGlobalCalculationTypeApi constructor.
     *
     * @param DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
     * @param DesignerGlobalCalculationTypeStructureFromEntityConverter $designerGlobalCalculationTypeStructureFromEntityConverter
     * @param DesignerGlobalCalculationTypeSave $designerGlobalCalculationTypeSave
     * @param DesignerGlobalCalculationTypeDelete $designerGlobalCalculationTypeDelete
     */
    public function __construct(
        DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository,
        DesignerGlobalCalculationTypeStructureFromEntityConverter $designerGlobalCalculationTypeStructureFromEntityConverter,
        DesignerGlobalCalculationTypeSave $designerGlobalCalculationTypeSave,
        DesignerGlobalCalculationTypeDelete $designerGlobalCalculationTypeDelete
    ) {
        $this->designerGlobalCalculationTypeRepository = $designerGlobalCalculationTypeRepository;
        $this->designerGlobalCalculationTypeStructureFromEntityConverter = $designerGlobalCalculationTypeStructureFromEntityConverter;
        $this->designerGlobalCalculationTypeSave = $designerGlobalCalculationTypeSave;
        $this->designerGlobalCalculationTypeDelete = $designerGlobalCalculationTypeDelete;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(
        ListRequestArguments $arguments
    ): PaginationResult {
        $paginationResult = new PaginationResult();

        $entities = $this->designerGlobalCalculationTypeRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult->data = $this->designerGlobalCalculationTypeStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->designerGlobalCalculationTypeRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return DesignerGlobalCalculationTypeStructure
     *
     * @throws NotFoundException
     */
    public function getOne(
        int $id
    ): DesignerGlobalCalculationTypeStructure {
        /** @var DesignerGlobalCalculationType $entity */
        $entity = $this->designerGlobalCalculationTypeRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->designerGlobalCalculationTypeStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param DesignerGlobalCalculationTypeStructure $structure
     *
     * @return DesignerGlobalCalculationTypeStructure
     */
    public function save(
        DesignerGlobalCalculationTypeStructure $structure
    ): DesignerGlobalCalculationTypeStructure {
        $structure = $this->designerGlobalCalculationTypeSave->save($structure);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete(string $ids): void
    {
        $this->designerGlobalCalculationTypeDelete->delete($ids);
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['designerGlobalCalculationTypeText.title'];
    }
}
