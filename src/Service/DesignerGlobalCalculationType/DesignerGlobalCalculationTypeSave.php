<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\DesignerGlobalCalculationType;

use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;
use Redhotmagma\ConfiguratorApiBundle\Repository\DesignerGlobalCalculationTypeRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\DesignerGlobalCalculationType as DesignerGlobalCalculationTypeStructure;

/**
 * @internal
 */
class DesignerGlobalCalculationTypeSave
{
    /**
     * @var DesignerGlobalCalculationTypeRepository
     */
    private $designerGlobalCalculationTypeRepository;

    /**
     * @var DesignerGlobalCalculationTypeEntityFromStructureConverter
     */
    private $designerGlobalCalculationTypeEntityFromStructureConverter;

    /**
     * @var DesignerGlobalCalculationTypeStructureFromEntityConverter
     */
    private $designerGlobalCalculationTypeStructureFromEntityConverter;

    /**
     * @param DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository
     * @param DesignerGlobalCalculationTypeEntityFromStructureConverter $designerGlobalCalculationTypeEntityFromStructureConverter
     * @param DesignerGlobalCalculationTypeStructureFromEntityConverter $designerGlobalCalculationTypeStructureFromEntityConverter
     */
    public function __construct(
        DesignerGlobalCalculationTypeRepository $designerGlobalCalculationTypeRepository,
        DesignerGlobalCalculationTypeEntityFromStructureConverter $designerGlobalCalculationTypeEntityFromStructureConverter,
        DesignerGlobalCalculationTypeStructureFromEntityConverter $designerGlobalCalculationTypeStructureFromEntityConverter
    ) {
        $this->designerGlobalCalculationTypeRepository = $designerGlobalCalculationTypeRepository;
        $this->designerGlobalCalculationTypeEntityFromStructureConverter = $designerGlobalCalculationTypeEntityFromStructureConverter;
        $this->designerGlobalCalculationTypeStructureFromEntityConverter = $designerGlobalCalculationTypeStructureFromEntityConverter;
    }

    /**
     * @param DesignerGlobalCalculationTypeStructure $structure
     *
     * @return DesignerGlobalCalculationTypeStructure
     */
    public function save(DesignerGlobalCalculationTypeStructure $structure): DesignerGlobalCalculationTypeStructure
    {
        $entity = null;

        if (isset($structure->id) && $structure->id > 0) {
            $entity = $this->designerGlobalCalculationTypeRepository->findOneBy(['id' => $structure->id]);
        }

        /** @var DesignerGlobalCalculationType|null $entity */
        $entity = $this->designerGlobalCalculationTypeEntityFromStructureConverter->convertOne($structure, $entity);
        $entity = $this->designerGlobalCalculationTypeRepository->save($entity);

        $this->designerGlobalCalculationTypeRepository->clear();

        /** @var DesignerGlobalCalculationType $entity */
        $entity = $this->designerGlobalCalculationTypeRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->designerGlobalCalculationTypeStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
