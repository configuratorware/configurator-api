<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus;

use Redhotmagma\ConfiguratorApiBundle\Entity\Item;

/**
 * @internal
 */
class ItemStatusAvailability
{
    /**
     * @param Item $item
     *
     * @return bool
     */
    public function checkItemAvailability(Item $item): bool
    {
        $itemToCheck = $item->getParent() ?? $item;

        $itemStatus = $itemToCheck->getItemStatus();

        if (null === $itemStatus) {
            return false;
        }

        return $itemStatus->getItemAvailable();
    }
}
