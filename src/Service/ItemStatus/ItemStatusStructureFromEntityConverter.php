<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\ItemStatus;
use Redhotmagma\ConfiguratorApiBundle\Structure\ItemStatus as ItemStatusStructure;

/**
 * @internal
 */
class ItemStatusStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @param StructureFromEntityConverter $structureFromEntityConverter
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    public function convertOne($entity, $structureClassName = ItemStatusStructure::class)
    {
        /**
         * @var ItemStatusStructure $structure
         * @var ItemStatus $entity
         */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        return $structure;
    }

    public function convertMany($entities, $structureClassName = ItemStatusStructure::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            /* @var ItemStatus $entity */
            $structures[] = $this->convertOne($entity, $structureClassName);
        }

        return $structures;
    }
}
