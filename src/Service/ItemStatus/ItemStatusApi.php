<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ItemStatus;

use Redhotmagma\ConfiguratorApiBundle\Repository\ItemStatusRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class ItemStatusApi
{
    /**
     * @var ItemStatusRepository
     */
    private $itemStatusRepository;

    /**
     * @var ItemStatusStructureFromEntityConverter
     */
    private $itemStatusStructureFromEntityConverter;

    /**
     * ItemStatusApi constructor.
     *
     * @param ItemStatusRepository $itemStatusRepository
     * @param ItemStatusStructureFromEntityConverter $itemStatusStructureFromEntityConverter
     */
    public function __construct(
        ItemStatusRepository $itemStatusRepository,
        ItemStatusStructureFromEntityConverter $itemStatusStructureFromEntityConverter
    ) {
        $this->itemStatusRepository = $itemStatusRepository;
        $this->itemStatusStructureFromEntityConverter = $itemStatusStructureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(
        ListRequestArguments $arguments
    ): PaginationResult {
        $paginationResult = new PaginationResult();

        $entities = $this->itemStatusRepository->fetchList($arguments, $this->getSearchableFields());

        $paginationResult->data = $this->itemStatusStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->itemStatusRepository->fetchListCount(
            $arguments,
            $this->getSearchableFields()
        );

        return $paginationResult;
    }

    /**
     * a list of fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields(): array
    {
        return ['itemStatus.identifier'];
    }
}
