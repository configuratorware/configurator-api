<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models;

class Cover
{
    public string $title = '';

    public string $imagePath = '';

    public int $variantAmount = 0;

    public int $selectedItemAmount = 0;

    public string $itemSummary = '';

    public string $configurationCode = '';

    /**
     * Dynamic font size calculated for displaying configuration code.
     */
    public float $configurationCodeDynamicFontSize = 0;

    public string $configurationUrl = '';

    public int $itemSum = 0;

    public string $comment = '';
}
