<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models;

class Preview
{
    public string $title = '';

    public bool $isVisible = true;

    /**
     * contains a flat array of image paths.
     */
    public array $images = [];
}
