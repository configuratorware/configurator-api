<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models;

use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;

final class ComponentInfo
{
    public string $componentTitle;

    public string $optionTitle;

    public int $amount;

    public string $componentIdentifier;

    public string $optionIdentifier;

    /**
     * @var array<string,Option>
     */
    public array $selectedOptions;

    public OptionClassification $component;
}
