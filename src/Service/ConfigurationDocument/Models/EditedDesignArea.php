<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models;

class EditedDesignArea
{
    public string $title = '';

    public int $colorAmount = 0;

    public array $types = [];

    public string $designProductionMethodTitle = '';

    public int $designProductionMethodMaxColorAmount = 0;

    public array $images = [];

    public array $imagesBase64 = [];

    public array $colors = [];

    public ?int $width = 0;

    public ?int $height = 0;

    public array $editedDesignAreaElements = [];
}
