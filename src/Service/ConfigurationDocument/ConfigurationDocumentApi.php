<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument;

use Redhotmagma\ConfiguratorApiBundle\Events\ConfigurationDocument\DataCollectionEvent;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\FileResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ConfigurationDocumentApi
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @var DocumentGenerator
     */
    private $documentGenerator;

    /**
     * ConfigurationDocumentApi constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param DocumentGenerator $documentGenerator
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, DocumentGenerator $documentGenerator)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->documentGenerator = $documentGenerator;
    }

    /**
     * @param string $documentType
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     *
     * @return FileResult
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function generatePdf(string $documentType, string $configurationCode, ControlParameters $controlParameters): FileResult
    {
        $event = new DataCollectionEvent($configurationCode, $documentType, $controlParameters);

        $this->eventDispatcher->dispatch($event, DataCollectionEvent::NAME);

        $templateDataSet = $event->getDataSet();

        $pdf = $this->documentGenerator->generatePdf($templateDataSet);

        return new FileResult($pdf->getPath(), $templateDataSet['cover']->title . '_' . $configurationCode . '.pdf');
    }

    /**
     * @param string $documentType
     * @param string $configurationCode
     * @param ControlParameters $controlParameters
     *
     * @return string
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function generateHtml(string $documentType, string $configurationCode, ControlParameters $controlParameters): string
    {
        $event = new DataCollectionEvent($configurationCode, $documentType, $controlParameters);

        $this->eventDispatcher->dispatch($event, DataCollectionEvent::NAME);

        $html = $this->documentGenerator->generateHtml($event->getDataSet(), true);

        return $html->getContent();
    }
}
