<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument;

use Psr\Log\LoggerInterface;
use Redhotmagma\ConfiguratorApiBundle\Service\Configuration\PrintDesignDataProvider;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\EditedDesignArea;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @internal
 */
class EditedDesignAreaModifier
{
    private Filesystem $filesystem;

    private PrintDesignDataProvider $printDesignDataProvider;

    private LoggerInterface $logger;

    public function __construct(
        Filesystem $filesystem,
        PrintDesignDataProvider $printDesignDataProvider,
        LoggerInterface $logger
    ) {
        $this->filesystem = $filesystem;
        $this->printDesignDataProvider = $printDesignDataProvider;
        $this->logger = $logger;
    }

    /**
     * @param array $editedDesignArea
     *
     * @return EditedDesignArea[]
     */
    public function change(array $editedDesignArea): array
    {
        /** @var EditedDesignArea $area */
        foreach ($editedDesignArea as $area) {
            foreach ($area->images as $j => $image) {
                try {
                    $svg = $this->printDesignDataProvider->provide($image);
                } catch (\Throwable $exception) {
                    $this->logger->error($exception->getMessage(), [
                        'source' => sprintf('%s:%s', $exception->getFile(), $exception->getLine()),
                    ]);
                    $svg = '';
                }
                $area->images[$j] = $this->generatePngFromSvg($svg);
                $area->imagesBase64[$j] = $this->imageToBase64($area->images[$j]);
            }
        }

        return $editedDesignArea;
    }

    /**
     * @param string $svgContent
     *
     * @return string
     */
    private function generatePngFromSvg(string $svgContent): string
    {
        if (empty($svgContent)) {
            return $svgContent;
        }

        $tempFileName = $this->filesystem->tempnam(sys_get_temp_dir(), 'configuration_edited_design_area_');
        $pngPath = $tempFileName . '.png';
        $svgPath = $tempFileName . '.svg';

        try {
            if (0 !== strpos($svgContent, '<?xml')) {
                $svgContent = '<?xml version="1.0" encoding="UTF-8" standalone="no"?>' . $svgContent;
            }

            // replace visibility:hidden to display:none in screenshot SVGs,
            // because the conversion to PNG would display these elements
            if (false !== strpos($svgContent, 'visibility="hidden"')) {
                $svgContent = str_replace('visibility="hidden"', 'display="none"', $svgContent);
            }

            $this->filesystem->dumpFile($svgPath, $svgContent);

            $imagick = new \Imagick();
            $imagick->setBackgroundColor(new \ImagickPixel('transparent'));
            $imagick->readImage($svgPath);
            $imagick->setImageFormat('png');
            $imagick->writeImage($pngPath);
            $imagick->clear();
            $imagick->destroy();

            $this->filesystem->remove($svgPath);

            return $pngPath;
        } catch (\Throwable $exception) {
            $this->logger->error(sprintf('Failed to convert svg to png: %s', $exception->getMessage()), [
                'source' => sprintf('%s:%s', $exception->getFile(), $exception->getLine()),
            ]);
            $this->filesystem->remove($svgPath);
        }

        return $svgContent;
    }

    /**
     * Convert image to base64.
     *
     * @param string $imagePath
     *
     * @return array['type'=>'png', 'data' => '...base64image....']
     */
    private function imageToBase64(string $imagePath): array
    {
        if ($this->filesystem->exists($imagePath)) {
            return [
                'type' => pathinfo($imagePath, PATHINFO_EXTENSION),
                'data' => base64_encode(file_get_contents($imagePath)),
            ];
        }

        return [];
    }
}
