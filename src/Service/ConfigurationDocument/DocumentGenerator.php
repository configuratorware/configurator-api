<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument;

use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\Cover;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\Preview;
use Redhotmagma\ConfiguratorApiBundle\Service\Media\MediaHelper;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\TranslationsPaths;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Vector\Html;
use Redhotmagma\ConfiguratorApiBundle\Vector\HtmlPdfConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Vector\Pdf;
use Symfony\Component\Config\Definition\Exception\Exception;
use Twig\Environment;

/**
 * @internal
 */
class DocumentGenerator
{
    private Environment $engine;

    private HtmlPdfConverterInterface $htmlPdfConverter;

    private string $mediaBasePath;

    private TranslationsPaths $translationsPaths;

    private MediaHelper $mediaHelper;

    public function __construct(Environment $engine, HtmlPdfConverterInterface $htmlPdfConverter, string $mediaBasePath, TranslationsPaths $translationsPaths, MediaHelper $mediaHelper)
    {
        $this->engine = $engine;
        $this->htmlPdfConverter = $htmlPdfConverter;
        $this->mediaBasePath = $mediaBasePath;
        $this->translationsPaths = $translationsPaths;
        $this->mediaHelper = $mediaHelper;
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function generatePdf(array $customTemplateDataSet): Pdf
    {
        $html = $this->generateHtml($customTemplateDataSet);

        return $this->htmlPdfConverter->htmlToPdf($html);
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function generateHtml(array $customTemplateDataSet, bool $webPaths = false): Html
    {
        $rendered = $this->engine->render('@RedhotmagmaConfiguratorApi/configuration/main.html.twig', $this->mergeTemplateDataSet($customTemplateDataSet));

        if (true === $webPaths) {
            $rendered = str_replace($this->mediaBasePath, '', $rendered);
        }

        return Html::withContent($rendered);
    }

    private function mergeTemplateDataSet(array $customTemplateDataSet): array
    {
        $translations = $this->getLanguageFileContents();

        if (array_key_exists('translations', $customTemplateDataSet)) {
            $translations = array_replace($translations, $customTemplateDataSet['translations']);
        }

        $templateDataSet = array_replace([
            'calculationResult' => new CalculationResult(),
            'cover' => new Cover(),
            'documentInfo' => [],
            'editedDesignAreas' => [],
            'preview' => new Preview(),
            'options' => [],
        ], $customTemplateDataSet);

        $templateDataSet['translations'] = $translations;
        $templateDataSet['mediaBasePath'] = $this->mediaBasePath;
        $templateDataSet['hostName'] = $this->mediaHelper->getImageBaseUrl();

        return $templateDataSet;
    }

    private function getLanguageFileContents(): array
    {
        $languageSpecific = $this->translationsPaths->getBaseIsoFilePath(C_LANGUAGE_ISO);
        if (file_exists($languageSpecific)) {
            return json_decode(file_get_contents($languageSpecific), true);
        }

        $fallback = $this->translationsPaths->getBaseIsoFilePath('en_GB');
        if (file_exists($fallback)) {
            return json_decode(file_get_contents($fallback), true);
        }

        throw new Exception('Translations Files not found.');
    }
}
