<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument;

use Doctrine\ORM\NonUniqueResultException;
use Redhotmagma\ConfiguratorApiBundle\Entity\Configuration;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignArea;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignAreaDesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignProductionMethod;
use Redhotmagma\ConfiguratorApiBundle\Entity\Item;
use Redhotmagma\ConfiguratorApiBundle\Events\Calculation\CalculateEvent;
use Redhotmagma\ConfiguratorApiBundle\Exception\MinimumOrderAmountException;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ColorRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\ComponentInfo;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\Cover;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\EditedDesignArea;
use Redhotmagma\ConfiguratorApiBundle\Service\ConfigurationDocument\Models\Preview;
use Redhotmagma\ConfiguratorApiBundle\Service\Item\FrontendItemGroupingWalker;
use Redhotmagma\ConfiguratorApiBundle\Service\Setting\ShareUrlResolver;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ConfigurationImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Settings\Paths\ItemImagePathsInterface;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\CalculationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Configuration as FrontendConfigurationStructure;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\OptionClassification;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ControlParameters;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class DataCollector
{
    private string $clientLogoBasePath;

    private ColorRepository $colorRepository;

    private EventDispatcherInterface $eventDispatcher;

    private FrontendItemGroupingWalker $frontendItemGroupingWalker;

    private string $mediaBasePath;

    private ShareUrlResolver $shareUrl;

    private ItemImagePathsInterface $itemImagePaths;

    private ConfigurationImagePathsInterface $configurationImagePaths;

    public function __construct(
        string $clientLogoBasePath,
        ColorRepository $colorRepository,
        EventDispatcherInterface $eventDispatcher,
        FrontendItemGroupingWalker $frontendItemGroupingWalker,
        string $mediaBasePath,
        ShareUrlResolver $shareUrl,
        ItemImagePathsInterface $itemImagePaths,
        ConfigurationImagePathsInterface $configurationImagePaths
    ) {
        $this->clientLogoBasePath = $clientLogoBasePath;
        $this->colorRepository = $colorRepository;
        $this->eventDispatcher = $eventDispatcher;
        $this->frontendItemGroupingWalker = $frontendItemGroupingWalker;
        $this->mediaBasePath = rtrim($mediaBasePath, '/');
        $this->shareUrl = $shareUrl;
        $this->itemImagePaths = $itemImagePaths;
        $this->configurationImagePaths = $configurationImagePaths;
    }

    public function create(
        FrontendConfigurationStructure $frontendConfiguration,
        Configuration $configurationEntity,
        string $documentType,
        ControlParameters $controlParameters
    ): array {
        return [
            'calculationResult' => $this->getCalculationResult($frontendConfiguration),
            'cover' => $this->createCover($frontendConfiguration, $controlParameters),
            'documentInfo' => $this->getDocumentInfo($documentType),
            'editedDesignAreas' => $this->createEditedDesignAreas($frontendConfiguration, $configurationEntity),
            'preview' => $this->createPreview($frontendConfiguration),
            'componentInfos' => $this->createComponentData($frontendConfiguration),
        ];
    }

    private function getCalculationResult(FrontendConfigurationStructure $frontendConfiguration): ?CalculationResult
    {
        $event = new CalculateEvent($frontendConfiguration);

        try {
            $this->eventDispatcher->dispatch($event, CalculateEvent::NAME);

            return $event->getCalculationResult();
        } catch (MinimumOrderAmountException $exception) {
            // catch exception and let the document be generated even if calculation is invalid
            return null;
        }
    }

    private function createCover(
        FrontendConfigurationStructure $frontendConfiguration,
        ControlParameters $controlParameters
    ): Cover {
        $cover = new Cover();
        $cover->title = $frontendConfiguration->item->title ?? '';
        $customData = $frontendConfiguration->customdata;

        $selectedChildren = [];

        if (null !== $customData && isset($customData->selectedAmounts)) {
            $childrenFlat = $this->frontendItemGroupingWalker->walk($frontendConfiguration->item);
            $amounts = $customData->selectedAmounts;

            foreach ($childrenFlat as $childFlat) {
                $identifier = $childFlat->identifier;
                if (isset($amounts->$identifier)) {
                    $selectedChildren[] = [
                        'identifier' => $childFlat->identifier,
                        'title' => $childFlat->title,
                        'amount' => $amounts->$identifier,
                        'itemGroupEntryTitle' => $childFlat->itemgroupentryTitle ?? '',
                    ];
                }
            }
        }

        if (null !== $customData && isset($customData->comment)) {
            $cover->comment = $customData->comment;
        }

        $cover->imagePath = $this->selectCoverImage($frontendConfiguration, $selectedChildren);

        $itemCount = count($selectedChildren);
        $cover->variantAmount = $itemCount;
        foreach ($selectedChildren as $key => $child) {
            $itemTitle = $child['itemGroupEntryTitle'] ?? $child['title'];
            $cover->itemSummary .= $child['amount'] . ' x ' . $itemTitle . ' (' . $child['identifier'] . ')';

            if ($itemCount > $key + 1) {
                $cover->itemSummary .= ', ';
            }

            $cover->selectedItemAmount += $child['amount'];
        }

        $itemIdentifier = $frontendConfiguration->item->identifier;
        if ([] === $selectedChildren && isset($customData->selectedAmounts, $customData->selectedAmounts->$itemIdentifier)) {
            $cover->selectedItemAmount = $customData->selectedAmounts->$itemIdentifier;
        }

        $cover->configurationCode = $frontendConfiguration->code;
        $cover->configurationCodeDynamicFontSize = $this->calculateConfigCodeDynamicFontSize($cover->configurationCode);
        $cover->configurationUrl = $this->shareUrl->getShareUrl($frontendConfiguration->code, $controlParameters);

        return $cover;
    }

    private function selectCoverImage(
        FrontendConfigurationStructure $frontendConfiguration,
        array $selectedChildren
    ): string {
        $default = $this->getItemImagePath($frontendConfiguration->item->identifier);

        // if it is a creator item return preview or fallback to $default
        if (false !== strpos($this->determineConfigurationMode($frontendConfiguration), Item::CONFIGURATION_MODE_CREATOR)) {
            return isset($frontendConfiguration->previewImageURL) && '' !== $frontendConfiguration->previewImageURL
                ? $this->mediaBasePath . $frontendConfiguration->previewImageURL
                : $default;
        }

        // if there are multiple children, the alphabetically first color/dimension should be selected
        usort($selectedChildren, static function ($a, $b) {
            return $a['itemGroupEntryTitle'] <=> $b['itemGroupEntryTitle'];
        });

        $firstChild = current($selectedChildren);
        if (false === $firstChild) {
            return $default;
        }

        return $this->getItemImagePath($firstChild['identifier'], $default);
    }

    /**
     * Calculate a dynamic font size based on configuration code length.
     */
    private function calculateConfigCodeDynamicFontSize(string $configCode): float
    {
        if (strlen($configCode) > 30) {
            return 10;
        }

        return 40 - strlen($configCode) + sqrt(strlen($configCode)) / 3;
    }

    private function getDocumentInfo(string $documentType): array
    {
        return [
            'currentDate' => date(C_LANGUAGE_SETTINGS['dateformat']),
            'channelSettings' => C_CHANNEL_SETTINGS,
            'documentType' => $documentType,
            'logoPath' => $this->getLogoPath(),
            'theme' => C_CLIENT_SETTINGS['theme'],
            'contactEmail' => C_CLIENT_SETTINGS['contactEmail'],
            'pdfHeader' => C_CLIENT_SETTINGS['pdfHeader'],
            'pdfFooter' => C_CLIENT_SETTINGS['pdfFooter'],
            'displayNetPrices' => CALCULATION_PRICES_NET,
        ];
    }

    private function getLogoPath(): ?string
    {
        $logoPath = $this->clientLogoBasePath . C_CLIENT . '.png';
        // change logo to client logo, if available
        if (isset(C_CLIENT_SETTINGS['theme']['logo']) && !empty(C_CLIENT_SETTINGS['theme']['logo'])) {
            $logoPath = $this->mediaBasePath . C_CLIENT_SETTINGS['theme']['logo'];
        }

        return file_exists($logoPath) ? $logoPath : null;
    }

    /**
     * @return EditedDesignArea[]
     *
     * @throws NonUniqueResultException
     */
    private function createEditedDesignAreas(
        FrontendConfigurationStructure $frontendConfiguration,
        Configuration $configurationEntity
    ): array {
        $editedDesignAreas = [];
        $designAreaEntities = $configurationEntity->getItem()->getDesignArea();

        foreach ((array)$frontendConfiguration->designdata as $designAreaIdentifier => $designAreaDesignData) {
            if (!isset($designAreaDesignData->canvasData->objects)) {
                continue;
            }

            $filteredAreas = $designAreaEntities->filter(static function (DesignArea $designArea) use (
                $designAreaIdentifier
            ) {
                return (string)$designArea->getIdentifier() === (string)$designAreaIdentifier;
            });

            if (false === $filteredAreas->first()) {
                continue;
            }

            $designProductionMethodIdentifier = $designAreaDesignData->designProductionMethodIdentifier;
            $filteredAreaProductionMethods = $filteredAreas->first()->getDesignAreaDesignProductionMethod()
                ->filter(static function (DesignAreaDesignProductionMethod $designAreaDesignProductionMethod) use (
                    $designProductionMethodIdentifier
                ) {
                    return null !== $designAreaDesignProductionMethod->getDesignProductionMethod() &&
                        $designAreaDesignProductionMethod->getDesignProductionMethod()->getIdentifier() ===
                        $designProductionMethodIdentifier;
                });

            $currentAreaProductionMethodEntity = $filteredAreaProductionMethods->first();
            if (false === $currentAreaProductionMethodEntity) {
                throw new NotFoundException();
            }

            $aggregate = [];
            $designAreaObjectImages = [];
            $usedColors = [];

            foreach ($designAreaDesignData->canvasData->objects as $objectIndex => $object) {
                $type = lcfirst($object->type);
                $aggregate[$type]['count'] = !isset($aggregate[$type]['count']) ? 1 : $aggregate[$type]['count'] + 1;

                $designAreaObjectImages[$objectIndex] = $this->getDesignAreaObjectImagePath(
                    $frontendConfiguration->code,
                    $designAreaIdentifier,
                    $objectIndex
                );

                if ('Text' === $object->type && !empty($object->style->colors)) {
                    $usedColors[$objectIndex] = $this->getUsedColors($object->style->colors);
                }
                if ('Image' === $object->type && isset($object->imageData->operations->vectorizeColorsMap)) {
                    $usedColors[$objectIndex] = $this->getUsedColors($object->imageData->operations->vectorizeColorsMap);
                }
            }

            $editedDesignAreas[] = $this->createEditedDesignArea(
                $currentAreaProductionMethodEntity,
                $designAreaDesignData,
                $usedColors,
                $designAreaObjectImages,
                $aggregate
            );
        }

        return $editedDesignAreas;
    }

    private function createEditedDesignArea(
        DesignAreaDesignProductionMethod $designAreaDesignProductionMethodEntity,
        $designAreaDesignData,
        array $usedColors,
        array $designAreaObjectImages,
        array $aggregate
    ): EditedDesignArea {
        /** @var DesignProductionMethod $designProductionMethodEntity */
        $designProductionMethodEntity = $designAreaDesignProductionMethodEntity->getDesignProductionMethod();
        /** @var DesignArea $designAreaEntity */
        $designAreaEntity = $designAreaDesignProductionMethodEntity->getDesignArea();

        $editedDesignArea = new EditedDesignArea();
        $editedDesignArea->title = $designAreaEntity->getTranslatedTitle();
        $editedDesignArea->designProductionMethodTitle = $designProductionMethodEntity->getTranslatedTitle();
        $editedDesignArea->designProductionMethodMaxColorAmount = $designProductionMethodEntity->getOptions()->maxColorAmount ?? 0;
        $editedDesignArea->colorAmount = $designAreaDesignData->colorAmount ?? 0;

        $editedDesignArea->colors = $usedColors;
        $editedDesignArea->images = $designAreaObjectImages;
        $editedDesignArea->types = $aggregate;

        $editedDesignArea->width = $designAreaDesignProductionMethodEntity->getWidth() > 0
            ? $designAreaDesignProductionMethodEntity->getWidth()
            : $designAreaEntity->getWidth();
        $editedDesignArea->height = $designAreaDesignProductionMethodEntity->getHeight() > 0
            ? $designAreaDesignProductionMethodEntity->getHeight()
            : $designAreaEntity->getHeight();
        $editedDesignArea->editedDesignAreaElements = $designAreaDesignData->canvasData->objects ?? [];

        return $editedDesignArea;
    }

    /**
     * @throws NonUniqueResultException
     */
    private function getUsedColors($colors): array
    {
        $lookUpColors = [];
        $mappedColors = [];
        foreach ($colors as $color) {
            $colorInfo = json_decode($color->identifier, false);

            if (isset($colorInfo->custom)) {
                $palette = 'customColor';
            } elseif (isset($colorInfo->palette)) {
                $palette = $colorInfo->palette;
            } else {
                continue;
            }

            if (isset($colorInfo->color)) {
                $mappedColors[$palette][$colorInfo->color] = $colorInfo;
                $lookUpColors[] = $colorInfo->color;
            }
        }

        $colorsByPalette = $this->colorRepository->findColorsInAllPalettes($lookUpColors);

        $usedColors = [];
        foreach ($mappedColors as $paletteIdentifier => $palette) {
            foreach ($palette as $colorIdentifier => $color) {
                $paletteTitle = $paletteIdentifier;
                $colorTitle = $colorIdentifier;

                if (isset($colorsByPalette[$paletteIdentifier][$colorIdentifier])) {
                    $colorEntity = $colorsByPalette[$paletteIdentifier][$colorIdentifier];
                    $paletteTitle = $colorEntity->getColorPalette()->getTranslatedTitle();
                    $colorTitle = $colorEntity->getTranslatedTitle();
                }

                $usedColors[] = [
                    'colorPalette' => $paletteTitle,
                    'color' => $colorTitle,
                    'isCustom' => 'customColor' === $paletteIdentifier,
                ];
            }
        }

        return $usedColors;
    }

    private function getDesignAreaObjectImagePath(
        string $configurationCode,
        string $designAreaIdentifier,
        string $objectIndex
    ): string {
        return $this->configurationImagePaths->getConfigurationImagePath() . '/'
            . $configurationCode . '/designAreas/'
            . $designAreaIdentifier . '_' . $objectIndex . '.svg';
    }

    private function createPreview(FrontendConfigurationStructure $frontendConfiguration): Preview
    {
        $configurationImagePath = $this->configurationImagePaths->getConfigurationImagePath() . DIRECTORY_SEPARATOR . $frontendConfiguration->code;
        $configurationImages = glob($configurationImagePath . '/*/*.png');

        if (!$configurationImages) {
            // this case is for backwards compatibility
            $configurationImages = glob($configurationImagePath . '/*.png');
        }

        $minimumAmountOfImages = 1;
        if (false !== strpos($this->determineConfigurationMode($frontendConfiguration), Item::CONFIGURATION_MODE_CREATOR)) {
            $minimumAmountOfImages = 2;
        }

        $preview = new Preview();
        $preview->title = $frontendConfiguration->item->title ?? '';
        $preview->isVisible = count($configurationImages) >= $minimumAmountOfImages;
        $preview->images = $configurationImages;

        return $preview;
    }

    private function getItemImagePath(string $itemIdentifier, string $default = ''): string
    {
        $path = $this->itemImagePaths->getItemDetailImagePath() . DIRECTORY_SEPARATOR . $itemIdentifier . '.jpg';
        if (!file_exists($path)) {
            $path = $this->itemImagePaths->getItemDetailImagePath() . DIRECTORY_SEPARATOR . $itemIdentifier . '.png';
        }

        return file_exists($path) ? $path : $default;
    }

    /**
     * @return array|ComponentInfo[]
     */
    private function createComponentData(FrontendConfigurationStructure $configuration): array
    {
        /* @var $optionClassification OptionClassification */
        /* @var $selectedOption Option */

        $componentInfoArray = [];
        foreach ((array)$configuration->optionclassifications as $optionClassification) {
            if ($this->hideComponentInPdf($optionClassification)) {
                continue;
            }
            $componentInfo = new ComponentInfo();
            $componentInfo->component = $optionClassification;
            $componentInfo->componentTitle = $optionClassification->title;
            $componentInfo->componentIdentifier = $optionClassification->identifier;
            $optionNamesArray = [];
            $optionIdentifiers = [];
            foreach ((array)$optionClassification->selectedoptions as $selectedOption) {
                $optionNamesArray[] =
                    $selectedOption->amount > 1 ? $selectedOption->amount . ' x ' . $selectedOption->title : $selectedOption->title;
                $optionIdentifiers[] = $selectedOption->identifier;
            }
            $nameList = implode(', ', $optionNamesArray);
            $componentInfo->optionTitle = $nameList;
            $componentInfo->optionIdentifier = implode(',', $optionIdentifiers);
            $componentInfoArray[] = $componentInfo;
        }

        return $componentInfoArray;
    }

    private function hideComponentInPdf(OptionClassification $component): bool
    {
        return empty($component->selectedoptions) || true === $component->hiddenInFrontend || true === $component->hiddenInPdf;
    }

    /**
     * bridge to convert the format of configurationModes in FrontendConfigurationStructure back to how its saved to the database:
     * - creator
     * - designer
     * - creator+designer.
     */
    private function determineConfigurationMode(FrontendConfigurationStructure $frontendConfiguration): string
    {
        $configurationModeAsString = '';

        foreach ($frontendConfiguration->item->configurationModes as $configurationMode) {
            if (isset($configurationMode->itemStatus->item_available) && true === $configurationMode->itemStatus->item_available) {
                if ('' !== $configurationModeAsString) {
                    $configurationModeAsString .= '+';
                }

                $configurationModeAsString .= $configurationMode->identifier;
            }
        }

        return $configurationModeAsString;
    }
}
