<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage as ImageGalleryImageEntity;
use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageTag;
use Redhotmagma\ConfiguratorApiBundle\Repository\ChannelRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\TagRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImagePrice;

/**
 * @internal
 */
class ImageGalleryImageEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    private EntityFromStructureConverter $entityFromStructureConverter;

    private TagRepository $tagRepository;

    private ChannelRepository $channelRepository;

    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        TagRepository $tagRepository,
        ChannelRepository $channelRepository
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->tagRepository = $tagRepository;
        $this->channelRepository = $channelRepository;
    }

    /**
     * @param ImageGalleryImage $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $entity = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($localStructure, $entity, 'texts',
            'ImageGalleryImageText');
        $entity = $this->entityFromStructureConverter->addNewManyToOneRelations($localStructure, $entity, 'texts',
            'ImageGalleryImageText');

        // update ImageGalleryImageTag relations
        $entity = $this->entityFromStructureConverter->setManyToManyRelationsDeleted($localStructure, $entity,
            'ImageGalleryImageTag', 'tags', 'Tag');
        $entity = $this->entityFromStructureConverter->addNewManyToManyRelations($localStructure, $entity,
            ImageGalleryImageTag::class, 'tags', 'Tag',
            $this->tagRepository);

        $entity = $this->updateGalleryImagePrices($entity, $structure);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ImageGalleryImage::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    protected function updateGalleryImagePrices(ImageGalleryImageEntity $galleryImage, ImageGalleryImage $structure): ImageGalleryImageEntity
    {
        /** @var ImageGalleryImagePrice $priceStructure */
        foreach ($structure->prices as $i => $priceStructure) {
            $structure->prices[$i]->channel = $this->channelRepository->find($priceStructure->channel);
        }

        $galleryImage = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $galleryImage, 'prices',
            'ImageGalleryImagePrice');
        $galleryImage = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $galleryImage, 'prices',
            'ImageGalleryImagePrice');

        return $galleryImage;
    }
}
