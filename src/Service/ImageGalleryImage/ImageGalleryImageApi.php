<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageUploadData;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\SequenceNumberArguments;

class ImageGalleryImageApi
{
    /**
     * @var ImageGalleryImageRepository
     */
    private $imageGalleryImageRepository;

    /**
     * @var ImageGalleryImageStructureFromEntityConverter
     */
    private $imageGalleryImageStructureFromEntityConverter;

    /**
     * @var ImageGalleryImageSave
     */
    private $imageGalleryImageSave;

    /**
     * @var ImageGalleryImageDelete
     */
    private $imageGalleryImageDelete;

    /**
     * @var ImageGalleryImageUpload
     */
    private $imageGalleryImageUpload;

    /**
     * @var ImageGalleryImageFrontendList
     */
    private $imageGalleryImageFrontendList;

    /**
     * ImageGalleryImageApi constructor.
     *
     * @param ImageGalleryImageRepository $imageGalleryImageRepository
     * @param ImageGalleryImageStructureFromEntityConverter $imageGalleryImageStructureFromEntityConverter
     * @param ImageGalleryImageSave $imageGalleryImageSave
     * @param ImageGalleryImageDelete $imageGalleryImageDelete
     * @param ImageGalleryImageUpload $imageGalleryImageUpload
     * @param ImageGalleryImageFrontendList $imageGalleryImageFrontendList
     */
    public function __construct(
        ImageGalleryImageRepository $imageGalleryImageRepository,
        ImageGalleryImageStructureFromEntityConverter $imageGalleryImageStructureFromEntityConverter,
        ImageGalleryImageSave $imageGalleryImageSave,
        ImageGalleryImageDelete $imageGalleryImageDelete,
        ImageGalleryImageUpload $imageGalleryImageUpload,
        ImageGalleryImageFrontendList $imageGalleryImageFrontendList
    ) {
        $this->imageGalleryImageRepository = $imageGalleryImageRepository;
        $this->imageGalleryImageStructureFromEntityConverter = $imageGalleryImageStructureFromEntityConverter;
        $this->imageGalleryImageSave = $imageGalleryImageSave;
        $this->imageGalleryImageDelete = $imageGalleryImageDelete;
        $this->imageGalleryImageUpload = $imageGalleryImageUpload;
        $this->imageGalleryImageFrontendList = $imageGalleryImageFrontendList;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->imageGalleryImageRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->imageGalleryImageStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->imageGalleryImageRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage
    {
        /** @var ImageGalleryImage $entity */
        $entity = $this->imageGalleryImageRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->imageGalleryImageStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage $imageGalleryImage
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage $imageGalleryImage)
    {
        $structure = $this->imageGalleryImageSave->save($imageGalleryImage);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->imageGalleryImageDelete->delete($id);
    }

    /**
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage
     */
    public function upload(ImageGalleryImageUploadData $imageGalleryImageUploadData)
    {
        $imageGalleryImage = $this->imageGalleryImageUpload->upload($imageGalleryImageUploadData);

        return $imageGalleryImage;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageFrontendList $imageGalleryImageFrontendList
     *
     * @return array
     */
    public function frontendList(
        \Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageFrontendList $imageGalleryImageFrontendList
    ) {
        $imageGalleryImageStructures = $this->imageGalleryImageFrontendList->frontendList($imageGalleryImageFrontendList);

        return $imageGalleryImageStructures;
    }

    /**
     * @return array
     */
    public function frontendListTags()
    {
        $tags = $this->imageGalleryImageFrontendList->listTags();

        return $tags;
    }

    /**
     * @param SequenceNumberArguments $sequenceNumberArguments
     */
    public function saveSequenceNumbers(SequenceNumberArguments $sequenceNumberArguments): void
    {
        foreach ($sequenceNumberArguments->data as $sequenceNumberData) {
            $entity = $this->imageGalleryImageRepository->findOneBy(['id' => $sequenceNumberData->id]);
            if ($entity) {
                $entity->setSequenceNumber($sequenceNumberData->sequencenumber);
                $this->imageGalleryImageRepository->save($entity, false);
            }
        }

        $this->imageGalleryImageRepository->flush();
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['imageGalleryImageText.title'];
    }
}
