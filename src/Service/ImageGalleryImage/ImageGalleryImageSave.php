<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;

/**
 * @internal
 */
class ImageGalleryImageSave
{
    /**
     * @var ImageGalleryImageRepository
     */
    private $imageGalleryImageRepository;

    /**
     * @var ImageGalleryImageEntityFromStructureConverter
     */
    private $imageGalleryImageEntityFromStructureConverter;

    /**
     * @var ImageGalleryImageStructureFromEntityConverter
     */
    private $imageGalleryImageStructureFromEntityConverter;

    /**
     * @param ImageGalleryImageRepository $optionPoolRepository
     * @param ImageGalleryImageEntityFromStructureConverter $optionPoolEntityFromStructureConverter
     * @param ImageGalleryImageStructureFromEntityConverter $optionPoolStructureFromEntityConverter
     */
    public function __construct(
        ImageGalleryImageRepository $optionPoolRepository,
        ImageGalleryImageEntityFromStructureConverter $optionPoolEntityFromStructureConverter,
        ImageGalleryImageStructureFromEntityConverter $optionPoolStructureFromEntityConverter
    ) {
        $this->imageGalleryImageRepository = $optionPoolRepository;
        $this->imageGalleryImageEntityFromStructureConverter = $optionPoolEntityFromStructureConverter;
        $this->imageGalleryImageStructureFromEntityConverter = $optionPoolStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage $imageGalleryImage
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage $imageGalleryImage)
    {
        $entity = null;
        if (isset($imageGalleryImage->id) && $imageGalleryImage->id > 0) {
            $entity = $this->imageGalleryImageRepository->findOneBy(['id' => $imageGalleryImage->id]);
        }
        $entity = $this->imageGalleryImageEntityFromStructureConverter->convertOne($imageGalleryImage, $entity);
        $entity = $this->imageGalleryImageRepository->save($entity);

        /** @var ImageGalleryImage $entity */
        $entity = $this->imageGalleryImageRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->imageGalleryImageStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
