<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage as ImageGalleryImageEntity;
use Redhotmagma\ConfiguratorApiBundle\Service\Tag\TagStructureFromEntityConverter;
use Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImagePrice;
use Redhotmagma\ConfiguratorApiBundle\Structure\ImageGalleryImageText;
use Redhotmagma\ConfiguratorApiBundle\Structure\Tag;

/**
 * @internal
 */
class ImageGalleryImageStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    /**
     * @var TagStructureFromEntityConverter
     */
    private $tagStructureFromEntityConverter;

    /**
     * @var array
     */
    private $imageGalleryConfig;

    /**
     * ImageGalleryImageStructureFromEntityConverter constructor.
     *
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param TagStructureFromEntityConverter $tagStructureFromEntityConverter
     * @param array $imageGalleryConfig
     */
    public function __construct(
        StructureFromEntityConverter $structureFromEntityConverter,
        TagStructureFromEntityConverter $tagStructureFromEntityConverter,
        array $imageGalleryConfig
    ) {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->tagStructureFromEntityConverter = $tagStructureFromEntityConverter;
        $this->imageGalleryConfig = $imageGalleryConfig;
    }

    /**
     * @param ImageGalleryImageEntity $entity
     * @param string $structureClassName
     *
     * @return ImageGalleryImage
     */
    public function convertOne($entity, $structureClassName = ImageGalleryImage::class)
    {
        /** @var ImageGalleryImage $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure = $this->addImageGalleryImageTextsToStructure($structure, $entity);
        $structure = $this->addImageGalleryImageTagsToStructure($structure, $entity);
        $structure = $this->addImageGalleryImagePricesToStructure($structure, $entity);

        $structure->imageUrl = $this->imageGalleryConfig['url_path'] . '/' . $entity->getImagename();
        $structure->thumbUrl = $this->imageGalleryConfig['url_path'] . '/' . $entity->getThumbname();
        $structure->printUrl = $this->imageGalleryConfig['url_path'] . '/' . $entity->getPrintname();

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = ImageGalleryImage::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param ImageGalleryImage $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage $entity
     *
     * @return ImageGalleryImage
     */
    protected function addImageGalleryImageTextsToStructure($structure, $entity)
    {
        $texts = [];

        $imageGalleryImageTexts = $entity->getImageGalleryImageText();

        if (!empty($imageGalleryImageTexts)) {
            /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageText $imageGalleryImageText */
            foreach ($imageGalleryImageTexts as $imageGalleryImageText) {
                $imageGalleryImageTextStructure = $this->structureFromEntityConverter->convertOne($imageGalleryImageText,
                    ImageGalleryImageText::class);

                if (!empty($imageGalleryImageTextStructure)) {
                    $imageGalleryImageTextStructure->language = $imageGalleryImageText->getLanguage()
                        ->getIso();
                    $texts[] = $imageGalleryImageTextStructure;
                }
            }
        }

        $structure->texts = $texts;

        return $structure;
    }

    /**
     * @param ImageGalleryImage $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage $entity
     *
     * @return ImageGalleryImage
     */
    protected function addImageGalleryImageTagsToStructure($structure, $entity)
    {
        $tags = [];

        $imageGalleryImageTags = $entity->getImageGalleryImageTag();

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImageTag $imageGalleryImageTag */
        foreach ($imageGalleryImageTags as $imageGalleryImageTag) {
            $imageGalleryImageTagStructure = $this->tagStructureFromEntityConverter->convertOne($imageGalleryImageTag->getTag(),
                Tag::class);

            $tags[] = $imageGalleryImageTagStructure;
        }

        $structure->tags = $tags;

        return $structure;
    }

    public function addImageGalleryImagePricesToStructure(ImageGalleryImage $structure, ImageGalleryImageEntity $entity): ImageGalleryImage
    {
        $prices = [];

        $imagePrices = $entity->getImageGalleryImagePrice();
        foreach ($imagePrices as $imagePrice) {
            $priceStructure = new ImageGalleryImagePrice();
            $priceStructure->id = $imagePrice->getId();
            $priceStructure->price = $imagePrice->getPrice();
            $priceStructure->priceNet = $imagePrice->getPriceNet();
            $priceStructure->channel = $imagePrice->getChannel()->getId();

            $prices[] = $priceStructure;
        }

        $structure->prices = $prices;

        return $structure;
    }
}
