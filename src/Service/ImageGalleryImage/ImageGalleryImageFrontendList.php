<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageTagRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Tag\FrontendTagStructureFromEntityConverter;

/**
 * @internal
 */
class ImageGalleryImageFrontendList
{
    /**
     * @var FrontendImageGalleryImageStructureFromEntityConverter
     */
    private $frontendStructureFromEntityConverter;

    /**
     * @var FrontendTagStructureFromEntityConverter
     */
    private $frontendTagStructureFromEntityConverter;

    /**
     * @var ImageGalleryImageRepository
     */
    private $imageGalleryImageRepository;

    /**
     * @var ImageGalleryImageTagRepository
     */
    private $imageGalleryImageTagRepository;

    /**
     * ImageGalleryImageFrontendList constructor.
     *
     * @param FrontendImageGalleryImageStructureFromEntityConverter $frontendStructureFromEntityConverter
     * @param FrontendTagStructureFromEntityConverter $frontendTagStructureFromEntityConverter
     * @param ImageGalleryImageRepository $imageGalleryImageRepository
     * @param ImageGalleryImageTagRepository $imageGalleryImageTagRepository
     */
    public function __construct(
        FrontendImageGalleryImageStructureFromEntityConverter $frontendStructureFromEntityConverter,
        FrontendTagStructureFromEntityConverter $frontendTagStructureFromEntityConverter,
        ImageGalleryImageRepository $imageGalleryImageRepository,
        ImageGalleryImageTagRepository $imageGalleryImageTagRepository
    ) {
        $this->frontendStructureFromEntityConverter = $frontendStructureFromEntityConverter;
        $this->frontendTagStructureFromEntityConverter = $frontendTagStructureFromEntityConverter;
        $this->imageGalleryImageRepository = $imageGalleryImageRepository;
        $this->imageGalleryImageTagRepository = $imageGalleryImageTagRepository;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageFrontendList $imageGalleryImageFrontendList
     *
     * @return array
     */
    public function frontendList(
        \Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageFrontendList $imageGalleryImageFrontendList
    ) {
        $imageGalleryImages = $this->imageGalleryImageRepository->searchByTagsAndTitle($imageGalleryImageFrontendList->title,
            $imageGalleryImageFrontendList->tags);

        $imageGalleryImageStructures = $this->frontendStructureFromEntityConverter->convertMany($imageGalleryImages);

        return $imageGalleryImageStructures;
    }

    /**
     * @return array
     */
    public function listTags()
    {
        $tags = $this->imageGalleryImageTagRepository->fetchImageGalleryTags();

        $tagStructures = $this->frontendTagStructureFromEntityConverter->convertMany($tags);

        return $tagStructures;
    }
}
