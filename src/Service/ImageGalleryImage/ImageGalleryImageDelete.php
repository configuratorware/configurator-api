<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;

/**
 * @internal
 */
class ImageGalleryImageDelete
{
    /**
     * @var ImageGalleryImageRepository
     */
    private $imageGalleryImageRepository;

    /**
     * ImageGalleryImageDelete constructor.
     *
     * @param ImageGalleryImageRepository $imageGalleryImageRepository
     */
    public function __construct(ImageGalleryImageRepository $imageGalleryImageRepository)
    {
        $this->imageGalleryImageRepository = $imageGalleryImageRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var ImageGalleryImage $entity */
            $entity = $this->imageGalleryImageRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            if (!empty($entity)) {
                $tagRelations = $entity->getImageGalleryImageTag();
                if (!empty($tagRelations)) {
                    foreach ($tagRelations as $tagRelation) {
                        $this->imageGalleryImageRepository->delete($tagRelation);
                    }
                }

                $textRelations = $entity->getImageGalleryImageText();
                if (!empty($textRelations)) {
                    foreach ($textRelations as $textRelation) {
                        $this->imageGalleryImageRepository->delete($textRelation);
                    }
                }
            }

            $this->imageGalleryImageRepository->delete($entity);
        }
    }
}
