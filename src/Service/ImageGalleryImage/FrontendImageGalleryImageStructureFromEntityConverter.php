<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage as ImageGalleryImageEntity;
use Redhotmagma\ConfiguratorApiBundle\Service\Calculation\VatCalculationService;
use Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage;

/**
 * @internal
 */
class FrontendImageGalleryImageStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    private StructureFromEntityConverter $structureFromEntityConverter;

    private VatCalculationService $vatCalculationService;

    /**
     * @var array<string>
     */
    private array $imageGalleryConfig;

    /**
     * @param StructureFromEntityConverter $structureFromEntityConverter
     * @param array $imageGalleryConfig
     * @param VatCalculationService $vatCalculationService
     */
    public function __construct(StructureFromEntityConverter $structureFromEntityConverter, array $imageGalleryConfig, VatCalculationService $vatCalculationService)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
        $this->imageGalleryConfig = $imageGalleryConfig;
        $this->vatCalculationService = $vatCalculationService;
    }

    /**
     * @param ImageGalleryImageEntity $entity
     * @param string $structureClassName
     *
     * @return ImageGalleryImage
     */
    public function convertOne($entity, $structureClassName = ImageGalleryImage::class): ImageGalleryImage
    {
        /** @var ImageGalleryImage $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $structure->title = $entity->getTranslatedTitle();
        $structure->description = $entity->getTranslatedDescription();

        $structure->imageUrl = $this->imageGalleryConfig['url_path'] . '/' . $entity->getImagename();
        $structure->thumbUrl = $this->imageGalleryConfig['url_path'] . '/' . $entity->getThumbname();
        $structure->printUrl = $this->imageGalleryConfig['url_path'] . '/' . $entity->getPrintname();

        $this->addPriceToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array<ImageGalleryImageEntity> $entities
     * @param string $structureclassname
     *
     * @return array
     */
    public function convertMany($entities, $structureclassname = ImageGalleryImage::class): array
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureclassname);
            $structures[] = $structure;
        }

        return $structures;
    }

    protected function addPriceToStructure(ImageGalleryImage $structure, ImageGalleryImageEntity $entity): ImageGalleryImage
    {
        $priceEntity = $entity->getCurrentChannelImagePrice();
        if ($priceEntity) {
            $price = $priceEntity->getPrice();
            $priceNet = $priceEntity->getPriceNet();
            $this->vatCalculationService->addPricesToStructure($structure, $price, $priceNet);
        }

        return $structure;
    }
}
