<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\ImageGalleryImage;

use Redhotmagma\ConfiguratorApiBundle\Repository\ImageGalleryImageRepository;
use Redhotmagma\ConfiguratorApiBundle\Service\Base\ImageConverterService;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ImageGalleryImageUploadData;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @internal
 */
class ImageGalleryImageUpload
{
    /**
     * @var FrontendImageGalleryImageStructureFromEntityConverter
     */
    private $frontendStructureFromEntityConverter;

    /**
     * @var ImageGalleryImageRepository
     */
    private $imageGalleryImageRepository;

    /**
     * @var ImageConverterService
     */
    private $imageConverterService;

    /**
     * @var array
     */
    private $imageGalleryConfig;

    /**
     * ImageGalleryImageUpload constructor.
     *
     * @param FrontendImageGalleryImageStructureFromEntityConverter $frontendStructureFromEntityConverter
     * @param ImageGalleryImageRepository $imageGalleryImageRepository
     * @param ImageConverterService $imageConverterService
     * @param array $imageGalleryConfig
     */
    public function __construct(
        FrontendImageGalleryImageStructureFromEntityConverter $frontendStructureFromEntityConverter,
        ImageGalleryImageRepository $imageGalleryImageRepository,
        ImageConverterService $imageConverterService,
        array $imageGalleryConfig
    ) {
        $this->frontendStructureFromEntityConverter = $frontendStructureFromEntityConverter;
        $this->imageGalleryImageRepository = $imageGalleryImageRepository;
        $this->imageConverterService = $imageConverterService;
        $this->imageGalleryConfig = $imageGalleryConfig;
    }

    /**
     * @param ImageGalleryImageUploadData $imageGalleryImageUploadData
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\Frontend\ImageGalleryImage
     */
    public function upload(ImageGalleryImageUploadData $imageGalleryImageUploadData)
    {
        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\ImageGalleryImage $imageGalleryImage */
        $imageGalleryImage = $this->imageGalleryImageRepository->findOneBy(['id' => $imageGalleryImageUploadData->imageGalleryImageId]);

        $imageBaseName = md5($imageGalleryImage->getId());

        /** @var UploadedFile $galleryImageFormData */
        $galleryImageFormData = $imageGalleryImageUploadData->galleryImageFormData;
        if (!empty($galleryImageFormData)) {
            $imageGalleryImageFileName = $imageBaseName . '.' . $galleryImageFormData->guessExtension();
            $galleryImageFormData->move($this->getImagePath(), $imageGalleryImageFileName);

            // create thumb
            $imageNameThumb = $imageBaseName . '_t.png';
            $this->imageConverterService->contain(
                $this->getImagePath() . $imageGalleryImageFileName,
                $this->getImagePath() . $imageNameThumb,
                $this->imageGalleryConfig['thumb_size']['width'],
                $this->imageGalleryConfig['thumb_size']['height']
            );

            $imageGalleryImage->setThumbname($imageNameThumb);
            $imageGalleryImage->setImagename($imageGalleryImageFileName);
        }

        /** @var UploadedFile $printImageFormData */
        $printImageFormData = $imageGalleryImageUploadData->printImageFormData;
        if (!empty($printImageFormData)) {
            $printFileName = $imageBaseName . '_p.' . $printImageFormData->guessExtension();
            $printImageFormData->move($this->getImagePath(), $printFileName);

            $imageGalleryImage->setPrintname($printFileName);
        } else {
            // if there is no print image fallback to image gallery image
            if (empty($imageGalleryImage->getPrintname()) && !empty($imageGalleryImage->getImagename())) {
                $imageGalleryImage->setPrintname($imageGalleryImage->getImagename());
            }
        }

        $this->imageGalleryImageRepository->save($imageGalleryImage);

        $imageGalleryImageStructure = $this->frontendStructureFromEntityConverter->convertOne($imageGalleryImage);

        return $imageGalleryImageStructure;
    }

    /**
     * @return string
     */
    protected function getImagePath()
    {
        if (!is_dir($this->imageGalleryConfig['filesystem_path'])) {
            mkdir($this->imageGalleryConfig['filesystem_path']);
        }

        return realpath($this->imageGalleryConfig['filesystem_path']) . '/';
    }
}
