<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionPool;

use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverter;
use Redhotmagma\ApiBundle\Service\Converter\StructureFromEntityConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool as OptionPoolEntity;
use Redhotmagma\ConfiguratorApiBundle\Structure\Option;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionPoolText;

/**
 * @internal
 */
class OptionPoolStructureFromEntityConverter implements StructureFromEntityConverterInterface
{
    /**
     * @var StructureFromEntityConverter
     */
    private $structureFromEntityConverter;

    public function __construct(StructureFromEntityConverter $structureFromEntityConverter)
    {
        $this->structureFromEntityConverter = $structureFromEntityConverter;
    }

    /**
     * @param OptionPoolEntity $entity
     * @param string $structureClassName
     *
     * @return OptionPool
     */
    public function convertOne($entity, $structureClassName = OptionPool::class)
    {
        /** @var OptionPool $structure */
        $structure = $this->structureFromEntityConverter->convertOne($entity, $structureClassName);

        $options = [];
        foreach ($entity->getOptionOptionPool() as $optionOptionPool) {
            $option = $this->structureFromEntityConverter->convertOne($optionOptionPool->getOption(), Option::class);

            $options[] = $option;
        }

        $structure->options = $options;

        $this->addOptionPoolTextsToStructure($structure, $entity);

        return $structure;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = OptionPool::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param OptionPool $structure
     * @param OptionPoolEntity $entity
     *
     * @return OptionPool
     */
    protected function addOptionPoolTextsToStructure(OptionPool $structure, OptionPoolEntity $entity): OptionPool
    {
        $textStructucres = [];

        $textEntities = $entity->getOptionPoolText();

        foreach ($textEntities as $textEntity) {
            $optionPoolTextStructure = $this->structureFromEntityConverter->convertOne($textEntity,
                OptionPoolText::class);

            if (!empty($optionPoolTextStructure)) {
                $optionPoolTextStructure->language = $textEntity->getLanguage()
                    ->getIso();
                $textStructucres[] = $optionPoolTextStructure;
            }
        }

        $structure->texts = $textStructucres;

        return $structure;
    }
}
