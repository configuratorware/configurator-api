<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionPool;

use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverter;
use Redhotmagma\ApiBundle\Service\Converter\EntityFromStructureConverterInterface;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionOptionPool;
use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool as OptionPoolEntity;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool;

/**
 * @internal
 */
class OptionPoolEntityFromStructureConverter implements EntityFromStructureConverterInterface
{
    /**
     * @var EntityFromStructureConverter
     */
    private $entityFromStructureConverter;

    /**
     * @var OptionRepository
     */
    private $optionRepository;

    public function __construct(
        EntityFromStructureConverter $entityFromStructureConverter,
        OptionRepository $optionRepository
    ) {
        $this->entityFromStructureConverter = $entityFromStructureConverter;
        $this->optionRepository = $optionRepository;
    }

    /**
     * @param OptionPool $structure
     * @param \Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool $entity
     * @param string $entityClassName
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool
     */
    public function convertOne(
        $structure,
        $entity = null,
        $entityClassName = \Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool::class
    ) {
        // clone the structure to avoid side reference effects
        $localStructure = clone $structure;

        /** @var \Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool $entity */
        $entity = $this->entityFromStructureConverter->convertOne($localStructure, $entity, $entityClassName);

        $this->entityFromStructureConverter->setManyToManyRelationsDeleted($localStructure, $entity, 'OptionOptionPool',
            'options', 'Option');

        $this->entityFromStructureConverter->addNewManyToManyRelations($localStructure, $entity,
            OptionOptionPool::class, 'options', 'Option',
            $this->optionRepository);

        $entity = $this->updateTexts($entity, $localStructure);

        return $entity;
    }

    /**
     * @param array $entities
     * @param string $structureClassName
     *
     * @return array
     */
    public function convertMany($entities, $structureClassName = OptionOptionPool::class)
    {
        $structures = [];

        foreach ($entities as $entity) {
            $structure = $this->convertOne($entity, $structureClassName);
            $structures[] = $structure;
        }

        return $structures;
    }

    /**
     * @param OptionPoolEntity $optionPool
     * @param OptionPool $structure
     *
     * @return OptionPoolEntity
     */
    protected function updateTexts(OptionPoolEntity $optionPool, OptionPool $structure): OptionPoolEntity
    {
        $optionPool = $this->entityFromStructureConverter->setManyToOneRelationsDeleted($structure, $optionPool, 'texts',
            'OptionPoolText');
        $optionPool = $this->entityFromStructureConverter->addNewManyToOneRelations($structure, $optionPool, 'texts',
            'OptionPoolText');

        return $optionPool;
    }
}
