<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionPool;

use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionPoolRepository;

/**
 * @internal
 */
class OptionPoolSave
{
    /**
     * @var OptionPoolRepository
     */
    private $optionPoolRepository;

    /**
     * @var OptionPoolEntityFromStructureConverter
     */
    private $optionPoolEntityFromStructureConverter;

    /**
     * @var OptionPoolStructureFromEntityConverter
     */
    private $optionPoolStructureFromEntityConverter;

    /**
     * @param OptionPoolRepository $optionPoolRepository
     * @param OptionPoolEntityFromStructureConverter $optionPoolEntityFromStructureConverter
     * @param OptionPoolStructureFromEntityConverter $optionPoolStructureFromEntityConverter
     */
    public function __construct(
        OptionPoolRepository $optionPoolRepository,
        OptionPoolEntityFromStructureConverter $optionPoolEntityFromStructureConverter,
        OptionPoolStructureFromEntityConverter $optionPoolStructureFromEntityConverter
    ) {
        $this->optionPoolRepository = $optionPoolRepository;
        $this->optionPoolEntityFromStructureConverter = $optionPoolEntityFromStructureConverter;
        $this->optionPoolStructureFromEntityConverter = $optionPoolStructureFromEntityConverter;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool $optionPool
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool $optionPool)
    {
        $entity = null;
        if (isset($optionPool->id) && $optionPool->id > 0) {
            $entity = $this->optionPoolRepository->findOneBy(['id' => $optionPool->id]);
        }
        $entity = $this->optionPoolEntityFromStructureConverter->convertOne($optionPool, $entity);
        $entity = $this->optionPoolRepository->save($entity);

        /** @var OptionPool $entity */
        $entity = $this->optionPoolRepository->findOneBy(['id' => $entity->getId()]);
        $structure = $this->optionPoolStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }
}
