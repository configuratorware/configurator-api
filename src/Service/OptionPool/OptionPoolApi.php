<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionPool;

use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionPoolRepository;
use Redhotmagma\ConfiguratorApiBundle\Structure\Internal\PaginationResult;
use Redhotmagma\ConfiguratorApiBundle\Structure\RequestArguments\ListRequestArguments;

class OptionPoolApi
{
    /**
     * @var OptionPoolDelete
     */
    private $optionPoolDelete;

    /**
     * @var OptionPoolRepository
     */
    private $optionPoolRepository;

    /**
     * @var OptionPoolSave
     */
    private $optionPoolSave;

    /**
     * @var OptionPoolEntityFromStructureConverter
     */
    private $optionPoolEntityFromStructureConverter;

    /**
     * @var OptionPoolStructureFromEntityConverter
     */
    private $optionPoolStructureFromEntityConverter;

    /**
     * @param OptionPoolDelete $optionPoolDelete
     * @param OptionPoolRepository $optionPoolRepository
     * @param OptionPoolSave $optionPoolSave
     * @param OptionPoolEntityFromStructureConverter $optionPoolEntityFromStructureConverter
     * @param OptionPoolStructureFromEntityConverter $optionPoolStructureFromEntityConverter
     */
    public function __construct(
        OptionPoolDelete $optionPoolDelete,
        OptionPoolRepository $optionPoolRepository,
        OptionPoolSave $optionPoolSave,
        OptionPoolEntityFromStructureConverter $optionPoolEntityFromStructureConverter,
        OptionPoolStructureFromEntityConverter $optionPoolStructureFromEntityConverter
    ) {
        $this->optionPoolDelete = $optionPoolDelete;
        $this->optionPoolRepository = $optionPoolRepository;
        $this->optionPoolSave = $optionPoolSave;
        $this->optionPoolEntityFromStructureConverter = $optionPoolEntityFromStructureConverter;
        $this->optionPoolStructureFromEntityConverter = $optionPoolStructureFromEntityConverter;
    }

    /**
     * @param ListRequestArguments $arguments
     *
     * @return PaginationResult
     */
    public function getListResult(ListRequestArguments $arguments): PaginationResult
    {
        $paginationResult = new PaginationResult();

        $entities = $this->optionPoolRepository->fetchList($arguments, $this->getSearchableFields());
        $paginationResult->data = $this->optionPoolStructureFromEntityConverter->convertMany($entities);

        $paginationResult->count = $this->optionPoolRepository->fetchListCount($arguments,
            $this->getSearchableFields());

        return $paginationResult;
    }

    /**
     * @param int $id
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool
     *
     * @throws NotFoundException
     */
    public function getOne(int $id): \Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool
    {
        /** @var OptionPool $entity */
        $entity = $this->optionPoolRepository->findOneBy(['id' => $id]);

        if (empty($entity)) {
            throw new NotFoundException();
        }

        $structure = $this->optionPoolStructureFromEntityConverter->convertOne($entity);

        return $structure;
    }

    /**
     * @param \Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool $optionPool
     *
     * @return \Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool
     */
    public function save(\Redhotmagma\ConfiguratorApiBundle\Structure\OptionPool $optionPool)
    {
        $structure = $this->optionPoolSave->save($optionPool);

        return $structure;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $this->optionPoolDelete->delete($id);
    }

    /**
     * a list af fields that are searchable with the query param.
     *
     * @return  array
     */
    protected function getSearchableFields()
    {
        return ['identifier'];
    }
}
