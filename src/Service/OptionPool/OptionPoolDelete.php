<?php

namespace Redhotmagma\ConfiguratorApiBundle\Service\OptionPool;

use Redhotmagma\ConfiguratorApiBundle\Entity\OptionPool;
use Redhotmagma\ConfiguratorApiBundle\Exception\NotFoundException;
use Redhotmagma\ConfiguratorApiBundle\Repository\OptionPoolRepository;

/**
 * @internal
 */
class OptionPoolDelete
{
    /**
     * @var OptionPoolRepository
     */
    private $optionPoolRepository;

    /**
     * @param OptionPoolRepository $optionPoolRepository
     */
    public function __construct(
        OptionPoolRepository $optionPoolRepository
    ) {
        $this->optionPoolRepository = $optionPoolRepository;
    }

    /**
     * @param string $id
     */
    public function delete($id)
    {
        $ids = explode(',', $id);
        foreach ($ids as $id) {
            /** @var OptionPool $entity */
            $entity = $this->optionPoolRepository->findOneBy(['id' => $id]);

            if (empty($entity)) {
                throw new NotFoundException();
            }

            foreach ($entity->getOptionOptionPool() as $optionOptionPool) {
                $this->optionPoolRepository->delete($optionOptionPool);
            }

            $this->optionPoolRepository->delete($entity);
        }
    }
}
