<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Doctrine;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\Migrations\Version\MigrationFactory;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MigrationFactoryDecorator implements MigrationFactory
{
    /**
     * @var MigrationFactory
     */
    private $migrationFactory;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * MigrationFactoryDecorator constructor.
     *
     * @param MigrationFactory $migrationFactory
     * @param ContainerInterface $container
     */
    public function __construct(MigrationFactory $migrationFactory, ContainerInterface $container)
    {
        $this->migrationFactory = $migrationFactory;
        $this->container = $container;
    }

    /**
     * @param string $migrationClassName
     *
     * @return AbstractMigration
     */
    public function createVersion(string $migrationClassName): AbstractMigration
    {
        $instance = $this->migrationFactory->createVersion($migrationClassName);

        if ($instance instanceof ContainerAwareInterface) {
            $instance->setContainer($this->container);
        }

        return $instance;
    }
}
