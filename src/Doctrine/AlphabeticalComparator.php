<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Doctrine;

use Doctrine\Migrations\Version\Comparator;
use Doctrine\Migrations\Version\Version;

/**
 * @internal
 */
class AlphabeticalComparator implements Comparator
{
    public function compare(Version $a, Version $b): int
    {
        return strcmp($this->removeNameSpace((string)$a), $this->removeNameSpace((string)$b));
    }

    private function removeNameSpace(string $namespaced): string
    {
        $parts = explode('\\', $namespaced);

        if ([] === $parts) {
            return '';
        }

        return end($parts);
    }
}
