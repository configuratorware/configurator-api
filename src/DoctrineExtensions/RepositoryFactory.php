<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\DoctrineExtensions;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

/**
 * Factory for creating Doctrine Repositories that require extra services to be injected.
 *
 * @internal
 */
class RepositoryFactory
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * RepositoryFactory constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates repositories that can be used to split functionality between API and customer.
     *
     * @param class-string $entityClass The entity class
     * @param string $repositoryClass The repository class of the entity
     *
     * @return ObjectRepository The created $repositoryClass
     */
    public function createRepository($entityClass, $repositoryClass): ObjectRepository
    {
        $classMetadata = $this->entityManager->getClassMetadata($entityClass);

        /** @var ObjectRepository $repository */
        $repository = new $repositoryClass($this->entityManager, $classMetadata);

        return $repository;
    }
}
