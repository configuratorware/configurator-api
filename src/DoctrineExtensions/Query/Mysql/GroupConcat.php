<?php

namespace Redhotmagma\ConfiguratorApiBundle\DoctrineExtensions\Query\Mysql;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Usage: GROUP_CONCAT(field)
 * Returns: distance in km.
 *
 * @internal
 */
class GroupConcat extends FunctionNode
{
    protected $fieldName;

    public function getSql(SqlWalker $sqlWalker)
    {
        return sprintf(
            'GROUP_CONCAT(%s)',
            $sqlWalker->walkArithmeticPrimary($this->fieldName)
        );
    }

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->fieldName = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
