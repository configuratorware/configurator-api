<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201007124356 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE `item` SET `configuration_mode` = "creator" WHERE `item`.`parent_id` IS NULL AND `item`.`id` IN (SELECT `item_configuration_mode_item_status`.`item_id` FROM `item_configuration_mode_item_status` JOIN `configuration_mode` ON `item_configuration_mode_item_status`.`configuration_mode_id` = `configuration_mode`.`id` JOIN `item_status` ON `item_configuration_mode_item_status`.`item_status_id` = `item_status`.`id` WHERE `item_status`.`item_available` = TRUE AND `configuration_mode`.`identifier` = "creator") AND `item`.`id` NOT IN (SELECT `item_configuration_mode_item_status`.`item_id` FROM `item_configuration_mode_item_status` JOIN `configuration_mode` ON `item_configuration_mode_item_status`.`configuration_mode_id` = `configuration_mode`.`id` JOIN `item_status` ON `item_configuration_mode_item_status`.`item_status_id` = `item_status`.`id` WHERE `item_status`.`item_available` = TRUE AND `configuration_mode`.`identifier` = "designer")');
        $this->addSql('UPDATE `item` SET `configuration_mode` = "designer" WHERE `item`.`parent_id` IS NULL AND `item`.`id` IN (SELECT `item_configuration_mode_item_status`.`item_id` FROM `item_configuration_mode_item_status` JOIN `configuration_mode` ON `item_configuration_mode_item_status`.`configuration_mode_id` = `configuration_mode`.`id` JOIN `item_status` ON `item_configuration_mode_item_status`.`item_status_id` = `item_status`.`id` WHERE `item_status`.`item_available` = TRUE AND `configuration_mode`.`identifier` = "designer") AND `item`.`id` NOT IN (SELECT `item_configuration_mode_item_status`.`item_id` FROM `item_configuration_mode_item_status` JOIN `configuration_mode` ON `item_configuration_mode_item_status`.`configuration_mode_id` = `configuration_mode`.`id` JOIN `item_status` ON `item_configuration_mode_item_status`.`item_status_id` = `item_status`.`id` WHERE `item_status`.`item_available` = TRUE AND `configuration_mode`.`identifier` = "creator")');
        $this->addSql('UPDATE `item` SET `configuration_mode` = "creator+designer" WHERE `item`.`parent_id` IS NULL AND `item`.`id` IN (SELECT `item_configuration_mode_item_status`.`item_id` FROM `item_configuration_mode_item_status` JOIN `configuration_mode` ON `item_configuration_mode_item_status`.`configuration_mode_id` = `configuration_mode`.`id` JOIN `item_status` ON `item_configuration_mode_item_status`.`item_status_id` = `item_status`.`id` WHERE `item_status`.`item_available` = TRUE AND `configuration_mode`.`identifier` = "creator") AND `item`.`id` IN (SELECT `item_configuration_mode_item_status`.`item_id` FROM `item_configuration_mode_item_status` JOIN `configuration_mode` ON `item_configuration_mode_item_status`.`configuration_mode_id` = `configuration_mode`.`id` JOIN `item_status` ON `item_configuration_mode_item_status`.`item_status_id` = `item_status`.`id` WHERE `item_status`.`item_available` = TRUE AND `configuration_mode`.`identifier` = "designer")');

        $this->addSql('UPDATE `item` SET `item_status_id` = (SELECT id FROM `item_status` WHERE `item_available` = TRUE LIMIT 1) WHERE `item`.`parent_id` IS NULL AND `item`.`id` IN (SELECT `item_configuration_mode_item_status`.`item_id` FROM `item_configuration_mode_item_status` JOIN `configuration_mode` ON `item_configuration_mode_item_status`.`configuration_mode_id` = `configuration_mode`.`id` JOIN `item_status` ON `item_configuration_mode_item_status`.`item_status_id` = `item_status`.`id` WHERE `item_status`.`item_available` = TRUE)');

        $this->addSql('UPDATE `item` SET `visualization_mode_id` = CASE WHEN (SELECT `visualizationcomponent` FROM `setting`) = "layer" THEN 1 WHEN (SELECT `visualizationcomponent` FROM `setting`) = "2d" THEN 2 WHEN (SELECT `visualizationcomponent` FROM `setting`) = "3d" THEN 4 END WHERE `item`.`parent_id` IS NULL');
        $this->addSql('UPDATE `item` SET `visualization_mode_id` = (SELECT id FROM `visualization_mode` WHERE `identifier` = "2dLayer") WHERE `item`.`parent_id` IS NULL AND `item`.`id` IN (SELECT `item_id` FROM `itemsetting` WHERE `visualization_designer` = "layer")');
        $this->addSql('UPDATE `item` SET `visualization_mode_id` = (SELECT id FROM `visualization_mode` WHERE `identifier` = "2dVariant") WHERE `item`.`parent_id` IS NULL AND `item`.`id` IN (SELECT `item_id` FROM `itemsetting` WHERE `visualization_designer` = "2d")');
        $this->addSql('UPDATE `item` SET `visualization_mode_id` = (SELECT id FROM `visualization_mode` WHERE `identifier` = "3dVariant") WHERE `item`.`parent_id` IS NULL AND `item`.`id` IN (SELECT `item_id` FROM `itemsetting` WHERE `visualization_designer` = "3d")');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE `item` SET configuration_mode = NULL');

        $this->addSql('UPDATE `item` SET item_status_id = NULL');

        $this->addSql('UPDATE `item` SET visualization_mode_id = NULL');
    }
}
