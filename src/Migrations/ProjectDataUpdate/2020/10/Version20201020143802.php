<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201020143802 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE `design_production_method_price` SET `design_production_method_price`.`amount_from` = 1 WHERE `design_production_method_price`.`amount_from` IS NULL');
    }

    public function down(Schema $schema): void
    {
        // design_production_method_price cannot be reverted, as the previous state cannot be determined
    }
}
