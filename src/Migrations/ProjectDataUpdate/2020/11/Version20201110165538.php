<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201110165538 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE `rulefeedbacktexttranslation` JOIN `language` ON `rulefeedbacktexttranslation`.`language_id` = `language`.`id` JOIN `rulefeedbacktext` ON `rulefeedbacktexttranslation`.`rulefeedbacktext_id` = `rulefeedbacktext`.`id` SET `translation` = \'Werte für "[attribute]" stimmen nicht überein.\' WHERE `rulefeedbacktext`.`identifier` = "error_itemattributematch" AND `language`.`iso` = "de_DE"');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE `rulefeedbacktexttranslation` JOIN `language` ON `rulefeedbacktexttranslation`.`language_id` = `language`.`id` JOIN `rulefeedbacktext` ON `rulefeedbacktexttranslation`.`rulefeedbacktext_id` = `rulefeedbacktext`.`id` SET `translation` = \'Incompatible values for "[attribute]".\' WHERE `rulefeedbacktext`.`identifier` = "error_itemattributematch" AND `language`.`iso` = "de_DE"');
    }
}
