INSERT INTO itemgroup(id, identifier, sequencenumber, configurationvariant, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id)
VALUES
    (1, 'color', 1, 0, '2019-01-30 15:30:02', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL),
    (2, 'size', 2, 0, '2019-01-30 15:30:10', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL);

INSERT INTO itemgrouptranslation(id, translation, date_created, date_updated, date_deleted, user_created_id, user_updated_id, user_deleted_id, itemgroup_id, language_id)
VALUES
    (1, 'Color', '2019-01-30 15:34:13', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 1),
    (2, 'Farbe', '2019-01-30 15:34:20', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 1, 2),
    (3, 'Size', '2019-01-30 15:34:29', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 1),
    (4, 'Größe', '2019-01-30 15:34:39', NULL, '0001-01-01 00:00:00', NULL, NULL, NULL, 2, 2);