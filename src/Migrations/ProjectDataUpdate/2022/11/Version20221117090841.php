<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221117090841 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO `visualization_mode` (`id`, `identifier`, `valid_for_configuration_modes`, `date_created`, `label`)
                       VALUES
                       	(5, "2dVariantOverlay", \'[\"designer\"]\', NOW(), "2doverlay");');
    }

    public function down(Schema $schema): void
    {
    }
}
