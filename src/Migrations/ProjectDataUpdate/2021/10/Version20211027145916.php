<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211027145916 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $clientContactInfos = $this->connection->fetchAllAssociative('SELECT `client`.`id`, `client`.`contact_name`, `client`.`contact_street`, `client`.`contact_post_code`, `client`.`contact_city`, `client`.`contact_phone`, `client`.`contact_email` FROM `client`;');

        foreach ($clientContactInfos as $clientContactInfo) {
            // replace null values with empty string
            $clientContactInfo = array_map(function ($v) {
                return (is_null($v)) ? '' : $v;
            }, $clientContactInfo);

            $pdfHeader = $clientContactInfo['contact_email'] . '<br>' . $clientContactInfo['contact_phone'];
            $pdfFooter = implode(', ', array_filter([$clientContactInfo['contact_name'], $clientContactInfo['contact_street'], $clientContactInfo['contact_post_code']]));
            $pdfFooter = '' !== $clientContactInfo['contact_post_code'] ? $pdfFooter . ' ' . $clientContactInfo['contact_city'] : $pdfFooter;
            $values = ['pdfHeader' => $pdfHeader, 'pdfFooter' => $pdfFooter, 'id' => $clientContactInfo['id']];
            $this->addSql('UPDATE `client` SET `pdf_header_html` = :pdfHeader, `pdf_footer_html` = :pdfFooter, `pdf_header_text` = :pdfHeader, `pdf_footer_text` = :pdfFooter WHERE `id` = :id', $values);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
