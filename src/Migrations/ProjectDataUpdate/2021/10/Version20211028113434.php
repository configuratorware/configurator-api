<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211028113434 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE `client` SET `call_to_action` = (SELECT `call_to_action` FROM `setting` WHERE `setting`.`date_deleted` = "0001-01-01 00:00:00" LIMIT 1) WHERE `client`.`identifier` = "_default";');
    }

    public function down(Schema $schema): void
    {
    }
}
