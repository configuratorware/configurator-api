<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210923151523 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE rule SET date_deleted = NOW() WHERE ruletype_id = (SELECT id FROM ruletype WHERE identifier = "optionexclusion" LIMIT 1);');
    }

    public function down(Schema $schema): void
    {
        // down not possible
    }
}
