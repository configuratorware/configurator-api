<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210426142330 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        // get creator, creator+designer item ids that have more than one default configuration
        $itemIds = $this->connection->fetchAllNumeric('SELECT `item`.`id` FROM `item` LEFT JOIN `configuration` ON `configuration`.`item_id` = `item`.`id` LEFT JOIN `configurationtype` ON `configuration`.`configurationtype_id` = `configurationtype`.`id` WHERE `configuration`.`date_deleted` = "0001-01-01 00:00:00" AND `configurationtype`.`identifier` = "defaultoptions" AND `item`.`configuration_mode` IN ("creator", "creator+designer") GROUP BY `item`.`id` HAVING COUNT(`configuration`.`id`) > 1;');
        $itemIds = array_map('current', $itemIds);

        // for found items, delete empty default configuration if client id is not set
        if (!empty($itemIds)) {
            $this->addSql('UPDATE `configuration` JOIN `configurationtype` ON `configuration`.`configurationtype_id` = `configurationtype`.`id` JOIN `item` ON `configuration`.`item_id` = `item`.`id` SET `configuration`.`date_deleted` = NOW() WHERE `configurationtype`.`identifier` = "defaultoptions" AND `configuration`.`selectedoptions` = "[]" AND `configuration`.`client_id` IS NULL AND `item`.`id` IN (?);', [$itemIds], [Connection::PARAM_INT_ARRAY]);
        }
        // null client id for all default configurations
        $this->addSql('UPDATE `configuration` JOIN `configurationtype` ON `configuration`.`configurationtype_id` = `configurationtype`.`id` SET `configuration`.`client_id` = NULL WHERE `configurationtype`.`identifier` = "defaultoptions";');
    }

    public function down(Schema $schema): void
    {
        // configuration.client_id can not be reverted.
    }
}
