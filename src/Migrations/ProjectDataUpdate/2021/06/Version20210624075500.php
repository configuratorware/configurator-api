<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

final class Version20210624075500 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var mixed
     */
    private $configuratorImagesPath;

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $creatorDesignerItems = $this->connection->fetchAllAssociative('SELECT `item`.`id`, `item`.`identifier` FROM `item` WHERE `item`.`configuration_mode` = "creator+designer";');

        foreach ($creatorDesignerItems as $item) {
            $designViews = $this->connection->fetchAllAssociative('SELECT `design_view`.`id`, `design_view`.`item_id`, `design_view`.`identifier`, `design_view`.`sequence_number`, `design_view`.`custom_data`, `design_view`.`date_deleted` FROM `design_view` WHERE `design_view`.`item_id` = :id;', $item);
            $fileInfo = $this->findCreatorViews($item['identifier']);

            foreach ($designViews as $designView) {
                $designView = array_replace($designView, ['directory' => ''], $fileInfo[$designView['identifier']] ?? []);

                $this->addSql('INSERT INTO `creator_view` (`identifier`, `sequence_number`, `directory`, `item_id`, `date_created`, `date_deleted`) VALUES ( :identifier, :sequence_number, :directory, :item_id, NOW(), :date_deleted)', $designView);
                $this->addSql('DELETE `design_view` FROM `design_view` WHERE `design_view`.`id` = :id', $designView);

                $designViewTexts = $this->connection->fetchAllAssociative('SELECT `design_view_text`.`id`, `design_view_text`.`language_id`, `design_view_text`.`title`, `design_view_text`.`date_deleted` FROM `design_view_text` WHERE `design_view_text`.`design_view_id` = :id;', $designView);
                foreach ($designViewTexts as $designViewText) {
                    $designViewText['identifier'] = $designView['identifier'];
                    $designViewText['item_id'] = $designView['item_id'];
                    $designViewText['creator_view_date_deleted'] = $designView['date_deleted'];
                    $this->addSql('INSERT INTO `creator_view_text` (`language_id`, `title`, `creator_view_id`, `date_created`, `date_deleted`) VALUES ( :language_id, :title, (SELECT `creator_view`.`id` FROM `creator_view` WHERE `creator_view`.`identifier` = :identifier AND `creator_view`.`item_id` = :item_id AND `creator_view`.`date_deleted` = :creator_view_date_deleted), NOW(), :date_deleted)', $designViewText);
                    $this->addSql('DELETE `design_view_text` FROM `design_view_text` WHERE `design_view_text`.`id` = :id', $designViewText);
                }

                $designViewDesignAreas = $this->connection->fetchAllAssociative('SELECT `design_view_design_area`.`id`, `design_view_design_area`.`design_area_id`, `design_view_design_area`.`base_shape`, `design_view_design_area`.`position`, `design_view_design_area`.`custom_data`, `design_view_design_area`.`is_default`, `design_view_design_area`.`date_deleted` FROM `design_view_design_area` WHERE `design_view_design_area`.`design_view_id` = :id;', $designView);
                foreach ($designViewDesignAreas as $designViewDesignArea) {
                    $designViewDesignArea['identifier'] = $designView['identifier'];
                    $designViewDesignArea['item_id'] = $designView['item_id'];
                    $designViewDesignArea['creator_view_date_deleted'] = $designView['date_deleted'];
                    $this->addSql('INSERT INTO `creator_view_design_area` (`design_area_id`, `creator_view_id`, `base_shape`, `position`, `custom_data`, `is_default`, `date_created`, `date_deleted`) VALUES ( :design_area_id, (SELECT `creator_view`.`id` FROM `creator_view` WHERE `creator_view`.`identifier` = :identifier AND `creator_view`.`item_id` = :item_id AND `creator_view`.`date_deleted` = :creator_view_date_deleted), :base_shape, :position, :custom_data, :is_default, NOW(), :date_deleted)', $designViewDesignArea);
                    $this->addSql('DELETE `design_view_design_area` FROM `design_view_design_area` WHERE `design_view_design_area`.`id` = :id', $designViewDesignArea);
                }
            }
        }
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $creatorDesignerItems = $this->connection->fetchAllAssociative('SELECT `item`.`id`, `item`.`identifier` FROM `item` WHERE `item`.`configuration_mode` = "creator+designer";');

        foreach ($creatorDesignerItems as $item) {
            $creatorViews = $this->connection->fetchAllAssociative('SELECT `creator_view`.`id`, `creator_view`.`item_id`, `creator_view`.`identifier`, `creator_view`.`sequence_number`, `creator_view`.`custom_data`, `creator_view`.`date_deleted` FROM `creator_view` WHERE `creator_view`.`item_id` = :id;', $item);

            foreach ($creatorViews as $creatorView) {
                $this->addSql('INSERT INTO `design_view` (`identifier`, `sequence_number`, `item_id`, `date_created`, `date_deleted`) VALUES ( :identifier, :sequence_number, :item_id, NOW(), :date_deleted)', $creatorView);
                $this->addSql('DELETE `creator_view` FROM `creator_view` WHERE `creator_view`.`id` = :id', $creatorView);

                $creatorViewTexts = $this->connection->fetchAllAssociative('SELECT `creator_view_text`.`id`, `creator_view_text`.`language_id`, `creator_view_text`.`title`, `creator_view_text`.`date_deleted` FROM `creator_view_text` WHERE `creator_view_text`.`creator_view_id` = :id;', $creatorView);
                foreach ($creatorViewTexts as $creatorViewText) {
                    $creatorViewText['identifier'] = $creatorView['identifier'];
                    $creatorViewText['item_id'] = $creatorView['item_id'];
                    $creatorViewText['design_view_date_deleted'] = $creatorView['date_deleted'];
                    $this->addSql('INSERT INTO `design_view_text` (`language_id`, `title`, `design_view_id`, `date_created`, `date_deleted`) VALUES ( :language_id, :title, (SELECT `design_view`.`id` FROM `design_view` WHERE `design_view`.`identifier` = :identifier AND `design_view`.`item_id` = :item_id AND `design_view`.`date_deleted` = :design_view_date_deleted), NOW(), :date_deleted)', $creatorViewText);
                    $this->addSql('DELETE `creator_view_text` FROM `creator_view_text` WHERE `creator_view_text`.`id` = :id', $creatorViewText);
                }

                $creatorViewDesignAreas = $this->connection->fetchAllAssociative('SELECT `creator_view_design_area`.`id`, `creator_view_design_area`.`design_area_id`, `creator_view_design_area`.`base_shape`, `creator_view_design_area`.`position`, `creator_view_design_area`.`custom_data`, `creator_view_design_area`.`is_default`, `creator_view_design_area`.`date_deleted` FROM `creator_view_design_area` WHERE `creator_view_design_area`.`creator_view_id` = :id;', $creatorView);
                foreach ($creatorViewDesignAreas as $creatorViewDesignArea) {
                    $creatorViewDesignArea['identifier'] = $creatorView['identifier'];
                    $creatorViewDesignArea['item_id'] = $creatorView['item_id'];
                    $creatorViewDesignArea['design_view_date_deleted'] = $creatorView['date_deleted'];
                    $this->addSql('INSERT INTO `design_view_design_area` (`design_area_id`, `design_view_id`, `base_shape`, `position`, `custom_data`, `is_default`, `date_created`, `date_deleted`) VALUES ( :design_area_id, (SELECT `design_view`.`id` FROM `design_view` WHERE `design_view`.`identifier` = :identifier AND `design_view`.`item_id` = :item_id AND `design_view`.`date_deleted` = :design_view_date_deleted), :base_shape, :position, :custom_data, :is_default, NOW(), :date_deleted)', $creatorViewDesignArea);
                    $this->addSql('DELETE `creator_view_design_area` FROM `creator_view_design_area` WHERE `creator_view_design_area`.`id` = :id', $creatorViewDesignArea);
                }
            }
        }
    }

    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    private function findCreatorViews(string $itemIdentifier): array
    {
        $viewDirectory = $this->getViewsDirectory($itemIdentifier);
        if (!is_dir($viewDirectory)) {
            return [];
        }

        $finder = new Finder();
        $finder->in($viewDirectory)->directories()->depth('<1');

        $viewDirectories = [];
        foreach ($finder as $fileInfo) {
            $dirName = $fileInfo->getFilename();
            $viewDirectories[$this->trimViewFolderName($dirName)] = [
                'sequence_number' => $this->trimViewOrderNumber($dirName),
                'directory' => $this->trimConfiguratorImagesPath($fileInfo->getPathname()),
            ];
        }

        return $viewDirectories;
    }

    /**
     * @param string $viewDirectoryName
     *
     * @return string
     */
    private function trimViewFolderName(string $viewDirectoryName): string
    {
        return preg_replace('/^\d+_/', '', $viewDirectoryName);
    }

    /**
     * @param $dirName
     *
     * @return string
     */
    private function trimViewOrderNumber($dirName): string
    {
        preg_match('/^\d+_/', $dirName, $matchingString);

        return trim($matchingString[0] ?? '', '_');
    }

    /**
     * @param string $path
     *
     * @return string
     */
    private function trimConfiguratorImagesPath(string $path): string
    {
        return str_replace($this->configuratorImagesPath . DIRECTORY_SEPARATOR, '', $path);
    }

    /**
     * @param string $itemIdentifier
     *
     * @return string
     */
    private function getViewsDirectory(string $itemIdentifier): string
    {
        return $this->configuratorImagesPath . DIRECTORY_SEPARATOR . $itemIdentifier;
    }

    /**
     * @param ContainerInterface|null $container
     *
     * @throws \Exception
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        if (null === $container) {
            throw new \Exception('Container must be set to execute this migration.');
        }

        $this->configuratorImagesPath = $container->getParameter('configurator_images_path');
    }
}
