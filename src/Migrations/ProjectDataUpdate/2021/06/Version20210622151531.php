<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;

final class Version20210622151531 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var mixed
     */
    private $configuratorImagesPath;

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $creatorItems = $this->connection->fetchAllAssociative('SELECT `item`.`id`, `item`.`identifier` FROM `item` WHERE `item`.`date_deleted` = "0001-01-01 00:00:00" AND `item`.`configuration_mode` = "creator";');

        foreach ($creatorItems as $creatorItem) {
            $creatorViews = $this->findCreatorViews($creatorItem['identifier']);
            foreach ($creatorViews as $creatorView) {
                $creatorView['itemId'] = $creatorItem['id'];
                $this->addSql("INSERT INTO `creator_view` (`identifier`, `sequence_number`, `directory`, `item_id`, `date_created`, `date_deleted`) VALUES ( :identifier, :sequenceNumber, :directory, :itemId, NOW(), '0001-01-01 00:00:00')", $creatorView);
            }
        }
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DELETE `creator_view` FROM `creator_view` JOIN `item` ON `creator_view`.`item_id` = `item`.`id` WHERE `item`.`configuration_mode` = "creator"');
    }

    /**
     * @param string $itemIdentifier
     *
     * @return array
     */
    private function findCreatorViews(string $itemIdentifier): array
    {
        $viewDirectory = $this->getViewsDirectory($itemIdentifier);
        if (!is_dir($viewDirectory)) {
            return [];
        }

        $finder = new Finder();
        $finder->in($viewDirectory)->directories()->depth('<1');

        $viewDirectories = [];
        foreach ($finder as $fileInfo) {
            $dirName = $fileInfo->getFilename();
            $viewDirectories[] = [
                'identifier' => $this->trimViewFolderName($dirName),
                'sequenceNumber' => $this->trimViewOrderNumber($dirName),
                'directory' => $this->trimConfiguratorImagesPath($fileInfo->getPathname()),
            ];
        }

        return $viewDirectories;
    }

    /**
     * @param string $viewDirectoryName
     *
     * @return string
     */
    private function trimViewFolderName(string $viewDirectoryName): string
    {
        return preg_replace('/^\d+_/', '', $viewDirectoryName);
    }

    /**
     * @param $dirName
     *
     * @return string
     */
    private function trimViewOrderNumber($dirName): string
    {
        preg_match('/^\d+_/', $dirName, $matchingString);

        return trim($matchingString[0] ?? '', '_');
    }

    /**
     * @param string $path
     *
     * @return string
     */
    private function trimConfiguratorImagesPath(string $path): string
    {
        return str_replace($this->configuratorImagesPath . DIRECTORY_SEPARATOR, '', $path);
    }

    /**
     * @param string $itemIdentifier
     *
     * @return string
     */
    private function getViewsDirectory(string $itemIdentifier): string
    {
        return $this->configuratorImagesPath . DIRECTORY_SEPARATOR . $itemIdentifier;
    }

    /**
     * @param ContainerInterface|null $container
     *
     * @throws \Exception
     */
    public function setContainer(ContainerInterface $container = null): void
    {
        if (null === $container) {
            throw new \Exception('Container must be set to execute this migration.');
        }

        $this->configuratorImagesPath = $container->getParameter('configurator_images_path');
    }
}
