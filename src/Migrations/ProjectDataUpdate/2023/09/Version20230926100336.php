<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\ProjectDataUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230926100336 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $existingGroups = $this->connection->fetchAllNumeric('SELECT COUNT(`itemgroup`.`id`) FROM `itemgroup`');

        if (isset($existingGroups[0],$existingGroups[0][0]) && ('0' === $existingGroups[0][0] || 0 === $existingGroups[0][0])) {
            $this->addSql(file_get_contents(__DIR__ . '/../../sql/Version20230926100336.sql'));
        }
    }

    public function down(Schema $schema): void
    {
    }
}
