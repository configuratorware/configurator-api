<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\BaseData;

use Doctrine\DBAL\Exception as DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201005101010 extends AbstractMigration
{
    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO `visualization_mode` (`id`, `identifier`, `valid_for_configuration_modes`, `date_created`, `date_deleted`, `label`) VALUES (1, '2dLayer', '[\"creator\",\"creator+designer\"]', NOW(), '0001-01-01 00:00:00', '2d'), (2, '2dVariant', '[\"designer\"]', NOW(), '0001-01-01 00:00:00', '2d'), (3, '3dScene', '[\"creator\",\"creator+designer\"]', NOW(), '0001-01-01 00:00:00', '3d'), (4, '3dVariant', '[\"designer\"]', NOW(), '0001-01-01 00:00:00', '3d');");
        $this->addSql("UPDATE `setting` SET `default_visualization_mode_label` = '2d';");
    }

    /**
     * @param Schema $schema
     *
     * @throws DBALException
     */
    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM `visualization_mode`;');
        $this->addSql("UPDATE `setting` SET `default_visualization_mode_label` = NULL';");
    }
}
