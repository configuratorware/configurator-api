<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210622123613 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE creator_view (id BIGINT AUTO_INCREMENT NOT NULL, item_id BIGINT DEFAULT NULL, identifier VARCHAR(255) DEFAULT NULL, directory VARCHAR(255) DEFAULT NULL, sequence_number VARCHAR(255) DEFAULT NULL, custom_data LONGTEXT DEFAULT NULL, date_created DATETIME NOT NULL, date_updated DATETIME DEFAULT NULL, date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\' NOT NULL, user_created_id BIGINT DEFAULT NULL, user_updated_id BIGINT DEFAULT NULL, user_deleted_id BIGINT DEFAULT NULL, INDEX IDX_8875EA52126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE creator_view_design_area (id BIGINT AUTO_INCREMENT NOT NULL, design_area_id BIGINT DEFAULT NULL, creator_view_id BIGINT DEFAULT NULL, base_shape LONGTEXT DEFAULT NULL, position LONGTEXT DEFAULT NULL, custom_data LONGTEXT DEFAULT NULL, is_default TINYINT(1) DEFAULT \'0\', date_created DATETIME NOT NULL, date_updated DATETIME DEFAULT NULL, date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\' NOT NULL, user_created_id BIGINT DEFAULT NULL, user_updated_id BIGINT DEFAULT NULL, user_deleted_id BIGINT DEFAULT NULL, INDEX IDX_C38180824A4D0B6 (design_area_id), INDEX IDX_C3818088D87A1B3 (creator_view_id), UNIQUE INDEX uq_default (creator_view_id, design_area_id, date_deleted), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE creator_view_text (id BIGINT AUTO_INCREMENT NOT NULL, language_id BIGINT DEFAULT NULL, creator_view_id BIGINT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, date_created DATETIME NOT NULL, date_updated DATETIME DEFAULT NULL, date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\' NOT NULL, user_created_id BIGINT DEFAULT NULL, user_updated_id BIGINT DEFAULT NULL, user_deleted_id BIGINT DEFAULT NULL, INDEX IDX_3CDF52D382F1BAF4 (language_id), INDEX IDX_3CDF52D38D87A1B3 (creator_view_id), UNIQUE INDEX uq_default (creator_view_id, language_id, date_deleted), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE creator_view');
        $this->addSql('DROP TABLE creator_view_design_area');
        $this->addSql('DROP TABLE creator_view_text');
    }
}
