<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210602121406 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE configuration ADD channel_id BIGINT DEFAULT NULL, ADD language_id BIGINT DEFAULT NULL, ADD calculation_result LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql('CREATE INDEX IDX_A5E2A5D772F5A1AA ON configuration (channel_id)');
        $this->addSql('CREATE INDEX IDX_A5E2A5D782F1BAF4 ON configuration (language_id)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX IDX_A5E2A5D772F5A1AA ON configuration');
        $this->addSql('DROP INDEX IDX_A5E2A5D782F1BAF4 ON configuration');
        $this->addSql('ALTER TABLE configuration DROP channel_id, DROP language_id, DROP calculation_result');
    }
}
