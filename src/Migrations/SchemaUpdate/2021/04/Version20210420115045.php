<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210420115045 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE inspiration (id BIGINT AUTO_INCREMENT NOT NULL, item_id BIGINT DEFAULT NULL, active TINYINT(1) DEFAULT \'0\' NOT NULL, configuration_code VARCHAR(255) DEFAULT NULL, thumbnail VARCHAR(255) DEFAULT NULL, date_created DATETIME NOT NULL, date_updated DATETIME DEFAULT NULL, date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\' NOT NULL, user_created_id BIGINT DEFAULT NULL, user_updated_id BIGINT DEFAULT NULL, user_deleted_id BIGINT DEFAULT NULL, INDEX IDX_FDEC4440126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE inspiration_text (id BIGINT AUTO_INCREMENT NOT NULL, language_id BIGINT DEFAULT NULL, inspiration_id BIGINT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, date_created DATETIME NOT NULL, date_updated DATETIME DEFAULT NULL, date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\' NOT NULL, user_created_id BIGINT DEFAULT NULL, user_updated_id BIGINT DEFAULT NULL, user_deleted_id BIGINT DEFAULT NULL, INDEX IDX_2BC1F6CB82F1BAF4 (language_id), INDEX IDX_2BC1F6CB2B726C5F (inspiration_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE inspiration');
        $this->addSql('DROP TABLE inspiration_text');
    }
}
