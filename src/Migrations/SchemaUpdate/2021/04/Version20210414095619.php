<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210414095619 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM `option_classification_text` WHERE `option_classification_text`.`date_deleted` = "0001-01-01 00:00:00" AND `option_classification_text`.`id` NOT IN (SELECT MAX(id) FROM (SELECT * FROM `option_classification_text`) AS `inner_text` GROUP BY `option_classification_id`, `language_id`)');
        $this->addSql('DROP INDEX uq_default ON option_classification_text');
        $this->addSql('CREATE UNIQUE INDEX uq_default ON option_classification_text (date_deleted, option_classification_id, language_id)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX uq_default ON option_classification_text');
        $this->addSql('CREATE INDEX uq_default ON option_classification_text (date_deleted, option_classification_id, language_id)');
    }
}
