<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211014115304 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE client ADD from_email_address VARCHAR(255) DEFAULT NULL, ADD to_email_addresses VARCHAR(255) DEFAULT NULL, ADD cc_email_addresses VARCHAR(255) DEFAULT NULL, ADD bcc_email_addresses VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE client DROP from_email_address, DROP to_email_addresses, DROP cc_email_addresses, DROP bcc_email_addresses');
    }
}
