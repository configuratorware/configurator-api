<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211014081623 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE setting ADD configurations_client_restricted TINYINT(1) DEFAULT \'0\', CHANGE show_item_identifier show_item_identifier TINYINT(1) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE setting DROP configurations_client_restricted, CHANGE show_item_identifier show_item_identifier TINYINT(1) DEFAULT \'0\'');
    }
}
