<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211018145816 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client ADD pdf_header_html LONGTEXT DEFAULT NULL, ADD pdf_footer_html LONGTEXT DEFAULT NULL, ADD pdf_header_text LONGTEXT DEFAULT NULL, ADD pdf_footer_text LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client DROP pdf_header_html, DROP pdf_footer_html, DROP pdf_header_text, DROP pdf_footer_text');
    }
}
