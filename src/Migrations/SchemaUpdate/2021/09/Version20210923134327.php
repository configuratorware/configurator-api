<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210923134327 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE rule ADD reverse_rule_id BIGINT DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_46D8ACCC13771DD8 ON rule (reverse_rule_id)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX IDX_46D8ACCC13771DD8 ON rule');
        $this->addSql('ALTER TABLE rule DROP reverse_rule_id');
    }
}
