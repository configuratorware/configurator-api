<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20250304080104 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE setting CHANGE max_zoom_2d max_zoom_2d DOUBLE PRECISION DEFAULT \'2\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE setting CHANGE max_zoom_2d max_zoom_2d DOUBLE PRECISION DEFAULT \'2.0\' NOT NULL');
    }
}
