<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221201121346 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `option` ADD `has_textinput` TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE `option` ADD `input_validation_type` TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `option` DROP `has_textinput`');
        $this->addSql('ALTER TABLE `option` DROP `input_validation_type`');
    }
}
