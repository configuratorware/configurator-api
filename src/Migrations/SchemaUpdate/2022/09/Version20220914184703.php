<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220914184703 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE question_tree (
                                id INT AUTO_INCREMENT NOT NULL, 
                                identifier VARCHAR(255) NOT NULL,
                                date_created DATETIME NOT NULL,
                                date_updated DATETIME DEFAULT NULL,
                                date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\',
                                user_created_id BIGINT DEFAULT NULL,
                                user_updated_id BIGINT DEFAULT NULL,
                                user_deleted_id BIGINT DEFAULT NULL,
                                UNIQUE INDEX uq_identifier_date_deleted (identifier, date_deleted), PRIMARY KEY(id)
                            ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question_tree_data (
                                id INT AUTO_INCREMENT NOT NULL,
                                file_name VARCHAR(255) DEFAULT NULL,
                                language_id BIGINT NOT NULL,
                                question_tree_id INT NOT NULL,
                                data LONGTEXT DEFAULT NULL,
                                date_created DATETIME NOT NULL,
                                date_updated DATETIME DEFAULT NULL,
                                date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\',
                                user_created_id BIGINT DEFAULT NULL,
                                user_updated_id BIGINT DEFAULT NULL,
                                user_deleted_id BIGINT DEFAULT NULL,
                                INDEX IDX_1BA5DDCF82F1BAF4 (language_id),
                                INDEX IDX_1BA5DDCF957B1BF (question_tree_id),
                                PRIMARY KEY(id)
                            ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question_tree_text (
                                id INT AUTO_INCREMENT NOT NULL,
                                language_id BIGINT NOT NULL,
                                question_tree_id INT NOT NULL,
                                title VARCHAR(255) DEFAULT NULL,
                                date_created DATETIME NOT NULL,
                                date_updated DATETIME DEFAULT NULL,
                                date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\',
                                user_created_id BIGINT DEFAULT NULL,
                                user_updated_id BIGINT DEFAULT NULL,
                                user_deleted_id BIGINT DEFAULT NULL,
                                INDEX IDX_8DDD896B82F1BAF4 (language_id),
                                INDEX IDX_8DDD896B957B1BF (question_tree_id),
                                UNIQUE INDEX uq_default (date_deleted, question_tree_id, language_id),
                                PRIMARY KEY(id)
                            ) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE question_tree_data
                                ADD CONSTRAINT FK_1BA5DDCF82F1BAF4
                                FOREIGN KEY (language_id)
                                REFERENCES language (id)');
        $this->addSql('ALTER TABLE question_tree_data
                                ADD CONSTRAINT FK_1BA5DDCF957B1BF
                                FOREIGN KEY (question_tree_id)
                                REFERENCES question_tree (id)');
        $this->addSql('ALTER TABLE question_tree_text
                                ADD CONSTRAINT FK_8DDD896B82F1BAF4
                                FOREIGN KEY (language_id)
                                REFERENCES language (id)');
        $this->addSql('ALTER TABLE question_tree_text
                                ADD CONSTRAINT FK_8DDD896B957B1BF
                                FOREIGN KEY (question_tree_id)
                                REFERENCES question_tree (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE question_tree_data DROP FOREIGN KEY FK_1BA5DDCF957B1BF');
        $this->addSql('ALTER TABLE question_tree_text DROP FOREIGN KEY FK_8DDD896B957B1BF');
        $this->addSql('DROP TABLE question_tree');
        $this->addSql('DROP TABLE question_tree_data');
        $this->addSql('DROP TABLE question_tree_text');
    }
}
