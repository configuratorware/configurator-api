<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230911142310 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE item ADD override_option_order TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE item_optionclassification_option ADD sequence_number BIGINT DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE item DROP override_option_order');
        $this->addSql('ALTER TABLE item_optionclassification_option DROP sequence_number');
    }
}
