<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230906115944 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add field "vectorizedLogoMandatory" to DesignProductionMethod entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE design_production_method ADD vectorized_logo_mandatory TINYINT(1) DEFAULT \'0\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE attribute DROP write_protected');
    }
}
