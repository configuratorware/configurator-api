<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231012141730 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `designer_global_calculation_type` ADD `price_per_item` TINYINT(1) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `designer_global_calculation_type` DROP `price_per_item`');
    }
}
