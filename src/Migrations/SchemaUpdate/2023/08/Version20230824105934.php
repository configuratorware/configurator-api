<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Redhotmagma\ConfiguratorApiBundle\Entity\DesignerGlobalCalculationType;

final class Version20230824105934 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update global calculation types to support more types of image related data';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE designer_global_calculation_type ADD image_dependent VARCHAR(255) DEFAULT NULL');
        $this->addSql('
                    UPDATE designer_global_calculation_type 
                    SET image_dependent="' . DesignerGlobalCalculationType::IMAGE_DATA_DEPENDENT . '" 
                    WHERE image_data_dependent=1');
        $this->addSql('ALTER TABLE designer_global_calculation_type DROP image_data_dependent');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE designer_global_calculation_type ADD image_data_dependent TINYINT(1) DEFAULT NULL');
        $this->addSql('
                    UPDATE designer_global_calculation_type 
                    SET image_data_dependent="' . DesignerGlobalCalculationType::IMAGE_DATA_DEPENDENT . '" 
                    WHERE image_dependent IS NOT NULL');
        $this->addSql('ALTER TABLE designer_global_calculation_type DROP image_dependent');
    }
}
