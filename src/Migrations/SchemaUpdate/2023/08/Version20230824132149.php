<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230824132149 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add field "writeProtected" to Attribute entity';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE attribute ADD write_protected TINYINT(1) DEFAULT \'0\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE attribute DROP write_protected');
    }
}
