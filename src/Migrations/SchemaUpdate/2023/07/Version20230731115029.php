<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230731115029 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE `image_gallery_image_price` 
            (`id` BIGINT AUTO_INCREMENT NOT NULL, 
            `channel_id` BIGINT DEFAULT NULL, 
            `imagegalleryimage_id` BIGINT DEFAULT NULL, 
            `price` NUMERIC(25, 5) DEFAULT NULL, 
            `price_net` NUMERIC(25, 5) DEFAULT NULL, 
            `date_created` DATETIME NOT NULL, 
            `date_updated` DATETIME DEFAULT NULL, 
            `date_deleted` DATETIME DEFAULT \'0001-01-01 00:00:00\', 
            `user_created_id` BIGINT DEFAULT NULL, 
            `user_updated_id` BIGINT DEFAULT NULL, 
            `user_deleted_id` BIGINT DEFAULT NULL, 
            INDEX IDX_9B1ED9E772F5A1AA (`channel_id`), INDEX IDX_9B1ED9E73A780FDD (`imagegalleryimage_id`), 
            UNIQUE INDEX uq_default (`date_deleted`, `channel_id`, `imagegalleryimage_id`), 
            PRIMARY KEY(`id`)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql('ALTER TABLE `image_gallery_image_price` ADD CONSTRAINT FK_9B1ED9E772F5A1AA FOREIGN KEY (`channel_id`) REFERENCES channel (`id`)');
        $this->addSql('ALTER TABLE `image_gallery_image_price` ADD CONSTRAINT FK_9B1ED9E73A780FDD FOREIGN KEY (`imagegalleryimage_id`) REFERENCES imagegalleryimage (`id`)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE image_gallery_image_price');
    }
}
