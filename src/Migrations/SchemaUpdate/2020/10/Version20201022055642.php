<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201022055642 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add show_item_identifier to settings table';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE setting ADD show_item_identifier TINYINT(1) DEFAULT \'0\'');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE setting DROP show_item_identifier');
    }
}
