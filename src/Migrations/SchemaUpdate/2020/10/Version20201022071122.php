<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201022071122 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add call to action to setting and item';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE setting ADD call_to_action VARCHAR(255) DEFAULT \'addToCart\'');
        $this->addSql('ALTER TABLE item ADD call_to_action VARCHAR(255)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE setting DROP call_to_action');
        $this->addSql('ALTER TABLE item DROP call_to_action');
    }
}
