<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200924151335 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE visualization_mode (id BIGINT AUTO_INCREMENT NOT NULL, identifier VARCHAR(255) DEFAULT NULL, valid_for_configuration_modes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json)\', date_created DATETIME NOT NULL, date_updated DATETIME DEFAULT NULL, date_deleted DATETIME DEFAULT \'0001-01-01 00:00:00\' NOT NULL, user_created_id BIGINT DEFAULT NULL, user_updated_id BIGINT DEFAULT NULL, user_deleted_id BIGINT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE item ADD visualization_mode_id BIGINT DEFAULT NULL, ADD item_status_id BIGINT DEFAULT NULL, ADD configuration_mode VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_1F1B251ED5002379 ON item (visualization_mode_id)');
        $this->addSql('CREATE INDEX IDX_1F1B251E672D164D ON item (item_status_id)');
        $this->addSql('ALTER TABLE optionclassification ADD hidden_in_frontend TINYINT(1) DEFAULT \'0\'');
        $this->addSql('ALTER TABLE setting ADD default_visualization_mode_label VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE visualization_mode ADD `label` VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE visualization_mode');
        $this->addSql('DROP INDEX IDX_1F1B251ED5002379 ON item');
        $this->addSql('DROP INDEX IDX_1F1B251E672D164D ON item');
        $this->addSql('ALTER TABLE item DROP visualization_mode_id, DROP item_status_id, DROP configuration_mode');
        $this->addSql('ALTER TABLE optionclassification DROP hidden_in_frontend');
        $this->addSql('ALTER TABLE setting DROP default_visualization_mode_label');
        $this->addSql('ALTER TABLE visualization_mode DROP `label`');
    }
}
