<?php

declare(strict_types=1);

namespace Redhotmagma\ConfiguratorApiBundle\Migrations\SchemaUpdate;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200904122800 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client ADD custom_css LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE setting ADD custom_css LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE channel ADD global_discount_percentage NUMERIC(15, 5) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE client DROP custom_css');
        $this->addSql('ALTER TABLE setting DROP custom_css');
        $this->addSql('ALTER TABLE channel DROP global_discount_percentage');
    }
}
